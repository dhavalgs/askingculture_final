﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starteku.Data.Manager;
using System.Data;
namespace starteku_BusinessLogic
{
    public class IndustryBM
    {
        #region Private Declaration

        private DataSet _ds;
        private string _returnString;
        private bool _returnBoolean;
        private Int32 _indId;
        private Int32 _intParentId;
        private String _indName;
        private DateTime _indCreatedDate;
        private DateTime _indUpdatedDate;
        private Boolean _indIsDeleted;
        private Boolean _indIsActive;
        private Int32 _indCompanyId;
        private String _indCreateBy;
        private Int32 _indDepId;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public DataSet ds
        {
            get { return _ds; }
            set { _ds = value; }
        }
        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public string ReturnString
        {
            get { return _returnString; }
            set { _returnString = value; }
        }

        /// <summary>
        /// Gets or sets the ReturnBoolean value.
        /// </summary>
        public bool ReturnBoolean
        {
            get { return _returnBoolean; }
            set { _returnBoolean = value; }
        }

        /// <summary>
        /// Gets or sets the indId value.
        /// </summary>
        public Int32 indId
        {
            get { return _indId; }
            set { _indId = value; }
        }

        /// <summary>
        /// Gets or sets the intParentId value.
        /// </summary>
        public Int32 intParentId
        {
            get { return _intParentId; }
            set { _intParentId = value; }
        }

        /// <summary>
        /// Gets or sets the indName value.
        /// </summary>
        public String indName
        {
            get { return _indName; }
            set { _indName = value; }
        }

        /// <summary>
        /// Gets or sets the indCreatedDate value.
        /// </summary>
        public DateTime indCreatedDate
        {
            get { return _indCreatedDate; }
            set { _indCreatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the indUpdatedDate value.
        /// </summary>
        public DateTime indUpdatedDate
        {
            get { return _indUpdatedDate; }
            set { _indUpdatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the indIsDeleted value.
        /// </summary>
        public Boolean indIsDeleted
        {
            get { return _indIsDeleted; }
            set { _indIsDeleted = value; }
        }

        /// <summary>
        /// Gets or sets the indIsActive value.
        /// </summary>
        public Boolean indIsActive
        {
            get { return _indIsActive; }
            set { _indIsActive = value; }
        }

        /// <summary>
        /// Gets or sets the indCompanyId value.
        /// </summary>
        public Int32 indCompanyId
        {
            get { return _indCompanyId; }
            set { _indCompanyId = value; }
        }

        /// <summary>
        /// Gets or sets the indCreateBy value.
        /// </summary>
        public String indCreateBy
        {
            get { return _indCreateBy; }
            set { _indCreateBy = value; }
        }

        /// <summary>
        /// Gets or sets the indDepId value.
        /// </summary>
        public Int32 indDepId
        {
            get { return _indDepId; }
            set { _indDepId = value; }
        }

        #endregion
        
        #region Methods
        public void InsertIndustry()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[9];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue; 

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@intParentId";
                param[0].DataType = DbType.Int32;
                param[0].value = _intParentId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@indName";
                param[1].DataType = DbType.String;
                if (String.IsNullOrEmpty(_indName))
                    param[1].value = DBNull.Value;
                else
                    param[1].value = _indName;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@indCreatedDate";
                param[2].DataType = DbType.DateTime;
                if (_indCreatedDate.Ticks > dtmin.Ticks && _indCreatedDate.Ticks < dtmax.Ticks)
                    param[2].value = _indCreatedDate;
                else if (_indCreatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[2].value = _indCreatedDate;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@indUpdatedDate";
                param[3].DataType = DbType.DateTime;
                if (_indUpdatedDate.Ticks > dtmin.Ticks && _indUpdatedDate.Ticks < dtmax.Ticks)
                    param[3].value = _indUpdatedDate;
                else if (_indUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[3].value = _indUpdatedDate;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@indIsDeleted";
                param[4].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_indIsDeleted.ToString()))
                    param[4].value = DBNull.Value;
                else
                    param[4].value = _indIsDeleted;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@indIsActive";
                param[5].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_indIsActive.ToString()))
                    param[5].value = DBNull.Value;
                else
                    param[5].value = _indIsActive;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@indCompanyId";
                param[6].DataType = DbType.Int32;               
                param[6].value = _indCompanyId;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@indCreateBy";
                param[7].DataType = DbType.String;
                if (String.IsNullOrEmpty(_indCreateBy))
                    param[7].value = DBNull.Value;
                else
                    param[7].value = _indCreateBy;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@indDepId";
                param[8].DataType = DbType.Int32;               
                param[8].value = _indDepId;

            
                _ds = DBAccess.ExecDataSet("InsertIndustry", CommandType.StoredProcedure, param);
                _returnBoolean = true;
               
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }
        public void UpdateIndustry()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[4];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue; 

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@indId";
                param[0].DataType = DbType.Int32;
                param[0].value = _indId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@intParentId";
                param[1].DataType = DbType.Int32;
                param[1].value = _intParentId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@indName";
                param[2].DataType = DbType.String;
                if (String.IsNullOrEmpty(_indName))
                    param[2].value = DBNull.Value;
                else
                    param[2].value = _indName;

               
                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@indUpdatedDate";
                param[3].DataType = DbType.DateTime;
                if (_indUpdatedDate.Ticks > dtmin.Ticks && _indUpdatedDate.Ticks < dtmax.Ticks)
                    param[3].value = _indUpdatedDate;
                else if (_indUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[3].value = _indUpdatedDate;

                _ds = DBAccess.ExecDataSet("UpdateIndustry", CommandType.StoredProcedure, param);
                _returnBoolean = true;
               
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        public void GetAllIndustry()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@indIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _indIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@indIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _indIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@indCompanyId";
                param[2].DataType = DbType.Int32;
                param[2].value = _indCompanyId;

                _ds = DBAccess.ExecDataSet("GetAllIndustry", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllDepartments" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void IndustryStatusUpdate()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] pm = new ParamStruct[3];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@indId";
                pm[0].DataType = DbType.Int32;
                pm[0].value = _indId;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@indIsActive";
                pm[1].DataType = DbType.Boolean;
                pm[1].value = _indIsActive;

                pm[2].direction = ParameterDirection.Input;
                pm[2].ParamName = "@indIsDeleted";
                pm[2].DataType = DbType.Boolean;
                pm[2].value = _indIsDeleted;

                _ds = DBAccess.ExecDataSet("IndustryStatusUpdate", CommandType.StoredProcedure, pm);
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in IndustryStatusUpdate  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            }
        }

        public void getIndustrybyid()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@indId";
                param[0].DataType = DbType.Int32;
                param[0].value = _indId;


                _ds = DBAccess.ExecDataSet("getIndustrybyid", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in getIndustrybyid" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void IndustryCheckDuplication(string name, Int32 id)
        {
            try
            {
                DataAccess obj = new DataAccess();
                obj.Provider = EnumProviders.SQLClient;
                obj.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["connstring"].ToString();

                ParamStruct[] param = new ParamStruct[2];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue; 

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@indName";
                param[0].DataType = DbType.String;
                param[0].value = name;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@indId";
                param[1].DataType = DbType.Int32;
                param[1].value = id;

                _ds = obj.ExecDataSet("IndustryCheckDuplication", CommandType.StoredProcedure, param);
                if (_ds.Tables[0].Rows.Count > 0)
                {
                    _returnString = (_ds.Tables[0].Rows[0]["returnStatement"].ToString());

                }

            }
            catch (DataException ex)
            {
                throw ex;
            }


        }

        public void GetIndustryFromParentId()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] pm = new ParamStruct[2];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@intParentId";
                pm[0].DataType = DbType.Int32;
                pm[0].value = _intParentId;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@indCompanyId";
                pm[1].DataType = DbType.Int32;
                pm[1].value = _indCompanyId;


                _ds = DBAccess.ExecDataSet("GetIndustryFromParentId", CommandType.StoredProcedure, pm);
            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetIndustryFromParentId" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;
            }
        }

        #endregion


    }
}
