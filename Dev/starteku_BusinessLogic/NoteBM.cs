using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starteku.Data.Manager;
using System.Data;
using starteku_BusinessLogic;

namespace startetku.Business.Logic {

	public class NoteBM {
		#region Private Declaration

		private DataSet _ds;
		private string _returnString;
		private bool _returnBoolean;
		private Int32 _noteId;
		private String _noteName;
		private DateTime _noteCreatedDate;
		private DateTime _noteUpdatedDate;
		private Boolean _noteIsDeleted;
		private Boolean _noteIsActive;
		private Int32 _noteCompanyId;
        private Int32 _noteUserId;
		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the ReturnString value.
		/// </summary>
		public DataSet ds
		{
			get { return _ds; }
			set { _ds = value; }
		}
		/// <summary>
		/// Gets or sets the ReturnString value.
		/// </summary>
		public string ReturnString {
			get { return _returnString; }
			set {  _returnString = value; }
		}

		/// <summary>
		/// Gets or sets the ReturnBoolean value.
		/// </summary>
		public bool ReturnBoolean {
			get { return _returnBoolean; }
			set {  _returnBoolean = value; }
		}

		/// <summary>
		/// Gets or sets the noteId value.
		/// </summary>
		public Int32 noteId  {
			get { return _noteId; }
			set { _noteId = value; }
		}
        public Int32 noteUserId
        {
            get { return _noteUserId; }
            set { _noteUserId = value; }
        }
		/// <summary>
		/// Gets or sets the noteName value.
		/// </summary>
		public String noteName  {
			get { return _noteName; }
			set { _noteName = value; }
		}

		/// <summary>
		/// Gets or sets the noteCreatedDate value.
		/// </summary>
		public DateTime noteCreatedDate  {
			get { return _noteCreatedDate; }
			set { _noteCreatedDate = value; }
		}

		/// <summary>
		/// Gets or sets the noteUpdatedDate value.
		/// </summary>
		public DateTime noteUpdatedDate  {
			get { return _noteUpdatedDate; }
			set { _noteUpdatedDate = value; }
		}

		/// <summary>
		/// Gets or sets the noteIsDeleted value.
		/// </summary>
		public Boolean noteIsDeleted  {
			get { return _noteIsDeleted; }
			set { _noteIsDeleted = value; }
		}

		/// <summary>
		/// Gets or sets the noteIsActive value.
		/// </summary>
		public Boolean noteIsActive  {
			get { return _noteIsActive; }
			set { _noteIsActive = value; }
		}

		/// <summary>
		/// Gets or sets the noteCompanyId value.
		/// </summary>
		public Int32 noteCompanyId  {
			get { return _noteCompanyId; }
			set { _noteCompanyId = value; }
		}

		#endregion

		#region Methods
		
		/// <summary>
		/// Inserts Records in the Note table.
		/// </summary>
		public void InsertNote() {
			try 
			{
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

				ParamStruct[] param = new ParamStruct[7];
				DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue; 

				param[0].direction = ParameterDirection.Input;
				param[0].ParamName = "@noteName";
				param[0].DataType = DbType.String;
				if (String.IsNullOrEmpty(_noteName))
					param[0].value = DBNull.Value;
				else
					param[0].value = _noteName;

				param[1].direction = ParameterDirection.Input;
				param[1].ParamName = "@noteCreatedDate";
				param[1].DataType = DbType.DateTime;
				if (_noteCreatedDate.Ticks > dtmin.Ticks && _noteCreatedDate.Ticks < dtmax.Ticks)
					param[1].value = _noteCreatedDate;
				else if (_noteCreatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
				{}
				else
					param[1].value = _noteCreatedDate;

				param[2].direction = ParameterDirection.Input;
				param[2].ParamName = "@noteUpdatedDate";
				param[2].DataType = DbType.DateTime;
				if (_noteUpdatedDate.Ticks > dtmin.Ticks && _noteUpdatedDate.Ticks < dtmax.Ticks)
					param[2].value = _noteUpdatedDate;
				else if (_noteUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
				{}
				else
					param[2].value = _noteUpdatedDate;

				param[3].direction = ParameterDirection.Input;
				param[3].ParamName = "@noteIsDeleted";
				param[3].DataType = DbType.Boolean;
				if (String.IsNullOrEmpty(_noteIsDeleted.ToString()))
					param[3].value = DBNull.Value;
				else
					param[3].value = _noteIsDeleted;

				param[4].direction = ParameterDirection.Input;
				param[4].ParamName = "@noteIsActive";
				param[4].DataType = DbType.Boolean;
				if (String.IsNullOrEmpty(_noteIsActive.ToString()))
					param[4].value = DBNull.Value;
				else
					param[4].value = _noteIsActive;

				param[5].direction = ParameterDirection.Input;
				param[5].ParamName = "@noteCompanyId";
				param[5].DataType = DbType.Int32;
					if (_noteCompanyId == 0)
					{ }		//param[8].value = DBNull.Value;
					else
					param[5].value = _noteCompanyId;

                    param[6].direction = ParameterDirection.Input;
                    param[6].ParamName = "@noteUserId";
                    param[6].DataType = DbType.Int32;
                    if (_noteCompanyId == 0)
                    { }		//param[8].value = DBNull.Value;
                    else
                        param[6].value = _noteUserId;



				//obj.ExecScalar("InsertNote", CommandType.StoredProcedure, param);				_returnBoolean = true;
                    _ds = DBAccess.ExecDataSet("InsertNote", CommandType.StoredProcedure, param);
                    _returnBoolean = true;
			}
			catch (DataException ex) 
			{
				_returnBoolean = false;
				throw ex;
			}
		}


		/// <summary>
		/// Inserts Records in the Note table.
		/// </summary>
        public void GetAllNote()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[4];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@noteIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _noteIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@noteIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _noteIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@noteCompanyId";
                param[2].DataType = DbType.Int32;
                param[2].value = _noteCompanyId;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@noteUserId";
                param[3].DataType = DbType.Int32;
                if (_noteCompanyId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[3].value = _noteUserId;


                _ds = DBAccess.ExecDataSet("GetAllNote", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllNote" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void NoteStatusUpdate()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] pm = new ParamStruct[3];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@noteId";
                pm[0].DataType = DbType.Int32;
                pm[0].value = _noteId;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@noteIsActive";
                pm[1].DataType = DbType.Boolean;
                pm[1].value = _noteIsActive;

                pm[2].direction = ParameterDirection.Input;
                pm[2].ParamName = "@noteIsDeleted";
                pm[2].DataType = DbType.Boolean;
                pm[2].value = _noteIsDeleted;

                _ds = DBAccess.ExecDataSet("NoteStatusUpdate", CommandType.StoredProcedure, pm);
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in NoteStatusUpdate  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            }
        }

		#endregion

	}
}
