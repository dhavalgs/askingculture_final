﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starteku.Data.Manager;
using System.Data;

namespace starteku_BusinessLogic
{
    public class JobTypeBM
    {
        #region Private Declaration

        private DataSet _ds;
        private string _returnString;
        private bool _returnBoolean;
        private Int32 _jobId;
        private String _jobName;
        private String _jobNameDN;
        private DateTime _jobCreatedDate;
        private DateTime _jobUpdatedDate;
        private Boolean _jobIsDeleted;
        private Boolean _jobIsActive;
        private Int32 _jobCompanyId;
        private String _jobCreateBy;
        private Int32 _jobDepId;
        private string _jobDescription;
        private string _jobcatid;

        private String _TeamIDs;
        
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public DataSet ds
        {
            get { return _ds; }
            set { _ds = value; }
        }
        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public string ReturnString
        {
            get { return _returnString; }
            set { _returnString = value; }
        }

        /// <summary>
        /// Gets or sets the ReturnBoolean value.
        /// </summary>
        public bool ReturnBoolean
        {
            get { return _returnBoolean; }
            set { _returnBoolean = value; }
        }

        /// <summary>
        /// Gets or sets the jobId value.
        /// </summary>
        public Int32 jobId
        {
            get { return _jobId; }
            set { _jobId = value; }
        }

        /// <summary>
        /// Gets or sets the jobName value.
        /// </summary>
        public String jobDescription
        {
            get { return _jobDescription; }
            set { _jobDescription = value; }
        }
        public String jobName
        {
            get { return _jobName; }
            set { _jobName = value; }
        }
        public String jobNameDN
        {
            get { return _jobNameDN; }
            set { _jobNameDN = value; }
        }
        /// <summary>
        /// Gets or sets the jobCreatedDate value.
        /// </summary>
        public DateTime jobCreatedDate
        {
            get { return _jobCreatedDate; }
            set { _jobCreatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the jobUpdatedDate value.
        /// </summary>
        public DateTime jobUpdatedDate
        {
            get { return _jobUpdatedDate; }
            set { _jobUpdatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the jobIsDeleted value.
        /// </summary>
        public Boolean jobIsDeleted
        {
            get { return _jobIsDeleted; }
            set { _jobIsDeleted = value; }
        }

        /// <summary>
        /// Gets or sets the jobIsActive value.
        /// </summary>
        public Boolean jobIsActive
        {
            get { return _jobIsActive; }
            set { _jobIsActive = value; }
        }

        /// <summary>
        /// Gets or sets the jobCompanyId value.
        /// </summary>
        public Int32 jobCompanyId
        {
            get { return _jobCompanyId; }
            set { _jobCompanyId = value; }
        }

        /// <summary>
        /// Gets or sets the jobCreateBy value.
        /// </summary>
        public String jobCreateBy
        {
            get { return _jobCreateBy; }
            set { _jobCreateBy = value; }
        }

        /// <summary>
        /// Gets or sets the jobDepId value.
        /// </summary>
        public Int32 jobDepId
        {
            get { return _jobDepId; }
            set { _jobDepId = value; }
        }
        public String jobcatid
        {
            get { return _jobcatid; }
            set { _jobcatid = value; }
        }


        public String TeamIDs
        {
            get { return _TeamIDs; }
            set { _TeamIDs = value; }
        }

        #endregion

        #region Methods
      
        public void InsertJobType()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();


                ParamStruct[] param = new ParamStruct[11];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = Convert.ToDateTime("1/1/9999 11:59:59 PM");

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@jobName";
                param[0].DataType = DbType.String;
                if (String.IsNullOrEmpty(_jobName))
                    param[0].value = DBNull.Value;
                else
                    param[0].value = _jobName;


                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@jobNameDN";
                param[1].DataType = DbType.String;
                if (String.IsNullOrEmpty(_jobNameDN))
                    param[1].value = DBNull.Value;
                else
                    param[1].value = _jobNameDN;


                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@jobCreatedDate";
                param[2].DataType = DbType.DateTime;
                if (_jobCreatedDate.Ticks > dtmin.Ticks && _jobCreatedDate.Ticks < dtmax.Ticks)
                    param[2].value = _jobCreatedDate;
                else if (_jobCreatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[2].value = _jobCreatedDate;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@jobUpdatedDate";
                param[3].DataType = DbType.DateTime;
                if (_jobUpdatedDate.Ticks > dtmin.Ticks && _jobUpdatedDate.Ticks < dtmax.Ticks)
                    param[3].value = _jobUpdatedDate;
                else if (_jobUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[3].value = _jobUpdatedDate;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@jobIsDeleted";
                param[4].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_jobIsDeleted.ToString()))
                    param[4].value = DBNull.Value;
                else
                    param[4].value = _jobIsDeleted;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@jobIsActive";
                param[5].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_jobIsActive.ToString()))
                    param[5].value = DBNull.Value;
                else
                    param[5].value = _jobIsActive;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@jobCompanyId";
                param[6].DataType = DbType.Int32;                
                param[6].value = _jobCompanyId;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@jobCreateBy";
                param[7].DataType = DbType.String;
                if (String.IsNullOrEmpty(_jobCreateBy))
                    param[7].value = DBNull.Value;
                else
                    param[7].value = _jobCreateBy;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@jobDepId";
                param[8].DataType = DbType.Int32;                
                param[8].value = _jobDepId;

                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@jobDescription";
                param[9].DataType = DbType.String;
                if (String.IsNullOrEmpty(_jobDescription))
                    param[9].value = DBNull.Value;
                else
                    param[9].value = _jobDescription;

                param[10].direction = ParameterDirection.Input;
                param[10].ParamName = "@jobcatid";
                param[10].DataType = DbType.String;
                if (String.IsNullOrEmpty(_jobcatid))
                    param[10].value = DBNull.Value;
                else
                    param[10].value = _jobcatid;

                //obj.ExecScalar("InsertJobType", CommandType.StoredProcedure, param); _returnBoolean = true;

                _ds = DBAccess.ExecDataSet("InsertJobType", CommandType.StoredProcedure, param);
                _returnBoolean = true;

            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        public void InsertTeam()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();


                ParamStruct[] param = new ParamStruct[12];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = Convert.ToDateTime("1/1/9999 11:59:59 PM");

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@TeamID";
                param[0].DataType = DbType.Int32;
                param[0].value = _jobId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@TeamName";
                param[1].DataType = DbType.String;
                if (String.IsNullOrEmpty(_jobName))
                    param[1].value = DBNull.Value;
                else
                    param[1].value = _jobName;


                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@TeamNameDN";
                param[2].DataType = DbType.String;
                if (String.IsNullOrEmpty(_jobNameDN))
                    param[2].value = DBNull.Value;
                else
                    param[2].value = _jobNameDN;


                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@TeamCreateDate";
                param[3].DataType = DbType.DateTime;
                if (_jobCreatedDate.Ticks > dtmin.Ticks && _jobCreatedDate.Ticks < dtmax.Ticks)
                    param[3].value = _jobCreatedDate;
                else if (_jobCreatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[3].value = _jobCreatedDate;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@TeamUpdateDate";
                param[4].DataType = DbType.DateTime;
                if (_jobUpdatedDate.Ticks > dtmin.Ticks && _jobUpdatedDate.Ticks < dtmax.Ticks)
                    param[4].value = _jobUpdatedDate;
                else if (_jobUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[4].value = _jobUpdatedDate;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@TeamIsDeleted";
                param[5].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_jobIsDeleted.ToString()))
                    param[5].value = DBNull.Value;
                else
                    param[5].value = _jobIsDeleted;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@TeamIsActive";
                param[6].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_jobIsActive.ToString()))
                    param[6].value = DBNull.Value;
                else
                    param[6].value = _jobIsActive;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@TeamCompanyID";
                param[7].DataType = DbType.Int32;
                param[7].value = _jobCompanyId;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@TeamCreatedBy";
                param[8].DataType = DbType.String;
                if (String.IsNullOrEmpty(_jobCreateBy))
                    param[8].value = DBNull.Value;
                else
                    param[8].value = _jobCreateBy;

                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@TeamDepID";
                param[9].DataType = DbType.Int32;
                param[9].value = _jobDepId;

                param[10].direction = ParameterDirection.Input;
                param[10].ParamName = "@TeamDescription";
                param[10].DataType = DbType.String;
                if (String.IsNullOrEmpty(_jobDescription))
                    param[10].value = DBNull.Value;
                else
                    param[10].value = _jobDescription;

                param[11].direction = ParameterDirection.Input;
                param[11].ParamName = "@TeamCatID";
                param[11].DataType = DbType.String;
                if (String.IsNullOrEmpty(_jobcatid))
                    param[11].value = DBNull.Value;
                else
                    param[11].value = _jobcatid;

                //obj.ExecScalar("InsertJobType", CommandType.StoredProcedure, param); _returnBoolean = true;

                _ds = DBAccess.ExecDataSet("InsertUpdateTeam", CommandType.StoredProcedure, param);
                _returnBoolean = true;

            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        public void UpdateJobType()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[7];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = Convert.ToDateTime("1/1/9999 11:59:59 PM");
               
                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@jobId";
                param[0].DataType = DbType.Int32;
                param[0].value = _jobId;
               
                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@jobName";
                param[1].DataType = DbType.String;
                if (String.IsNullOrEmpty(_jobName))
                    param[1].value = DBNull.Value;
                else
                    param[1].value = _jobName;
               
               
                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@jobUpdatedDate";
                param[2].DataType = DbType.DateTime;
                if (_jobUpdatedDate.Ticks > dtmin.Ticks && _jobUpdatedDate.Ticks < dtmax.Ticks)
                    param[2].value = _jobUpdatedDate;
                else if (_jobUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[2].value = _jobUpdatedDate;
               
                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@jobDescription";
                param[3].DataType = DbType.String;
                if (String.IsNullOrEmpty(_jobDescription))
                    param[3].value = DBNull.Value;
                else
                    param[3].value = _jobDescription;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@jobDepId";
                param[4].DataType = DbType.Int32;
                param[4].value = _jobDepId;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@jobcatid";
                param[5].DataType = DbType.String;
                if (String.IsNullOrEmpty(_jobcatid))
                    param[5].value = DBNull.Value;
                else
                    param[5].value = _jobcatid;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@jobNameDN";
                param[6].DataType = DbType.String;
                if (String.IsNullOrEmpty(_jobNameDN))
                    param[6].value = DBNull.Value;
                else
                    param[6].value = _jobNameDN;

                _ds = DBAccess.ExecDataSet("UpdateJobType", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        public void GetAllJobType()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@jobIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _jobIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@jobIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _jobIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@jobCompanyId";
                param[2].DataType = DbType.Int32;
                param[2].value = _jobCompanyId;

                _ds = DBAccess.ExecDataSet("GetAllJobType", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllJobType" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        public void GetAllTeam()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[4];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@TeamIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _jobIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@TeamIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _jobIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@TeamCompanyId";
                param[2].DataType = DbType.Int32;
                param[2].value = _jobCompanyId;


                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@TeamIDs";
                param[3].DataType = DbType.String;
                param[3].value = _TeamIDs;

                _ds = DBAccess.ExecDataSet("GetAllTeam", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllTeam" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        public void JobTypeStatusUpdate()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] pm = new ParamStruct[3];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@jobId";
                pm[0].DataType = DbType.Int32;
                pm[0].value = _jobId;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@jobIsActive";
                pm[1].DataType = DbType.Boolean;
                pm[1].value = _jobIsActive;

                pm[2].direction = ParameterDirection.Input;
                pm[2].ParamName = "@jobIsDeleted";
                pm[2].DataType = DbType.Boolean;
                pm[2].value = _jobIsDeleted;

                _ds = DBAccess.ExecDataSet("JobTypeStatusUpdate", CommandType.StoredProcedure, pm);
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in JobTypeStatusUpdate  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            }
        }
        public void TeamStatusUpdate()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] pm = new ParamStruct[3];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@TeamID";
                pm[0].DataType = DbType.Int32;
                pm[0].value = _jobId;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@TeamIsActive";
                pm[1].DataType = DbType.Boolean;
                pm[1].value = _jobIsActive;

                pm[2].direction = ParameterDirection.Input;
                pm[2].ParamName = "@TeamIsDeleted";
                pm[2].DataType = DbType.Boolean;
                pm[2].value = _jobIsDeleted;

                _ds = DBAccess.ExecDataSet("TeamStatusUpdate", CommandType.StoredProcedure, pm);
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in TeamStatusUpdate  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            }
        }
        public void GetAllJobTypebyid()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@jobId";
                param[0].DataType = DbType.Int32;
                param[0].value = _jobId;


                _ds = DBAccess.ExecDataSet("GetAllJobTypebyid", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllJobTypebyid" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        public void GetAllTeambyid()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@TeamID";
                param[0].DataType = DbType.Int32;
                param[0].value = _jobId;


                _ds = DBAccess.ExecDataSet("GetAllTeambyid", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllTeambyid" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        public void GetAllJobTypeByCompanyId()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@jobCompanyId";
                param[0].DataType = DbType.Int32;
                param[0].value = _jobCompanyId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@jobIsActive";
                param[1].DataType = DbType.Boolean;
                param[1].value = _jobIsActive;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@jobIsDeleted";
                param[2].DataType = DbType.Boolean;
                param[2].value = _jobIsDeleted;


                _ds = DBAccess.ExecDataSet("GetAllJobTypeByCompanyId", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllDivisionsbyid" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        public void JobTypeCheckDuplication(string name, Int32 id, Int32 jobCompanyId)
        {
            try
            {
                DataAccess obj = new DataAccess();
                obj.Provider = EnumProviders.SQLClient;
                obj.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["connstring"].ToString();

                ParamStruct[] param = new ParamStruct[3];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = Convert.ToDateTime("1/1/9999 11:59:59 PM");

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@jobName";
                param[0].DataType = DbType.String;
                param[0].value = name;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@jobId";
                param[1].DataType = DbType.Int32;
                param[1].value = id;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@jobCompanyId";
                param[2].DataType = DbType.Int32;
                param[2].value = jobCompanyId;

                _ds = obj.ExecDataSet("JobTypeCheckDuplication", CommandType.StoredProcedure, param);
                if (_ds.Tables[0].Rows.Count > 0)
                {
                    _returnString = (_ds.Tables[0].Rows[0]["returnStatement"].ToString());

                }

            }
            catch (DataException ex)
            {
                throw ex;
            }


        }
        #endregion
    }
}
