﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starteku.Data.Manager;
using System.Data;

namespace starteku_BusinessLogic
{
    public class TargetMaxLongBM
    {
        #region Private Declaration
            private DataSet _ds;
            private string _returnString;
            private bool _returnBoolean;
            private Int32 _RefID;
            private String _tarType;
            private String _ComptenceCatIDs;
            private Int32 _CompanyID;
            private Int32 _UserID;
        #endregion

            #region Properties

            /// <summary>
            /// Gets or sets the ReturnString value.
            /// </summary>
            public DataSet ds
            {
                get { return _ds; }
                set { _ds = value; }
            }
            /// <summary>
            /// Gets or sets the ReturnString value.
            /// </summary>
            public string ReturnString
            {
                get { return _returnString; }
                set { _returnString = value; }
            }

            /// <summary>
            /// Gets or sets the ReturnBoolean value.
            /// </summary>
            public bool ReturnBoolean
            {
                get { return _returnBoolean; }
                set { _returnBoolean = value; }
            }

            /// <summary>
            /// Gets or sets the jobId value.
            /// </summary>
            public Int32 RefID
            {
                get { return _RefID; }
                set { _RefID = value; }
            }
            public Int32 CompanyID
            {
                get { return _CompanyID; }
                set { _CompanyID = value; }
            }
            public Int32 UserID
            {
                get { return _UserID; }
                set { _UserID = value; }
            }
            public String tarType
            {
                get { return _tarType; }
                set { _tarType = value; }
            }
            public String ComptenceCatIDs
            {
                get { return _ComptenceCatIDs; }
                set { _ComptenceCatIDs = value; }
            }
            #endregion

        #region Method
            public void InsertUpdateTargetMaxLong()
            {
                try
                {
                    DataAccess DBAccess = new DataAccess();
                    DBAccess.Provider = EnumProviders.SQLClient;
                    DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();


                    ParamStruct[] param = new ParamStruct[5];

                    param[0].direction = ParameterDirection.Input;
                    param[0].ParamName = "@tarType";
                    param[0].DataType = DbType.String;
                    param[0].value = _tarType;

                    param[1].direction = ParameterDirection.Input;
                    param[1].ParamName = "@RefID";
                    param[1].DataType = DbType.Int32;
                    param[1].value = _RefID;


                    param[2].direction = ParameterDirection.Input;
                    param[2].ParamName = "@CompanyID";
                    param[2].DataType = DbType.Int32;
                    param[2].value = _CompanyID;


                    param[3].direction = ParameterDirection.Input;
                    param[3].ParamName = "@UserID";
                    param[3].DataType = DbType.Int32;
                    param[3].value = _UserID;

                    param[4].direction = ParameterDirection.Input;
                    param[4].ParamName = "@CompetenceCat";
                    param[4].DataType = DbType.String;
                    param[4].value = _ComptenceCatIDs;

                    _ds = DBAccess.ExecDataSet("InsertUpdateTargetLongMaxLevel", CommandType.StoredProcedure, param);
                    _returnBoolean = true;

                }
                catch (DataException ex)
                {
                    _returnBoolean = false;
                    throw ex;
                }
            }
            public void GetAllTargetMaxLong()
            {
                try
                {
                    DataAccess DBAccess = new DataAccess();
                    DBAccess.Provider = EnumProviders.SQLClient;
                    DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();


                    ParamStruct[] param = new ParamStruct[4];

                    param[0].direction = ParameterDirection.Input;
                    param[0].ParamName = "@tarType";
                    param[0].DataType = DbType.String;
                    param[0].value = _tarType;

                    param[1].direction = ParameterDirection.Input;
                    param[1].ParamName = "@RefID";
                    param[1].DataType = DbType.Int32;
                    param[1].value = _RefID;


                    param[2].direction = ParameterDirection.Input;
                    param[2].ParamName = "@CompanyID";
                    param[2].DataType = DbType.Int32;
                    param[2].value = _CompanyID;


                    param[3].direction = ParameterDirection.Input;
                    param[3].ParamName = "@UserID";
                    param[3].DataType = DbType.Int32;
                    param[3].value = _UserID;


                    _ds = DBAccess.ExecDataSet("GetAllTargetLongMaxLevel", CommandType.StoredProcedure, param);

                }
                catch (DataException ex)
                {
                    _returnBoolean = false;
                    throw ex;
                }
            }
        #endregion

    }
}
