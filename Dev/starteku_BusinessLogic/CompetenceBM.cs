﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starteku.Data.Manager;
using System.Data;
using System.Web;


namespace starteku_BusinessLogic
{
    public class CompetenceBM
    {
        #region Private Declaration

        private DataSet _ds;
        private string _returnString;
        private bool _returnBoolean;
        private Int32 _comId;
        private Int32 _comJobId;
        private Int32 _comLevel;
        private String _comCompetence;
        private DateTime _comCreatedDate;
        private DateTime _comUpdatedDate;
        private Boolean _comIsDeleted;
        private Boolean _comIsActive;
        private Int32 _comCompanyId;
        private String _comCreateBy;
        private Int32 _comDepId;
        private String _comQuestion;
        private Int32 _comQuestionNo;
        private String _comDepartmentId;
        private String _comIndId;
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public DataSet ds
        {
            get { return _ds; }
            set { _ds = value; }
        }
        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public string ReturnString
        {
            get { return _returnString; }
            set { _returnString = value; }
        }

        /// <summary>
        /// Gets or sets the ReturnBoolean value.
        /// </summary>
        public bool ReturnBoolean
        {
            get { return _returnBoolean; }
            set { _returnBoolean = value; }
        }

        /// <summary>
        /// Gets or sets the comId value.
        /// </summary>
        public Int32 comId
        {
            get { return _comId; }
            set { _comId = value; }
        }

        /// <summary>
        /// Gets or sets the comJobId value.
        /// </summary>
        public Int32 comJobId
        {
            get { return _comJobId; }
            set { _comJobId = value; }
        }

        /// <summary>
        /// Gets or sets the comLevel value.
        /// </summary>
        public Int32 comLevel
        {
            get { return _comLevel; }
            set { _comLevel = value; }
        }

        /// <summary>
        /// Gets or sets the comCompetence value.
        /// </summary>
        public String comCompetence
        {
            get { return _comCompetence; }
            set { _comCompetence = value; }
        }

        /// <summary>
        /// Gets or sets the comCreatedDate value.
        /// </summary>
        public DateTime comCreatedDate
        {
            get { return _comCreatedDate; }
            set { _comCreatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the comUpdatedDate value.
        /// </summary>
        public DateTime comUpdatedDate
        {
            get { return _comUpdatedDate; }
            set { _comUpdatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the comIsDeleted value.
        /// </summary>
        public Boolean comIsDeleted
        {
            get { return _comIsDeleted; }
            set { _comIsDeleted = value; }
        }

        /// <summary>
        /// Gets or sets the comIsActive value.
        /// </summary>
        public Boolean comIsActive
        {
            get { return _comIsActive; }
            set { _comIsActive = value; }
        }

        /// <summary>
        /// Gets or sets the comCompanyId value.
        /// </summary>
        public Int32 comCompanyId
        {
            get { return _comCompanyId; }
            set { _comCompanyId = value; }
        }

        /// <summary>
        /// Gets or sets the comCreateBy value.
        /// </summary>
        public String comCreateBy
        {
            get { return _comCreateBy; }
            set { _comCreateBy = value; }
        }

        /// <summary>
        /// Gets or sets the comDepId value.
        /// </summary>
        public Int32 comDepId
        {
            get { return _comDepId; }
            set { _comDepId = value; }
        }

        public String comQuestion
        {
            get { return _comQuestion; }
            set { _comQuestion = value; }
        }

        /// <summary>
        /// Gets or sets the comDepId value.
        /// </summary>
        public Int32 comQuestionNo
        {
            get { return _comQuestionNo; }
            set { _comQuestionNo = value; }
        }

        public String comDepartmentId
        {
            get { return _comDepartmentId; }
            set { _comDepartmentId = value; }
        }
        public String comIndId
        {
            get { return _comIndId; }
            set { _comIndId = value; }
        }
        #endregion

        #region Methods Organization & Employer

        public int InsertCompetence()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[14];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue;

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@comJobId";
                param[0].DataType = DbType.Int32;
                if (_comJobId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[0].value = _comJobId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@comLevel";
                param[1].DataType = DbType.Int32;
                param[1].value = _comLevel;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@comCompetence";
                param[2].DataType = DbType.String;
                if (String.IsNullOrEmpty(_comCompetence))
                    param[2].value = DBNull.Value;
                else
                    param[2].value = _comCompetence;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@comCreatedDate";
                param[3].DataType = DbType.DateTime;
                if (_comCreatedDate.Ticks > dtmin.Ticks && _comCreatedDate.Ticks < dtmax.Ticks)
                    param[3].value = _comCreatedDate;
                else if (_comCreatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[3].value = _comCreatedDate;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@comUpdatedDate";
                param[4].DataType = DbType.DateTime;
                if (_comUpdatedDate.Ticks > dtmin.Ticks && _comUpdatedDate.Ticks < dtmax.Ticks)
                    param[4].value = _comUpdatedDate;
                else if (_comUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[4].value = _comUpdatedDate;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@comIsDeleted";
                param[5].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_comIsDeleted.ToString()))
                    param[5].value = DBNull.Value;
                else
                    param[5].value = _comIsDeleted;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@comIsActive";
                param[6].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_comIsActive.ToString()))
                    param[6].value = DBNull.Value;
                else
                    param[6].value = _comIsActive;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@comCompanyId";
                param[7].DataType = DbType.Int32;
                param[7].value = _comCompanyId;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@comCreateBy";
                param[8].DataType = DbType.String;
                if (String.IsNullOrEmpty(_comCreateBy))
                    param[8].value = DBNull.Value;
                else
                    param[8].value = _comCreateBy;

                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@comDepId";
                param[9].DataType = DbType.Int32;
                param[9].value = _comDepId;

                param[10].direction = ParameterDirection.Input;
                param[10].ParamName = "@comQuestion";
                param[10].DataType = DbType.String;
                if (String.IsNullOrEmpty(_comQuestion))
                    param[10].value = DBNull.Value;
                else
                    param[10].value = _comQuestion;

                param[11].direction = ParameterDirection.Input;
                param[11].ParamName = "@comQuestionNo";
                param[11].DataType = DbType.Int32;
                param[11].value = _comQuestionNo;

                param[12].direction = ParameterDirection.Input;
                param[12].ParamName = "@comDepartmentId";
                param[12].DataType = DbType.String;
                if (String.IsNullOrEmpty(_comDepartmentId))
                    param[12].value = DBNull.Value;
                else
                    param[12].value = _comDepartmentId;


                param[13].direction = ParameterDirection.Input;
                param[13].ParamName = "@comIndId";
                param[13].DataType = DbType.String;
                if (String.IsNullOrEmpty(_comIndId))
                    param[13].value = DBNull.Value;
                else
                    param[13].value = _comIndId;

                _ds = DBAccess.ExecDataSet("InsertCompetence", CommandType.StoredProcedure, param);
                return Convert.ToInt32(_ds.Tables[0].Rows[0][0]);
            }
            catch (DataException ex)
            {
                return 0;
            }
        }
        public void UpdateCompetence()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[9];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue;

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@comId";
                param[0].DataType = DbType.Int32;
                param[0].value = _comId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@comJobId";
                param[1].DataType = DbType.Int32;
                if (_comJobId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[1].value = _comJobId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@comLevel";
                param[2].DataType = DbType.Int32;
                if (_comLevel == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[2].value = _comLevel;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@comCompetence";
                param[3].DataType = DbType.String;
                if (String.IsNullOrEmpty(_comCompetence))
                    param[3].value = DBNull.Value;
                else
                    param[3].value = _comCompetence;



                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@comUpdatedDate";
                param[4].DataType = DbType.DateTime;
                if (_comUpdatedDate.Ticks > dtmin.Ticks && _comUpdatedDate.Ticks < dtmax.Ticks)
                    param[4].value = _comUpdatedDate;
                else if (_comUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[4].value = _comUpdatedDate;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@comQuestion";
                param[5].DataType = DbType.String;
                if (String.IsNullOrEmpty(_comQuestion))
                    param[5].value = DBNull.Value;
                else
                    param[5].value = _comQuestion;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@comQuestionNo";
                param[6].DataType = DbType.Int32;
                param[6].value = _comQuestionNo;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@comDepartmentId";
                param[7].DataType = DbType.String;
                if (String.IsNullOrEmpty(_comDepartmentId))
                    param[7].value = DBNull.Value;
                else
                    param[7].value = _comDepartmentId;


                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@comIndId";
                param[8].DataType = DbType.String;
                if (String.IsNullOrEmpty(_comIndId))
                    param[8].value = DBNull.Value;
                else
                    param[8].value = _comIndId;


                _ds = DBAccess.ExecDataSet("UpdateCompetence", CommandType.StoredProcedure, param);
                _returnBoolean = true;
                // obj.ExecScalar("UpdateCompetence", CommandType.StoredProcedure, param); _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }
        public void GetAllCompetence()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@comIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _comIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@comIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _comIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@comCompanyId";
                param[2].DataType = DbType.Int32;
                param[2].value = _comCompanyId;

                _ds = DBAccess.ExecDataSet("GetAllCompetence", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllState" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        public void GetAllCompetencebyid()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@comId";
                param[0].DataType = DbType.Int32;
                param[0].value = _comId;


                _ds = DBAccess.ExecDataSet("GetAllCompetencebyid", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllCompetencebyid" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void JobTypeCompetenceUpdate()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] pm = new ParamStruct[3];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@comId";
                pm[0].DataType = DbType.Int32;
                pm[0].value = _comId;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@comIsActive";
                pm[1].DataType = DbType.Boolean;
                pm[1].value = _comIsActive;

                pm[2].direction = ParameterDirection.Input;
                pm[2].ParamName = "@comIsDeleted";
                pm[2].DataType = DbType.Boolean;
                pm[2].value = _comIsDeleted;

                _ds = DBAccess.ExecDataSet("JobTypeCompetenceUpdate", CommandType.StoredProcedure, pm);
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in JobTypeCompetenceUpdate  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            }
        }
        public void CompetenceCheckDuplication(string name, Int32 id)
        {
            try
            {
                DataAccess obj = new DataAccess();
                obj.Provider = EnumProviders.SQLClient;
                obj.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["connstring"].ToString();

                ParamStruct[] param = new ParamStruct[2];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue;

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@comName";
                param[0].DataType = DbType.String;
                param[0].value = name;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@comId";
                param[1].DataType = DbType.Int32;
                param[1].value = id;

                _ds = obj.ExecDataSet("CompetenceCheckDuplication", CommandType.StoredProcedure, param);
                if (_ds.Tables[0].Rows.Count > 0)
                {
                    _returnString = (_ds.Tables[0].Rows[0]["returnStatement"].ToString());

                }

            }
            catch (DataException ex)
            {
                throw ex;
            }


        }
        public Boolean InsertCompetenceComment(Int32 compId, Int32 comuserid, string compcomment, Int32 comtouserid)
        {
            try
            {
                DataAccess obj = new DataAccess();
                obj.Provider = EnumProviders.SQLClient;
                obj.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["connstring"].ToString();

                ParamStruct[] param = new ParamStruct[5];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue;

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@compId";
                param[0].DataType = DbType.Int32;
                param[0].value = compId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@comtouserid";
                param[1].DataType = DbType.Int32;
                param[1].value = comtouserid;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@comuserid";
                param[2].DataType = DbType.Int32;
                param[2].value = comuserid;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@compcomment";
                param[3].DataType = DbType.String;
                param[3].value = compcomment;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@comcreatedate";
                param[4].DataType = DbType.DateTime;
                param[4].value = DateTime.Now;

                _ds = obj.ExecDataSet("InsertCompetenceComment", CommandType.StoredProcedure, param);

                return true;
            }
            catch (DataException ex)
            {
                return false;
                throw ex;

            }


        }

        public void GetCompetenceCommentById(Int32 compId, Int32 compuserid)
        {
            //var cacheKey = string.Format("GetCompetenceCommentById-{0}-{1}-{2}-{3}-{4}", compId, compuserid, 1, 2, 3);
            //var cacheItem = HttpRuntime.Cache.Get(cacheKey) as DataSet;
            try
            {
                //if (cacheItem == null)
                //{
                    DataAccess obj = new DataAccess();
                    obj.Provider = EnumProviders.SQLClient;
                    obj.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["connstring"].ToString();

                    ParamStruct[] param = new ParamStruct[2];
                    DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                    DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue;

                    param[0].direction = ParameterDirection.Input;
                    param[0].ParamName = "@compId";
                    param[0].DataType = DbType.Int32;
                    param[0].value = compId;

                    param[1].direction = ParameterDirection.Input;
                    param[1].ParamName = "@compuserid";
                    param[1].DataType = DbType.Int32;
                    param[1].value = compuserid;



                    //cacheItem = obj.ExecDataSet("GetCompetenceCommentById", CommandType.StoredProcedure, param);
                    //HttpRuntime.Cache.Insert(cacheKey, cacheItem, null, DateTime.Now.AddDays(Convert.ToInt32(CommonUtilities.GetConfigValue("CacheDuration", "2"))), TimeSpan.Zero);
                //}

               // _ds = cacheItem;
                    _ds = obj.ExecDataSet("GetCompetenceCommentById", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                throw ex;
            }


        }
        #endregion

      
    }
}
