﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starteku.Data.Manager;
using System.Data;

namespace starteku_BusinessLogic
{
  public  class CityBM
    {

        #region Private Declaration

        private DataSet _ds;
        private string _returnString;
        private bool _returnBoolean;
        private Int32 _citId;
        private String _citName;
        private Int32 _citStateId;
        private DateTime _citCreatedDate;
        private DateTime _citUpdatedDate;
        private Boolean _citIsDeleted;
        private Boolean _citIsActive;
        private Int32 _citCompanyId;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public DataSet ds
        {
            get { return _ds; }
            set { _ds = value; }
        }
        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public string ReturnString
        {
            get { return _returnString; }
            set { _returnString = value; }
        }

        /// <summary>
        /// Gets or sets the ReturnBoolean value.
        /// </summary>
        public bool ReturnBoolean
        {
            get { return _returnBoolean; }
            set { _returnBoolean = value; }
        }

        /// <summary>
        /// Gets or sets the citId value.
        /// </summary>
        public Int32 citId
        {
            get { return _citId; }
            set { _citId = value; }
        }

        /// <summary>
        /// Gets or sets the citName value.
        /// </summary>
        public String citName
        {
            get { return _citName; }
            set { _citName = value; }
        }

        /// <summary>
        /// Gets or sets the citStateId value.
        /// </summary>
        public Int32 citStateId
        {
            get { return _citStateId; }
            set { _citStateId = value; }
        }

        /// <summary>
        /// Gets or sets the citCreatedDate value.
        /// </summary>
        public DateTime citCreatedDate
        {
            get { return _citCreatedDate; }
            set { _citCreatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the citUpdatedDate value.
        /// </summary>
        public DateTime citUpdatedDate
        {
            get { return _citUpdatedDate; }
            set { _citUpdatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the staIsDeleted value.
        /// </summary>
        public Boolean citIsDeleted
        {
            get { return _citIsDeleted; }
            set { _citIsDeleted = value; }
        }

        /// <summary>
        /// Gets or sets the staIsActive value.
        /// </summary>
        public Boolean citIsActive
        {
            get { return _citIsActive; }
            set { _citIsActive = value; }
        }

        /// <summary>
        /// Gets or sets the staCompanyId value.
        /// </summary>
        public Int32 citCompanyId
        {
            get { return _citCompanyId; }
            set { _citCompanyId = value; }
        }

        #endregion

        #region Method
        public void InsertCity()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[7];

                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = Convert.ToDateTime("12/31/9999 11:59:59 PM");

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@citName";
                param[0].DataType = DbType.String;
                if (String.IsNullOrEmpty(_citName))
                    param[0].value = DBNull.Value;
                else
                    param[0].value = _citName;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@citStateId";
                param[1].DataType = DbType.Int32;
                if (_citStateId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[1].value = _citStateId;
                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@citCreatedDate";
                param[2].DataType = DbType.DateTime;
                if (_citCreatedDate.Ticks > dtmin.Ticks && _citCreatedDate.Ticks < dtmax.Ticks)
                    param[2].value = _citCreatedDate;

                else if (_citCreatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[2].value = _citCreatedDate;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@citUpdatedDate";
                param[3].DataType = DbType.DateTime;
                if (_citUpdatedDate.Ticks > dtmin.Ticks && _citUpdatedDate.Ticks < dtmax.Ticks)
                    param[3].value = _citUpdatedDate;

                else if (_citUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[3].value = _citUpdatedDate;
                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@citIsDeleted";
                param[4].DataType = DbType.Boolean;

                if (String.IsNullOrEmpty(_citIsDeleted.ToString()))
                    param[4].value = DBNull.Value;
                else
                    param[4].value = _citIsDeleted;
                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@citIsActive";
                param[5].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_citIsActive.ToString()))
                    param[5].value = DBNull.Value;
                else
                    param[5].value = _citIsActive;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@citCompanyId";
                param[6].DataType = DbType.Int32;                
                param[6].value = _citCompanyId;
             
                _ds = DBAccess.ExecDataSet("InsertCity", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        public void UpdateCity()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[4];


                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = Convert.ToDateTime("12/31/9999 11:59:59 PM");
                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@citId";
                param[0].DataType = DbType.Int32;
                param[0].value = _citId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@citName";
                param[1].DataType = DbType.String;
                if (String.IsNullOrEmpty(_citName))
                    param[1].value = DBNull.Value;
                else
                    param[1].value = _citName;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@citStateId";
                param[2].DataType = DbType.Int32;
                if (_citStateId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[2].value = _citStateId;

               
                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@citUpdatedDate";
                param[3].DataType = DbType.DateTime;
                if (_citUpdatedDate.Ticks > dtmin.Ticks && _citUpdatedDate.Ticks < dtmax.Ticks)
                    param[3].value = _citUpdatedDate;
                else if (_citUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[3].value = _citUpdatedDate;


                _ds = DBAccess.ExecDataSet("UpdateCity", CommandType.StoredProcedure, param);
                _returnBoolean = true;
               
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        public void GetAllcity()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@citIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _citIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@citIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _citIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@citCompanyId";
                param[2].DataType = DbType.Int32;
                param[2].value = _citCompanyId;

                _ds = DBAccess.ExecDataSet("GetAllcity", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllcity" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void CityStatusUpdate()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] pm = new ParamStruct[3];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@citId";
                pm[0].DataType = DbType.Int32;
                pm[0].value = _citId;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@citIsActive";
                pm[1].DataType = DbType.Boolean;
                pm[1].value = _citIsActive;

                pm[2].direction = ParameterDirection.Input;
                pm[2].ParamName = "@citIsDeleted";
                pm[2].DataType = DbType.Boolean;
                pm[2].value = _citIsDeleted;

                _ds = DBAccess.ExecDataSet("CityStatusUpdate", CommandType.StoredProcedure, pm);
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in CityStatusUpdate  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            }
        }

        public void GetAllcitybyid()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@citId";
                param[0].DataType = DbType.Int32;
                param[0].value = _citId;


                _ds = DBAccess.ExecDataSet("GetAllcitybyid", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllcitybyid" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void CityCheckDuplication(string name, Int32 id)
        {
            try
            {
                DataAccess obj = new DataAccess();
                obj.Provider = EnumProviders.SQLClient;
                obj.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["connstring"].ToString();

                ParamStruct[] param = new ParamStruct[2];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = Convert.ToDateTime("12/31/9999 11:59:59 PM");

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@citName";
                param[0].DataType = DbType.String;
                param[0].value = name;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@citId";
                param[1].DataType = DbType.Int32;
                param[1].value = id;

                _ds = obj.ExecDataSet("cityCheckDuplication", CommandType.StoredProcedure, param);
                if (_ds.Tables[0].Rows.Count > 0)
                {
                    _returnString = (_ds.Tables[0].Rows[0]["returnStatement"].ToString());

                }

            }
            catch (DataException ex)
            {
                throw ex;
            }


        }

        public void Getcitybystateid()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@citIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _citIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@citIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _citIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@citStateId";
                param[2].DataType = DbType.Int32;
                param[2].value = _citStateId;

                _ds = DBAccess.ExecDataSet("Getcitybystateid", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllState" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        #endregion
    }
}
