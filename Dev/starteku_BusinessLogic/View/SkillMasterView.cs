﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace starteku_BusinessLogic.View
{
    public class SkillMasterView
    {
        public int skillId { get; set; }
        public Nullable<int> skillUserId { get; set; }
        public Nullable<int> skillComId { get; set; }
        public Nullable<int> skillStatus { get; set; }
        public Nullable<System.DateTime> skillCreatedDate { get; set; }
        public Nullable<System.DateTime> skillUpdatedDate { get; set; }
        public Nullable<bool> skillIsDeleted { get; set; }
        public Nullable<bool> skillIsActive { get; set; }
        public Nullable<int> skillCompanyId { get; set; }
        public Nullable<int> skillLocal { get; set; }
        public Nullable<int> skillAchive { get; set; }
        public Nullable<int> skilltarget { get; set; }
        public Nullable<int> skillSet { get; set; }
        public Nullable<System.DateTime> skillAcceptDate { get; set; }
        public string skillComment { get; set; }
        public Nullable<bool> skillIsApproved { get; set; }
        public string name { get; set; }
        public string status { get; set; }
        public Nullable<bool> skillIsSaved { get; set; }

        public string SkillType { get; set; }
    }
}
