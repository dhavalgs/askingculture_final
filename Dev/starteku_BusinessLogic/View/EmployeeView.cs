﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace starteku_BusinessLogic.View
{
    public class EmployeeView
    {
        public int compId { get; set; }
        public string CompName { get; set; }
        public int LocalPoint { get; set; }
        public int EmpID { get; set; }
        public string UserFullName { get; set; }
        public decimal CompValue { get; set; } 


    }
}
