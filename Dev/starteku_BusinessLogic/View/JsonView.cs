﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace starteku_BusinessLogic.View
{
   public class JsonView
    {
        public string name { get; set; }

        public List<Int32> data { get; set; }

        public int id { get; set; }

        public string value { get; set; }
        public string label { get; set; }
    }
}
