﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace starteku_BusinessLogic.View
{
    public class DocumentView
    {
        public int DocId { get; set; }

        public int CompanyId { get; set; }

        public bool IsPublic { get; set; }

        public int UserId { get; set; }

        public string UserLastName { get; set; }

        public string UserGender { get; set; }

        public string UserZip { get; set; }

        public string UserEmail { get; set; }

       
        public int docId { get; set; }
        public string docFileName_Friendly { get; set; }
        public string docFileName_inSystem { get; set; }
        public string docattachment { get; set; }
        public string docType { get; set; }
        public Nullable<int> docUserId { get; set; }
        public Nullable<System.DateTime> docCreatedDate { get; set; }
        public Nullable<System.DateTime> docUpdatedDate { get; set; }
        public Nullable<bool> docIsDeleted { get; set; }
        public Nullable<bool> docIsActive { get; set; }
        public Nullable<int> docCompanyId { get; set; }
        public string docCreateBy { get; set; }
        public Nullable<int> docDepId { get; set; }
        public Nullable<bool> docApprovedStatus { get; set; }
        public string doccompetence { get; set; }
        public string docDivisionIds { get; set; }
        public string docJobTypeIds { get; set; }
        public Nullable<bool> docIsPublic { get; set; }
        public string docKeywords { get; set; }
        public string docRepository { get; set; }
        public string docDescription { get; set; }
        public Nullable<int> docShareToId { get; set; }

        public string MinAgo { get; set; }

        public int docUserID { get; set; }
        public string FullName { get; set; }
        public int docCatId { get; set; }

        //public string UserCategory { get; set; }
    }
}
