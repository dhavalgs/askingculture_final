﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starteku.Data.Manager;
using System.Data;
using starteku_BusinessLogic.Model;
using starteku_BusinessLogic.View;

namespace starteku_BusinessLogic
{
    public class UserBM
    {
        #region Private Declaration

        private DataSet _ds;
        private string _returnString;
        private bool _returnBoolean;
        private Int32 _userId;
        private String _userFirstName;
        private String _userFullName;
        private String _userLastName;
        private String _userGender;
        private String _userZip;
        private String _userAddress;
        private Int32 _userCountryId;
        private Int32 _userStateId;
        //private Int32 _userCityId;
        private String _userCityId;
        private String _usercontact;
        private String _userEmail;
        private Int32 _userType;
        private DateTime _userCreatedDate;
        private DateTime _userLastLoginDateTime;
        private DateTime _userUpdatedDate;
        private Boolean _userIsDeleted;
        private Boolean _userIsActive;
        private Int32 _userCompanyId;
        private String _userCreateBy;
        private String _userPassword;
        private DateTime _userDOB;
        private Boolean _depIsActive;
        private Boolean _depIsDeleted;
        private Int32 _depPerentId;
        private Boolean _divIsActive;
        private Boolean _divIsDeleted;
        private Int32 _divPerentId;
        private String _userdepId;
        private String _userdivId;
        private String _comname;

        private String _comAddress;
        private String _comphone;
        private String _userImage;
        private Int32 _userJobType;
        private Int32 _userLevel;

        private Int32 _languageUserId;
        private String _languageName;
        private String _languageCulture;
        private String _userCategory;
        private String _userNumber;

        private Int32 _TeamID;

        #region setting Declare Private Variables

        private int _SetUserId;
        private bool _SetNewFileUploadwarning;
        private bool _SetCompetenceLevelsChange;
        private bool _SetKnowledgeSharingRequests;
        private bool _SetInvitationNotification;
        private bool _SetActivityNotification;
        private bool _SetMessageNotification;
        private bool _SetIsDeleted;
        private bool _SetIsActive;
        private DateTime _SetCreatedDate;
        private DateTime _SetUpdatedDate;

        private String _UserIDs;
        #endregion


        #endregion

        #region Properties
        public string userNumber
        {
            get { return _userNumber; }
            set { _userNumber = value; }
        }

        public string UserCategory
        {
            get { return _userCategory; }
            set { _userCategory = value; }
        }

        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public DataSet ds
        {
            get { return _ds; }
            set { _ds = value; }
        }
        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public string ReturnString
        {
            get { return _returnString; }
            set { _returnString = value; }
        }

        /// <summary>
        /// Gets or sets the ReturnBoolean value.
        /// </summary>
        public bool ReturnBoolean
        {
            get { return _returnBoolean; }
            set { _returnBoolean = value; }
        }

        /// <summary>
        /// Gets or sets the userId value.
        /// </summary>
        public Int32 userId
        {
            get { return _userId; }
            set { _userId = value; }
        }

        public Int32 TeamID
        {
            get { return _TeamID; }
            set { _TeamID = value; }
        }

        /// <summary>
        /// Gets or sets the userFirstName value.
        /// </summary>
        public String userFirstName
        {
            get { return _userFirstName; }
            set { _userFirstName = value; }
        }

        public String UserFullName
        {
            get { return _userFullName; }
            set { _userFullName = value; }
        }

        /// <summary>
        /// Gets or sets the userLastName value.
        /// </summary>
        public String userLastName
        {
            get { return _userLastName; }
            set { _userLastName = value; }
        }

        /// <summary>
        /// Gets or sets the userGender value.
        /// </summary>
        public String userGender
        {
            get { return _userGender; }
            set { _userGender = value; }
        }

        /// <summary>
        /// Gets or sets the userZip value.
        /// </summary>
        public String userZip
        {
            get { return _userZip; }
            set { _userZip = value; }
        }

        /// <summary>
        /// Gets or sets the userAddress value.
        /// </summary>
        public String userAddress
        {
            get { return _userAddress; }
            set { _userAddress = value; }
        }

        /// <summary>
        /// Gets or sets the userCountryId value.
        /// </summary>
        public Int32 userCountryId
        {
            get { return _userCountryId; }
            set { _userCountryId = value; }
        }

        /// <summary>
        /// Gets or sets the userStateId value.
        /// </summary>
        public Int32 userStateId
        {
            get { return _userStateId; }
            set { _userStateId = value; }
        }

        /// <summary>
        /// Gets or sets the userCityId value.
        /// </summary>
        //public Int32 userCityId
        //{
        //    get { return _userCityId; }
        //    set { _userCityId = value; }
        //}
        public String userCityId
        {
            get { return _userCityId; }
            set { _userCityId = value; }
        }
        /// <summary>
        /// Gets or sets the usercontact value.
        /// </summary>
        public String usercontact
        {
            get { return _usercontact; }
            set { _usercontact = value; }
        }

        /// <summary>
        /// Gets or sets the userEmail value.
        /// </summary>
        public String userEmail
        {
            get { return _userEmail; }
            set { _userEmail = value; }
        }

        /// <summary>
        /// Gets or sets the userType value.
        /// </summary>
        public Int32 userType
        {
            get { return _userType; }
            set { _userType = value; }
        }

        /// <summary>
        /// Gets or sets the userCreatedDate value.
        /// </summary>
        public DateTime userCreatedDate
        {
            get { return _userCreatedDate; }
            set { _userCreatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the userUpdatedDate value.
        /// </summary>
        public DateTime userUpdatedDate
        {
            get { return _userUpdatedDate; }
            set { _userUpdatedDate = value; }
        }
        public DateTime userLastLoginDateTime
        {
            get { return _userLastLoginDateTime; }
            set { _userLastLoginDateTime = value; }
        }

        /// <summary>
        /// Gets or sets the userIsDeleted value.
        /// </summary>
        public Boolean userIsDeleted
        {
            get { return _userIsDeleted; }
            set { _userIsDeleted = value; }
        }

        /// <summary>
        /// Gets or sets the userIsActive value.
        /// </summary>
        public Boolean userIsActive
        {
            get { return _userIsActive; }
            set { _userIsActive = value; }
        }

        /// <summary>
        /// Gets or sets the userCompanyId value.
        /// </summary>
        public Int32 userCompanyId
        {
            get { return _userCompanyId; }
            set { _userCompanyId = value; }
        }

        /// <summary>
        /// Gets or sets the userCreateBy value.
        /// </summary>
        public String userCreateBy
        {
            get { return _userCreateBy; }
            set { _userCreateBy = value; }
        }
        public String userPassword
        {
            get { return _userPassword; }
            set { _userPassword = value; }
        }
        public DateTime userDOB
        {
            get { return _userDOB; }
            set { _userDOB = value; }
        }


        public Boolean depIsActive
        {
            get
            {
                return _depIsActive;
            }
            set
            {
                _depIsActive = value;
            }
        }

        public Boolean depIsDeleted
        {
            get
            {
                return _depIsDeleted;
            }
            set
            {
                _depIsDeleted = value;
            }
        }

        public Int32 depPerentId
        {
            get
            {
                return _depPerentId;
            }
            set
            {
                _depPerentId = value;
            }
        }

        public Boolean divIsActive
        {
            get
            {
                return _divIsActive;
            }
            set
            {
                _divIsActive = value;
            }
        }

        public Boolean divIsDeleted
        {
            get
            {
                return _divIsDeleted;
            }
            set
            {
                _divIsDeleted = value;
            }
        }

        public Int32 divPerentId
        {
            get
            {
                return _divPerentId;
            }
            set
            {
                _divPerentId = value;
            }
        }

        public String userdepId
        {
            get
            {
                return _userdepId;
            }
            set
            {
                _userdepId = value;
            }
        }
        public String userdivId
        {
            get
            {
                return _userdivId;
            }
            set
            {
                _userdivId = value;
            }
        }
        public String comname
        {
            get { return _comname; }
            set { _comname = value; }
        }
        public String comAddress
        {
            get { return _comAddress; }
            set { _comAddress = value; }
        }
        public String comphone
        {
            get { return _comphone; }
            set { _comphone = value; }
        }
        public String userImage
        {
            get { return _userImage; }
            set { _userImage = value; }
        }
        public Int32 userJobType
        {
            get
            {
                return _userJobType;
            }
            set
            {
                _userJobType = value;
            }
        }
        public Int32 userLevel
        {
            get
            {
                return _userLevel;
            }
            set
            {
                _userLevel = value;
            }
        }

        public Int32 languageUserId
        {
            get { return _languageUserId; }
            set { _languageUserId = value; }
        }
        public String languageName
        {
            get { return _languageName; }
            set { _languageName = value; }
        }
        public String languageCulture
        {
            get { return _languageCulture; }
            set { _languageCulture = value; }
        }


        public String UserIDs
        {
            get { return _UserIDs; }
            set { _UserIDs = value; }
        }
        #region setting properties

        public int SetUserId
        {
            get { return _SetUserId; }
            set { _SetUserId = value; }
        }
        public bool SetNewFileUploadwarning
        {
            get { return _SetNewFileUploadwarning; }
            set { _SetNewFileUploadwarning = value; }
        }
        public bool SetCompetenceLevelsChange
        {
            get { return _SetCompetenceLevelsChange; }
            set { _SetCompetenceLevelsChange = value; }
        }
        public bool SetKnowledgeSharingRequests
        {
            get { return _SetKnowledgeSharingRequests; }
            set { _SetKnowledgeSharingRequests = value; }
        }
        public bool SetIsDeleted
        {
            get { return _SetIsDeleted; }
            set { _SetIsDeleted = value; }
        }
        public bool SetIsActive
        {
            get { return _SetIsActive; }
            set { _SetIsActive = value; }
        }
        public DateTime SetCreatedDate
        {
            get { return _SetCreatedDate; }
            set { _SetCreatedDate = value; }
        }
        public DateTime SetUpdatedDate
        {
            get { return _SetUpdatedDate; }
            set { _SetUpdatedDate = value; }
        }
        public bool SetInvitationNotification
        {
            get { return _SetInvitationNotification; }
            set { _SetInvitationNotification = value; }
        }
        public bool SetMessageNotification
        {
            get { return _SetMessageNotification; }
            set { _SetMessageNotification = value; }
        }
        public bool SetActivityNotification
        {
            get { return _SetActivityNotification; }
            set { _SetActivityNotification = value; }
        }
        #endregion

        #endregion

        #region Methods
        public void GetLanguageById(Int32 userid)
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@userId";
                param[0].DataType = DbType.Int32;
                param[0].value = userid;



                _ds = DBAccess.ExecDataSet("GetLanguageById", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetLanguageById" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        public void userMasterLogin()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["connstring"].ToString();
                ParamStruct[] pm = new ParamStruct[3];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@userEmail";
                pm[0].DataType = DbType.String;
                pm[0].value = _userEmail;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@userPassword";
                pm[1].DataType = DbType.String;
                pm[1].value = _userPassword;

                pm[2].direction = ParameterDirection.Input;
                pm[2].ParamName = "@userType";
                pm[2].DataType = DbType.Int32;
                pm[2].value = _userType;

                _ds = DBAccess.ExecDataSet("userMasterLogin", CommandType.StoredProcedure, pm);
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in userMasterLogin " + " error message " + ex.Message + " error stack trace " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            }
        }
        public void userMasterLoginOrganisation()
        {
            try
            {

                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["connstring"].ToString();
                ParamStruct[] pm = new ParamStruct[3];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@userEmail";
                pm[0].DataType = DbType.String;
                pm[0].value = _userEmail;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@userPassword";
                pm[1].DataType = DbType.String;
                pm[1].value = _userPassword;

                pm[2].direction = ParameterDirection.Input;
                pm[2].ParamName = "@userType";
                pm[2].DataType = DbType.Int32;
                pm[2].value = _userType;

                _ds = DBAccess.ExecDataSet("userMasterLoginOrganisation", CommandType.StoredProcedure, pm);
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in userMasterLogin " + " error message " + ex.Message + " error stack trace " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            }
        }

        //public void changepassword()
        //{
        //    try
        //    {

        //        DataAccess DBAccess = new DataAccess();
        //        DBAccess.Provider = EnumProviders.SQLClient;
        //        DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["connstring"].ToString();
        //        ParamStruct[] pm = new ParamStruct[2];

        //        pm[0].direction = ParameterDirection.Input;
        //        pm[0].ParamName = "@userEmail";
        //        pm[0].DataType = DbType.String;
        //        pm[0].value = _userEmail;

        //        pm[1].direction = ParameterDirection.Input;
        //        pm[1].ParamName = "@userPassword";
        //        pm[1].DataType = DbType.String;
        //        pm[1].value = _userPassword;

        //        _ds = DBAccess.ExecDataSet("changepassword", CommandType.StoredProcedure, pm);
        //    }
        //    catch (Exception ex)
        //    {
        //        Common.WriteLog("error in userMasterLogin " + " error message " + ex.Message + " error stack trace " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
        //    }
        //}

        public void InsertUser()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[28];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue;

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@userFirstName";
                param[0].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userFirstName))
                    param[0].value = DBNull.Value;
                else
                    param[0].value = _userFirstName;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@userLastName";
                param[1].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userLastName))
                    param[1].value = DBNull.Value;
                else
                    param[1].value = _userLastName;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@userGender";
                param[2].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userGender))
                    param[2].value = DBNull.Value;
                else
                    param[2].value = _userGender;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@userZip";
                param[3].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userZip))
                    param[3].value = DBNull.Value;
                else
                    param[3].value = _userZip;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@userAddress";
                param[4].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userAddress))
                    param[4].value = DBNull.Value;
                else
                    param[4].value = _userAddress;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@userCountryId";
                param[5].DataType = DbType.Int32;
                if (_userCountryId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[5].value = _userCountryId;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@userStateId";
                param[6].DataType = DbType.Int32;
                if (_userStateId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[6].value = _userStateId;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@userCityId";
                param[7].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userCityId))
                    param[7].value = DBNull.Value;
                else
                    param[7].value = _userCityId;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@usercontact";
                param[8].DataType = DbType.String;
                if (String.IsNullOrEmpty(_usercontact))
                    param[8].value = DBNull.Value;
                else
                    param[8].value = _usercontact;

                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@userEmail";
                param[9].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userEmail))
                    param[9].value = DBNull.Value;
                else
                    param[9].value = _userEmail;

                param[10].direction = ParameterDirection.Input;
                param[10].ParamName = "@userType";
                param[10].DataType = DbType.Int32;
                if (_userType == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[10].value = _userType;

                param[11].direction = ParameterDirection.Input;
                param[11].ParamName = "@userCreatedDate";
                param[11].DataType = DbType.DateTime;
                if (_userCreatedDate.Ticks > dtmin.Ticks && _userCreatedDate.Ticks < dtmax.Ticks)
                    param[11].value = _userCreatedDate;
                else if (_userCreatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[11].value = _userCreatedDate;

                param[12].direction = ParameterDirection.Input;
                param[12].ParamName = "@userUpdatedDate";
                param[12].DataType = DbType.DateTime;
                if (_userUpdatedDate.Ticks > dtmin.Ticks && _userUpdatedDate.Ticks < dtmax.Ticks)
                    param[12].value = _userUpdatedDate;
                else if (_userUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[12].value = _userUpdatedDate;

                param[13].direction = ParameterDirection.Input;
                param[13].ParamName = "@userIsDeleted";
                param[13].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_userIsDeleted.ToString()))
                    param[13].value = DBNull.Value;
                else
                    param[13].value = _userIsDeleted;

                param[14].direction = ParameterDirection.Input;
                param[14].ParamName = "@userIsActive";
                param[14].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_userIsActive.ToString()))
                    param[14].value = DBNull.Value;
                else
                    param[14].value = _userIsActive;

                param[15].direction = ParameterDirection.Input;
                param[15].ParamName = "@userCompanyId";
                param[15].DataType = DbType.Int32;
                param[15].value = _userCompanyId;

                param[16].direction = ParameterDirection.Input;
                param[16].ParamName = "@userCreateBy";
                param[16].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userCreateBy))
                    param[16].value = DBNull.Value;
                else
                    param[16].value = _userCreateBy;

                param[17].direction = ParameterDirection.Input;
                param[17].ParamName = "@userPassword";
                param[17].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userPassword))
                    param[17].value = DBNull.Value;
                else
                    param[17].value = _userPassword;

                param[18].direction = ParameterDirection.Input;
                param[18].ParamName = "@userDOB";
                param[18].DataType = DbType.DateTime;
                if (_userDOB.Ticks > dtmin.Ticks && _userDOB.Ticks < dtmax.Ticks)
                    param[18].value = _userDOB;
                else if (_userDOB.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[18].value = _userDOB;

                param[19].direction = ParameterDirection.Input;
                param[19].ParamName = "@userdepId";
                param[19].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userdepId))
                    param[19].value = DBNull.Value;
                else
                    param[19].value = _userdepId;


                param[20].direction = ParameterDirection.Input;
                param[20].ParamName = "@userdivId";
                param[20].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userdivId))
                    param[20].value = DBNull.Value;
                else
                    param[20].value = _userdivId;

                param[21].direction = ParameterDirection.Input;
                param[21].ParamName = "@comname";
                param[21].DataType = DbType.String;
                if (String.IsNullOrEmpty(_comname))
                    param[21].value = DBNull.Value;
                else
                    param[21].value = _comname;

                param[22].direction = ParameterDirection.Input;
                param[22].ParamName = "@comAddress";
                param[22].DataType = DbType.String;
                if (String.IsNullOrEmpty(_comAddress))
                    param[22].value = DBNull.Value;
                else
                    param[22].value = _comAddress;

                param[23].direction = ParameterDirection.Input;
                param[23].ParamName = "@comphone";
                param[23].DataType = DbType.String;
                if (String.IsNullOrEmpty(_comphone))
                    param[23].value = DBNull.Value;
                else
                    param[23].value = _comphone;

                param[24].direction = ParameterDirection.Input;
                param[24].ParamName = "@userImage";
                param[24].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userImage))
                    param[24].value = DBNull.Value;
                else
                    param[24].value = _userImage;

                param[25].direction = ParameterDirection.Input;
                param[25].ParamName = "@userLevel";
                param[25].DataType = DbType.String;
                param[25].value = 0;

                param[26].direction = ParameterDirection.Input;
                param[26].ParamName = "@userJobType";
                param[26].DataType = DbType.String;
                param[26].value = 0;

                param[27].direction = ParameterDirection.Input;
                param[27].ParamName = "@userLastLoginDateTime";
                param[27].DataType = DbType.DateTime;
                if (_userLastLoginDateTime.Ticks > dtmin.Ticks && _userLastLoginDateTime.Ticks < dtmax.Ticks)
                    param[27].value = _userLastLoginDateTime;
                else if (_userLastLoginDateTime.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[27].value = _userLastLoginDateTime;

                //obj.ExecScalar("InsertUser", CommandType.StoredProcedure, param); _returnBoolean = true;
                _ds = DBAccess.ExecDataSet("InsertUser", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                Common.WriteLog("error in InsertUser" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;
            }
        }

        public void useremailCheckDuplication(string name, Int32 id)
        {
            try
            {
                DataAccess obj = new DataAccess();
                obj.Provider = EnumProviders.SQLClient;
                obj.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["connstring"].ToString();

                ParamStruct[] param = new ParamStruct[2];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue;

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@userEmail";
                param[0].DataType = DbType.String;
                param[0].value = name;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@userId";
                param[1].DataType = DbType.Int32;
                param[1].value = id;

                _ds = obj.ExecDataSet("useremailCheckDuplication", CommandType.StoredProcedure, param);
                if (_ds.Tables[0].Rows.Count > 0)
                {
                    _returnString = (_ds.Tables[0].Rows[0]["returnStatement"].ToString());

                }

            }
            catch (DataException ex)
            {
                throw ex;
            }


        }

        public void GetAllEmployee()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[5];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@userIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _userIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@userIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _userIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@userType";
                param[2].DataType = DbType.Int32;
                param[2].value = _userType;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@userCompanyId";
                param[3].DataType = DbType.Int32;
                param[3].value = _userCompanyId;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@userCreateBy";
                param[4].DataType = DbType.Int32;
                param[4].value = Convert.ToInt32(_userCreateBy);

                _ds = DBAccess.ExecDataSet("GetAllEmployee", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllState" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        public void GetAllEmployeecreatebymanager()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[5];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@userIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _userIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@userIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _userIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@userType";
                param[2].DataType = DbType.Int32;
                param[2].value = _userType;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@userCompanyId";
                param[3].DataType = DbType.Int32;
                param[3].value = _userCompanyId;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@userCreateBy";
                param[4].DataType = DbType.String;
                param[4].value = _userCreateBy;

                _ds = DBAccess.ExecDataSet("GetAllEmployeecreatebymanager", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllEmployeecreatebymanager" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        public void GetAllEmployeecreatebymanager_ViewPoint()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[5];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@userIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _userIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@userIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _userIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@userType";
                param[2].DataType = DbType.Int32;
                param[2].value = _userType;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@userCompanyId";
                param[3].DataType = DbType.Int32;
                param[3].value = _userCompanyId;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@userCreateBy";
                param[4].DataType = DbType.String;
                param[4].value = _userCreateBy;

                _ds = DBAccess.ExecDataSet("GetAllEmployeecreatebymanager_ViewPoint", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllEmployeecreatebymanager_ViewPoint" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void GetAllLearningPointReport()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@CreateBy";
                param[0].DataType = DbType.Int32;
                param[0].value = _userCreateBy;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@CompanyId";
                param[1].DataType = DbType.Int32;
                param[1].value = _userCompanyId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@usrType";
                param[2].DataType = DbType.Int32;
                param[2].value = _userType;


                _ds = DBAccess.ExecDataSet("GetAllLearningPointReport", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllLearningPointReport" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void GetAllEmployeeByCompanyId()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[4];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@userIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _userIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@userIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _userIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@userCompanyId";
                param[2].DataType = DbType.Int32;
                param[2].value = _userCompanyId;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@name";
                param[3].DataType = DbType.String;
                param[3].value = _userFirstName;

                _ds = DBAccess.ExecDataSet("GetAllEmployeeByCompanyId", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllEmployeeByCompanyId" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void EmployeeStatusUpdate()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] pm = new ParamStruct[3];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@userId";
                pm[0].DataType = DbType.Int32;
                pm[0].value = _userId;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@userIsActive";
                pm[1].DataType = DbType.Boolean;
                pm[1].value = _userIsActive;

                pm[2].direction = ParameterDirection.Input;
                pm[2].ParamName = "@userIsDeleted";
                pm[2].DataType = DbType.Boolean;
                pm[2].value = _userIsDeleted;

                _ds = DBAccess.ExecDataSet("EmployeeStatusUpdate", CommandType.StoredProcedure, pm);
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in EmployeeStatusUpdate  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            }
        }

        public void GetAllEmployeebyid()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@userId";
                param[0].DataType = DbType.Int32;
                param[0].value = _userId;


                _ds = DBAccess.ExecDataSet("GetAllEmployeebyid", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllEmployeebyid" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void UpdateUser()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[20];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue;

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@userId";
                param[0].DataType = DbType.Int32;
                param[0].value = _userId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@userFirstName";
                param[1].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userFirstName))
                    param[1].value = DBNull.Value;
                else
                    param[1].value = _userFirstName;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@userLastName";
                param[2].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userLastName))
                    param[2].value = DBNull.Value;
                else
                    param[2].value = _userLastName;


                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@userZip";
                param[3].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userZip))
                    param[3].value = DBNull.Value;
                else
                    param[3].value = _userZip;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@userAddress";
                param[4].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userAddress))
                    param[4].value = DBNull.Value;
                else
                    param[4].value = _userAddress;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@userCountryId";
                param[5].DataType = DbType.Int32;
                if (_userCountryId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[5].value = _userCountryId;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@userStateId";
                param[6].DataType = DbType.Int32;
                if (_userStateId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[6].value = _userStateId;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@userCityId";
                param[7].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userCityId))
                    param[7].value = DBNull.Value;
                else
                    param[7].value = _userCityId;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@usercontact";
                param[8].DataType = DbType.String;
                if (String.IsNullOrEmpty(_usercontact))
                    param[8].value = DBNull.Value;
                else
                    param[8].value = _usercontact;

                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@userEmail";
                param[9].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userEmail))
                    param[9].value = DBNull.Value;
                else
                    param[9].value = _userEmail;



                param[10].direction = ParameterDirection.Input;
                param[10].ParamName = "@userUpdatedDate";
                param[10].DataType = DbType.DateTime;
                if (_userUpdatedDate.Ticks > dtmin.Ticks && _userUpdatedDate.Ticks < dtmax.Ticks)
                    param[10].value = _userUpdatedDate;
                else if (_userUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[10].value = _userUpdatedDate;

                param[11].direction = ParameterDirection.Input;
                param[11].ParamName = "@userPassword";
                param[11].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userPassword))
                    param[11].value = DBNull.Value;
                else
                    param[11].value = _userPassword;


                param[12].direction = ParameterDirection.Input;
                param[12].ParamName = "@userDOB";
                param[12].DataType = DbType.DateTime;
                if (_userDOB.Ticks > dtmin.Ticks && _userDOB.Ticks < dtmax.Ticks)
                    param[12].value = _userDOB;
                else if (_userDOB.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[12].value = _userDOB;

                param[13].direction = ParameterDirection.Input;
                param[13].ParamName = "@userdepId";
                param[13].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userdepId))
                    param[13].value = DBNull.Value;
                else
                    param[13].value = _userdepId;

                param[14].direction = ParameterDirection.Input;
                param[14].ParamName = "@userdivId";
                param[14].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userdivId))
                    param[14].value = DBNull.Value;
                else
                    param[14].value = _userdivId;


                param[15].direction = ParameterDirection.Input;
                param[15].ParamName = "@comname";
                param[15].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userFirstName))
                    param[15].value = DBNull.Value;
                else
                    param[15].value = _comname;


                param[16].direction = ParameterDirection.Input;
                param[16].ParamName = "@comAddress";
                param[16].DataType = DbType.String;
                if (String.IsNullOrEmpty(_comAddress))
                    param[16].value = DBNull.Value;
                else
                    param[16].value = _comAddress;

                param[17].direction = ParameterDirection.Input;
                param[17].ParamName = "@comphone";
                param[17].DataType = DbType.String;
                if (String.IsNullOrEmpty(_comphone))
                    param[17].value = DBNull.Value;
                else
                    param[17].value = _comphone;


                param[18].direction = ParameterDirection.Input;
                param[18].ParamName = "@userGender";
                param[18].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userGender))
                    param[18].value = DBNull.Value;
                else
                    param[18].value = _userGender;

                param[19].direction = ParameterDirection.Input;
                param[19].ParamName = "@userImage";
                param[19].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userImage))
                    param[19].value = DBNull.Value;
                else
                    param[19].value = _userImage;

                _ds = DBAccess.ExecDataSet("UpdateUser", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        // Saurin | 20150102 |  This method get result from usermaster table by userId and return result as collection List
        public static UserMaster GetUserByIdLinq(int id)
        {
            var db = new startetkuEntities1();
            try
            {
                //var userData = (from o in db.UserMasters
                //                from od in db.ActicityEnableMasters
                //                where (o.userId == od.Aemcompid || o.userCompanyId == od.Aemcompid) &&
                //                 o.userId == id select o).FirstOrDefault();
                var userData = (from o in db.UserMasters
                                where o.userId == id
                                select o).FirstOrDefault();
                return userData;
            }
            catch (Exception)
            {


            }
            return new UserMaster();
        }
        public static List<UserMasterView> GetUserById(int id)
        {
            var obj = new UserBM();
            obj.userId = Convert.ToInt32(id);
            obj.GetAllEmployeebyid();
            DataSet ds = obj.ds;

            var userDetailInList = ds.Tables[0].AsEnumerable().Select(r => new UserMasterView
            {
                UserId = r.Field<int>("userId"),
                UserType = r.Field<int>("userType"),
                UserLastLoginDateTime = r.Field<DateTime?>("userLastLoginDateTime"),
                UserCompanyId = r.Field<int>("userCompanyId"),
                UserFirstName = r.Field<string>("userFirstName"),
                UserLastName = r.Field<string>("userLastName")

            });
            return userDetailInList.ToList(); // For if you really need a List and not IEnumerable

        }

        public void InsertOrganisation()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[27];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue;

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@userFirstName";
                param[0].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userFirstName))
                    param[0].value = DBNull.Value;
                else
                    param[0].value = _userFirstName;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@userLastName";
                param[1].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userLastName))
                    param[1].value = DBNull.Value;
                else
                    param[1].value = _userLastName;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@userGender";
                param[2].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userGender))
                    param[2].value = DBNull.Value;
                else
                    param[2].value = _userGender;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@userZip";
                param[3].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userZip))
                    param[3].value = DBNull.Value;
                else
                    param[3].value = _userZip;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@userAddress";
                param[4].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userAddress))
                    param[4].value = DBNull.Value;
                else
                    param[4].value = _userAddress;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@userCountryId";
                param[5].DataType = DbType.Int32;
                if (_userCountryId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[5].value = _userCountryId;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@userStateId";
                param[6].DataType = DbType.Int32;
                if (_userStateId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[6].value = _userStateId;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@userCityId";
                param[7].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userCityId))
                    param[7].value = DBNull.Value;
                else
                    param[7].value = _userCityId;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@usercontact";
                param[8].DataType = DbType.String;
                if (String.IsNullOrEmpty(_usercontact))
                    param[8].value = DBNull.Value;
                else
                    param[8].value = _usercontact;

                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@userEmail";
                param[9].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userEmail))
                    param[9].value = DBNull.Value;
                else
                    param[9].value = _userEmail;

                param[10].direction = ParameterDirection.Input;
                param[10].ParamName = "@userType";
                param[10].DataType = DbType.Int32;
                if (_userType == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[10].value = _userType;

                param[11].direction = ParameterDirection.Input;
                param[11].ParamName = "@userCreatedDate";
                param[11].DataType = DbType.DateTime;
                if (_userCreatedDate.Ticks > dtmin.Ticks && _userCreatedDate.Ticks < dtmax.Ticks)
                    param[11].value = _userCreatedDate;
                else if (_userCreatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[11].value = _userCreatedDate;

                param[12].direction = ParameterDirection.Input;
                param[12].ParamName = "@userUpdatedDate";
                param[12].DataType = DbType.DateTime;
                if (_userUpdatedDate.Ticks > dtmin.Ticks && _userUpdatedDate.Ticks < dtmax.Ticks)
                    param[12].value = _userUpdatedDate;
                else if (_userUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[12].value = _userUpdatedDate;

                param[13].direction = ParameterDirection.Input;
                param[13].ParamName = "@userIsDeleted";
                param[13].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_userIsDeleted.ToString()))
                    param[13].value = DBNull.Value;
                else
                    param[13].value = _userIsDeleted;

                param[14].direction = ParameterDirection.Input;
                param[14].ParamName = "@userIsActive";
                param[14].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_userIsActive.ToString()))
                    param[14].value = DBNull.Value;
                else
                    param[14].value = _userIsActive;

                param[15].direction = ParameterDirection.Input;
                param[15].ParamName = "@userCompanyId";
                param[15].DataType = DbType.Int32;
                param[15].value = _userCompanyId;

                param[16].direction = ParameterDirection.Input;
                param[16].ParamName = "@userCreateBy";
                param[16].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userCreateBy))
                    param[16].value = DBNull.Value;
                else
                    param[16].value = _userCreateBy;


                param[17].direction = ParameterDirection.Input;
                param[17].ParamName = "@userPassword";
                param[17].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userPassword))
                    param[17].value = DBNull.Value;
                else
                    param[17].value = _userPassword;


                param[18].direction = ParameterDirection.Input;
                param[18].ParamName = "@userDOB";
                param[18].DataType = DbType.DateTime;
                if (_userDOB.Ticks > dtmin.Ticks && _userUpdatedDate.Ticks < dtmax.Ticks)
                    param[18].value = _userDOB;
                else if (_userDOB.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[18].value = _userDOB;


                param[19].direction = ParameterDirection.Input;
                param[19].ParamName = "@userdepId";
                param[19].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userdepId))
                    param[19].value = DBNull.Value;
                else
                    param[19].value = _userdepId;


                param[20].direction = ParameterDirection.Input;
                param[20].ParamName = "@userdivId";
                param[20].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userdivId))
                    param[20].value = DBNull.Value;
                else
                    param[20].value = _userdivId;

                param[21].direction = ParameterDirection.Input;
                param[21].ParamName = "@userImage";
                param[21].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userImage))
                    param[21].value = DBNull.Value;
                else
                    param[21].value = _userImage;


                param[22].direction = ParameterDirection.Input;
                param[22].ParamName = "@userJobType";
                param[22].DataType = DbType.Int32;
                param[22].value = _userJobType;

                param[23].direction = ParameterDirection.Input;
                param[23].ParamName = "@userLevel";
                param[23].DataType = DbType.String;
                param[23].value = _userLevel;

                param[24].direction = ParameterDirection.Input;
                param[24].ParamName = "@userLastLoginDateTime";
                param[24].DataType = DbType.DateTime;
                if (_userLastLoginDateTime.Ticks > dtmin.Ticks && _userLastLoginDateTime.Ticks < dtmax.Ticks)
                    param[24].value = _userLastLoginDateTime;
                else if (_userLastLoginDateTime.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[24].value = _userLastLoginDateTime;

                param[25].direction = ParameterDirection.Input;
                param[25].ParamName = "@userCategory";
                param[25].DataType = DbType.String;
                param[25].value = _userCategory;

                param[26].direction = ParameterDirection.Input;
                param[26].ParamName = "@userTeam";
                param[26].DataType = DbType.Int32;
                param[26].value = _TeamID;

                _ds = DBAccess.ExecDataSet("InsertOrganisation", CommandType.StoredProcedure, param);
                // return Convert.ToInt32(_ds.Tables[0].Rows[0][0]);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                Common.WriteLog("error in InsertUser" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;
            }
        }
        public void UpdateOrganisation()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[18];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue;

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@userId";
                param[0].DataType = DbType.Int32;
                param[0].value = _userId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@userFirstName";
                param[1].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userFirstName))
                    param[1].value = DBNull.Value;
                else
                    param[1].value = _userFirstName;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@userLastName";
                param[2].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userLastName))
                    param[2].value = DBNull.Value;
                else
                    param[2].value = _userLastName;


                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@userZip";
                param[3].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userZip))
                    param[3].value = DBNull.Value;
                else
                    param[3].value = _userZip;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@userAddress";
                param[4].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userAddress))
                    param[4].value = DBNull.Value;
                else
                    param[4].value = _userAddress;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@userCountryId";
                param[5].DataType = DbType.Int32;
                if (_userCountryId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[5].value = _userCountryId;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@userStateId";
                param[6].DataType = DbType.Int32;
                if (_userStateId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[6].value = _userStateId;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@userCityId";
                param[7].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userCityId))
                    param[7].value = DBNull.Value;
                else
                    param[7].value = _userCityId;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@usercontact";
                param[8].DataType = DbType.String;
                if (String.IsNullOrEmpty(_usercontact))
                    param[8].value = DBNull.Value;
                else
                    param[8].value = _usercontact;

                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@userEmail";
                param[9].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userEmail))
                    param[9].value = DBNull.Value;
                else
                    param[9].value = _userEmail;



                param[10].direction = ParameterDirection.Input;
                param[10].ParamName = "@userUpdatedDate";
                param[10].DataType = DbType.DateTime;
                if (_userUpdatedDate.Ticks > dtmin.Ticks && _userUpdatedDate.Ticks < dtmax.Ticks)
                    param[10].value = _userUpdatedDate;
                else if (_userUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[10].value = _userUpdatedDate;

                param[11].direction = ParameterDirection.Input;
                param[11].ParamName = "@userPassword";
                param[11].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userPassword))
                    param[11].value = DBNull.Value;
                else
                    param[11].value = _userPassword;


                param[12].direction = ParameterDirection.Input;
                param[12].ParamName = "@userDOB";
                param[12].DataType = DbType.DateTime;
                if (_userDOB.Ticks > dtmin.Ticks && _userUpdatedDate.Ticks < dtmax.Ticks)
                    param[12].value = _userDOB;
                else if (_userDOB.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[12].value = _userDOB;

                param[13].direction = ParameterDirection.Input;
                param[13].ParamName = "@userdepId";
                param[13].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userdepId))
                    param[13].value = DBNull.Value;
                else
                    param[13].value = _userdepId;

                param[14].direction = ParameterDirection.Input;
                param[14].ParamName = "@userdivId";
                param[14].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userdivId))
                    param[14].value = DBNull.Value;
                else
                    param[14].value = _userdivId;


                param[15].direction = ParameterDirection.Input;
                param[15].ParamName = "@userGender";
                param[15].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userGender))
                    param[15].value = DBNull.Value;
                else
                    param[15].value = _userGender;

                param[16].direction = ParameterDirection.Input;
                param[16].ParamName = "@userImage";
                param[16].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userImage))
                    param[16].value = DBNull.Value;
                else
                    param[16].value = _userImage;

                param[17].direction = ParameterDirection.Input;
                param[17].ParamName = "@userJobType";
                param[17].DataType = DbType.Int32;
                param[17].value = _userJobType;

                _ds = DBAccess.ExecDataSet("UpdateOrganisation", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }
        public void UpdateUsernew()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[16];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue;

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@userId";
                param[0].DataType = DbType.Int32;
                param[0].value = _userId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@userFirstName";
                param[1].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userFirstName))
                    param[1].value = DBNull.Value;
                else
                    param[1].value = _userFirstName;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@userLastName";
                param[2].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userLastName))
                    param[2].value = DBNull.Value;
                else
                    param[2].value = _userLastName;


                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@userZip";
                param[3].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userZip))
                    param[3].value = DBNull.Value;
                else
                    param[3].value = _userZip;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@userAddress";
                param[4].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userAddress))
                    param[4].value = DBNull.Value;
                else
                    param[4].value = _userAddress;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@userCountryId";
                param[5].DataType = DbType.Int32;
                if (_userCountryId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[5].value = _userCountryId;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@userStateId";
                param[6].DataType = DbType.Int32;
                if (_userStateId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[6].value = _userStateId;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@userCityId";
                param[7].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userCityId))
                    param[7].value = DBNull.Value;
                else
                    param[7].value = _userCityId;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@usercontact";
                param[8].DataType = DbType.String;
                if (String.IsNullOrEmpty(_usercontact))
                    param[8].value = DBNull.Value;
                else
                    param[8].value = _usercontact;

                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@userEmail";
                param[9].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userEmail))
                    param[9].value = DBNull.Value;
                else
                    param[9].value = _userEmail;



                param[10].direction = ParameterDirection.Input;
                param[10].ParamName = "@userUpdatedDate";
                param[10].DataType = DbType.DateTime;
                if (_userUpdatedDate.Ticks > dtmin.Ticks && _userUpdatedDate.Ticks < dtmax.Ticks)
                    param[10].value = _userUpdatedDate;
                else if (_userUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[10].value = _userUpdatedDate;

                param[11].direction = ParameterDirection.Input;
                param[11].ParamName = "@userPassword";
                param[11].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userPassword))
                    param[11].value = DBNull.Value;
                else
                    param[11].value = _userPassword;


                param[12].direction = ParameterDirection.Input;
                param[12].ParamName = "@userDOB";
                param[12].DataType = DbType.DateTime;
                if (_userDOB.Ticks > dtmin.Ticks && _userDOB.Ticks < dtmax.Ticks)
                    param[12].value = _userDOB;
                else if (_userDOB.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[12].value = _userDOB;

                param[13].direction = ParameterDirection.Input;
                param[13].ParamName = "@userGender";
                param[13].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userGender))
                    param[13].value = DBNull.Value;
                else
                    param[13].value = _userGender;

                param[14].direction = ParameterDirection.Input;
                param[14].ParamName = "@userImage";
                param[14].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userImage))
                    param[14].value = DBNull.Value;
                else
                    param[14].value = _userImage;

                param[15].direction = ParameterDirection.Input;
                param[15].ParamName = "@userNumber";
                param[15].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userNumber))
                    param[15].value = DBNull.Value;
                else
                    param[15].value = _userNumber;


                // obj.ExecScalar("UpdateUser", CommandType.StoredProcedure, param); _returnBoolean = true;

                _ds = DBAccess.ExecDataSet("UpdateUsernew", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        public void GetAllDepartmentsByPerentId()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@depIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _depIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@depIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _depIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@depPerentId";
                param[2].DataType = DbType.Int32;
                param[2].value = _depPerentId;

                _ds = DBAccess.ExecDataSet("GetAllDepartmentsByPerentId", CommandType.StoredProcedure, param);
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in GetAllDepartmentsByPerentId" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;
            }
        }

        public void GetAllDivisionsByPerentId()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@divIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _divIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@divIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _divIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@divPerentId";
                param[2].DataType = DbType.Int32;
                param[2].value = _divPerentId;

                _ds = DBAccess.ExecDataSet("GetAllDivisionsByPerentId", CommandType.StoredProcedure, param);
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in GetAllDivisionsByPerentId" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;
            }
        }

        public void InsertBulkdata(int id, String udepId, String udivId)
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[3];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue;

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@userId";
                param[0].DataType = DbType.Int32;
                param[0].value = id;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@userdepId";
                param[1].DataType = DbType.String;
                param[1].value = udepId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@userdivId";
                param[2].DataType = DbType.String;
                param[2].value = udivId;

                _ds = DBAccess.ExecDataSet("InsertBulkdata", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                Common.WriteLog("error in InsertUser" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;
            }
        }

        public void SelectPasswordByUserName()
        {
            DataAccess obj = new DataAccess();
            obj.Provider = EnumProviders.SQLClient;
            obj.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["connstring"].ToString(); ;
            ParamStruct[] param = new ParamStruct[1];

            param[0].direction = ParameterDirection.Input;
            param[0].ParamName = "@userEmail";
            param[0].DataType = DbType.String;
            param[0].value = _userEmail;


            _ds = obj.ExecDataSet("SelectPasswordByUserName", CommandType.StoredProcedure, param);
        }

        public void Updatepassword()
        {
            try
            {
                DataAccess obj = new DataAccess();
                obj.Provider = EnumProviders.SQLClient;
                obj.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["connstring"].ToString(); ;
                ParamStruct[] param = new ParamStruct[2];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue;
                param[0].direction = ParameterDirection.Input;

                param[0].ParamName = "@userId";
                param[0].DataType = DbType.Int32;
                param[0].value = _userId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@userPassword";
                param[1].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userPassword))
                    param[1].value = DBNull.Value;
                else
                    param[1].value = _userPassword;


                obj.ExecScalar("Updatepassword", CommandType.StoredProcedure, param); _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        public void GetEmployeedetailsbyid()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@userId";
                param[0].DataType = DbType.Int32;
                param[0].value = _userId;


                _ds = DBAccess.ExecDataSet("GetEmployeedetailsbyid", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetEmployeedetailsbyid" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void UpdateUserSetting()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[8];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue;

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@SetUserId";
                param[0].DataType = DbType.Int32;
                param[0].value = _SetUserId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@SetNewFileUploadwarning";
                param[1].DataType = DbType.Boolean;
                param[1].value = _SetNewFileUploadwarning;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@SetCompetenceLevelsChange";
                param[2].DataType = DbType.Boolean;
                param[2].value = _SetCompetenceLevelsChange;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@SetKnowledgeSharingRequests";
                param[3].DataType = DbType.Boolean;
                param[3].value = _SetKnowledgeSharingRequests;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@SetUpdatedDate";
                param[4].DataType = DbType.DateTime;
                if (_SetUpdatedDate.Ticks > dtmin.Ticks && _SetUpdatedDate.Ticks < dtmax.Ticks)
                    param[4].value = _SetUpdatedDate;
                else if (_SetUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[4].value = _SetUpdatedDate;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@SetInvitationNotification";
                param[5].DataType = DbType.Boolean;
                param[5].value = _SetInvitationNotification;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@SetMessageNotification";
                param[6].DataType = DbType.Boolean;
                param[6].value = _SetMessageNotification;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@SetActivityNotification";
                param[7].DataType = DbType.Boolean;
                param[7].value = _SetActivityNotification;

                _ds = DBAccess.ExecDataSet("UpdateUserSetting", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (Exception ex)
            {

            }
        }
        public void GetUserSettingById()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@SetUserId";
                param[0].DataType = DbType.Int32;
                param[0].value = _SetUserId;

                _ds = DBAccess.ExecDataSet("GetUserSettingById", CommandType.StoredProcedure, param);
            }
            catch (Exception ex)
            {

            }
        }

        public void UpdateEmployees()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[12];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue;

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@userId";
                param[0].DataType = DbType.Int32;
                param[0].value = _userId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@userFirstName";
                param[1].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userFirstName))
                    param[1].value = DBNull.Value;
                else
                    param[1].value = _userFirstName;



                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@userAddress";
                param[2].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userAddress))
                    param[2].value = DBNull.Value;
                else
                    param[2].value = _userAddress;



                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@usercontact";
                param[3].DataType = DbType.String;
                if (String.IsNullOrEmpty(_usercontact))
                    param[3].value = DBNull.Value;
                else
                    param[3].value = _usercontact;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@userEmail";
                param[4].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userEmail))
                    param[4].value = DBNull.Value;
                else
                    param[4].value = _userEmail;



                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@userUpdatedDate";
                param[5].DataType = DbType.DateTime;
                if (_userUpdatedDate.Ticks > dtmin.Ticks && _userUpdatedDate.Ticks < dtmax.Ticks)
                    param[5].value = _userUpdatedDate;
                else if (_userUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[5].value = _userUpdatedDate;



                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@userdepId";
                param[6].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userdepId))
                    param[6].value = DBNull.Value;
                else
                    param[6].value = _userdepId;


                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@userJobType";
                param[7].DataType = DbType.Int32;
                param[7].value = _userJobType;


                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@userCategory";
                param[8].DataType = DbType.String;
                param[8].value = _userCategory;


                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@userLastName";
                param[9].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userLastName))
                    param[9].value = DBNull.Value;
                else
                    param[9].value = _userLastName;


                param[10].direction = ParameterDirection.Input;
                param[10].ParamName = "@userTeamID";
                param[10].DataType = DbType.Int32;
                param[10].value = _TeamID;


                param[11].direction = ParameterDirection.Input;
                param[11].ParamName = "@userGender";
                param[11].DataType = DbType.String;
                if (String.IsNullOrEmpty(_userGender))
                    param[11].value = DBNull.Value;
                else
                    param[11].value = _userGender;


                _ds = DBAccess.ExecDataSet("UpdateEmployees", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        public void updateUserMasterLastLoginDatetime(int userId, DateTime loginTime)
        {
            try
            {

                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["connstring"].ToString();
                ParamStruct[] pm = new ParamStruct[2];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@userId";
                pm[0].DataType = DbType.String;
                pm[0].value = userId;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@loginTime";
                pm[1].DataType = DbType.DateTime;
                pm[1].value = loginTime;

                _ds = DBAccess.ExecDataSet("updateUserMasterLastLoginDatetime", CommandType.StoredProcedure, pm);
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in updateUserMasterLastLoginDatetime " + " error message " + ex.Message + " error stack trace " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            }
        }

        public Boolean Updatelanguagemaster()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@languageUserId";
                param[0].DataType = DbType.Int32;
                param[0].value = _languageUserId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@languageName";
                param[1].DataType = DbType.String;
                param[1].value = _languageName;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@languageCulture";
                param[2].DataType = DbType.String;
                param[2].value = _languageCulture;

                _ds = DBAccess.ExecDataSet("Updatelanguagemaster", CommandType.StoredProcedure, param);
                return true;

            }
            catch (Exception ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        public void SelectPasswordByUserId()
        {
            DataAccess obj = new DataAccess();
            obj.Provider = EnumProviders.SQLClient;
            obj.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["connstring"].ToString(); ;
            ParamStruct[] param = new ParamStruct[1];

            param[0].direction = ParameterDirection.Input;
            param[0].ParamName = "@userId";
            param[0].DataType = DbType.String;
            param[0].value = _userId;


            _ds = obj.ExecDataSet("SelectPasswordByUserId", CommandType.StoredProcedure, param);
        }

        public void SelectmanagerByUserId()
        {
            DataAccess obj = new DataAccess();
            obj.Provider = EnumProviders.SQLClient;
            obj.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["connstring"].ToString(); ;
            ParamStruct[] param = new ParamStruct[1];

            param[0].direction = ParameterDirection.Input;
            param[0].ParamName = "@userId";
            param[0].DataType = DbType.String;
            param[0].value = _userId;


            _ds = obj.ExecDataSet("SelectPasswordByUserId", CommandType.StoredProcedure, param);
        }
        public void GetAllmessagebyId()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[4];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@userIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _userIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@userIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _userIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@userType";
                param[2].DataType = DbType.Int32;
                param[2].value = _userType;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@userCompanyId";
                param[3].DataType = DbType.Int32;
                param[3].value = _userCompanyId;

                _ds = DBAccess.ExecDataSet("GetAllmessagebyId", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllState" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void GetAllEmployeebymessge()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[5];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@userIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _userIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@userIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _userIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@userType";
                param[2].DataType = DbType.Int32;
                param[2].value = _userType;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@userCompanyId";
                param[3].DataType = DbType.Int32;
                param[3].value = _userCompanyId;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@userId";
                param[4].DataType = DbType.Int32;
                param[4].value = _userId;

                _ds = DBAccess.ExecDataSet("GetAllEmployeebymessge", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllEmployeebymessge" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        /*saurin |150107| --------------------------------------Get records by NAME : firstname + lastname*/
        public void GetAllEmployeeByName()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@userFullName";
                param[0].DataType = DbType.String;
                param[0].value = _userFullName;


                _ds = DBAccess.ExecDataSet("GetAllEmployeeByName", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllEmployeebyid" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public static List<UserMasterView> GetUserByIdSPL(int id)
        {
            UserBM obj = new UserBM();
            obj.userId = Convert.ToInt32(id);
            obj.GetAllEmployeebyid();
            DataSet ds = obj.ds;

            var userDetailInList = ds.Tables[0].AsEnumerable().Select(r => new UserMasterView
            {
                UserId = r.Field<int>("userId"),
                UserType = r.Field<int>("userType"),
                UserCompanyId = r.Field<int>("userCompanyId"),
                UserFirstName = r.Field<string>("userFirstName"),
                UserLastName = r.Field<string>("userLastName")

            });
            return userDetailInList.ToList(); // For if you really need a List and not IEnumerable

        }

        public void GetEmployeeDetailbyPdf()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[4];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@userIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _userIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@userIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _userIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@userId";
                param[2].DataType = DbType.Int32;
                param[2].value = _userId;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@userCompanyId";
                param[3].DataType = DbType.Int32;
                param[3].value = _userCompanyId;

                _ds = DBAccess.ExecDataSet("GetEmployeeDetailbyPdf", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllEmployeecreatebymanager_ViewPoint" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void GetUserByTeamID()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] pm = new ParamStruct[2];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@TeamID";
                pm[0].DataType = DbType.Int32;
                pm[0].value = _TeamID;


                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@UserIDs";
                pm[1].DataType = DbType.String;
                pm[1].value = _UserIDs;


                _ds = DBAccess.ExecDataSet("GetTeamUserList", CommandType.StoredProcedure, pm);
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in GetUserByTeamID  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            }
        }


        public void GetUserByType_TeamID()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[6];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@userIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _userIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@userIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _userIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@userType";
                param[2].DataType = DbType.Int32;
                param[2].value = _userType;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@userCompanyId";
                param[3].DataType = DbType.Int32;
                param[3].value = _userCompanyId;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@userCreateBy";
                param[4].DataType = DbType.Int32;
                param[4].value = Convert.ToInt32(_userCreateBy);

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@TeamID";
                param[5].DataType = DbType.Int32;
                param[5].value = Convert.ToInt32(_TeamID);

                _ds = DBAccess.ExecDataSet("GetAllEmployee_Type_Team", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetUserByType_TeamID" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        #endregion

        #region NewMethod
        public void SearchUser()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[4];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@userIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _userIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@userIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _userIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@userFirstName";
                param[2].DataType = DbType.String;
                param[2].value = _userFirstName;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@userCompanyId";
                param[3].DataType = DbType.Int32;
                param[3].value = _userCompanyId;

                _ds = DBAccess.ExecDataSet("SpFindUser", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in SpFindUser" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        #endregion

    }
}
