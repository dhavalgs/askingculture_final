using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starteku.Data.Manager;
using System.Data;
using starteku_BusinessLogic;
namespace startetku.Business.Logic
{

    public class QuestionBM
    {
        #region Private Declaration

        private DataSet _ds;
        private string _returnString;
        private bool _returnBoolean;
        private Int32 _queId;
        private Int32 _queComId;
        private DateTime _queCreatedDate;
        private DateTime _queUpdatedDate;
        private Boolean _queIsDeleted;
        private Boolean _queIsActive;
        private Int32 _queCompanyId;
        private String _queCreateBy;
        private Int32 _queDepId;
        private Int32 _queQuestionNo;
        private String _queQuestion;
        private String _quesDescription;
        private String _queDepartmentId;
        private String _queIndId;

        private Int32 _quesCreatedByID;
        private Int32 _userId;
        private Int32 _loginuserId;

        private Int32 _quesCatID;
        private Int32 _SeqNo;
        private String _TemplateIDs;

        private String _quesCategoryName;
        private String _quesCategoryNameDN;

        private Int32 _quesTmpID;
        private String _quesTemplateName;
        private String _quesTemplateNameDN;
        private Boolean _quesIsPublic;
        private Boolean _quesIsMandatory;

        private String _quesTmpIDs;
        private String _quesIDs;

        private Int32 _TeamID;
        private Int32 _assignID;
        private DateTime? _StartDate;
        private DateTime? _EndDate;
        private String _Status;
        private String _UserIDs;

        private String _Answer;

        private Int32 _ToUserID;
        private String _Comment;
        private Int32 _UserType;

        private Int32 _qpercomID;
        private Int32 _Index;
        private Boolean _IsPDPApproved;
        private Boolean _IsDisplayInReport;

        private Int32 _PDPUserID;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public DataSet ds
        {
            get { return _ds; }
            set { _ds = value; }
        }
        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public string ReturnString
        {
            get { return _returnString; }
            set { _returnString = value; }
        }

        /// <summary>
        /// Gets or sets the ReturnBoolean value.
        /// </summary>
        public bool ReturnBoolean
        {
            get { return _returnBoolean; }
            set { _returnBoolean = value; }
        }

        /// <summary>
        /// Gets or sets the queId value.
        /// </summary>
        public Int32 queId
        {
            get { return _queId; }
            set { _queId = value; }
        }

        /// <summary>
        /// Gets or sets the queComId value.
        /// </summary>
        public Int32 queComId
        {
            get { return _queComId; }
            set { _queComId = value; }
        }

        /// <summary>
        /// Gets or sets the queCreatedDate value.
        /// </summary>
        public DateTime queCreatedDate
        {
            get { return _queCreatedDate; }
            set { _queCreatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the queUpdatedDate value.
        /// </summary>
        public DateTime queUpdatedDate
        {
            get { return _queUpdatedDate; }
            set { _queUpdatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the queIsDeleted value.
        /// </summary>
        public Boolean queIsDeleted
        {
            get { return _queIsDeleted; }
            set { _queIsDeleted = value; }
        }

        /// <summary>
        /// Gets or sets the queIsActive value.
        /// </summary>
        public Boolean queIsActive
        {
            get { return _queIsActive; }
            set { _queIsActive = value; }
        }

        /// <summary>
        /// Gets or sets the queCompanyId value.
        /// </summary>
        public Int32 queCompanyId
        {
            get { return _queCompanyId; }
            set { _queCompanyId = value; }
        }

        /// <summary>
        /// Gets or sets the queCreateBy value.
        /// </summary>
        public String queCreateBy
        {
            get { return _queCreateBy; }
            set { _queCreateBy = value; }
        }

        /// <summary>
        /// Gets or sets the queDepId value.
        /// </summary>
        public Int32 queDepId
        {
            get { return _queDepId; }
            set { _queDepId = value; }
        }

        /// <summary>
        /// Gets or sets the queQuestionNo value.
        /// </summary>
        public Int32 queQuestionNo
        {
            get { return _queQuestionNo; }
            set { _queQuestionNo = value; }
        }

        /// <summary>
        /// Gets or sets the queQuestion value.
        /// </summary>
        public String queQuestion
        {
            get { return _queQuestion; }
            set { _queQuestion = value; }
        }


        public String quesDescription
        {
            get { return _quesDescription; }
            set { _quesDescription = value; }
        }

        /// <summary>
        /// Gets or sets the queDepartmentId value.
        /// </summary>
        public String queDepartmentId
        {
            get { return _queDepartmentId; }
            set { _queDepartmentId = value; }
        }

        /// <summary>
        /// Gets or sets the queIndId value.
        /// </summary>
        public String queIndId
        {
            get { return _queIndId; }
            set { _queIndId = value; }
        }
        public Int32 CreatedByID
        {
            get { return _quesCreatedByID; }
            set { _quesCreatedByID = value; }
        }
        public Int32 UserId
        {
            get { return _userId; }
            set { _userId = value; }
        }

        public Int32 loginuserId
        {
            get { return _loginuserId; }
            set { _loginuserId = value; }
        }


        public String quesTemplateID
        {
            get { return _TemplateIDs; }
            set { _TemplateIDs = value; }
        }
        public Int32 SeqNo
        {
            get { return _SeqNo; }
            set { _SeqNo = value; }
        }
        public Int32 quesCatID
        {
            get { return _quesCatID; }
            set { _quesCatID = value; }
        }

        public String quesCategoryName
        {
            get { return _quesCategoryName; }
            set { _quesCategoryName = value; }
        }
        public String quesCategoryNameDN
        {
            get { return _quesCategoryNameDN; }
            set { _quesCategoryNameDN = value; }
        }

        public String quesTemplateName
        {
            get { return _quesTemplateName; }
            set { _quesTemplateName = value; }
        }
        public String quesTemplateNameDN
        {
            get { return _quesTemplateNameDN; }
            set { _quesTemplateNameDN = value; }
        }

        public Boolean quesIsMandatory
        {
            get { return _quesIsMandatory; }
            set { _quesIsMandatory = value; }
        }

        public Boolean quesIsPublic
        {
            get { return _quesIsPublic; }
            set { _quesIsPublic = value; }
        }

        public Int32 quesTmpID
        {
            get { return _quesTmpID; }
            set { _quesTmpID = value; }
        }


        public String quesTmpIDs
        {
            get { return _quesTmpIDs; }
            set { _quesTmpIDs = value; }
        }

        public String quesIDs
        {
            get { return _quesIDs; }
            set { _quesIDs = value; }
        }


        public DateTime? StartDate
        {
            get { return _StartDate; }
            set { _StartDate = value; }
        }
        public DateTime? EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }
        public Int32 AssignID
        {
            get { return _assignID; }
            set { _assignID = value; }
        }
        public String Status
        {
            get { return _Status; }
            set { _Status = value; }
        }
        public Int32 TeamID
        {
            get { return _TeamID; }
            set { _TeamID = value; }
        }
        public String UserIDs
        {
            get { return _UserIDs; }
            set { _UserIDs = value; }
        }

        public String Answer
        {
            get { return _Answer; }
            set { _Answer = value; }
        }
        public String Comment
        {
            get { return _Comment; }
            set { _Comment = value; }
        }
        public Int32 ToUserID
        {
            get { return _ToUserID; }
            set { _ToUserID = value; }
        }
        public Int32 UserType
        {
            get { return _UserType; }
            set { _UserType = value; }
        }


        public Int32 qpercomID
        {
            get { return _qpercomID; }
            set { _qpercomID = value; }
        }
        public Int32 Index
        {
            get { return _Index; }
            set { _Index = value; }
        }
        public Boolean IsPDPApproved
        {
            get { return _IsPDPApproved; }
            set { _IsPDPApproved = value; }
        }

        public Boolean IsDisplayInReport
        {
            get { return _IsDisplayInReport; }
            set { _IsDisplayInReport = value; }
        }

        public Int32 PDPUserID
        {
            get { return _PDPUserID; }
            set { _PDPUserID = value; }
        }
        #endregion

        #region Methods



        public int InsertQuestion()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[10];


                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@queComId";
                param[0].DataType = DbType.Int32;
                param[0].value = _queComId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@queCreatedDate";
                param[1].DataType = DbType.DateTime;

                param[1].value = _queCreatedDate;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@queUpdatedDate";
                param[2].DataType = DbType.DateTime;

                param[2].value = _queUpdatedDate;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@queIsDeleted";
                param[3].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_queIsDeleted.ToString()))
                    param[3].value = DBNull.Value;
                else
                    param[3].value = _queIsDeleted;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@queIsActive";
                param[4].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_queIsActive.ToString()))
                    param[4].value = DBNull.Value;
                else
                    param[4].value = _queIsActive;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@queCompanyId";
                param[5].DataType = DbType.Int32;
                if (_queCompanyId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[5].value = _queCompanyId;
                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@queCreateBy";
                param[6].DataType = DbType.String;
                if (String.IsNullOrEmpty(_queCreateBy))
                    param[6].value = DBNull.Value;
                else
                    param[6].value = _queCreateBy;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@queDepId";
                param[7].DataType = DbType.Int32;
                param[7].value = _queDepId;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@queQuestionNo";
                param[8].DataType = DbType.Int32;
                if (_queQuestionNo == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[8].value = _queQuestionNo;

                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@queQuestion";
                param[9].DataType = DbType.String;
                if (String.IsNullOrEmpty(_queQuestion))
                    param[9].value = DBNull.Value;
                else
                    param[9].value = _queQuestion;


                //obj.ExecScalar("InsertQuestion", CommandType.StoredProcedure, param); _returnBoolean = true;
                _ds = DBAccess.ExecDataSet("InsertQuestion", CommandType.StoredProcedure, param);
                return Convert.ToInt32(_ds.Tables[0].Rows[0][0]);
            }
            catch (DataException ex)
            {
                return 0;
                throw ex;
            }
        }


        /// <summary>
        /// Inserts Records in the Question table.
        /// </summary>
        public void UpdateQuestion()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[4];


                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@queId";
                param[0].DataType = DbType.Int32;
                param[0].value = _queId;


                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@queUpdatedDate";
                param[1].DataType = DbType.DateTime;

                param[1].value = _queUpdatedDate;


                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@queQuestionNo";
                param[2].DataType = DbType.Int32;
                if (_queQuestionNo == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[2].value = _queQuestionNo;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@queQuestion";
                param[3].DataType = DbType.String;
                if (String.IsNullOrEmpty(_queQuestion))
                    param[3].value = DBNull.Value;
                else
                    param[3].value = _queQuestion;


                //  obj.ExecScalar("UpdateQuestion", CommandType.StoredProcedure, param); _returnBoolean = true;
                _ds = DBAccess.ExecDataSet("UpdateQuestion", CommandType.StoredProcedure, param);
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }


        public void GetAllQuestion()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@queIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _queIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@queIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _queIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@queComId";
                param[2].DataType = DbType.Int32;
                param[2].value = _queComId;

                _ds = DBAccess.ExecDataSet("GetAllQuestion", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllQuestion" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void JobTypeQuestionUpdate()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] pm = new ParamStruct[3];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@queId";
                pm[0].DataType = DbType.Int32;
                pm[0].value = _queId;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@queIsActive";
                pm[1].DataType = DbType.Boolean;
                pm[1].value = _queIsActive;

                pm[2].direction = ParameterDirection.Input;
                pm[2].ParamName = "@queIsDeleted";
                pm[2].DataType = DbType.Boolean;
                pm[2].value = _queIsDeleted;

                _ds = DBAccess.ExecDataSet("JobTypeQuestionUpdate", CommandType.StoredProcedure, pm);
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in JobTypeQuestionUpdate  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            }
        }

        public void GetAllQuestionbyid()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@queId";
                param[0].DataType = DbType.Int32;
                param[0].value = _queId;


                _ds = DBAccess.ExecDataSet("GetAllQuestionbyid", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllQuestionbyid" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }



        #endregion

        #region NewQuestion Methods
        public void InsertQuestionData()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[11];



                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@qCatID";
                param[0].DataType = DbType.Int32;
                param[0].value = _quesCatID;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@Question";
                param[1].DataType = DbType.String;
                if (String.IsNullOrEmpty(_queQuestion))
                    param[1].value = DBNull.Value;
                else
                    param[1].value = _queQuestion;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@quesDescription";
                param[2].DataType = DbType.String;
                param[2].value = _quesDescription;


                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@SeqNo";
                param[3].DataType = DbType.Int32;
                param[3].value = _SeqNo;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@CreatedBy";
                param[4].DataType = DbType.Int32;
                param[4].value = _quesCreatedByID;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@CompanyID";
                param[5].DataType = DbType.Int32;
                param[5].value = _queCompanyId;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@TemplateID";
                param[6].DataType = DbType.String;
                if (String.IsNullOrEmpty(_TemplateIDs))
                    param[6].value = DBNull.Value;
                else
                    param[6].value = _TemplateIDs;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@IsActive";
                param[7].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(Convert.ToString(_queIsActive)))
                    param[7].value = DBNull.Value;
                else
                    param[7].value = _queIsActive;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@IsDeleted";
                param[8].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(Convert.ToString(_queIsDeleted)))
                    param[8].value = DBNull.Value;
                else
                    param[8].value = _queIsDeleted;

                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@CreateDate";
                param[9].DataType = DbType.DateTime;

                param[9].value = _queCreatedDate;

                param[10].direction = ParameterDirection.Input;
                param[10].ParamName = "@quesID";
                param[10].DataType = DbType.Int32;
                param[10].value = _queId;

                _ds = DBAccess.ExecDataSet("InsertQuestionData", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        public void InsertQuestionCategoryData()
        {

            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[9];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@qCatId";
                param[0].DataType = DbType.Int32;
                param[0].value = _quesCatID;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@qcatName";
                param[1].DataType = DbType.String;
                param[1].value = _quesCategoryName;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@qcatDescription";
                param[2].DataType = DbType.String;
                if (String.IsNullOrEmpty(_quesDescription))
                    param[2].value = DBNull.Value;
                else
                    param[2].value = _quesDescription;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@qcatNameDN";
                param[3].DataType = DbType.String;
                param[3].value = _quesCategoryNameDN;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@qcatCreatedBy";
                param[4].DataType = DbType.Int32;
                param[4].value = _quesCreatedByID;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@qcatCompanyID";
                param[5].DataType = DbType.Int32;
                param[5].value = _queCompanyId;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@qcatIsActive";
                param[6].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(Convert.ToString(_queIsActive)))
                    param[6].value = DBNull.Value;
                else
                    param[6].value = _queIsActive;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@qcatIsDeleted";
                param[7].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(Convert.ToString(_queIsDeleted)))
                    param[7].value = DBNull.Value;
                else
                    param[7].value = _queIsDeleted;


                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@qcatCreateDate";
                param[8].DataType = DbType.DateTime;
                param[8].value = _queCreatedDate;



                _ds = DBAccess.ExecDataSet("InsertQuestionCategoryData", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                ExceptionLogger.LogException(ex, 0, "_queCreatedDate" + Convert.ToString(_queCreatedDate) + ex.ToString());
                throw ex;
            }
        }

        public void InsertQuestionTemplateData()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[11];



                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@tmpName";
                param[0].DataType = DbType.String;
                param[0].value = _quesTemplateName;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@tmpNameDN";
                param[1].DataType = DbType.String;
                param[1].value = _quesTemplateNameDN;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@tmpDescription";
                param[2].DataType = DbType.String;
                if (String.IsNullOrEmpty(_quesDescription))
                    param[2].value = DBNull.Value;
                else
                    param[2].value = _quesDescription;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@tmpCreatedBy";
                param[3].DataType = DbType.Int32;
                param[3].value = _quesCreatedByID;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@tmpCompanyID";
                param[4].DataType = DbType.Int32;
                param[4].value = _queCompanyId;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@IsActive";
                param[5].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(Convert.ToString(_queIsActive)))
                    param[5].value = DBNull.Value;
                else
                    param[5].value = _queIsActive;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@IsDeleted";
                param[6].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(Convert.ToString(_queIsDeleted)))
                    param[6].value = DBNull.Value;
                else
                    param[6].value = _queIsDeleted;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@IsPublic";
                param[7].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(Convert.ToString(_quesIsPublic)))
                    param[7].value = DBNull.Value;
                else
                    param[7].value = _quesIsPublic;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@IsMandatory";
                param[8].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(Convert.ToString(_quesIsMandatory)))
                    param[8].value = DBNull.Value;
                else
                    param[8].value = _quesIsMandatory;

                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@CreateDate";
                param[9].DataType = DbType.DateTime;

                param[9].value = _queCreatedDate;

                param[10].direction = ParameterDirection.Input;
                param[10].ParamName = "@tmpID";
                param[10].DataType = DbType.Int32;
                param[10].value = _quesTmpID;

                _ds = DBAccess.ExecDataSet("InsertQuestionTemplateData", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        public void GetAllQuestionList()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[10];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@userId";
                param[0].DataType = DbType.Int32;
                param[0].value = _userId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@CompanyID";
                param[1].DataType = DbType.Int32;
                param[1].value = _queCompanyId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@CreatedBy";
                param[2].DataType = DbType.Int32;
                param[2].value = _quesCreatedByID;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@IsActive";
                param[3].DataType = DbType.Boolean;
                param[3].value = _queIsActive;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@IsDeleted";
                param[4].DataType = DbType.Boolean;
                param[4].value = _queIsDeleted;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@quesID";
                param[5].DataType = DbType.Int32;
                param[5].value = _queId;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@QuesCatID";
                param[6].DataType = DbType.Int32;
                param[6].value = _quesCatID;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@QuesTempID";
                param[7].DataType = DbType.Int32;
                param[7].value = _quesTmpID;


                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@StartDate";
                param[8].DataType = DbType.DateTime;
                param[8].value = _StartDate;

                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@EndDate";
                param[9].DataType = DbType.DateTime;
                param[9].value = _EndDate;

                _ds = DBAccess.ExecDataSet("GetQuestionList", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllQuestionList" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }


        public void GetAllQuestionCategoryList()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@qCatID";
                param[0].DataType = DbType.Int32;
                param[0].value = _quesCatID;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@IsActive";
                param[1].DataType = DbType.Boolean;
                param[1].value = _queIsActive;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@IsDeleted";
                param[2].DataType = DbType.Boolean;
                param[2].value = _queIsDeleted;

                _ds = DBAccess.ExecDataSet("GetAllQuestionCategoryList", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllQuestionCategoryList " + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void GetAllQuestionTemplateList()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[5];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@IsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _queIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@IsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _queIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@qTmpID";
                param[2].DataType = DbType.String;
                param[2].value = _quesTmpIDs;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@CompanyID";
                param[3].DataType = DbType.Int32;
                param[3].value = _queCompanyId;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@CreatedBy";
                param[4].DataType = DbType.Int32;
                param[4].value = _quesCreatedByID;

                _ds = DBAccess.ExecDataSet("GetAllQuestionTemplateList", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllQuestionTemplateList " + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }


        public void QuestionCategoryStatusUpdate()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] pm = new ParamStruct[3];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@quesCatID";
                pm[0].DataType = DbType.Int32;
                pm[0].value = _quesCatID;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@queIsActive";
                pm[1].DataType = DbType.Boolean;
                pm[1].value = _queIsActive;

                pm[2].direction = ParameterDirection.Input;
                pm[2].ParamName = "@queIsDeleted";
                pm[2].DataType = DbType.Boolean;
                pm[2].value = _queIsDeleted;

                _ds = DBAccess.ExecDataSet("QuestionCategoryStatusUpdate", CommandType.StoredProcedure, pm);
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in QuestionCategoryStatusUpdate  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            }
        }
        public void QuestionTemplateStatusUpdate()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] pm = new ParamStruct[3];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@qtmpID";
                pm[0].DataType = DbType.Int32;
                pm[0].value = _quesTmpID;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@IsActive";
                pm[1].DataType = DbType.Boolean;
                pm[1].value = _queIsActive;

                pm[2].direction = ParameterDirection.Input;
                pm[2].ParamName = "@IsDeleted";
                pm[2].DataType = DbType.Boolean;
                pm[2].value = _queIsDeleted;

                _ds = DBAccess.ExecDataSet("QuestionTemplateStatusUpdate", CommandType.StoredProcedure, pm);
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in QuestionTemplateStatusUpdate  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            }
        }
        public void QuestionStatusUpdate()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] pm = new ParamStruct[3];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@quesID";
                pm[0].DataType = DbType.Int32;
                pm[0].value = _queId;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@IsActive";
                pm[1].DataType = DbType.Boolean;
                pm[1].value = _queIsActive;

                pm[2].direction = ParameterDirection.Input;
                pm[2].ParamName = "@IsDeleted";
                pm[2].DataType = DbType.Boolean;
                pm[2].value = _queIsDeleted;

                _ds = DBAccess.ExecDataSet("QuestionStatusUpdate", CommandType.StoredProcedure, pm);
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in QuestionStatusUpdate  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            }
        }

        public void GetQuestionByTemplateCategory()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] pm = new ParamStruct[4];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@TmpID";
                pm[0].DataType = DbType.Int32;
                pm[0].value = _quesTmpID;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@CatID";
                pm[1].DataType = DbType.Int32;
                pm[1].value = _quesCatID;

                pm[2].direction = ParameterDirection.Input;
                pm[2].ParamName = "@IsTmpMandatory";
                pm[2].DataType = DbType.Boolean;
                pm[2].value = _quesIsMandatory;

                pm[3].direction = ParameterDirection.Input;
                pm[3].ParamName = "@QuesIDs";
                pm[3].DataType = DbType.String;
                pm[3].value = _quesIDs;

                _ds = DBAccess.ExecDataSet("GetQuestionBy_Temp_Cat", CommandType.StoredProcedure, pm);
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in GetQuestionByTemplateCategory  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            }
        }


        public void InsertQuestionAssignData()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[12];



                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@assignID";
                param[0].DataType = DbType.Int32;
                param[0].value = _assignID;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@UserID";
                param[1].DataType = DbType.Int32;
                param[1].value = _userId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@CompanyID";
                param[2].DataType = DbType.Int32;
                param[2].value = _queCompanyId;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@StartDate";
                param[3].DataType = DbType.DateTime;
                param[3].value = _StartDate;


                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@EndDate";
                param[4].DataType = DbType.DateTime;
                param[4].value = _EndDate;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@IsActive";
                param[5].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(Convert.ToString(_queIsActive)))
                    param[5].value = DBNull.Value;
                else
                    param[5].value = _queIsActive;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@IsDeleted";
                param[6].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(Convert.ToString(_queIsDeleted)))
                    param[6].value = DBNull.Value;
                else
                    param[6].value = _queIsDeleted;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@Status";
                param[7].DataType = DbType.String;
                param[7].value = _Status;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@TemplateID";
                param[8].DataType = DbType.Int32;
                param[8].value = quesTmpID;

                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@QuesIDs";
                param[9].DataType = DbType.String;
                param[9].value = _quesIDs;

                param[10].direction = ParameterDirection.Input;
                param[10].ParamName = "@TeamID";
                param[10].DataType = DbType.Int32;
                param[10].value = _TeamID;

                param[11].direction = ParameterDirection.Input;
                param[11].ParamName = "@UserIDs";
                param[11].DataType = DbType.String;
                param[11].value = _UserIDs;

                _ds = DBAccess.ExecDataSet("InsertQuestionAssign", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        public void GetAllQuestionAssignList()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[4];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@UserID";
                param[0].DataType = DbType.Int32;
                param[0].value = _userId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@CompanyID";
                param[1].DataType = DbType.Int32;
                param[1].value = _queCompanyId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@IsActive";
                param[2].DataType = DbType.Boolean;
                param[2].value = _queIsActive;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@IsDelete";
                param[3].DataType = DbType.Boolean;
                param[3].value = _queIsDeleted;

                _ds = DBAccess.ExecDataSet("GetAllAssignQuestionList", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllQuestionAssignList" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void DeleteQuestionAssign()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@assignID";
                param[0].DataType = DbType.Int32;
                param[0].value = _assignID;


                _ds = DBAccess.ExecDataSet("DeleteQuestionAssign", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in DeleteQuestionAssign" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void UpdateAssignQuestionStatus()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[2];


                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@assignID";
                param[0].DataType = DbType.Int32;
                param[0].value = _assignID;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@Status";
                param[1].DataType = DbType.String;
                param[1].value = _Status;


                _ds = DBAccess.ExecDataSet("UpdateQuestionAssignStatus", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        public void GetPublishQuesUserList()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[2];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@CreatedByID";
                param[0].DataType = DbType.Int32;
                param[0].value = _userId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@CompanyID";
                param[1].DataType = DbType.Int32;
                param[1].value = _queCompanyId;


                _ds = DBAccess.ExecDataSet("GetPublishQuesUserList", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetPublishQuesUserList" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        public void GetPublishUserQuestionList()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[4];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@CreatedByID";
                param[0].DataType = DbType.Int32;
                param[0].value = _userId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@CompanyID";
                param[1].DataType = DbType.Int32;
                param[1].value = _queCompanyId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@assignuserID";
                param[2].DataType = DbType.Int32;
                param[2].value = _assignID;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@Status";
                param[3].DataType = DbType.String;
                param[3].value = _Status;


                _ds = DBAccess.ExecDataSet("GetPublishUserQuestionList", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetPublishUserQuestionList" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void GetAllUserQuestion_PDP()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[4];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@UserID";
                param[0].DataType = DbType.Int32;
                param[0].value = _userId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@CompanyID";
                param[1].DataType = DbType.Int32;
                param[1].value = _queCompanyId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@Status";
                param[2].DataType = DbType.String;
                param[2].value = _Status;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@CurrentDate";
                param[3].DataType = DbType.DateTime;
                param[3].value = _queCreatedDate;

                _ds = DBAccess.ExecDataSet("GetAllUserQuestion_PDP", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetPublishUserQuestionList" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }


        public void InsertQuestionAnswer()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[6];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@UserID";
                param[0].DataType = DbType.Int32;
                param[0].value = _userId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@CompanyID";
                param[1].DataType = DbType.Int32;
                param[1].value = _queCompanyId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@QuestionID";
                param[2].DataType = DbType.Int32;
                param[2].value = _queId;


                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@Answer";
                param[3].DataType = DbType.String;
                param[3].value = _Answer;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@CreateDate";
                param[4].DataType = DbType.DateTime;
                param[4].value = _queCreatedDate;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@UpdateDate";
                param[5].DataType = DbType.DateTime;
                param[5].value = _queUpdatedDate;

                _ds = DBAccess.ExecDataSet("InsertQuestionAnswer", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }
        public void GetQuestionComment()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@UserID";
                param[0].DataType = DbType.Int32;
                param[0].value = _userId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@CompanyID";
                param[1].DataType = DbType.Int32;
                param[1].value = _queCompanyId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@UserIDTo";
                param[2].DataType = DbType.Int32;
                param[2].value = _ToUserID;

                _ds = DBAccess.ExecDataSet("GetQuestionComment", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetQuestionComment " + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void InsertQuestionComment()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[7];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@UserID";
                param[0].DataType = DbType.Int32;
                param[0].value = _userId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@CompanyID";
                param[1].DataType = DbType.Int32;
                param[1].value = _queCompanyId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@ToUserID";
                param[2].DataType = DbType.Int32;
                param[2].value = _ToUserID;


                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@Comment";
                param[3].DataType = DbType.String;
                param[3].value = _Comment;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@UserType";
                param[4].DataType = DbType.Int32;
                param[4].value = _UserType;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@CreateDate";
                param[5].DataType = DbType.DateTime;
                param[5].value = _queCreatedDate;


                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@LoginUserID";
                param[6].DataType = DbType.Int32;
                param[6].value = _loginuserId;

                _ds = DBAccess.ExecDataSet("InsertQuestionComment", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }
        public void InsertQuestionPersonalComment()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[8];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@qpercomID";
                param[0].DataType = DbType.Int32;
                param[0].value = _qpercomID;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@UserID";
                param[1].DataType = DbType.Int32;
                param[1].value = _userId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@CompanyID";
                param[2].DataType = DbType.Int32;
                param[2].value = _queCompanyId;


                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@PersonalComment";
                param[3].DataType = DbType.String;
                param[3].value = _Comment;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@CreateDate";
                param[4].DataType = DbType.DateTime;
                param[4].value = _queCreatedDate;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@IsPDPApproved";
                param[5].DataType = DbType.Boolean;
                param[5].value = _IsPDPApproved;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@IsDisplayInReport";
                param[6].DataType = DbType.Boolean;
                param[6].value = _IsDisplayInReport;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@PDPUserID";
                param[7].DataType = DbType.Int32;
                param[7].value = _PDPUserID;



                _ds = DBAccess.ExecDataSet("InsertQuestionPersonalComment", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        public void GetQuestionPersonalComment()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[6];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@qpercomID";
                param[0].DataType = DbType.Int32;
                param[0].value = _qpercomID;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@UserID";
                param[1].DataType = DbType.Int32;
                param[1].value = _userId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@CompanyID";
                param[2].DataType = DbType.Int32;
                param[2].value = _queCompanyId;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@Index";
                param[3].DataType = DbType.Int32;
                param[3].value = _Index;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@IsDisplayInReport";
                param[4].DataType = DbType.Boolean;
                param[4].value = _IsDisplayInReport;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@LoginUserID";
                param[5].DataType = DbType.Int32;
                param[5].value = _loginuserId;

                _ds = DBAccess.ExecDataSet("GetQuestionPersonalComment", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetQuestionPersonalComment " + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }


        public void UpdateQuestionPersonalCommentStatus()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@qpercomID";
                param[0].DataType = DbType.Int32;
                param[0].value = _qpercomID;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@IsPDPApproved";
                param[1].DataType = DbType.Boolean;
                param[1].value = _IsPDPApproved;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@IsDisplayInReport";
                param[2].DataType = DbType.Boolean;
                param[2].value = _IsDisplayInReport;

                _ds = DBAccess.ExecDataSet("UpdateQuestionPersonalComment", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }
        public void ApproveQuestionPersonalCommentStatus()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[4];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@qpercomID";
                param[0].DataType = DbType.Int32;
                param[0].value = _qpercomID;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@IsPDPApproved";
                param[1].DataType = DbType.Boolean;
                param[1].value = _IsPDPApproved;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@UserID";
                param[2].DataType = DbType.Int32;
                param[2].value = _userId;

                Common.WriteLog("Company ID :" + Convert.ToString(_queCompanyId));

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@CompanyID";
                param[3].DataType = DbType.Int32;
                param[3].value = _queCompanyId;

                _ds = DBAccess.ExecDataSet("Approve_UpdatePersonalComment", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }
        public void AddQuestionTemplate()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[2];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@QuesIDs";
                param[0].DataType = DbType.String;
                param[0].value = _quesIDs;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@TemplateID";
                param[1].DataType = DbType.Int32;
                param[1].value = _quesTmpID;



                _ds = DBAccess.ExecDataSet("AddQuestionsToTemplate", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }
        #endregion

    }
}
