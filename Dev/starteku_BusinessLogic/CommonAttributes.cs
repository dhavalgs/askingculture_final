﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Web;
using WatzOnTV.Logic;
using starteku_BusinessLogic;
using starteku_BusinessLogic.Model;

namespace starteku_BusinessLogic
{
    public static class CommonVariables
    {

    }

    public class CommonAttributes
    {
        public const int MAXVIDEOLIMITPERPROVIDER = 100;
        public const String PlayBackQuality = "UserPlayBackQuality";
        public static int GetMaxVideoCronLimitYoutube()
        {
            var limit = MAXVIDEOLIMITPERPROVIDER;
            try
            {
                limit = Int32.Parse(GetConfigValue("MaxVideoCronLimitYouTube"));
            }
            catch { }
            return limit;

        }
        public static int GetMaxVideoCronLimitYouTubeAccount()
        {
            var limit = MAXVIDEOLIMITPERPROVIDER;
            try
            {
                limit = Int32.Parse(GetConfigValue("MaxVideoCronLimitYouTubeAccount"));
            }
            catch { }
            return limit;

        }
        public static int GetMaxVideoCronLimitAOL()
        {
            var limit = MAXVIDEOLIMITPERPROVIDER;
            try
            {
                limit = Int32.Parse(GetConfigValue("MaxVideoCronLimitAOL"));
            }
            catch { }
            return limit;

        }
        public static string AdsSlugName()
        {
            var tempName = String.Empty;
            try
            {
                tempName = GetConfigValue("AdsSlugName");
            }
            catch { }
            return tempName;

        }
        public static string AdsTrackingURL()
        {
            var tempName = String.Empty;
            try
            {
                tempName = GetConfigValue("AdsTrackingURL");
            }
            catch { }
            return tempName;

        }
        public static int GetMaxVideoCronLimitDailyMotion()
        {
            var limit = MAXVIDEOLIMITPERPROVIDER;
            try
            {
                limit = Int32.Parse(GetConfigValue("MaxVideoCronLimitDailyMotion"));
            }
            catch { }
            return limit;

        }
        public static int GetMaxVideoCronLimitVimeo()
        {
            var limit = MAXVIDEOLIMITPERPROVIDER;
            try
            {
                limit = Int32.Parse(GetConfigValue("MaxVideoCronLimitVimeo"));
            }
            catch { }
            return limit;

        }
        public static int GetMaxVideoCronLimitHulu()
        {
            var limit = MAXVIDEOLIMITPERPROVIDER;
            try
            {
                limit = Int32.Parse(GetConfigValue("MaxVideoCronLimitHulu"));
            }
            catch
            { }
            return limit;

        }


        public static string GetConfigValue(string configKey)
        {
            return GetConfigValue(configKey, 120);
        }

        public static string GetConfigValue(string configKey, int cacheTime, string defaultValue = "1")
        {
            string cacheKey = "GetConfigValue-" + configKey.ToString();
            object cacheItem = null;
            try
            {
                cacheItem = HttpRuntime.Cache[cacheKey] as string;
            }
            catch (Exception)
            {


            }

            if ((cacheItem == null))
            {
                var db = new startetkuEntities1();
                try
                {
                    var optionMaster = db.OptionSwitchers.SingleOrDefault(o => o.keyName == configKey);
                    if (optionMaster != null)
                    {
                        cacheItem = optionMaster.value;
                    }
                    else
                    {
                        ExceptionLogger.LogException(new Exception("Config Key is not defined " + configKey), 0);
                        cacheItem = String.Empty;
                    }
                    HttpRuntime.Cache.Insert(cacheKey, cacheItem, null, DateTime.Now.AddSeconds(60 * cacheTime), TimeSpan.Zero);
                }
                catch (Exception exp)
                {
                    ExceptionLogger.LogException(exp);
                }
                finally
                {
                    // CommonAttributes.DisposeDBObject(ref db);
                }
            }

            if (String.IsNullOrWhiteSpace(Convert.ToString(cacheItem)))
            {
                return defaultValue;
            }
            return (string)cacheItem;
        }
        #region "Send Email"
        public enum TypeOfEmails
        {
            VideoDailLog,
            ForgotPassword,
            CronDailyLog,
            ErrorLog,
            Feedback,
            NoVideosFound,
            EmailAuthentication,
            CritialError,
            YoutubeUserHisory,// Saurin : 20140122 : Send email users youtube videos , which we dont have in our database
            SendMailForTooLongTimeFirstLoad, // Saurin : 20131217
            SendMailForTooLongTime,
            SendMailForTooLongTimeFirstLoadMobile, // Saurin : 20131217
            SendMailForTooLongTimeMobile

        }
        //20131108
        public enum UserMasterMeta
        {
            EmailAuthenticationCode,
            IsEmailVerified,
            HeardFrom,
            GoogleAcToken, // saurin : 20140118 : added  during Google/Fb login code
            FacebookAcToken,
            SignInVia,
            YoutubeAcToken, // saurin : 20140122 :Added while coding for Youtube history view 
            IsTuneYourChannelInProgress,

        }

        //saurin : 20140313 
        public enum ChannelLog
        {
            ChannelDeleted,
            UpdateChannelToAdmin
        }







        public static int GetGlobalNewCheckMinutes()
        {
            try
            {
                return Int32.Parse(GetConfigValue("GlobalNewCheckMinutes"));
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return 15;
            }
        }
        public static string GetFbPageId()
        {
            try
            {
                return GetConfigValue("FBPageId");
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return null;
            }
        }

        public static string GetIPAddressApiKey()
        {
            try
            {
                return GetConfigValue("IPAddressApiKey");
            }
            catch (Exception ex)
            {

                ExceptionLogger.LogException(ex);
                return null;
            }
        }

        public static string AllowFBPosting()
        {
            try
            {
                return GetConfigValue("AllowFBPosting");
            }
            catch (Exception ex)
            {
                //ExceptionLogger.LogException(ex);
                return null;
            }
        }
        public static string GetFbURL()
        {
            try
            {
                return GetConfigValue("FBWallURL");
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return null;
            }

        }
        public static string GetFbAppId()
        {
            try
            {
                return GetConfigValue("FBAppId");
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return null;
            }

        }
        public static string GetFbToken()
        {
            try
            {
                return GetConfigValue("FBAccessToken");
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return null;
            }

        }

        public static bool SendFeedback(string firstName, string lastName, string emailAddress, string message, string platform)
        {
            try
            {
                var fromEmailAddress = GetConfigValue("SMTPFromEmailAddress");
                var fromEmailName = GetConfigValue("SMTPFromEmailName");
                var sendToEmail = GetConfigValue("ContactusEmailAddress");
                var subject = "";
                var emailbody = "";
                sbyte isLogToDropbox = 0;
                GetEmailTemplate(TypeOfEmails.Feedback, out subject, out emailbody, out isLogToDropbox);
                subject = FormatEmail(subject, "", firstName, lastName, emailAddress, message, platform, "");  //FormateFeedbackTemplate(subject, firstName, lastName, emailAddress, message, platform);
                emailbody = FormatEmail(emailbody, "", firstName, lastName, emailAddress, message, platform, ""); //FormateFeedbackTemplate(emailbody, firstName, lastName, emailAddress, message, platform);
                return SendEmail(fromEmailAddress, fromEmailName, sendToEmail, "", emailAddress, subject, emailbody, isLogToDropbox);
            }
            catch (Exception)
            {
                return false;
            }
        }
        //saurin :20140304  SendErrorEmail : not used anywhere in watzOnTV Web
        public static void SendErrorEmail(string message, string emailBody)
        {
            try
            {
                if (message.ToLower().Contains("thread was being aborted")) return;
                var fromEmailAddress = GetConfigValue("SMTPFromEmailAddress");
                var fromEmailName = GetConfigValue("SMTPFromEmailName");
                string subject = "Error on WatzOnTV -" + message;
                string body = String.Empty;
                body += emailBody;
                SendEmail(fromEmailAddress, fromEmailName, "rajendradewani@gmail.com", "", subject, body, 0);
            }
            catch (Exception)
            {

            }
        }
        //saurin :20140304  SendEmailMissingDetails : not used anywhere in watzOnTV Web
        public static void SendEmailMissingDetails(string title, string htmlPageSource)
        {
            try
            {
                var fromEmailAddress = GetConfigValue("SMTPFromEmailAddress");
                var fromEmailName = GetConfigValue("SMTPFromEmailName");
                var cc = GetConfigValue("AdminEmailAddress");
                string subject = "Attention: Missing Details in WatzOnTV " + title;
                string body = String.Empty;
                body += htmlPageSource;
                SendEmail(fromEmailAddress, fromEmailName, "rajendradewani@gmail.com", cc, subject, body, 0);
            }
            catch (Exception ex)
            {

            }
        }


        public static void SendEmail1(TypeOfEmails typeOfEmails, string emailbody, string to, string cc, string htmlPageSource, int userId)
        {
            try
            {

                var fromEmailAddress = GetConfigValue("SMTPFromEmailAddress");
                var fromEmailName = GetConfigValue("SMTPFromEmailName");
                string ccAdmin;

                ccAdmin = GetConfigValue("AdminEmailAddress");
                if (!String.IsNullOrEmpty(ccAdmin))
                {
                    if (!String.IsNullOrEmpty(cc))
                    {
                        cc += ";";
                        cc += ccAdmin;
                    }
                    else
                    {
                        cc = ccAdmin;
                    }
                }
                if (String.IsNullOrEmpty(to) || to.Trim().Length == 0)
                {
                    to = cc;
                    cc = "";
                }
                var fullName = "";
                if (userId > 0) //Added on 20140207 :Saurin. 
                {
                    //fullName = ManageUser.getUserById(userId, false).FullName;
                }
                string subject = String.Empty;
                string body = String.Empty;
                sbyte isLogToDropbox = 0;
                GetEmailTemplate(typeOfEmails, out subject, out body, out isLogToDropbox);

                subject = FormatEmail(subject, emailbody, fullName, "", "", "", "", htmlPageSource);
                subject = subject.Replace("/", "-"); //saurin : remove slash from email subject.
                subject = subject.Replace("\\", "-"); //saurin : remove slash from email subject.
                subject = subject.Replace(":", "-"); //saurin : remove slash from email subject.
                body = FormatEmail(body, emailbody, fullName, "", "", "", "", htmlPageSource); //FormatEmail(body, emailbody, htmlPageSource);

                switch (typeOfEmails)
                {
                    case TypeOfEmails.VideoDailLog:
                        SendEmail(fromEmailAddress, fromEmailName, to, cc, subject, body, isLogToDropbox);
                        break;
                    case TypeOfEmails.ForgotPassword:
                        SendEmail(fromEmailAddress, fromEmailName, to, "", subject, body, isLogToDropbox);
                        break;
                    case TypeOfEmails.CronDailyLog:
                        SendEmail(fromEmailAddress, fromEmailName, to, cc, subject, body, isLogToDropbox);
                        break;
                    case TypeOfEmails.ErrorLog:
                        SendEmail(fromEmailAddress, fromEmailName, to, cc, subject, body, isLogToDropbox);
                        break;
                    case TypeOfEmails.NoVideosFound:
                        SendEmail(fromEmailAddress, fromEmailName, to, cc, subject, body, isLogToDropbox);
                        break;
                    case TypeOfEmails.YoutubeUserHisory: //saurin : 20140122
                        SendEmail(fromEmailAddress, fromEmailName, to, cc, subject, body, isLogToDropbox);
                        break;
                }
            }

            catch (Exception ex)
            {


            }

        }





        /*  public static void SendEmailOldCode(TypeOfEmails typeOfEmails, string emailbody, string to, string cc, string htmlPageSource)
          {
              try
              {

                  var fromEmailAddress = GetConfigValue("SMTPFromEmailAddress");
                  var fromEmailName = GetConfigValue("SMTPFromEmailName");
                  string ccAdmin;

                  ccAdmin = GetConfigValue("AdminEmailAddress");
                  if (!string.IsNullOrEmpty(ccAdmin))
                  {
                      if (!string.IsNullOrEmpty(cc))
                      {
                          cc += ";";
                          cc += ccAdmin;
                      }
                      else
                      {
                          cc = ccAdmin;
                      }
                  }
                  if (string.IsNullOrEmpty(to) || to.Trim().Length == 0)
                  {
                      to = cc;
                      cc = "";
                  }
                  string subject = string.Empty;
                  string body = string.Empty;
                  GetEmailTemplate(typeOfEmails, out subject, out body);

                  subject = FormatEmail(subject, emailbody, "", "", "", "", "", htmlPageSource); // FormatEmail(subject, emailbody, htmlPageSource);
                 // subject = subject.Replace("\\", " ");
                  body = FormatEmail(body, emailbody, "", "", "", "", "", htmlPageSource); //FormatEmail(body, emailbody, htmlPageSource);

                  switch (typeOfEmails)
                  {
                      case TypeOfEmails.VideoDailLog:
                          SendEmail(fromEmailAddress, fromEmailName, to, cc, subject, body);
                          break;
                      case TypeOfEmails.ForgotPassword:
                          SendEmail(fromEmailAddress, fromEmailName, to, "", subject, body);
                          break;
                      case TypeOfEmails.CronDailyLog:
                          SendEmail(fromEmailAddress, fromEmailName, to, cc, subject, body);
                          break;
                      case TypeOfEmails.ErrorLog:
                          SendEmail(fromEmailAddress, fromEmailName, to, cc, subject, body);
                          break;
                      case TypeOfEmails.NoVideosFound:
                          SendEmail(fromEmailAddress, fromEmailName, to, cc, subject, body);
                          break;
                      case TypeOfEmails.YoutubeUserHisory: //saurin : 20140122
                          SendEmail(fromEmailAddress, fromEmailName, to, cc, subject, body);
                          break;
                  }
              }

              catch (Exception ex)
              {


              }

          }*/
        private static string FormatEmail(string body, string emailbody, string firstName, string lastName, string emailAddress, string message, string platform, string htmlPageSource)
        {
            try
            {
                if (String.IsNullOrEmpty(body))
                {
                    return String.Empty;
                }
                body = body.Replace("{CurrentDateTimeUTC}", CommonUtilities.GetCurrentDateTime().ToString("yyy-MM-dd hh:mm:ss") + " - UTC");
                body = body.Replace("{CurrentDateUTC}", CommonUtilities.GetCurrentDateTime().ToString("yyy-MM-dd") + " - UTC");
                body = body.Replace("{CurrentDateTime}", CommonUtilities.GetCurrentDateTime().ToString("yyy-MM-dd hh:mm:ss") + "");
                body = body.Replace("{CurrentDate}", CommonUtilities.GetCurrentDateTime().ToString("yyy-MM-dd") + "");
                body = body.Replace("{CurrentTime}", CommonUtilities.GetCurrentDateTime().ToString("hh_mm_ss") + "");
                body = body.Replace("{ServerCurrentDateTime}", DateTime.Now.ToString("yyy-MM-dd hh:mm:ss") + "");
                body = body.Replace("{ServerCurrentDate}", DateTime.Now.ToString("yyy-MM-dd") + "");
                body = body.Replace("{emailbody}", emailbody);
                body = body.Replace("{htmlPageSource}", htmlPageSource);
                body = body.Replace("{resetlink}", emailbody);

                body = body.Replace("{CurrentDateTime}", CommonUtilities.GetCurrentDateTime().ToString("yyy-MM-dd hh:mm:ss") + "");
                body = body.Replace("{CurrentDate}", CommonUtilities.GetCurrentDateTime().ToString("yyy-MM-dd") + "");
                body = body.Replace("{ServerCurrentDateTime}", DateTime.Now.ToString("yyy-MM-dd hh:mm:ss") + "");
                body = body.Replace("{ServerCurrentDate}", DateTime.Now.ToString("yyy-MM-dd") + "");
                body = body.Replace("{FirstName}", firstName);
                body = body.Replace("{LastName}", lastName);
                body = body.Replace("{FromEmail}", emailAddress);
                body = body.Replace("{Message}", message);
                body = body.Replace("{Platform}", platform);


                return body;
            }
            catch (Exception ex) { }
            body = body.Replace("\n", "<br />");
            return body;
        }
        public static bool SendEmailwithAttachment(string fromEmail, string fromEmailName, string to, string cc, string replyTo, string subject, string body, string attachmentPath)
        {

            if (to.StartsWith("test"))
                return false;
            //remove return after UAT is finished
            //return;
            // Instantiate a new instance of MailMessage
            var mMailMessage = new MailMessage();

            mMailMessage.From = new MailAddress(fromEmail, fromEmailName);
            // Set the recepient address of the mail message
            to = to.Replace(",", ";");
            cc = cc.Replace(",", ";");
            string[] toEmails = (to + ";").Split(';');
            foreach (var email in toEmails)
            {
                if (!String.IsNullOrEmpty(email))
                {
                    mMailMessage.To.Add(new MailAddress(email));
                }
            }

            string[] ccEmails = (cc + ";").Split(';');
            foreach (var ccEmail in ccEmails)
            {
                if (!String.IsNullOrEmpty(ccEmail))
                {
                    mMailMessage.CC.Add(new MailAddress(ccEmail));
                }
            }
            if (!String.IsNullOrEmpty(replyTo))
            {

                mMailMessage.ReplyToList.Add(new MailAddress(replyTo)); ;
            }
            // Set the subject of the mail message
            mMailMessage.Subject = subject;
            mMailMessage.IsBodyHtml = true;
            // Set the body of the mail message
            mMailMessage.Body = body;

            // Instantiate a new instance of SmtpClient
            SmtpClient mSmtpClient = new SmtpClient();
            mSmtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            var port = GetConfigValue("SMTPPort");
            if (!String.IsNullOrEmpty(port) && port != "0")
            {
                mSmtpClient.Port = Int32.Parse(port);
            }

            if (!String.IsNullOrEmpty(attachmentPath))
            {
                Attachment attachment = new Attachment(attachmentPath);
                mMailMessage.Attachments.Add(attachment);

                mSmtpClient.UseDefaultCredentials = Boolean.Parse(GetConfigValue("SMTPUseDefaultCredentials"));
                mSmtpClient.Credentials = new NetworkCredential
                    (GetConfigValue("SMTPUserName"), GetConfigValue("SMTPPassword"));

                mSmtpClient.EnableSsl = Boolean.Parse(GetConfigValue("SMTPEnableSsl"));
                mSmtpClient.Host = GetConfigValue("SMTPHost");

                // Send the mail message
                mSmtpClient.Send(mMailMessage);
                attachment.Dispose();
            }
            return true;
        }

        public static bool SendEmail(string fromEmail, string fromEmailName, string to, string cc, string subject, string body, sbyte isLogToDropbox)
        {
            return SendEmail(fromEmail, fromEmailName, to, cc, "", subject, body, isLogToDropbox);
        }

        /// <summary>
        /// Sends an mail message
        /// </summary>
        /// <param name="to">Recepient address</param>
        /// <param name="subject">Subject of mail message</param>
        /// /// <param name="template">Email body template</param>
        /// <param name="values">Email's tokens in pairs: key + value + key + value...</param>
        /// <param name="isLogToDropbox">Send mail to dropbox</param>
        public static bool SendEmail(string fromEmail, string fromEmailName, string to, string cc, string replyTo, string subject, string body, sbyte isLogToDropbox)
        {
            try
            {
                //saurin : 20140304 : if LogToDropbox=1 then send mail to dropbox
                var SMTPUserName = GetConfigValue("SMTPUserName");
                if (isLogToDropbox == 1)
                {
                    /*
                    SendEmail(fromEmail, fromEmailName, to, cc, replyTo, subject,
                              "For Detail Report Please refer DropBox - Thank You", 0);
                    //this : Log to dropbox
                    to = GetConfigValue("DailyLogToDropbox");
                    cc = GetConfigValue("AdminEmailAddress");
                     * */
                    to += ";" + GetConfigValue("DailyLogToDropbox");
                    fromEmail = GetConfigValue("SMTPFromEmailAddressLog");
                    fromEmailName = GetConfigValue("SMTPFromEmailNameLog");
                    //SMTPUserName = GetConfigValue("SMTPUserNameLog");
                }


                //TODO:HACK:
                if (to.StartsWith("test"))
                    return false;
                //remove return after UAT is finished
                //return;
                // Instantiate a new instance of MailMessage
                var mMailMessage = new MailMessage();

                mMailMessage.From = new MailAddress(fromEmail, fromEmailName);
                // Set the recepient address of the mail message
                to = to.Replace(",", ";");
                cc = cc.Replace(",", ";");
                string[] toEmails = (to + ";").Split(';');
                foreach (var email in toEmails)
                {
                    if (!String.IsNullOrEmpty(email))
                    {
                        mMailMessage.To.Add(new MailAddress(email));
                    }
                }

                string[] ccEmails = (cc + ";").Split(';');
                foreach (var ccEmail in ccEmails)
                {
                    if (!String.IsNullOrEmpty(ccEmail))
                    {
                        mMailMessage.CC.Add(new MailAddress(ccEmail));
                    }
                }
                if (!String.IsNullOrEmpty(replyTo))
                {

                    mMailMessage.ReplyToList.Add(new MailAddress(replyTo)); ;
                }
                // Set the subject of the mail message
                mMailMessage.Subject = subject;
                mMailMessage.IsBodyHtml = true;
                // Set the body of the mail message
                mMailMessage.Body = body;

                // Instantiate a new instance of SmtpClient
                SmtpClient mSmtpClient = new SmtpClient();
                mSmtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                var port = GetConfigValue("SMTPPort");
                if (!String.IsNullOrEmpty(port) && port != "0")
                {
                    mSmtpClient.Port = Int32.Parse(port);
                }
                mSmtpClient.UseDefaultCredentials = Boolean.Parse(GetConfigValue("SMTPUseDefaultCredentials"));
                mSmtpClient.Credentials = new NetworkCredential
                 (SMTPUserName, GetConfigValue("SMTPPassword"));

                mSmtpClient.EnableSsl = Boolean.Parse(GetConfigValue("SMTPEnableSsl"));
                mSmtpClient.Host = GetConfigValue("SMTPHost");

                // Send the mail message
                mSmtpClient.Send(mMailMessage);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public static void GetEmailTemplate(TypeOfEmails templateName, out string subject, out string body, out sbyte isLogToDropbox)
        {
            var templateResult = new List<string>();

            body = "";
            subject = templateName.ToString();
            isLogToDropbox = 0;
            //saurin : 20140304
            //isLogToDropbox = 0 : Regular email
            //isLogToDropbox = 1 : Log to Dropbox
            string cacheKey = "GetEmailTemplate-{0}" + templateName.ToString();
            object cacheItem = HttpRuntime.Cache[cacheKey] as List<string>;
            if ((cacheItem == null))
            {
                var db = new startetkuEntities1();
                try
                {


                    var templateNameInString = templateName.ToString();
                    //var optionMaster = db.emailtemplates.SingleOrDefault(o => o.EmailTemplateKey == templateNameInString);


                    //if (optionMaster != null)
                    //{
                    //    body = optionMaster.EmailTemplateValue;
                    //    subject = optionMaster.EmailTemplateSubject;
                    //    isLogToDropbox = optionMaster.LogToDropbox;

                    //}
                    templateResult.Clear();
                    templateResult.Add(body);
                    templateResult.Add(subject);
                    cacheItem = templateResult;
                    HttpRuntime.Cache.Insert(cacheKey, cacheItem, null, DateTime.Now.AddSeconds(60 * 120), TimeSpan.Zero);
                }

                catch (Exception exp)
                {
                    ExceptionLogger.LogException(exp);
                }
                finally
                {
                    // CommonAttributes.DisposeDBObject(ref db);
                }
            }
            else
            {
                templateResult = HttpRuntime.Cache[cacheKey] as List<string>;
                if (templateResult != null && templateResult.Count >= 2)
                {
                    body = templateResult[0];
                    subject = templateResult[1];
                }
            }



        }
        #endregion
        public static void RemoveCacheOnChannelCron(int userID)
        {
            try
            {
                if (HttpRuntime.Cache == null) return;
                if (HttpRuntime.Cache.Count > 0)
                {
                    IDictionaryEnumerator enumerator = HttpRuntime.Cache.GetEnumerator();

                    while (enumerator.MoveNext())
                    {
                        try
                        {
                            if (enumerator.Key.ToString().ToLower().Contains(("userID-" + userID).ToLower()) && !(enumerator.Key.ToString().ToLower().Contains("userID-0-".ToLower())))
                            {
                                RemoveCache(enumerator.Key.ToString());
                            }
                        }
                        catch (Exception)
                        {


                        }


                    }
                }
            }
            catch (Exception)
            {


            }
        }

        public static void RemoveAllCache()
        {
            try
            {
                if (HttpRuntime.Cache == null) return;
                if (HttpRuntime.Cache.Count > 0)
                {
                    IDictionaryEnumerator enumerator = HttpRuntime.Cache.GetEnumerator();

                    while (enumerator.MoveNext())
                    {
                        try
                        {
                            RemoveCache(enumerator.Key.ToString());
                        }
                        catch (Exception)
                        {


                        }


                    }
                }
            }
            catch (Exception)
            {


            }
        }
        public static void RemoveCacheContains(string cacheKey)
        {
            try
            {
                if (HttpRuntime.Cache == null) return;
                if (HttpRuntime.Cache.Count > 0)
                {
                    IDictionaryEnumerator enumerator = HttpRuntime.Cache.GetEnumerator();

                    while (enumerator.MoveNext())
                    {
                        try
                        {
                            if (enumerator.Key.ToString().ToLower().Contains(cacheKey.ToLower()))
                            {
                                RemoveCache(enumerator.Key.ToString());
                            }
                        }
                        catch (Exception)
                        {


                        }


                    }
                }
            }
            catch (Exception)
            {


            }
        }

        public static void RemoveCache(string cacheKey)
        {
            try
            {
                HttpRuntime.Cache.Remove(cacheKey);
            }
            catch (Exception)
            {


            }
            try
            {
                if (HttpContext.Current != null)
                    HttpContext.Current.Cache.Remove(cacheKey);

            }
            catch (Exception)
            {


            }

        }

        internal static int GetMaxLimitOfChannelToCron()
        {

            var maxLimit = "0";// MaxLimitOfChannelToCron();
            if (String.IsNullOrEmpty(maxLimit))
                return 1;
            else
            {
                return Int32.Parse(maxLimit);
            }
        }




        public static int GetGlobalWatzOnTVDJChannelCheckMinutes()
        {
            try
            {
                return Int32.Parse(GetConfigValue("GlobalWatzOnTVDJChannelCheckMinutes"));
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return 60;
            }
        }




        #region
        //saurin :20131214: mail send if channel change take time more than 20 sec. Task:  100 timeout message

        public static bool SendMailForCritialError(string errorTitle, int userID, Exception exception, string additionInfo)
        {
            //check if critial email of same title already sent in last 5 minutes then do not send 
            string cacheKey = String.Format("SendMailForCritialError");
            object cacheItem = HttpRuntime.Cache[cacheKey] as string;
            if ((cacheItem == null) ||
                !cacheItem.ToString().ToLower().Contains(String.Format("[{0}]", errorTitle.ToLower().ToString())))
            {
                if (cacheItem == null) cacheItem = String.Empty;
                cacheItem += String.Format("[{0}],", errorTitle.ToString());
                HttpRuntime.Cache.Insert(cacheKey, cacheItem, null, DateTime.Now.AddSeconds(60 * 5),
                                         TimeSpan.Zero);

                var db = new startetkuEntities1();

                try
                {

                    var ipAddress = CommonUtilities.GetIPAddress();
                    var country = "N/A";
                    var city = "N/A";
                    try
                    {
                        var ipDetail = CommonUtilities.GetIPAddress();

                        //if (ipDetail != null)
                        //{
                        //    city = ipDetail.city_name;
                        //    country = ipDetail.country_name;
                        //}
                    }
                    catch (Exception)
                    {


                    }
                    var userFullName = "Guest";
                    var userEmail = "N/A";
                    var userOrGuestId = "" + userID;
                    if (userID > 0)
                    {
                        try
                        {
                            var userDetail = UserBM.GetUserByIdLinq(userID);
                            userFullName = userDetail.userFirstName;
                            userEmail = userDetail.userEmail;

                        }
                        catch (Exception)
                        {


                        }

                    }
                    var subject = "";
                    var emailbody = "";

                    CmsBM objMail = new CmsBM();
                    objMail.cmsName = "ErrorMail";
                    objMail.SelectMailTemplateByName();
                    DataSet dsMail = objMail.ds;
                    if (dsMail.Tables[0].Rows.Count > 0)
                    {
                        string confirmMail = dsMail.Tables[0].Rows[0]["cmsDescription"].ToString();
                        emailbody = confirmMail;

                    }

                    emailbody = emailbody.Replace("{UserID}", userOrGuestId);
                    emailbody = emailbody.Replace("{userFullName}", userFullName);
                    emailbody = emailbody.Replace("{userEmailAddress}", userEmail);

                    emailbody = emailbody.Replace("{errorTitle}", errorTitle);
                    emailbody = emailbody.Replace("{Title}", errorTitle);
                    if (exception != null)
                    {
                        emailbody = emailbody.Replace("{StackTrace}", exception.StackTrace);
                        emailbody = emailbody.Replace("{Message}", exception.Message);
                        emailbody = emailbody.Replace("{Source}", exception.Source);
                        emailbody = emailbody.Replace("{HelpLink}", exception.HelpLink);
                        emailbody = emailbody.Replace("{TargetSite}", exception.TargetSite.Name);

                        if (exception.InnerException != null)
                        {
                            emailbody = emailbody.Replace("{InnerException}", exception.InnerException.ToString());
                            emailbody = emailbody.Replace("{InnerException.StackTrace}",
                                exception.InnerException.StackTrace);
                        }
                    }

                    emailbody = emailbody.Replace("{ipAddress}", ipAddress);
                    emailbody = emailbody.Replace("{city}", city);
                    emailbody = emailbody.Replace("{country}", country);
                    emailbody = emailbody.Replace("{additionInfo}", additionInfo);


                    subject = "ERROR !!! " + ExceptionLogger.GetExceptionTypeStack(exception);

                    return SendEmail(
                        GetConfigValue("AlertEmailList"), "", "", subject, emailbody);



                }
                catch (Exception ex)
                {
                    ExceptionLogger.LogException(ex);
                    return false;
                }
                finally
                {
                    db.Dispose();
                }
            }
            return true;
        }

        public static bool SendEmail(string to, string cc, string replyTo, string subject, string body)
        {
            try
            {
                //saurin : 20140304 : if LogToDropbox=1 then send mail to dropbox
                var SMTPUserName = CommonUtilities.GetConfigValue("SMTPUserName");

                //to += ";" + CommonUtilities.GetConfigValue("DailyLogToDropbox");
                var fromEmail = CommonUtilities.GetConfigValue("SMTPFromEmailAddressLog");
                var fromEmailName = CommonUtilities.GetConfigValue("SMTPFromEmailNameLog");



                //remove return after UAT is finished
                //return;
                // Instantiate a new instance of MailMessage
                var mMailMessage = new MailMessage();

                mMailMessage.From = new MailAddress(fromEmail, fromEmailName);
                // Set the recepient address of the mail message
                to = to.Replace(",", ";");
                cc = cc.Replace(",", ";");
                string[] toEmails = (to + ";").Split(';');
                foreach (var email in toEmails)
                {
                    if (!String.IsNullOrEmpty(email))
                    {
                        mMailMessage.To.Add(new MailAddress(email));
                    }
                }

                string[] ccEmails = (cc + ";").Split(';');
                foreach (var ccEmail in ccEmails)
                {
                    if (!String.IsNullOrEmpty(ccEmail))
                    {
                        mMailMessage.CC.Add(new MailAddress(ccEmail));
                    }
                }
                if (!String.IsNullOrEmpty(replyTo))
                {

                    mMailMessage.ReplyToList.Add(new MailAddress(replyTo)); ;
                }
                // Set the subject of the mail message
                mMailMessage.Subject = subject;
                mMailMessage.IsBodyHtml = true;
                // Set the body of the mail message
                mMailMessage.Body = body;

                // Instantiate a new instance of SmtpClient
                SmtpClient mSmtpClient = new SmtpClient();
                mSmtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                var port = CommonUtilities.GetConfigValue("SMTPPort");
                if (!String.IsNullOrEmpty(port) && port != "0")
                {
                    mSmtpClient.Port = Int32.Parse(port);
                }
                mSmtpClient.UseDefaultCredentials = Boolean.Parse(CommonUtilities.GetConfigValue("SMTPUseDefaultCredentials"));
                mSmtpClient.Credentials = new System.Net.NetworkCredential
                    (SMTPUserName, CommonUtilities.GetConfigValue("SMTPPassword"));

                mSmtpClient.EnableSsl = Boolean.Parse(CommonUtilities.GetConfigValue("SMTPEnableSsl"));
                mSmtpClient.Host = CommonUtilities.GetConfigValue("SMTPHost");

                // Send the mail message
                mSmtpClient.Send(mMailMessage);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        #endregion
        //saurin :20131214: mail send if channel change take time more than 20 sec. Task:  100 timeout message
        public static bool SendMailForTooLongTime(int channelId, int userID, string guestID, bool isQuickChannel, bool isFirstLoad, string errortitle)
        {
            return true;// SendMailForTooLongTime(channelId, userID, guestID, isQuickChannel, false, isFirstLoad, "Web", String.Empty, String.Empty, errortitle);
        }
        /*Added By Manish for timout from Mobile on 20140113*/


        public static bool AllowTwitterPostingOnChannelCreate()
        {
            try
            {
                string result = GetConfigValue("AllowTwitterPostingOnChannelCreate");
                if (String.IsNullOrEmpty(result)) return false;
                if (result.ToLower() == "true" || result.ToLower() == "on" || result.ToLower() == "yes") return true;
                return false;
            }
            catch (Exception ex)
            {
                //ExceptionLogger.LogException(ex);
                return false;
            }
        }
        public static bool AllowFBPostingOnChannelCreate()
        {
            try
            {
                string result = GetConfigValue("AllowFBPostingOnChannelCreate");
                if (String.IsNullOrEmpty(result)) return false;
                if (result.ToLower() == "true" || result.ToLower() == "on" || result.ToLower() == "yes") return true;
                return false;
            }
            catch (Exception ex)
            {
                //ExceptionLogger.LogException(ex);
                return false;
            }
        }
        //Saurin : 20131224 : Twitter: getting values from option table 

        public static string AllowTwitterPosting()
        {
            try
            {
                return GetConfigValue("AllowTwitterPosting");
            }
            catch (Exception ex)
            {
                //ExceptionLogger.LogException(ex);
                return null;
            }
        }
        public static string GetTwitterConsumerSecret()
        {
            try
            {
                return GetConfigValue("TwitterConsumerSecret");
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex, 0);
                return null;
            }

        }

        public static string GetTwitterConsumerKey()
        {
            try
            {
                return GetConfigValue("TwitterConsumerKey");
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return null;
            }

        }

        public static string GetTwitterAccessToken()
        {
            try
            {
                return GetConfigValue("TwitterAccessToken");
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return null;
            }

        }

        public static string GetTwitterAccessTokenSecret()
        {
            try
            {
                return GetConfigValue("TwitterAccessTokenSecret");
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return null;
            }

        }
        public static string GetCountriesExcludedForDelayEmail()
        {
            try
            {
                return GetConfigValue("CountriesExcludedForDelayEmail");
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return null;
            }

        }
        //saurin: 20140328 :  100 Timeout error should be for USA users only / CANADA too.
        public static string GetCountriesIncludedForDelayEmail()
        {
            try
            {
                return GetConfigValue("CountriesIncludedForDelayEmail");
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return null;
            }

        }

        public static string GetTweetText()
        {
            try
            {
                return GetConfigValue("TweetText");
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return null;
            }

        }

        // Saurin : 20131224 : make tinyURL :  
        //Term of use: http://tinyurl.com/#termsTinyURL >> was created as a free service to make posting long URLs easier,This service is provided without warranty of any kind.

        public static string ToTinyUrl(string txt)
        {
            Regex regx = new Regex("http://([\\w+?\\.\\w+])+([a-zA-Z0-9\\~\\!\\@\\#\\$\\%\\^\\&amp;\\*\\(\\)_\\-\\=\\+\\\\\\/\\?\\.\\:\\;\\'\\,]*)?", RegexOptions.IgnoreCase);
            try
            {
                MatchCollection mactches = regx.Matches(txt);
                foreach (Match match in mactches)
                {
                    string tURL = MakeTinyUrl(match.Value);
                    txt = txt.Replace(match.Value, tURL);
                }
            }
            catch (Exception ex)
            {

                ExceptionLogger.LogException(ex);
            }
            return txt;
        }

        public static string MakeTinyUrl(string url)
        {

            try
            {
                if (url.Length <= 12)
                {
                    return url;
                }
                if (!url.ToLower().StartsWith("http") && !url.ToLower().StartsWith("ftp"))
                {
                    url = "http://" + url;
                }
                var request = WebRequest.Create("http://tinyurl.com/api-create.php?url=" + url);
                var res = request.GetResponse();
                string text;
                using (var reader = new StreamReader(res.GetResponseStream()))
                {
                    text = reader.ReadToEnd();
                }
                return text;
            }
            catch (Exception)
            {
                return url;
            }
        }



        internal static string GetFaceBookPostLink()
        {
            try
            {
                return GetConfigValue("FaceBookPostLink");
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return null;
            }
        }

        internal static string GetFaceBookPostDescription()
        {
            try
            {
                return GetConfigValue("FaceBookPostDescription");
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return null;
            }
        }

        internal static string GetFaceBookPostCaption()
        {
            try
            {
                return GetConfigValue("FaceBookPostCaption");
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return null;
            }
        }

        internal static string GetFaceBookPostName()
        {
            try
            {
                return GetConfigValue("FaceBookPostName");
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return null;
            }
        }

        public static string GetAuthorToExcludeOnIOS()
        {
            try
            {
                return GetConfigValue("AuthorToExcludeOnIOS");
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return null;
            }
        }
        public static string GetVideoTitleToExcludeOnIOS()
        {
            try
            {
                return GetConfigValue("VideoTitleToExcludeOnIOS");
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return null;
            }
        }
        public static string GetVideoKeywordsToExcludeOnIOS()
        {
            try
            {
                return GetConfigValue("VideoKeywordsToExcludeOnIOS");
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return null;
            }
        }
        public static int GetMaxIPFetchCount()
        {
            var limit = 20;
            try
            {
                limit = Int32.Parse(GetConfigValue("MaxIPFetchCount"));
            }
            catch { }
            return limit;

        }



        public static string VimeoConsumerKey()
        {
            try
            {
                return GetConfigValue("VimeoConsumerKey");
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return null;
            }
        }
        //saurin : 20140109 : Task :100 Change all hard-coding of API key for youtube,vimeo, dailymotion to dynamic.
        public static string VimeoConsumerSecret()
        {
            try
            {
                return GetConfigValue("VimeoConsumerSecret");
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return null;
            }
        }

        public static string VimeoStandardAdvancedApiUrl()
        {
            try
            {
                return GetConfigValue("VimeoStandardAdvancedApiUrl");
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return null;
            }
        }

        public static string VimeoSearchUrl()
        {
            try
            {
                return GetConfigValue("VimeoSearchUrl");
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return null;
            }
        }

        public static string DailyMotionUri()
        {
            try
            {
                return GetConfigValue("DailyMotionUri");
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return null;
            }
        }
        public static string YoutubeDeveloperKey()
        {
            try
            {
                return GetConfigValue("YoutubeDeveloperKey");
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return null;
            }
        }


        public static int YoutubeUserHistoryLimit()
        {

            var limit = 500;
            try
            {
                limit = Int32.Parse(GetConfigValue("YoutubeUserHistoryLimit"));
            }

            catch { }
            return limit;
        }
        //saurin : 20140122 : Added while coding for "tune your channels"
        public static string GetGoogleAppOathUrl()
        {
            try
            {
                return GetConfigValue("GoogleAppOathUrl");
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return null;
            }
        }
        public static string GetGoogleAppClientId()
        {
            try
            {
                return GetConfigValue("GoogleAppClientId");
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return null;
            }
        }
        public static string GetGoogleAppScope()
        {
            try
            {
                return GetConfigValue("GoogleAppScope");
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return null;
            }
        }
        public static string GetGoogleAppValidUrl()
        {
            try
            {
                return GetConfigValue("GoogleAppValidUrl");
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return null;
            }
        }
        public static string GetGoogleAppRedirectUrl()
        {
            try
            {
                return GetConfigValue("GoogleAppRedirectUrl");
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return null;
            }
        }

        public static string GetGoogleAppUserHistoryViewUri()
        {
            try
            {
                return GetConfigValue("GoogleAppUserHistoryViewUri");
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return null;
            }
        }



        public static string GetYouTubeUserInfoUri()
        {
            try
            {
                return GetConfigValue("GoogleAppYouTubeUserInfoUri");
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return null;
            }
        }

        public static string GetFacebookAppId()
        {
            try
            {
                return GetConfigValue("FacebookAppId");
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return null;
            }
        }
        //Saurin : 20140127 : 
        public static int GetMaxVideoCronLimitExternal()
        {
            var limit = 20;
            try
            {
                limit = Int32.Parse(GetConfigValue("MaxVideoCronLimitExternal"));
            }
            catch { }
            return limit;

        }

        public static int GetMaxVideoCronLimitVideoCollection()
        {

            var limit = 20;
            try
            {
                limit = Int32.Parse(GetConfigValue("MaxVideoCronLimitVideoCollection"));
            }
            catch { }
            return limit;

        }
        //saurin : 20140312 : 
        public static string ReplaceNumericTextToSpecialChar(string sampleString, string specialChar)
        {
            if (String.IsNullOrWhiteSpace(sampleString)) return sampleString;
            try
            {
                return Regex.Replace(sampleString, @"\d", specialChar);  //specialChar : anything like "%, + - or a-z" 
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return "";
            }



        }
        //saurin ; 20140325 : Additional Parameter for youtube query
        public static string YoutubeApiParameterForEmbeddableVideos()
        {
            try
            {
                return GetConfigValue("YoutubeApiParameterForEmbeddableVideos");
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return null;
            }
        }
        public static int GetMobileCriticalTimeoutSeconds()
        {
            var limit = 30;
            try
            {
                limit = Int32.Parse(GetConfigValue("MobileCriticalTimeoutSeconds"));
            }
            catch { }
            return limit;

        }

        public static void DisposeDBObject(ref startetkuEntities1 db)
        {
            db.Dispose();
        }
    }
}
