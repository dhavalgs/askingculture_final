using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starteku.Data.Manager;
using System.Data;
using starteku_BusinessLogic;

namespace startetku.Business.Logic {

	public class EmailSettingBM {

		#region Private Declaration
		private DataSet _ds;
		private string _returnString;
		private bool _returnBoolean;
		private Int32 _EmailId;
		private Int32 _EmailUserId;
		private Int32 _EmailCompanyId;
		private DateTime _EmailCreatedDate;
		private DateTime _EmailUpdatedDate;
		private Boolean _EmailIsActive;
		private Boolean _EmailIsDeleted;
		private String _EmailImage;
		private String _EmailHeader;
		private String _EmailFooter;
		private String _EmailSender;
		private String _Email;
		private String _password;
		private String _EmailHost;
		private String _EmailPort;
        private String _EmailDisplayName;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the ReturnString value.
		/// </summary>
		public DataSet ds
		{
			get { return _ds; }
			set { _ds = value; }
		}
		/// <summary>
		/// Gets or sets the ReturnString value.
		/// </summary>
		public string ReturnString {
			get { return _returnString; }
			set {  _returnString = value; }
		}

		/// <summary>
		/// Gets or sets the ReturnBoolean value.
		/// </summary>
		public bool ReturnBoolean {
			get { return _returnBoolean; }
			set {  _returnBoolean = value; }
		}

		/// <summary>
		/// Gets or sets the EmailId value.
		/// </summary>
		public Int32 EmailId  {
			get { return _EmailId; }
			set { _EmailId = value; }
		}

		/// <summary>
		/// Gets or sets the EmailUserId value.
		/// </summary>
		public Int32 EmailUserId  {
			get { return _EmailUserId; }
			set { _EmailUserId = value; }
		}

		/// <summary>
		/// Gets or sets the EmailCompanyId value.
		/// </summary>
		public Int32 EmailCompanyId  {
			get { return _EmailCompanyId; }
			set { _EmailCompanyId = value; }
		}

		/// <summary>
		/// Gets or sets the EmailCreatedDate value.
		/// </summary>
		public DateTime EmailCreatedDate  {
			get { return _EmailCreatedDate; }
			set { _EmailCreatedDate = value; }
		}

		/// <summary>
		/// Gets or sets the EmailUpdatedDate value.
		/// </summary>
		public DateTime EmailUpdatedDate  {
			get { return _EmailUpdatedDate; }
			set { _EmailUpdatedDate = value; }
		}

		/// <summary>
		/// Gets or sets the EmailIsActive value.
		/// </summary>
		public Boolean EmailIsActive  {
			get { return _EmailIsActive; }
			set { _EmailIsActive = value; }
		}

		/// <summary>
		/// Gets or sets the EmailIsDeleted value.
		/// </summary>
		public Boolean EmailIsDeleted  {
			get { return _EmailIsDeleted; }
			set { _EmailIsDeleted = value; }
		}

		/// <summary>
		/// Gets or sets the EmailImage value.
		/// </summary>
		public String EmailImage  {
			get { return _EmailImage; }
			set { _EmailImage = value; }
		}

		/// <summary>
		/// Gets or sets the EmailHeader value.
		/// </summary>
		public String EmailHeader  {
			get { return _EmailHeader; }
			set { _EmailHeader = value; }
		}

		/// <summary>
		/// Gets or sets the EmailFooter value.
		/// </summary>
		public String EmailFooter  {
			get { return _EmailFooter; }
			set { _EmailFooter = value; }
		}

		/// <summary>
		/// Gets or sets the EmailSender value.
		/// </summary>
		public String EmailSender  {
			get { return _EmailSender; }
			set { _EmailSender = value; }
		}

		/// <summary>
		/// Gets or sets the Email value.
		/// </summary>
		public String Email  {
			get { return _Email; }
			set { _Email = value; }
		}

		/// <summary>
		/// Gets or sets the password value.
		/// </summary>
		public String password  {
			get { return _password; }
			set { _password = value; }
		}

		/// <summary>
		/// Gets or sets the EmailHost value.
		/// </summary>
		public String EmailHost  {
			get { return _EmailHost; }
			set { _EmailHost = value; }
		}

		/// <summary>
		/// Gets or sets the EmailPort value.
		/// </summary>
		public String EmailPort  {
			get { return _EmailPort; }
			set { _EmailPort = value; }
		}
        public string EmailDisplayName
        {
            get { return _EmailDisplayName; }
            set { _EmailDisplayName = value; }
        }
		#endregion

		


		/// <summary>
		/// Inserts Records in the EmailSetting table.
		/// </summary>
		public void InsertEmailSetting() {
			try 
			{
			    DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

				ParamStruct[] param = new ParamStruct[15];
				DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
				DateTime dtmax = Convert.ToDateTime("1/1/9999 11:59:59 PM");

				param[0].direction = ParameterDirection.Input;
				param[0].ParamName = "@EmailUserId";
				param[0].DataType = DbType.Int32;
					if (_EmailUserId == 0)
					{ }		//param[8].value = DBNull.Value;
					else
					param[0].value = _EmailUserId;

				param[1].direction = ParameterDirection.Input;
				param[1].ParamName = "@EmailCompanyId";
				param[1].DataType = DbType.Int32;
				param[1].value = _EmailCompanyId;

				param[2].direction = ParameterDirection.Input;
				param[2].ParamName = "@EmailCreatedDate";
				param[2].DataType = DbType.DateTime;
				if (_EmailCreatedDate.Ticks > dtmin.Ticks && _EmailCreatedDate.Ticks < dtmax.Ticks)
					param[2].value = _EmailCreatedDate;
				else if (_EmailCreatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
				{}
				else
					param[2].value = _EmailCreatedDate;

				param[3].direction = ParameterDirection.Input;
				param[3].ParamName = "@EmailUpdatedDate";
				param[3].DataType = DbType.DateTime;
				if (_EmailUpdatedDate.Ticks > dtmin.Ticks && _EmailUpdatedDate.Ticks < dtmax.Ticks)
					param[3].value = _EmailUpdatedDate;
				else if (_EmailUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
				{}
				else
					param[3].value = _EmailUpdatedDate;
				param[4].direction = ParameterDirection.Input;
				param[4].ParamName = "@EmailIsActive";
				param[4].DataType = DbType.Boolean;
				if (String.IsNullOrEmpty(_EmailIsActive.ToString()))
					param[4].value = DBNull.Value;
				else
					param[4].value = _EmailIsActive;

				param[5].direction = ParameterDirection.Input;
				param[5].ParamName = "@EmailIsDeleted";
				param[5].DataType = DbType.Boolean;
				if (String.IsNullOrEmpty(_EmailIsDeleted.ToString()))
					param[5].value = DBNull.Value;
				else
					param[5].value = _EmailIsDeleted;

				param[6].direction = ParameterDirection.Input;
				param[6].ParamName = "@EmailImage";
				param[6].DataType = DbType.String;
				if (String.IsNullOrEmpty(_EmailImage))
					param[6].value = DBNull.Value;
				else
					param[6].value = _EmailImage;

				param[7].direction = ParameterDirection.Input;
				param[7].ParamName = "@EmailHeader";
				param[7].DataType = DbType.String;
				if (String.IsNullOrEmpty(_EmailHeader))
					param[7].value = DBNull.Value;
				else
					param[7].value = _EmailHeader;

				param[8].direction = ParameterDirection.Input;
				param[8].ParamName = "@EmailFooter";
				param[8].DataType = DbType.String;
				if (String.IsNullOrEmpty(_EmailFooter))
					param[8].value = DBNull.Value;
				else
					param[8].value = _EmailFooter;

				param[9].direction = ParameterDirection.Input;
				param[9].ParamName = "@EmailSender";
				param[9].DataType = DbType.String;
				if (String.IsNullOrEmpty(_EmailSender))
					param[9].value = DBNull.Value;
				else
					param[9].value = _EmailSender;

				param[10].direction = ParameterDirection.Input;
				param[10].ParamName = "@Email";
				param[10].DataType = DbType.String;
				if (String.IsNullOrEmpty(_Email))
					param[10].value = DBNull.Value;
				else
					param[10].value = _Email;

				param[11].direction = ParameterDirection.Input;
				param[11].ParamName = "@password";
				param[11].DataType = DbType.String;
				if (String.IsNullOrEmpty(_password))
					param[11].value = DBNull.Value;
				else
					param[11].value = _password;

				param[12].direction = ParameterDirection.Input;
				param[12].ParamName = "@EmailHost";
				param[12].DataType = DbType.String;
				if (String.IsNullOrEmpty(_EmailHost))
					param[12].value = DBNull.Value;
				else
					param[12].value = _EmailHost;

				param[13].direction = ParameterDirection.Input;
				param[13].ParamName = "@EmailPort";
				param[13].DataType = DbType.String;
				if (String.IsNullOrEmpty(_EmailPort))
					param[13].value = DBNull.Value;
				else
					param[13].value = _EmailPort;

                param[14].direction = ParameterDirection.Input;
                param[14].ParamName = "@EmailDisplayName";
                param[14].DataType = DbType.String;
                if (String.IsNullOrEmpty(_EmailDisplayName))
                    param[14].value = DBNull.Value;
                else
                    param[14].value = _EmailDisplayName;

				//obj.ExecScalar("InsertEmailSetting", CommandType.StoredProcedure, param);				_returnBoolean = true;
                 _ds = DBAccess.ExecDataSet("InsertEmailSetting", CommandType.StoredProcedure, param);
                _returnBoolean = true;
			}
			catch (DataException ex) 
			{
				_returnBoolean = false;
				throw ex;
			}
		}
        public void GetAllEmailSetting()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@EmailIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _EmailIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@EmailIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _EmailIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@EmailCompanyId";
                param[2].DataType = DbType.Int32;
                param[2].value = _EmailCompanyId;

                _ds = DBAccess.ExecDataSet("GetAllEmailSetting", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllEmailSetting" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

   
	}
}
