﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starteku.Data.Manager;
using System.Data;
using starteku_BusinessLogic.Model;
using starteku_BusinessLogic.View;
using System.Data.SqlClient;
using starteku.Data.Manager;

namespace starteku_BusinessLogic
{
  public class DirectionCategory
    {
        #region Private Declaration

        private DataSet _ds;
        private String _catID;
       // private Int32 _catID;
        private String _catName;
        private String _catNameDN;
        private String _catDescription;
        private Int32 _catIndex;
        private Int32 _catCreatedBy;
        private DateTime _catCreateDate;
        private DateTime _catUpdateDate;
        private Boolean _catIsActive;
        private Int32 _catCompanyID;
        private Int32 _LangID;
        private Boolean _catIsDelete;

        #endregion

        #region Properties

        public string catID
        {
            get { return _catID; }
            set { _catID = value; }
        }

        public string catName
        {
            get { return _catName; }
            set { _catName = value; }
        }

        public string catNameDN
        {
            get { return _catNameDN; }
            set { _catNameDN = value; }
        }

        /// Gets or sets the ReturnString value.

        public DataSet ds
        {
            get { return _ds; }
            set { _ds = value; }
        }



        public string catDescription
        {
            get { return _catDescription; }
            set { _catDescription = value; }
        }



        public Int32 catIndex
        {
            get { return _catIndex; }
            set { _catIndex = value; }
        }
        public Int32 LangID
        {
            get { return _LangID; }
            set { _LangID = value; }
        }

        public Int32 catCreatedBy
        {
            get { return _catCreatedBy; }
            set { _catCreatedBy = value; }
        }
        public DateTime catCreateDate
        {
            get { return _catCreateDate; }
            set { _catCreateDate = value; }
        }


        public DateTime catUpdateDate
        {
            get { return _catUpdateDate; }
            set { _catUpdateDate = value; }
        }



        public Int32 catCompanyID
        {
            get { return _catCompanyID; }
            set { _catCompanyID = value; }
        }


        public Boolean catIsActive
        {
            get { return _catIsActive; }
            set { _catIsActive = value; }
        }

        public Boolean catIsDelete
        {
            get { return _catIsDelete; }
            set { _catIsDelete = value; }
        }

        #endregion

        #region Methods
        public void InsertDirectionCategory()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[10];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@catName";
                param[0].DataType = DbType.String;
                param[0].value = _catName;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@catNameDN";
                param[1].DataType = DbType.String;
                param[1].value = _catNameDN;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@catDescription";
                param[2].DataType = DbType.String;
                param[2].value = _catDescription;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@catIndex";
                param[3].DataType = DbType.Int32;
                param[3].value = _catIndex;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@catCreatedBy";
                param[4].DataType = DbType.Int32;
                param[4].value = _catCreatedBy;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@catCreateDate";
                param[5].DataType = DbType.DateTime;
                param[5].value = _catCreateDate;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@catUpdateDate";
                param[6].DataType = DbType.DateTime;
                param[6].value = _catUpdateDate;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@catIsActive";
                param[7].DataType = DbType.Boolean;
                param[7].value = _catIsActive;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@catCompanyID";
                param[8].DataType = DbType.Int32;
                param[8].value = _catCompanyID;

                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@catIsDelete";
                param[9].DataType = DbType.Boolean;
                param[9].value = _catIsDelete;

                _ds = DBAccess.ExecDataSet("SpInsertCategoryDirection", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {

                Common.WriteLog("error in SpInsertCategoryDirection" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;
            }
        }

        public void UpdateDirectionCategory()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[11];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@catName";
                param[0].DataType = DbType.String;
                param[0].value = _catName;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@catNameDN";
                param[1].DataType = DbType.String;
                param[1].value = _catNameDN;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@catDescription";
                param[2].DataType = DbType.String;
                param[2].value = _catDescription;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@catIndex";
                param[3].DataType = DbType.Int32;
                param[3].value = _catIndex;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@catCreatedBy";
                param[4].DataType = DbType.Int32;
                param[4].value = _catCreatedBy;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@catCreateDate";
                param[5].DataType = DbType.DateTime;
                param[5].value = _catCreateDate;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@catUpdateDate";
                param[6].DataType = DbType.DateTime;
                param[6].value = _catUpdateDate;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@catIsActive";
                param[7].DataType = DbType.Boolean;
                param[7].value = _catIsActive;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@catCompanyID";
                param[8].DataType = DbType.Int32;
                param[8].value = _catCompanyID;

                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@catIsDelete";
                param[9].DataType = DbType.Boolean;
                param[9].value = _catIsDelete;

                param[10].direction = ParameterDirection.Input;
                param[10].ParamName = "@catID";
                param[10].DataType = DbType.String;
                param[10].value = _catID;

                _ds = DBAccess.ExecDataSet("UpdateCompanyDirectionCategory", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {

                Common.WriteLog("error in UpdateCompanyDirectionCategory" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;
            }
        }
        #endregion

        public void GetAllCompanyCategoryName()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[5];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@catID";
                param[0].DataType = DbType.String;
                param[0].value = _catID;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@IsActive";
                param[1].DataType = DbType.Boolean;
                param[1].value = _catIsActive;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@IsDeleted";
                param[2].DataType = DbType.Boolean;
                param[2].value = _catIsDelete;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@LangId";
                param[3].DataType = DbType.Int32;
                param[3].value = _LangID;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@CompanyID";
                param[4].DataType = DbType.Int32;
                param[4].value = _catCompanyID;

                _ds = DBAccess.ExecDataSet("GetAllCompanyCategoryList", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllCompanyCategoryList " + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void CompanyDirCatDelete()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] pm = new ParamStruct[3];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@catID";
                pm[0].DataType = DbType.String;
                pm[0].value = _catID;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@catIsActive";
                pm[1].DataType = DbType.Boolean;
                pm[1].value = _catIsActive;

                pm[2].direction = ParameterDirection.Input;
                pm[2].ParamName = "@catIsDelete";
                pm[2].DataType = DbType.Boolean;
                pm[2].value = _catIsDelete;

                _ds = DBAccess.ExecDataSet("CompanyDirectionCategoryDelete", CommandType.StoredProcedure, pm);
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in CompanyDirectionCategoryDelete  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            }
        }
    }
}
