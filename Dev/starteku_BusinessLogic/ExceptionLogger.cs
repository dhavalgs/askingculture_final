﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading;
using WatzOnTV.Logic;
using starteku_BusinessLogic.Model;


namespace starteku_BusinessLogic
{
    /// <summary>Enumerated type that defines how users will be notified of exceptions</summary>
    public enum NotificationType
    {
        /// <summary>Users will not be notified, exceptions will be automatically logged to the registered loggers</summary>
        Silent,
        /// <summary>Users will be notified an exception has occurred, exceptions will be automatically logged to the registered loggers</summary>
        Inform,
        /// <summary>Users will be notified an exception has occurred and will be asked if they want the exception logged</summary>
        Ask
    }

    /// <summary>
    /// Abstract class for logging errors to different output devices, primarily for use in Windows Forms applications
    /// </summary>
    public abstract class LoggerImplementation
    {
        /// <summary>Logs the specified error.</summary>
        /// <param name="error">The error to log.</param>
        public abstract void LogError(string error);
    }

    /// <summary>
    /// Class to log unhandled exceptions
    /// </summary>
    public class ExceptionLogger
    {
        /// <summary>
        /// Creates a new instance of the ExceptionLogger class
        /// </summary>
        public ExceptionLogger()
        {


            //  AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(OnUnhandledException);
            // loggers = new List<LoggerImplementation>();
        }

        private List<LoggerImplementation> loggers;
        /// <summary>
        /// Adds a logger implementation to the list of used loggers.
        /// </summary>
        /// <param name="logger">The logger to add.</param>
        public void AddLogger(LoggerImplementation logger)
        {
            loggers.Add(logger);
        }

        private NotificationType notificationType = NotificationType.Silent;
        private bool logStactTrace = false;

        /// <summary>
        /// Gets or sets the type of the notification shown to the end user.
        /// </summary>
        public NotificationType NotificationType
        {
            get { return notificationType; }
            set { notificationType = value; }
        }
        public bool LogStactTrace
        {
            get { return logStactTrace; }
            set { logStactTrace = value; }
        }
        delegate void LogExceptionDelegate(Exception e);
        private void HandleException(Exception e)
        {
            switch (notificationType)
            {
                case NotificationType.Ask:

                    return;
                    break;
                case NotificationType.Inform:
                    {
                        if (e.Message.Contains("Could not start QuickBooks"))
                        {
                            //MessageBox.Show("Could Not Connect to QuickBooks, Please make sure QuickBooks is running on this machine", "QXL", MessageBoxButtons.OK, MessageBoxIcon.Error);     
                        }
                        else
                        {
                            //MessageBox.Show("An unexpected error occurred - " + e.Message,"QXL",MessageBoxButtons.OK,MessageBoxIcon.Error);     
                        }


                    }


                    break;
                case NotificationType.Silent:
                    break;
            }

            //LogExceptionDelegate logDelegate = new LogExceptionDelegate(LogException);
            //logDelegate.BeginInvoke(e, new AsyncCallback(LogCallBack), null);
        }

        // Event handler that will be called when an unhandled
        // exception is caught
        private void OnThreadException(object sender, ThreadExceptionEventArgs e)
        {
            // Log the exception to a file
            HandleException(e.Exception);
        }

        private void OnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            HandleException((Exception)e.ExceptionObject);
        }

        private void LogCallBack(IAsyncResult result)
        {
            AsyncResult asyncResult = (AsyncResult)result;
            LogExceptionDelegate logDelegate = (LogExceptionDelegate)asyncResult.AsyncDelegate;
            if (!asyncResult.EndInvokeCalled)
            {
                logDelegate.EndInvoke(result);
            }
        }

        public static string GetExceptionTypeStack(Exception e)
        {
            if (e.InnerException != null)
            {
                StringBuilder message = new StringBuilder();
                message.AppendLine(GetExceptionTypeStack(e.InnerException));
                message.AppendLine("   " + e.GetType().ToString());
                return (message.ToString());
            }
            else
            {
                return "   " + e.GetType().ToString();
            }
        }

        private static string GetExceptionMessageStack(Exception e)
        {
            if (e.InnerException != null)
            {
                StringBuilder message = new StringBuilder();
                message.AppendLine(GetExceptionMessageStack(e.InnerException));
                message.AppendLine("   " + e.Message);
                return (message.ToString());
            }
            else
            {
                return "   " + e.Message;
            }
        }

        private string GetExceptionCallStack(Exception e)
        {
            if (e.InnerException != null)
            {
                StringBuilder message = new StringBuilder();
                message.AppendLine(GetExceptionCallStack(e.InnerException));
                message.AppendLine("--- Next Call Stack:");
                message.AppendLine(e.StackTrace);
                return (message.ToString());
            }
            else
            {
                return e.StackTrace;
            }
        }

        private static TimeSpan GetSystemUpTime()
        {
            PerformanceCounter upTime = new PerformanceCounter("System", "System Up Time");
            upTime.NextValue();
            return TimeSpan.FromSeconds(upTime.NextValue());
        }


        //public static void LogException(Exception exception)
        //{
        //    LogException(exception, "");
        //}

        /// <summary>writes exception details to the registered loggers</summary>
        /// <param name="exception">The exception to log.</param>
        /// <param name="additionalInfo"></param>
        public static void LogException(Exception exception, int userId = 0, string additionalInfo = "")
        {
            var db = new startetkuEntities1();
            try
            {
                //saurin  : 20140226 Critical Error :  Out Of Memory Error 



                var error = new errorlog();
                error.ErrorDateTime = CommonUtilities.GetCurrentDateTime();
                error.ActiveUserSession = 1;
                error.CurrentCulture = CultureInfo.CurrentCulture.Name;
                error.ErrorId = Convert.ToString(Guid.NewGuid());
                error.OSVersion = Environment.OSVersion.ToString();
                error.AssemblyVersion = string.Format("{0}.{1}.{2}.{3}", Environment.Version.Major,
                                                      Environment.Version.Minor, Environment.Version.Revision,
                                                      Environment.Version.Build);
                error.ExceptionClasses = GetExceptionTypeStack(exception);
                error.ErrorMessage = exception.Message;
                error.StackTrace = String.IsNullOrEmpty(exception.StackTrace) ? "NA" : exception.StackTrace + " INNER EXCEPTION : " + exception.InnerException;
                error.Source = String.IsNullOrEmpty(exception.Source) ? "NA" : exception.Source;
                error.ExceptionMessages = additionalInfo + "  " + GetExceptionMessageStack(exception);
                // error.CronnerName = CommonAttributes.GetCronnerName();//saurin : 20140228 : 
                //error.AppendLine("System up time:    " + GetSystemUpTime());

                //db.ErrorLogs.InsertOnSubmit(error);
                //db.SubmitChanges();
                db.errorlogs.Add(error);
                db.SaveChanges();
                //CommonAttributes.SendMailForCritialError("Out Of Memory Error", 0, "", exception);

             //   CommonAttributes.SendMailForCritialError("STRATEKU - Exception - Mail for Developer Only", userId, exception, additionalInfo);
            }
            catch (Exception ex)
            {

                //as suggested by Giorgi D.  commenting  //throw ex;
                //  LogToFile(exception, additionalInfo);
                //  LogToFile(ex, additionalInfo);

            }
            finally
            {
                CommonAttributes.DisposeDBObject(ref db);
            }
            //MEMORYSTATUSEX memStatus = new MEMORYSTATUSEX();
            //if (GlobalMemoryStatusEx(memStatus))
            //{
            //    error.AppendLine("Total memory:      " + memStatus.ullTotalPhys / (1024 * 1024) + "Mb");
            //    error.AppendLine("Available memory:  " + memStatus.ullAvailPhys / (1024 * 1024) + "Mb");
            //}

            //error.AppendLine("");

            //error.AppendLine("Exception classes:   ");
            //error.Append(GetExceptionTypeStack(exception));
            //error.AppendLine("");
            //error.AppendLine("Exception messages: ");
            //error.Append(GetExceptionMessageStack(exception));

            //error.AppendLine("");
            //if (logStactTrace)
            //{
            //    error.AppendLine("Stack Traces:");
            //    error.Append(GetExceptionCallStack(exception));
            //    error.AppendLine("");
            //    error.AppendLine("Loaded Modules:");
            //    Process thisProcess = Process.GetCurrentProcess();
            //    foreach (ProcessModule module in thisProcess.Modules)
            //    {
            //        error.AppendLine(module.FileName + " " + module.FileVersionInfo.FileVersion);
            //    }
            //    error.AppendLine("");
            //}
            //error.AppendLine("****   End Of Log   ****");
            //for (int i = 0; i < loggers.Count; i++)
            //{
            //    loggers[i].LogError(error.ToString());
            //}
        }

        private static void LogToFile(Exception exception, string additionalInfo)
        {
            try
            {


                string errorPath = "";// CommonAttributes.GetLocalErrorFilePath();
                errorPath = errorPath.Replace("Day", DateTime.UtcNow.Day.ToString());
                errorPath = errorPath.Replace("Month", DateTime.UtcNow.Month.ToString());
                errorPath = errorPath.Replace("Year", DateTime.UtcNow.Year.ToString());
                {
                    StreamWriter sw = new StreamWriter(errorPath, true);
                    sw.WriteLine("-----------------------------------");
                    sw.WriteLine(CommonUtilities.GetCurrentDateTime());
                    sw.WriteLine(1);
                    sw.WriteLine(CultureInfo.CurrentCulture.Name);
                    sw.WriteLine(Guid.NewGuid());
                    sw.WriteLine(Environment.OSVersion.ToString());
                    sw.WriteLine(string.Format("{0}.{1}.{2}.{3}", Environment.Version.Major,
                                               Environment.Version.Minor, Environment.Version.Revision,
                                               Environment.Version.Build));
                    sw.WriteLine(GetExceptionTypeStack(exception));
                    sw.WriteLine(exception.Message);
                    sw.WriteLine(String.IsNullOrEmpty(exception.StackTrace)
                                     ? "NA"
                                     : exception.StackTrace + " INNER EXCEPTION : " + exception.InnerException);
                    sw.WriteLine(String.IsNullOrEmpty(exception.Source) ? "NA" : exception.Source);
                    sw.WriteLine(additionalInfo + "  " + GetExceptionMessageStack(exception));
                    sw.WriteLine("--------------End---------------------");
                    sw.WriteLine("");
                    sw.Flush();
                    sw.Close();
                }
            }
            catch (Exception)
            {


            }

        }
    }
}


