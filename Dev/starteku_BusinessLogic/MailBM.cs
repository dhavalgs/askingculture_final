﻿using System.Net.Mail;
using System.Web;
using WatzOnTV.Logic;
using starteku.Data.Manager;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using starteku_BusinessLogic.Model;

namespace starteku_BusinessLogic
{
    public class MailBM
    {
        #region Private Declaration
        private DataSet _ds;
        private string _returnString;
        private bool _returnBoolean;
        private int _MailId;
        private String _MailTo;
        private String _MailCc;
        private String _MailSubject;
        private Int32 _MailAttachmentId;
        private String _MailBody;
        private Boolean _MailNotificationStatus;
        private DateTime _MailCreatedDate;
        private DateTime _MailUpdatedDate;
        private Boolean _MailIsDeleted;
        private Boolean _MailIsActive;
        private Int32 _MailCompanyId;
        private Int32 _MailUserId;
        private String _MailToEmailId;
        private Boolean _MailSendStatus;
        private Boolean _MailInboxStatus;
        #endregion

        #region Properties
        public string returnString
        {
            get { return _returnString; }
            set { _returnString = value; }
        }

        public bool returnBoolean
        {
            get { return _returnBoolean; }
            set { _returnBoolean = value; }
        }
        public Int32 MailId
        {
            get { return _MailId; }
            set { _MailId = value; }
        }
        public String MailTo
        {
            get
            {
                return _MailTo;
            }
            set
            {
                _MailTo = value;
            }
        }

        public String MailCc
        {
            get
            {
                return _MailCc;
            }
            set
            {
                _MailCc = value;
            }
        }

        public String MailSubject
        {
            get
            {
                return _MailSubject;
            }
            set
            {
                _MailSubject = value;
            }
        }

        public Int32 MailAttachmentId
        {
            get
            {
                return _MailAttachmentId;
            }
            set
            {
                _MailAttachmentId = value;
            }
        }

        public String MailBody
        {
            get
            {
                return _MailBody;
            }
            set
            {
                _MailBody = value;
            }
        }

        public Boolean MailNotificationStatus
        {
            get
            {
                return _MailNotificationStatus;
            }
            set
            {
                _MailNotificationStatus = value;
            }
        }

        public DateTime MailCreatedDate
        {
            get
            {
                return _MailCreatedDate;
            }
            set
            {
                _MailCreatedDate = value;
            }
        }

        public DateTime MailUpdatedDate
        {
            get
            {
                return _MailUpdatedDate;
            }
            set
            {
                _MailUpdatedDate = value;
            }
        }

        public Boolean MailIsDeleted
        {
            get
            {
                return _MailIsDeleted;
            }
            set
            {
                _MailIsDeleted = value;
            }
        }

        public Boolean MailIsActive
        {
            get
            {
                return _MailIsActive;
            }
            set
            {
                _MailIsActive = value;
            }
        }

        public Int32 MailCompanyId
        {
            get
            {
                return _MailCompanyId;
            }
            set
            {
                _MailCompanyId = value;
            }
        }

        public Int32 MailUserId
        {
            get
            {
                return _MailUserId;
            }
            set
            {
                _MailUserId = value;
            }
        }

        public String MailToEmailId
        {
            get
            {
                return _MailToEmailId;
            }
            set
            {
                _MailToEmailId = value;
            }
        }

        public Boolean MailSendStatus
        {
            get
            {
                return _MailSendStatus;
            }
            set
            {
                _MailSendStatus = value;
            }
        }

        public Boolean MailInboxStatus
        {
            get
            {
                return _MailInboxStatus;
            }
            set
            {
                _MailInboxStatus = value;
            }
        }
        #endregion

        #region Methods
        public void InsertSendMail()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[14];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue; 

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@MailTo";
                param[0].DataType = DbType.String;
                if (String.IsNullOrEmpty(_MailTo))
                    param[0].value = DBNull.Value;
                else
                    param[0].value = _MailTo;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@MailCc";
                param[1].DataType = DbType.String;
                if (String.IsNullOrEmpty(_MailCc))
                    param[1].value = DBNull.Value;
                else
                    param[1].value = _MailCc;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@MailSubject";
                param[2].DataType = DbType.String;
                if (String.IsNullOrEmpty(_MailSubject))
                    param[2].value = DBNull.Value;
                else
                    param[2].value = _MailSubject;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@MailAttachmentId";
                param[3].DataType = DbType.Int32;
                param[3].value = _MailAttachmentId;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@MailBody";
                param[4].DataType = DbType.String;
                if (String.IsNullOrEmpty(_MailSubject))
                    param[4].value = DBNull.Value;
                else
                    param[4].value = _MailBody;


                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@MailNotificationStatus";
                param[5].DataType = DbType.Boolean;
                param[5].value = _MailNotificationStatus;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@MailCreatedDate";
                param[6].DataType = DbType.DateTime;
                if (_MailCreatedDate.Ticks > dtmin.Ticks && _MailCreatedDate.Ticks < dtmax.Ticks)
                    param[6].value = _MailCreatedDate;
                else if (_MailCreatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[6].value = _MailCreatedDate;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@MailUpdatedDate";
                param[7].DataType = DbType.DateTime;
                if (_MailUpdatedDate.Ticks > dtmin.Ticks && _MailUpdatedDate.Ticks < dtmax.Ticks)
                    param[7].value = _MailUpdatedDate;
                else if (_MailUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[7].value = _MailUpdatedDate;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@MailIsDeleted";
                param[8].DataType = DbType.Boolean;
                param[8].value = _MailIsDeleted;

                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@MailIsActive";
                param[9].DataType = DbType.Boolean;
                param[9].value = _MailIsActive;

                param[10].direction = ParameterDirection.Input;
                param[10].ParamName = "@MailCompanyId";
                param[10].DataType = DbType.Int32;
                param[10].value = _MailCompanyId;

                param[11].direction = ParameterDirection.Input;
                param[11].ParamName = "@MailUserId";
                param[11].DataType = DbType.Int32;
                param[11].value = _MailUserId;

                param[12].direction = ParameterDirection.Input;
                param[12].ParamName = "@MailSendStatus";
                param[12].DataType = DbType.Boolean;
                param[12].value = _MailSendStatus;

                param[13].direction = ParameterDirection.Input;
                param[13].ParamName = "@MailInboxStatus";
                param[13].DataType = DbType.Boolean;
                param[13].value = _MailInboxStatus;

                _ds = DBAccess.ExecDataSet("InsertSendMail", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (Exception ex)
            {

            }
        }

        public DataSet GetAllMailByUserId()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@MailIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _MailIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@MailIsDeleted";
                param[1].DataType = DbType.Int32;
                param[1].value = _MailIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@MailUserId";
                param[2].DataType = DbType.Int32;
                param[2].value = _MailUserId;

                _ds = DBAccess.ExecDataSet("GetAllMailByUserId", CommandType.StoredProcedure, param);
                return _ds;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public DataSet GetAllMailByCompanyId()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@MailIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _MailIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@MailIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _MailIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@MailCompanyId";
                param[2].DataType = DbType.Int32;
                param[2].value = _MailCompanyId;

                _ds = DBAccess.ExecDataSet("GetAllMailByCompanyId", CommandType.StoredProcedure, param);
                return _ds;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public DataSet GetMailDetailByMailId()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@MailId";
                param[0].DataType = DbType.Int32;
                param[0].value = _MailId;

                _ds = DBAccess.ExecDataSet("GetMailDetailByMailId", CommandType.StoredProcedure, param);
                return _ds;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public DataSet GetAllMailByEmailId()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@MailToEmailId";
                param[0].DataType = DbType.String;
                if (string.IsNullOrEmpty(_MailToEmailId))
                    param[0].value = DBNull.Value;
                else
                    param[0].value = _MailToEmailId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@MailIsActive";
                param[1].DataType = DbType.Boolean;
                param[1].value = _MailIsActive;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@MailIsDeleted";
                param[2].DataType = DbType.Boolean;
                param[2].value = _MailIsDeleted;

                _ds = DBAccess.ExecDataSet("GetAllMailByEmailId", CommandType.StoredProcedure, param);
                return _ds;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public DataSet GetAllDeletedMailByUserId()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[4];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@MailToEmailId";
                param[0].DataType = DbType.String;
                if (string.IsNullOrEmpty(_MailToEmailId))
                    param[0].value = DBNull.Value;
                else
                    param[0].value = _MailToEmailId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@MailIsActive";
                param[1].DataType = DbType.Boolean;
                param[1].value = _MailIsActive;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@MailIsDeleted";
                param[2].DataType = DbType.Boolean;
                param[2].value = _MailIsDeleted;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@MailUserId";
                param[3].DataType = DbType.Int32;
                param[3].value = _MailUserId;

                _ds = DBAccess.ExecDataSet("GetAllDeletedMailByUserId", CommandType.StoredProcedure, param);
                return _ds;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public void DeleteMailByMailId()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@MailId";
                param[0].DataType = DbType.Int32;
                param[0].value = _MailId;

                _ds = DBAccess.ExecDataSet("DeleteMailByMailId", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (Exception ex)
            {

            }
        }
        public void DeleteSendMailByMailId()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@MailId";
                param[0].DataType = DbType.Int32;
                param[0].value = _MailId;

                _ds = DBAccess.ExecDataSet("DeleteSendMailByMailId", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (Exception ex)
            {

            }
        }
        public void DeleteAllMailByMailId()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@MailId";
                param[0].DataType = DbType.Int32;
                param[0].value = _MailId;

                _ds = DBAccess.ExecDataSet("DeleteAllMailByMailId", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
    }
}
