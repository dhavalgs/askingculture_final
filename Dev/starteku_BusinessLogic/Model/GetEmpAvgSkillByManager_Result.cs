//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace starteku_BusinessLogic.Model
{
    using System;
    
    public partial class GetEmpAvgSkillByManager_Result
    {
        public Nullable<int> skillAchive { get; set; }
        public Nullable<int> skillLocal { get; set; }
        public Nullable<int> skilltarget { get; set; }
        public string userdivId { get; set; }
        public Nullable<int> userJobType { get; set; }
        public Nullable<int> skillComId { get; set; }
        public string comCompetence { get; set; }
        public string comCompetenceDN { get; set; }
    }
}
