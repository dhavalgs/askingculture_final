//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace starteku_BusinessLogic.Model
{
    using System;
    
    public partial class GetAllCompetenceCategoryById_Result
    {
        public int catId { get; set; }
        public string catName { get; set; }
        public string catNameDN { get; set; }
    }
}
