//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace starteku_BusinessLogic.Model
{
    using System;
    
    public partial class GetTO_DO_LISTbyid_Result
    {
        public Nullable<long> no { get; set; }
        public int listId { get; set; }
        public Nullable<int> listUserId { get; set; }
        public string listsubject { get; set; }
        public Nullable<int> listCompanyId { get; set; }
        public Nullable<int> listCreateBy { get; set; }
        public string listCreatedDate { get; set; }
    }
}
