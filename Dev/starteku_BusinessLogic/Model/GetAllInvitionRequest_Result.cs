//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace starteku_BusinessLogic.Model
{
    using System;
    
    public partial class GetAllInvitionRequest_Result
    {
        public int invId { get; set; }
        public Nullable<int> invfromUserId { get; set; }
        public Nullable<int> invToUserId { get; set; }
        public string invCreatedDate { get; set; }
        public Nullable<int> invComId { get; set; }
        public string userFirstName { get; set; }
        public string invtype { get; set; }
        public Nullable<int> invHelpTrackId { get; set; }
        public string status { get; set; }
        public string invsubject { get; set; }
        public string MinAgo { get; set; }
    }
}
