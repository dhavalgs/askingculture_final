//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace starteku_BusinessLogic.Model
{
    using System;
    
    public partial class PDPPointMasterInsert_Result
    {
        public Nullable<int> Column1 { get; set; }
        public Nullable<int> Column2 { get; set; }
        public Nullable<int> Column3 { get; set; }
        public Nullable<int> Column4 { get; set; }
        public Nullable<System.DateTime> Column5 { get; set; }
        public string Column6 { get; set; }
        public string Column7 { get; set; }
        public Nullable<int> Column8 { get; set; }
    }
}
