//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace starteku_BusinessLogic.Model
{
    using System;
    
    public partial class GetEmailSendList_Result
    {
        public int userId { get; set; }
        public string userFirstName { get; set; }
        public string userLastName { get; set; }
        public string userEmail { get; set; }
        public Nullable<int> userCompanyId { get; set; }
        public string userCreateBy { get; set; }
        public string CreateUserFirstName { get; set; }
        public string UserCreateLastName { get; set; }
        public Nullable<int> RefId { get; set; }
        public string EmailType { get; set; }
        public string EmailContent { get; set; }
    }
}
