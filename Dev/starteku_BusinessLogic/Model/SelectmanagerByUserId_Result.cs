//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace starteku_BusinessLogic.Model
{
    using System;
    
    public partial class SelectmanagerByUserId_Result
    {
        public string userEmail { get; set; }
        public string userPassword { get; set; }
        public int userId { get; set; }
        public string userFirstName { get; set; }
    }
}
