//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace starteku_BusinessLogic.Model
{
    using System;
    
    public partial class GetLevelByUserIdandcomId_Result
    {
        public int skillId { get; set; }
        public Nullable<int> skillUserId { get; set; }
        public Nullable<int> skillComId { get; set; }
        public Nullable<int> skillCompanyId { get; set; }
        public Nullable<int> skillLocal { get; set; }
        public Nullable<int> skillAchive { get; set; }
        public Nullable<int> skilltarget { get; set; }
        public Nullable<long> isActiveSkill { get; set; }
        public Nullable<int> PDPPoint { get; set; }
    }
}
