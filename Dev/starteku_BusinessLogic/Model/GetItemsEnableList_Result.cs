//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace starteku_BusinessLogic.Model
{
    using System;
    
    public partial class GetItemsEnableList_Result
    {
        public Nullable<int> Aemcompid { get; set; }
        public Nullable<int> comdirCompID { get; set; }
        public Nullable<int> compCompID { get; set; }
        public Nullable<int> pointCompID { get; set; }
        public Nullable<int> quesemID { get; set; }
    }
}
