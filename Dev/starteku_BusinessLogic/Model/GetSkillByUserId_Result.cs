//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace starteku_BusinessLogic.Model
{
    using System;
    
    public partial class GetSkillByUserId_Result
    {
        public int skillId { get; set; }
        public Nullable<int> skillUserId { get; set; }
        public Nullable<int> skillComId { get; set; }
        public Nullable<int> skillStatus { get; set; }
        public Nullable<bool> skillIsActive { get; set; }
        public Nullable<int> skillCompanyId { get; set; }
        public Nullable<int> skillLocal { get; set; }
        public Nullable<int> skillAchive { get; set; }
        public Nullable<int> skilltarget { get; set; }
        public Nullable<int> skillSet { get; set; }
        public Nullable<System.DateTime> skillAcceptDate { get; set; }
        public string skillMonth { get; set; }
        public string userFirstName { get; set; }
        public string comCompetence { get; set; }
        public string comCompetenceDN { get; set; }
        public Nullable<System.DateTime> skillCreatedDate { get; set; }
        public string PDP { get; set; }
    }
}
