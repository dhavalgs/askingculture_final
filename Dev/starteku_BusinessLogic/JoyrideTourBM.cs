﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using starteku.Data.Manager;

namespace starteku_BusinessLogic
{
    public class JoyrideTourBM
    {
        
        #region Method
        public static void JoyRideTourShowHideData(int userId,bool isActive,bool isDeleted,bool isJoyRideOff)
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[6];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@jtIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = isActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@jtIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = isDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@userId";
                param[2].DataType = DbType.Int32;
                param[2].value = userId;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@jtUpdatedDate";
                param[3].DataType = DbType.DateTime;
                param[3].value = null;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@jtCreatedDate";
                param[4].DataType = DbType.DateTime;
                param[4].value = Common.GetCurrentDatetime();

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@jtIsOff";
                param[5].DataType = DbType.Boolean;
                param[5].value = isJoyRideOff;

                DBAccess.ExecDataSet("InsertJoyRideTourData", CommandType.StoredProcedure, param);



            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllCompetenceCategory" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }

        }


        public  DataSet GetJoyrideTourByUserId(int userId)
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@userId";
                param[0].DataType = DbType.Int32;
                param[0].value = userId;

                return DBAccess.ExecDataSet("GetJoyrideTourByUserId", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllCompetenceCategory" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }

        }
        #endregion
    }
}
