using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starteku.Data.Manager;
using System.Data;
using starteku_BusinessLogic;

namespace startetku.Business.Logic {

	public class InvitationBM {

		#region Private Declaration


		private DataSet _ds;
		private string _returnString;
		private bool _returnBoolean;
		private Int32 _InvitationId;
		private String _InvitationToEmail;
		private DateTime _InvitationCreatedDate;
		private DateTime _InvitationUpdatedDate;
		private Boolean _InvitationIsDeleted;
		private Boolean _InvitationIsActive;
		private Int32 _InvitationCompanyId;
		private Int32 _InvitationUserId;
		private String _InvitationSubject;
		private DateTime _InvitationStartDate;
		private DateTime _InvitationEndDate;
		private String _InvitationMessage;
        private DateTime _invDate;

        #region Invitation
        private Int32 _invId;
        private Int32 _invfromUserId;
        private Int32 _invToUserId;
        private String _invsubject;
        private DateTime _invCreatedDate;
        private DateTime _invUpdatedDate;
        private Boolean _invIsDeleted;
        private Boolean _invIsActive;
        private Int32 _invCompanyId;
        private String _invtype;
        private Int32 _invComId;
        private Int32 _invStatus;
	    private Int32 _invFromUserId;
        private String _invType;
	    private string _invDocSuggestPath;
	    private string _invDocName;
	    private int _invHelpTrackId;
	    private int _InvitationToUserId;

	    #endregion

		#endregion

        #region Properties
        #region Invitation

        public Int32 invId
        {
            get { return _invId; }
            set { _invId = value; }
        }
        public Int32 invStatus
        {
            get { return _invStatus; }
            set { _invStatus = value; }
        }
        public Int32 invComId
        {
            get { return _invComId; }
            set { _invComId = value; }
        }
        /// <summary>
        /// Gets or sets the notUserId value.
        /// </summary>
        public Int32 invfromUserId
        {
            get { return _invfromUserId; }
            set { _invfromUserId = value; }
        }
        public Int32 invToUserId
        {
            get { return _invToUserId; }
            set { _invToUserId = value; }
        }
        /// <summary>
        /// Gets or sets the notsubject value.
        /// </summary>
        public String invsubject
        {
            get { return _invsubject; }
            set { _invsubject = value; }
        }

        /// <summary>
        /// Gets or sets the notCreatedDate value.
        /// </summary>
        public DateTime invCreatedDate
        {
            get { return _invCreatedDate; }
            set { _invCreatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the notUpdatedDate value.
        /// </summary>
        public DateTime invUpdatedDate
        {
            get { return _invUpdatedDate; }
            set { _invUpdatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the notIsDeleted value.
        /// </summary>
        public Boolean invIsDeleted
        {
            get { return _invIsDeleted; }
            set { _invIsDeleted = value; }
        }

        /// <summary>
        /// Gets or sets the notIsActive value.
        /// </summary>
        public Boolean invIsActive
        {
            get { return _invIsActive; }
            set { _invIsActive = value; }
        }

        /// <summary>
        /// Gets or sets the notCompanyId value.
        /// </summary>
        public Int32 invCompanyId
        {
            get { return _invCompanyId; }
            set { _invCompanyId = value; }
        }



        /// <summary>
        /// Gets or sets the notpage value.
        /// </summary>
        public String invtype
        {
            get { return _invtype; }
            set { _invtype = value; }
        }

        #endregion

        public int invHelpTrackId
        {
            get { return _invHelpTrackId; }
            set { _invHelpTrackId = value; }
        }
        public int invFromUserId
        {
            get { return _invFromUserId; }
            set { _invFromUserId = value; }
        }

        /// <summary>
        /// Gets or sets the ReturnBoolean value.
        /// </summary>
        public string invType
        {
            get { return _invType; }
            set { _invType = value; }
        }
		/// <summary>
		/// Gets or sets the ReturnString value.
		/// </summary>
		public DataSet ds
		{
			get { return _ds; }
			set { _ds = value; }
		}
		/// <summary>
		/// Gets or sets the ReturnString value.
		/// </summary>
		public string ReturnString {
			get { return _returnString; }
			set {  _returnString = value; }
		}

		/// <summary>
		/// Gets or sets the ReturnBoolean value.
		/// </summary>
		public bool ReturnBoolean {
			get { return _returnBoolean; }
			set {  _returnBoolean = value; }
		}

		/// <summary>
		/// Gets or sets the InvitationId value.
		/// </summary>
		public Int32 InvitationId  {
			get { return _InvitationId; }
			set { _InvitationId = value; }
		}

		/// <summary>
		/// Gets or sets the InvitationToEmail value.
		/// </summary>
		public String InvitationToEmail  {
			get { return _InvitationToEmail; }
			set { _InvitationToEmail = value; }
		}

		/// <summary>
		/// Gets or sets the InvitationCreatedDate value.
		/// </summary>
		public DateTime InvitationCreatedDate  {
			get { return _InvitationCreatedDate; }
			set { _InvitationCreatedDate = value; }
		}

		/// <summary>
		/// Gets or sets the InvitationUpdatedDate value.
		/// </summary>
		public DateTime InvitationUpdatedDate  {
			get { return _InvitationUpdatedDate; }
			set { _InvitationUpdatedDate = value; }
		}

		/// <summary>
		/// Gets or sets the InvitationIsDeleted value.
		/// </summary>
		public Boolean InvitationIsDeleted  {
			get { return _InvitationIsDeleted; }
			set { _InvitationIsDeleted = value; }
		}

		/// <summary>
		/// Gets or sets the InvitationIsActive value.
		/// </summary>
		public Boolean InvitationIsActive  {
			get { return _InvitationIsActive; }
			set { _InvitationIsActive = value; }
		}

		/// <summary>
		/// Gets or sets the InvitationCompanyId value.
		/// </summary>
		public Int32 InvitationCompanyId  {
			get { return _InvitationCompanyId; }
			set { _InvitationCompanyId = value; }
		}

		/// <summary>
		/// Gets or sets the InvitationUserId value.
		/// </summary>
		public Int32 InvitationUserId  {
			get { return _InvitationUserId; }
			set { _InvitationUserId = value; }
		}

        public Int32 InvitationToUserId
        {
            get { return _InvitationToUserId; }
            set { _InvitationToUserId = value; }
        }

		/// <summary>
		/// Gets or sets the InvitationSubject value.
		/// </summary>
		public String InvitationSubject  {
			get { return _InvitationSubject; }
			set { _InvitationSubject = value; }
		}

		/// <summary>
		/// Gets or sets the InvitationStartDate value.
		/// </summary>
		public DateTime InvitationStartDate  {
			get { return _InvitationStartDate; }
			set { _InvitationStartDate = value; }
		}

		/// <summary>
		/// Gets or sets the InvitationEndDate value.
		/// </summary>
		public DateTime InvitationEndDate  {
			get { return _InvitationEndDate; }
			set { _InvitationEndDate = value; }
		}

		/// <summary>
		/// Gets or sets the InvitationMessage value.
		/// </summary>
		public String InvitationMessage  {
			get { return _InvitationMessage; }
			set { _InvitationMessage = value; }
		}

        public String invDocName
        {
            get { return _invDocName; }
            set { _invDocName = value; }
        }

	    public string invDocSuggestPath
	    {
	        get { return _invDocSuggestPath; }
	        set { _invDocSuggestPath = value; }
	    }
        public DateTime invDate
        {
            get { return _invDate; }
            set { _invDate = value; }
        }

	    #endregion

		#region Methods
        public Boolean InsertInvition()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[15];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue;

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@invfromUserId";
                param[0].DataType = DbType.Int32;
                if (_invfromUserId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[0].value = _invfromUserId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@invToUserId";
                param[1].DataType = DbType.Int32;
                param[1].value = _invToUserId;


                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@invsubject";
                param[2].DataType = DbType.String;
                if (String.IsNullOrEmpty(_invsubject))
                    param[2].value = DBNull.Value;
                else
                    param[2].value = _invsubject;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@invCreatedDate";
                param[3].DataType = DbType.DateTime;
                if (_invCreatedDate.Ticks > dtmin.Ticks && _invCreatedDate.Ticks < dtmax.Ticks)
                    param[3].value = _invCreatedDate;
                else if (_invCreatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[3].value = _invCreatedDate;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@invUpdatedDate";
                param[4].DataType = DbType.DateTime;
                if (_invUpdatedDate.Ticks > dtmin.Ticks && _invUpdatedDate.Ticks < dtmax.Ticks)
                    param[4].value = _invUpdatedDate;
                else if (_invUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[4].value = _invUpdatedDate;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@invIsDeleted";
                param[5].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_invIsDeleted.ToString()))
                    param[5].value = DBNull.Value;
                else
                    param[5].value = _invIsDeleted;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@invIsActive";
                param[6].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_invIsActive.ToString()))
                    param[6].value = DBNull.Value;
                else
                    param[6].value = _invIsActive;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@invCompanyId";
                param[7].DataType = DbType.Int32;
                if (_invCompanyId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[7].value = _invCompanyId;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@invtype";
                param[8].DataType = DbType.String;
                if (String.IsNullOrEmpty(_invtype))
                    param[8].value = DBNull.Value;
                else
                    param[8].value = _invtype;


                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@invComId";
                param[9].DataType = DbType.Int32;
                param[9].value = _invComId;

                param[10].direction = ParameterDirection.Input;
                param[10].ParamName = "@invStatus";
                param[10].DataType = DbType.Int32;
                param[10].value = _invStatus;

                param[11].direction = ParameterDirection.Input;
                param[11].ParamName = "@invDocName";
                param[11].DataType = DbType.String;
                param[11].value = _invDocName;

                param[12].direction = ParameterDirection.Input;
                param[12].ParamName = "@invDocSuggestPath";
                param[12].DataType = DbType.String;
                param[12].value = _invDocSuggestPath;


                param[13].direction = ParameterDirection.Input;
                param[13].ParamName = "@invHelpTrackId";
                param[13].DataType = DbType.Int32;
                if (_invHelpTrackId == 0)
                {
                    param[13].value = 0;}		//param[8].value = DBNull.Value;
                else
                    param[13].value = _invHelpTrackId;

                param[14].direction = ParameterDirection.Input;
                param[14].ParamName = "@invDate";
                param[14].DataType = DbType.DateTime;
                if (_invDate.Ticks > dtmin.Ticks && _invDate.Ticks < dtmax.Ticks)
                    param[14].value = _invDate;
                else if (_invDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[14].value = _invDate;


                //obj.ExecScalar("InsertNotification", CommandType.StoredProcedure, param); _returnBoolean = true;
                _ds = DBAccess.ExecDataSet("InsertInvition", CommandType.StoredProcedure, param);
                // _returnBoolean = true;
                return true;
            }
            catch (DataException ex)
            {
                return false;
                throw ex;
            }
        } 

        //Saurin  |2015 01 08 |
	    public void GetInvitionStatusTotal()
	    {

	        try
	        {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[6];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@invComId";
                param[0].DataType = DbType.String;
                if (String.IsNullOrEmpty(_invType.ToString()))
                    param[0].value = DBNull.Value;
                else
                    param[0].value = _invComId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@invFromUserId";
                param[1].DataType = DbType.Int32;
                if (_invFromUserId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[1].value = _invFromUserId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@invStatus";
                param[2].DataType = DbType.Int32;
                    param[2].value = _invStatus;


                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@invIsDeleted";
                param[3].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_invIsDeleted.ToString()))
                    param[3].value = DBNull.Value;
                else
                    param[3].value = _invIsDeleted;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@invIsActive";
                param[4].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_invIsActive.ToString()))
                    param[4].value = DBNull.Value;
                else
                    param[4].value = _invIsActive;

                
                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@invType";
                param[5].DataType = DbType.String;
                if (String.IsNullOrEmpty(_invType.ToString()))
                    param[5].value = DBNull.Value;
                else
                    param[5].value = _invType;



             

                _ds = DBAccess.ExecDataSet("GetInvitionStatusTotal", CommandType.StoredProcedure, param);
                _returnBoolean = true;
                
	        }
            catch (DataException ex)
            {
                _returnBoolean = false;
               
            }

	    }

	    public void InsertInvitation() {
			try 
			{
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

				ParamStruct[] param = new ParamStruct[12];
				DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue; 

				param[0].direction = ParameterDirection.Input;
				param[0].ParamName = "@InvitationToEmail";
				param[0].DataType = DbType.String;
				if (String.IsNullOrEmpty(_InvitationToEmail))
					param[0].value = DBNull.Value;
				else
					param[0].value = _InvitationToEmail;

				param[1].direction = ParameterDirection.Input;
				param[1].ParamName = "@InvitationCreatedDate";
				param[1].DataType = DbType.DateTime;
				if (_InvitationCreatedDate.Ticks > dtmin.Ticks && _InvitationCreatedDate.Ticks < dtmax.Ticks)
					param[1].value = _InvitationCreatedDate;
				else if (_InvitationCreatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
				{}
				else
					param[1].value = _InvitationCreatedDate;

				param[2].direction = ParameterDirection.Input;
				param[2].ParamName = "@InvitationUpdatedDate";
				param[2].DataType = DbType.DateTime;
				if (_InvitationUpdatedDate.Ticks > dtmin.Ticks && _InvitationUpdatedDate.Ticks < dtmax.Ticks)
					param[2].value = _InvitationUpdatedDate;
				else if (_InvitationUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
				{}
				else
					param[2].value = _InvitationUpdatedDate;

				param[3].direction = ParameterDirection.Input;
				param[3].ParamName = "@InvitationIsDeleted";
				param[3].DataType = DbType.Boolean;
				if (String.IsNullOrEmpty(_InvitationIsDeleted.ToString()))
					param[3].value = DBNull.Value;
				else
					param[3].value = _InvitationIsDeleted;

				param[4].direction = ParameterDirection.Input;
				param[4].ParamName = "@InvitationIsActive";
				param[4].DataType = DbType.Boolean;
				if (String.IsNullOrEmpty(_InvitationIsActive.ToString()))
					param[4].value = DBNull.Value;
				else
					param[4].value = _InvitationIsActive;

				param[5].direction = ParameterDirection.Input;
				param[5].ParamName = "@InvitationCompanyId";
				param[5].DataType = DbType.Int32;
					if (_InvitationCompanyId == 0)
					{ }		//param[8].value = DBNull.Value;
					else
					param[5].value = _InvitationCompanyId;

				param[6].direction = ParameterDirection.Input;
				param[6].ParamName = "@InvitationUserId";
				param[6].DataType = DbType.Int32;
					if (_InvitationUserId == 0)
					{ }		//param[8].value = DBNull.Value;
					else
					param[6].value = _InvitationUserId;

				param[7].direction = ParameterDirection.Input;
				param[7].ParamName = "@InvitationSubject";
				param[7].DataType = DbType.String;
				if (String.IsNullOrEmpty(_InvitationSubject))
					param[7].value = DBNull.Value;
				else
					param[7].value = _InvitationSubject;

				param[8].direction = ParameterDirection.Input;
				param[8].ParamName = "@InvitationStartDate";
				param[8].DataType = DbType.DateTime;
				if (_InvitationStartDate.Ticks > dtmin.Ticks && _InvitationStartDate.Ticks < dtmax.Ticks)
					param[8].value = _InvitationStartDate;
				else if (_InvitationStartDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
				{}
				else
					param[8].value = _InvitationStartDate;

				param[9].direction = ParameterDirection.Input;
				param[9].ParamName = "@InvitationEndDate";
				param[9].DataType = DbType.DateTime;
				if (_InvitationEndDate.Ticks > dtmin.Ticks && _InvitationEndDate.Ticks < dtmax.Ticks)
					param[9].value = _InvitationEndDate;
				else if (_InvitationEndDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
				{}
				else
					param[9].value = _InvitationEndDate;

				param[10].direction = ParameterDirection.Input;
				param[10].ParamName = "@InvitationMessage";
				param[10].DataType = DbType.String;
				if (String.IsNullOrEmpty(_InvitationMessage))
					param[10].value = DBNull.Value;
				else
					param[10].value = _InvitationMessage;


                param[11].direction = ParameterDirection.Input;
                param[11].ParamName = "@InvitationToUserId";
                param[11].DataType = DbType.Int32;
                param[11].value = _InvitationToUserId;


				//obj.ExecScalar("InsertInvitation", CommandType.StoredProcedure, param);				_returnBoolean = true;
                _ds = DBAccess.ExecDataSet("InsertInvitation", CommandType.StoredProcedure, param);
                _returnBoolean = true;
			}

			catch (DataException ex) 
			{
				_returnBoolean = false;
				throw ex;
			}
		}
       
        public void GetAllInvitionRequest()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[4];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@invToUserId";
                param[0].DataType = DbType.Int32;
                param[0].value = _invToUserId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@invCompanyId";
                param[1].DataType = DbType.Int32;
                param[1].value = _invCompanyId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@invIsDeleted";
                param[2].DataType = DbType.Boolean;
                param[2].value = _invIsDeleted;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@invIsActive";
                param[3].DataType = DbType.Boolean;
                param[3].value = _invIsActive;  

                _ds = DBAccess.ExecDataSet("GetAllInvitionRequest", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllInvitionRequest" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        public void InvitionStatusUpdate()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] pm = new ParamStruct[3];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@invId";
                pm[0].DataType = DbType.Int32;
                pm[0].value = _invId;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@invIsActive";
                pm[1].DataType = DbType.Boolean;
                pm[1].value = _invIsActive;

                pm[2].direction = ParameterDirection.Input;
                pm[2].ParamName = "@invIsDeleted";
                pm[2].DataType = DbType.Boolean;
                pm[2].value = _invIsDeleted;

                _ds = DBAccess.ExecDataSet("InvitionStatusUpdate", CommandType.StoredProcedure, pm);
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in InvitionStatusUpdate  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            }
        }
        public void GetAllInvitionbyid()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@invId";
                param[0].DataType = DbType.Int32;
                param[0].value = _invId;

                _ds = DBAccess.ExecDataSet("GetAllInvitionbyid", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllInvitionbyid" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }


        public void GetHelpRequestedByUserId()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[4];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@invFromUserId";
                param[0].DataType = DbType.Int32;
                param[0].value = _invFromUserId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@invCompanyId";
                param[1].DataType = DbType.Int32;
                param[1].value = _invCompanyId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@invIsDeleted";
                param[2].DataType = DbType.Boolean;
                param[2].value = _invIsDeleted;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@invIsActive";
                param[3].DataType = DbType.Boolean;
                param[3].value = _invIsActive;

                _ds = DBAccess.ExecDataSet("GetHelpRequestedByUserId", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetHelpRequestedByUserId" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void GetHelpProvidedByOtherUser()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[4];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@invToUserId";
                param[0].DataType = DbType.Int32;
                param[0].value = _invToUserId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@invCompanyId";
                param[1].DataType = DbType.Int32;
                param[1].value = _invCompanyId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@invIsDeleted";
                param[2].DataType = DbType.Boolean;
                param[2].value = _invIsDeleted;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@invIsActive";
                param[3].DataType = DbType.Boolean;
                param[3].value = _invIsActive;

               

                _ds = DBAccess.ExecDataSet("GetHelpProvidedByOtherUser", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetHelpRequestedByUserId" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void GetHelpSuggestedToOther()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[4];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@invFromUserId";
                param[0].DataType = DbType.Int32;
                param[0].value = _invFromUserId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@invCompanyId";
                param[1].DataType = DbType.Int32;
                param[1].value = _invCompanyId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@invIsDeleted";
                param[2].DataType = DbType.Boolean;
                param[2].value = _invIsDeleted;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@invIsActive";
                param[3].DataType = DbType.Boolean;
                param[3].value = _invIsActive;



                _ds = DBAccess.ExecDataSet("GetHelpSuggestedToOther", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetHelpRequestedByUserId" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void GetHelpResponseByTrackId()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[5];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@invHelpTrackId";
                param[0].DataType = DbType.Int32;
                param[0].value = _invHelpTrackId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@invCompanyId";
                param[1].DataType = DbType.Int32;
                param[1].value = _invCompanyId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@invIsDeleted";
                param[2].DataType = DbType.Boolean;
                param[2].value = _invIsDeleted;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@invIsActive";
                param[3].DataType = DbType.Boolean;
                param[3].value = _invIsActive;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@invtype";
                param[4].DataType = DbType.String;
                param[4].value = _invType;

                _ds = DBAccess.ExecDataSet("GetHelpResponseByTrackId", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetHelpRequestedByUserId" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }



        public void UpdateInvitionRequest()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[2];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = Convert.ToDateTime("1/1/9999 11:59:59 PM");

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@invId";
                param[0].DataType = DbType.Int32;
                param[0].value = _invId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@invStatus";
                param[1].DataType = DbType.Int32;
                param[1].value = _invStatus;

                _ds = DBAccess.ExecDataSet("UpdateInvitionRequest", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }
        public void GetHelpReplyForUserId()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[4];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@invFromUserId";
                param[0].DataType = DbType.Int32;
                param[0].value = _invFromUserId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@invCompanyId";
                param[1].DataType = DbType.Int32;
                param[1].value = _invCompanyId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@invIsDeleted";
                param[2].DataType = DbType.Boolean;
                param[2].value = _invIsDeleted;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@invIsActive";
                param[3].DataType = DbType.Boolean;
                param[3].value = _invIsActive;

                _ds = DBAccess.ExecDataSet("GetHelpReplyForUserId", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetHelpRequestedByUserId" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void GetAcceptedHelp(Int32 fromUserid, Int32 compId, Int32 userID, int status, string type)
	    {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[5];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@fromUserid";
                param[0].DataType = DbType.Int32;
                param[0].value = fromUserid;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@compId";
                param[1].DataType = DbType.Int32;
                param[1].value = compId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@userID";
                param[2].DataType = DbType.Int32;
                param[2].value = userID;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@status";
                param[3].DataType = DbType.Int32;
                param[3].value = status;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@type";
                param[4].DataType = DbType.String;
                param[4].value = type;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@type";
                param[4].DataType = DbType.String;
                param[4].value = type;



                _ds = DBAccess.ExecDataSet("GetAcceptedHelp", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAcceptedHelp" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
	    }
		#endregion

	    public void GetAllInvitationByUserId()
	    {
            DataAccess DBAccess = new DataAccess();
            DBAccess.Provider = EnumProviders.SQLClient;
            DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
            ParamStruct[] param = new ParamStruct[3];

            param[0].direction = ParameterDirection.Input;
            param[0].ParamName = "@userId";
            param[0].DataType = DbType.Int32;
            param[0].value = _InvitationUserId;

            param[1].direction = ParameterDirection.Input;
            param[1].ParamName = "@isActive";
            param[1].DataType = DbType.Boolean;
            param[1].value = _InvitationIsActive;

            param[2].direction = ParameterDirection.Input;
            param[2].ParamName = "@isDelete";
            param[2].DataType = DbType.Boolean;
            param[2].value = _InvitationIsDeleted;

            _ds = DBAccess.ExecDataSet("GetAllInvitationByUserId", CommandType.StoredProcedure, param);

	    }
	}
}
