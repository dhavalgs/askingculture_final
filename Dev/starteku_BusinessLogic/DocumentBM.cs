﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WatzOnTV.Logic;
using starteku.Data.Manager;
using System.Data;
using starteku_BusinessLogic.Model;
using starteku_BusinessLogic.View;

namespace starteku_BusinessLogic
{
    public class DocumentBM
    {

        #region Private Declaration

        private DataSet _ds;
        private string _returnString;
        private bool _returnBoolean;
        private Int32 _docId;
        private String _docTitle;
        private String _docDescription;
        private String _docattachment;
        private String _docType;
        private Int32 _docUserId;
        private DateTime _docCreatedDate;
        private DateTime _docUpdatedDate;
        private Boolean _docIsDeleted;
        private Boolean _docIsActive;
        private Int32 _docCompanyId;
        private String _docCreateBy;
        private Int32 _docDepId;
        private Boolean _docApprovedStatus;
        private String _doccompetence;
        #endregion


        #region Properties

        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public DataSet ds
        {
            get { return _ds; }
            set { _ds = value; }
        }
        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public string ReturnString
        {
            get { return _returnString; }
            set { _returnString = value; }
        }
        public string doccompetence
        {
            get { return _doccompetence; }
            set { _doccompetence = value; }
        }
        /// <summary>
        /// Gets or sets the ReturnBoolean value.
        /// </summary>
        public bool ReturnBoolean
        {
            get { return _returnBoolean; }
            set { _returnBoolean = value; }
        }

        /// <summary>
        /// Gets or sets the docId value.
        /// </summary>
        public Int32 docId
        {
            get { return _docId; }
            set { _docId = value; }
        }

        /// <summary>
        /// Gets or sets the docTitle value.
        /// </summary>
        public String docTitle
        {
            get { return _docTitle; }
            set { _docTitle = value; }
        }

        /// <summary>
        /// Gets or sets the docDescription value.
        /// </summary>
        public String docDescription
        {
            get { return _docDescription; }
            set { _docDescription = value; }
        }

        /// <summary>
        /// Gets or sets the docattachment value.
        /// </summary>
        public String docattachment
        {
            get { return _docattachment; }
            set { _docattachment = value; }
        }

        /// <summary>
        /// Gets or sets the docType value.
        /// </summary>
        public String docType
        {
            get { return _docType; }
            set { _docType = value; }
        }

        /// <summary>
        /// Gets or sets the docUserId value.
        /// </summary>
        public Int32 docUserId
        {
            get { return _docUserId; }
            set { _docUserId = value; }
        }

        /// <summary>
        /// Gets or sets the docCreatedDate value.
        /// </summary>
        public DateTime docCreatedDate
        {
            get { return _docCreatedDate; }
            set { _docCreatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the docUpdatedDate value.
        /// </summary>
        public DateTime docUpdatedDate
        {
            get { return _docUpdatedDate; }
            set { _docUpdatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the docIsDeleted value.
        /// </summary>
        public Boolean docIsDeleted
        {
            get { return _docIsDeleted; }
            set { _docIsDeleted = value; }
        }

        /// <summary>
        /// Gets or sets the docIsActive value.
        /// </summary>
        public Boolean docIsActive
        {
            get { return _docIsActive; }
            set { _docIsActive = value; }
        }

        /// <summary>
        /// Gets or sets the docCompanyId value.
        /// </summary>
        public Int32 docCompanyId
        {
            get { return _docCompanyId; }
            set { _docCompanyId = value; }
        }

        /// <summary>
        /// Gets or sets the docCreateBy value.
        /// </summary>
        public String docCreateBy
        {
            get { return _docCreateBy; }
            set { _docCreateBy = value; }
        }

        /// <summary>
        /// Gets or sets the docDepId value.
        /// </summary>
        public Int32 docDepId
        {
            get { return _docDepId; }
            set { _docDepId = value; }
        }

        /// <summary>
        /// Gets or sets the docApprovedStatus value.
        /// </summary>
        public Boolean docApprovedStatus
        {
            get { return _docApprovedStatus; }
            set { _docApprovedStatus = value; }
        }

        #endregion

        #region Methods
       
        public void GetAllDocument()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@docIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _docIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@docIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _docIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@CorrentDateTime";
                param[2].DataType = DbType.DateTime;
                param[2].value = DateTime.Now;

               

                _ds = DBAccess.ExecDataSet("GetAllDocument", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllDocument" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }





        public void InsertDocument()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[14];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue; 

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@docTitle";
                param[0].DataType = DbType.String;
                if (String.IsNullOrEmpty(_docTitle))
                    param[0].value = DBNull.Value;
                else
                    param[0].value = _docTitle;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@docDescription";
                param[1].DataType = DbType.String;
                if (String.IsNullOrEmpty(_docDescription))
                    param[1].value = DBNull.Value;
                else
                    param[1].value = _docDescription;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@docattachment";
                param[2].DataType = DbType.String;
                if (String.IsNullOrEmpty(_docattachment))
                    param[2].value = DBNull.Value;
                else
                    param[2].value = _docattachment;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@docType";
                param[3].DataType = DbType.String;
                if (String.IsNullOrEmpty(_docType))
                    param[3].value = DBNull.Value;
                else
                    param[3].value = _docType;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@docUserId";
                param[4].DataType = DbType.Int32;
                param[4].value = _docUserId;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@docCreatedDate";
                param[5].DataType = DbType.DateTime;
                if (_docCreatedDate.Ticks > dtmin.Ticks && _docCreatedDate.Ticks < dtmax.Ticks)
                    param[5].value = _docCreatedDate;
                else if (_docCreatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[5].value = _docCreatedDate;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@docUpdatedDate";
                param[6].DataType = DbType.DateTime;
                if (_docUpdatedDate.Ticks > dtmin.Ticks && _docUpdatedDate.Ticks < dtmax.Ticks)
                    param[6].value = _docUpdatedDate;
                else if (_docUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[6].value = _docUpdatedDate;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@docIsDeleted";
                param[7].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_docIsDeleted.ToString()))
                    param[7].value = DBNull.Value;
                else
                    param[7].value = _docIsDeleted;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@docIsActive";
                param[8].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_docIsActive.ToString()))
                    param[8].value = DBNull.Value;
                else
                    param[8].value = _docIsActive;

                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@docCompanyId";
                param[9].DataType = DbType.Int32;
                param[9].value = _docCompanyId;

                param[10].direction = ParameterDirection.Input;
                param[10].ParamName = "@docCreateBy";
                param[10].DataType = DbType.String;
                if (String.IsNullOrEmpty(_docCreateBy))
                    param[10].value = DBNull.Value;
                else
                    param[10].value = _docCreateBy;

                param[11].direction = ParameterDirection.Input;
                param[11].ParamName = "@docDepId";
                param[11].DataType = DbType.Int32;
                if (_docDepId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[11].value = _docDepId;

                param[12].direction = ParameterDirection.Input;
                param[12].ParamName = "@docApprovedStatus";
                param[12].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_docApprovedStatus.ToString()))
                    param[12].value = DBNull.Value;
                else
                    param[12].value = _docApprovedStatus;

                param[13].direction = ParameterDirection.Input;
                param[13].ParamName = "@doccompetence";
                param[13].DataType = DbType.String;
                if (String.IsNullOrEmpty(_doccompetence))
                    param[13].value = DBNull.Value;
                else
                    param[13].value = _doccompetence;


                _ds = DBAccess.ExecDataSet("InsertDocument", CommandType.StoredProcedure, param);
                _returnBoolean = true;

            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }


        public void InsertDocument(string docFileNameFriendly, string docFileNameInSystem, string docAttachmentName, string docTypes, int userId, DateTime docCreatedDt, DateTime docUpdatedDt, bool docIsDel, bool docIsActiv
           , int docCompnyId, string docCreatedBy, int docDepIds, string docTitl, string doccompetenc, bool docApprovedStatu, string docDivisionIds, string docJobTypeIds, bool docIsPublic, string keyword, string repository, string description, int docShareToId, int docCatId, string docCategory)
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[23];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue;

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@docFileName_Friendly";
                param[0].DataType = DbType.String;
                if (String.IsNullOrEmpty(docFileNameFriendly))
                    param[0].value = DBNull.Value;
                else
                    param[0].value = docFileNameFriendly;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@docFileName_inSystem";
                param[1].DataType = DbType.String;
                if (String.IsNullOrEmpty(docFileNameInSystem))
                    param[1].value = DBNull.Value;
                else
                    param[1].value = docFileNameInSystem;


                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@docattachment";
                param[2].DataType = DbType.String;
                if (String.IsNullOrEmpty(docAttachmentName))
                    param[2].value = DBNull.Value;
                else
                    param[2].value = docAttachmentName;



                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@docType";
                param[3].DataType = DbType.String;
                if (String.IsNullOrEmpty(docTypes))
                    param[3].value = DBNull.Value;
                else
                    param[3].value = docTypes;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@docUserId";
                param[4].DataType = DbType.Int32;
                param[4].value = userId;



                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@docCreatedDate";
                param[5].DataType = DbType.DateTime;
                if (docCreatedDt.Ticks > dtmin.Ticks && docCreatedDt.Ticks < dtmax.Ticks)
                    param[5].value = docCreatedDt;
                else if (docCreatedDt.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[5].value = docCreatedDt;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@docUpdatedDate";
                param[6].DataType = DbType.DateTime;
                if (docUpdatedDt.Ticks > dtmin.Ticks && docUpdatedDt.Ticks < dtmax.Ticks)
                    param[6].value = docUpdatedDt;
                else if (docUpdatedDt.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[6].value = docUpdatedDt;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@docIsDeleted";
                param[7].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(docIsDeleted.ToString()))
                    param[7].value = DBNull.Value;
                else
                    param[7].value = docIsDeleted;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@docIsActive";
                param[8].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(docIsActiv.ToString()))
                    param[8].value = DBNull.Value;
                else
                    param[8].value = docIsActiv;

                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@docCompanyId";
                param[9].DataType = DbType.Int32;
                param[9].value = docCompnyId;

                param[10].direction = ParameterDirection.Input;
                param[10].ParamName = "@docCreateBy";
                param[10].DataType = DbType.String;
                if (String.IsNullOrEmpty(docCreateBy))
                    param[10].value = DBNull.Value;
                else
                    param[10].value = docCreatedBy;

                param[11].direction = ParameterDirection.Input;
                param[11].ParamName = "@docDepId";
                param[11].DataType = DbType.Int32;
                param[11].value = docDepIds;

                param[12].direction = ParameterDirection.Input;
                param[12].ParamName = "@docApprovedStatus";
                param[12].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(docApprovedStatus.ToString()))
                    param[12].value = DBNull.Value;
                else
                    param[12].value = docApprovedStatus;

                param[13].direction = ParameterDirection.Input;
                param[13].ParamName = "@doccompetence";
                param[13].DataType = DbType.String;
                if (String.IsNullOrEmpty(doccompetence))
                    param[13].value = DBNull.Value;
                else
                    param[13].value = doccompetence;


                param[14].direction = ParameterDirection.Input;
                param[14].ParamName = "@docRepository";
                param[14].DataType = DbType.String;
                if (String.IsNullOrEmpty(repository))
                    param[14].value = DBNull.Value;
                else
                    param[14].value = repository;


                param[15].direction = ParameterDirection.Input;
                param[15].ParamName = "@docDivisionIds";
                param[15].DataType = DbType.String;
                if (String.IsNullOrEmpty(docDivisionIds))
                    param[15].value = DBNull.Value;
                else
                    param[15].value = docDivisionIds;



                param[16].direction = ParameterDirection.Input;
                param[16].ParamName = "@docJobTypeIds";
                param[16].DataType = DbType.String;
                if (String.IsNullOrEmpty(docJobTypeIds))
                    param[16].value = DBNull.Value;
                else
                    param[16].value = docJobTypeIds;



                param[17].direction = ParameterDirection.Input;
                param[17].ParamName = "@docIsPublic";
                param[17].DataType = DbType.Boolean;
                param[17].value = docIsPublic;

                param[18].direction = ParameterDirection.Input;
                param[18].ParamName = "@docKeywords";
                param[18].DataType = DbType.String;
                if (String.IsNullOrEmpty(keyword))
                    param[18].value = DBNull.Value;
                else
                    param[18].value = keyword;

                param[19].direction = ParameterDirection.Input;
                param[19].ParamName = "@docDescription";
                param[19].DataType = DbType.String;
                if (String.IsNullOrEmpty(description))
                    param[19].value = DBNull.Value;
                else
                    param[19].value = description;

                param[20].direction = ParameterDirection.Input;
                param[20].ParamName = "@docShareToId";
                param[20].DataType = DbType.Int32;
                param[20].value = docShareToId;

                param[21].direction = ParameterDirection.Input;
                param[21].ParamName = "@docCatId";
                param[21].DataType = DbType.Int32;
                param[21].value = docCatId;


                param[22].direction = ParameterDirection.Input;
                param[22].ParamName = "@docCategory";
                param[22].DataType = DbType.String;
                param[22].value = docCategory;


                _ds = DBAccess.ExecDataSet("InsertDocument", CommandType.StoredProcedure, param);
                _returnBoolean = true;

            }
            catch (DataException ex)
            {
                 _returnBoolean = false;
                throw ex;
            }
        }

        public void UpdateDocument()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[14];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue; 

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@docId";
                param[0].DataType = DbType.Int32;
                param[0].value = _docId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@docTitle";
                param[1].DataType = DbType.String;
                if (String.IsNullOrEmpty(_docTitle))
                    param[1].value = DBNull.Value;
                else
                    param[1].value = _docTitle;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@docDescription";
                param[2].DataType = DbType.String;
                if (String.IsNullOrEmpty(_docDescription))
                    param[2].value = DBNull.Value;
                else
                    param[2].value = _docDescription;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@docattachment";
                param[3].DataType = DbType.String;
                if (String.IsNullOrEmpty(_docattachment))
                    param[3].value = DBNull.Value;
                else
                    param[3].value = _docattachment;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@docType";
                param[4].DataType = DbType.String;
                if (String.IsNullOrEmpty(_docType))
                    param[4].value = DBNull.Value;
                else
                    param[4].value = _docType;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@docUserId";
                param[5].DataType = DbType.Int32;
                if (_docUserId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[5].value = _docUserId;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@docCreatedDate";
                param[6].DataType = DbType.DateTime;
                if (_docCreatedDate.Ticks > dtmin.Ticks && _docCreatedDate.Ticks < dtmax.Ticks)
                    param[6].value = _docCreatedDate;
                else if (_docCreatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[6].value = _docCreatedDate;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@docUpdatedDate";
                param[7].DataType = DbType.DateTime;
                if (_docUpdatedDate.Ticks > dtmin.Ticks && _docUpdatedDate.Ticks < dtmax.Ticks)
                    param[7].value = _docUpdatedDate;
                else if (_docUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[7].value = _docUpdatedDate;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@docIsDeleted";
                param[8].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_docIsDeleted.ToString()))
                    param[8].value = DBNull.Value;
                else
                    param[8].value = _docIsDeleted;

                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@docIsActive";
                param[9].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_docIsActive.ToString()))
                    param[9].value = DBNull.Value;
                else
                    param[9].value = _docIsActive;

                param[10].direction = ParameterDirection.Input;
                param[10].ParamName = "@docCompanyId";
                param[10].DataType = DbType.Int32;
                if (_docCompanyId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[10].value = _docCompanyId;

                param[11].direction = ParameterDirection.Input;
                param[11].ParamName = "@docCreateBy";
                param[11].DataType = DbType.String;
                if (String.IsNullOrEmpty(_docCreateBy))
                    param[11].value = DBNull.Value;
                else
                    param[11].value = _docCreateBy;

                param[12].direction = ParameterDirection.Input;
                param[12].ParamName = "@docDepId";
                param[12].DataType = DbType.Int32;
                if (_docDepId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[12].value = _docDepId;

                param[13].direction = ParameterDirection.Input;
                param[13].ParamName = "@docApprovedStatus";
                param[13].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_docApprovedStatus.ToString()))
                    param[13].value = DBNull.Value;
                else
                    param[13].value = _docApprovedStatus;


                _ds = DBAccess.ExecDataSet("UpdateDocument", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        public void DeleteDocumentById()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@docId";
                param[0].DataType = DbType.Int32;
                param[0].value = _docId;


                _ds = DBAccess.ExecDataSet("DeleteDocumentById", CommandType.StoredProcedure, param);
                _returnBoolean = true;

            }
            catch (Exception ex)
            {

            }

        }
        public void GetDocumentById()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@docId";
                param[0].DataType = DbType.Int32;
                param[0].value = _docId;


                _ds = DBAccess.ExecDataSet("GetDocumentById", CommandType.StoredProcedure, param);
                _returnBoolean = true;

            }
            catch (Exception ex)
            {

            }

        }
        public static List<DocumentView> GetDocumentById(int docId)
        {
            try
            {
                DocumentBM obj = new DocumentBM();
                obj.docId = docId;
                obj.GetDocumentById();
                DataSet ds = obj.ds;

                var userDetailInList = ds.Tables[0].AsEnumerable().Select(r => new DocumentView()
                {
                    DocId = r.Field<int>("docId"),
                    UserId = r.Field<int>("docUserId"),
                    IsPublic = r.Field<bool>("docIsPublic"),
                    CompanyId = r.Field<int>("docCompanyId")

                });
                return userDetailInList.ToList(); // For if you really need a List and not IEnumerable

            }
            catch (Exception ex)
            {
                return null;
            }

        }

        #endregion
        #region Linq
        public static List<DocumentView> GetAllDocumentsLinq(int CompanyId)
        { var db = new startetkuEntities1();
            var documentList = new List<DocumentView>();
            try
            {
              
                if (CompanyId != 0)
                {
                    var documents = (from o in db.GetAllDocument(true,false,DateTime.Now) where o.docCompanyId == CompanyId select o).ToList();
                 

                    if (documents != null && documents.Any())
                    {
                        foreach (var i in documents)
                        {
                            documentList.Add(GetDocumentView(i));
                        }
                    }
                    else
                    {
                       
                    }
                }

            }
            catch (Exception e)
            {

                ExceptionLogger.LogException(e);
            }
            finally
            {
                db.Dispose();
            }
            return documentList;
        }

        private static DocumentView GetDocumentView(GetAllDocument_Result document)
        {
            var v = new DocumentView();
            try
            {
                v.DocId = document.docId;
                v.docCompanyId = document.docCompanyId;
                v.docCreateBy = document.docCreateBy;
                v.docCreatedDate = document.docCreatedDate;
                v.docDepId = document.docDepId;
                v.docDescription = document.docDescription;
                v.docDivisionIds = document.docDivisionIds;
                v.docApprovedStatus = document.docApprovedStatus;
                v.docFileName_Friendly = document.docFileName_Friendly;
                v.docFileName_inSystem = document.docFileName_inSystem;
                v.docType = document.docType;
                v.docJobTypeIds = document.docJobTypeIds;
                v.docattachment = document.docattachment;
                v.docKeywords = document.docKeywords;
                v.MinAgo = document.MinAgo;// CommonUtilities.GetAgeAgo(DateTime.Now, Convert.ToDateTime(v.docCreatedDate));
                v.docUserID = Convert.ToInt32(document.docUserId);
                v.FullName = document.FullName;
                v.docRepository = document.docRepository;
                v.doccompetence = document.doccompetence;
                v.docCatId = Convert.ToInt32( document.docCatId );
            }
            catch (Exception e)
            {
                    
                ExceptionLogger.LogException(e);
            }
            return v;
        }

        #endregion
    }
}
