﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;
using starteku_BusinessLogic.Model;

namespace starteku_BusinessLogic
{
    public class CommonUtilities
    {
        public enum TypeOfEmails
        {
            ContactUS,
            ChangedPassword,
            FeedBack,
            ForgotPassword,
            WatzOnTV,

        }
        public static string GetAgeAgo(DateTime dt1, DateTime dt2)
        {

            TimeSpan diff = DateTime.Now - DateTime.Today;
            string formatted = string.Format(
                                   CultureInfo.CurrentCulture,
                                   "{0} days, {1} hours, {2} minutes ago",
                                   diff.Days,
                                   diff.Hours,
                                   diff.Minutes,
                                   diff.Seconds);
            return formatted;
        }
        public static string WebPlatform = "Web";
        //public static string ChannelTableName = "channel";
        public static string ChannelTableName = "s";
        internal static string FormatEmail(string body, string fullName, string emailAddress, string subject, string message)
        {
            try
            {


                body = body.Replace("{FullName}", fullName);
                body = body.Replace("{EmailAddress}", emailAddress);
                body = body.Replace("{Subject}", subject);
                body = body.Replace("{Message}", message);
            }
            catch (Exception)
            {


            }
            body = body.Replace("\n", "<br />");
            return body;
        }
        //Saurin: 201301028 
        public static string GetMobileIPAddress()
        {
            try
            {
                string ip = string.Empty;
                try
                {
                    if (HttpContext.Current.Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        ip = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                }
                catch { }
                if (string.IsNullOrEmpty(ip))
                {
                    ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                }
                try
                {
                    if (string.IsNullOrEmpty(ip))
                    {
                        ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_CLIENT_IP"];
                    }
                }
                catch (Exception)
                {


                }

                if (string.IsNullOrEmpty(ip))
                {
                    ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                }
                //Get your local DNS Host
                if (string.IsNullOrEmpty(ip))
                {
                    ip = HttpContext.Current.Request.UserHostAddress; ;
                }



                ip = GetLastIPAddress(ip);
                return ip;

            }
            catch (Exception ex)
            {
                return "0.0.0.0";
            }


        }
        public static string GetLastIPAddress(string ipAddress)
        {
            try
            {
                var myLastIp = "";
                var mulitipleIp = ipAddress.Split(',');
                foreach (var s in mulitipleIp)
                {
                    if (!string.IsNullOrEmpty(s))
                        myLastIp = s.Trim();
                }
                if (string.IsNullOrWhiteSpace(myLastIp))
                    myLastIp = ipAddress;
                return myLastIp;
            }
            catch (Exception)
            {
                return ipAddress;
            }
        }
        public static string GetIPAddress()
        {
            try
            {
                string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (string.IsNullOrEmpty(ip))
                {
                    ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                }
                if (ip == "::1") ip = GetMobileIPAddress();
                ip = GetLastIPAddress(ip);
                return ip;


            }
            catch (Exception ex)
            {
                return "0.0.0.0";
            }


        }
        public static void GetEmailTemplate(TypeOfEmails templateName, out string subject, out string body)
        {

            body = "";
            subject = templateName.ToString();
            var db = new startetkuEntities1();
            try
            {
                var optionMaster = db.Cms.SingleOrDefault(o => o.cmsName == templateName.ToString());

                if (optionMaster != null)
                {
                    body = optionMaster.cmsDescription;
                    subject = subject;
                    CommonAttributes.DisposeDBObject(ref db);
                    return;
                }
                else
                {
                    body = "";
                    subject = templateName.ToString();
                }
            }
            catch (Exception exp)
            {
                ExceptionLogger.LogException(exp);
            }
            finally
            {
                CommonAttributes.DisposeDBObject(ref db);
            }

        }

        /// <summary>
        /// Sends an mail message
        /// </summary>
        /// <param name="to">Recepient address</param>
        /// <param name="subject">Subject of mail message</param>
        /// /// <param name="template">Email body template</param>
        /// <param name="values">Email's tokens in pairs: key + value + key + value...</param>
        public static void SendEmail(string fromEmail, string fromEmailName, string to, string cc, string subject, string body)
        {
            try
            {
                //TODO:HACK:
                //remove return after UAT is finished
                //return;
                // Instantiate a new instance of MailMessage
                var mMailMessage = new MailMessage();

                mMailMessage.From = new MailAddress(fromEmail, fromEmailName);
                // Set the recepient address of the mail message
                mMailMessage.To.Add(new MailAddress(to));
                string[] ccEmails = (cc + ";").Split(';');
                foreach (var ccEmail in ccEmails)
                {
                    if (!string.IsNullOrEmpty(ccEmail))
                    {
                        mMailMessage.CC.Add(new MailAddress(ccEmail));
                    }
                }

                // Set the subject of the mail message
                mMailMessage.Subject = subject;
                mMailMessage.IsBodyHtml = true;
                // Set the body of the mail message
                mMailMessage.Body = body;

                // Instantiate a new instance of SmtpClient
                SmtpClient mSmtpClient = new SmtpClient();
                mSmtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                var port = GetConfigValue("SMTPPort");
                if (!string.IsNullOrEmpty(port) && port != "0")
                {
                    mSmtpClient.Port = int.Parse(port);
                }
                mSmtpClient.UseDefaultCredentials = bool.Parse(GetConfigValue("SMTPUseDefaultCredentials"));
                mSmtpClient.Credentials = new System.Net.NetworkCredential
                 (GetConfigValue("SMTPUserName"), GetConfigValue("SMTPPassword"));

                mSmtpClient.EnableSsl = bool.Parse(GetConfigValue("SMTPEnableSsl"));
                mSmtpClient.Host = GetConfigValue("SMTPHost");

                // Send the mail message
                mSmtpClient.Send(mMailMessage);
            }
            catch (Exception exp)
            {
                ExceptionLogger.LogException(exp);
            }

        }
        public static int RemoveAllJobCache()
        {

            try
            {
                var i = 0;
                foreach (System.Collections.DictionaryEntry entry in HttpContext.Current.Cache)
                {
                    i++;
                    HttpContext.Current.Cache.Remove((string)entry.Key);
                }
                return i;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public static string GetConfigValue(string configKey, string defaultVal = "")
        {
            var db = new startetkuEntities1();
            var cacheKey = string.Format("GetConfigValue-{0}-{1}-{2}-{3}-{4}", configKey, 1, 2, 3, 4);
            var cacheItem = HttpRuntime.Cache.Get(cacheKey) as string;
            try
            {



                if (cacheItem == null)
                {
                    var optionMaster = db.OptionSwitchers.SingleOrDefault(o => o.keyName == configKey);

                    if (optionMaster == null)
                    {

                        optionMaster = new OptionSwitcher();
                        optionMaster.CreatedDate = DateTime.Now;
                        optionMaster.keyName = configKey;
                        optionMaster.value = defaultVal;
                        db.OptionSwitchers.Add(optionMaster);
                        db.SaveChanges();
                    }
                    cacheItem = optionMaster.value;
                    HttpRuntime.Cache.Insert(cacheKey, cacheItem, null, DateTime.Now.AddDays(100), TimeSpan.Zero);
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
            }
            CommonAttributes.DisposeDBObject(ref db);
            return cacheItem;

        }

        public static string TimeFormat(int seconds)
        {
            TimeSpan ts = new TimeSpan(0, 0, 0, seconds, 0);
            int Hours = (int)ts.TotalHours;
            return string.Format("{0:00}:{1:00}:{2:00}", Hours, ts.Minutes, ts.Seconds);
            //return string.Format("{0}:{1}:{2}", Hours, ts.Minutes, ts.Seconds);
        }

        public static DateTime GetCurrentDateTime()
        {
            return DateTime.UtcNow;
        }

        public static string GetAssemblyInfo()
        {
            try
            {

                //Version version = Assembly.GetExecutingAssembly().GetName().Version;
                Version version = Assembly.GetCallingAssembly().GetName().Version;

                return string.Format("V {0}", version.ToString());
            }
            catch (Exception e)
            {
                return string.Empty;
            }

        }

        //public static string GenerateQrCode(string textdata)
        //{
        //    try
        //    {

        //        BitMatrix Matrix;
        //        var encoder = new QrEncoder();
        //        QrCode qr;
        //        if (!encoder.TryEncode(textdata, out qr))
        //        {
        //            return string.Empty;
        //        }
        //        Matrix = qr.Matrix;
        //        MemoryStream ms = new MemoryStream();
        //        var render = new GraphicsRenderer(new FixedModuleSize(12, QuietZoneModules.Zero));
        //        render.WriteToStream(Matrix, System.Drawing.Imaging.ImageFormat.Bmp, ms);
        //        byte[] imageBytes = ms.ToArray();
        //        string base64String = Convert.ToBase64String(imageBytes);
        //        return base64String;
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogger.LogException(ex);
        //        return string.Empty;
        //    }
        //}
        public static bool IsMacOS(string userAgent)
        {
            var osInfo = userAgent.Split(new Char[] { '(', ')' })[1];
            return osInfo.Contains("Mac_PowerPC") || osInfo.Contains("Macintosh") || osInfo.Contains("Mac OS");
        }

        public static string GetOSInfo(HttpRequest httpRequest)
        {
            try
            {


                string OSName = string.Empty;
                OSName = httpRequest.Browser.Platform;
                if (httpRequest.UserAgent != null)
                {
                    string userAgent = httpRequest.UserAgent;
                    if (httpRequest.Browser.Platform.Contains("WinNT"))
                    {
                        if (userAgent.Contains("Windows NT 6.0"))
                            OSName = "Vista";
                        else if (userAgent.Contains("Windows NT 6.1"))
                            OSName = "Windows 7";
                        else if (userAgent.Contains("Windows NT 6.1"))
                            OSName = "Windows 7";
                        else if (userAgent.Contains("Windows NT 6.2"))
                            OSName = "Windows 8";
                        else if (userAgent.Contains("Windows NT 6.3"))
                            OSName = "Windows 8.1";
                        else if (userAgent.Contains("Windows NT 6.4"))
                            OSName = "Windows 8.2";

                        else if (userAgent.Contains("Windows NT 5.2"))
                            OSName = "Windows Server 2003";
                        else if (userAgent.Contains("Windows NT 5.1"))
                            OSName = "Windows XP";
                        else if (userAgent.Contains("Windows NT 5.0"))
                            OSName = "Windows Server 2000";

                        else
                        {
                            OSName = httpRequest.Browser.Platform;
                        }
                    }
                    else
                    {
                        if (IsMacOS(httpRequest.UserAgent))
                        {
                            OSName = "Macintosh";
                        }
                        if (userAgent.ToLower().Contains("iphone;"))
                        {
                            OSName = "iPhone";
                        }
                        if (userAgent.ToLower().Contains("iPad;"))
                        {
                            OSName = "iPad";
                        }
                    }
                }
                return OSName;
            }
            catch (Exception)
            {

                return string.Empty;
            }
        }
        //saurin : 20132910 : 
        public static string GetServerOSInfo()
        {
            try
            {


                //Get Operating system information.
                OperatingSystem os = Environment.OSVersion;
                //Get version information about the os.
                Version vs = os.Version;

                //Variable to hold our return value
                if (os.ToString().ToLower().Contains("win"))
                {
                    string operatingSystem = "";

                    if (os.Platform == PlatformID.Win32Windows)
                    {
                        //This is a pre-NT version of Windows
                        switch (vs.Minor)
                        {
                            case 0:
                                operatingSystem = "95";
                                break;
                            case 10:
                                if (vs.Revision.ToString() == "2222A")
                                    operatingSystem = "98SE";
                                else
                                    operatingSystem = "98";
                                break;
                            case 90:
                                operatingSystem = "Me";
                                break;
                            default:
                                break;
                        }
                    }
                    else if (os.Platform == PlatformID.Win32NT)
                    {
                        switch (vs.Major)
                        {
                            case 3:
                                operatingSystem = "NT 3.51";
                                break;
                            case 4:
                                operatingSystem = "NT 4.0";
                                break;
                            case 5:
                                if (vs.Minor == 0)
                                    operatingSystem = "2000";
                                else if (vs.Minor == 1)
                                    operatingSystem = "XP";
                                else if (vs.Minor == 2)
                                    operatingSystem = "XP 64-Bit edition";
                                break;
                            case 6:
                                if (vs.Minor == 0)
                                    operatingSystem = "Vista";
                                else if (vs.Minor == 1)
                                    operatingSystem = "7";
                                else if (vs.Minor == 2)
                                    operatingSystem = "8";
                                else if (vs.Minor == 3)
                                    operatingSystem = "8.1";
                                break;
                            default:
                                break;
                        }
                    }
                    //Make sure we actually got something in our OS check
                    //We don't want to just return " Service Pack 2" or " 32-bit"
                    //That information is useless without the OS version.
                    if (operatingSystem != "")
                    {
                        //Got something.  Let's prepend "Windows" and get more info.
                        operatingSystem = "Windows " + operatingSystem;
                        //See if there's a service pack installed.
                        if (os.ServicePack != "")
                        {
                            //Append it to the OS name.  i.e. "Windows XP Service Pack 3"
                            operatingSystem += " " + os.ServicePack;
                        }
                        //Append the OS architecture.  i.e. "Windows XP Service Pack 3 32-bit"
                        operatingSystem += " " + (Environment.Is64BitOperatingSystem ? 64 : 32).ToString() + "-bit";
                    }
                    //Return the information we've gathered.
                    return operatingSystem;
                }
                return os.ToString();
            }
            catch (Exception)
            {


            }
            return string.Empty;
        }
        /*saurin : 20140101 : for reset password , to check user is on mobile/tab or computer */
        public static bool IsMobileBrowser()
        {
            try
            {



                //GETS THE CURRENT USER CONTEXT
                HttpContext context = HttpContext.Current;

                //FIRST TRY BUILT IN ASP.NT CHECK
                if (context.Request.Browser.IsMobileDevice)
                {
                    return true;
                }
                //THEN TRY CHECKING FOR THE HTTP_X_WAP_PROFILE HEADER
                if (context.Request.ServerVariables["HTTP_X_WAP_PROFILE"] != null)
                {
                    return true;
                }
                //THEN TRY CHECKING THAT HTTP_ACCEPT EXISTS AND CONTAINS WAP
                if (context.Request.ServerVariables["HTTP_ACCEPT"] != null &&
                    context.Request.ServerVariables["HTTP_ACCEPT"].ToLower().Contains("wap"))
                {
                    return true;
                }

                //saurin : 

                //AND FINALLY CHECK THE HTTP_USER_AGENT 
                //HEADER VARIABLE FOR ANY ONE OF THE FOLLOWING
                if (context.Request.ServerVariables["HTTP_USER_AGENT"] != null)
                {
                    //Create a list of all mobile types
                    var mobiles =
                        new[]
                {
                    "midp", "j2me", "avant", "docomo", 
                    "novarra", "palmos", "palmsource", 
                    "240x320", "opwv", "chtml",
                    "pda", "windows ce", "mmp/", 
                    "blackberry", "mib/", "symbian", 
                    "wireless", "nokia", "hand", "mobi",
                    "phone", "cdm", "up.b", "audio", 
                    "SIE-", "SEC-", "samsung", "HTC", 
                    "mot-", "mitsu", "sagem", "sony"
                    , "alcatel", "lg", "eric", "vx", 
                    "NEC", "philips", "mmm", "xx", 
                    "panasonic", "sharp", "wap", "sch",
                    "rover", "pocket", "benq", "java", 
                    "pt", "pg", "vox", "amoi", 
                    "bird", "compal", "kg", "voda","vodafone",
                    "sany", "kdd", "dbt", "sendo", 
                    "sgh", "gradi", "jb", "dddi", 
                    "moto", "iphone","micromax","fly","lava","android","intex","idea",
                    "china","t-mobile","hcl","benq","spice","airtel","xolo","swipe","asha","ipod","ipad",
                    "Karbonn","Max","sensui","salora","blackberry","dell","iris","iphone","huwai"
                };

                    //Loop through each item in the list created above 
                    //and check if the header contains that text
                    foreach (var mobile in mobiles)
                    {
                        if (context.Request.ServerVariables["HTTP_USER_AGENT"].
                                                            ToLower().Contains(mobile.ToLower()))
                        {
                            return true;
                        }
                    }
                }

                return false;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                return false;
            }
        }
        public static bool IsCronningOn()
        {
            try
            {

                var normalCronSwitch = CommonAttributes.GetConfigValue("NormalCronSwitch", 10);
                if (string.IsNullOrEmpty(normalCronSwitch)) return false;
                if (normalCronSwitch.ToLower() == "off" || normalCronSwitch.ToLower() == "false") return false;
                return true;
            }
            catch (Exception)
            {

                return false;
            }

        }



        internal static void DoCronListCleanUp()
        {
            try
            {



            }
            catch (Exception)
            {


            }
        }
        public static bool ConvertToBoolean(string p, bool defaultValue)
        {
            if (string.IsNullOrWhiteSpace(p)) return defaultValue;
            try
            {
                if (p == "1") return true;
                if (p == "0") return false;
                return bool.Parse(p);
            }
            catch (Exception)
            {

                return defaultValue;
            }
        }
        public static int ConvertToNumber(string p, int defaultValue)
        {
            if (string.IsNullOrWhiteSpace(p)) return defaultValue;
            try
            {
                return int.Parse(p);
            }
            catch (Exception)
            {

                return defaultValue;
            }
        }
        //saurin : 20140307 :  check Is value  convertable in other datatype 
        public static Boolean CanConvert<T>(string value)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    return false;
                }

                //Type type = Type.GetType(testType);
                TypeConverter converter = TypeDescriptor.GetConverter(typeof(T));
                return converter.IsValid(value);
            }

            catch (Exception)
            {

                return false;
            }
        }
        public static bool IsContains(string source, string toCheck, StringComparison comp)
        {
            try
            {
                return source.IndexOf(toCheck, comp) >= 0;
            }
            catch (Exception ex)
            {

                ExceptionLogger.LogException(ex);
                return false;

            }

        }
        public static string GetJSonSerialized(object value)
        {

            var jSerializer = new JavaScriptSerializer();
            jSerializer.MaxJsonLength = Int32.MaxValue;
            return jSerializer.Serialize(value);

        }
        public static DateTime ConvertStringToDateTime(string dt)
        {
            try
            {
                IFormatProvider culture = new CultureInfo("en-GB", true);

                if (string.IsNullOrWhiteSpace(dt))
                {
                    dt = null;
                }
                else
                {
                    DateTime date = DateTime.Parse(dt, culture, DateTimeStyles.AssumeLocal);
                    return date;
                }
            }
            catch (Exception ex)
            {
                try
                {
                    return DateTime.Parse(dt);
                }
                catch (Exception ex1)
                {

                    ExceptionLogger.LogException(ex1);
                }

                
                return GetCurrentDateTime();
            }
            return GetCurrentDateTime();
        }
    }
}
