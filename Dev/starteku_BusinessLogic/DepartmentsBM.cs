﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starteku.Data.Manager;
using System.Data;


namespace starteku_BusinessLogic
{
   public class DepartmentsBM
    {
        #region Private Declaration

        private DataSet _ds;
        private string _returnString;
        private bool _returnBoolean;
        private Int32 _depId;
        private Int32 _depPerentId;
        private String _depName;
        private DateTime _depCreatedDate;
        private DateTime _depUpdatedDate;
        private Boolean _depIsDeleted;
        private Boolean _depIsActive;
        private Int32 _depCompanyId;
        private String _depCreateBy;
        private Int32 _depDepId;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public DataSet ds
        {
            get { return _ds; }
            set { _ds = value; }
        }
        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public string ReturnString
        {
            get { return _returnString; }
            set { _returnString = value; }
        }

        /// <summary>
        /// Gets or sets the ReturnBoolean value.
        /// </summary>
        public bool ReturnBoolean
        {
            get { return _returnBoolean; }
            set { _returnBoolean = value; }
        }

        /// <summary>
        /// Gets or sets the depId value.
        /// </summary>
        public Int32 depId
        {
            get { return _depId; }
            set { _depId = value; }
        }

        /// <summary>
        /// Gets or sets the depPerentId value.
        /// </summary>
        public Int32 depPerentId
        {
            get { return _depPerentId; }
            set { _depPerentId = value; }
        }

        /// <summary>
        /// Gets or sets the depName value.
        /// </summary>
        public String depName
        {
            get { return _depName; }
            set { _depName = value; }
        }

        /// <summary>
        /// Gets or sets the depCreatedDate value.
        /// </summary>
        public DateTime depCreatedDate
        {
            get { return _depCreatedDate; }
            set { _depCreatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the depUpdatedDate value.
        /// </summary>
        public DateTime depUpdatedDate
        {
            get { return _depUpdatedDate; }
            set { _depUpdatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the depIsDeleted value.
        /// </summary>
        public Boolean depIsDeleted
        {
            get { return _depIsDeleted; }
            set { _depIsDeleted = value; }
        }

        /// <summary>
        /// Gets or sets the depIsActive value.
        /// </summary>
        public Boolean depIsActive
        {
            get { return _depIsActive; }
            set { _depIsActive = value; }
        }

        /// <summary>
        /// Gets or sets the depCompanyId value.
        /// </summary>
        public Int32 depCompanyId
        {
            get { return _depCompanyId; }
            set { _depCompanyId = value; }
        }

        /// <summary>
        /// Gets or sets the depCreateBy value.
        /// </summary>
        public String depCreateBy
        {
            get { return _depCreateBy; }
            set { _depCreateBy = value; }
        }

        /// <summary>
        /// Gets or sets the depDepId value.
        /// </summary>
        public Int32 depDepId
        {
            get { return _depDepId; }
            set { _depDepId = value; }
        }

        #endregion


        #region Methods

        public void InsertDepartments()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[9];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue; 

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@depPerentId";
                param[0].DataType = DbType.Int32;                
                param[0].value = _depPerentId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@depName";
                param[1].DataType = DbType.String;
                if (String.IsNullOrEmpty(_depName))
                    param[1].value = DBNull.Value;
                else
                    param[1].value = _depName;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@depCreatedDate";
                param[2].DataType = DbType.DateTime;
                if (_depCreatedDate.Ticks > dtmin.Ticks && _depCreatedDate.Ticks < dtmax.Ticks)
                    param[2].value = _depCreatedDate;
                else if (_depCreatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[2].value = _depCreatedDate;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@depUpdatedDate";
                param[3].DataType = DbType.DateTime;
                if (_depUpdatedDate.Ticks > dtmin.Ticks && _depUpdatedDate.Ticks < dtmax.Ticks)
                    param[3].value = _depUpdatedDate;
                else if (_depUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[3].value = _depUpdatedDate;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@depIsDeleted";
                param[4].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_depIsDeleted.ToString()))
                    param[4].value = DBNull.Value;
                else
                    param[4].value = _depIsDeleted;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@depIsActive";
                param[5].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_depIsActive.ToString()))
                    param[5].value = DBNull.Value;
                else
                    param[5].value = _depIsActive;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@depCompanyId";
                param[6].DataType = DbType.Int32;                
                param[6].value = _depCompanyId;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@depCreateBy";
                param[7].DataType = DbType.String;
                if (String.IsNullOrEmpty(_depCreateBy))
                    param[7].value = DBNull.Value;
                else
                    param[7].value = _depCreateBy;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@depDepId";
                param[8].DataType = DbType.Int32;                
                param[8].value = _depDepId;


                _ds = DBAccess.ExecDataSet("InsertDepartments", CommandType.StoredProcedure, param);
                _returnBoolean = true;
               
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }
        public void UpdateDepartments()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[4];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue; 

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@depId";
                param[0].DataType = DbType.Int32;
                param[0].value = _depId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@depPerentId";
                param[1].DataType = DbType.Int32;               
                param[1].value = _depPerentId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@depName";
                param[2].DataType = DbType.String;
                if (String.IsNullOrEmpty(_depName))
                    param[2].value = DBNull.Value;
                else
                    param[2].value = _depName;


                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@depUpdatedDate";
                param[3].DataType = DbType.DateTime;
                if (_depUpdatedDate.Ticks > dtmin.Ticks && _depUpdatedDate.Ticks < dtmax.Ticks)
                    param[3].value = _depUpdatedDate;
                else if (_depUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[3].value = _depUpdatedDate;

                _ds = DBAccess.ExecDataSet("UpdateDepartments", CommandType.StoredProcedure, param);
                _returnBoolean = true;
               
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        public void GetAllDepartments()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@depIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _depIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@depIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _depIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@depCompanyId";
                param[2].DataType = DbType.Int32;
                param[2].value = _depCompanyId;

                _ds = DBAccess.ExecDataSet("GetAllDepartments", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllDepartments" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void DepartmentsStatusUpdate()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] pm = new ParamStruct[3];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@depId";
                pm[0].DataType = DbType.Int32;
                pm[0].value = _depId;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@depIsActive";
                pm[1].DataType = DbType.Boolean;
                pm[1].value = _depIsActive;

                pm[2].direction = ParameterDirection.Input;
                pm[2].ParamName = "@depIsDeleted";
                pm[2].DataType = DbType.Boolean;
                pm[2].value = _depIsDeleted;

                _ds = DBAccess.ExecDataSet("DepartmentsStatusUpdate", CommandType.StoredProcedure, pm);
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in DepartmentsStatusUpdate  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            }
        }

        public void GetAllDepartmentsbyid()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@depId";
                param[0].DataType = DbType.Int32;
                param[0].value = _depId;


                _ds = DBAccess.ExecDataSet("GetAllDepartmentsbyid", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllDepartmentsbyid" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void DepartmentsCheckDuplication(string name, Int32 id)
        {
            try
            {
                DataAccess obj = new DataAccess();
                obj.Provider = EnumProviders.SQLClient;
                obj.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["connstring"].ToString();

                ParamStruct[] param = new ParamStruct[2];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue; 

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@depName";
                param[0].DataType = DbType.String;
                param[0].value = name;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@depId";
                param[1].DataType = DbType.Int32;
                param[1].value = id;

                _ds = obj.ExecDataSet("DepartmentsCheckDuplication", CommandType.StoredProcedure, param);
                if (_ds.Tables[0].Rows.Count > 0)
                {
                    _returnString = (_ds.Tables[0].Rows[0]["returnStatement"].ToString());

                }

            }
            catch (DataException ex)
            {
                throw ex;
            }


        }

        public void GetDepartmentsFromParentId()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] pm = new ParamStruct[2];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@depPerentId";
                pm[0].DataType = DbType.Int32;
                pm[0].value = _depPerentId;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@depCompanyId";
                pm[1].DataType = DbType.Int32;
                pm[1].value = _depCompanyId;

                _ds = DBAccess.ExecDataSet("GetDepartmentsFromParentId", CommandType.StoredProcedure, pm);
            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetDepartmentsFromParentId" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;
            }
        }
   
        #endregion
    }
}
