﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using starteku.Data.Manager;

namespace starteku_BusinessLogic
{
    public class CategoryBM
    {

        #region Private Declaration

        private DataSet _ds;
        private string _returnString;
        private bool _returnBoolean;
        private Int32 _catId;
        private Int32 _catPerentId;
        private String _catName;
        private String _catNameDN;
        private DateTime _catCreatedDate;
        private DateTime _catUpdatedDate;
        private Boolean _catIsDeleted;
        private Boolean _catIsActive;
        private Int32 _catCompanyId;
        private String _catCreateBy;
        private Int32 _catDepId;
        private String _catDescription;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public DataSet ds
        {
            get { return _ds; }
            set { _ds = value; }
        }
        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public string ReturnString
        {
            get { return _returnString; }
            set { _returnString = value; }
        }

        /// <summary>
        /// Gets or sets the ReturnBoolean value.
        /// </summary>
        public bool ReturnBoolean
        {
            get { return _returnBoolean; }
            set { _returnBoolean = value; }
        }

        /// <summary>
        /// Gets or sets the depId value.
        /// </summary>
        public Int32 depId
        {
            get { return _catId; }
            set { _catId = value; }
        }

        /// <summary>
        /// Gets or sets the depPerentId value.
        /// </summary>
        public Int32 depPerentId
        {
            get { return _catPerentId; }
            set { _catPerentId = value; }
        }

        /// <summary>
        /// Gets or sets the depName value.
        /// </summary>
        public String depName
        {
            get { return _catName; }
            set { _catName = value; }
        }

        public String depNameDN
        {
            get { return _catNameDN; }
            set { _catNameDN = value; }
        }

        /// <summary>
        /// Gets or sets the depCreatedDate value.
        /// </summary>
        public DateTime depCreatedDate
        {
            get { return _catCreatedDate; }
            set { _catCreatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the depUpdatedDate value.
        /// </summary>
        public DateTime depUpdatedDate
        {
            get { return _catUpdatedDate; }
            set { _catUpdatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the depIsDeleted value.
        /// </summary>
        public Boolean depIsDeleted
        {
            get { return _catIsDeleted; }
            set { _catIsDeleted = value; }
        }

        /// <summary>
        /// Gets or sets the depIsActive value.
        /// </summary>
        public Boolean depIsActive
        {
            get { return _catIsActive; }
            set { _catIsActive = value; }
        }

        /// <summary>
        /// Gets or sets the depCompanyId value.
        /// </summary>
        public Int32 depCompanyId
        {
            get { return _catCompanyId; }
            set { _catCompanyId = value; }
        }

        /// <summary>
        /// Gets or sets the depCreateBy value.
        /// </summary>
        public String depCreateBy
        {
            get { return _catCreateBy; }
            set { _catCreateBy = value; }
        }

        /// <summary>
        /// Gets or sets the depDepId value.
        /// </summary>
        public Int32 depDepId
        {
            get { return _catDepId; }
            set { _catDepId = value; }
        }
        public String divDescription
        {
            get { return _catDescription; }
            set { _catDescription = value; }
        }
        #endregion



        /* Saurin 20141211*/
        #region Methods
        public string GetConnString()
        {
            return System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
        }

        public void GetAllCompetenceCategory()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = GetConnString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@catIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _catIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@catIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _catIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@catCompanyId";
                param[2].DataType = DbType.Int32;
                param[2].value = _catCompanyId;

                _ds = DBAccess.ExecDataSet("GetAllCompetenceCategory", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllCompetenceCategory" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }

        }
       

        public void InsertCompetenceCategory()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[7];
                

                //param[0].direction = ParameterDirection.Input;
                //param[0].ParamName = "@catPerentId";
                //param[0].DataType = DbType.Int32;
                //param[0].value = _catPerentId;

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@catName";
                param[0].DataType = DbType.String;
                if (String.IsNullOrEmpty(_catName))
                    param[0].value = DBNull.Value;
                else
                    param[0].value = _catName;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@catCreatedDate";
                param[1].DataType = DbType.DateTime;
                if (isValidSqlDate(_catCreatedDate))
                {
                    param[1].value = _catCreatedDate;
                }
                else if (_catUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
            
               
                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@catUpdatedDate";
                param[2].DataType = DbType.DateTime;
                if (isValidSqlDate(_catUpdatedDate))
                {
                    param[2].value = _catUpdatedDate;
                }
                else if (_catUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@catIsDeleted";
                param[3].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_catIsDeleted.ToString()))
                    param[3].value = DBNull.Value;
                else
                    param[3].value = _catIsDeleted;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@catIsActive";
                param[4].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_catIsActive.ToString()))
                    param[4].value = DBNull.Value;
                else
                    param[4].value = _catIsActive;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@catCompanyId";
                param[5].DataType = DbType.Int32;
                param[5].value = _catCompanyId;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@catNameDN";
                param[6].DataType = DbType.String;
                if (String.IsNullOrEmpty(_catNameDN))
                    param[6].value = DBNull.Value;
                else
                    param[6].value = _catNameDN;

                //param[7].direction = ParameterDirection.Input;
                //param[7].ParamName = "@catCreateBy";
                //param[7].DataType = DbType.String;
                //if (String.IsNullOrEmpty(_catCreateBy))
                //    param[7].value = DBNull.Value;
                //else
                //    param[7].value = _catCreateBy;

                //param[8].direction = ParameterDirection.Input;
                //param[8].ParamName = "@catDepId";
                //param[8].DataType = DbType.Int32;
                //param[8].value = _catDepId;


                //param[9].direction = ParameterDirection.Input;
                //param[9].ParamName = "@catDescription";
                //param[9].DataType = DbType.String;
                //if (String.IsNullOrEmpty(_catDescription))
                //    param[9].value = DBNull.Value;
                //else
                //    param[9].value = _catDescription;

                _ds = DBAccess.ExecDataSet("InsertCompetenceCategory", CommandType.StoredProcedure, param);
                _returnBoolean = true;

            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }
        public void UpdateCompetenceCategory()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[4];
                

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@catId";
                param[0].DataType = DbType.Int32;
                param[0].value = _catId;

                //param[1].direction = ParameterDirection.Input;
                //param[1].ParamName = "@catPerentId";
                //param[1].DataType = DbType.Int32;
                //param[1].value = _catPerentId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@catName";
                param[1].DataType = DbType.String;
                if (String.IsNullOrEmpty(_catName))
                    param[1].value = DBNull.Value;
                else
                    param[1].value = _catName;


                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@catUpdatedDate";
                param[2].DataType = DbType.DateTime;
                if (isValidSqlDate(_catCreatedDate))
                {
                    param[2].value = _catUpdatedDate;
                }
                else if (_catUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@catNameDN";
                param[3].DataType = DbType.String;
                if (String.IsNullOrEmpty(_catNameDN))
                    param[3].value = DBNull.Value;
                else
                    param[3].value = _catNameDN;

                _ds = DBAccess.ExecDataSet("UpdateCompetenceCategory", CommandType.StoredProcedure, param);
                _returnBoolean = true;

            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }
        public void CompetenceCategoryCheckDuplication(string name, Int32 id, Int32 divCompanyId)
        {
            try
            {
                DataAccess obj = new DataAccess();
                obj.Provider = EnumProviders.SQLClient;
                obj.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["connstring"].ToString();

                ParamStruct[] param = new ParamStruct[3];
               // DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
               // DateTime dtmax = Convert.ToDateTime("12/31/9999 11:59:59 PM");

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@catName";
                param[0].DataType = DbType.String;
                param[0].value = name;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@catId";
                param[1].DataType = DbType.Int32;
                param[1].value = id;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@catCompanyId";
                param[2].DataType = DbType.Int32;
                param[2].value = divCompanyId;

                _ds = obj.ExecDataSet("CompetenceCategoryCheckDuplication", CommandType.StoredProcedure, param);
                if (_ds.Tables[0].Rows.Count > 0)
                {
                    _returnString = (_ds.Tables[0].Rows[0]["returnStatement"].ToString());

                }

            }
            catch (DataException ex)
            {
                throw ex;
            }


        }

        public void CompetenceCategoryStatusUpdate()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] pm = new ParamStruct[3];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@catId";
                pm[0].DataType = DbType.Int32;
                pm[0].value = _catId;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@catIsActive";
                pm[1].DataType = DbType.Boolean;
                pm[1].value = _catIsActive;

                pm[2].direction = ParameterDirection.Input;
                pm[2].ParamName = "@catIsDeleted";
                pm[2].DataType = DbType.Boolean;
                pm[2].value = _catIsDeleted;

                _ds = DBAccess.ExecDataSet("CompetenceCategoryStatusUpdate", CommandType.StoredProcedure, pm);
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in CompetenceCategoryStatusUpdate  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            }
        }

        public void GetCompetenceCategoryFromParentId()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] pm = new ParamStruct[2];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@depPerentId";
                pm[0].DataType = DbType.Int32;
                pm[0].value = _catPerentId;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@depCompanyId";
                pm[1].DataType = DbType.Int32;
                pm[1].value = _catCompanyId;

                _ds = DBAccess.ExecDataSet("GetCompetenceCategoryFromParentId", CommandType.StoredProcedure, pm);
            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetCompetenceCategoryFromParentId" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;
            }
        }

        public void GetAllDivisionbyid()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@catId";
                param[0].DataType = DbType.Int32;
                param[0].value = _catId;


                _ds = DBAccess.ExecDataSet("GetAllCompetenceCategoryById", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllDivisionsbyid" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }


        public void GetAllCompetenceSkills()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = GetConnString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@jobType";
                param[0].DataType = DbType.Boolean;
                param[0].value = _catIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@divId";
                param[1].DataType = DbType.Boolean;
                param[1].value = _catIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@catCompanyId";
                param[2].DataType = DbType.Int32;
                param[2].value = _catCompanyId;

                _ds = DBAccess.ExecDataSet("GetAllCompetenceCategory", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllCompetenceCategory" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }

        }

        public void GetAllCompetenceCategoryById()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@catId";
                param[0].DataType = DbType.Int32;
                param[0].value = _catId;


                _ds = DBAccess.ExecDataSet("GetAllCompetenceCategoryById", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllDivisionsbyid" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        #endregion

        #region CommonFunction
        static bool isValidSqlDate(DateTime date)
        {
            return ((date >= (DateTime)SqlDateTime.MinValue) && (date <= (DateTime)SqlDateTime.MaxValue));
        }
        #endregion
    }   
}
