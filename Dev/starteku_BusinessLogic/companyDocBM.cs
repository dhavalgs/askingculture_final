using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starteku.Data.Manager;
using System.Data;
using starteku_BusinessLogic;

namespace startetku.Business.Logic
{

    public class companyDocBM
    {

        #region Private Declaration

        private DataSet _ds;
        private string _returnString;
        private bool _returnBoolean;
        private Int32 _comId;
        private String _comTitle;
        private String _comDescription;
        private String _comattachment;
        private Int32 _comJobType;
        private Int32 _comUserId;
        private DateTime _comCreatedDate;
        private DateTime _comUpdatedDate;
        private Boolean _comIsDeleted;
        private Boolean _comIsActive;
        private Int32 _comCompanyId;
        private String _comCreateBy;
        private Int32 _comDepId;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public DataSet ds
        {
            get { return _ds; }
            set { _ds = value; }
        }
        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public string ReturnString
        {
            get { return _returnString; }
            set { _returnString = value; }
        }

        /// <summary>
        /// Gets or sets the ReturnBoolean value.
        /// </summary>
        public bool ReturnBoolean
        {
            get { return _returnBoolean; }
            set { _returnBoolean = value; }
        }

        /// <summary>
        /// Gets or sets the comId value.
        /// </summary>
        public Int32 comId
        {
            get { return _comId; }
            set { _comId = value; }
        }

        /// <summary>
        /// Gets or sets the comTitle value.
        /// </summary>
        public String comTitle
        {
            get { return _comTitle; }
            set { _comTitle = value; }
        }

        /// <summary>
        /// Gets or sets the comDescription value.
        /// </summary>
        public String comDescription
        {
            get { return _comDescription; }
            set { _comDescription = value; }
        }

        /// <summary>
        /// Gets or sets the comattachment value.
        /// </summary>
        public String comattachment
        {
            get { return _comattachment; }
            set { _comattachment = value; }
        }

        /// <summary>
        /// Gets or sets the comJobType value.
        /// </summary>
        public Int32 comJobType
        {
            get { return _comJobType; }
            set { _comJobType = value; }
        }

        /// <summary>
        /// Gets or sets the comUserId value.
        /// </summary>
        public Int32 comUserId
        {
            get { return _comUserId; }
            set { _comUserId = value; }
        }

        /// <summary>
        /// Gets or sets the comCreatedDate value.
        /// </summary>
        public DateTime comCreatedDate
        {
            get { return _comCreatedDate; }
            set { _comCreatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the comUpdatedDate value.
        /// </summary>
        public DateTime comUpdatedDate
        {
            get { return _comUpdatedDate; }
            set { _comUpdatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the comIsDeleted value.
        /// </summary>
        public Boolean comIsDeleted
        {
            get { return _comIsDeleted; }
            set { _comIsDeleted = value; }
        }

        /// <summary>
        /// Gets or sets the comIsActive value.
        /// </summary>
        public Boolean comIsActive
        {
            get { return _comIsActive; }
            set { _comIsActive = value; }
        }

        /// <summary>
        /// Gets or sets the comCompanyId value.
        /// </summary>
        public Int32 comCompanyId
        {
            get { return _comCompanyId; }
            set { _comCompanyId = value; }
        }

        /// <summary>
        /// Gets or sets the comCreateBy value.
        /// </summary>
        public String comCreateBy
        {
            get { return _comCreateBy; }
            set { _comCreateBy = value; }
        }

        /// <summary>
        /// Gets or sets the comDepId value.
        /// </summary>
        public Int32 comDepId
        {
            get { return _comDepId; }
            set { _comDepId = value; }
        }

        #endregion

        #region Methods


        public void InsertcompanyDoc()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[12];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                //DateTime dtmax = Convert.ToDateTime("12/31/9999 11:59:59 PM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue; 

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@comTitle";
                param[0].DataType = DbType.String;
                if (String.IsNullOrEmpty(_comTitle))
                    param[0].value = DBNull.Value;
                else
                    param[0].value = _comTitle;
                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@comDescription";
                param[1].DataType = DbType.String;
                if (String.IsNullOrEmpty(_comDescription))
                    param[1].value = DBNull.Value;
                else
                    param[1].value = _comDescription;
                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@comattachment";
                param[2].DataType = DbType.String;
                if (String.IsNullOrEmpty(_comattachment))
                    param[2].value = DBNull.Value;
                else
                    param[2].value = _comattachment;
                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@comJobType";
                param[3].DataType = DbType.Int32;
                if (_comJobType == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[3].value = _comJobType;
                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@comUserId";
                param[4].DataType = DbType.Int32;
                if (_comUserId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[4].value = _comUserId;
                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@comCreatedDate";
                param[5].DataType = DbType.DateTime;
                if (_comCreatedDate.Ticks > dtmin.Ticks && _comCreatedDate.Ticks < dtmax.Ticks)
                    param[5].value = _comCreatedDate;
                else if (_comCreatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[5].value = _comCreatedDate;
                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@comUpdatedDate";
                param[6].DataType = DbType.DateTime;
                if (_comUpdatedDate.Ticks > dtmin.Ticks && _comUpdatedDate.Ticks < dtmax.Ticks)
                    param[6].value = _comUpdatedDate;
                else if (_comUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[6].value = _comUpdatedDate;
                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@comIsDeleted";
                param[7].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_comIsDeleted.ToString()))
                    param[7].value = DBNull.Value;
                else
                    param[7].value = _comIsDeleted;
                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@comIsActive";
                param[8].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_comIsActive.ToString()))
                    param[8].value = DBNull.Value;
                else
                    param[8].value = _comIsActive;
                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@comCompanyId";
                param[9].DataType = DbType.Int32;
                if (_comCompanyId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[9].value = _comCompanyId;
                param[10].direction = ParameterDirection.Input;
                param[10].ParamName = "@comCreateBy";
                param[10].DataType = DbType.String;
                if (String.IsNullOrEmpty(_comCreateBy))
                    param[10].value = DBNull.Value;
                else
                    param[10].value = _comCreateBy;
                param[11].direction = ParameterDirection.Input;
                param[11].ParamName = "@comDepId";
                param[11].DataType = DbType.Int32;
                if (_comDepId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[11].value = _comDepId;
            
                _ds = DBAccess.ExecDataSet("InsertcompanyDoc", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }


        public void GetAllCompanyDocument()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[2];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@comIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _comIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@CorrentDateTime";
                param[1].DataType = DbType.DateTime;
                param[1].value = DateTime.Now;

                _ds = DBAccess.ExecDataSet("GetAllCompanyDocument", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllCompanyDocument" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void DeleteCompanyDocumentById()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@comId";
                param[0].DataType = DbType.Int32;
                param[0].value = _comId;


                _ds = DBAccess.ExecDataSet("DeleteCompanyDocumentById", CommandType.StoredProcedure, param);
                _returnBoolean = true;

            }
            catch (Exception ex)
            {

            }

        }

        #endregion
        
    }
}
