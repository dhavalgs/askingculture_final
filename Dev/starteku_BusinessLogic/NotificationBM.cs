using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starteku.Data.Manager;
using System.Data;
using starteku_BusinessLogic;
using starteku_BusinessLogic.Model;

namespace startetku.Business.Logic
{

    public class NotificationBM
    {
        #region Private Declaration

        #region Notification
        private DataSet _ds;
        private string _returnString;
        private bool _returnBoolean;
        private Int32 _notId;
        private Int32 _notUserId;
        private String _notsubject;
        private DateTime _notCreatedDate;
        private DateTime _notUpdatedDate;
        private Boolean _notIsDeleted;
        private Boolean _notIsActive;
        private Int32 _notCompanyId;
        private String _notpage;
        private String _userCreateBy;
        private String _nottype;
        private Int32 _notToUserId;
        private Boolean _notPopUpStatus;

        private String _notPDPHistoryID;
      
        #endregion


        #region Message
        private Int32 _mesId;
        private Int32 _mesfromUserId;
        private Int32 _masToUserId;
        private String _messubject;
        private DateTime _masCreatedDate;
        private DateTime _masUpdatedDate;
        private Boolean _masIsDeleted;
        private Boolean _masIsActive;
        private Int32 _masCompanyId;
        private String _mastype;
        private Boolean _masStatus;
        #endregion

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        /// 
        #region Notification
        public DataSet ds
        {
            get { return _ds; }
            set { _ds = value; }
        }
        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public string ReturnString
        {
            get { return _returnString; }
            set { _returnString = value; }
        }

        /// <summary>
        /// Gets or sets the ReturnBoolean value.
        /// </summary>
        public bool ReturnBoolean
        {
            get { return _returnBoolean; }
            set { _returnBoolean = value; }
        }

        /// <summary>
        /// Gets or sets the notId value.
        /// </summary>
        public Int32 notId
        {
            get { return _notId; }
            set { _notId = value; }
        }

        /// <summary>
        /// Gets or sets the notUserId value.
        /// </summary>
        public Int32 notUserId
        {
            get { return _notUserId; }
            set { _notUserId = value; }
        }

        /// <summary>
        /// Gets or sets the notsubject value.
        /// </summary>
        public String notsubject
        {
            get { return _notsubject; }
            set { _notsubject = value; }
        }

        /// <summary>
        /// Gets or sets the notCreatedDate value.
        /// </summary>
        public DateTime notCreatedDate
        {
            get { return _notCreatedDate; }
            set { _notCreatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the notUpdatedDate value.
        /// </summary>
        public DateTime notUpdatedDate
        {
            get { return _notUpdatedDate; }
            set { _notUpdatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the notIsDeleted value.
        /// </summary>
        public Boolean notIsDeleted
        {
            get { return _notIsDeleted; }
            set { _notIsDeleted = value; }
        }

        /// <summary>
        /// Gets or sets the notIsActive value.
        /// </summary>
        public Boolean notIsActive
        {
            get { return _notIsActive; }
            set { _notIsActive = value; }
        }

        /// <summary>
        /// Gets or sets the notCompanyId value.
        /// </summary>
        public Int32 notCompanyId
        {
            get { return _notCompanyId; }
            set { _notCompanyId = value; }
        }

        /// <summary>
        /// Gets or sets the notpage value.
        /// </summary>
        public String notpage
        {
            get { return _notpage; }
            set { _notpage = value; }
        }
        public String userCreateBy
        {
            get { return _userCreateBy; }
            set { _userCreateBy = value; }
        }
        public string nottype
        {
            get { return _nottype; }
            set { _nottype = value; }
        }
        public Int32 notToUserId
        {
            get { return _notToUserId; }
            set { _notToUserId = value; }
        }

        public String notPDPHistoryID
        {
            get { return _notPDPHistoryID; }
            set { _notPDPHistoryID = value; }
        }
        public Boolean notPopUpStatus
        {
            get { return _notPopUpStatus; }
            set { _notPopUpStatus = value; }
        }
        #endregion

        #region Message

        public Int32 mesId
        {
            get { return _mesId; }
            set { _mesId = value; }
        }

        /// <summary>
        /// Gets or sets the notUserId value.
        /// </summary>
        public Int32 mesfromUserId
        {
            get { return _mesfromUserId; }
            set { _mesfromUserId = value; }
        }
        public Int32 masToUserId
        {
            get { return _masToUserId; }
            set { _masToUserId = value; }
        }
        /// <summary>
        /// Gets or sets the notsubject value.
        /// </summary>
        public String messubject
        {
            get { return _messubject; }
            set { _messubject = value; }
        }

        /// <summary>
        /// Gets or sets the notCreatedDate value.
        /// </summary>
        public DateTime masCreatedDate
        {
            get { return _masCreatedDate; }
            set { _masCreatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the notUpdatedDate value.
        /// </summary>
        public DateTime masUpdatedDate
        {
            get { return _masUpdatedDate; }
            set { _masUpdatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the notIsDeleted value.
        /// </summary>
        public Boolean masIsDeleted
        {
            get { return _masIsDeleted; }
            set { _masIsDeleted = value; }
        }

        /// <summary>
        /// Gets or sets the notIsActive value.
        /// </summary>
        public Boolean masIsActive
        {
            get { return _masIsActive; }
            set { _masIsActive = value; }
        }

        /// <summary>
        /// Gets or sets the notCompanyId value.
        /// </summary>
        public Int32 masCompanyId
        {
            get { return _masCompanyId; }
            set { _masCompanyId = value; }
        }

        /// <summary>
        /// Gets or sets the notpage value.
        /// </summary>
        public String mastype
        {
            get { return _mastype; }
            set { _mastype = value; }
        }

        public Boolean masStatus
        {
            get { return _masStatus; }
            set { _masStatus = value; }
        }

        #endregion

        #endregion

        #region Methods

        #region Notification
        public void InsertNotification()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[12];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue;

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@notUserId";
                param[0].DataType = DbType.Int32;
                if (_notUserId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[0].value = _notUserId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@notsubject";
                param[1].DataType = DbType.String;
                if (String.IsNullOrEmpty(_notsubject))
                    param[1].value = DBNull.Value;
                else
                    param[1].value = _notsubject;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@notCreatedDate";
                param[2].DataType = DbType.DateTime;
                if (_notCreatedDate.Ticks > dtmin.Ticks && _notCreatedDate.Ticks < dtmax.Ticks)
                    param[2].value = _notCreatedDate;
                else if (_notCreatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[2].value = _notCreatedDate;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@notUpdatedDate";
                param[3].DataType = DbType.DateTime;
                if (_notUpdatedDate.Ticks > dtmin.Ticks && _notUpdatedDate.Ticks < dtmax.Ticks)
                    param[3].value = _notUpdatedDate;
                else if (_notUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[3].value = _notUpdatedDate;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@notIsDeleted";
                param[4].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_notIsDeleted.ToString()))
                    param[4].value = DBNull.Value;
                else
                    param[4].value = _notIsDeleted;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@notIsActive";
                param[5].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_notIsActive.ToString()))
                    param[5].value = DBNull.Value;
                else
                    param[5].value = _notIsActive;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@notCompanyId";
                param[6].DataType = DbType.Int32;
                if (_notCompanyId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[6].value = _notCompanyId;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@notpage";
                param[7].DataType = DbType.String;
                if (String.IsNullOrEmpty(_notpage))
                    param[7].value = DBNull.Value;
                else
                    param[7].value = _notpage;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@nottype";
                param[8].DataType = DbType.String;
                if (String.IsNullOrEmpty(_nottype))
                    param[8].value = DBNull.Value;
                else
                    param[8].value = _nottype;


                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@notToUserId";
                param[9].DataType = DbType.Int32;
                param[9].value = _notToUserId;


                param[10].direction = ParameterDirection.Input;
                param[10].ParamName = "@notPopUpStatus";
                param[10].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_notPopUpStatus.ToString()))
                    param[10].value = true;
                else
                    param[10].value = true;

                param[11].direction = ParameterDirection.Input;
                param[11].ParamName = "@notPDPHistoryID";
                param[11].DataType = DbType.String;
                if (String.IsNullOrEmpty(_notPDPHistoryID))
                    param[11].value = DBNull.Value;
                else
                    param[11].value = _notPDPHistoryID;

                //param[11].value = _notPDPHistoryID;

                //obj.ExecScalar("InsertNotification", CommandType.StoredProcedure, param); _returnBoolean = true;
                _ds = DBAccess.ExecDataSet("InsertNotification", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }
        public void GetAllNotification()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@notIsDeleted";
                param[0].DataType = DbType.Boolean;
                param[0].value = _notIsDeleted;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@notIsActive";
                param[1].DataType = DbType.Boolean;
                param[1].value = _notIsActive;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@userCreateBy";
                param[2].DataType = DbType.String;
                param[2].value = _userCreateBy;


                _ds = DBAccess.ExecDataSet("GetAllNotification", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllNotification" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        public void GetAllNotificationFULL()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@notIsDeleted";
                param[0].DataType = DbType.Boolean;
                param[0].value = _notIsDeleted;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@notIsActive";
                param[1].DataType = DbType.Boolean;
                param[1].value = _notIsActive;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@userCreateBy";
                param[2].DataType = DbType.String;
                param[2].value = _userCreateBy;


                _ds = DBAccess.ExecDataSet("GetAllNotificationFULL", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllNotification" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        public void GetAllPopUPNotification()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@notPopUpStatus";
                param[0].DataType = DbType.Boolean;
                param[0].value = _notPopUpStatus;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@notIsActive";
                param[1].DataType = DbType.Boolean;
                param[1].value = _notIsActive;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@userCreateBy";
                param[2].DataType = DbType.String;
                param[2].value = _userCreateBy;


                _ds = DBAccess.ExecDataSet("GetAllPopUPNotification", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllPopUPNotification" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }


        //Method used when value pass by parameter instead of public getter setter, usefull on webservice |20141225 | Saurin
        public static void UpdateNotificationByNotToUserId(bool isActive, int notToUserId)
        {
            var obj = new NotificationBM
                {
                    notIsActive = false,
                    notUpdatedDate =DateTime.Now,
                    notToUserId = notToUserId,
                    nottype = "Notification"
                };
            obj.UpdateNotificationByNotToUserId();
        }
        public static void UpdateMessageByNotToUserId(bool isActive, int notToUserId)
        {
            var obj = new NotificationBM
            {
                notIsActive = false,
                notUpdatedDate = DateTime.Now,
                notToUserId = notToUserId,
                nottype = "Message"
            };
            obj.UpdateNotificationByNotToUserId();
        }
        //A method to update user notification to set zero. |20141225 | Saurin
        public void UpdateNotificationByNotToUserId()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[4];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue;

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@notToUserId";
                param[0].DataType = DbType.Int32;
                if (_notToUserId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[0].value = _notToUserId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@notUpdatedDate";
                param[1].DataType = DbType.DateTime;
                if (_notUpdatedDate.Ticks > dtmin.Ticks && _notUpdatedDate.Ticks < dtmax.Ticks)
                    param[1].value = _notUpdatedDate;
                else if (_notUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[1].value = _notUpdatedDate;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@notIsActive";
                param[2].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_notIsActive.ToString()))
                    param[2].value = DBNull.Value;
                else
                    param[2].value = _notIsActive;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@nottype";
                param[3].DataType = DbType.String;
                param[3].value = _nottype;


                _ds = DBAccess.ExecDataSet("UpdateNotificationByNotToUserID", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        public void UpdateNotification()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[4];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue;

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@notUserId";
                param[0].DataType = DbType.Int32;
                if (_notUserId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[0].value = _notUserId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@notUpdatedDate";
                param[1].DataType = DbType.DateTime;
                if (_notUpdatedDate.Ticks > dtmin.Ticks && _notUpdatedDate.Ticks < dtmax.Ticks)
                    param[1].value = _notUpdatedDate;
                else if (_notUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[1].value = _notUpdatedDate;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@notIsActive";
                param[2].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_notIsActive.ToString()))
                    param[2].value = DBNull.Value;
                else
                    param[2].value = _notIsActive;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@nottype";
                param[3].DataType = DbType.String;
                param[3].value = _nottype;


                _ds = DBAccess.ExecDataSet("UpdateNotification", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }
        public void UpdateNotificationbyid()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[2];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue;

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@notId";
                param[0].DataType = DbType.Int32;
                param[0].value = _notId;


                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@notPopUpStatus";
                param[1].DataType = DbType.Boolean;
                param[1].value = _notPopUpStatus;

                _ds = DBAccess.ExecDataSet("UpdateNotificationbyid", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        #endregion

        #region Message
        public Boolean InsertMessage()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[10];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue;

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@mesfromUserId";
                param[0].DataType = DbType.Int32;
                if (_mesfromUserId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[0].value = _mesfromUserId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@masToUserId";
                param[1].DataType = DbType.Int32;
                param[1].value = _masToUserId;


                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@messubject";
                param[2].DataType = DbType.String;
                if (String.IsNullOrEmpty(_messubject))
                    param[2].value = DBNull.Value;
                else
                    param[2].value = _messubject;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@masCreatedDate";
                param[3].DataType = DbType.DateTime;
                if (_masCreatedDate.Ticks > dtmin.Ticks && _masCreatedDate.Ticks < dtmax.Ticks)
                    param[3].value = _masCreatedDate;
                else if (_masCreatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[3].value = _notCreatedDate;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@masUpdatedDate";
                param[4].DataType = DbType.DateTime;
                if (_masUpdatedDate.Ticks > dtmin.Ticks && _masUpdatedDate.Ticks < dtmax.Ticks)
                    param[4].value = _masUpdatedDate;
                else if (_masUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[4].value = _masUpdatedDate;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@masIsDeleted";
                param[5].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_masIsDeleted.ToString()))
                    param[5].value = DBNull.Value;
                else
                    param[5].value = _masIsDeleted;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@masIsActive";
                param[6].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_masIsActive.ToString()))
                    param[6].value = DBNull.Value;
                else
                    param[6].value = _masIsActive;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@masCompanyId";
                param[7].DataType = DbType.Int32;
                if (_masCompanyId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[7].value = _masCompanyId;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@mastype";
                param[8].DataType = DbType.String;
                if (String.IsNullOrEmpty(_mastype))
                    param[8].value = DBNull.Value;
                else
                    param[8].value = _mastype;

                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@masStatus";
                param[9].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_masStatus.ToString()))
                    param[9].value = true;
                else
                    param[9].value = true;

               
                //obj.ExecScalar("InsertNotification", CommandType.StoredProcedure, param); _returnBoolean = true;
                _ds = DBAccess.ExecDataSet("InsertMessage", CommandType.StoredProcedure, param);
               // _returnBoolean = true;
                return true;
            }
            catch (DataException ex)
            {
                return false;
                throw ex;
            }
        }
        public void GetAllmessagebyId()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[2];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@mesfromUserId";
                param[0].DataType = DbType.Int32;
                param[0].value = _mesfromUserId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@masToUserId";
                param[1].DataType = DbType.Int32;
                param[1].value = _masToUserId;


                _ds = DBAccess.ExecDataSet("GetAllmessagebyId", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllmessagebyId" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        public void GetAllmessagebyId_stauts()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@mesfromUserId";
                param[0].DataType = DbType.Int32;
                param[0].value = _mesfromUserId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@masToUserId";
                param[1].DataType = DbType.Int32;
                param[1].value = _masToUserId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@masStatus";
                param[2].DataType = DbType.Boolean;
                param[2].value = _masStatus;


                _ds = DBAccess.ExecDataSet("GetAllmessagebyId_stauts", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllmessagebyId" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        public void UpdatemessagebyId_stauts()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[2];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue;

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@notUserId";
                param[0].DataType = DbType.Int32;
                param[0].value = _notUserId;
                
                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@masStatus";
                param[1].DataType = DbType.Boolean;
                param[1].value = _masStatus;

                _ds = DBAccess.ExecDataSet("UpdatemessagebyId_stauts", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        #endregion
        #endregion

        #region Linq

        public static void UpdateNotificationPopup(int notId, bool notPopUpStatus=false)
        {
            // obj.UpdateNotificationbyid();
            var db = new startetkuEntities1();
            var dbCall = (from o in db.Notifications where o.notId == notId select o).FirstOrDefault();
            if (dbCall != null)
            {
                dbCall.notPopUpStatus = notPopUpStatus;
               // dbCall.notIsActive = false;
                db.SaveChanges();
            }
        }
        #endregion
    }
}
