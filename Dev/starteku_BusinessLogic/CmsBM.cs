﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starteku.Data.Manager;
using System.Data;


namespace starteku_BusinessLogic
{
    public class CmsBM
    {

        #region Private Declaration

        private DataSet _ds;
        private string _returnString;
        private bool _returnBoolean;
        private Int32 _cmsId;
        private String _cmsName;
        private String _cmsDescription;
        private String _cmsDescriptionDN;
        private DateTime _cmsCreatedDate;
        private DateTime _cmsUpdatedDate;
        private Boolean _cmsIsDeleted;
        private Boolean _cmsIsActive;
        private Int32 _cmsCompanyId;
        private String _cmsCreateBy;


        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public DataSet ds
        {
            get { return _ds; }
            set { _ds = value; }
        }
        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public string ReturnString
        {
            get { return _returnString; }
            set { _returnString = value; }
        }

        /// <summary>
        /// Gets or sets the ReturnBoolean value.
        /// </summary>
        public bool ReturnBoolean
        {
            get { return _returnBoolean; }
            set { _returnBoolean = value; }
        }

        /// <summary>
        /// Gets or sets the cmsId value.
        /// </summary>
        public Int32 cmsId
        {
            get { return _cmsId; }
            set { _cmsId = value; }
        }

        /// <summary>
        /// Gets or sets the cmsName value.
        /// </summary>
        public String cmsName
        {
            get { return _cmsName; }
            set { _cmsName = value; }
        }

        /// <summary>
        /// Gets or sets the cmsDescription value.
        /// </summary>
        public String cmsDescription
        {
            get { return _cmsDescription; }
            set { _cmsDescription = value; }
        }

        public String cmsDescriptionDN
        {
            get { return _cmsDescriptionDN; }
            set { _cmsDescriptionDN = value; }
        }

        /// <summary>
        /// Gets or sets the cmsCreatedDate value.
        /// </summary>
        public DateTime cmsCreatedDate
        {
            get { return _cmsCreatedDate; }
            set { _cmsCreatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the cmsUpdatedDate value.
        /// </summary>
        public DateTime cmsUpdatedDate
        {
            get { return _cmsUpdatedDate; }
            set { _cmsUpdatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the cmsIsDeleted value.
        /// </summary>
        public Boolean cmsIsDeleted
        {
            get { return _cmsIsDeleted; }
            set { _cmsIsDeleted = value; }
        }

        /// <summary>
        /// Gets or sets the cmsIsActive value.
        /// </summary>
        public Boolean cmsIsActive
        {
            get { return _cmsIsActive; }
            set { _cmsIsActive = value; }
        }

        /// <summary>
        /// Gets or sets the cmsCompanyId value.
        /// </summary>
        public Int32 cmsCompanyId
        {
            get { return _cmsCompanyId; }
            set { _cmsCompanyId = value; }
        }

        /// <summary>
        /// Gets or sets the cmsCreateBy value.
        /// </summary>
        public String cmsCreateBy
        {
            get { return _cmsCreateBy; }
            set { _cmsCreateBy = value; }
        }

        #endregion


        #region Methods

        public void InsertCms()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[9];
            
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = Convert.ToDateTime("1/1/9999 11:59:59 PM");
                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@cmsName";
                param[0].DataType = DbType.String;
                if (String.IsNullOrEmpty(_cmsName))
                    param[0].value = DBNull.Value;
                else
                    param[0].value = _cmsName;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@cmsDescription";
                param[1].DataType = DbType.String;
                if (String.IsNullOrEmpty(_cmsDescription))
                    param[1].value = DBNull.Value;
                else
                    param[1].value = _cmsDescription;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@cmsCreatedDate";
                param[2].DataType = DbType.DateTime;
                if (_cmsCreatedDate.Ticks > dtmin.Ticks && _cmsCreatedDate.Ticks < dtmax.Ticks)
                    param[2].value = _cmsCreatedDate;
                else if (_cmsCreatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[2].value = _cmsCreatedDate;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@cmsUpdatedDate";
                param[3].DataType = DbType.DateTime;
                if (_cmsUpdatedDate.Ticks > dtmin.Ticks && _cmsUpdatedDate.Ticks < dtmax.Ticks)
                    param[3].value = _cmsUpdatedDate;
                else if (_cmsUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[3].value = _cmsUpdatedDate;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@cmsIsDeleted";
                param[4].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_cmsIsDeleted.ToString()))
                    param[4].value = DBNull.Value;
                else
                    param[4].value = _cmsIsDeleted;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@cmsIsActive";
                param[5].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_cmsIsActive.ToString()))
                    param[5].value = DBNull.Value;
                else
                    param[5].value = _cmsIsActive;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@cmsCompanyId";
                param[6].DataType = DbType.Int32;                
                param[6].value = _cmsCompanyId;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@cmsCreateBy";
                param[7].DataType = DbType.String;
                if (String.IsNullOrEmpty(_cmsCreateBy))
                    param[7].value = DBNull.Value;
                else
                    param[7].value = _cmsCreateBy;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@cmsDescriptionDN";
                param[8].DataType = DbType.String;
                if (String.IsNullOrEmpty(_cmsDescriptionDN))
                    param[8].value = DBNull.Value;
                else
                    param[8].value = _cmsDescriptionDN;

                _ds = DBAccess.ExecDataSet("InsertCms", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        public void UpdateCms()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[5];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = Convert.ToDateTime("1/1/9999 11:59:59 PM");

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@cmsId";
                param[0].DataType = DbType.Int32;
                param[0].value = _cmsId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@cmsName";
                param[1].DataType = DbType.String;
                if (String.IsNullOrEmpty(_cmsName))
                    param[1].value = DBNull.Value;
                else
                    param[1].value = _cmsName;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@cmsDescription";
                param[2].DataType = DbType.String;
                if (String.IsNullOrEmpty(_cmsDescription))
                    param[2].value = DBNull.Value;
                else
                    param[2].value = _cmsDescription;


                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@cmsUpdatedDate";
                param[3].DataType = DbType.DateTime;
                if (_cmsUpdatedDate.Ticks > dtmin.Ticks && _cmsUpdatedDate.Ticks < dtmax.Ticks)
                    param[3].value = _cmsUpdatedDate;
                else if (_cmsUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[3].value = _cmsUpdatedDate;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@cmsDescriptionDN";
                param[4].DataType = DbType.String;
                if (String.IsNullOrEmpty(_cmsDescriptionDN))
                    param[4].value = DBNull.Value;
                else
                    param[4].value = _cmsDescriptionDN;


                _ds = DBAccess.ExecDataSet("UpdateCms", CommandType.StoredProcedure, param);
                _returnBoolean = true;
                //obj.ExecScalar("UpdateCms", CommandType.StoredProcedure, param); _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        public void GetAllCms()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@cmsIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _cmsIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@cmsIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _cmsIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@cmsCompanyId";
                param[2].DataType = DbType.Int32;
                param[2].value = _cmsCompanyId;

                _ds = DBAccess.ExecDataSet("GetAllCms", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllcity" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void cmsStatusUpdate()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] pm = new ParamStruct[3];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@cmsId";
                pm[0].DataType = DbType.Int32;
                pm[0].value = _cmsId;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@cmsIsActive";
                pm[1].DataType = DbType.Boolean;
                pm[1].value = _cmsIsActive;

                pm[2].direction = ParameterDirection.Input;
                pm[2].ParamName = "@cmsIsDeleted";
                pm[2].DataType = DbType.Boolean;
                pm[2].value = _cmsIsDeleted;

                _ds = DBAccess.ExecDataSet("cmsStatusUpdate", CommandType.StoredProcedure, pm);
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in cmsStatusUpdate  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            }
        }

        public void GetAllcmsbyid()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@cmsId";
                param[0].DataType = DbType.Int32;
                param[0].value = _cmsId;


                _ds = DBAccess.ExecDataSet("GetAllcmsbyid", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllcmsbyid" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void cmsCheckDuplication(string name, Int32 id)
        {
            try
            {
                DataAccess obj = new DataAccess();
                obj.Provider = EnumProviders.SQLClient;
                obj.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["connstring"].ToString();

                ParamStruct[] param = new ParamStruct[2];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = Convert.ToDateTime("12/31/9999 11:59:59 PM");

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@cmsName";
                param[0].DataType = DbType.String;
                param[0].value = name;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@cmsId";
                param[1].DataType = DbType.Int32;
                param[1].value = id;

                _ds = obj.ExecDataSet("cmsCheckDuplication", CommandType.StoredProcedure, param);
                if (_ds.Tables[0].Rows.Count > 0)
                {
                    _returnString = (_ds.Tables[0].Rows[0]["returnStatement"].ToString());

                }

            }
            catch (DataException ex)
            {
                throw ex;
            }


        }

        public void SelectMailTemplateByName()
        {
            DataAccess DBAccess = new DataAccess();
            DBAccess.Provider = EnumProviders.SQLClient;
            DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["connstring"].ToString();
            ParamStruct[] pm = new ParamStruct[1];

            pm[0].direction = ParameterDirection.Input;
            pm[0].ParamName = "@cmsName";
            pm[0].DataType = DbType.String;
            pm[0].value = _cmsName;

            _ds = DBAccess.ExecDataSet("MailTemplate_SelectByName", CommandType.StoredProcedure, pm);


        }


        #endregion
    }
}
