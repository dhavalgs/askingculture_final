﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using starteku_BusinessLogic;
using starteku_BusinessLogic.Model;

/// <summary>
/// Summary description for GetPersonalDevelopmentPlanComBM
/// </summary>
public class GetPersonalDevelopmentPlanComBM
{
	public GetPersonalDevelopmentPlanComBM()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static List<GetPersonalDevelopmentPlanCom_Result> GetPersonalDevelopmentPlanComRequestList(int PerCreaterId)
    {
        startetkuEntities1 db = DbOperation.GetDb();
        try
        {
            var getLists = db.GetPersonalDevelopmentPlanCom(PerCreaterId).ToList();
            return getLists;

        }
        catch (Exception ex)
        {

            ExceptionLogger.LogException(ex);
            return new List<GetPersonalDevelopmentPlanCom_Result>();
        }
        finally
        {
            CommonAttributes.DisposeDBObject(ref db);
        }
    }
}