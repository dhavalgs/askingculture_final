﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using starteku_BusinessLogic;
using starteku_BusinessLogic.Model;



/// <summary>
/// Summary description for ActivityOverviewLogic
/// </summary>
public class ActivityOverviewLogic
{
    public ActivityOverviewLogic()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static List<Overview_GetPersonStatus_Result> GetPersonOverviewList(GetActivityLists_Result actObj,int CountSum=0)
    {
        var db = new startetkuEntities1();

        var actCatList = new List<Overview_GetPersonStatus_Result>();
        try
        {
            if (actObj.ActStartDate < DateTime.Now.AddYears(-100))
            {
                actObj.ActStartDate = CommonUtilities.GetCurrentDateTime().AddYears(-100);
            }
            if (actObj.ActEndDate < DateTime.Now.AddYears(-100))
            {
                actObj.ActEndDate = CommonUtilities.GetCurrentDateTime().AddYears(100);
            }

            actCatList = db.Overview_GetPersonStatus(actObj.ActId,
                        actObj.ActCreatedBy,
                        actObj.ActCompanyId,
                        actObj.ActReqUserId,
                        actObj.ActReqId,
                        actObj.ActReqUserId,
                        actObj.ActCatRefId,
                        actObj.ActStartDate,
                        actObj.ActEndDate,
                        actObj.ActDivId,
                        actObj.ActJobId,
                        actObj.ActCompId,
                        actObj.ActName,
                        actObj.ActIsActive,
                        actObj.comIsDeleted, CountSum,actObj.userTeamID).ToList();
        }

        catch (Exception ex)
        {

            ExceptionLogger.LogException(ex);
            return new List<Overview_GetPersonStatus_Result>();
        }
        finally
        {

            CommonAttributes.DisposeDBObject(ref db);
        }

        return actCatList;
    }
    

    public static List<Overview_GetActivityStatus_Result> GetActivityOverviewList(GetActivityLists_Result actObj)
    {
        var db = new startetkuEntities1();

        var actCatList = new List<Overview_GetActivityStatus_Result>();
        try
        {
            actCatList = db.Overview_GetActivityStatus(actObj.ActId, actObj.ActCreatedBy, actObj.ActCompanyId, actObj.ActReqUserId, actObj.ActReqId, actObj.ActReqUserId, actObj.ActCatRefId, null, null,
                            actObj.ActDivId, actObj.ActJobId, actObj.ActCompId, actObj.ActName, actObj.ActIsActive, actObj.comIsDeleted,actObj.userTeamID).ToList();

        }

        catch (Exception ex)
        {

            ExceptionLogger.LogException(ex);
            return new List<Overview_GetActivityStatus_Result>();
        }
        finally
        {

            CommonAttributes.DisposeDBObject(ref db);
        }

        return actCatList;
    }

    public static List<Overview_GetActivityStatus_Total_Result> GetActivityOverviewList_Total(GetActivityLists_Result actObj)
    {
        var db = new startetkuEntities1();

        var actCatList = new List<Overview_GetActivityStatus_Total_Result>();
        try
        {
            actCatList = db.Overview_GetActivityStatus_Total(actObj.ActId, actObj.ActCreatedBy, actObj.ActCompanyId, actObj.ActReqUserId, actObj.ActReqId, actObj.ActReqUserId, actObj.ActCatRefId, null, null,
                            actObj.ActDivId, actObj.ActJobId, actObj.ActCompId, actObj.ActName, actObj.ActIsActive, actObj.comIsDeleted,actObj.userTeamID).ToList();

        }

        catch (Exception ex)
        {

            ExceptionLogger.LogException(ex);
            return new List<Overview_GetActivityStatus_Total_Result>();
        }
        finally
        {

            CommonAttributes.DisposeDBObject(ref db);
        }

        return actCatList;
    }


    public static List<Overview_GetPersonStatus_Tab2_Result> GetPersonOverviewListTab2(GetActivityLists_Result actObj)
    {
        var db = new startetkuEntities1();

        var actCatList = new List<Overview_GetPersonStatus_Tab2_Result>();
        try
        {
            actCatList = db.Overview_GetPersonStatus_Tab2(actObj.ActId, actObj.ActCreatedBy, actObj.ActCompanyId, actObj.ActReqUserId, actObj.ActReqId, actObj.ActReqUserId, actObj.ActCatRefId, null, null,
                            actObj.ActDivId, actObj.ActJobId, actObj.ActCompId, actObj.ActName, actObj.ActIsActive, actObj.comIsDeleted,actObj.userTeamID).ToList();

        }

        catch (Exception ex)
        {

            ExceptionLogger.LogException(ex);
            return new List<Overview_GetPersonStatus_Tab2_Result>();
        }
        finally
        {

            CommonAttributes.DisposeDBObject(ref db);
        }

        return actCatList;
    }

    public static List<Overview_GetPersonStatus_Tab2_Total_Result> GetPersonOverviewListTab2_Total(GetActivityLists_Result actObj)
    {
        var db = new startetkuEntities1();

        var actCatList = new List<Overview_GetPersonStatus_Tab2_Total_Result>();
        try
        {
            actCatList = db.Overview_GetPersonStatus_Tab2_Total(actObj.ActId, actObj.ActCreatedBy, actObj.ActCompanyId, actObj.ActReqUserId, actObj.ActReqId, actObj.ActReqUserId, actObj.ActCatRefId, null, null,
                            actObj.ActDivId, actObj.ActJobId, actObj.ActCompId, actObj.ActName, actObj.ActIsActive, actObj.comIsDeleted,actObj.userTeamID).ToList();

        }

        catch (Exception ex)
        {

            ExceptionLogger.LogException(ex);
            return new List<Overview_GetPersonStatus_Tab2_Total_Result>();
        }
        finally
        {

            CommonAttributes.DisposeDBObject(ref db);
        }

        return actCatList;
    }
}