﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starteku.Data.Manager;
using System.Data;
using System.Web;

namespace starteku_BusinessLogic
{
    public class PDPHistoryBM
    {
        #region Private Declaration

        private DataSet _ds;

        private Int32 _PdpCompanyID;
        private Int32 _PdpManagerID;
        private Int32 _PdpUserID;
        private String _PdpHisContent;
        #endregion

        public DataSet ds
        {
            get { return _ds; }
            set { _ds = value; }
        }
        public Int32 PdpCompanyID
        {
            get { return _PdpCompanyID; }
            set { _PdpCompanyID = value; }
        }

        public Int32 PdpManagerID
        {
            get { return _PdpManagerID; }
            set { _PdpManagerID = value; }
        }

        public Int32 PdpUserID
        {
            get { return _PdpUserID; }
            set { _PdpUserID = value; }
        }
        public String PdpHisContent
        {
            get { return _PdpHisContent; }
            set { _PdpHisContent = value; }
        }


        #region Methods


        public void InsertPDPHistory()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[4];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@PdpCompanyID";
                param[0].DataType = DbType.Int32;
                param[0].value = _PdpCompanyID;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@PdpManagerID";
                param[1].DataType = DbType.Int32;
                param[1].value = _PdpManagerID;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@PdpUserID";
                param[2].DataType = DbType.Int32;
                param[2].value = _PdpUserID;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@PdpHisContent";
                param[3].DataType = DbType.String;
                param[3].value = _PdpHisContent;



                _ds = DBAccess.ExecDataSet("InsertPDPHistory", CommandType.StoredProcedure, param);
                
            }
            catch (DataException ex)
            {
                ExceptionLogger.LogException(ex);
            }
        }

        public void GetHistory()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[4];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@PdpCompanyID";
                param[0].DataType = DbType.Int32;
                param[0].value = _PdpCompanyID;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@PdpManagerID";
                param[1].DataType = DbType.Int32;
                param[1].value = _PdpManagerID;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@PdpUserID";
                param[2].DataType = DbType.Int32;
                param[2].value = _PdpUserID;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@PdpHisContent";
                param[3].DataType = DbType.String;
                param[3].value = _PdpHisContent;



                _ds = DBAccess.ExecDataSet("InsertPDPHistory", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                ExceptionLogger.LogException(ex);
            }
        }
        #endregion
    }
}
