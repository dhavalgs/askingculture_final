﻿//20141219 | Saurin



function ExampanCollapsMenu_Old() {
    /*$("html, body").animate({ scrollTop: 20 }, "slow");*/
    
    $('.nestMenu').toggle();
    if ($(".moreText").text() == "Expand") {
       
        $(".moreText").text("Collapse");


    } else {
       
        $(".moreText").text("Expand");
        $(".moreText1").text("Expand");
        $('.nestsubMenu').hide();

    }

}
function ExampanCollapsMenu() {
    //alert();
    /*$("html, body").animate({ scrollTop: 20 }, "slow");*/
    
    $('.nestMenu').toggle();

    if ($(".moreText").val() == "Expand") {
       
        $(".moreText").val("Collapse");

        $(".moreText").text($(".hdnCollaps").text());


    } else {
       
        $(".moreText").val("Expand");
        $(".moreText1").val("Expand");
        $(".moreText1").text($(".hdnExpand").text());
        $(".moreText").text($(".hdnExpand").text());
        $('.nestsubMenu').hide();

    }

}
function toggleFullScreen() {
    if ((document.fullScreenElement && document.fullScreenElement !== null) ||
     (!document.mozFullScreen && !document.webkitIsFullScreen)) {
        if (document.documentElement.requestFullScreen) {
            document.documentElement.requestFullScreen();
        } else if (document.documentElement.mozRequestFullScreen) {
            document.documentElement.mozRequestFullScreen();
        } else if (document.documentElement.webkitRequestFullScreen) {
            document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
        }
    } else {
        if (document.cancelFullScreen) {
            document.cancelFullScreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
        }
    }
}
function ExampanCollapssubMenu_Old() {

    /*$("html, body").animate({ scrollTop: 50 }, "slow");*/
    $('.nestsubMenu').toggle("slow");

    if ($(".moreText1").text() == "Expand") {
        $(".moreText1").text("Collapse");
        //$("html, body").animate({ scrollTop: 350 }, "slow");
    } else {
        $(".moreText1").text("Expand");
        //$("html, body").animate({ scrollTop: 50 }, "slow");
    }

}
function ExampanCollapssubMenu() {

    /*$("html, body").animate({ scrollTop: 50 }, "slow");*/
    $('.nestsubMenu').toggle("slow");
    if ($(".moreText1").val() == "Expand") {

        $(".moreText1").val("Collapse");
        $(".moreText1").text($(".hdnCollaps").text());
        //$(".moreText1").text("Collapse");
        //$("html, body").animate({ scrollTop: 350 }, "slow");
    } else {
        $(".moreText1").val("Expand");
        $(".moreText1").text($(".hdnExpand").text());
        //$("html, body").animate({ scrollTop: 50 }, "slow");
    }

}
function accordian() {
    return;
    $(function () {
        $("#accordion").accordion({
            collapsible: true, heightStyle: "scroll_checkboxes"
        });
    });

    $(function () {
        $("#accordion-resizer").resizable({
            minHeight: 140,
            minWidth: 200,
            resize: function () {
                $("#accordion").accordion("refresh");
            }
        });
    });
    /*
    * hoverIntent | Copyright 2011 Brian Cherne
    * http://cherne.net/brian/resources/jquery.hoverIntent.html
    * modified by the jQuery UI team
    */
    $.event.special.hoverintent = {
        setup: function () {
            $(this).bind("mouseover", jQuery.event.special.hoverintent.handler);
        },
        teardown: function () {
            $(this).unbind("mouseover", jQuery.event.special.hoverintent.handler);
        },
        handler: function (event) {
            var currentX, currentY, timeout,
args = arguments,
target = $(event.target),
previousX = event.pageX,
previousY = event.pageY;
            function track(event) {
                currentX = event.pageX;
                currentY = event.pageY;
            };
            function clear() {
                target
.unbind("mousemove", track)
.unbind("mouseout", clear);
                clearTimeout(timeout);
            }
            function handler() {
                var prop,
orig = event;
                if ((Math.abs(previousX - currentX) +
Math.abs(previousY - currentY)) < 7) {
                    clear();
                    event = $.Event("hoverintent");
                    for (prop in orig) {
                        if (!(prop in event)) {
                            event[prop] = orig[prop];
                        }
                    }
                    // Prevent accessing the original event since the new event
                    // is fired asynchronously and the old event is no longer
                    // usable (#6028)
                    delete event.originalEvent;
                    target.trigger(event);
                } else {
                    previousX = currentX;
                    previousY = currentY;
                    timeout = setTimeout(handler, 100);
                }
            }
            timeout = setTimeout(handler, 100);
            target.bind({
                mousemove: track,
                mouseout: clear
            });
        }
    };
}

function handleJqueryConflictForCheckBoxlist() {
    return;
    setTimeout(function () {
        $accui(function () {
            $accui("#accordion").accordion({
                collapsible: true, heightStyle: "scroll_checkboxes"
            });
        });

        $accui(function () {
            $accui("#accordion-resizer").resizable({
                minHeight: 140,
                minWidth: 200,
                resize: function () {
                    $accui("#accordion").accordion("refresh");
                }
            });
        });
    }, 500);

}

function modelPopupDilouge(title, msg, isSuccess) {
    
    if (isSuccess) {
        $(".ui-widget-header").css("background", "linear-gradient(to bottom, #40a5db 0%, #005c9d 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)");
    } else {
        $(".ui-widget-header").css("background", "linear-gradient(to bottom, gray 0%, red 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)");

    }
    $(".ui-widget-header").css("color", "white");
    setTimeout(function () {

        /*Ref : http://jqueryui.com/dialog/#modal-confirmation*/
        $("#saveSuccessDilouge").attr('title', title);
        $("#saveSuccessDilouge p").text(msg);
        $("#saveSuccessDilouge").dialog({
            show: {
                effect: "blind",
                duration: 1000
            },
            hide: {
                effect: "fade",
                duration: 1000
            },
            "title": title, //NEW!,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery('#saveSuccessDilouge').dialog('close');
                });
            }

        });
        $('.modelDilougBox').show();
        $('*').on('click', function (e) {
            $('.modelDilougBox').dialog("close");
        });
    }, 2000);
}

function ShowCompotenceCategory(toggle) {

    accordian();

    if (toggle) {
        
        if (!CheckValidations("chkNewEmp")) {

            //Ahh... User not validate all fields... Let him inform


        } else {
            toggleSubmitButtons(true);
            $("#CompetenceCheckboxes").show();
            $(".otherDiv").hide();
        }

    } else {

        //this functions will make user on top main div
        toggleSubmitButtons(false);
        $("#CompetenceCheckboxes").hide();
        $(".otherDiv").show();
       // showpopup1();
    }

}

function toggleSubmitButtons(toggle) {
    if (toggle) {
        $("#submitButtons").show();
        $("#nextButtons").hide();

    } else {
        $("#submitButtons").hide();
        $("#nextButtons").show();

    }
}

function toggleShowHideButtons(toggle) {
    if (toggle) {
        $("#submitButtons").show();
    } else {
        $("#submitButtons").hide();
    }
}

function GetCheckBoxListValues(chkBoxID) {
    var chkBox = document.getElementById('<%=ddljobtype.ClientID%>');
    var options = chkBox.getElementsByTagName('input');
    var listOfSpans = chkBox.getElementsByTagName('span');
    for (var i = 0; i < options.length; i++) {
        if (options[i].checked) {
            alert(listOfSpans[i].attributes["JSvalue"].value);
        }
    }
}

function CheckValidations(groupName) {

    return window.Page_ClientValidate(groupName);
}


function SetNotificationTotal() {
    return;
    if ($("#lblcountnotification").text() == "0") return;

    var curVal = $("#lblcountnotification").text();

    
    $("#lblcountnotification").text(parseInt(currVal) - 1);
    $("#lblshownotification").text(parseInt(currVal) - 1);

    $.ajax({
        type: "POST",
        url: 'WebService1.asmx/UpdateNotification',
        data: "{'notIdQ':''}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            $("#divResult").html(msg.d);
        },
        error: function (e) {
            $("#divResult").html("WebSerivce unreachable");
        }
    });
}


//Saurin : 20150105 |Comman Ajax Call function- just pass webservice name in this function  | 

function CommonAJSON(strUrl, strData) {
    // ie. : >>  var checkVideo = CommonAJSON("VideoList.asmx/IsItABadVideo", '{watchID:"' + watchID + '",platform:"Web"}'); 
    var ReturnData;
    $.ajax({
        type: "POST",
        url: strUrl,
        data: strData,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            $("#divResult").html(msg.d);

            alert(msg.d);
            return msg.d;
        },
        error: function (e) {
            $("#divResult").html("WebSerivce unreachable");
        }
    });
    return ReturnData;
    alert("aaa");
}
//20141228 |Get   quesry string prameter   data|
function GetQueryParameterByName(name, url) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(url);  //window.location.search
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

function notificationShowHide() {

    $(".notification-btn").mouseenter(function () {
        $(this).next('.notification').fadeIn();
    });


    $(".notification").mouseleave(function () {
        $('.notification').fadeOut();
    });
}
