﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master" AutoEventWireup="true" CodeFile="OrgCountryList.aspx.cs" Inherits="Organisation_OrgCountryList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/ajaxtab.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg"></asp:Label>
    <div class="col-md-6">
        <div class="heading-sec">
            <h1>Country  <i><span runat="server" id="Country"></span></i></h1>
        </div>
    </div>

    <%-- <div class="col-md-3">
        <div class="dropdown-example">
            <ul class="nav nav-pills">
                <li class="dropdown" style="float: left!important; width: 100%;">
                    <a class="skill_dropdown" id="drop7" role="button" data-toggle="dropdown" href="#">Account Department<b class="skill_caret"></b></a>
                    <ul id="menu7" class="dropdown-menu" role="menu" aria-labelledby="drop7" style="width: 100%;">
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Account Department</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Sales Department</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Human Resources Department </a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Software Department</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Network Department</a></li>
                        <li role="presentation" class="divider"></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="report_all.html">All</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-md-3">
        <div class="dropdown-example">
            <ul class="nav nav-pills">
                <li class="dropdown" style="float: left!important; width: 100%;">
                    <a class="skill_dropdown" id="A1" role="button" data-toggle="dropdown" href="report_all.html">All<b class="skill_caret"></b></a>
                    <ul id="Ul1" class="dropdown-menu" role="menu" aria-labelledby="drop7" style="width: 100%;">
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="report.html">Original competence level</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">New competence level</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Development points</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Knowledge share</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Potential for development</a></li>
                        <li role="presentation" class="divider"></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">All</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>--%>

    <div class="col-md-12" style="margin-top: 20px;">
        <div id="graph-wrapper">
            <div class="col-md-12">
                <a href="#add-post-title" data-toggle="modal" title="" style="margin-bottom: 15px;">
                    <button style="border: 0px;" class="btn btn-primary yellow" type="button">Add New Country</button>
                </a>
                <div aria-hidden="true" role="dialog" tabindex="-1" class="modal fade" id="add-post-title" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header blue">
                                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">× </button>
                                <h4 class="modal-title">Add New Country</h4>
                            </div>
                            <div class="modal-body">
                                <asp:TextBox runat="server" placeholder="Country Name :" Text="" ID="txtcountryName" MaxLength="50" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtcountryName"
                                    ErrorMessage="Please enter first name." CssClass="commonerrormsg" Display="Dynamic"
                                    ValidationGroup="chk"></asp:RequiredFieldValidator>
                            </div>
                            <div class="modal-footer">
                                <asp:Button runat="server" ID="btnsubmit" Text="Submit" CssClass="btn btn-primary yellow" ValidationGroup="chk" OnClick="btnsubmit_click" />
                                <button data-dismiss="modal" class="btn btn-default black" type="button">Close </button>

                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                </div>
                <br />
                <br />
                <div class="chart-tab">
                    <div id="tabs-container">
                        <cc1:TabContainer ID="TabContainer1" runat="server" CssClass="Tab">
                            <cc1:TabPanel ID="tbpnluser" runat="server">
                                <HeaderTemplate>
                                    Country
                                <a></a>
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <div class="invoice" style="background-color: white; margin-top: 0px;">
                                        <asp:GridView ID="gvGrid" runat="server" AutoGenerateColumns="False" CellPadding="0"
                                            Width="100%" GridLines="None" DataKeyNames="couId" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                                            EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>' OnRowCommand="gvGrid_RowCommand">
                                            <HeaderStyle CssClass="aa" />
                                            <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                            <AlternatingRowStyle BackColor="White" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Sr.No">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow" Width="10%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Country Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUserNamer" runat="server" Text="<%# bind('couName') %>"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow" Width="40%" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Action" ItemStyle-Width="50px">
                                                    <ItemTemplate>
                                                        <div class="vat" style="width: 50%;">
                                                            <p>
                                                                <i class="fa fa-pencil"></i>
                                                                <a href="<%# String.Format("OrgCountry.aspx?id={0}", Eval("couId")) %>"
                                                                    title="Edit">Edit</a>
                                                            </p>
                                                        </div>
                                                        <div class="total" style="width: 50%;">
                                                            <p>
                                                                <i class="fa fa-trash-o"></i>
                                                                <asp:LinkButton ID="lnkBtnName" runat="server" CommandName="archive" CommandArgument='<%# Eval("couId") %>'
                                                                    ToolTip="Archive" OnClientClick="return confirm('Are you sure you want to archive this record?');">Archive</asp:LinkButton>
                                                            </p>
                                                        </div>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow" Width="10%" />
                                                </asp:TemplateField>

                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </ContentTemplate>
                            </cc1:TabPanel>
                            <cc1:TabPanel ID="tbpnlusrdetails" runat="server">
                                <HeaderTemplate>
                                    Archive
                                <a></a>
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <div class="invoice" style="background-color: white; margin-top: 0px;">
                                        <asp:GridView ID="gvArchive" runat="server" AutoGenerateColumns="False" CellPadding="0"
                                            CellSpacing="0" Width="100%" GridLines="none"
                                            EmptyDataText='<%#CommonModule.msgGridRecordNotfound %>'
                                            DataKeyNames="couId" OnRowCommand="gvArchive_RowCommand"
                                            CssClass="table table-striped table-bordered table-hover table-checkable datatable">
                                            <HeaderStyle CssClass="aa" />
                                            <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                            <AlternatingRowStyle BackColor="White" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Sr.No" ItemStyle-Width="50px">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow" Width="20%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUserNamer" runat="server" Text="<%# bind('couName') %>">
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow" Width="20%" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Action" ItemStyle-Width="50px">
                                                    <ItemTemplate>
                                                        <span class="btn-group">
                                                            <asp:LinkButton ID="lnkbtnrestore" runat="server" CommandName="archive"
                                                                CommandArgument='<%# Eval("couId") %>'
                                                                ToolTip="Restore"
                                                                OnClientClick="return confirm('Are you sure you want to restore this record?');"
                                                                Text="Restore"></asp:LinkButton>

                                                            &nbsp;|&nbsp;
                                                    <asp:LinkButton ID="lnkBtnPermanentlydelete" runat="server" CommandName="permanentlydelete"
                                                        CommandArgument='<%# Eval("couId") %>' Text="Delete Permanently" ToolTip="Delete Permanently"
                                                        OnClientClick="return confirm('Are you sure you want to permanently delete this record?');"></asp:LinkButton>
                                                        </span>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow" Width="20%" />
                                                    <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </ContentTemplate>
                            </cc1:TabPanel>
                        </cc1:TabContainer>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

