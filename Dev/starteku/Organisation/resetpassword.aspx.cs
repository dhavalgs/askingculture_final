﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;using starteku_BusinessLogic;
using System.Data;
using System.Configuration;
using System.Net.Mail;

public partial class Organisation_resetpassword : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnForgot_ServerClick(object sender, EventArgs e)
    {
        try
        {
            Int32 userID = Convert.ToInt32(CommonModule.decrypt(Request.QueryString["id"]));
            UserBM obj1 = new UserBM();
            obj1.userId = userID;
            obj1.userPassword = CommonModule.encrypt(txtPassword.Text.Trim());
            obj1.Updatepassword();
            if (obj1.ReturnBoolean == true)
            {
                lblMsg.Text = CommonModule.msgPasswordHasBeenUpdateSuccess;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }

        }
        catch (Exception ex)
        {
            lblMsg.Text = CommonModule.msgSomeProblemOccure;
            CommonModule.WriteLog("error in btnsubmint  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
        }
    }
}