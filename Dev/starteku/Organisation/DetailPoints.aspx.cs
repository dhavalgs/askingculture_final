﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;

using System.Threading;
using System.Globalization;
using System.ComponentModel.DataAnnotations;

public partial class Organisation_DetailPoints : System.Web.UI.Page
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "my", " SetExpandCollapse();", true);

        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }

        if (!IsPostBack)
        {
            Employee.InnerHtml = GetLocalResourceObject("Welcome.Text").ToString() + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            GetAllpointSummery();
            SetDefaultMessage();
        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion

    #region method
    protected void SetDefaultMessage()
    {
        // lblMsg.Text = GetLocalResourceObject("lblHelloWorld.Text").ToString();

        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["msg"])))
        {
            if (Convert.ToString(Request.QueryString["msg"]) == "del")
            {
                //lblMsg.Text = CommonModule.msgRecordHasBeenDeletedSuccessfully;
                //lblMsg.Text = CommonMessages.msgRecordHasBeenDeletedSuccessfully;
                lblMsg.Text = GetLocalResourceObject("msgRecordHasBeenDeletedSuccessfully.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "ins")
            {
                //lblMsg.Text = CommonMessages.msgRecordInsertedSuccessfully;
                lblMsg.Text = GetLocalResourceObject("msgRecordInsertedSuccessfully.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "upd")
            {
                //lblMsg.Text = CommonMessages.msgRecordUpdatedSuccss;
                lblMsg.Text = GetLocalResourceObject("msgRecordUpdatedSuccss.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "arc")
            {
                // lblMsg.Text = CommonMessages.msgRecordArchiveSucces;
                lblMsg.Text = GetLocalResourceObject("CommonMessages.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
        }

    }
    protected void GetAllpointSummery()
    {
        Boolean plp = false;
        Boolean alp = false;
        Boolean id = false;
        String  name="";
        plp = false;
        alp = false;
        id = true;


        int userid = 0;
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if ((Convert.ToString(Session["OrguserType"]) == "3"))
            {
                userid = Convert.ToInt32(Session["OrgUserId"]);
            }
            else
            {
                userid = Convert.ToInt32(Request.QueryString["Uid"]);
            }
        }
        if (!String.IsNullOrEmpty(Request.QueryString["PLP"]))
        {
            Points.InnerText = Convert.ToString(Request.QueryString["PLP"]);
           name=Convert.ToString(Request.QueryString["PLP"]);
        }
        else if (!String.IsNullOrEmpty(Request.QueryString["ALP"]))
        {
            Points.InnerText = Convert.ToString(Request.QueryString["ALP"]);
            name = Convert.ToString(Request.QueryString["ALP"]);
        }
        else if (!String.IsNullOrEmpty(Request.QueryString["All"]))
        {
            var querystring = Convert.ToString(Request.QueryString["All"]);
            Points.InnerText = Regex.Replace(querystring, @"((?<=\p{Ll})\p{Lu})|((?!\A)\p{Lu}(?>\p{Ll}))", " $0");
            name = querystring;
        }
        PointBM obj = new PointBM();
        obj.GetUserPointByNameDetail(userid, plp, alp, true, id, name);
        DataSet ds = obj.ds;
        if (ds != null)
        {
            if (ds.Tables[1].Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[1].Rows[0]["name"])))
                {
                    lblUserName.Text = Convert.ToString(ds.Tables[1].Rows[0]["name"]);
                }
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                ViewState["logpointinfo"] = ds.Tables[0].Rows[0]["logPointInfo"];

                string strName = Convert.ToString(ds.Tables[0].Rows[0]["logPointInfo"]);
                


                if (name == "AcceptProvidedHelp")
                {
                    strName = lblAcceptProvidedHelp.Value;
                }
                else if (name == "ReadDoc")
                {
                    strName = lblReadDoc.Value;
                }
                else if (name == "ReadDocByOther")
                {
                    strName = lblReadDocByOther.Value;
                }
                else if (name == "RequestSession")
                {
                    strName = lblRequestSession.Value;
                }
                else if (name == "SuggestSession")
                {
                    strName = lblSuggestSession.Value;
                }

                else if (name == "UploadPublicDoc")
                {
                    strName = lblUploadPublicDoc.Value;
                }
                else if (name == "FinalFillCompetences")
                {
                    strName = lblFinalFillCompetences.Value;
                }
                else if (name == "OpenCompetencesScreen")
                {
                    strName = lblOpenCompetencesScreen.Value;
                }
                else if (name == "SignIn")
                {
                    strName = lblSignIn.Value;
                }


                Points.InnerText = strName;

                ds.Tables[0].AsEnumerable().ToList().ForEach(row => row["logPointInfo"] = strName);
                ds.Tables[0].AcceptChanges();
                gvGrid.DataSource = ds.Tables[0];
                gvGrid.DataBind();
                gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;

                AAlp.InnerHtml = Convert.ToString(ds.Tables[1].Rows[0]["TotalPoint"]);
            }
            else
            {
                gvGrid.DataSource = null;
                gvGrid.DataBind();
            }
        }
        else
        {
            gvGrid.DataSource = null;
            gvGrid.DataBind();
        }
    }
    ////protected void GetAllEmployeeList()
    ////{
    ////    Boolean plp = false;
    ////    Boolean alp = false;
    ////    Boolean id = false;
    ////    if (!String.IsNullOrEmpty(Request.QueryString["plp"]))
    ////    {
    ////        plp = true;
    ////        alp = false;
    ////        id = false;
    ////    }
    ////    else if (!String.IsNullOrEmpty(Request.QueryString["alp"]))
    ////    {
    ////        plp = false;
    ////        alp = true;
    ////        id = false;
    ////    }
    ////    else if (!String.IsNullOrEmpty(Request.QueryString["id"]))
    ////    {
    ////        plp = false;
    ////        alp = false;
    ////        id = true;
    ////    }
    ////    PointBM obj = new PointBM();
    ////    obj.GetUserPointByName(Convert.ToInt32(Session["OrgUserId"]),plp,alp,true,id);
       
    ////    DataSet ds = obj.ds;

    ////    if (ds != null)
    ////    {
    ////        if (ds.Tables[0].Rows.Count > 0)
    ////        {
    ////            gvGrid.DataSource = ds.Tables[0];
    ////            gvGrid.DataBind();
    ////            gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
    ////        }
    ////        else
    ////        {
    ////            gvGrid.DataSource = null;
    ////            gvGrid.DataBind();
    ////        }
    ////    }
    ////    else
    ////    {
    ////        gvGrid.DataSource = null;
    ////        gvGrid.DataBind();
    ////    }
    ////}
    #endregion

    #region Button Event
    protected void btnback_Click(object sender, EventArgs e)
    {


        if (!String.IsNullOrEmpty(Request.QueryString["Uid"]))
        {
            string str = Convert.ToString(Request.QueryString["Allpoint"]);
            if (string.IsNullOrEmpty(str))
            {
                

                if (!String.IsNullOrEmpty(Request.QueryString["PLP"]))
                {
                    Response.Redirect("apl_points.aspx?PLP=1");
                }
                else if (!String.IsNullOrEmpty(Request.QueryString["ALP"]))
                {
                    Response.Redirect("apl_points.aspx?ALP=1");
                }
                else if (!String.IsNullOrEmpty(Request.QueryString["All"]))
                {
                    Response.Redirect("apl_points.aspx?All=1");
                }
                Response.Redirect("apl_points.aspx?All=" + Request.QueryString["Uid"]);
            }
            else
            {
                if (!String.IsNullOrEmpty(Request.QueryString["PLP"]))
                {
                    Response.Redirect("apl_points.aspx?PLP=1&Allpoint="+Request.QueryString["Uid"]+"");
                }
                else if (!String.IsNullOrEmpty(Request.QueryString["ALP"]))
                {
                    Response.Redirect("apl_points.aspx?ALP=1&Allpoint=" + Request.QueryString["Uid"]+"");
                }
                else if (!String.IsNullOrEmpty(Request.QueryString["PDP"]))
                {
                    Response.Redirect("apl_points.aspx?PDP=1&Allpoint=" + Request.QueryString["Uid"]+"");
                }
                Response.Redirect("apl_points.aspx?Allpoint=" + Request.QueryString["Uid"]);
            }
           
            //if (!String.IsNullOrEmpty(Request.QueryString["PLP"]))
            //{
            //    Response.Redirect("apl_points.aspx?PLP=" + Request.QueryString["Uid"]);
            //}
            //else if (!String.IsNullOrEmpty(Request.QueryString["ALP"]))
            //{
            //    Response.Redirect("apl_points.aspx?ALP=" + Request.QueryString["Uid"]);
            //}
            //else if (!String.IsNullOrEmpty(Request.QueryString["All"]))
            //{
            //    Response.Redirect("apl_points.aspx?All=" + Request.QueryString["Uid"]);
            //}
        }
        else
        {
            if (!String.IsNullOrEmpty(Request.QueryString["PLP"]))
            {
                Response.Redirect("apl_points.aspx?PLP=1");
            }
            else if (!String.IsNullOrEmpty(Request.QueryString["ALP"]))
            {
                Response.Redirect("apl_points.aspx?ALP=1");
            }
            else if (!String.IsNullOrEmpty(Request.QueryString["All"]))
            {
                Response.Redirect("apl_points.aspx?All=1");
            }
        }
        //if (Convert.ToString(Session["OrguserType"]) == "3")
        //{
        //    Response.Redirect("Employee_dashboard.aspx");
        //}
        
    }

    #endregion



    #region Culture
    protected override void InitializeCulture()
    {
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Denish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion
}