﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp.text.html;

using starteku_BusinessLogic;
using System.Data;
using startetku.Business.Logic;
using starteku_BusinessLogic.View;
using System.Threading;
using System.Globalization;
using System.IO;

using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System.Configuration;


using ListItem = System.Web.UI.WebControls.ListItem;
using starteku_BusinessLogic.Model;
using Winnovative.PdfCreator;


public partial class Organisation_Report : System.Web.UI.Page
{
    #region variable
    public List<Int32> MainComIds = new List<Int32>();
    public static List<String> UserName = new List<String>();
    public static List<String> UserNameNew = new List<String>();
    public static List<String> UserNameDevlopment = new List<String>();
    public static List<String> UserNameKnowledgeDevelopment = new List<String>();
    public static List<String> comname = new List<String>();
    public List<Int32> NewMainComIds = new List<Int32>();
    public List<String> newcomname = new List<String>();
    public List<Int32> DevelopmentMainComIds = new List<Int32>();
    public EmployeeView ev = new EmployeeView();
    public static List<EmployeeView> evList = new List<EmployeeView>();
    public static List<EmployeeView> DevEvList = new List<EmployeeView>();
    public static List<EmployeeView> KnowledgeEvList = new List<EmployeeView>();
    public static List<EmployeeView> CompNewList = new List<EmployeeView>();

    public static List<String> Developmentcomname = new List<String>();
    public static List<String> DevelopmentcomnameV2 = new List<String>();
    public List<Int32> DevelopmentMainComIds1 = new List<Int32>();
    public List<String> Developmentcomname1 = new List<String>();
    public int total = 0; public int count = 0; public int Empcount = 0;

    public int Newtotal = 0; public int Newcount = 0; public int NewEmpcount = 0;
    public int Developmenttotal = 0; public int Developmentcount = 0; public int DevelopmentEmpcount = 0;
    public List<int> CountList = new List<int>();
    public List<int> NewCountList = new List<int>();
    public List<int> depCountList = new List<int>();
    public List<int> knowCountlist = new List<int>();


    #endregion

    #region Page Event

    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
            {
                Response.Redirect("login.aspx");
            }
            if (!IsPostBack)
            {
                Settings.InnerHtml = GetLocalResourceObject("Welcome.Text") + "  " + Convert.ToString(Session["OrgUserName"]) + "!";

                Page.ClientScript.RegisterStartupScript(this.GetType(), "", "SetSesssionVariableValue();", true);
                Session["hdnPdpChart"] = hdnPdpChart.Value;
                Session["hdnAlpPlpChart"] = hdnAlpPlpChart.Value;
                Session["hdnCompCatNaME"] = hdnCompCatNaME.Value;
                Session["hdnBasic"] = hdnBasic.Value;
                Session["hdnActual"] = hdnActual.Value;
                Session["hdnTarget"] = hdnTarget.Value;
                Session["hdnreportApproveBy"] = hdnreportApproveBy.Value;
                Session["hdnReportGraph"] = hdnReportGraph.Value;

                Session["hdnDocumentShared"] = hdnDocumentShared.Value;
                Session["hdndocType"] = hdndocType.Value;
                Session["hdndocSharedName"] = hdndocSharedName.Value;
                Session["hdndocName"] = hdndocName.Value;
                Session["hdndocDate"] = hdndocDate.Value;


                GetAllJobtype();
                GetAllDepartments();
                GetAllTeam();
                GetCompetenceMaster();
                GetAllEmployeeList();

                // string ComIDs = String.IsNullOrEmpty(ddtTemplateVal.Value) ? "0" : ddtTemplateVal.Value;

                string ComIDs = "0";
                ddlCompetence(ComIDs);

            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "Organisation_Report->PageLoad");
        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion

    #region background Process
    private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
    {
        Thread.Sleep(1000); // One second.
    }
    #endregion

    #region Method

    protected void ddlCompetence(string ComIDs)
    {
        try
        {


            CompetenceMasterBM obj = new CompetenceMasterBM();
            obj.comIsActive = true;
            obj.comIsDeleted = false;
            obj.comCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            obj.comJobtype = Convert.ToInt32(ddljobtype1.SelectedValue);
            obj.comDivId = Convert.ToInt32(ddlDepartment.SelectedValue);
            obj.TeamID = Convert.ToInt32(ddlTeam.SelectedValue);

            //  string ComIDs = String.IsNullOrEmpty(ddtTemplateVal.Value) ? "0" : ddtTemplateVal.Value;
            var usertype = Convert.ToInt32(Session["OrguserType"]);
            int usertyp = 0;
            if (usertype == 2)
            {
                usertyp = 3;
            }
            else
            {
                usertyp = 1;
            }
            Int32 usercreateby = 0;
            if (usertype == 1)
            {
                usercreateby = 0;
            }
            else
            {
                usercreateby = Convert.ToInt32(Session["OrgUserId"]);
            }

            obj.GetAllCompetenceMasterAddforaverage(usertyp, usercreateby, ComIDs);
            DataSet ds = obj.ds;
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    //if (Convert.ToString(Session["Culture"]) == "Danish")
                    //{
                    //    ds.Tables[0].Columns["catName"].ColumnName = "abcd";
                    //    ds.Tables[0].Columns["catNameDN"].ColumnName = "catName";

                    //    ds.Tables[0].Columns["comCompetence"].ColumnName = "abcde";
                    //    ds.Tables[0].Columns["comCompetenceDN"].ColumnName = "comCompetence";
                    //}

                    if (Convert.ToString(Session["Culture"]) == "Danish")
                    {
                        managercat.DataTextField = "comCompetenceDN";
                    }
                    else
                    {
                        managercat.DataTextField = "comCompetence";
                    }
                    managercat.DataValueField = "comId";
                    managercat.DataSource = ds.Tables[0];
                    managercat.DataBind();

                }
                ViewState["Competence"] = ds;
            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "ddlCompetence");
        }

    }
    protected void GetCompetenceMaster()
    {
        try
        {


            CompetenceMasterBM obj = new CompetenceMasterBM();
            obj.comIsActive = true;
            obj.comIsDeleted = false;
            obj.comCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            obj.comJobtype = Convert.ToInt32(ddljobtype1.SelectedValue);
            obj.comDivId = Convert.ToInt32(ddlDepartment.SelectedValue);
            obj.TeamID = Convert.ToInt32(ddlTeam.SelectedValue);

            string ComIDs = String.IsNullOrEmpty(ddtTemplateVal.Value) ? "0" : ddtTemplateVal.Value;
            var usertype = Convert.ToInt32(Session["OrguserType"]);
            int usertyp = 0;
            if (usertype == 2)
            {
                usertyp = 3;
            }
            else
            {
                usertyp = 1;
            }
            Int32 usercreateby = 0;
            if (usertype == 1)
            {
                usercreateby = 0;
            }
            else
            {
                usercreateby = Convert.ToInt32(Session["OrgUserId"]);
            }

            obj.GetAllCompetenceMasterAddforaverage(usertyp, usercreateby, ComIDs);
            DataSet ds = obj.ds;
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    if (Convert.ToString(Session["Culture"]) == "Danish")
                    {
                        ds.Tables[0].Columns["catName"].ColumnName = "abcd";
                        ds.Tables[0].Columns["catNameDN"].ColumnName = "catName";

                        ds.Tables[0].Columns["comCompetence"].ColumnName = "abcde";
                        ds.Tables[0].Columns["comCompetenceDN"].ColumnName = "comCompetence";

                    }

                }
            }
            ViewState["Competence"] = ds;
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "GetCompetenceMaster");
        }
    }
    protected void GetAllEmployeeList()
    {
        UserBM obj = new UserBM();
        obj.userIsActive = true;
        obj.userIsDeleted = false;
        obj.userCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var usertype = Convert.ToInt32(Session["OrguserType"]);
        if (usertype == 2)
        {
            obj.userType = 3;
        }
        else
        {
            obj.userType = 1;
        }
        if (usertype == 1)
        {
            obj.userCreateBy = Convert.ToString("0");
        }
        else
        {
            obj.userCreateBy = Convert.ToString(Session["OrgUserId"]);
        }
        obj.GetAllEmployeecreatebymanager_ViewPoint();



        //

        //
        var divid = 0;
        var jobtypeid = 0;
        var TeamID = 0;
        if (!string.IsNullOrWhiteSpace(ddlDepartment.SelectedValue))
        {
            divid = Convert.ToInt32(ddlDepartment.SelectedValue);
        }

        if (!string.IsNullOrWhiteSpace(ddljobtype1.SelectedValue))
        {
            jobtypeid = Convert.ToInt32(ddljobtype1.SelectedValue);
        }
        if (!string.IsNullOrWhiteSpace(ddlTeam.SelectedValue))
        {
            TeamID = Convert.ToInt32(ddlTeam.SelectedValue);
        }



        string filter;
        if (divid == 0)
        {

            if (jobtypeid == 0)
            {
                filter = "";

            }
            else
            {
                filter = "userJobType=" + jobtypeid;
            }
        }
        else
        {
            if (jobtypeid == 0)
            {
                filter = "userdepId=" + divid;
            }
            else
            {
                filter = "userdepId=" + divid + " and userJobType=" + jobtypeid;
            }

        }

        if (TeamID != 0)
        {
            if (filter == "")
            {
                filter = "userTeamID=" + TeamID;
            }
            else
            {
                filter = filter + " and userTeamID=" + TeamID;
            }
        }
        DataView dv = GetView(obj.ds, filter, "");
        //
        //
        if (dv != null)
        {


            var ds = dv.ToTable();
            ViewState["data"] = ds;
            if (ds != null)
            {
                Empcount = Convert.ToInt32(ds.Rows.Count);
                if (ds.Rows.Count > 0)
                {
                    rowRepeater.DataSource = ds;
                    rowRepeater.DataBind();

                    rptnew.DataSource = ds;
                    rptnew.DataBind();

                    rptDevelopment.DataSource = ds;
                    rptDevelopment.DataBind();

                    rpt_Knowledge.DataSource = ds;
                    rpt_Knowledge.DataBind();

                }
                else
                {
                    rowRepeater.DataSource = null;
                    rowRepeater.DataBind();

                    rptnew.DataSource = null;
                    rptnew.DataBind();

                    rptDevelopment.DataSource = null;
                    rptDevelopment.DataBind();

                    rpt_Knowledge.DataSource = null;
                    rpt_Knowledge.DataBind();
                }
            }
            else
            {
                rowRepeater.DataSource = null;
                rowRepeater.DataBind();

                rptnew.DataSource = null;
                rptnew.DataBind();

                rptDevelopment.DataSource = null;
                rptDevelopment.DataBind();

                rpt_Knowledge.DataSource = null;
                rpt_Knowledge.DataBind();

            }
        }
        Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "SubmitBtn();", true);

    }
    public static DataView GetView(DataSet ds, string filter, string sort)
    {
        try
        {
            DataView dv = new DataView(ds.Tables[0]);
            dv.Sort = sort;
            dv.RowFilter = filter;
            return dv;
        }
        catch (Exception ex)
        {

            Common.WriteLog("GetView===============Error============" + ex.StackTrace);
            return null;
        }
    }

    public static List<int> dataList = new List<Int32>();
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<JsonView> BtnSubmitAJAX(string txtName, string txtDesc)
    {
        try
        {

            var returnList = new List<JsonView>();


            foreach (var name in UserName)
            {

                foreach (var i in DevEvList)
                {
                    if (i.EmpID == null || i.EmpID == 0)
                    {
                        continue;
                    }



                    var uname = i.UserFullName;
                    if (name == uname)
                    {
                        dataList.Add(Convert.ToInt32(i.CompValue));
                    }
                    else
                    {
                        //  mylist += "" + 0 + ",";
                    }

                }
                returnList.Add(GetIntoList(name, dataList));
                dataList = new List<int>();

            }

            DevEvList = new List<EmployeeView>();
            UserName = new List<string>();
            // return mylist;

            //  var x = JsonConvert.SerializeObject(keys);
            return returnList;
        }
        catch (Exception e)
        {

            throw e;
        }
        // return "";
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<JsonView> DataNewComp(string txtName, string txtDesc)
    {
        try
        {
            comname.ToList();
            CompNewList.ToList();
            var x = evList;
            // return evList;
            var returnList = new List<JsonView>();

            var view = new JsonView();
            foreach (var name in UserNameDevlopment)
            {
                foreach (var i in CompNewList)
                {
                    if (i.EmpID == null || i.EmpID == 0)
                    {
                        continue;
                    }
                    var uname = i.UserFullName;
                    if (name == uname)
                    {
                        dataList.Add(Convert.ToInt32(i.CompValue));
                    }
                    else
                    {
                        //  mylist += "" + 0 + ",";
                    }

                }
                returnList.Add(GetIntoList(name, dataList));
                dataList = new List<int>();

            }

            CompNewList = new List<EmployeeView>();
            UserNameNew = new List<string>();

            return returnList;
        }
        catch (Exception e)
        {

            throw e;
        }
        // return "";
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<JsonView> DataDevelopmentComp(string txtName, string txtDesc)
    {
        try
        {



            var returnList = new List<JsonView>();




            foreach (var name in UserNameDevlopment)
            {


                foreach (var i in evList)
                {
                    if (i.EmpID == null || i.EmpID == 0)
                    {
                        continue;
                    }
                    UserMasterView user = null;
                    /* try
                     {
                         user = UserBM.GetUserByIdSPL(Convert.ToInt32(i.EmpID)).FirstOrDefault();
                     }
                     catch (Exception e)
                     {

                     }*/
                    var uname = i.UserFullName;
                    if (name == uname)
                    {
                        dataList.Add(Convert.ToInt32(i.CompValue));
                    }
                    else
                    {
                        //  mylist += "" + 0 + ",";
                    }

                }
                returnList.Add(GetIntoList(name, dataList));
                dataList = new List<int>();

            }

            CompNewList = new List<EmployeeView>();
            UserNameDevlopment = new List<string>();
            evList.Clear();
            // return mylist;

            //  var x = JsonConvert.SerializeObject(keys);
            return returnList;
        }
        catch (Exception e)
        {

            throw e;
        }
        // return "";
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]


    /*Knowledge Development-----------------------------*/
    public static List<JsonView> DataKnowledgeDevelopment(string txtName, string txtDesc)
    {
        try
        {



            var returnList = new List<JsonView>();




            foreach (var name in UserNameKnowledgeDevelopment)
            {


                foreach (var i in KnowledgeEvList)
                {
                    if (i.EmpID == null || i.EmpID == 0)
                    {
                        continue;
                    }
                    UserMasterView user = null;
                    /* try
                     {
                         user = UserBM.GetUserByIdSPL(Convert.ToInt32(i.EmpID)).FirstOrDefault();
                     }
                     catch (Exception e)
                     {

                     }*/
                    var uname = i.UserFullName;
                    if (name == uname)
                    {
                        dataList.Add(Convert.ToInt32(i.CompValue));
                    }
                    else
                    {
                        //  mylist += "" + 0 + ",";
                    }

                }
                returnList.Add(GetIntoList(name, dataList));
                dataList = new List<int>();

            }

            CompNewList = new List<EmployeeView>();
            UserNameKnowledgeDevelopment = new List<string>();
            KnowledgeEvList.Clear();
            // return mylist;

            //  var x = JsonConvert.SerializeObject(keys);
            return returnList;
        }
        catch (Exception e)
        {

            throw e;
        }
        // return "";
    }
    private static JsonView GetIntoList(string name, List<int> dataList)
    {
        var view = new JsonView();
        view.name = name;
        view.data = dataList;
        return view;
    }
    [WebMethod(EnableSession = true)]
    public static List<string> Developmentcomnames(string txtName, string txtDesc)
    {
        CompetenceMasterBM obj = new CompetenceMasterBM();
        obj.comIsActive = true;
        obj.comIsDeleted = false;
        obj.comCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
        var usertype = Convert.ToInt32(HttpContext.Current.Session["OrguserType"]);
        int usertyp = 0;
        if (usertype == 2)
        {
            usertyp = 3;
        }
        else
        {
            usertyp = 1;
        }
        Int32 usercreateby = 0;
        if (usertype == 1)
        {
            usercreateby = 0;
        }
        else
        {
            usercreateby = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        }

        obj.GetAllCompetenceMasterAddforaverage(usertyp, usercreateby, "0");
        DataSet ds = obj.ds;

        if (Convert.ToString(HttpContext.Current.Session["Culture"]) == "Danish")
        {
            ds.Tables[0].Columns["catName"].ColumnName = "abcd";
            ds.Tables[0].Columns["catNameDN"].ColumnName = "catName";

            ds.Tables[0].Columns["comCompetence"].ColumnName = "abcde";
            ds.Tables[0].Columns["comCompetenceDN"].ColumnName = "comCompetence";

        }
        var abc = new List<string>();

        if (ds != null)
        {
            var dr = ds.Tables[0];
            for (var i = 0; i < dr.Rows.Count; i++)
            {

                abc.Add(Convert.ToString(dr.Rows[i]["comCompetence"]));

            }

        }
        return abc.ToList();

    }
    #endregion

    #region Bind Grid Row DataBound
    protected void rowRepeater_ItemBound(object sender, RepeaterItemEventArgs e)
    {
        total = 0;
        count = 0;

        Label lblavg = e.Item.FindControl("lblavg") as Label;
        Repeater columnRepeater = e.Item.FindControl("columnRepeater") as Repeater;
        if (e.Item.ItemType == ListItemType.Header)
        {
            Repeater headerRepeater = e.Item.FindControl("headerRepeater") as Repeater;

            DataSet ds = (DataSet)ViewState["Competence"];

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    headerRepeater.DataSource = ds.Tables[0];
                    headerRepeater.DataBind();
                }
            }

            foreach (RepeaterItem bItem in headerRepeater.Items)
            {
                Label lblUserName = (Label)bItem.FindControl("lblUserName");//its a comp name
                Label lblcid = (Label)bItem.FindControl("lblcid"); //its a compId
                Label comId1 = e.Item.FindControl("comId1") as Label;
                MainComIds.Add(Convert.ToInt32(lblcid.Text));
                comname.Add((lblUserName.Text));

            }
        }
        else if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblUsername = e.Item.FindControl("lblhead1") as Label;

            if (lblUsername != null)
            {
                UserName.Add(lblUsername.Text);
                UserNameNew.Add(lblUsername.Text);
                UserNameDevlopment.Add(lblUsername.Text);
                UserNameKnowledgeDevelopment.Add(lblUsername.Text);
            }


            DataSet ds = (DataSet)ViewState["Competence"];
            if (ds != null)
            {

                if (ds.Tables[0].Rows.Count > 0)
                {
                    //count = ds.Tables[0].Rows.Count;
                    columnRepeater.DataSource = ds.Tables[0];
                    columnRepeater.DataBind();
                }
                else
                {

                }
            }
            else
            {

            }
            List<Int32> copyOfComIds = new List<int>();
            copyOfComIds.AddRange(MainComIds);

            HiddenField userId = e.Item.FindControl("userId") as HiddenField;
            count = ds.Tables[0].Rows.Count;

            int count2 = 0;
            int count1 = 0;
            foreach (RepeaterItem bItem in columnRepeater.Items)
            {


                Label lblhead = (Label)bItem.FindControl("lblhead");
                HiddenField hdnComid = (HiddenField)bItem.FindControl("hdnComid");
                Label Lacname = (Label)bItem.FindControl("Lacname");
                if (ds.Tables[0].Rows.Count > 0)
                {

                    foreach (var x in copyOfComIds)
                    {

                        try
                        {
                            int i = CountList[count1];
                        }
                        catch
                        {

                            CountList.Add(0);
                        }
                        count1 = count1 + 1;
                        EmployeeSkillBM obj = new EmployeeSkillBM();
                        obj.skillIsActive = true;
                        obj.skillIsDeleted = false;
                        obj.skillComId = Convert.ToInt32(x.ToString());
                        if (userId != null) obj.skillUserId = Convert.ToInt32(userId.Value);
                        obj.GetLevelByUserIdandcomId(Convert.ToInt32(hdnComid.Value));
                        DataSet ds1 = obj.ds;
                        if (ds1.Tables[0].Rows.Count > 0)
                        {
                            if (Convert.ToString(ds1.Tables[0].Rows[0]["isActiveSkill"]) == "0")
                            {
                                lblhead.ForeColor = System.Drawing.Color.Red;
                                Page.ClientScript.RegisterStartupScript(this.Page.GetType(), Guid.NewGuid().ToString() + "1", "ChangeColor('" + lblhead.ClientID + "');", true);
                                lblhead.Style.Add("font-weight", "bold");
                                count = count - 1;
                                CountList[count1 - 1] = CountList[count1 - 1] + 1;


                            }
                            else
                            {
                                total = total + Convert.ToInt32(ds1.Tables[0].Rows[0]["skillLocal"]);
                            }
                            lblhead.Text = Convert.ToString(ds1.Tables[0].Rows[0]["skillLocal"]);  //First table : ORIGINAL

                            DevEvList.Add(GetEmpSkillViewALL(
                                Convert.ToInt32(userId.Value),
                                Convert.ToDecimal(lblhead.Text),
                                Convert.ToInt32(x.ToString()) //ComId
                                ));
                            copyOfComIds.Remove(x);
                            break;
                        }
                        else
                        {

                            DevEvList.Add(GetEmpSkillViewALL(
                               Convert.ToInt32(userId.Value),
                               Convert.ToDecimal(0),
                               Convert.ToInt32(x.ToString()) //ComId
                               ));
                            lblhead.ForeColor = System.Drawing.Color.Red;
                            Page.ClientScript.RegisterStartupScript(this.Page.GetType(), Guid.NewGuid().ToString(), "ChangeColor('" + lblhead.ClientID + "');", true);
                            lblhead.Style.Add("font-weight", "bold");
                            CountList[count1 - 1] = CountList[count1 - 1] + 1;
                            count = count - 1;
                            copyOfComIds.Remove(x);
                            break;
                        }


                    }

                }

            }
            try
            {
                var k = Math.Round(((Convert.ToDecimal(total) / Convert.ToDecimal(count))), 1);
                lblavg.Text = Convert.ToString(k);
            }
            catch
            {

                lblavg.Text = Convert.ToString(0);
            }

            count1 = 0;
        }

        Repeater r1 = (Repeater)sender;

        if (e.Item.ItemType == ListItemType.Footer)
        {

            Repeater FooterRepeater = e.Item.FindControl("FooterRepeater") as Repeater;
            DataSet ds = (DataSet)ViewState["Competence"];
            List<int> intlist = CountList;
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    FooterRepeater.DataSource = ds.Tables[0];
                    FooterRepeater.DataBind();
                    int count12 = 0;
                    foreach (RepeaterItem bItem in FooterRepeater.Items)
                    {
                        var empcount = 0;
                        empcount = Empcount;


                        Repeater r = rowRepeater;
                        //Repeater r = (Repeater)rowRepeater.FindControl("columnRepeater");
                        //foreach (RepeaterItem bItem1 in r.Items)
                        //{
                        //    Label lblhead = (Label)bItem.FindControl("lblhead");
                        //    if (lblhead.ForeColor == System.Drawing.Color.Red)
                        //    {
                        //        empcount--;
                        //    }

                        //}
                        Label lblFavg = (Label)bItem.FindControl("lblFavg");
                        try
                        {
                            var k = Math.Round(((Convert.ToDecimal(lblFavg.Text) / Convert.ToDecimal((empcount - CountList[count12])))), 1);
                            lblFavg.Text = Convert.ToString(k);
                        }
                        catch
                        {
                            lblFavg.Text = Convert.ToString(0);
                        }

                        count12 = count12 + 1;
                    }

                }
            }
        }
    }
    protected void rptnew_ItemBound(object sender, RepeaterItemEventArgs e)
    {
        Newtotal = 0;
        Newcount = 0;

        Label lblavg = e.Item.FindControl("lblavg") as Label;
        Repeater columnRepeater = e.Item.FindControl("columnRepeater") as Repeater;
        if (e.Item.ItemType == ListItemType.Header)
        {
            Repeater headerRepeater = e.Item.FindControl("headerRepeater") as Repeater;

            DataSet ds = (DataSet)ViewState["Competence"];

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    headerRepeater.DataSource = ds.Tables[0];
                    headerRepeater.DataBind();
                }
            }

            foreach (RepeaterItem bItem in headerRepeater.Items)
            {
                Label lblUserName = (Label)bItem.FindControl("lblUserName");
                Label lblcid = (Label)bItem.FindControl("lblcid");
                HiddenField comId1 = e.Item.FindControl("comId1") as HiddenField;
                NewMainComIds.Add(Convert.ToInt32(lblcid.Text));
                newcomname.Add((lblUserName.Text));
            }
        }
        else if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DataSet ds = (DataSet)ViewState["Competence"];
            if (ds != null)
            {
                Newcount = ds.Tables[0].Rows.Count;
                if (ds.Tables[0].Rows.Count > 0)
                {
                    columnRepeater.DataSource = ds.Tables[0];
                    columnRepeater.DataBind();

                }
                else
                {

                }
            }
            else
            {

            }
            List<Int32> copyOfComIds = new List<int>();
            copyOfComIds.AddRange(NewMainComIds);

            HiddenField userId = e.Item.FindControl("userId") as HiddenField;

            int Newcount2 = 0;
            int Newcount1 = 0;

            foreach (RepeaterItem bItem in columnRepeater.Items)
            {
                Label lblhead = (Label)bItem.FindControl("lblhead");
                Label Lacname = (Label)bItem.FindControl("Lacname");
                HiddenField hdnComid = (HiddenField)bItem.FindControl("hdnComid");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (var x in copyOfComIds)
                    {
                        try
                        {
                            int i = NewCountList[Newcount1];
                        }
                        catch
                        {

                            NewCountList.Add(0);
                        }
                        Newcount1 = Newcount1 + 1;
                        EmployeeSkillBM obj = new EmployeeSkillBM();
                        obj.skillIsActive = true;
                        obj.skillIsDeleted = false;
                        obj.skillComId = Convert.ToInt32(x.ToString());
                        obj.skillUserId = Convert.ToInt32(userId.Value);
                        obj.GetLevelByUserIdandcomId(Convert.ToInt32(hdnComid.Value));
                        DataSet ds1 = obj.ds;
                        if (ds1.Tables[0].Rows.Count > 0)
                        {
                            if (Convert.ToString(ds1.Tables[0].Rows[0]["isActiveSkill"]) == "0")
                            {
                                lblhead.ForeColor = System.Drawing.Color.Red;
                                Page.ClientScript.RegisterStartupScript(this.Page.GetType(), Guid.NewGuid().ToString() + "2", "ChangeColor('" + lblhead.ClientID + "','1');", true);
                                lblhead.Style.Add("font-weight", "bold");
                                Newcount = Newcount - 1;
                                NewCountList[Newcount1 - 1] = NewCountList[Newcount1 - 1] + 1;

                            }

                            else
                            {
                                Newtotal = Newtotal + Convert.ToInt32(ds1.Tables[0].Rows[0]["skillAchive"]);
                            }

                            lblhead.Text = Convert.ToString(ds1.Tables[0].Rows[0]["skillAchive"]);

                            copyOfComIds.Remove(x);
                            CompNewList.Add(GetEmpSkillViewALL(
                              Convert.ToInt32(userId.Value),
                              Convert.ToDecimal(lblhead.Text),
                              Convert.ToInt32(x.ToString()) //ComId
                              ));
                            break;
                        }
                        else
                        {
                            CompNewList.Add(GetEmpSkillViewALL(
                             Convert.ToInt32(userId.Value),
                             Convert.ToDecimal(lblhead.Text),
                             Convert.ToInt32(x.ToString()) //ComId
                             ));
                            lblhead.ForeColor = System.Drawing.Color.Red;
                            Page.ClientScript.RegisterStartupScript(this.Page.GetType(), Guid.NewGuid().ToString(), "ChangeColor('" + lblhead.ClientID + "');", true);
                            lblhead.Style.Add("font-weight", "bold");
                            NewCountList[Newcount1 - 1] = NewCountList[Newcount1 - 1] + 1;
                            Newcount = Newcount - 1;
                            copyOfComIds.Remove(x);
                            break;
                        }
                    }
                }

            }
            try
            {
                var k = Math.Round(((Convert.ToDecimal(Newtotal) / Convert.ToDecimal(Newcount))), 1);
                lblavg.Text = Convert.ToString(k);
            }
            catch
            {

                lblavg.Text = Convert.ToString(0);
            }

            Newcount1 = 0;
        }
        if (e.Item.ItemType == ListItemType.Footer)
        {
            Repeater FooterRepeater = e.Item.FindControl("FooterRepeater") as Repeater;
            DataSet ds = (DataSet)ViewState["Competence"];

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    FooterRepeater.DataSource = ds.Tables[0];
                    FooterRepeater.DataBind();
                    int count12 = 0;
                    foreach (RepeaterItem bItem in FooterRepeater.Items)
                    {
                        var empcount = 0;
                        empcount = Empcount;
                        Label lblFavg = (Label)bItem.FindControl("lblFavg");
                        try
                        {
                            var k = Math.Round(((Convert.ToDecimal(lblFavg.Text) / Convert.ToDecimal((empcount - NewCountList[count12])))), 1);
                            lblFavg.Text = Convert.ToString(k);
                        }
                        catch
                        {
                            lblFavg.Text = Convert.ToString(0);
                        }

                        count12 = count12 + 1;


                    }

                }
            }
        }
    }
    protected void rptDevelopment_ItemBound(object sender, RepeaterItemEventArgs e)
    {
        Developmenttotal = 0;
        Developmentcount = 0;
        Label lblavg = e.Item.FindControl("lblavg") as Label;
        Repeater columnRepeater = e.Item.FindControl("columnRepeater") as Repeater;
        if (e.Item.ItemType == ListItemType.Header)
        {
            Repeater headerRepeater = e.Item.FindControl("headerRepeater") as Repeater;

            DataSet ds = (DataSet)ViewState["Competence"];

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    headerRepeater.DataSource = ds.Tables[0];
                    headerRepeater.DataBind();
                }
            }

            foreach (RepeaterItem bItem in headerRepeater.Items)
            {
                Label lblUserName = (Label)bItem.FindControl("lblUserName");
                Label lblcid = (Label)bItem.FindControl("lblcid");
                HiddenField comId1 = e.Item.FindControl("comId1") as HiddenField;
                DevelopmentMainComIds.Add(Convert.ToInt32(lblcid.Text));
                Developmentcomname.Add((lblUserName.Text));
                DevelopmentcomnameV2.Add((lblUserName.Text));


                // evList.Add(GetEmpSkillView(Convert.ToInt32(lblcid.Text)));
            }
        }
        else if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DataSet ds = (DataSet)ViewState["Competence"];
            if (ds != null)
            {
                Developmentcount = ds.Tables[0].Rows.Count;
                if (ds.Tables[0].Rows.Count > 0)
                {
                    columnRepeater.DataSource = ds.Tables[0];
                    columnRepeater.DataBind();

                }
                else
                {

                }
            }
            else
            {

            }
            List<Int32> copyOfComIds = new List<int>();
            copyOfComIds.AddRange(DevelopmentMainComIds);

            HiddenField userId = e.Item.FindControl("userId") as HiddenField;
            //


            //
            DevelopmentMainComIds1.Add(Convert.ToInt32(userId.Value));
            int depcount1 = 0;
            foreach (RepeaterItem bItem in columnRepeater.Items)
            {
                Label lblhead = (Label)bItem.FindControl("lblhead");
                Label Lacname = (Label)bItem.FindControl("Lacname");
                HiddenField hdnComid = (HiddenField)bItem.FindControl("hdnComid");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (var x in copyOfComIds)
                    {
                        try
                        {
                            int i = depCountList[depcount1];
                        }
                        catch
                        {

                            depCountList.Add(0);
                        }
                        depcount1 = depcount1 + 1;
                        EmployeeSkillBM obj = new EmployeeSkillBM();
                        obj.skillIsActive = true;
                        obj.skillIsDeleted = false;
                        obj.skillComId = Convert.ToInt32(x.ToString());
                        obj.skillUserId = Convert.ToInt32(userId.Value);
                        obj.GetLevelByUserIdandcomId(Convert.ToInt32(hdnComid.Value));
                        DataSet ds1 = obj.ds;
                        if (ds1.Tables[0].Rows.Count > 0)
                        {
                            if (Convert.ToString(ds1.Tables[0].Rows[0]["isActiveSkill"]) == "0")
                            {
                                lblhead.ForeColor = System.Drawing.Color.Red;
                                Page.ClientScript.RegisterStartupScript(this.Page.GetType(), Guid.NewGuid().ToString() + "3", "ChangeColor('" + lblhead.ClientID + "','1');", true);
                                lblhead.Style.Add("font-weight", "bold");
                                Developmentcount = Developmentcount - 1;
                                depCountList[depcount1 - 1] = depCountList[depcount1 - 1] + 1;



                            }
                            //  A/B*100 + ALP + PLP
                            decimal a = 0;
                            decimal f = 0;
                            try
                            {
                                if (string.IsNullOrWhiteSpace(Convert.ToString(ds1.Tables[0].Rows[0]["skillAchive"])))
                                {
                                    a = (Convert.ToDecimal(ds1.Tables[0].Rows[0]["skillLocal"]) / Convert.ToDecimal(ds1.Tables[0].Rows[0]["skillAchive"]));
                                }
                                else
                                {
                                    a = 0;
                                }
                            }
                            catch
                            {
                                a = 0;
                            }
                            var b = (Convert.ToDecimal(a) * 100);
                            if (ds1.Tables[1].Rows.Count > 0)
                            {
                                if (!String.IsNullOrEmpty(Convert.ToString(ds1.Tables[0].Rows[0]["PDPPoint"])))
                                {
                                    f = Math.Round((Convert.ToDecimal(b) + Convert.ToDecimal(ds1.Tables[0].Rows[0]["PDPPoint"])), 0);
                                }
                                else
                                {
                                    f = Math.Round((Convert.ToDecimal(b) + 0), 0);
                                }
                            }
                            else
                            {
                                f = Math.Round((Convert.ToDecimal(b) + 0), 0);
                            }


                            if (Convert.ToString(ds1.Tables[0].Rows[0]["isActiveSkill"]) == "0")
                            {
                                evList.Add(GetEmpSkillViewALL(Convert.ToInt32(userId.Value), 0, Convert.ToInt32(x.ToString())));
                            }
                            else
                            {
                                evList.Add(GetEmpSkillViewALL(Convert.ToInt32(userId.Value), f, Convert.ToInt32(x.ToString())));
                            }


                            /* evList.AddRange({
                                 comname = Developmentcomname.ToList();
                             });*/
                            lblhead.Text = Convert.ToString(f);
                            if (Convert.ToString(ds1.Tables[0].Rows[0]["isActiveSkill"]) == "0")
                            {
                            }
                            else
                            {
                                Developmenttotal = Developmenttotal + Convert.ToInt32(f);
                            }

                            copyOfComIds.Remove(x);
                            break;
                        }
                        else
                        {
                            evList.Add(GetEmpSkillViewALL(Convert.ToInt32(userId.Value), 0, Convert.ToInt32(x.ToString())));
                            lblhead.ForeColor = System.Drawing.Color.Red;
                            Page.ClientScript.RegisterStartupScript(this.Page.GetType(), Guid.NewGuid().ToString(), "ChangeColor('" + lblhead.ClientID + "');", true);
                            lblhead.Style.Add("font-weight", "bold");
                            depCountList[depcount1 - 1] = depCountList[depcount1 - 1] + 1;
                            Developmentcount = Developmentcount - 1;
                            copyOfComIds.Remove(x);
                            break;
                        }
                    }
                }

            }

            try
            {
                var k = Math.Round(((Convert.ToDecimal(Developmenttotal) / Convert.ToDecimal(Developmentcount))), 1);
                lblavg.Text = Convert.ToString(k);
            }
            catch
            {

                lblavg.Text = Convert.ToString(0);
            }


        }
        if (e.Item.ItemType == ListItemType.Footer)
        {
            Repeater FooterRepeater = e.Item.FindControl("FooterRepeater") as Repeater;
            DataSet ds = (DataSet)ViewState["Competence"];

            List<Int32> copyOfComIds1 = new List<int>();
            copyOfComIds1.AddRange(DevelopmentMainComIds1);


            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    FooterRepeater.DataSource = ds.Tables[0];
                    FooterRepeater.DataBind();
                    int count12 = 0;
                    foreach (RepeaterItem bItem in FooterRepeater.Items)
                    {
                        Label lblFavg = (Label)bItem.FindControl("lblFavg");
                        Label lblFavg1 = (Label)bItem.FindControl("lblFavg1");
                        Label lblComIdFooter = (Label)bItem.FindControl("lblComIdFooter");




                        decimal tt = 00;
                        foreach (var i in evList)
                        {
                            if (lblComIdFooter.Text == i.compId.ToString())
                            {
                                tt += i.CompValue;
                            }
                        }

                        var empcount = 0;
                        empcount = Empcount;
                        try
                        {
                            var k = Math.Round(((Convert.ToDecimal(tt) / Convert.ToDecimal(empcount - depCountList[count12]))), 1);
                            lblFavg.Text = Convert.ToString(k);
                        }
                        catch
                        {

                            lblFavg.Text = Convert.ToString(0);
                        }

                        count12 = count12 + 1;


                        //decimal a = 0;
                        //decimal f = 0;
                        //try
                        //{
                        //    a = (Convert.ToDecimal(lblFavg1.Text) / Convert.ToDecimal(lblFavg.Text));
                        //}
                        //catch
                        //{
                        //    a = 0;
                        //}
                        //var b = (Convert.ToDecimal(a) * 100);

                        //foreach (var x in copyOfComIds1)
                        //{
                        //    EmployeeSkillBM obj = new EmployeeSkillBM();
                        //    obj.skillIsActive = true;
                        //    obj.skillIsDeleted = false;
                        //    obj.skillComId = 0;
                        //    obj.skillUserId = Convert.ToInt32(x.ToString());
                        //    obj.GetLevelByUserIdandcomId();
                        //    DataSet ds1 = obj.ds;
                        //    if (ds1.Tables[1].Rows.Count > 0)
                        //    {

                        //        if (!String.IsNullOrEmpty(Convert.ToString(ds1.Tables[1].Rows[0]["pointTotal"])))
                        //        {
                        //            f = Math.Round((Convert.ToDecimal(b) + Convert.ToDecimal(ds1.Tables[1].Rows[0]["pointTotal"])), 0);
                        //        }
                        //        else
                        //        {
                        //            f = Math.Round((Convert.ToDecimal(b) + 0), 0);
                        //        }

                        //     //   f = Math.Round((Convert.ToDecimal(b) + Convert.ToDecimal(ds1.Tables[1].Rows[0]["pointTotal"])), 0);

                        //        lblFavg.Text = Convert.ToString(f);
                        //        copyOfComIds1.Remove(x);
                        //        break;
                        //    }
                        //    else
                        //    {
                        //        f = Math.Round((Convert.ToDecimal(b) + 0), 0);
                        //        lblFavg.Text = Convert.ToString(f);
                        //        copyOfComIds1.Remove(x);
                        //        break;
                        //    }
                        //}




                        //var k = Math.Round(((Convert.ToDecimal(lblFavg.Text) / Convert.ToDecimal(Empcount))), 1);
                        //lblFavg.Text = Convert.ToString(k);
                    }

                }
            }
        }
    }

    protected void rpt_Knowledge_ItemBound(object sender, RepeaterItemEventArgs e)
    {
        Newtotal = 0;
        Newcount = 0;
        Label lblavg = e.Item.FindControl("lblavg") as Label;
        Repeater columnRepeater = e.Item.FindControl("columnRepeater") as Repeater;
        if (e.Item.ItemType == ListItemType.Header)
        {
            Repeater headerRepeater = e.Item.FindControl("headerRepeater") as Repeater;
            DataSet ds = (DataSet)ViewState["Competence"];

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    headerRepeater.DataSource = ds.Tables[0];
                    headerRepeater.DataBind();
                }
            }

            foreach (RepeaterItem bItem in headerRepeater.Items)
            {
                Label lblUserName = (Label)bItem.FindControl("lblUserName");
                Label lblcid = (Label)bItem.FindControl("lblcid");

                NewMainComIds.Add(Convert.ToInt32(lblcid.Text));
                newcomname.Add((lblUserName.Text));
            }
        }
        else if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DataSet ds = (DataSet)ViewState["Competence"];
            if (ds != null)
            {
                Newcount = ds.Tables[0].Rows.Count;
                if (ds.Tables[0].Rows.Count > 0)
                {
                    columnRepeater.DataSource = ds.Tables[0];
                    columnRepeater.DataBind();

                }
                else
                {

                }
            }
            else
            {

            }
            List<Int32> copyOfComIds = new List<int>();
            copyOfComIds.AddRange(NewMainComIds);

            HiddenField userId = e.Item.FindControl("userId") as HiddenField;
            int knowcount1 = 0;
            foreach (RepeaterItem bItem in columnRepeater.Items)
            {
                Label lblhead = (Label)bItem.FindControl("lblhead");
                Label Lacname = (Label)bItem.FindControl("Lacname");
                HiddenField hdnComid = (HiddenField)bItem.FindControl("hdnComid");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (var x in copyOfComIds)
                    {
                        try
                        {
                            int i = knowCountlist[knowcount1];
                        }
                        catch
                        {

                            knowCountlist.Add(0);
                        }
                        knowcount1 = knowcount1 + 1;
                        //EmployeeSkillBM obj = new EmployeeSkillBM();
                        //obj.skillIsActive = true;
                        //obj.skillIsDeleted = false;
                        //obj.skillComId = Convert.ToInt32(x.ToString());
                        //obj.skillUserId = Convert.ToInt32(userId.Value);
                        //obj.GetLevelByUserIdandcomId();
                        Knowledge_PointBM obj = new Knowledge_PointBM();
                        obj.IsActive = true;
                        obj.IsDeleted = false;
                        obj.Knowledge_ComId = Convert.ToInt32(x.ToString());
                        obj.Knowledge_UserId = Convert.ToInt32(userId.Value);
                        obj.GetKnowledge_PointByUserIdandcomId(Convert.ToInt32(hdnComid.Value));
                        DataSet ds1 = obj.ds;
                        if (ds1.Tables[0].Rows.Count > 0)
                        {
                            if (Convert.ToString(ds1.Tables[0].Rows[0]["isActiveSkill"]) == "0")
                            {
                                lblhead.ForeColor = System.Drawing.Color.Red;
                                Page.ClientScript.RegisterStartupScript(this.Page.GetType(), Guid.NewGuid().ToString() + "4", "ChangeColor('" + lblhead.ClientID + "','1');", true);
                                lblhead.Style.Add("font-weight", "bold");
                                Newcount = Newcount - 1;
                                knowCountlist[knowcount1 - 1] = knowCountlist[knowcount1 - 1] + 1;


                            }
                            else
                            {
                                Newtotal = Newtotal + Convert.ToInt32(ds1.Tables[0].Rows[0]["Knowledge_point"]);
                            }
                            lblhead.Text = Convert.ToString(ds1.Tables[0].Rows[0]["Knowledge_point"]);


                            copyOfComIds.Remove(x);
                            KnowledgeEvList.Add(GetEmpSkillViewALL(
                              Convert.ToInt32(userId.Value),
                              Convert.ToDecimal(lblhead.Text),
                              Convert.ToInt32(x.ToString()) //ComId
                              ));
                            break;
                        }
                        else
                        {
                            KnowledgeEvList.Add(GetEmpSkillViewALL(
                             Convert.ToInt32(userId.Value),
                             Convert.ToDecimal(lblhead.Text),
                             Convert.ToInt32(x.ToString()) //ComId
                             ));

                            if (ds1.Tables[1].Rows.Count > 0)
                            {
                                if (Convert.ToString(ds1.Tables[1].Rows[0]["isActiveSkill"]) == "0")
                                {
                                    lblhead.ForeColor = System.Drawing.Color.Red;
                                    Page.ClientScript.RegisterStartupScript(this.Page.GetType(), Guid.NewGuid().ToString(), "ChangeColor('" + lblhead.ClientID + "');", true);
                                    lblhead.Style.Add("font-weight", "bold");
                                    knowCountlist[knowcount1 - 1] = knowCountlist[knowcount1 - 1] + 1;
                                    Newcount = Newcount - 1;

                                }
                            }


                            copyOfComIds.Remove(x);
                            break;
                        }
                    }
                }

            }

            try
            {
                var k = Math.Round(((Convert.ToDecimal(Newtotal) / Convert.ToDecimal(Newcount))), 1);
                lblavg.Text = Convert.ToString(k);
            }
            catch
            {

                lblavg.Text = Convert.ToString(0);
            }

            knowcount1 = 0;
        }
        if (e.Item.ItemType == ListItemType.Footer)
        {
            Repeater FooterRepeater = e.Item.FindControl("FooterRepeater") as Repeater;
            DataSet ds = (DataSet)ViewState["Competence"];

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    FooterRepeater.DataSource = ds.Tables[0];
                    FooterRepeater.DataBind();
                    int count12 = 0;
                    foreach (RepeaterItem bItem in FooterRepeater.Items)
                    {
                        var empcount = 0;
                        empcount = Empcount;
                        Label lblFavg = (Label)bItem.FindControl("lblFavg");
                        //var k = Math.Round(((Convert.ToDecimal(lblFavg.Text) / Convert.ToDecimal(Empcount))), 1);
                        //lblFavg.Text = Convert.ToString(k);
                        try
                        {
                            var k = Math.Round(((Convert.ToDecimal(lblFavg.Text) / Convert.ToDecimal((empcount - knowCountlist[count12])))), 1);
                            lblFavg.Text = Convert.ToString(k);
                        }
                        catch
                        {
                            lblFavg.Text = Convert.ToString(0);
                        }
                        count12 = count12 + 1;
                    }

                }
            }
        }
    }

    private EmployeeView GetEmpSkillViewALL(int uid, decimal compval, int comid)
    {
        var view = new EmployeeView();
        view.CompValue = compval;
        view.EmpID = uid;

        var user = UserBM.GetUserByIdSPL(Convert.ToInt32(uid)).FirstOrDefault();

        if (user != null)
        {
            view.UserFullName = user.UserFirstName + " " + user.UserLastName;
        }
        view.compId = comid;

        return view;
    }

    private EmployeeView GetEmpSkillViewK(decimal k)
    {
        var view = new EmployeeView();
        view.CompValue = k;
        return view;
    }

    private EmployeeView GetEmpSkillView(int empid)
    {
        var view = new EmployeeView();
        view.EmpID = empid;
        return view;
    }

    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        //string language = "French";
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Danish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }

    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion

    #region DropDown Event

    protected void managercat_Click(object sender, EventArgs e)
    {
        // string ComIDs = String.IsNullOrEmpty(ddtTemplateVal.Value) ? "0" : ddtTemplateVal.Value;
        //ddlCompetence("0");
        try
        {


            GetCompetenceMaster();
            GetAllEmployeeList();
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "managercat_Click");
        }
    }

    protected void ddlcompetence_SelectedIndexChanged(object sender, EventArgs e)
    {
        // GetCheckBoxListData();
        try
        {
            string ComIDs = String.IsNullOrEmpty(ddtTemplateVal.Value) ? "0" : ddtTemplateVal.Value;

            ddlCompetence("0");
            GetCompetenceMaster();
            GetAllEmployeeList();
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "ddlcompetence_SelectedIndexChanged");
        }


    }
    protected void ddljobtype_SelectedIndexChanged(object sender, EventArgs e)
    {
        //  GetCheckBoxListData();
        try
        {
            string ComIDs = String.IsNullOrEmpty(ddtTemplateVal.Value) ? "0" : ddtTemplateVal.Value;
            ddlCompetence("0");
            GetCompetenceMaster();
            GetAllEmployeeList();
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "ddljobtype_SelectedIndexChanged");
        }

    }
    protected void ddlTeam_SelectedIndexChanged(object sender, EventArgs e)
    {
        //  GetCheckBoxListData();
        try
        {
            string ComIDs = String.IsNullOrEmpty(ddtTemplateVal.Value) ? "0" : ddtTemplateVal.Value;
            ddlCompetence("0");
            GetCompetenceMaster();
            GetAllEmployeeList();
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "ddlTeam_SelectedIndexChanged");
        }

    }
    protected void GetCheckBoxListData()
    {
        CompetenceMasterBM obj = new CompetenceMasterBM();
        obj.catIsActive = true;
        obj.catIsDeleted = false;
        if (string.IsNullOrEmpty(ddlDepartment.SelectedValue)) return;
        obj.comDivId = Convert.ToInt32(ddlDepartment.SelectedValue);
        obj.comJobtype = Convert.ToInt32(ddljobtype1.SelectedValue);
        obj.GetAllCategoryByJobTypeId_DivId();
        // lblDivIdName.Text = ddlDepartment.SelectedItem.Text;

        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                //repCompCat.DataSource = ds.Tables[0];
                //chkList.DataTextField = "catName";
                //chkList.DataValueField = "catId";
                //repCompCat.DataBind();
                //chkList.SelectedIndex = 0;


                //foreach (ListItem li in chkList.Items)
                //{
                //    li.Selected = true;
                //}
            }
            else
            {
                //chkList.Items.Clear();
                //ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownCategory, CommonModule.dropDownZeroValue));
            }

        }
        else
        {
            //chkList.Items.Clear();
        }
    }
    protected void GetAllJobtype()
    {
        try
        {


            JobTypeBM obj = new JobTypeBM();
            obj.jobIsActive = true;
            obj.jobIsDeleted = false;
            obj.jobCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            obj.GetAllJobType();
            DataSet ds = obj.ds;

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddljobtype1.Items.Clear();

                    ddljobtype1.DataSource = ds.Tables[0];
                    ddljobtype1.DataTextField = "jobName";
                    ddljobtype1.DataValueField = "jobId";
                    ddljobtype1.DataBind();
                    string chkListJobtype = GetLocalResourceObject("chkListJobtype.Text").ToString();
                    ddljobtype1.Items.Insert(0, new ListItem(chkListJobtype, CommonModule.dropDownZeroValue));
                }
                else
                {
                    ddljobtype1.Items.Clear();
                    ddljobtype1.Items.Insert(0, new ListItem(GetLocalResourceObject("chkListJobtype.Text").ToString(), CommonModule.dropDownZeroValue));
                }
            }
            else
            {
                ddljobtype1.Items.Clear();
                ddljobtype1.Items.Insert(0, new ListItem(GetLocalResourceObject("chkListJobtype.Text").ToString(), CommonModule.dropDownZeroValue));
            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "GetAllJobtype");
        }

    }

    protected void GetAllTeam()
    {
        JobTypeBM obj = new JobTypeBM();
        obj.TeamIDs = "0";
        obj.jobIsActive = true;
        obj.jobIsDeleted = false;
        obj.jobCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllTeam();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (Convert.ToString(Session["Culture"]) == "Danish")
                {

                    ddlTeam.Items.Clear();

                    ddlTeam.DataSource = ds.Tables[0];
                    ddlTeam.DataTextField = "TeamNameDN";
                    ddlTeam.DataValueField = "TeamID";
                    ddlTeam.DataBind();

                    ddlTeam.Items.Insert(0, new ListItem(GetLocalResourceObject("SelectTeam.Text").ToString(), CommonModule.dropDownZeroValue));
                    //ddlTeam.Items.Insert(0, new ListItem(hdnSelectTeam.Value, CommonModule.dropDownZeroValue));
                }

                else
                {
                    ddlTeam.Items.Clear();

                    ddlTeam.DataSource = ds.Tables[0];
                    ddlTeam.DataTextField = "TeamName";
                    ddlTeam.DataValueField = "TeamID";
                    ddlTeam.DataBind();

                    ddlTeam.Items.Insert(0, new ListItem(GetLocalResourceObject("SelectTeam.Text").ToString(), CommonModule.dropDownZeroValue));

                }
            }
            else
            {
                ddlTeam.Items.Clear();
                ddlTeam.Items.Insert(0, new ListItem(GetLocalResourceObject("SelectTeam.Text").ToString(), CommonModule.dropDownZeroValue));
            }
        }
        else
        {
            ddlTeam.Items.Clear();
            ddlTeam.Items.Insert(0, new ListItem(GetLocalResourceObject("SelectTeam.Text").ToString(), CommonModule.dropDownZeroValue));
        }

    }
    #endregion
    protected void GetAllDepartments()
    {
        //DepartmentsBM obj = new DepartmentsBM();
        //obj.depIsActive = true;
        //obj.depIsDeleted = false;
        //obj.depCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        //obj.GetAllDepartments();
        //DataSet ds = obj.ds;
        try
        {


            DivisionBM obj = new DivisionBM();
            obj.depIsActive = true;
            obj.depIsDeleted = false;
            obj.depCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            obj.GetAllDivision();
            DataSet ds = obj.ds;

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {



                    //

                    ddlDepartment.Items.Clear();

                    ddlDepartment.DataSource = ds.Tables[0];
                    ddlDepartment.DataTextField = "divName";
                    ddlDepartment.DataValueField = "divId";
                    ddlDepartment.DataBind();

                    ddlDepartment.Items.Insert(0, new ListItem(GetLocalResourceObject("SelectDivision.Text").ToString(), CommonModule.dropDownZeroValue));
                }
                else
                {
                    ddlDepartment.Items.Clear();
                    ddlDepartment.Items.Insert(0, new ListItem(GetLocalResourceObject("SelectDivision.Text").ToString(), CommonModule.dropDownZeroValue));
                }
            }
            else
            {
                ddlDepartment.Items.Clear();
                ddlDepartment.Items.Insert(0, new ListItem(GetLocalResourceObject("SelectDivision.Text").ToString(), CommonModule.dropDownZeroValue));
            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "GetAllDepartments");
        }
    }

    //protected void btnPDF_Click(object sender, EventArgs e)
    //{
    //    Response.ContentType = "application/pdf";
    //    Response.AddHeader("content-disposition", "attachment;filename=TestPage.pdf");
    //    Response.Cache.SetCacheability(HttpCacheability.NoCache);
    //    StringWriter sw = new StringWriter();
    //    HtmlTextWriter hw = new HtmlTextWriter(sw);
    //    this.Page.RenderControl(hw);
    //    StringReader sr = new StringReader(sw.ToString());
    //    Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
    //    HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
    //    PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
    //    pdfDoc.Open();
    //    htmlparser.Parse(sr);
    //    pdfDoc.Close();
    //    Response.Write(pdfDoc);
    //    Response.End();
    //}

    #region PDF
    public override void VerifyRenderingInServerForm(Control control)
    { // // Confirms that an HtmlForm control is rendered for the // // specified ASP.NET server control at run time. // // No code required here. //
    }
    protected void btnPDF_Click(object sender, EventArgs e)
    {
        /*  HtmlToPdf("images\\", ".pdf", new[] {""});
          return;*/

        Button btn = (Button)sender;
        var head = string.Empty;
        var repeatername = "";
        switch (btn.CommandName)
        {
            case "Original":
                head = Originalcompetencelevel.Text;
                repeatername = "rowRepeater";
                break;
            case "New":
                head = Newcompetencelevel.Text;
                break;
            case "Developmentpoints":
                head = Developmentpoints.Text;
                break;
            case "Knowledgeshare":
                head = Knowledgeshare.Text;
                break;
        }

        Response.ContentType = "application/pdf";
        Response.AddHeader("content-disposition", "attachment;filename=Report.pdf");
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        StringWriter sw = new StringWriter();
        sw.Write("<div style='margin-bottom:10px;'><h1 style='margin:10px;font-size:15px'>" + head + "</h1></div><br/>");
        HtmlTextWriter hw = new HtmlTextWriter(sw);

        //this.Page.RenderControl(hw); 

        switch (btn.CommandName)
        {
            case "Original":
                this.rowRepeater.RenderControl(hw);
                break;
            case "New":
                this.rptnew.RenderControl(hw);
                break;
            case "Developmentpoints":
                this.rptDevelopment.RenderControl(hw);
                break;
            case "Knowledgeshare":
                this.rpt_Knowledge.RenderControl(hw);
                break;
        }

        StringReader sr = new StringReader(sw.ToString().Replace("\r", "").Replace("\n", "").Replace("  ", "").Replace("<img src='../images/wait.gif'/>", ""));

        iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.LEGAL);
        // pdfDoc.o

        pdfDoc.HtmlStyleClass = "body{color:red}";
        HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
        htmlparser.Style = GenerateStyleSheet();
        PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
        pdfDoc.Open();
        htmlparser.Parse(sr);
        pdfDoc.Close();
        Response.Write(pdfDoc);
        Response.End();
        // HtmlToPdf.ConvertUrl(urlOrHtmlFile, outputFileName);
    }

    private static StyleSheet GenerateStyleSheet()
    {
        /* FontFactory.Register(@"c:\windows\fonts\gara.ttf", "Garamond");
         FontFactory.Register(@"c:\windows\fonts\garabd.ttf");
         FontFactory.Register(@"c:\windows\fonts\garait.ttf");*/

        StyleSheet css = new StyleSheet();

        css.LoadTagStyle("body", "face", "Garamond");
        css.LoadTagStyle("body", "encoding", "Identity-H");
        css.LoadTagStyle("body", "size", "13pt");
        css.LoadTagStyle("h1", "size", "30pt");

        css.LoadTagStyle("h1", "style", "line-height:30pt;font-weight:bold;margin-bottom:50px;");
        css.LoadTagStyle("h2", "size", "22pt");
        css.LoadTagStyle("h2", "style", "line-height:30pt;font-weight:bold;margin-top:5pt;margin-bottom:12pt;");
        css.LoadTagStyle("h3", "size", "15pt");
        css.LoadTagStyle("h3", "style", "line-height:25pt;font-weight:bold;margin-top:1pt;margin-bottom:15pt;");
        css.LoadTagStyle("h4", "size", "13pt");
        css.LoadTagStyle("h4", "style", "line-height:23pt;margin-top:1pt;margin-bottom:15pt;");
        css.LoadTagStyle("hr", "width", "100%");
        css.LoadTagStyle("a", "style", "text-decoration:underline;");
        css.LoadTagStyle(HtmlTags.HEADERCELL, HtmlTags.BORDERWIDTH, "0.5");
        css.LoadTagStyle(HtmlTags.HEADERCELL, HtmlTags.BORDERCOLOR, "#333");
        css.LoadTagStyle(HtmlTags.HEADERCELL, HtmlTags.BACKGROUNDCOLOR, "#cccccc");
        css.LoadTagStyle(HtmlTags.CELL, HtmlTags.BACKGROUNDCOLOR, "#EFEFEF");
        css.LoadTagStyle(HtmlTags.CELL, HtmlTags.BORDERWIDTH, "0.5");
        css.LoadTagStyle(HtmlTags.CELL, HtmlTags.BORDERCOLOR, "#333");

        return css;
    }

    //private static void pdf(DataSet ds)
    //{
    //    string strmail = System.IO.File.ReadAllText(ConfigurationManager.AppSettings["Htmlpath"].ToString() + "template/Report.html");
    //    ConfigurationManager.AppSettings["pdfkey"].ToString();

    //    HttpContext.Current.Response.ContentType = "application/pdf";
    //    HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=Report.pdf");

    //    HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
    //    StringReader sr = new StringReader(strmail.ToString().Replace("\r", "").Replace("\n", "").Replace("  ", "").Replace("<img src='../images/wait.gif'/>", ""));

    //    iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.LEGAL);


    //    HTMLWorker htmlparser = new HTMLWorker(pdfDoc);

    //    PdfWriter.GetInstance(pdfDoc, HttpContext.Current.Response.OutputStream);
    //    pdfDoc.Open();
    //    htmlparser.Parse(sr);
    //    pdfDoc.Close();
    //    HttpContext.Current.Response.Write(pdfDoc);
    //    HttpContext.Current.Response.End();

    //}


    protected String pdfFunction(DataSet ds, String pdp, String apl, String conclusion)
    {
        WriteLog("REP1 String pdfFunction() ");
        Session.Timeout = 20000;
        string strmail = "";
        try
        {
            var path = ConfigurationManager.AppSettings["Htmlpath"].ToString() + "template/Report.html";


            // WriteLog("REP2 var path : " + path);

            //var path = @"F:\Dharmesh\2-05-2015 work\starteku\starteku\Log\template\Report.html";

            //var test = @"H:\Saurin\kmlesh\starteku\starteku\starteku\Log\template\Report.html";

            //WriteLog("REP3 var test : " + test);

            //var test = @"F:\Dharmesh\2-05-2015 work\starteku\starteku\Log\template\Report.html";

            //strmail = System.IO.File.ReadAllText(test);

            strmail = System.IO.File.ReadAllText(path);

            //WriteLog("REP4 strmail : " + strmail);

            if (Session["hdnPdpChart"] == null)
            {
                Response.Redirect("Login.aspx");

            }


            if (Convert.ToString(Session["Culture"]) == "Danish")
            {
                ds.Tables[0].Columns["divName"].ColumnName = "abcd1";
                ds.Tables[0].Columns["divNameDN"].ColumnName = "divName";

                ds.Tables[0].Columns["jobName"].ColumnName = "abcd2";
                ds.Tables[0].Columns["jobNameDN"].ColumnName = "jobName";

            }

            ConfigurationManager.AppSettings["pdfkey"].ToString();

            #region General
            strmail = strmail.Replace("##logo##", "<img src=\"" + ConfigurationManager.AppSettings["Htmlpath"].ToString() + "/upload/Userimage/LoginLogo.png" + "\"style=\"width:237px;height:62px;\"/>");

            strmail = strmail.Replace("###CompCatName###", Convert.ToString(Session["hdnCompCatNaME"]));
            strmail = strmail.Replace("###ApproveBy###", Convert.ToString(Session["hdnreportApproveBy"]));
            strmail = strmail.Replace("##reportGraphdesc##", Convert.ToString(Session["hdnReportGraph"]));
            if (!string.IsNullOrEmpty(conclusion))
            {
                strmail = strmail.Replace("###Conclusion###", conclusion);
            }
            else
            {
                strmail = strmail.Replace("###Conclusion###", " ");
            }

            if (pdp != "")
            {
                //strmail = strmail.Replace("##pdphead##", "Personal Development Points Chart");
                strmail = strmail.Replace("##pdphead##", Convert.ToString(Session["hdnPdpChart"]));
                strmail = strmail.Replace("##imgchartpdp##", "<img src=\"" + pdp + "\"style=\"width:950px;height:270px;\"/>");
            }
            else
            {
                strmail = strmail.Replace("##imgchartpdp##", "");
                strmail = strmail.Replace("##pdphead##", "");
            }
            if (apl != "")
            {
                //strmail = strmail.Replace("##ALPhead##", "Active learningspoints And Passive learning points Chart");
                strmail = strmail.Replace("##ALPhead##", Convert.ToString(Session["hdnAlpPlpChart"]));
                strmail = strmail.Replace("##imgchartALP##", "<img src=\"" + apl + "\"style=\"width:950px;height:270px;\"/>");
            }
            else
            {
                strmail = strmail.Replace("##ALPhead##", "");
                strmail = strmail.Replace("##imgchartALP##", "");
            }
            if (ds.Tables[0].Rows.Count > 0)
            {

                String PdfReportDetail = CommonModule.getTemplatebyname("PdfReport", 0);

                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["name"])))
                {
                    PdfReportDetail = PdfReportDetail.Replace(" ###name###", Convert.ToString(ds.Tables[0].Rows[0]["name"]));
                }
                else
                {
                    PdfReportDetail = PdfReportDetail.Replace(" ###name###", "this employee");
                }

                String PdfReportPurpose = CommonModule.getTemplatebyname("Report PDP Purpose", 0);



                if (!string.IsNullOrEmpty(PdfReportDetail))
                {
                    strmail = strmail.Replace("###OwnerReport###", PdfReportDetail);
                }
                else
                {
                    strmail = strmail.Replace("###OwnerReport###", " ");
                }

                if (!string.IsNullOrEmpty(PdfReportPurpose))
                {
                    strmail = strmail.Replace("###ReportPurpose###", PdfReportPurpose);
                }
                else
                {
                    strmail = strmail.Replace("###ReportPurpose###", " ");
                }

                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userImage"])))
                {
                    strmail = strmail.Replace("##image##", "<img src=\"" + ConfigurationManager.AppSettings["Htmlpath"].ToString() + "upload/Userimage/" + Convert.ToString(ds.Tables[0].Rows[0]["userImage"]) + "\"style=\"width:79px;height:79px;\"/>");
                    strmail = strmail.Replace("##image1##", "<img src=\"" + ConfigurationManager.AppSettings["Htmlpath"].ToString() + "upload/Userimage/" + Convert.ToString(ds.Tables[0].Rows[0]["userImage"]) + "\"style=\"width:135px;height:135px;\"/>");

                }
                else
                {
                    strmail = strmail.Replace("#image", "<img src=\"" + ConfigurationManager.AppSettings["Htmlpath"].ToString() + "html/Image/logo.png" + "\"style=\"width:79px;height:79px;\"/>");
                    strmail = strmail.Replace("#image1", "<img src=\"" + ConfigurationManager.AppSettings["Htmlpath"].ToString() + "html/Image/person1.png" + "\"style=\"width:79px;height:79px;\"/>");
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["name"])))
                {
                    strmail = strmail.Replace("##Name##", Convert.ToString(ds.Tables[0].Rows[0]["name"]));
                }
                else
                {
                    strmail = strmail.Replace("##Name##", "");
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["jobName"])))
                {
                    strmail = strmail.Replace("##Jobtitle##", Convert.ToString(ds.Tables[0].Rows[0]["jobName"]));
                }
                else
                {
                    strmail = strmail.Replace("##Jobtitle##", "");
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["divName"])))
                {
                    strmail = strmail.Replace("##Division##", Convert.ToString(ds.Tables[0].Rows[0]["divName"]));
                }
                else
                {
                    strmail = strmail.Replace("##Division##", "");
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["comname"])))
                {
                    strmail = strmail.Replace("##Companyname##", Convert.ToString(ds.Tables[0].Rows[0]["comname"]));
                }
                else
                {
                    strmail = strmail.Replace("##Companyname##", "");
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userEmail"])))
                {
                    strmail = strmail.Replace("##Email##", Convert.ToString(ds.Tables[0].Rows[0]["userEmail"]));
                }
                else
                {
                    strmail = strmail.Replace("##Email##", "");
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["usercontact"])))
                {
                    strmail = strmail.Replace("##Phonenumber##", Convert.ToString(ds.Tables[0].Rows[0]["usercontact"]));
                }
                else
                {
                    strmail = strmail.Replace("##Phonenumber##", "");
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["Apl"])))
                {
                    strmail = strmail.Replace("##ALP##", Convert.ToString(ds.Tables[0].Rows[0]["Apl"]));
                }
                else
                {
                    strmail = strmail.Replace("##ALP##", "");
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["Ppl"])))
                {
                    strmail = strmail.Replace("##PLP##", Convert.ToString(ds.Tables[0].Rows[0]["Ppl"]));
                }
                else
                {
                    strmail = strmail.Replace("##PLP##", "");
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["allpoint"])))
                {
                    strmail = strmail.Replace("##Total##", Convert.ToString(ds.Tables[0].Rows[0]["allpoint"]));
                }
                else
                {
                    strmail = strmail.Replace("##Total##", "");
                }

                if (ds.Tables[1].Rows.Count > 0)
                {

                    if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[1].Rows[0]["PDPTotal"])))
                    {
                        strmail = strmail.Replace("##PDP##", Convert.ToString(ds.Tables[1].Rows[0]["PDPTotal"]));
                    }
                    else
                    {
                        strmail = strmail.Replace("##PDP##", "0");
                    }
                }

                else
                {
                    strmail = strmail.Replace("##PDP##", "0");
                }
                //strmail = strmail.Replace("##Date##", Convert.ToString(DateTime.ParseExact(Convert.ToString(DateTime.Now), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None)));
                strmail = strmail.Replace("##Date##", DateTime.Now.ToString("dd/MM/yy"));
                strmail = strmail.Replace("##Managername##", Convert.ToString(Session["OrgUserName"]));

            }

            #endregion

            #region Competence
            String Competence = "";
            if (ds.Tables[1].Rows.Count > 0)
            {
                if (Convert.ToString(Session["Culture"]) == "Danish")
                {
                    ds.Tables[1].Columns["comCompetence"].ColumnName = "abcd";
                    ds.Tables[1].Columns["comCompetenceDN"].ColumnName = "comCompetence";

                }

                Competence = "<table width='100%' border='0' style='font-size:22px;'>";
                Competence = Competence + "<tr style='font-size:22px;font-weight:bold;font-family:Verdana, Arial, Helvetica, sans-serif; color: #fcfcfc; background: #038ff5;'> <th>&nbsp;</th><th>" + Convert.ToString(Session["hdnBasic"]) + "</th><th>" + Convert.ToString(Session["hdnActual"]) + "</th><th>" + Convert.ToString(Session["hdnTarget"]) + "</th></tr>";
                for (int i = 0; i <= ds.Tables[1].Rows.Count - 1; i++)
                {
                    //Competence = Competence + "<tr> <td>&nbsp;</td><td>"+ Convert.ToString(Session["hdnBasic"])+"</td><td>"+Convert.ToString(Session["hdnActual"])+"</td><td>"+Convert.ToString(Session["hdnTarget"])+"</td></tr>";
                    Competence = Competence + "<tr style='font-size:16px;font-weight:bold;font-family:Verdana, Arial, Helvetica, sans-serif; color: black; background-color: white;'><td style=''>" + Convert.ToString(ds.Tables[1].Rows[i]["comCompetence"]) + "</td><td style='text-align:center;'>" + Convert.ToString(ds.Tables[1].Rows[i]["skillLocal"]) + "</td>"
                                            + "</td><td style='text-align:center;'>" + Convert.ToString(ds.Tables[1].Rows[i]["skillAchive"])
                                          + "</td><td style='text-align:center;'>" + Convert.ToString(ds.Tables[1].Rows[i]["skilltarget"]) + "</td></tr>";
                }
            }
            strmail = strmail.Replace("##Competence##", Competence);
            #endregion


            #region Documents shared by (name)
            String Documents = "";
            if (ds.Tables[2].Rows.Count > 0)
            {
                // Documents = "";##Documentshared##
                Documents = "<table width='100%' border='0' style='font-size:16px;margin-bottom:50px;'>";
                Documents = Documents + "<tr style='font-size:16px;font-weight:bold;font-family:Verdana, Arial, Helvetica, sans-serif; color:#4f81bd;'><td width='220' height='35'>" + Convert.ToString(Session["hdndocType"]) + "</td><td>" + Convert.ToString(Session["hdndocName"]) + "</td><td  width='252'>" + Convert.ToString(Session["hdnDocumentShared"]) + "</td><td width='100'>" + Convert.ToString(Session["hdndocDate"]) + "</td></tr>";
                for (int i = 0; i <= ds.Tables[2].Rows.Count - 1; i++)
                {
                    Documents = Documents + "<tr><td height='27'>Learning doc</td>" + "<td>" + Convert.ToString(ds.Tables[2].Rows[i]["name"])
                                            + "</td><td>" + Convert.ToString(ds.Tables[2].Rows[i]["docFileName_Friendly"]) + "</td>"
                                             + "<td>" + Convert.ToString(ds.Tables[2].Rows[i]["docCreatedDate"])
                                            + "</td></tr>";
                }
            }
            strmail = strmail.Replace("##Document##", Documents);
            if (!String.IsNullOrEmpty(Documents))
            {

                strmail = strmail.Replace("##Documentshared##", Convert.ToString(Session["hdnDocumentShared"]));
                //strmail = strmail.Replace("##Documentshared##", "Document shared");
            }
            else { strmail = strmail.Replace("##Documentshared##", ""); }
            #endregion
        }
        catch (DataException ex)
        {
            WriteLog("REP5 strmail : " + "error pdfFunction" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));

            Common.WriteLog("error pdfFunction" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;

        }
        return strmail;
    }

    private String pdf(DataSet ds, String pdp, String apl, String conclusion)
    {
        try
        {
            String Content = pdfFunction(ds, pdp, apl, conclusion);
            string[] CntArr = Content.Split('`');
            // ConfigurationManager.AppSettings["Htmlpath"].ToString() + "template/Report.html"
            LicensingManager.LicenseKey = ConfigurationManager.AppSettings["pdfkey"].ToString();
            Winnovative.PdfCreator.Document document = new Winnovative.PdfCreator.Document();

            Winnovative.PdfCreator.PdfPage page = document.Pages.AddNewPage(Winnovative.PdfCreator.PageSize.A3, new Margins(3), PageOrientation.Portrait);
            HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(10, 10, 830, CntArr[0], null);
            htmlToPdfElement.ActiveXEnabledInImage = true;
            htmlToPdfElement.AvoidImageBreak = true;
            htmlToPdfElement.AvoidTextBreak = true;
            htmlToPdfElement.FitWidth = true;
            htmlToPdfElement.StretchToFit = true;
            ExplicitDestination pageDestination = new ExplicitDestination(page);
            pageDestination.ZoomPercentage = 100;
            pageDestination.DestPage = page;
            AddElementResult addResult;
            addResult = page.AddElement(htmlToPdfElement);
            page.Orientation = PageOrientation.Portrait;

            if (CntArr.Length > 1)
            {
                Winnovative.PdfCreator.PdfPage page1 = document.Pages.AddNewPage(Winnovative.PdfCreator.PageSize.A3, new Margins(3), PageOrientation.Portrait);
                HtmlToPdfElement htmlToPdfElement1 = new HtmlToPdfElement(10, 10, 830, CntArr[1], null);
                htmlToPdfElement1.ActiveXEnabledInImage = true;
                htmlToPdfElement1.AvoidImageBreak = true;
                htmlToPdfElement1.AvoidTextBreak = true;
                htmlToPdfElement1.FitWidth = true;
                htmlToPdfElement1.StretchToFit = true;
                ExplicitDestination pageDestination1 = new ExplicitDestination(page1);
                pageDestination.ZoomPercentage = 100;
                pageDestination.DestPage = page1;
                AddElementResult addResult1;
                addResult1 = page1.AddElement(htmlToPdfElement1);
                page1.Orientation = PageOrientation.Portrait;
            }

            document.CompressionLevel = CompressionLevel.BestCompression;
            // send the generated PDF document to client browser
            string PdfName = DateTime.Now.ToString("ddMMyyyyhhmmmss");

            string path = ConfigurationManager.AppSettings["Htmlpath"].ToString() + "html/" + PdfName + ".pdf";

            WriteLog("REP6 document.Save(path); path : " + path);

            //string path = @"F:\Dharmesh\2-05-2015 work\starteku\starteku\Log\template\Report.html";

            Bookmark bm = document.AddBookmark("Pdf Page", pageDestination);
            //if (!System.IO.File.Exists(path))
            document.Save(path);
            Session["pdfPathForPrint"] = PdfName + ".pdf";
            string pdfDownloadURL = ConfigurationManager.AppSettings["siteurl"].ToString() + "Log/html/" + PdfName + ".pdf";

            WriteLog("REP7 pdfDownloadURL : " + pdfDownloadURL);

            return pdfDownloadURL;
            //HttpContext.Current.Response.Write("<script>");
            //HttpContext.Current.Response.Write("window.open('" + pdfDownloadURL + "','_blank')");
            //HttpContext.Current.Response.Write("</script>");
        }
        catch (DataException ex)
        {
            Common.WriteLog("error pdf" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }

    public static void WriteLog(string functionName)
    {
        string writelogtest = Convert.ToString(ConfigurationSettings.AppSettings["writelogstatus"]).ToLower();
        //string writelogtest = "true";
        if (!string.IsNullOrEmpty(writelogtest))
        {
            if (writelogtest == "true")
            {
                string fileName = DateTime.Today.ToString("dd_MMM_yyyy");
                string path = ConfigurationSettings.AppSettings["logpath"].ToString() + fileName + ".txt";
                //string path = "c:/inetpub/wwwroot/starteku/Log/" + fileName + ".txt";
                //
                if (!System.IO.File.Exists(path))
                {
                    using (FileStream fs = File.Create(path))
                    {
                        Byte[] info = new UTF8Encoding(true).GetBytes("");
                        fs.Write(info, 0, 0);
                    }
                }
                TextWriter tw1 = new StreamWriter(path, true);
                tw1.WriteLine(functionName);
                tw1.Close();
                tw1.Dispose();
            }
        }
    }

    #endregion

    #region WebMethod
    [WebMethod(EnableSession = true)]
    public static string Getdata(String id, String pdp, String apl, String conclusion)
    {
        string msg = string.Empty;
        msg = "";

        try
        {
            Int32 rtpUserId = Convert.ToInt32(id);
            Int32 rptCreatedUSerId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
            Int32 rptcompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);

            startetkuEntities1 db = new startetkuEntities1();
            Report report = new Report();
            var reportdata = db.Reports.FirstOrDefault(x => x.rptUserId == rtpUserId && x.rptCreatedUserId == rptCreatedUSerId && x.rptCompanyId == rptcompanyId && x.rptIsActive == true);
            if (reportdata != null)
            {
                reportdata.rptConclusion = conclusion;
                db.SaveChanges();
            }
            else
            {
                report.rptCompanyId = rptcompanyId;
                report.rptConclusion = conclusion;
                report.rptCreateDate = DateTime.Now;
                report.rptCreatedUserId = rptCreatedUSerId;
                report.rptIsActive = true;
                report.rptUserId = rtpUserId;
                db.Reports.Add(report);
                db.SaveChanges();
            }

        }
        catch
        {

        }

        //GetEmployeeDetailbyPdf
        UserBM objAtt = new UserBM();
        objAtt.userIsActive = true;
        objAtt.userIsDeleted = false;
        objAtt.userId = Convert.ToInt32(id);
        objAtt.userCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
        objAtt.GetEmployeeDetailbyPdf();
        DataSet ds = objAtt.ds;
        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                Organisation_Report x = new Organisation_Report();
                msg = x.pdf(ds, pdp, apl, conclusion);
                //msg = "df";
            }
        }

        return msg;
    }

    [WebMethod(EnableSession = true)]
    public static string GetConclusion(String id)
    {
        string msg = string.Empty;

        try
        {
            Int32 rtpUserId = Convert.ToInt32(id);
            Int32 rptCreatedUSerId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
            Int32 rptcompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);

            startetkuEntities1 db = new startetkuEntities1();
            var reportdata = db.Reports.FirstOrDefault(x => x.rptUserId == rtpUserId && x.rptCreatedUserId == rptCreatedUSerId && x.rptCompanyId == rptcompanyId && x.rptIsActive == true);
            if (reportdata != null)
            {
                msg = reportdata.rptConclusion;
            }
        }
        catch
        {

        }
        return msg;
    }
    #endregion



}