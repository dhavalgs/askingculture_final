﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using starteku_BusinessLogic.Model;

public partial class Organisation_ActivityCategory : System.Web.UI.Page     
{
    // Developed By JAINAM SHAH -----  16-3-2017 ------
    public int ResLangId = 0;
    public Organisation_ActivityCategory()
    {
       
    }
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        ResLangId = Convert.ToInt32(Session["resLangID"]);
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            //Division.InnerHtml = "Welcome   " + Convert.ToString(Session["OrgUserName"]) + "!";
            Category.InnerHtml = GetLocalResourceObject("Welcome.Text").ToString() + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            //PopulateRootLevel();
            GetActivityCategoryList();
            // DivisionArchiveAll();
            SetDefaultMessage();


            

            if (Convert.ToInt32(Session["OrguserType"]) == 2)
            {
                Response.Redirect("Manager-dashboard.aspx");

            }
            else if (Convert.ToInt32(Session["OrguserType"]) == 3)
            {
                Response.Redirect("Employee_dashboard.aspx");

            }

        }
       

    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {
                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion

    #region method
    protected void SetDefaultMessage()
    {
        // lblMsg.Text = GetLocalResourceObject("lblHelloWorld.Text").ToString();

        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["msg"])))
        {
            if (Convert.ToString(Request.QueryString["msg"]) == "del")
            {
                //lblMsg.Text = CommonModule.msgRecordHasBeenDeletedSuccessfully;
                //lblMsg.Text = CommonMessages.msgRecordHasBeenDeletedSuccessfully;
                lblMsg.Text = GetLocalResourceObject("msgRecordHasBeenDeletedSuccessfully.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "ins")
            {
                //lblMsg.Text = CommonMessages.msgRecordInsertedSuccessfully;
                lblMsg.Text = GetLocalResourceObject("msgRecordInsertedSuccessfully.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "upd")
            {
                //lblMsg.Text = CommonMessages.msgRecordUpdatedSuccss;
                lblMsg.Text = GetLocalResourceObject("msgRecordUpdatedSuccss.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "arc")
            {
                // lblMsg.Text = CommonMessages.msgRecordArchiveSucces;
                lblMsg.Text = GetLocalResourceObject("CommonMessages.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
        }

    }

    protected void GetActivityCategoryList()
    {
        try
        {
            var actCatList = ActivityCategoryLogic.GetActivityCategoryList(ResLangId,Convert.ToInt32(Session["OrgUserId"]));

            if (actCatList != null)
            {

                gvGrid.DataSource = actCatList;

                gvGrid.DataBind();
            }
            else
            {
                gvGrid.DataSource = null;

                gvGrid.DataBind();
                    
            }
        }
        catch (Exception)
        {
                
            throw;
        }
       


    }

    protected void btnsubmit_click(object sender, EventArgs e)
    {
        var db=new startetkuEntities1();
        int actid;
        int companyid;
        var isDup = db.ActivityCategoryMasters.FirstOrDefault(o => o.ActCatName == txtActName.Text);
        if (isDup != null)
        {
            LabelAlready.Visible = true;
            LabelAdded.Visible = false;
            Labeldeleted.Visible = false;
            LabelUpdated.Visible = false;
            actid=isDup.ActCatId;
            companyid =Convert.ToInt32( isDup.ActCatCreatedBy);
            //Label2.Text = "Opps The Activity is Already Available.";
        }
        else
        {
            var actCat = new ActivityCategoryMaster();
            actCat.ActCatName = txtActName.Text;
            actCat.ActCatReqEnabled = rdoActCatReqEnabled.SelectedItem.Value == "1" ? true : false;
            actCat.ActCompEnabled = rdoActCompEnabled.SelectedItem.Value == "1" ? true : false;
            //----------------12-4-2017--------------------------------------
            actCat.ActCatPublic = rdoActCatPublic.SelectedItem.Value == "1" ? true : false;
            actCat.ActCatDeveplan = rdoActCatDevelopmentplan.SelectedItem.Value == "1" ? true : false;
            //----------------------------------------------------------------
            actCat.ActCatCreatedBy = Convert.ToInt32(Session["OrgUserId"]);
            actCat.ActCatsActive = true;
            
            actCat.ActCatCreatedDate = CommonUtilities.GetCurrentDateTime();
            actCat.ActCatUpdateDate = CommonUtilities.GetCurrentDateTime();
            //

            db.ActivityCategoryMasters.Add(actCat);
            db.SaveChanges();
            actid=actCat.ActCatId;
            companyid =Convert.ToInt32( actCat.ActCatCreatedBy);
            GetActivityCategoryList();
            
            LabelAdded.Visible = true;
            LabelAlready.Visible = false;
            Labeldeleted.Visible = false;
            LabelUpdated.Visible = false;
            //Label2.Text = "Activity Category Added Successfully ";
        }

        Clear();

        LangTranslationMasterBM lobj = new LangTranslationMasterBM();
        string resLangID, LangText;
        for (int i = 0; i < gridTranslate.Rows.Count; i++)
        {
            resLangID= ((HiddenField)gridTranslate.Rows[i].Cells[0].FindControl("hdnResLangID")).Value;
            LangText = ((TextBox)gridTranslate.Rows[i].Cells[0].FindControl("txtTranValue")).Text;
            lobj.LangType = "actcat";
            lobj.LangActCatId = actid;
            lobj.ResLangID =Convert.ToInt32( resLangID);
            lobj.LangText = LangText;
            lobj.LangCompanyID = companyid;
            lobj.InsertLangResource();
        }
    }
    #endregion


    protected void Test_click(object sender, EventArgs e)
    {


        txtActName.Text = "";
        Session["ActID"] = 0;
        ResourceLanguageBM obj = new ResourceLanguageBM();
        DataSet ds = new DataSet();
        obj.LangCompanyID = Convert.ToInt32(Session["OrgUserId"]);
        obj.LangActCatId = Convert.ToInt32(Session["ActID"]);
        obj.LangType = "actcat";
        ds = obj.GetAllResourceLangTran();

        gridTranslate.DataSource = ds.Tables[0];
        gridTranslate.DataBind();
        ClientScript.RegisterStartupScript(this.GetType(), "ModelPopup", "$('.add_user').click(); ShowCreateLable(false);", true);

    }

    protected void btnupdate_click(object sender, EventArgs e)
    {
        var db = new startetkuEntities1();
        var id = hdnActCatId.Value;
        var idInt = Convert.ToInt32(id);
        int actid;
        int companyid;
        var actCat = db.ActivityCategoryMasters.FirstOrDefault(o => o.ActCatId == idInt);

        //check dup
        var isDup = db.ActivityCategoryMasters.FirstOrDefault(o => o.ActCatName == txtActName.Text &&  o.ActCatId != idInt );
        if (isDup != null)
        {
            LabelAlready.Visible = true;
            LabelAdded.Visible = false;
            Labeldeleted.Visible = false;
            LabelUpdated.Visible = false;
            actid = isDup.ActCatId;
            companyid = Convert.ToInt32(isDup.ActCatCreatedBy);
        }
        else
        {
            actCat.ActCatName = txtActName.Text;
            actCat.ActCatReqEnabled = rdoActCatReqEnabled.SelectedItem.Value == "1" ? true : false;
            actCat.ActCompEnabled = rdoActCompEnabled.SelectedItem.Value == "1" ? true : false;
            //----------------15-4-2017--------------------------------------
            actCat.ActCatPublic = rdoActCatPublic.SelectedItem.Value == "1" ? true : false;
            actCat.ActCatDeveplan = rdoActCatDevelopmentplan.SelectedItem.Value == "1" ? true : false;
            //----------------------------------------------------------------
            actCat.ActCatsActive = true;
            actCat.ActCatUpdateDate = CommonUtilities.GetCurrentDateTime();


            db.SaveChanges();

            LabelUpdated.Visible = true;
            LabelAlready.Visible = false;
            LabelAdded.Visible = false;
            Labeldeleted.Visible = false;
            actid = actCat.ActCatId;
            companyid = Convert.ToInt32(actCat.ActCatCreatedBy);
        }
        GetActivityCategoryList();
        Clear();


        LangTranslationMasterBM lobj = new LangTranslationMasterBM();
        string resLangID, LangText;
        for (int i = 0; i < gridTranslate.Rows.Count; i++)
        {
            resLangID = ((HiddenField)gridTranslate.Rows[i].Cells[0].FindControl("hdnResLangID")).Value;
            LangText = ((TextBox)gridTranslate.Rows[i].Cells[0].FindControl("txtTranValue")).Text;
            lobj.LangType = "actcat";
            lobj.LangActCatId = actid;
            lobj.ResLangID = Convert.ToInt32(resLangID);
            lobj.LangText = LangText;
            lobj.LangCompanyID = companyid;
            lobj.InsertLangResource();
        }

    }

    #region grid Event
    protected void gvGrid_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Delete_CAT")
        {
            string ActCatId= e.CommandArgument.ToString();

            var db = new startetkuEntities1();
            var ac = Convert.ToInt32(ActCatId);
            var isDup = db.ActivityCategoryMasters.FirstOrDefault(o => o.ActCatId == ac); 
            if (isDup != null)
            {
                //isDup.ActCatsActive = false;
                db.ActivityCategoryMasters.Remove(isDup);
                db.SaveChanges();

                GetActivityCategoryList();
                LabelUpdated.Visible = false;
                LabelAlready.Visible = false;
                LabelAdded.Visible = false;
                Labeldeleted.Visible = true;
            }
        }

        else if (e.CommandName == "Edit_CAT")
        {
            string ActCatId = e.CommandArgument.ToString();
            var db = new startetkuEntities1();
            var ac = Convert.ToInt32(ActCatId);
            var getActCatData = db.ActivityCategoryMasters.FirstOrDefault(o => o.ActCatId == ac);
           
            hdnActCatId.Value = getActCatData.ActCatId.ToString();
            txtActName.Text = getActCatData.ActCatName;
            string ACR = getActCatData.ActCatReqEnabled.ToString();
            if (ACR == "True")
            { rdoActCatReqEnabled.SelectedValue = "1" ; }
            else
            { rdoActCatReqEnabled.SelectedValue= "2"; }

            string ACR1 = getActCatData.ActCompEnabled.ToString();
            if (ACR1 == "True")
            { rdoActCompEnabled.SelectedValue = "1"; }
            else
            { rdoActCompEnabled.SelectedValue = "2"; }


            rdoActCatPublic.SelectedValue = getActCatData.ActCatPublic == true ? "1" :"2";

            rdoActCatDevelopmentplan.SelectedValue = getActCatData.ActCatDeveplan == true ? "1" : "2";

            //model open code
            
            //submit text : update
            btnsubmit.Visible = true;
            //lblCreate.Visible = false;
            //LblUpdate.Visible = true;
            Buttonupdate.Visible = true;


            ClientScript.RegisterStartupScript(this.GetType(), "ModelPopup", "$('.add_user').click();ShowEditLable(); ", true);
            Session["ActID"] = Convert.ToInt32(ActCatId);

            ResourceLanguageBM obj = new ResourceLanguageBM();
            DataSet ds = new DataSet();
            obj.LangCompanyID = Convert.ToInt32(Session["OrgUserId"]);
            obj.LangActCatId = Convert.ToInt32(Session["ActID"]);
            obj.LangType = "actcat";
            ds = obj.GetAllResourceLangTran();

            gridTranslate.DataSource = ds.Tables[0];
            gridTranslate.DataBind();
            //LangTranslationMasterBM lobj = new LangTranslationMasterBM();
            //string resLangID, LangText;
            //for (int i = 0; i < gridTranslate.Rows.Count; i++)
            //{
            //    resLangID = ((HiddenField)gridTranslate.Rows[i].Cells[0].FindControl("hdnResLangID")).Value;
            //    LangText = ((TextBox)gridTranslate.Rows[i].Cells[0].FindControl("txtTranValue")).Text;
            //    lobj.LangType = "actcat";
            //    lobj.LangActCatId = Convert.ToInt32(ActCatId);
            //    lobj.ResLangID = Convert.ToInt32(resLangID);
            //    lobj.LangText = LangText;
            //    lobj.LangCompanyID =Convert.ToInt32( getActCatData.ActCatCreatedBy);
            //    lobj.InsertLangResource();
            //}
            
        }
    }
    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        // string language = "French";
        string language = Convert.ToString(Session["Culture"]);

        string languageId = "";

        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }

        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion

    protected void Clear()
    {
        
            txtActName.Text = string.Empty;
            btnsubmit.Visible = true;
            Buttonupdate.Visible = false;
            
    }
   
}

