﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Threading;
using System.Globalization;
using startetku.Business.Logic;

public partial class Organisation_OrgJobTypeList : System.Web.UI.Page
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            Session["OriginPage"] = "OrgJobTypeList.aspx";
            JobType.InnerHtml = GetLocalResourceObject("Welcome.Text").ToString() + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            jobtypeSelectAll();
            GetAllcompetenceCategory();
            // jobtypeSelectArchiveAll();
            SetDefaultMessage();
        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion

    #region method
    protected void txtJobTypeName_TextChanged(object sender, EventArgs e)
    {
        JobTypeBM obj2 = new JobTypeBM();
        obj2.JobTypeCheckDuplication(txtJobTypeName.Text, -1, Convert.ToInt32(Session["OrgCompanyId"]));
        String returnMsg = obj2.ReturnString;
        if (returnMsg == "")
        {
        }
        else
        {
            if (returnMsg == "jobName")
                lblJobTypeName.Text = CommonModule.msgNameAlreadyExists;

        }
    }
    protected void SetDefaultMessage()
    {
        // lblMsg.Text = GetLocalResourceObject("lblHelloWorld.Text").ToString();

        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["msg"])))
        {
            if (Convert.ToString(Request.QueryString["msg"]) == "del")
            {
                //lblMsg.Text = CommonModule.msgRecordHasBeenDeletedSuccessfully;
                //lblMsg.Text = CommonMessages.msgRecordHasBeenDeletedSuccessfully;
                lblMsg.Text = GetLocalResourceObject("msgRecordHasBeenDeletedSuccessfully.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "ins")
            {
                //lblMsg.Text = CommonMessages.msgRecordInsertedSuccessfully;
                lblMsg.Text = GetLocalResourceObject("msgRecordInsertedSuccessfully.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "upd")
            {
                //lblMsg.Text = CommonMessages.msgRecordUpdatedSuccss;
                lblMsg.Text = GetLocalResourceObject("msgRecordUpdatedSuccss.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "arc")
            {
                // lblMsg.Text = CommonMessages.msgRecordArchiveSucces;
                lblMsg.Text = GetLocalResourceObject("CommonMessages.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
        }

    }
    protected void jobtypeSelectAll()
    {
        JobTypeBM obj = new JobTypeBM();
        obj.jobIsActive = true;
        obj.jobIsDeleted = false;
        obj.jobCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllJobType();
        DataSet ds = obj.ds;  
        if (Convert.ToString(Session["Culture"]) == "Danish") {
            ds.Tables[0].Columns["jobName"].ColumnName = "abcd";
            ds.Tables[0].Columns["jobNameDN"].ColumnName = "jobName";
            
        }
        ViewState["ds"] = ds;
        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvGrid.DataSource = ds.Tables[0];
                gvGrid.DataBind();

                gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvGrid.DataSource = null;
                gvGrid.DataBind();
            }
        }
        else
        {
            gvGrid.DataSource = null;
            gvGrid.DataBind();
        }
    }
    protected void jobtypeSelectArchiveAll()
    {
        JobTypeBM obj = new JobTypeBM();
        obj.jobIsActive = false;
        obj.jobIsDeleted = false;
        obj.jobCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllJobType();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvArchive.DataSource = ds.Tables[0];
                gvArchive.DataBind();

                gvArchive.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvArchive.DataSource = null;
                gvArchive.DataBind();
            }
        }
        else
        {
            gvArchive.DataSource = null;
            gvArchive.DataBind();
        }
    }
    //protected void GetAllDepartments()

    //{
    //    DepartmentsBM obj = new DepartmentsBM();
    //    obj.depIsActive = true;
    //    obj.depIsDeleted = false;
    //    obj.depCompanyId = 0;
    //    obj.GetAllDepartments();
    //    DataSet ds = obj.ds;

    //    if (ds != null)
    //    {
    //        if (ds.Tables[0].Rows.Count > 0)
    //        {
    //            ddlDepartment.Items.Clear();

    //            ddlDepartment.DataSource = ds.Tables[0];
    //            ddlDepartment.DataTextField = "depName";
    //            ddlDepartment.DataValueField = "depId";
    //            ddlDepartment.DataBind();

    //            ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownSelectDepartments, CommonModule.dropDownZeroValue));
    //        }
    //        else
    //        {
    //            ddlDepartment.Items.Clear();
    //            ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownSelectDepartments, CommonModule.dropDownZeroValue));
    //        }
    //    }
    //    else
    //    {
    //        ddlDepartment.Items.Clear();
    //        ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownSelectDepartments, CommonModule.dropDownZeroValue));
    //    }

    //}
    protected void GetAllcompetenceCategory()
    {
        CompetenceMasterBM obj = new CompetenceMasterBM();
        obj.catIsActive = true;
        obj.catIsDeleted = false;
        obj.catCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllcompetenceCategory();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {

                if (Convert.ToString(Session["Culture"]) == "Danish")
                {
                    ds.Tables[0].Columns["catName"].ColumnName = "abcd";
                    ds.Tables[0].Columns["catNameDN"].ColumnName = "catName";

                }


                ddlDepartment.Items.Clear();

                ddlDepartment.DataSource = ds.Tables[0];
                ddlDepartment.DataTextField = "catName";
                ddlDepartment.DataValueField = "catId";
                ddlDepartment.DataBind();

                chkList.DataSource = ds.Tables[0];
                chkList.DataTextField = "catName";
                chkList.DataValueField = "catId";
                chkList.DataBind();


                //chkList.Items.Insert(0, new ListItem(CommonModule.dropDownCategory, CommonModule.dropDownZeroValue));

                ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownCategory, CommonModule.dropDownZeroValue));
            }
            else
            {
                ddlDepartment.Items.Clear();
                ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownCategory, CommonModule.dropDownZeroValue));

                chkList.Items.Clear();
                //ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownCategory, CommonModule.dropDownZeroValue));
            }


        }
        else
        {
            chkList.Items.Clear();
            ddlDepartment.Items.Clear();
            ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownCategory, CommonModule.dropDownZeroValue));
        }

    }
    #endregion

    #region grid Event
    protected void gvGrid_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "archive")
        {
            JobTypeBM obj = new JobTypeBM();
            obj.jobId = Convert.ToInt32(e.CommandArgument);
            obj.jobIsActive = false;
            obj.jobIsDeleted = false;
            obj.JobTypeStatusUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    jobtypeSelectAll();
                    jobtypeSelectArchiveAll();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }
    }
    protected void gvArchive_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "archive")
        {
            JobTypeBM obj = new JobTypeBM();
            obj.jobId = Convert.ToInt32(e.CommandArgument);
            obj.jobIsActive = true;
            obj.jobIsDeleted = false;
            obj.JobTypeStatusUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    jobtypeSelectAll();
                    jobtypeSelectArchiveAll();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }
        else if (e.CommandName == "permanentlydelete")
        {
            JobTypeBM obj = new JobTypeBM();
            obj.jobId = Convert.ToInt32(e.CommandArgument);
            obj.jobIsActive = false;
            obj.jobIsDeleted = true;
            obj.JobTypeStatusUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    jobtypeSelectAll();
                    jobtypeSelectArchiveAll();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }
    }
    protected void gvGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        DataSet ds = (DataSet)ViewState["ds"];
        //foreach (TableCell tc in e.Row.Cells)
        //{
        //    tc.BorderStyle = BorderStyle.None;
        //}
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        HiddenField jobCompanyId = e.Row.FindControl("jobCompanyId") as HiddenField;
                        HtmlGenericControl divEdit = (HtmlGenericControl)e.Row.FindControl("divEdit");
                        HtmlGenericControl divDeletet = (HtmlGenericControl)e.Row.FindControl("divDeletet");
                        HtmlGenericControl div1 = (HtmlGenericControl)e.Row.FindControl("div1");
                        if (Convert.ToInt32(jobCompanyId.Value) == 0)
                        {
                            divEdit.Visible = false;
                            divDeletet.Visible = false;
                        }
                        else
                        {
                            div1.Visible = false;
                        }
                    }
                }
            }
        }


    }
    #endregion

    #region button event
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        JobTypeBM obj2 = new JobTypeBM();
        obj2.JobTypeCheckDuplication(txtJobTypeName.Text, -1, Convert.ToInt32(Session["OrgCompanyId"]));
        String returnMsg = obj2.ReturnString;
        if (returnMsg == "")
        {
            JobTypeBM obj = new JobTypeBM();
            obj.jobName = txtJobTypeName.Text;
            obj.jobNameDN = txtJobTypeNameDn.Text;
            obj.jobCompanyId = Convert.ToInt32(Session["OrgUserId"]);
            obj.jobIsActive = true;
            obj.jobIsDeleted = false;
            obj.jobCreatedDate = DateTime.Now;
            obj.jobDescription = txtDESCRIPTION.Text;
            obj.jobDepId = Convert.ToInt32(ddlDepartment.SelectedValue);//insert com. categoryId
            //
            string str_clk_list_QualityArea = string.Empty;
            for (int i = 0; i < chkList.Items.Count; i++)
            {
                if (chkList.Items[i].Selected)
                {
                    if (string.IsNullOrEmpty(str_clk_list_QualityArea))
                    {
                        str_clk_list_QualityArea = chkList.Items[i].Value;
                    }
                    else
                    {
                        str_clk_list_QualityArea = str_clk_list_QualityArea + "," + chkList.Items[i].Value;
                    }
                }
            }
            //
            obj.jobcatid = str_clk_list_QualityArea;
            obj.InsertJobType();
            DataSet ds = obj.ds;

            if(ds!=null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    TargetMaxLongBM obj1 = new TargetMaxLongBM();
                    obj1.CompanyID = Convert.ToInt32(Session["OrgCompanyId"]);
                    obj1.UserID = Convert.ToInt32(Session["OrgUserId"]);
                    obj1.tarType = "JobType";
                    obj1.RefID = Convert.ToInt32(ds.Tables[0].Rows[0]["JobID"]);
                    obj1.ComptenceCatIDs = obj.jobcatid;

                    obj1.InsertUpdateTargetMaxLong();
                }
            }

            if (obj.ReturnBoolean == true)
            {
                Response.Redirect("OrgJobTypeList.aspx");
            }
        }
        else
        {
            if (returnMsg == "jobName")
                lblMsg.Text = CommonModule.msgNameAlreadyExists;
            else
                lblMsg.Text = CommonModule.msgProblemInsertRecord;

        }
    }
    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Danish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion
}