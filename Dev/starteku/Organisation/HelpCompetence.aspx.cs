﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using App_code;
using starteku_BusinessLogic;
using System.Web.Services;
using startetku.Business.Logic;
using System.Configuration;
using ASP;
using starteku_BusinessLogic.Model;


public partial class Organisation_HelpCompetence : System.Web.UI.Page
{
    Int32 skillComId = 0;
    Int32 skillAchive = 0;
    startetkuEntities1 db = new startetkuEntities1();
    #region page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "my", " SetExpandCollapse();", true);

        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            Skills.InnerHtml = GetLocalResourceObject("Welcome.Text").ToString() + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            txtDate.Text = DateTime.Now.ToString("dd-MM-yyyy");
            if (!String.IsNullOrEmpty(Request.QueryString["Hid"]))
            {
                skillComId = Convert.ToInt32(Request.QueryString["Hid"]);
                skillAchive = Convert.ToInt32(Request.QueryString["Nid"]);
                 subCompetenceSelectAll(2);
                GetMessgeSkillLessthen(2);
                Headname.InnerHtml = hdnRequestHelp.Value;
            }
            else if (!String.IsNullOrEmpty(Request.QueryString["Lid"]))
            {
                skillComId = Convert.ToInt32(Request.QueryString["Lid"]);
                skillAchive = Convert.ToInt32(Request.QueryString["Gid"]);
                 subCompetenceSelectAll(1);
                GetMessgeSkillLessthen(1);
                Headname.InnerHtml = hdnRequestHelp.Value;
                //txtInviteGiveHelp.Text = Session["OrgUserName"] + " have sent you Invitation";
                // txtInviteGiveHelp.Text = "Sent you Reuest Help For Competence";
            }

          
        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion

    #region method
    protected void subCompetenceSelectAll(Int32 id)
    {
        try
        {
            EmployeeSkillBM obj = new EmployeeSkillBM();
            obj.skillIsActive = true;
            obj.skillIsDeleted = false;
            obj.skillStatus = 1;
            obj.skillCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            obj.skillComId = skillComId;
            obj.skillUserId = Convert.ToInt32(Session["OrgUserId"]);
            obj.GetHelpCompotenceLessthen(id);
            DataSet ds = obj.ds;

            ViewState["data"] = ds;

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    //Int32 id = 3;
                    //DataTable dtTop = ds.Tables[0].Rows.Cast<DataRow>().Take(id).CopyToDataTable();  //Take top 3
                    //DataTable dtBottom = ds.Tables[0].Rows.Cast<DataRow>().Skip(ds.Tables[0].Rows.Count - id).CopyToDataTable(); // Take bottom 3

                    //rptAll.DataSource = ds;
                    //rptAll.DataBind();

                    //Repeater1.DataSource = ds;
                    //Repeater1.DataBind();

                    if (!String.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["comCompetence"])))
                    {
                        ViewState["Compid"] = (Convert.ToString(ds.Tables[0].Rows[0]["comId"]));
                        if (Convert.ToString(Session["Culture"]) != "Danish")
                        {
                            
                                 Competencetitle.InnerHtml = (Convert.ToString(ds.Tables[0].Rows[0]["comCompetence"]));   
                           
                           
                            
                           // if()
                        }
                        else
                        {
                            Competencetitle.InnerHtml = (Convert.ToString(ds.Tables[0].Rows[0]["comCompetenceDN"]));   
                        }
                        
                    }
                    if (!String.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["skillLocal"])))
                    { 
                        Splocal.InnerHtml =localText.Text+ " : "+ (Convert.ToString(ds.Tables[0].Rows[0]["skillLocal"]));
                       
                    }
                    if (!String.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["skillAchive"])))
                    {
                        spAchive1.InnerHtml = Convert.ToString(ds.Tables[0].Rows[0]["skillAchive"]);
                        spAchive.InnerHtml = AchiveText.Text+ " : " + (Convert.ToString(ds.Tables[0].Rows[0]["skillAchive"]));
                       
                    }

                }
                else
                {
                    rptAll.DataSource = null;
                    rptAll.DataBind();
                }
            }
            else
            {
                rptAll.DataSource = null;
                rptAll.DataBind();
            }
        }
        catch (Exception ex)
        {
            Common.WriteLog("error in subCompetenceSelectAll" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }
    protected void GetMessgeSkillLessthen(Int32 id)
    {
        try
        {
            EmployeeSkillBM obj = new EmployeeSkillBM();
            obj.skillIsActive = true;
            obj.skillIsDeleted = false;
            obj.skillAchive = skillAchive;
            obj.skillCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            obj.skillComId = skillComId;
            obj.skillUserId = Convert.ToInt32(Session["OrgUserId"]);
            obj.GetMessgeSkillLessthen(id);
            DataSet ds = obj.ds;
            if (Convert.ToString(Session["Culture"]) == "Danish")
            {
                ds.Tables[0].Columns["divName"].ColumnName = "abcd";
                ds.Tables[0].Columns["divNameDN"].ColumnName = "divName";

                ds.Tables[0].Columns["jobName"].ColumnName = "abcde";
                ds.Tables[0].Columns["jobNameDN"].ColumnName = "jobName";

            }
            ViewState["msg"] = ds;

            Repeater1.DataSource = ds;
            Repeater1.DataBind();



        }
        catch (Exception ex)
        {
            Common.WriteLog("error in subCompetenceSelectAll" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }
    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        //Common.WriteLog("Culture");
        //string language = "French";
        string language = Convert.ToString(Session["Culture"]);
        // Common.WriteLog("language" + language);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    // Common.WriteLog("languagein");
        //    if (language.EndsWith("Danish"))
        //    {
        //        languageId = "da-DK";
        //    }
        //    else
        //    {
        //        // Common.WriteLog("languageelse");
        //        languageId = "en-GB";
        //    }
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion

    #region Repeater Event
    protected void rptAll_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Header)
            {
                if (!String.IsNullOrEmpty(Request.QueryString["Lid"]))
                {
                    HtmlGenericControl row = (HtmlGenericControl)e.Item.FindControl("row");
                    row.Visible = false;
                }
                if (String.IsNullOrEmpty(Request.QueryString["Lid"]))
                {
                    HtmlGenericControl divMessage = (HtmlGenericControl)e.Item.FindControl("divMessage");

                    divMessage.Visible = false;
                }

            }
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (!String.IsNullOrEmpty(Request.QueryString["Lid"]))
                {
                    HtmlGenericControl Emptyrow = (HtmlGenericControl)e.Item.FindControl("Emptyrow");
                   
                    Emptyrow.Visible = false;
                }
                if (String.IsNullOrEmpty(Request.QueryString["Lid"]))
                {
                  

                    HtmlGenericControl divMessageButton = (HtmlGenericControl)e.Item.FindControl("divMessageButton");

                    divMessageButton.Visible = false;
                }
                
            }
        }
        catch (Exception ex)
        {
            Common.WriteLog("error in R1_ItemDataBound" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }

    protected void rptAll_OnItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "total")
        {
            try
            {
                DataSet ds = (DataSet)ViewState["msg"];
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        Repeater1.DataSource = ds;
                        Repeater1.DataBind();
                        dvsubretr.Visible = true;
                    }
                    else
                    {
                        dvsubretr.Visible = false;
                    }
                }
                else
                {
                    dvsubretr.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in total" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            }

        }
        else if (e.CommandName == "Division")
        {
            try
            {
                Int32 userdepId = 0;
                Int32 userJobType = 0;
                DataSet ds = (DataSet)ViewState["msg"];
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (!String.IsNullOrEmpty(Convert.ToString(ds.Tables[1].Rows[0]["userdepId"])))
                        {
                            userdepId = (Convert.ToInt32(ds.Tables[1].Rows[0]["userdepId"]));
                        }
                        if (!String.IsNullOrEmpty(Convert.ToString(ds.Tables[1].Rows[0]["userJobType"])))
                        {
                            userJobType = (Convert.ToInt32(ds.Tables[1].Rows[0]["userJobType"]));
                        }
                    }
                }
                DataView dv = new DataView();
                dv = ds.Tables[0].DefaultView;
                dv.RowFilter = "userdepId = '" + Convert.ToInt32(userdepId) + "'";
                DataTable dtitm = dv.ToTable();
                DataSet ds1 = new DataSet();
                ds1.Tables.Add(dtitm);
                if (ds1 != null)
                {
                    if (ds1.Tables[0].Rows.Count > 0)
                    {
                        Repeater1.DataSource = ds1;
                        Repeater1.DataBind();
                        dvsubretr.Visible = true;
                    }
                    else
                    {
                        dvsubretr.Visible = false;
                    }
                }
                else
                {
                    dvsubretr.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in Division" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            }
        }
        else if (e.CommandName == "Jobtype")
        {
            try
            {
                Int32 userdepId = 0;
                Int32 userJobType = 0;
                DataSet ds = (DataSet)ViewState["msg"];
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (!String.IsNullOrEmpty(Convert.ToString(ds.Tables[1].Rows[0]["userdepId"])))
                        {
                            userdepId = (Convert.ToInt32(ds.Tables[1].Rows[0]["userdepId"]));
                        }
                        if (!String.IsNullOrEmpty(Convert.ToString(ds.Tables[1].Rows[0]["userJobType"])))
                        {
                            userJobType = (Convert.ToInt32(ds.Tables[1].Rows[0]["userJobType"]));
                        }
                    }
                }
                DataView dv = new DataView();
                dv = ds.Tables[0].DefaultView;
                dv.RowFilter = "userJobType = '" + Convert.ToInt32(userJobType) + "'";
                DataTable dtitm = dv.ToTable();
                DataSet ds1 = new DataSet();
                ds1.Tables.Add(dtitm);
                if (ds1 != null)
                {
                    if (ds1.Tables[0].Rows.Count > 0)
                    {
                        Repeater1.DataSource = ds1;
                        Repeater1.DataBind();
                        dvsubretr.Visible = true;
                    }
                    else
                    {
                        dvsubretr.Visible = false;
                    }
                }
                else
                {
                    dvsubretr.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in Jobtype" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            }
        }
    }
    protected void Repeater1_OnItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Message")
        {
            try
            {
                //string col1 = ((Label)e.Item.FindControl("lbpoint")).Text;
                string lblFirstName = ((Label)e.Item.FindControl("lblFirstName")).Text;
                //Int32 masToUserId = Convert.ToInt32(e.CommandArgument);
                Session["masToUserId"] = e.CommandArgument;
                litname.Text = lblFirstName;
                
                Response.Redirect("OrgProvideHelp.aspx?Comp="+Convert.ToString(ViewState["Compid"])+"&ToUser="+e.CommandArgument+"");
                
                //mpecal.Show();
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in total" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            }
        }
        else if (e.CommandName == "Invite")
        {
            try
            {
                string lblFirstName = ((Label)e.Item.FindControl("lblFirstName")).Text;
                //Int32 masToUserId = Convert.ToInt32(e.CommandArgument);
                Session["InviteToUserId"] = e.CommandArgument;
                if (!String.IsNullOrEmpty(Request.QueryString["Hid"]))
                {
                    Session["Inviteformail"] = "Need Help Competence";
                    Session["Invitemessge"] = "Need Help";
                    //txtinvite.Text = Session["OrgUserName"] + "  Needs Help.";Sent you Reuest Help For Competence
                  
                    //txtinvite.Text = "Sent you Request Help For Competence";
                    
                 String inviteMessage = CommonModule.getTemplatebyname("InviteMessage", Convert.ToInt32(e.CommandArgument));
                  inviteMessage=  inviteMessage.Replace("###ToUser###",lblFirstName );
                  inviteMessage = inviteMessage.Replace("###FromUser###", Convert.ToString(Session["OrgUserName"]));
                    inviteMessage = inviteMessage.Replace("<div>", "");
                    inviteMessage = inviteMessage.Replace("</div>", "");
                    inviteMessage = inviteMessage.Replace("<p>", "");
                    inviteMessage = inviteMessage.Replace("</p>", "");
                    inviteMessage = inviteMessage.Trim();
                    //inviteMessage.ToString().
                    txtinvite.Text = HttpUtility.HtmlDecode(inviteMessage);// string.Format("Hi {0}! " + Environment.NewLine + "In relation to our knowledgesharing/learning culture activities I would like your help to improve my competence level." + Environment.NewLine + "Will you be ok helping me? " + Environment.NewLine + "Thanks in advance {1}. ", lblFirstName, Session["OrgUserName"]);

                    //Great Anders! You are inviting for knowledge sharing and helping to create a learning culture in the company! Thanks! "
                    Session["InviteComId"] = Request.QueryString["Hid"];
                    Session["notificationmsg"] = "sent you invitation for needs help";
                    mpe2.Show();
                }
                else if (!String.IsNullOrEmpty(Request.QueryString["Lid"]))
                {
                    Session["Inviteformail"] = "Provide Help Competence";
                    Session["Invitemessge"] = "Provide Help";

                    Session["InviteComId"] = Request.QueryString["Lid"];
                    Session["notificationmsg"] = "sent you invitation for Provide Help";


                    txtInviteGiveHelp.Text = Session["OrgUserName"] + " have sent you invitation";
                    mpeGiveHelp.Show();

                    //Upload doc
                }



            }
            catch (Exception ex)
            {
                Common.WriteLog("error in Invite" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            }

        }
    }
    #endregion

    #region uploadDOc

    protected void uploadDoc(object sender, EventArgs e)
    {
        throw new NotImplementedException();
    }

    #endregion
    
    #region WebMethod
    [WebMethod(EnableSession = true)]
    public static string UpdateData(string txtto)
    {
        string msg = string.Empty;
        // msg = "true";
        NotificationBM objAtt = new NotificationBM();
        objAtt.mesfromUserId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        objAtt.masToUserId = Convert.ToInt32(HttpContext.Current.Session["masToUserId"]);
        objAtt.messubject = txtto;
        objAtt.masCreatedDate = DateTime.Now;
        objAtt.masIsDeleted = false;
        objAtt.masIsActive = true;
        objAtt.masCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
        objAtt.mastype = "";
        if (!objAtt.InsertMessage())
        {
            msg = "false";

        }
        else
        {
            #region Notification
            UserBM obj1 = new UserBM();
            obj1.SetUserId = Convert.ToInt32(HttpContext.Current.Session["masToUserId"]);
            obj1.GetUserSettingById();
            DataSet ds1 = obj1.ds;
            if (ds1.Tables[0].Rows.Count > 0)
            {
                if (!String.IsNullOrEmpty(Convert.ToString(ds1.Tables[0].Rows[0]["SetMessageNotification"])))
                {
                    if (Convert.ToBoolean(ds1.Tables[0].Rows[0]["SetMessageNotification"]))
                    {
                        NotificationBM obj = new NotificationBM();
                        obj.notsubject = "Click here to check your message.";
                        obj.notUserId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
                        obj.notIsActive = true;
                        obj.notIsDeleted = false;
                        obj.notCreatedDate = DateTime.Now;
                        obj.notCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
                        obj.notpage = "chart.aspx?mes=" + Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
                        obj.nottype = "Message";
                        obj.notToUserId = Convert.ToInt32(HttpContext.Current.Session["masToUserId"]);
                        obj.InsertNotification();
                    }
                }
            }
            #endregion

            msg = "true";
            HttpContext.Current.Session["masToUserId"] = "";

        }

        return msg;
    }
    [WebMethod(EnableSession = true)]
    public static string Invitation(string txtto, String date, string compTitle, string Ach, string engSub, string DenishSub)
    {
        
                                    

        string msg = string.Empty;
        InvitationBM objAtt = new InvitationBM();
        objAtt.invfromUserId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        objAtt.invToUserId = Convert.ToInt32(HttpContext.Current.Session["InviteToUserId"]);
        objAtt.invsubject = txtto;
        objAtt.invCreatedDate = CommonUtility.GetCurrentDateTime();
        objAtt.invIsDeleted = false;
        objAtt.invIsActive = true;
        objAtt.invCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
        objAtt.invtype = Convert.ToString(HttpContext.Current.Session["Invitemessge"]);
        objAtt.invComId = Convert.ToInt32(HttpContext.Current.Session["InviteComId"]);
        objAtt.invStatus = Convert.ToInt32(3);
        //Add docfilename and suggestpath
        objAtt.invDocSuggestPath = string.Empty;
        objAtt.invDocName = Convert.ToString(HttpContext.Current.Session["docFileName"]);
      
        

        //
        if (!objAtt.InsertInvition())
        {
            msg = "false";
        }
        else
        {
            var dsAtt = objAtt.ds;
            var lastId = 0;

            if (!String.IsNullOrEmpty(date))
            {
                // objAtt.invDate = Convert.ToDateTime(date.ToString(dd/Co));
                try
                {
                    //objAtt.invDate = DateTime.ParseExact(date, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                    objAtt.invDate = CommonUtility.GetCurrentDateTime(); //DateTime.ParseExact(date, "dd/MM/yyyy", null);
                }
                catch (Exception ex)
                {
                    objAtt.invDate = DateTime.Now;

                }
            }

            if (dsAtt.Tables[0].Rows.Count > 0)
            {
                if (!String.IsNullOrEmpty(Convert.ToString(dsAtt.Tables[0].Rows[0]["lastId"])))
                {
                    lastId = Convert.ToInt32(dsAtt.Tables[0].Rows[0]["lastId"]);
                }
                
            }
            if (dsAtt.Tables[1].Rows.Count > 0)
            {
                if (!String.IsNullOrEmpty(Convert.ToString(dsAtt.Tables[1].Rows[0]["InviCount"])))
                {
                    int count = Convert.ToInt32(dsAtt.Tables[1].Rows[0]["InviCount"]);

                    if (count == 0)
                    {
                        LogMasterLogic obj = new LogMasterLogic();

                        

                        //Give point  to  user who need  help On First Request For ------------- Updated by Suhani 27/1/2016
                        var isPointAllow = true;
                        var db = new startetkuEntities1();
                        int needHelpUserID = Convert.ToInt32(HttpContext.Current.Session["InviteToUserId"]);
                        var needHelpUser = (from u in db.UserMasters where u.userId == needHelpUserID select new { u.userFirstName, u.userLastName }).FirstOrDefault();
                        obj.LogDescription = string.Format("{0} Take help from  userID : {1} ", Convert.ToString(HttpContext.Current.Session["OrgUserName"]), needHelpUser.userFirstName + " " + needHelpUser.userLastName);   // User log description..

                        //obj.LogDescription = string.Format("userID : {0} Take help from {1} ", Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]), Convert.ToInt32(HttpContext.Current.Session["InviteToUserId"]));   // User log description..

                        var point = LogMasterLogic.InsertUpdateLogParam(Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]), Common.ALP(), Common.RequestSession(), obj.LogDescription, isPointAllow, needHelpUserID,0);

                        if (isPointAllow)
                        {
                            PointBM.InsertUpdatePoint(Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]), 0,point);
                        }
                        if (point > 0)
                        {
                            Organisation_HelpCompetence hp=new Organisation_HelpCompetence();
                            hp.insertKnowledge_Point(point, Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]), Convert.ToInt32(HttpContext.Current.Session["InviteComId"]));
                        }
                    }
                   


                }


            }



          
            #region Notification
            UserBM obj1 = new UserBM();
            obj1.SetUserId = Convert.ToInt32(HttpContext.Current.Session["InviteToUserId"]);
            obj1.GetUserSettingById();
            DataSet ds1 = obj1.ds;
            if (ds1.Tables[0].Rows.Count > 0)
            {
                if (!String.IsNullOrEmpty(Convert.ToString(ds1.Tables[0].Rows[0]["SetInvitationNotification"])))
                {
                    if (Convert.ToBoolean(ds1.Tables[0].Rows[0]["SetNewFileUploadwarning"]))
                    {
                        NotificationBM obj = new NotificationBM();
                        obj.notsubject = Convert.ToString(HttpContext.Current.Session["notificationmsg"]);
                        obj.notUserId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
                        obj.notIsActive = true;
                        obj.notIsDeleted = false;
                        obj.notCreatedDate = DateTime.Now;
                        obj.notCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
                        //obj.notpage = "OrgInvitation.aspx?id=" + Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
                        obj.notpage = "OrgInvitation.aspx?id=" + lastId;
                        obj.nottype = "Invitation";
                        obj.notToUserId = Convert.ToInt32(HttpContext.Current.Session["InviteToUserId"]);
                        obj.InsertNotification();

                        #region Send mail
                        
                        Template template = new Template();
                        template= CommonModule.getTemplatebyname1("Need Help Competence", Convert.ToInt32(HttpContext.Current.Session["InviteToUserId"]));
                        String confirmMail = template.TemplateName;
                        
                        if (!String.IsNullOrEmpty(confirmMail))
                        {
                            String subject = engSub;

                            if (template.Language == "Danish")
                            {
                                subject = DenishSub;
                            }
                            
                            

                            UserBM Cust = new UserBM();
                            Cust.userId = Convert.ToInt32(HttpContext.Current.Session["InviteToUserId"]);
                            Cust.SelectPasswordByUserId();
                            DataSet ds = Cust.ds;
                            if (ds != null)
                            {
                                if (ds.Tables[0].Rows.Count > 0)
                                {//Session["Inviteformail"]
                                    String name = Convert.ToString(ds.Tables[0].Rows[0]["userFirstName"]);
                                    string fromid = Convert.ToString(ConfigurationManager.AppSettings["fromid"]);
                                    String empname = Convert.ToString(HttpContext.Current.Session["OrgUserName"]);
                                    String level = "";
                                    String competence = "";

                                    if (!string.IsNullOrEmpty(compTitle))
                                    {
                                        level = Ach;
                                        competence = compTitle;
                                    }
                                    else
                                    {
                                        if (ds.Tables[1].Rows.Count > 0)
                                        {
                                            level = Convert.ToString(ds.Tables[1].Rows[0]["skillAchive"]);
                                            competence = Convert.ToString(ds.Tables[1].Rows[0]["comCompetence"]);
                                        }
                                    }

                                   
                                    string tempString = confirmMail;
                                    tempString = tempString.Replace("###name###", name);
                                    tempString = tempString.Replace("###empname###", empname);
                                    tempString = tempString.Replace("###level###", level);
                                    tempString = tempString.Replace("###competence###", competence);
                                    tempString = tempString.Replace("###Message###", txtto);
                                    CommonModule.SendMailToUser(Convert.ToString(ds.Tables[0].Rows[0]["userEmail"]), subject, tempString, fromid, Convert.ToString(HttpContext.Current.Session["OrgCompanyId"]));
                                }
                            }
                        }

                        #endregion
                    }
                }
            }

            #endregion

            msg = "true";
            HttpContext.Current.Session["InviteToUserId"] = "";
            HttpContext.Current.Session["Invitemessge"] = "";
            HttpContext.Current.Session["InviteComId"] = "";
            HttpContext.Current.Session["notificationmsg"] = "";
        }

        return msg;
    }
    #endregion

    public void insertKnowledge_Point(Int32 point,Int32 Userid,Int32 ComId)
    {
        // Session["InviteComId"];
        Knowledge_PointBM obj = new Knowledge_PointBM();
        obj.Knowledge_ComId = ComId;
        obj.Knowledge_CompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.Knowledge_point = Convert.ToString(point);
        obj.Knowledge_UserId = Userid;
        obj.CreatedDate = DateTime.Now;
        obj.IsDeleted = false;
        obj.IsActive = true;
        obj.InsertKnowledge_Point();

    }

    protected void SubmitGiveHelpInvitation(object sender, EventArgs e)
    {
        String filePath = "";
        DocumentBM obj = new DocumentBM();

        var docFileNameFriendly = string.Empty;
        var docAttachmentName = string.Empty;

        obj.docDescription = txtInviteGiveHelp.Text;

        if (FileUpload1.HasFile)
        {
            filePath = System.DateTime.Now.ToString("MMddyyhhmmss") + "_" + FileUpload1.PostedFile.FileName;

            Session["docFileName"] = FileUpload1.PostedFile.FileName;

            FileUpload1.SaveAs(Server.MapPath("../") + "Log/upload/Document/" + System.IO.Path.GetFileName(Common.CleanFileName(filePath)));
            docAttachmentName = System.IO.Path.GetFileName(Common.CleanFileName(filePath));
            docFileNameFriendly = FileUpload1.PostedFile.FileName;
        }
        var docFileNameInSystem = Common.CleanFileName(docAttachmentName);
        docAttachmentName = Common.CleanFileName(docAttachmentName);
        const string docTypes = "pdf";
        var userId = Convert.ToInt32(Session["OrgUserId"]);

        var docCreatedDt = DateTime.Now;
        var docCompnyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.docDepId = 0;
        obj.docApprovedStatus = false;
        obj.doccompetence = txtInviteGiveHelp.Text;

        
        DateTime docUpdatedDate = new DateTime();

        string docCreatedBy = string.Empty;
        int docDepIds = 0;
        string docTitle = docAttachmentName;
        bool docApprovedStatus = false;

        //
        var aa = new List<string>();

        string divisionIds = string.Join(",", aa.ToArray());
        aa.Clear();
        //

        string jobTypeIds = string.Join(",", aa.ToArray());


        var isPublic = false;

        var keywords = string.Empty;

        var repository = string.Empty;

        var txtDescriptions = txtInviteGiveHelp.Text;

        var docShareToId = Convert.ToInt32(Session["InviteToUserId"]);

        obj.InsertDocument(docFileNameFriendly, docFileNameInSystem, docAttachmentName, docTypes, userId, docCreatedDt, docUpdatedDate, false, true, docCompnyId, docCreatedBy, docDepIds, docTitle, string.Empty, docApprovedStatus, divisionIds, jobTypeIds, isPublic, keywords, repository, txtDescriptions, docShareToId, 0, "");
        
        Invitation(txtInviteGiveHelp.Text,"","","","","");

        mpeGiveHelp.Hide();

        InsertUpdateLog(docShareToId);
    }

    public void InsertUpdateLog(int needHelpUserID)
    {

        //Givehelp  point  to   loggedin user

        LogMasterLogic obj = new LogMasterLogic();

        var db = new startetkuEntities1();
        var needHelpUser = (from u in db.UserMasters where u.userId == needHelpUserID select new { u.userFirstName, u.userLastName }).FirstOrDefault();
        obj.LogDescription = string.Format("{0} provide Help to  userID : {1} ", Convert.ToString(Session["OrgUserName"]), needHelpUser.userFirstName+" "+needHelpUser.userLastName);   // User log description..

        var userID = Convert.ToInt32(Session["OrgUserId"]);

        var isPointAllow = true;

        var point = LogMasterLogic.InsertUpdateLogParam(userID, Common.ALP(), Common.SuggestSession(), obj.LogDescription, isPointAllow,0,0);

        if (isPointAllow)
            PointBM.InsertUpdatePoint(userID, 0, point);

        //Give point  to  user who need  help

        obj.LogDescription = string.Format("user: {0} take help from {1} ", needHelpUser.userFirstName + " " + needHelpUser.userLastName, Convert.ToString(Session["OrgUserName"]));   // User log description..

        point = LogMasterLogic.InsertUpdateLogParam(needHelpUserID, Common.ALP(), Common.RequestSession(), obj.LogDescription, isPointAllow,0,0);

        if (isPointAllow)
            PointBM.InsertUpdatePoint(needHelpUserID,0, point);


    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("myCompetence.aspx");

    }
}