﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master" AutoEventWireup="true" CodeFile="CompanyDirectionCategoryEdit.aspx.cs" Inherits="Organisation_CompanyDirectionCategoryEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <style type="text/css">
        .chat-widget-head h4 {
            float: none !important;
        }

        .treeNode {
        }

            .treeNode input {
                width: auto;
                margin: 5px;
                float: left !important;
            }

        .form-control {
            text-transform: none;
        }

        label {
            display: inline-block;
            font-size: 14px;
            font-weight: inherit;
            margin-bottom: 5px;
        }

        .inline-form input, .inline-form textarea {
            font-size: 15px;
        }
    </style>
    <style type="text/css">
        body {
            font-family: Geneva,Arial,Helvetica,sans-serif;
        }

        #ContentPlaceHolder1_chkList input {
            width: 33px;
            margin-bottom: 0px !important;
        }

        #ContentPlaceHolder1_chkList label {
            margin-top: 2px;
        }
    </style>
    <style type="text/css">
        .scroll_checkboxes {
            height: 120px;
            width: 270px;
            padding: 5px;
            overflow: auto;
            border: 1px solid #ccc;
        }

        .FormText {
            font-size: 11px;
            font-family: tahoma,sans-serif;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="col-md-6">
        <div class="heading-sec">
            <%--<h1>
                <asp:Label runat="server" ID="lblDataDisplayTitle" Text="Division"></asp:Label>
                <i>Welcome to Flat Lab </i></h1>--%>
            <%--            <h1>--%>
            <%--                Company Direction Category Edit<i><span runat="server" id="QuestionTemplate"></span></i>--%>
            <%--            </h1>--%>
            <asp:Label runat="server" ID="Label12" CssClass="lblModel" meta:resourcekey="comdircatedit" Font-Bold="True" ForeColor="white" Font-Size="22px"></asp:Label>

        </div>
    </div>

    <br />
    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" Font-Size="Large" meta:resourcekey="lblMsgResource1"></asp:Label>
    <br />

    <div class="col-md-12">
        <div class="chat-widget widget-body" style="background: #fff;">
            <div class="chat-widget-head yellow yellow-radius">
                <h4>
                    <asp:Label runat="server" ID="Label1" CssClass="lblModel" meta:resourcekey="editcomdirection" Font-Bold="True" ForeColor="white" Font-Size="22px"></asp:Label>

                </h4>
            </div>
            <div class="indicatesRequireFiled">
                <i><span>* </span>
                    <asp:Literal ID="Literal1" runat="server" EnableViewState="false" />
                </i>
            </div>

            <asp:HiddenField runat="server" ID="hdcatID" />

            <div class="col-md-6" style="width: 100%;">
                <div class="inline-form">
                    <label class="c-label">
                        <asp:Literal ID="Literal2" runat="server" Text="Company Direction Category Name" EnableViewState="false" meta:resourcekey="catname" />:*</label>
                    <asp:TextBox runat="server" ID="tbcatName" MaxLength="50"
                        CssClass="form-control" placeholder="TITLE :" meta:resourcekey="title" />
                    <br />

                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="tbcatName"
                        ErrorMessage="Please enter Company Direction Category Name." CssClass="commonerrormsg" Display="Dynamic"
                        ValidationGroup="chk"></asp:RequiredFieldValidator>
                </div>
            </div>

            <div class="col-md-6" style="width: 100%; display: none;">
                <div class="inline-form">
                    <label class="c-label">
                        <asp:Literal ID="Literal5" Text="Company Direction Category Name(Danish)" runat="server" EnableViewState="false" meta:resourcekey="catname" />:*</label>
                    <asp:TextBox runat="server" ID="tbcatNameDN" MaxLength="50"
                        CssClass="form-control" placeholder="TITLE :" meta:resourcekey="title" /><br />

                </div>
            </div>

            <div class="col-md-6" style="width: 100%;">
                <div class="inline-form">
                    <label class="c-label">
                        <asp:Literal ID="Literal4" runat="server" Text="Description" EnableViewState="false" meta:resourcekey="Descriptionres" />:</label>
                    <asp:TextBox runat="server" placeholder="DESCRIPTION :" ID="tbDESCRIPTION" MaxLength="500"
                        TextMode="MultiLine" Rows="5" CssClass="form-control" meta:resourcekey="descrires" /><br />
                </div>
            </div>

            <div class="col-md-6" style="width: 100%;">
                <div class="inline-form">
                    <label class="c-label">
                        <asp:Literal ID="Literal3" Text="Company Direction Sequence Number" runat="server" EnableViewState="false" meta:resourcekey="seqno" />:*</label>
                    <asp:TextBox runat="server" ID="tbsqno"
                        CssClass="form-control" placeholder="Sequence Number :" meta:resourcekey="seqnores" MaxLength="3" min="0" type="number" max="999" /><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="tbsqno"
                        ErrorMessage="Please enter sequence number." CssClass="commonerrormsg" Display="Dynamic"
                        ValidationGroup="chk"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12" style="width: 100%;">
                    <div class="col-md-4" style="margin-top: 20px; margin-left: -14px; color: black; margin-top: 50px; font-size: 20px;">
                        <label class="c-label">
                            <asp:Literal ID="Literal12" runat="server" meta:resourcekey="Translations"> </asp:Literal></label>
                    </div>

                    <div class="col-md-8" style="overflow-y: scroll; height: 150px; margin-left: 10px;">
                        <asp:GridView ID="gridTranslate" runat="server" AutoGenerateColumns="False" CellPadding="0" Style="background-color: #f9f9f9; margin-bottom: 0px !important;"
                            Width="100%" GridLines="None" CssClass="table table-striped table-bordered table-hover table-checkable datatable">
                            <Columns>
                                <asp:TemplateField HeaderText="Language" meta:resourcekey="language">
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("resLanguage") %>'></asp:Label>
                                        <asp:HiddenField ID="hdnResLangID" runat="server" Value='<%# Eval("resLangID") %>'></asp:HiddenField>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" Font-Size="14px" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" Font-Size="15px" BorderWidth="0px" Height="20px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Value" meta:resourcekey="value">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtTranValue" Value='<%# Eval("LangText") %>' runat="server"></asp:TextBox>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" Font-Size="14px" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" Font-Size="15px" BorderWidth="0px" Height="20px" />
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="border: 0px;">
                <asp:Button runat="server" ID="btnsubmit" class="btn btn-primary yellow"
                    Text="Save" ValidationGroup="chk" OnClick="btnSave_click"
                    Style="border-radius: 5px; width: 7%" meta:resourcekey="btnsave"></asp:Button>
                <asp:Button runat="server" ID="btnCancel" OnClick="btnCancel_click" class="btn btn-primary black"
                    Text="Cancel" meta:resourcekey="btnCancel"></asp:Button>
            </div>

        </div>
    </div>
    <script>
        $(document).ready(function () {
            "use strict";

            App.init(); // Init layout and core plugins
            Plugins.init(); // Init all plugins
            FormComponents.init(); // Init all form-specific plugins
        });
    </script>
</asp:Content>

