﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;

public partial class Organisation_EditCategory : System.Web.UI.Page
{
    #region define object
    private CategoryBM obj = new CategoryBM();
    #endregion

    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "my", " SetExpandCollapse();", true);
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            // Division.InnerHtml = "Welcome   " + Convert.ToString(Session["OrgUserName"]) + "!";
            Division.InnerHtml = GetLocalResourceObject("Welcome.Text").ToString() + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            ViewState["catParentId"] = "0";
            PopulateRootLevel();
            GetAllCompetenceCategory();
            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                GetAllCompetenceCategorybyId();
            }
        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion

    #region method
    protected void SetDefaultMessage()
    {

        string id = "?id=";
        string childurl = "child.aspx";
        if (Request.RawUrl.ToString().ToLower().Contains(childurl.ToLower()))
        {
            if (Request.RawUrl.ToString().ToLower().Contains(id.ToLower()))
            {
                // lblDataDisplayTitle.Text = "Edit Children";
            }
            else
            {
                // lblDataDisplayTitle.Text = "Add Children";
            }
        }
    }
    protected void GetAllCompetenceCategory()
    {
        
        obj.depIsActive = true;
        obj.depIsDeleted = false;
        obj.depCompanyId = Convert.ToInt32(Session["OrgUserId"]);
        obj.GetAllCompetenceCategory();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlDepartment.Items.Clear();

                ddlDepartment.DataSource = ds.Tables[0];
                ddlDepartment.DataTextField = "catName";
                ddlDepartment.DataValueField = "catId";
                ddlDepartment.DataBind();

                ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
            }
            else
            {
                ddlDepartment.Items.Clear();
                ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
            }
        }
        else
        {
            ddlDepartment.Items.Clear();
            ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
        }

    }
    protected void InsertCompetenceCategory()
    {
        CategoryBM obj2 = new CategoryBM();
        obj2.CompetenceCategoryCheckDuplication(txtDepartmentName.Text, -1, -1);
        String returnMsg = obj2.ReturnString;
        if (returnMsg == "")
        {
            
            obj.depName = txtDepartmentName.Text;
            obj.depNameDN = txtDepartmentNameDN.Text;
            //if (Convert.ToInt32(ddlDepartment.SelectedValue) == 0)
            //{
            //    obj.depPerentId = 0;
            //}
            //else
            //{
            //    obj.depPerentId = Convert.ToInt32(ddlDepartment.SelectedValue);
            //}
            obj.depPerentId = getvalue();
            obj.depCompanyId = Convert.ToInt32(Session["OrgUserId"]);
            obj.depIsActive = true;
            obj.depIsDeleted = false;
            obj.depCreatedDate = DateTime.Now;
            obj.depDepId = 0;
            //obj.divDescription = txtDESCRIPTION.Text;
            obj.InsertCompetenceCategory();
            if (obj.ReturnBoolean == true)
            {
                Response.Redirect("Category.aspx?msg=ins");
            }
        }
        else
        {
            lblMsg.Text = returnMsg == "catName" ? CommonModule.msgNameAlreadyExists : CommonModule.msgProblemInsertRecord;
        }
    }
    protected void UpdateCompetenceCategory()
    {
        CategoryBM obj2 = new CategoryBM();
        obj2.CompetenceCategoryCheckDuplication(txtDepartmentName.Text, Convert.ToInt32(Request.QueryString["id"]), Convert.ToInt32(Session["OrgCompanyId"]));
        String returnMsg = obj2.ReturnString;
        if (returnMsg == "")
        {
            
            obj.depId = Convert.ToInt32(Request.QueryString["id"]);
            obj.depName = txtDepartmentName.Text;
            obj.depNameDN = txtDepartmentNameDN.Text;
           
            obj.depPerentId = getvalue();
            obj.depUpdatedDate = DateTime.Now;
            //obj.divDescription = txtDESCRIPTION.Text;
            obj.UpdateCompetenceCategory();
            if (obj.ReturnBoolean == true)
            {
                Response.Redirect("Category.aspx?msg=upd");
            }
        }
        else
        {
            lblMsg.Text = returnMsg == "catName" ? CommonModule.msgNameAlreadyExists : CommonModule.msgProblemInsertRecord;
        }

    }
    protected void GetAllCompetenceCategorybyId()
    {
        
        obj.depId = Convert.ToInt32(Request.QueryString["id"]);
        obj.GetAllDivisionbyid();
        DataSet ds = obj.ds;

        if (ds == null) return;
        if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["catName"])))
            txtDepartmentName.Text = Convert.ToString(ds.Tables[0].Rows[0]["catName"]);
        if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["catNameDN"])))
            txtDepartmentNameDN.Text = Convert.ToString(ds.Tables[0].Rows[0]["catNameDN"]);

        //if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["catDescription"])))
        //    txtDESCRIPTION.Text = Convert.ToString(ds.Tables[0].Rows[0]["catDescription"]);

        //if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["catPerentId"])))
        //    ddlDepartment.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["catPerentId"]);
        // ViewState["catParentId"] = Convert.ToString(ds.Tables[0].Rows[0]["catPerentId"]);
    }
    #endregion

    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Request.QueryString["id"]))
        {
            UpdateCompetenceCategory();
        }
        else
        {
            InsertCompetenceCategory();

        }
    }
    protected void btnCancel_click(object sender, EventArgs e)
    {
        Response.Redirect("Category.aspx");
    }
    #endregion


    #region TreeView
    private void FillCategoryChecklist(TreeNodeCollection nodes, int id)
    {

        foreach (TreeNode child in nodes)
        {

            if (Convert.ToString(id) == Convert.ToString(child.Value))
            {
                child.Checked = true;

            }

           // FillCategoryChecklist(child.ChildNodes, id);
        }


    }
    protected void TreeView_TreeNodePopulate(object sender, TreeNodeEventArgs e)
    {
        return;
        int ParentCatagoryID = Int32.Parse(e.Node.Value);
        PopulateSubLevel(ParentCatagoryID, e.Node);
        FillCategoryChecklist(TreeView1.Nodes, Convert.ToInt32(ViewState["catParentId"]));
    }
   
    private void PopulateSubLevel(int ParentCatagoryID, TreeNode parentNode)
    {
        //Your sublevel Datatable ie. dtSub
        obj.depPerentId = ParentCatagoryID;
        obj.depCompanyId = Convert.ToInt32(Session["OrgUserId"]);
        obj.GetCompetenceCategoryFromParentId();
        DataSet ds = obj.ds;
        PopulateNodes(ds.Tables[0], parentNode.ChildNodes);


    }
    private void PopulateNodes(DataTable dt, TreeNodeCollection nodes)
    {
        foreach (DataRow dr in dt.Rows)
        {
            TreeNode tn = new TreeNode();
            tn.Value = dr["catId"].ToString();
            tn.Text = dr["catName"].ToString();
            nodes.Add(tn);
            //If node has child nodes, then enable on-demand populating
            tn.PopulateOnDemand = ((int)(dr["catChildCount"]) > 0);
        }
    }
    private void PopulateRootLevel()
    {
        DataTable table = new DataTable();
        DataColumn column = new DataColumn();
        column.DataType = System.Type.GetType("System.Int32");
        column.ColumnName = "catId";
        table.Columns.Add(column);

        // Create second column.
        column = new DataColumn();
        column.DataType = Type.GetType("System.String");
        column.ColumnName = "catName";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.Int32");
        column.ColumnName = "catChildCount";
        table.Columns.Add(column);

        DataRow dr = table.NewRow();
        dr["catId"] = 0;
        dr["catName"] = "Root";
        dr["catChildCount"] = 10;
        table.Rows.Add(dr);
        table.AcceptChanges();


        
        obj.depPerentId = 0;
        obj.depCompanyId = Convert.ToInt32(Session["OrgUserId"]);
        obj.GetCompetenceCategoryFromParentId();
        DataSet ds = obj.ds;
        PopulateNodes(table, TreeView1.Nodes);

    }
    private Int32 getCategory(TreeNodeCollection treeNode, int ret)
    {
        foreach (TreeNode child in treeNode)
        {
            if (child.Checked == true)
            {
                ret = Convert.ToInt32(child.Value);
                return ret;
            }

            ret = getCategory(child.ChildNodes, ret);
        }
        return ret;
    }

    protected Int32 getvalue()
    {

        if (TreeView1.CheckedNodes.Count > 0)
        {

            foreach (TreeNode node in TreeView1.CheckedNodes)
            {

                if (node.Parent != null)
                {
                    string checkedValue = node.Text.ToString();
                    Int32 ret = Convert.ToInt32(node.Value);
                    return ret;
                }

            }
            foreach (TreeNode node in TreeView1.Nodes)
            {
                if (node.Checked == true)
                {
                    string checkedValue = node.Text.ToString();
                    Int32 ret = Convert.ToInt32(node.Value);
                    return ret;
                }
            }
            //stablish the session variable only when the foreach has finished
            //Session["listActivity"] = listActivity;
        }
        return 0;
    }
    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        //string language = "en-GB";
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Denish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion
}