﻿<%@ Page Title="AskingCulture" Language="C#"
    MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true"
    CodeFile="pending_helprequest.aspx.cs" Inherits="Organisation_pending_helprequest"
    Culture="auto"
    meta:resourcekey="PageResource" UICulture="auto" %>

<%@ Reference Page="~/Organisation/ActivityLists.aspx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%-- <link href="../assets/assets/css/plugins/jquery-ui.css" rel="stylesheet" />--%>


    <%--   <link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/themes/start/jquery-ui.css" />
    <script src="../plugins/bootstrap-inputmask/jquery.inputmask.min.js" type="text/javascript"></script>--%>

    <%-- <link href="../Scripts/fullcalendor/dATEPICKER/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <script src="../Scripts/fullcalendor/dATEPICKER/bootstrap-datetimepicker.min.js"></script>--%>


   <%-- <link type="text/css" href="Datatable/datatables.css" rel="stylesheet" />
    <script type="text/javascript" src="Datatable/datatables.js"></script>--%>

   



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:HiddenField ID="hdnShow" runat="server" Value="Show" meta:resourcekey="ShowRes"></asp:HiddenField>
    <asp:HiddenField ID="hdnentries" runat="server" Value="entries" meta:resourcekey="entriesRes"></asp:HiddenField>
    <asp:HiddenField ID="hdnSearch" runat="server" Value="Search" meta:resourcekey="SearchRes"></asp:HiddenField>

    <asp:HiddenField ID="hdnShowing1" runat="server" Value="Showing" meta:resourcekey="ShowingRes"></asp:HiddenField>
    <asp:HiddenField ID="hdnto" runat="server" Value="to" meta:resourcekey="toRes"></asp:HiddenField>
    <asp:HiddenField ID="hdnof" runat="server" Value="of" meta:resourcekey="ofRes"></asp:HiddenField>
    <asp:HiddenField ID="hdnentrie" runat="server" Value="entries" meta:resourcekey="entrieRes"></asp:HiddenField>

    <asp:HiddenField ID="hdnPrevious" runat="server" Value="of" meta:resourcekey="PreviousRes"></asp:HiddenField>
    <asp:HiddenField ID="hdnNext" runat="server" Value="entries" meta:resourcekey="NextRes"></asp:HiddenField>

    <%--  <style type="text/css">
         .dataTables_filter
          {
           font-weight:normal;
           }
     </style> --%>

    <!-- jQuery -->
    <%-- <script type="text/javascript" charset="utf8" src="//code.jquery.com/jquery-1.10.2.min.js"></script>--%>


    <div class="col-md-6">
        <div class="heading-sec">
            <h1 style="margin: 0 0 0 15px">
                <asp:HiddenField ID="hdnPendingRequest" runat="server" meta:resourcekey="hdnPendingRequest"></asp:HiddenField>
                <asp:HiddenField ID="hdnCompetence" runat="server" meta:resourcekey="hdnCompetence"></asp:HiddenField>
                <span runat="server" id="sp1"></span><i><span runat="server" id="Employee"></span>
                </i>
            </h1>
        </div>
    </div>
    <div class="col-md-12" style="margin-top: 20px;">
        <div id="graph-wrapper">
            <div class="col-md-12">
                <asp:HiddenField ID="hdnRecordNotFound" runat="server" meta:resourcekey="hdnRecordNotFound"></asp:HiddenField>
                <a href="pending_request.aspx" title="" class="btn btn-primary yellow" style="border: 0px; margin-left: 5px; border-radius: 5px">
                    <%--  <button type="button" class="btns  yellow  lrg-btn flat-btn add_user" style="border: 0px;
                        margin-left: 5px;">--%>
                    <asp:Literal ID="Literal9" runat="server" meta:resourcekey="PendingRequest" EnableViewState="false" />
                    <%--    </button>--%>
                </a>
                <div class="chart-tab  manager_table">
                    <div id="tabs-container" style="margin-top: 20px;" class="yellow">
                        <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" meta:resourcekey="lblMsgResource1"></asp:Label>
                        <asp:HiddenField ID="hdnPending" runat="server" Value="Pending" meta:resourcekey="PendingResource"></asp:HiddenField>
                        <asp:HiddenField ID="hdnAccept" runat="server" Value="Accept" meta:resourcekey="AcceptResource"></asp:HiddenField>
                        <asp:HiddenField ID="hdnCancel" runat="server" Value="Cancel" meta:resourcekey="CancelResource"></asp:HiddenField>
                        <%--   EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>' OnRowCommand="gvGrid_RowCommand"--%>

                        <asp:GridView ID="gvGrid" runat="server" AutoGenerateColumns="False" CellPadding="0"
                            Width="100%" GridLines="None" DataKeyNames="skillUserId" 
                           CssClass="table table-bordered table-hover table-checkable"
                            EmptyDataText='<%# hdnRecordNotFound.Value %>' OnRowCommand="gvGrid_RowCommand"
                            OnRowDataBound="gvGrid_RowDataBound" BackColor="White" meta:resourcekey="gvGridResource1" PageSize="15">

                             <%-- CssClass="table table-striped table-bordered table-hover table-checkable  datatable"--%>
                            <HeaderStyle CssClass="aa" />
                            <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>

                                <asp:TemplateField HeaderText="ID" meta:resourcekey="TemplateFieldResource1">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="5%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="EMPLOYEE NAME" meta:resourcekey="EMPLOYEENAME">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUNamer" runat="server" Text='<%# Eval("name") %>' meta:resourcekey="lblUNamerResource1"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                </asp:TemplateField>
                                <asp:TemplateField meta:resourcekey="RequestType">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSkillType" runat="server" Text='<%# Eval("SkillType") %>' meta:resourcekey="lblUNamerResource1"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Date" meta:resourcekey="Date">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserNamer1" runat="server" Text='<%# Eval("skillCreatedDate") %>'
                                            meta:resourcekey="lblUserNamer1Resource1"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="15%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Status" meta:resourcekey="Status">
                                    <ItemTemplate>

                                        <asp:Label ID="lblUserStatus" runat="server" Text='<%# Eval("status") %>'></asp:Label>


                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow icon icon-sort "
                                        Width="15%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                </asp:TemplateField>
                                <%-- <asp:TemplateField HeaderText="Comment" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblComment" runat="server" Text="<%# Eval('skillComment') %>" ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs"  />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                </asp:TemplateField>--%>
                                <asp:TemplateField HeaderText="Action" meta:resourcekey="Action">
                                    <ItemTemplate>
                                        <div class="vat" style="width: 70px;">
                                            <p>
                                                <%--<i class=""></i><a href="<%# String.Format("request.aspx?View={0}", Eval("skillId")) %>"
                                                    title="View"><%= CommonMessages.View%></a>--%>
                                                <i class=""></i>
                                                <a href="<%# String.Format("{0}", (string) Eval("SkillType")=="Activity Requests"?"ActivityRequestView.aspx?Vw=true&actReqId="+Eval("skillId"):"View_request.aspx?Vw="+Eval("skillUserId")) %>"
                                                    title="View">
                                                    <asp:Literal ID="Literal1" runat="server" meta:resourcekey="View" EnableViewState="false" />

                                                </a>
                                            </p>
                                        </div>
                                        <div class="vat" style="width: 100px;" id="Edit" runat="server">


                                            <p class='<%# Eval("status").ToString() == "Not Saved" ? "HideDiv" : "DisplayDiv" %>'>
                                                <%--<i class="fa fa-pencil"></i><a href="<%# String.Format("request.aspx?id={0}", Eval("skillId")) %>"
                                                    title="Edit">Edit</a>--%>
                                                <i class="fa fa-pencil"></i><a
                                                    href="<%# String.Format("{0}", (string) Eval("SkillType")=="Activity Requests"?"ActivityRequestView.aspx?actReqId="+Eval("skillId"):"View_request.aspx?rid="+Eval("skillUserId")) %>"
                                                    title="Edit">
                                                    <asp:Literal ID="Literal2" runat="server" meta:resourcekey="Edit" EnableViewState="false" />

                                                </a>
                                            </p>
                                        </div>
                                        <div class="total" style="width: 70px; display: none;" id="Delete" runat="server">
                                            <p>
                                                <i class="fa fa-trash-o"></i>
                                                <asp:LinkButton ID="lnkBtnName" runat="server" CommandName="archive" CommandArgument='<%# Eval("skillUserId") %>'
                                                    ToolTip="Delete" OnClientClick="return confirm('Are you sure you want to archive this record?');"
                                                    meta:resourcekey="Delete"></asp:LinkButton>
                                            </p>
                                        </div>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <a href="#" onclick="javascript:history.back(); return false;" class="btn black pull-right" style="border-radius: 5px; margin-left: 0px; margin-top: 5px; float: right; display: none">
                            <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Back" EnableViewState="false" />
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <style type="text/css">
        .yellow {
            border-radius: 0px;
        }

        .HideDiv {
            display: none;
        }

        .DisplayDiv {
            display: block;
        }
    </style>
    <style>
        .faS {
            font-family: FontAwesome !important;
        }

        .gridHeader {
            font-weight: bold;
            color: white;
            background-color: #80a0a0;
        }

            .gridHeader A {
                padding-right: 15px;
                padding-left: 3px;
                padding-bottom: 0px;
                color: #ffffff;
                padding-top: 0px;
                text-decoration: none;
            }

                .gridHeader A:hover {
                    text-decoration: underline;
                }
    </style>

    <!-- DataTables -->

    <%--<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.js"></script>
 <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.css"/>--%>
  
    <script type="text/javascript">
        $(document).ready(function () {
            //$(".datatable").dataTable();
            $(".datatable").removeClass("dataTable");
            $(".pending_helprequest").addClass("active_page");
            setTimeout(function () {
                $(".active").click();
                onLoad();
                // tabMenuClick();
                // $(".myCompetence").addClass("active_page");
            }, 500);
            $(".pending_helprequest").addClass("active_page");


            //$('.dataTables_length').find('arial-label').html($('.dataTables_length').find('arial-label').html().replace('Show', $('#ContentPlaceHolder1_hdnShow').val()));
            //$('.dataTables_filter').find('arial-label').html($('.dataTables_filter').find('arial-label').html().replace('Search', $('#ContentPlaceHolder1_hdnSearch').val()));
            //$('.dataTables_length').find('arial-label').html($('.dataTables_length').find('arial-label').html().replace('entries', $('#ContentPlaceHolder1_hdnentries').val()));

            //$('.dataTables_info').html($('.dataTables_info').html().replace('Showing', $('#ContentPlaceHolder1_hdnShowing1').val()));
            //$('.dataTables_info').html($('.dataTables_info').html().replace('to', $('#ContentPlaceHolder1_hdnto').val()));
            //$('.dataTables_info').html($('.dataTables_info').html().replace('of', $('#ContentPlaceHolder1_hdnof').val()));
            //$('.dataTables_info').html($('.dataTables_info').html().replace('entries', $('#ContentPlaceHolder1_hdnentrie').val()));


            //$('.paginate_button.previous').html($('.paginate_button.previous').html().replace('Previous', $('#ContentPlaceHolder1_hdnPrevious').val()));
            //$('.paginate_button.next').html($('.paginate_button.next').html().replace('Next', $('#ContentPlaceHolder1_hdnNext').val()));

        });
        //$(document).ready(function () {

        //    $(".datatable").dataTable();
        //    $(".datatable").removeClass("dataTable");
        //    ExampanCollapsMenu();

        //    $(".pending_helprequest").addClass("active_page");
        //});


    </script>
    <script type="text/javascript">
        function onLoad() {
            $(".pending_helprequest").addClass("active_page");
            SetExpandCollapse();
            setTimeout(function () {
                //ExampanCollapsMenu();

                // tabMenuClick();
                $(".pending_helprequest").addClass("active_page");
            }, 500);
        }
    </script>

</asp:Content>

