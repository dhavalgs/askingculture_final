﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master" AutoEventWireup="true" CodeFile="EditCategory.aspx.cs" Inherits="Organisation_EditCategory" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
      <style type="text/css">
        .chat-widget-head h4
        {
            float: none !important;
        }
        .treeNode
        {
        }
        
        .treeNode input
        {
            width: auto;
            margin: 5px;
            float: left !important;
        }
        .form-control {
   
    text-transform: none;
    
}

.inline-form input, .inline-form textarea {
    font-size: 14px;    
}

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" 
        meta:resourcekey="lblMsgResource1"></asp:Label>
    <div class="col-md-6">
        <div class="heading-sec">
            <%--<h1>
                <asp:Label runat="server" ID="lblDataDisplayTitle" Text="Division"></asp:Label>
                <i>Welcome to Flat Lab </i></h1>--%>
            <h1>
                <%--<%= CommonMessages.CATEGORY%> --%>
                 <asp:Literal ID="Literal1" runat="server" meta:resourcekey="CATEGORY" enableviewstate="false"/> <i><span runat="server" id="Division"></span></i>
            </h1>
        </div>
    </div>
    <br />
    <br />
    <div class="col-md-6 range ">
        <%--<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
            <i class="fa fa-calendar-o icon-calendar icon-large"></i>
            <span>August 5, 2014 - September 3, 2014</span>  <b class="caret"></b>
        </div>--%>
    </div>
    <div class="col-md-12">
        <div class="chat-widget widget-body" style="background: #fff;">
            <div class="chat-widget-head yellow yellow-radius">
                <h4>
                    <%--<%= CommonMessages.CATEGORY%>--%> <asp:Literal ID="Literal2" runat="server" meta:resourcekey="CATEGORYRes" enableviewstate="false"/>  
                </h4>
            </div>
            <div class="indicatesRequireFiled">
                <i>* <%--<%= CommonMessages.Indicatesrequiredfield %>--%> <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Indicatesrequiredfield" enableviewstate="false"/>  </i>
            </div>
            <div class="col-md-6" style="width: 100%;display:none;">
                <div class="inline-form">
                    <label class="c-label">
                        Category:</label>
                    <asp:DropDownList ID="ddlDepartment" runat="server" Style="width: 100px; height: 32px;
                        display: inline;" Visible="False" 
                        meta:resourcekey="ddlDepartmentResource1">
                    </asp:DropDownList>
                    <br />
                    <br />
                    <asp:TreeView ID="TreeView1" runat="server" ShowCheckBoxes="All" onclick="client_OnTreeNodeChecked(event)"
                        OnTreeNodePopulate="TreeView_TreeNodePopulate" Style="cursor: pointer" ShowLines="True"
                        NodeStyle-CssClass="treeNode" meta:resourcekey="TreeView1Resource1" />
                </div>
            </div>
            <div class="col-md-6" style="width:100%">
                <div class="inline-form">
                    <label class="c-label">
                    <asp:Literal ID="Literal4" runat="server" meta:resourcekey="CategoryName" enableviewstate="false"/> 
                   <%-- <%= CommonMessages.CATEGORY%> <%= CommonMessages.Name%>--%>:*</label>
                    <asp:TextBox runat="server" ID="txtDepartmentName" MaxLength="50" 
                        CssClass="form-control" placeholder="TITLE :" 
                        meta:resourcekey="txtDepartmentNameResource1"/><br/>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtDepartmentName"
                        ErrorMessage="Please enter Department name." CssClass="commonerrormsg" Display="Dynamic"
                        ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator5Resource1"></asp:RequiredFieldValidator>
                </div>
            </div>

            <div class="col-md-6" style="width:100%">
                <div class="inline-form">
                    <label class="c-label">
                    <asp:Literal ID="Literal5" runat="server" meta:resourcekey="CategoryDanish" enableviewstate="false"/> 
                   <%-- <%= CommonMessages.CATEGORY%> <%= CommonMessages.Name%>--%>:*</label>

                    <asp:TextBox runat="server" ID="txtDepartmentNameDN" MaxLength="50" placeholder="Category in danish"
                                         meta:resourcekey="txtDepartmentNameDNResource1" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtDepartmentNameDN"
                                        ErrorMessage="Please enter category name in danish." CssClass="commonerrormsg" Display="Dynamic"
                                        ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator6Resource1" style=" float: left;"></asp:RequiredFieldValidator>
                   
                </div>
            </div>



                  
            
           <%--//Saurin not use in Edit category page: Description--%>

           <%-- <div class="col-md-6" style="width:100%">
                <div class="inline-form">
                    <label class="c-label">
                      <%= CommonMessages.Description %>:*</label>
                    <asp:TextBox runat="server" placeholder="DESCRIPTION :" ID="txtDESCRIPTION" MaxLength="500"
                        TextMode="MultiLine" Rows="5" CssClass="form-control" 
                        meta:resourcekey="txtDESCRIPTIONResource1"/><br/>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDESCRIPTION"
                        ErrorMessage="Please Enter DESCRIPTION." CssClass="commonerrormsg" Display="Dynamic"
                        ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator1Resource1"></asp:RequiredFieldValidator>
                </div>
            </div>--%>
            
            

            <%--<div class="col-xs-12 profile_bottom">
            <asp:Button runat="server" ID="btnCancel" Text="Cancel" CssClass="btn black pull-right"
                    ValidationGroup="chk" CausesValidation="false" OnClick="btnCancel_click" />
                <asp:Button runat="server" ID="btnsubmit" Text="Save" CssClass="btn blue pull-right"
                    ValidationGroup="chk" OnClick="btnsubmit_click" />
                
            </div>--%>
            <div class="modal-footer" style="border: 0px;">
                <asp:Button runat="server" ID="btnsubmit" class="btn btn-primary yellow" 
                    Text="Save" OnClick="btnsubmit_click"  ValidationGroup="chk" 
                    style="border-radius: 5px;" meta:resourcekey="btnsubmitResource1">
                </asp:Button>
                 <asp:Button runat="server" ID="btnCancel" class="btn btn-primary black" 
                    Text="Cancel" OnClick="btnCancel_click" meta:resourcekey="btnCancelResource1"></asp:Button>
               <%-- <button data-dismiss="modal" class="btn btn-default black" type="button">Cancel  </button>--%>
            </div>

        </div>
    </div>
    <script>
        function client_OnTreeNodeChecked(event) {

            var treeNode = event.srcElement || event.target;
            if (treeNode.tagName == "INPUT" && treeNode.type == "checkbox") {
                if (treeNode.checked) {
                    uncheckOthers(treeNode.id);
                }
            }
        }

        function uncheckOthers(id) {
            var elements = document.getElementsByTagName('input');
            // loop through all input elements in form
            for (var i = 0; i < elements.length; i++) {
                if (elements.item(i).type == "checkbox") {
                    if (elements.item(i).id != id) {
                        elements.item(i).checked = false;
                    }
                }
            }
        }
    </script>
    <script>
        $(document).ready(function () {
            "use strict";

            App.init(); // Init layout and core plugins
            Plugins.init(); // Init all plugins
            FormComponents.init(); // Init all form-specific plugins
        });
    </script>
</asp:Content>

