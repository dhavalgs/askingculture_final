﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using startetku.Business.Logic;


public partial class Organisation_OrgQuestionList : System.Web.UI.Page
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            a.HRef = "OrgCompetence.aspx?Add="+Request.QueryString["id"];
            Competence.InnerHtml = "Welcome   " + Convert.ToString(Session["OrgUserName"]) + "!";
            CompetenceSelectAll();
            CompetenceSelectArchiveAll();
            SetDefaultMessage();

        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion


    #region method
    protected void SetDefaultMessage()
    {
        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["msg"])))
        {
            if (Convert.ToString(Request.QueryString["msg"]) == "del")
            {
                lblMsg.Text = CommonModule.msgRecordHasBeenDeletedSuccessfully;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "ins")
            {
                lblMsg.Text = CommonModule.msgRecordInsertedSuccessfully;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "upd")
            {
                lblMsg.Text = CommonModule.msgRecordUpdatedSuccss;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "arc")
            {
                lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
        }

    }
    protected void CompetenceSelectAll()
    {
        QuestionBM obj = new QuestionBM();
        obj.queIsActive = true;
        obj.queIsDeleted = false;
        obj.queComId = Convert.ToInt32(Request.QueryString["id"]);
        obj.GetAllQuestion();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvGrid.DataSource = ds.Tables[0];
                gvGrid.DataBind();

                gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvGrid.DataSource = null;
                gvGrid.DataBind();
            }
        }
        else
        {
            gvGrid.DataSource = null;
            gvGrid.DataBind();
        }
    }
    protected void CompetenceSelectArchiveAll()
    {
        QuestionBM obj = new QuestionBM();
        obj.queIsActive = false;
        obj.queIsDeleted = false;
        obj.queComId = Convert.ToInt32(Request.QueryString["id"]);
        obj.GetAllQuestion();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvArchive.DataSource = ds.Tables[0];
                gvArchive.DataBind();

                gvArchive.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvArchive.DataSource = null;
                gvArchive.DataBind();
            }
        }
        else
        {
            gvArchive.DataSource = null;
            gvArchive.DataBind();
        }
    }
    #endregion


    #region grid Event
    protected void gvGrid_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "archive")
        {
            QuestionBM obj = new QuestionBM();
            obj.queId = Convert.ToInt32(e.CommandArgument);
            obj.queIsActive = false;
            obj.queIsDeleted = false;
            obj.JobTypeQuestionUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    CompetenceSelectAll();
                    CompetenceSelectArchiveAll();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }
    }

    protected void gvArchive_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "archive")
        {
            QuestionBM obj = new QuestionBM();
            obj.queId = Convert.ToInt32(e.CommandArgument);
            obj.queIsActive = true;
            obj.queIsDeleted = false;
            obj.JobTypeQuestionUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    CompetenceSelectAll();
                    CompetenceSelectArchiveAll();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }
        else if (e.CommandName == "permanentlydelete")
        {
            QuestionBM obj = new QuestionBM();
            obj.queId = Convert.ToInt32(e.CommandArgument);
            obj.queIsActive = false;
            obj.queIsDeleted = true;
            obj.JobTypeQuestionUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    CompetenceSelectAll();
                    CompetenceSelectArchiveAll();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }
    }

    #endregion
}