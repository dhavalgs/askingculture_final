﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using System.Configuration;
using starteku_BusinessLogic.Model;
using starteku_BusinessLogic.View;
using startetku.Business.Logic;
using System.Web.Services;
using System.Data.SqlClient;
using System.Threading;
using System.Globalization;

public partial class Organisation_Library : System.Web.UI.Page
{
    startetkuEntities1 db = new startetkuEntities1();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        btnresetfilter.Text = ResetFilter.Text;
        if (!IsPostBack)
        {
            Getserchdoc();
            GetserchForum();
            Library.InnerHtml = GetLocalResourceObject("Welcome.Text").ToString() + "  " +
                                Convert.ToString(Session["OrgUserName"]) + "!";
            hdnDateFormat.Value = Convert.ToString(Session["DateFormat"]);
            GetAllDocuments();
            GetAllDocuments2();
            GetAllJobTypeByCompanyId();
            GetAllDivisionByCompanyId();
            CheckClicked();
            CompetenceSelectAll();
            GetAllUserCreatedBy();
        }
        else
        {
            upFilter.Update();

        }
    }



    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                UserType = "3";
                this.Page.MasterPageFile = "~/Organisation/Employee.master";


            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";


            }
        }
    }

    #region Methods

    protected void CompetenceSelectAll()
    {
        CompetenceMasterBM obj = new CompetenceMasterBM();
        obj.comIsActive = true;
        obj.comIsDeleted = false;
        obj.comCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllCompetenceMasterAdd();


        DataSet ds = obj.ds;
        ViewState["ds"] = ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                var str = Session["Culture"];
                if (Convert.ToString(Session["Culture"]) == "Danish")
                {
                    ds.Tables[0].Columns["comCompetence"].ColumnName = "abcd";
                    ds.Tables[0].Columns["comCompetenceDN"].ColumnName = "comCompetence";


                }

                ddlCompetences.DataTextField = "comCompetence";
                ddlCompetences.DataValueField = "comId";
                ddlCompetences.DataSource = ds.Tables[0];
                ddlCompetences.DataBind();
                ddlCompetences.Items.Insert(0, new ListItem(hdnSelectCompetence.Value, "0"));

                ddlcompetencefilter.DataTextField = "comCompetence";
                ddlcompetencefilter.DataValueField = "comId";
                ddlcompetencefilter.DataSource = ds.Tables[0];
                ddlcompetencefilter.DataBind();
                ddlcompetencefilter.Items.Insert(0, new ListItem(hdnSelectCompetence.Value, "0"));

            }
            else
            {
                ddlCompetences.DataSource = null;
                ddlCompetences.DataBind();
                ddlCompetences.Items.Insert(0, new ListItem(hdnSelectCompetence.Value, "0"));


                ddlcompetencefilter.DataSource = null;
                ddlcompetencefilter.DataBind();
                ddlcompetencefilter.Items.Insert(0, new ListItem(hdnSelectCompetence.Value, "0"));

            }
        }
        else
        {
            ddlCompetences.DataSource = null;
            ddlCompetences.DataBind();
            ddlCompetences.Items.Insert(0, new ListItem(hdnSelectCompetence.Value, "0"));

            ddlcompetencefilter.DataSource = null;
            ddlcompetencefilter.DataBind();
            ddlcompetencefilter.Items.Insert(0, new ListItem(hdnSelectCompetence.Value, "0"));

        }


        DivisionBM obj1 = new DivisionBM();
        obj1.depIsActive = true;
        obj1.depIsDeleted = false;
        obj1.depCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj1.GetAllDocCategory();

        DataSet ds1 = obj1.ds;
        ViewState["ds1"] = ds1;

        if (ds1 != null)
        {
            if (ds1.Tables[0].Rows.Count > 0)
            {
                var str = Session["Culture"];
                if (Convert.ToString(Session["Culture"]) == "Danish")
                {
                    ds1.Tables[0].Columns["dcName"].ColumnName = "abcd";
                    ds1.Tables[0].Columns["dcNameDN"].ColumnName = "dcName";


                }

                ddldocCatfilter.DataTextField = "dcName";
                ddldocCatfilter.DataValueField = "dcCatId";
                ddldocCatfilter.DataSource = ds1.Tables[0];
                ddldocCatfilter.DataBind();
                ddldocCatfilter.Items.Insert(0, new ListItem(hdnSelectDocCategory.Value, "0"));

                ddlDocumentCategory.DataTextField = "dcName";
                ddlDocumentCategory.DataValueField = "dcCatId";
                ddlDocumentCategory.DataSource = ds1.Tables[0];
                ddlDocumentCategory.DataBind();
                ddlDocumentCategory.Items.Insert(0, new ListItem(hdnSelectDocCategory.Value, "0"));


            }
            else
            {
                ddlDocumentCategory.DataSource = null;
                ddlDocumentCategory.DataBind();
                ddlDocumentCategory.Items.Insert(0, new ListItem(hdnSelectDocCategory.Value, "0"));
                ddldocCatfilter.Items.Insert(0, new ListItem(hdnSelectDocCategory.Value, "0"));

                //ddlcompetencefilter.DataSource = null;
                //ddlcompetencefilter.DataBind();
            }
        }
        else
        {
            ddlDocumentCategory.DataSource = null;
            ddlDocumentCategory.DataBind();
            ddlDocumentCategory.Items.Insert(0, new ListItem(hdnSelectDocCategory.Value, "0"));
            ddldocCatfilter.Items.Insert(0, new ListItem(hdnSelectDocCategory.Value, "0"));


        }
    }

    protected void GetAllDocuments()
    {
        try
        {
            DocumentBM obj = new DocumentBM();
            obj.docIsActive = true;
            obj.GetAllDocument();
            DataSet ds = obj.ds;

            DataView dv = new DataView();
            dv = ds.Tables[0].DefaultView;

            //dv.RowFilter = ("docCompanyId =  '" + Convert.ToInt32(Session["OrgCompanyId"]) + "' and docIsPublic='True'");
            dv.RowFilter = ("docShareToId= '0' AND docCompanyId =  '" + Convert.ToInt32(Session["OrgCompanyId"]) +
                            "' and docIsPublic='True'");
            //      dv.RowFilter = string.Concat("userFullname LIKE '%", Convert.ToString(txtserch.Value), "%'");

            DataTable dtitm = dv.ToTable();
            DataSet ds1 = new DataSet();
            ds1.Tables.Add(dtitm);
            foreach (DataTable table in ds.Tables)
            {
                foreach (DataRow dr in table.Rows)
                {
                    dr["docRepository"] = dr["docRepository"].ToString().Replace("None", GetLocalResourceObject("None.Text").ToString());
                    dr["MinAgo"] = dr["MinAgo"].ToString().Replace("Second", GetLocalResourceObject("Second.Text").ToString());
                    dr["MinAgo"] = dr["MinAgo"].ToString().Replace("Minutes", GetLocalResourceObject("Minutes.Text").ToString());
                    dr["MinAgo"] = dr["MinAgo"].ToString().Replace("Hours", GetLocalResourceObject("Hours.Text").ToString());
                    dr["MinAgo"] = dr["MinAgo"].ToString().Replace("Days", GetLocalResourceObject("Days.Text").ToString());

                }
            }
            if (ds != null)
            {
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    rep_document.DataSource = ds1;
                    rep_document.DataBind();
                }
                else
                {
                    rep_document.DataSource = null;
                    rep_document.DataBind();
                }
            }
            else
            {
                rep_document.DataSource = null;
                rep_document.DataBind();
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void GetAllDivisionByCompanyId()
    {
        DivisionBM obj = new DivisionBM();
        obj.depIsActive = true;
        obj.depIsDeleted = false;
        obj.depCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);


        obj.GetAllDivisionsbyCompanyId();


        DataSet ds = obj.ds;
        if (Convert.ToString(Session["Culture"]) == "Danish")
        {
            ds.Tables[0].Columns["divName"].ColumnName = "abcd";
            ds.Tables[0].Columns["divNameDN"].ColumnName = "divName";

        }
        ViewState["ds"] = ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                string strall = Convert.ToString(GetLocalResourceObject("All.Text"));
                chkListDivision.DataSource = ds.Tables[0];
                chkListDivision.DataTextField = "divName";
                chkListDivision.DataValueField = "divId";
                chkListDivision.DataBind();
                chkListDivision.Items.Insert(0, strall);

                //For Searching-filter
                ddSeachDivision.DataSource = ds.Tables[0];
                ddSeachDivision.DataTextField = "divName";
                ddSeachDivision.DataValueField = "divId";
                ddSeachDivision.DataBind();
                ddSeachDivision.Items.Insert(0, hdnSelectDivision.Value);

            }

        }
    }

    private void GetAllUserCreatedBy()
    {
        var db = new startetkuEntities1();
        try
        {
            var createdBy = Convert.ToString(Session["OrgCreatedBy"]);
            var companyId = Convert.ToInt32(Session["OrgCompanyId"]);

            List<UserMasterView> userList;
            if (Convert.ToString(Session["OrgUserType"]) == "1")
            {
                userList = (from o in db.UserMasters
                            where (o.userCompanyId == companyId || o.userId == companyId)
                            select
                                new UserMasterView() { UserFirstName = o.userFirstName + " " + o.userLastName, UserId = o.userId })
                    .ToList();
            }
            else
            {
                var createby = Convert.ToInt32(createdBy);
                userList = (from o in db.UserMasters
                            where (o.userCreateBy == createdBy || o.userId == companyId || o.userId == createby)
                            select
                                new UserMasterView() { UserFirstName = o.userFirstName + " " + o.userLastName, UserId = o.userId })
                    .ToList();
            }
            if (userList != null && userList.Any())
            {


                ddSearchPerson.DataSource = userList;
                ddSearchPerson.DataTextField = "UserFirstName";
                ddSearchPerson.DataValueField = "UserId";
                ddSearchPerson.DataBind();
            }
            else
            {
                ddSearchPerson.DataSource = null;
                ddSearchPerson.DataBind();
            }
            ddSearchPerson.Items.Insert(0, hdnSelectPerson.Value);

        }
        catch (Exception e)
        {

            ExceptionLogger.LogException(e, Convert.ToInt32(Session["OrgUserId"]),
                "Error In Library function GetAllUserCreatedBy");
        }
    }

    /*----------------------GET Jobtype BY Id -----------------------------------------*/

    protected void GetAllJobTypeByCompanyId()
    {
        JobTypeBM obj = new JobTypeBM();
        obj.jobIsActive = true;
        obj.jobIsDeleted = false;
        obj.jobCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);


        obj.GetAllJobTypeByCompanyId();


        DataSet ds = obj.ds;
        if (Convert.ToString(Session["Culture"]) == "Danish")
        {
            ds.Tables[0].Columns["jobName"].ColumnName = "abcd1";
            ds.Tables[0].Columns["jobNameDN"].ColumnName = "jobName";

        }
        ViewState["ds"] = ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                chkListJobtype.DataSource = ds.Tables[0];
                chkListJobtype.DataTextField = "jobName";
                chkListJobtype.DataValueField = "jobId";
                chkListJobtype.DataBind();

                string strall = Convert.ToString(GetLocalResourceObject("All.Text"));

                chkListJobtype.Items.Insert(0, strall);
                chkListDivision.Items.Insert(0, strall);

                ddSearchJobType.DataSource = ds.Tables[0];
                ddSearchJobType.DataTextField = "jobName";
                ddSearchJobType.DataValueField = "jobId";
                ddSearchJobType.DataBind();
                ddSearchJobType.Items.Insert(0, hdnSelectJob.Value);
            }

        }
    }

    public void GetAllDocuments2()
    {
        try
        {
            DocumentBM obj = new DocumentBM();
            obj.docIsActive = true;
            obj.GetAllDocument();
            DataSet ds = obj.ds;

            DataView dv = new DataView();
            dv = ds.Tables[0].DefaultView;

            // dv.RowFilter = ("docUserId =  '" + Convert.ToInt32(Session["OrgUserId"]) + "'");

            dv.RowFilter = "(docUserId = '" + Convert.ToInt32(Session["OrgUserId"]) + "' or docIsPublic = 'True'  ) ";


            //dv.RowFilter = "(docUserId = '" + Convert.ToInt32(Session["OrgUserId"]) + "' or docIsPublic = 1  ) ";

            dv.RowFilter += "and (docFileName_Friendly LIKE '%" + Convert.ToString(txtsearchDoc.Text.Trim()) +
                            "%' OR docKeywords like '%" + Convert.ToString(txtsearch.Text.Trim()) + "%') ";

            if (!string.IsNullOrWhiteSpace(txtsearchDoc.Text.Trim()))
            {
                dv.RowFilter += "and (docFileName_Friendly LIKE '%" + Convert.ToString(txtsearchDoc.Text.Trim()) +
                                "%') and (docUserId = '" + Convert.ToInt32(Session["OrgUserId"]) +
                                "' or docIsPublic = 'True'  )  ";
            }
            dv.RowFilter += " AND docCompanyId =  '" + Convert.ToInt32(Session["OrgCompanyId"]) + "'";
            // dv.RowFilter = string.Concat("userFullname LIKE '%", Convert.ToString(txtserch.Value), "%'");

            DataTable dtitm = dv.ToTable();
            DataSet ds1 = new DataSet();
            ds1.Tables.Add(dtitm);

            foreach (DataTable table in ds1.Tables)
            {
                foreach (DataRow dr in table.Rows)
                {
                    // dr["docRepository"] = dr["docRepository"].ToString().Replace("None", GetLocalResourceObject("None.Text").ToString());
                    dr["MinAgo"] = dr["MinAgo"].ToString().Replace("Second", GetLocalResourceObject("Second.Text").ToString());
                    dr["MinAgo"] = dr["MinAgo"].ToString().Replace("Minutes", GetLocalResourceObject("Minutes.Text").ToString());
                    dr["MinAgo"] = dr["MinAgo"].ToString().Replace("Hours", GetLocalResourceObject("Hours.Text").ToString());
                    dr["MinAgo"] = dr["MinAgo"].ToString().Replace("Days", GetLocalResourceObject("Days.Text").ToString());

                }
            }

            if (ds1.Tables[0].Rows.Count > 0)
            {
                rep_documentAll.DataSource = ds1;
                rep_documentAll.DataBind();
                lblNoRecordsFound.Visible = false;
            }
            else
            {
                rep_documentAll.DataSource = null;
                rep_documentAll.DataBind();
                lblNoRecordsFound.Visible = true;

            }

        }
        catch (Exception ex)
        {

        }
    }

    public string UserType = "1";

    public void rptItems_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            ScriptManager scriptMan = ScriptManager.GetCurrent(this);
            LinkButton btn = e.Item.FindControl("btn_delete") as LinkButton;
            if (btn != null)
            {
                if (Convert.ToString(Session["OrguserType"]) == "3")
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script",
                        "$('.deletebtn').hide()", false); // btn.Visible = false;
                }
                // btn.Click += btn_delete_Click;
                scriptMan.RegisterAsyncPostBackControl(btn);
            }
            //var checkBox = (CheckBox)e.Item.FindControl("ckbActive");

            ////Do something with your checkbox...
            //checkBox.Checked = true;
        }
    }

    protected void filterform(Boolean division, Boolean DOU, Boolean Full, Boolean Manuals, Boolean Forum)
    {
        DataSet ds = (DataSet)ViewState["Forum"];
        DataView dv = new DataView();
        dv = ds.Tables[0].DefaultView;
        //if (Manuals == true)
        //{
        //    dv.RowFilter = ("forumUserId =  '" + Convert.ToInt32(Session["OrgUserId"]) + "'");
        //}
        //dv.RowFilter = ("fourmCompanyId =  '" + Convert.ToInt32(Session["OrgCompanyId"]) + "'");
        //dv.RowFilter = string.Concat("forumTitle LIKE '%", Convert.ToString(txtsearch.Text.Trim()), "%'");
        if (Manuals == true)
        {
            dv.RowFilter = "fmCompanyId = '" + Convert.ToInt32(Session["OrgCompanyId"]) + "' AND fmCreateByUserId = '" +
                           Convert.ToInt32(Session["OrgUserId"]) + "' AND (fmQuestionName LIKE '%" +
                           Convert.ToString(txtsearch.Text.Trim()) + "%') or (fmTagNames LIKE '%" +
                           txtsearch.Text.Trim() + "%')";
        }
        else
        {
            dv.RowFilter = "fmCompanyId = '" + Convert.ToInt32(Session["OrgCompanyId"]) +
                           "' AND (fmQuestionName LIKE '%" + Convert.ToString(txtsearch.Text.Trim()) +
                           "%') or (fmTagNames LIKE '%" + txtsearch.Text.Trim() + "%')";
        }
        if ((Forum == true) || (Forum == false && Full == true))
        {
            DataTable dtitm = dv.ToTable();
            DataSet ds1 = new DataSet();
            ds1.Tables.Add(dtitm);

            foreach (DataTable table in ds1.Tables)
            {
                foreach (DataRow dr in table.Rows)
                {
                    // dr["docRepository"] = dr["docRepository"].ToString().Replace("None", GetLocalResourceObject("None.Text").ToString());
                    dr["MinAgo"] = dr["MinAgo"].ToString().Replace("Second", GetLocalResourceObject("Second.Text").ToString());
                    dr["MinAgo"] = dr["MinAgo"].ToString().Replace("Minutes", GetLocalResourceObject("Minutes.Text").ToString());
                    dr["MinAgo"] = dr["MinAgo"].ToString().Replace("Hours", GetLocalResourceObject("Hours.Text").ToString());
                    dr["MinAgo"] = dr["MinAgo"].ToString().Replace("Days", GetLocalResourceObject("Days.Text").ToString());

                }
            }

            if (ds1 != null)
            {
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    //rptfrm.DataSource = ds1;
                    //rptfrm.DataBind();

                    rptfrm.DataSource = null;
                    rptfrm.DataBind();
                }
                else
                {
                    rptfrm.DataSource = null;
                    rptfrm.DataBind();
                }
            }
            else
            {
                rptfrm.DataSource = null;
                rptfrm.DataBind();
            }
        }
        else
        {
            rptfrm.DataSource = null;
            rptfrm.DataBind();
        }
    }

    protected void filterdoc(Boolean division, Boolean DOU, Boolean Full, Boolean Manuals, Boolean Forum)
    {
        DataSet ds = (DataSet)ViewState["doc"];
        DataView dv = new DataView();
        dv = ds.Tables[0].DefaultView;

        if (Manuals == true)
        {
            dv.RowFilter = "docShareToId= '0' AND docCompanyId = '" + Convert.ToInt32(Session["OrgCompanyId"]) +
                           "' AND docUserId = '" + Convert.ToInt32(Session["OrgUserId"]) +
                           "' AND docFileName_Friendly LIKE '%" + Convert.ToString(txtsearch.Text.Trim()) + "%'";
        }
        else
        {
            dv.RowFilter = "docShareToId= '0' AND docCompanyId = '" + Convert.ToInt32(Session["OrgCompanyId"]) +
                           "' AND docFileName_Friendly LIKE '%" + Convert.ToString(txtsearch.Text.Trim()) + "%'";
        }
        if ((DOU == true) || (DOU == false && Full == true))
        {
            DataTable dtitm = dv.ToTable();
            DataSet ds1 = new DataSet();
            ds1.Tables.Add(dtitm);

            foreach (DataTable table in ds1.Tables)
            {
                foreach (DataRow dr in table.Rows)
                {
                    dr["docRepository"] = dr["docRepository"].ToString().Replace("None", GetLocalResourceObject("None.Text").ToString());
                    dr["MinAgo"] = dr["MinAgo"].ToString().Replace("Second", GetLocalResourceObject("Second.Text").ToString());
                    dr["MinAgo"] = dr["MinAgo"].ToString().Replace("Minutes", GetLocalResourceObject("Minutes.Text").ToString());
                    dr["MinAgo"] = dr["MinAgo"].ToString().Replace("Hours", GetLocalResourceObject("Hours.Text").ToString());
                    dr["MinAgo"] = dr["MinAgo"].ToString().Replace("Days", GetLocalResourceObject("Days.Text").ToString());

                }
            }

            if (ds1 != null)
            {
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    rptdoc.DataSource = ds1;
                    rptdoc.DataBind();
                    NoRecords.Visible = false;
                }
                else
                {
                    rptdoc.DataSource = null;
                    rptdoc.DataBind();
                    NoRecords.Visible = true;

                }
            }
            else
            {
                rptdoc.DataSource = null;
                rptdoc.DataBind();
                NoRecords.Visible = true;

            }
        }
        else
        {
            rptdoc.DataSource = null;
            rptdoc.DataBind();
            NoRecords.Visible = true;

        }


    }

    protected void filterFullform(Boolean division, Boolean DOU, Boolean Full, Boolean Manuals, Boolean Forum)
    {
        DataSet ds = (DataSet)ViewState["Forum"];
        DataView dv = new DataView();
        dv = ds.Tables[0].DefaultView;
        //if (Manuals == true)
        //{
        //    dv.RowFilter = ("forumUserId =  '" + Convert.ToInt32(Session["OrgUserId"]) + "'");
        //}
        //dv.RowFilter = ("fourmCompanyId =  '" + Convert.ToInt32(Session["OrgCompanyId"]) + "'");
        //dv.RowFilter = string.Concat("forumTitle LIKE '%", Convert.ToString(txtsearch.Text.Trim()), "%'");
        if (ds.Tables[0].Rows.Count < 0) return;
        if (Manuals == true)
        {
            dv.RowFilter = "fourmCompanyId = '" + Convert.ToInt32(Session["OrgCompanyId"]) + "' AND forumUserId = '" +
                           Convert.ToInt32(Session["OrgUserId"]) + "' AND forumTitle LIKE '%" +
                           Convert.ToString(txtsearch.Text.Trim()) + "%'";
        }
        else
        {
            dv.RowFilter = "fourmCompanyId = '" + Convert.ToInt32(Session["OrgCompanyId"]) + "' AND forumTitle LIKE '%" +
                           Convert.ToString(txtsearch.Text.Trim()) + "%'";
        }



        DataTable dtitm = dv.ToTable();
        DataSet ds1 = new DataSet();
        ds1.Tables.Add(dtitm);

        foreach (DataTable table in ds1.Tables)
        {
            foreach (DataRow dr in table.Rows)
            {
                // dr["docRepository"] = dr["docRepository"].ToString().Replace("None", GetLocalResourceObject("None.Text").ToString());
                dr["MinAgo"] = dr["MinAgo"].ToString().Replace("Second", GetLocalResourceObject("Second.Text").ToString());
                dr["MinAgo"] = dr["MinAgo"].ToString().Replace("Minutes", GetLocalResourceObject("Minutes.Text").ToString());
                dr["MinAgo"] = dr["MinAgo"].ToString().Replace("Hours", GetLocalResourceObject("Hours.Text").ToString());
                dr["MinAgo"] = dr["MinAgo"].ToString().Replace("Days", GetLocalResourceObject("Days.Text").ToString());

            }
        }


        if (ds1 != null)
        {
            if (ds1.Tables[0].Rows.Count > 0)
            {
                //rptfrm.DataSource = ds1;
                //rptfrm.DataBind();

                rptfrm.DataSource = null;
                rptfrm.DataBind();
            }
            else
            {
                rptfrm.DataSource = null;
                rptfrm.DataBind();
            }
        }
        else
        {
            rptfrm.DataSource = null;
            rptfrm.DataBind();
        }

    }

    protected void filterFulldoc(Boolean division, Boolean DOU, Boolean Full, Boolean Manuals, Boolean Forum)
    {
        DataSet ds = (DataSet)ViewState["doc"];
        DataView dv = new DataView();
        dv = ds.Tables[0].DefaultView;
        if (Manuals == true)
        {
            //dv.RowFilter = ("docUserId =  '" + Convert.ToInt32(Session["OrgUserId"]) + "'");
        }
        //dv.RowFilter = ("docCompanyId =  '" + Convert.ToInt32(Session["OrgCompanyId"]) + "'");
        // dv.RowFilter = string.Concat("docTitle LIKE '%", Convert.ToString(txtsearch.Text.Trim()), "%'") + ("docCompanyId =  '" + Convert.ToInt32(12) + "'");
        // dv.RowFilter = string.Concat("docTitle LIKE '%", Convert.ToString(txtsearch.Text.Trim()), "%'");

        //  dv.RowFilter = "docCompanyId = '" + Convert.ToInt32(Session["OrgUserId"]) + "' AND docUserId > '" + Convert.ToInt32(Session["OrgUserId"]) + "' AND docTitle LIKE '" +Convert.ToString(txtsearch.Text.Trim())+ "'";
        //  "gender LIKE '" +filterGender+ "'"
        // DataView.RowFilter = "SalesPerson = 'Bill' AND SaleDate > '1/1/1995'" 
        if (Manuals == true)
        {
            dv.RowFilter = "docShareToId= '0' AND docCompanyId = '" + Convert.ToInt32(Session["OrgCompanyId"]) +
                           "' AND docUserId = '" + Convert.ToInt32(Session["OrgUserId"]) +
                           "' AND docFileName_Friendly LIKE '%" + Convert.ToString(txtsearch.Text.Trim()) + "%'";
        }
        else
        {
            dv.RowFilter = "docShareToId= '0' AND docCompanyId = '" + Convert.ToInt32(Session["OrgCompanyId"]) +
                           "' AND docFileName_Friendly LIKE '%" + Convert.ToString(txtsearch.Text.Trim()) + "%'";
        }

        DataTable dtitm = dv.ToTable();
        DataSet ds1 = new DataSet();
        ds1.Tables.Add(dtitm);
        if (ds1 != null)
        {
            if (ds1.Tables[0].Rows.Count > 0)
            {
                rptdoc.DataSource = ds1;
                rptdoc.DataBind();
                NoRecords.Visible = false;
            }
            else
            {
                rptdoc.DataSource = null;
                rptdoc.DataBind();
                NoRecords.Visible = true;
            }
        }
        else
        {
            rptdoc.DataSource = null;
            rptdoc.DataBind();
            NoRecords.Visible = true;
        }


    }

    protected void Getserchdoc()
    {
        try
        {
            DocumentBM obj = new DocumentBM();
            obj.docIsActive = true;
            obj.docIsDeleted = true;
            obj.GetAllDocument();
            DataSet ds = obj.ds;
            ViewState["doc"] = ds;
        }
        catch (Exception ex)
        {

        }
    }

    protected void GetserchForum()
    {
        try
        {
            ForumMasterBM obj = new ForumMasterBM();
            obj.fmIsActive = true;
            obj.fmIsDeleted = false;
            obj.fmCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            // obj.userCreateBy = Convert.ToString(Session["OrgUserId"]);
            obj.GetAllForum();
            DataSet ds = obj.ds;
            ViewState["Forum"] = ds;

        }
        catch (Exception ex)
        {

        }
    }

    public static string InsertuserByWebService(string name, string desc)
    {
        ForumBM obj = new ForumBM();
        obj.forumTitle = name;
        obj.forumDescripiton = desc;
        obj.forumUserId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        obj.forumCreatedDate = DateTime.Now;
        obj.fourmIsActive = true;
        obj.forumIsDeleted = false;
        obj.fourmCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
        obj.fourmCreateBy = 0;
        obj.InsertForum();
        //  DataSet ds = obj.ds;
        if (obj.ReturnBoolean == true)
        {
            Organisation_Library s = new Organisation_Library();
            s.GetserchForum();
            s.Getserchdoc();
            return "success";
        }

        else
        {
            //lblMsg.Text = CommonModule.msgProblemInsertRecord;
        }
        return "fail";
    }

    public void Insertuser()
    {
        ForumBM obj = new ForumBM();
        obj.forumTitle = txtName.Text;
        obj.forumDescripiton = txtdes.Text;
        obj.forumUserId = Convert.ToInt32(Session["OrgUserId"]);
        obj.forumCreatedDate = DateTime.Now;
        obj.fourmIsActive = true;
        obj.forumIsDeleted = false;
        obj.fourmCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.fourmCreateBy = 0;
        obj.InsertForum();
        //  DataSet ds = obj.ds;
        if (obj.ReturnBoolean == true)
        {
            clear();
        }

        else
        {
            lblMsg.Text = CommonModule.msgProblemInsertRecord;
        }
    }

    protected void clear()
    {
        txtName.Text = "";
        txtdes.Text = "";
        lblMsg.Text = "";
    }

    #endregion


    #region Button Event

    protected void btnsubmit_click(object sender, EventArgs e)
    {
        Insertuser();
        CheckClicked();
    }

    protected void btn_delete_Click(object sender, EventArgs e)
    {
        LinkButton lb = (LinkButton)sender;
        int id = Convert.ToInt32(lb.CommandName);
        DocumentBM obj = new DocumentBM();
        obj.docId = id;
        obj.DeleteDocumentById();
        GetAllDocuments();
        GetAllDocuments2();
    }

    protected void txtsearch_TextChanged(object sender, EventArgs e)
    {
        //Literal6.Text = string.Format("{0} : {1}", Literal6.Text, txtsearch.Text);
        //Boolean division = chkdivision.Checked;
        //Boolean DOU = chkDOU.Checked;
        //Boolean Full = chkFull.Checked;
        //Boolean Manuals = chkManuals.Checked;
        //Boolean Forum = chkForum.Checked;
        //if (DOU == false && Full == false && Forum == false)
        //{
        //    filterFullform(division, DOU, Full, Manuals, Forum);
        //    filterFulldoc(division, DOU, Full, Manuals, Forum);
        //}
        //else
        //{
        //    filterform(division, DOU, Full, Manuals, Forum);
        //    filterdoc(division, DOU, Full, Manuals, Forum);
        //}

        FilterResult();

        //bindrptdoc();
        //bindrptfrm();

        //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>AnotherFunction();</script>", false);

    }

    protected void txtsearchDoc_TextChanged(object sender, EventArgs e)
    {
        // GetAllDocuments2();
        FilterResult();

        //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "$(".deletebtn").hide(), false);

    }

    protected void btnsubmitdoc_click(object sender, EventArgs e)
    {
        try
        {


            String filePath = "";
            DocumentBM obj = new DocumentBM();

            var docFileNameFriendly = txtFileName.Text;
            var docAttachmentName = string.Empty;

            obj.docDescription = txtKeywords.Text;

            if (FileUpload1.HasFile)
            {
                filePath = System.DateTime.Now.ToString("MMddyyhhmmss") + "_" + FileUpload1.PostedFile.FileName;
                FileUpload1.SaveAs(Server.MapPath("../") + "Log/upload/Document/" +
                                   System.IO.Path.GetFileName(CleanFileName(filePath)));
                docAttachmentName = System.IO.Path.GetFileName(CleanFileName(filePath));

            }
            var docFileNameInSystem = CleanFileName(docAttachmentName);
            docAttachmentName = CleanFileName(docAttachmentName);
            string docTypes = chkIsCompDevDoc.SelectedItem.Value;
            var userId = Convert.ToInt32(Session["OrgUserId"]);

            var docCreatedDt = DateTime.Now;
            var docCompnyId = Convert.ToInt32(Session["OrgCompanyId"]);
            obj.docDepId = 0;
            obj.docApprovedStatus = false;




            DateTime docUpdatedDate = new DateTime();

            string docCreatedBy = string.Empty;
            int docDepIds = 0;
            string docTitle = txtFileName.Text;
            bool docApprovedStatus = false;

            //
            var aa = new List<string>();
            foreach (ListItem item in chkListDivision.Items)
            {
                if (item.Selected)
                {
                    aa.Add(item.Value);
                    //string selectedValue = item.Value;
                }
            }
            string divisionIds = chkListDivision.SelectedItem.Value;
            aa.Clear();
            //
            foreach (ListItem item in chkListJobtype.Items)
            {
                if (item.Selected)
                {
                    aa.Add(item.Value);
                    //string selectedValue = item.Value;
                }
            }
            string jobTypeIds = chkListJobtype.SelectedItem.Value;


            var isPublic = rdoPrivatePublic.SelectedValue == "Public";

            var repository = "None";
            if (chkIsCompDevDoc.SelectedItem.Value != "General")
            {
                var Level = rdoLevel.Text;
                repository = rdoLevel.Text;
            }


            var keywords = txtKeywords.Text;



            var txtDescriptions = txtDescription.Text;
            Int32 docCatId = 0;
            var docCategory = "";
            if (ddlCompetences.SelectedIndex == 0)
            {
                obj.doccompetence = String.Empty;
            }
            else
            {
                obj.doccompetence = ddlCompetences.SelectedItem.Value;


            }

            if (ddlDocumentCategory.SelectedIndex == 0)
            {
                docCatId = 0;
                docCategory = String.Empty;

            }
            else
            {
                docCatId = Convert.ToInt32(ddlDocumentCategory.SelectedItem.Value);
                docCategory = ddlDocumentCategory.SelectedItem.Text;
            }

            obj.InsertDocument(docFileNameFriendly, docFileNameInSystem, docAttachmentName, docTypes, userId, docCreatedDt,
                docUpdatedDate, false, true, docCompnyId, docCreatedBy, docDepIds, docTitle, string.Empty, docApprovedStatus,
                divisionIds, jobTypeIds, isPublic, keywords, repository, txtDescriptions, 0, docCatId, docCreatedBy);

            var lastRecId = obj.ds.Tables[0].Rows[0].ItemArray[0];

            if (obj.ReturnBoolean == true)
            {
                // Response.Redirect("OrgDepartmentsList.aspx?msg=ins");
            }
            GetAllDocuments();
            GetAllDocuments2();


            InsertUpdateLog(Convert.ToInt32(lastRecId));
            ClearField();
            //makesa round-trip of the same page
            Response.Redirect(Request.Url.AbsolutePath);
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "btnsubmitdoc_click");
        }

    }

    private static string CleanFileName(string fileName)
    {


        var s =
            fileName.Replace(' ', '_')
                .Replace('^', '_')
                .Replace('%', '_')
                .Replace('*', '_')
                .Replace('(', '_')
                .Replace(')', '_')
                .Replace('{', '_')
                .Replace('}', '_')
                .Replace('@', '_')
                .Replace('$', '_')
                .Replace('+', '_')
                .Replace('-', '_')
                .Replace('`', '_')
                .Replace('?', '_')
                .Replace('&', '_')
                .Replace('#', '_');

        return s;
    }

    public void InsertUpdateLog(int docId)
    {
        LogMasterLogic obj = new LogMasterLogic();
        obj.LogDescription = string.Format("{0} has uploaded {1} document ", Convert.ToString(Session["OrgUserName"]),
            txtFileName.Text); // User log description..

        var userID = Convert.ToInt32(Session["OrgUserId"]);

        //var isPointAllow = rdoPrivatePublic.SelectedValue == "Public";
        var isPointAllow = true;

        var point = LogMasterLogic.InsertUpdateLogParam(userID, Common.ALP(), Common.PublicDocUpload(), obj.LogDescription,
            isPointAllow, 0, docId);

        if (isPointAllow)
            PointBM.InsertUpdatePoint(userID, 0, point);

    }

    public void InsertUpdateLogForRearDoc(int docId)
    {

        LogMasterLogic obj = new LogMasterLogic();

        obj.LogDescription = string.Format("{0} has Read or Downloaded {1} document ", Convert.ToString(Session["OrgUserName"]), db.Documents.Find(docId).docFileName_Friendly);

        var userID = Convert.ToInt32(Session["OrgUserId"]);

        //var isPointAllow = rdoPrivatePublic.SelectedValue == "Public";
        var isPointAllow = true;

        var point = LogMasterLogic.InsertUpdateLogParam(userID, Common.PLP(), Common.ALPPointForReadDoc(), obj.LogDescription,
            isPointAllow, 0, docId);

        if (isPointAllow)

            PointBM.InsertUpdatePoint(userID, point, 0);

    }

    private void ClearField()
    {
        txtFileName.Text = string.Empty;
        txtKeywords.Text = string.Empty;
        foreach (ListItem item in chkListDivision.Items)
        {
            item.Selected = false;
        }
        foreach (ListItem item in chkListJobtype.Items)
        {
            item.Selected = false;
        }
        ddRepository.SelectedIndex = 0;
        txtDescription.Text = string.Empty;
    }

    protected void Check_Clicked(Object sender, EventArgs e)
    {
        CheckClicked();

    }

    protected void CheckClicked()
    {
        Boolean division = chkdivision.Checked;
        Boolean DOU = chkDOU.Checked;
        Boolean Full = chkFull.Checked;
        Boolean Manuals = chkManuals.Checked;
        Boolean Forum = chkForum.Checked;
        Getserchdoc();
        GetserchForum();
        if (DOU == false && Full == false && Forum == false)
        {
            filterFullform(division, DOU, Full, Manuals, Forum);
            filterFulldoc(division, DOU, Full, Manuals, Forum);
        }
        else
        {
            filterform(division, DOU, Full, Manuals, Forum);
            filterdoc(division, DOU, Full, Manuals, Forum);
        }
    }

    protected void btnBack_click(object sender, EventArgs e)
    {
        switch (Convert.ToInt32(Session["OrguserType"]))
        {
            case 2:
                Response.Redirect("Manager-dashboard.aspx");
                break;
            case 1:
                Response.Redirect("Organisation-dashboard.aspx");
                break;
            default:
                Response.Redirect("Employee_dashboard.aspx");
                break;
        }
    }

    #endregion

    #region webservice

    [WebMethod(EnableSession = true)]
    // [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string BtnSubmitAJAX(string txtName, string txtDesc)
    {
        Organisation_Library s = new Organisation_Library();
        return InsertuserByWebService(txtName, txtDesc);
        // s.CheckClicked();

    }


    [WebMethod(EnableSession = true)]
    // [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string DeleteThisDoc(int uId, string deleteText)
    {
        var db = new startetkuEntities1();
        var userId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        var compnayId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);

        try
        {
            var doc =
                (from o in db.Documents
                 where o.docId == uId && (o.docUserId == userId || o.docCompanyId == userId)
                 select o).FirstOrDefault();

            if (doc != null)
            {
                db.Documents.Remove(doc);
                db.SaveChanges();
                // return "Your document has been deleted successfully.";
            }
            else
            {

                // return "Sorry! You can not delete this document.";
                return deleteText;
            }
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            db.Dispose();
        }

        //if (Convert.ToString(HttpContext.Current.Session["OrguserType"]) == "3")
        //{
        //    return "Employee can not delete document";
        //}
        //DocumentBM obj = new DocumentBM();
        //obj.docId = uId;
        //obj.DeleteDocumentById();
        //GetAllDocuments();
        //GetAllDocuments2();
        return "True";
    }

    [WebMethod(EnableSession = true)]
    // [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GeneratePoint(int docId)
    {
        var db = new startetkuEntities1();
        Organisation_Library lib = new Organisation_Library();
        var userId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        var docData = (from doc in db.DocMasters where doc.docreadId == docId && doc.docUserId == userId select doc.docMasterId).ToList();
        if (docData.Count <= 0)
        {
            DocMaster dm = new DocMaster();
            dm.docreadId = docId;
            dm.docUserId = userId;
            db.DocMasters.Add(dm);
            db.SaveChanges();
            lib.InsertUpdateLogForRearDoc(docId);

        }
        return "True";
    }

    #endregion



    #region Culture

    protected override void InitializeCulture()
    {
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Danish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //            Session["DateFormat"]= resds.Tables[0].Rows[i]["resDateFormat"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName))
                SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }

    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }

    #endregion

    protected void ddlcompetencefilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        //FilterResult();
        //bindrptfrm();
    }

    protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
    {
        // bindrptdoc();
        //bindrptfrm();
    }

    public void bindrptdoc()
    {
        DataSet ds = (DataSet)ViewState["doc"];
        DataView dv = new DataView();
        dv = ds.Tables[0].DefaultView;

        DataTable dtitm = dv.ToTable();
        DataSet ds1 = new DataSet();

        ds1.Tables.Add(dtitm);
        if (ds1 != null)
        {
            if (ds1.Tables[0].Rows.Count > 0)
            {
                rptdoc.DataSource = ds1;
                rptdoc.DataBind();
                NoRecords.Visible = false;
            }
            else
            {
                rptdoc.DataSource = null;
                rptdoc.DataBind();
                NoRecords.Visible = true;
            }
        }
        else
        {
            rptdoc.DataSource = null;
            rptdoc.DataBind();
            NoRecords.Visible = true;
        }

    }

    public void bindrptfrm()
    {

        DataSet ds = (DataSet)ViewState["Forum"];
        DataView dv = new DataView();


        if (ds != null && ds.Tables.Count > 0)
        {
            dv = ds.Tables[0].DefaultView;
            DataTable dtitm = dv.ToTable();
            DataSet ds1 = new DataSet();
            ds1.Tables.Add(dtitm);
            if (ds1 != null)
            {
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    //rptfrm.DataSource = ds1;
                    //rptfrm.DataBind();

                    rptfrm.DataSource = null;
                    rptfrm.DataBind();
                }
                else
                {
                    rptfrm.DataSource = null;
                    rptfrm.DataBind();
                }
            }
            else
            {
                rptfrm.DataSource = null;
                rptfrm.DataBind();
            }
        }

    }

    #region SearchFilter

    protected void ddSearchJob_Change(object sender, EventArgs e)
    {
        FilterResult();
    }

    private void FilterResult()
    {
        var db = new startetkuEntities1();
        try
        {
            var documentList = DocumentBM.GetAllDocumentsLinq(Convert.ToInt32(Session["OrgCompanyId"]));


            // LEVEL

            if (!string.IsNullOrEmpty(txtsearch.Text))
            {

                documentList =
                    documentList.Where(o => (o.docFileName_Friendly != null && o.docFileName_Friendly.ToLower().Contains(txtsearch.Text.ToLower()))
                                        || (o.docKeywords != null && o.docKeywords.ToLower().Contains(txtsearch.Text.ToLower()))
                                        || (o.docDescription != null && o.docDescription.ToLower().Contains(txtsearch.Text.ToLower()))).ToList();

            }

            // LEVEL
            var ddSearchLevelV = Convert.ToString(ddSearchLevel.SelectedItem.Value);
            if (ddSearchLevelV != "0")
            {
                documentList = documentList.Where(o => o.docRepository == ddSearchLevelV).ToList();

            }


            //Date - From

            if (!string.IsNullOrEmpty(txtFromDate.Text) && txtFromDate.Text != @"__/__/20__")
            {
                var fromDt = Convert.ToDateTime(txtFromDate.Text);
                documentList = documentList.Where(o => o.docCreatedDate >= fromDt).ToList();

            }

            //Date - To 

            if (!string.IsNullOrEmpty(txtTodate.Text) && txtTodate.Text != @"__/__/20__")
            {
                var toDt = Convert.ToDateTime(txtTodate.Text);
                documentList = documentList.Where(o => o.docCreatedDate <= toDt).ToList();

            }
            string strall = Convert.ToString(GetLocalResourceObject("All.Text"));
            // Job Type
            var ddSearchJobId = Convert.ToString(ddSearchJobType.SelectedItem.Value);
            if (ddSearchJobId != hdnSelectJob.Value)
            {

                documentList = documentList.Where(o => o.docJobTypeIds == ddSearchJobId || o.docJobTypeIds == "All").ToList();

            }

            //ddSeachDivision
            var divId = Convert.ToString(ddSeachDivision.SelectedItem.Value);
            if (divId != hdnSelectDivision.Value)
            {
                documentList = documentList.Where(o => o.docDivisionIds == divId || o.docDivisionIds == "All").ToList();

            }

            //ddSeachDivision
            var docId = Convert.ToInt32(ddldocCatfilter.SelectedItem.Value);
            if (docId != 0)
            {
                documentList = documentList.Where(o => o.docCatId == docId).ToList();

            }



            //Person
            var personId = Convert.ToString(ddSearchPerson.SelectedItem.Value);
            if (personId != hdnSelectPerson.Value)
            {
                var intPersonId = Convert.ToInt32(personId);
                documentList = documentList.Where(o => o.docUserID == intPersonId).ToList();
            }

            //Competence
            var compid = Convert.ToString(ddlcompetencefilter.SelectedItem.Value);
            if (compid != hdnSelectCompetence.Value && compid != "0")
            {

                documentList = documentList.Where(o => o.doccompetence == compid).ToList();
            }

            //SORT
            var ddSearchSortVal = Convert.ToString(ddSearchSort.SelectedItem.Value);
            if (ddSearchSortVal == "asc")
            {
                documentList = documentList.OrderByDescending(o => o.docCreatedDate).ToList();
            }
            else
            {
                documentList = documentList.OrderBy(o => o.docCreatedDate).ToList();

            }

            if (documentList != null && documentList.Any())
            {
                rptdoc.DataSource = documentList;
                rptdoc.DataBind();
                NoRecords.Visible = false;
            }
            else
            {
                rptdoc.DataSource = null;
                rptdoc.DataBind();
                NoRecords.Visible = true;
            }

            upFilter.Update();
        }
        catch (Exception e)
        {

            ExceptionLogger.LogException(e);
        }
        finally
        {

            db.Dispose();
        }


    }

    #endregion



    protected void btnFilter_OnClick(object sender, EventArgs e)
    {
        FilterResult();
        upFilter.Update();
    }

    protected void btnresetfilter_OnClick(object sender, EventArgs e)
    {
        Response.Redirect("Library.aspx");
    }

}

