﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true" CodeFile="EmailSetting.aspx.cs" Inherits="Organisation_EmailSetting" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .col-md-6
        {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   
    <div class="col-md-6">
        <div class="heading-sec">
            <h1>
                <asp:Literal ID="Literal1" runat="server" Text="Email Setting"  
                    EnableViewState="False" meta:resourcekey="Literal1Resource1" />
                <i><span runat="server" id="Setting"></span></i>
            </h1>
        </div>
    </div>
    <br />
    <br />
    <div class="col-md-6 range ">
    </div>
    <div class="col-md-12">
     <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" 
            meta:resourcekey="lblMsgResource1" ></asp:Label>
    <asp:Label runat="server" ID="Label1" CssClass="commonerrormsg" 
            meta:resourcekey="Label1Resource1"></asp:Label>
        <div class="chat-widget widget-body" style="background: #fff;" id="edit" runat="server">
            <div class="chat-widget-head yellow">
                <h4 style="margin: -6px 0px 0px;">
                   <%-- Email Setting--%>
                    <asp:Literal ID="Literal6" runat="server" Text="Email Setting"  
                    EnableViewState="False" meta:resourcekey="Literal1Resource1" /></h4>
            </div>
            <div class="indicatesRequireFiled">
                <i>*
                    <asp:Literal ID="Literal2" runat="server" meta:resourcekey="Indicatesrequiredfield"
                        EnableViewState="False" />
                </i>
            </div>
            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label">
                        <asp:Literal ID="Literal3" runat="server" Text="Email Sender" 
                        EnableViewState="False" meta:resourcekey="Literal3Resource1" />
                        :<span class="starValidation">*</span></label>
                    <asp:TextBox runat="server" ID="txtsEmail" MaxLength="80" onkeyup="run(this)" 
                        CssClass="input-style" meta:resourcekey="txtsEmailResource1"
                         /><br />
                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" CssClass="commonerrormsg"
                        ControlToValidate="txtsEmail" Display="Dynamic" ValidationGroup="chk" 
                        ErrorMessage="Please Enter Email." meta:resourcekey="rfvEmailResource1"
                        ></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="txtEmail" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        ControlToValidate="txtsEmail" CssClass="commonerrormsg" Display="Dynamic" ValidationGroup="chk"
                        ErrorMessage="Please Enter Valid Email." 
                        meta:resourcekey="txtEmailResource1" ></asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label">
                        <asp:Literal ID="Literal4" runat="server" Text="password" 
                        EnableViewState="False" meta:resourcekey="Literal4Resource1" />:<span
                            class="starValidation">*</span></label>
                    <asp:TextBox runat="server" ID="txtpassword" MaxLength="80" onkeyup="run(this)" CssClass="input-style"
                         TextMode="Password" meta:resourcekey="txtpasswordResource1"/><br />
                    <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                        TargetControlID="txtlname" Enabled="True" ValidChars=" ">
                    </cc1:FilteredTextBoxExtender>--%>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtpassword"
                        ErrorMessage="Please Enter Password." CssClass="commonerrormsg" Display="Dynamic"
                        ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator1Resource1"></asp:RequiredFieldValidator>
                </div>
            </div>
             <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label">
                          <asp:Literal ID="Literal7" runat="server" Text="Confirm Password" 
                        enableviewstate="False" meta:resourcekey="Literal7Resource1"/>:<span class="starValidation">*</span></label>
                     <asp:TextBox ID="txtConfirmPassword" type="text" class="input-style" runat="server"
                        MaxLength="20" TextMode="Password" 
                        meta:resourcekey="txtConfirmPasswordResource1" /><br />
                    <asp:CompareValidator ID="cvPasswordNotMatch" ControlToValidate="txtConfirmPassword"
                        ControlToCompare="txtpassword" runat="server" CssClass="commonerrormsg" Display="Dynamic"
                        ValidationGroup="chk" 
                        ErrorMessage="Password and confirm password does not match." meta:resourcekey="cvPasswordNotMatchResource1" 
                       ></asp:CompareValidator>
                    <asp:RequiredFieldValidator ID="rfvConfirmPassword" runat="server" CssClass="commonerrormsg"
                        ControlToValidate="txtConfirmPassword" Display="Dynamic" ErrorMessage="Confirm password is required."
                        ValidationGroup="chk" meta:resourcekey="rfvConfirmPasswordResource1" ></asp:RequiredFieldValidator>
                </div>
            </div>

            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label">
                        Host:<span class="starValidation">*</span></label>
                    <asp:TextBox runat="server" ID="txthost" MaxLength="80" onkeyup="run(this)" 
                        CssClass="input-style" meta:resourcekey="txthostResource1"
                        /><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txthost"
                        ErrorMessage="Please Enter Host." CssClass="commonerrormsg" Display="Dynamic"
                        ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator3Resource1"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label" style="width: 100%">
                        <asp:Literal ID="Literal5" runat="server" Text="Port" 
                        EnableViewState="False" meta:resourcekey="Literal5Resource1" />:</label>
                    <asp:TextBox runat="server" ID="txtport" MaxLength="10" onkeyup="run(this)" 
                        CssClass="input-style" meta:resourcekey="txtportResource1" /><br />
                      <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"
                        TargetControlID="txtport" Enabled="True" ValidChars=" ">
                    </cc1:FilteredTextBoxExtender>
                    <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" CssClass="commonerrormsg"
                        ControlToValidate="txtport" Display="Dynamic" ValidationGroup="chk" ErrorMessage="Please Enter Port."></asp:RequiredFieldValidator>--%>
                </div>
            </div>
             <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label">
                     <asp:Literal ID="Literal8" runat="server" 
                        enableviewstate="False" meta:resourcekey="DisplayName"/>:<span class="starValidation">*</span></label>
                    <asp:TextBox runat="server" ID="txtdisplayname" MaxLength="80" 
                        onkeyup="run(this)" CssClass="input-style" meta:resourcekey="txtdisplaynameResource1"
                        /><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtdisplayname"
                        ErrorMessage="Please Enter  Display Name." CssClass="commonerrormsg" Display="Dynamic"
                        ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator2Resource1"></asp:RequiredFieldValidator>
                </div>
            </div>
            <%--<div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label" style="width: 100%">
                        <asp:Literal ID="Literal6" runat="server" Text="Image:" EnableViewState="false" /></label>
                </div>
                <div style="width: 100%; float: left;">
                    <label class="c-label">
                        <asp:Image ID="img_profile" CssClass="ProfileImage" ImageUrl="~/Organisation/images/sign-in1.jpg"
                            runat="server" Style="border: 0px;" />
                    </label>
                    <div style="margin-top: 80px; margin-left: 170px;">
                        <asp:FileUpload ID="flupload1" runat="server">
                        </asp:FileUpload>
                        <asp:RegularExpressionValidator ID="reFile1" runat="server" ControlToValidate="flupload1"
                            CssClass="legend" Display="Dynamic" ErrorMessage="Please upload only .gif | .jpeg | .jpg   | .png image."
                            ValidationExpression="^.*\.(JPG|jpg|jpeg|PNG|png|JPEG)$" ForeColor="Red" ValidationGroup="chk1"
                            ></asp:RegularExpressionValidator>
                    </div>
                </div>
            </div>--%>
            <div class="modal-footer" style="border: 0px;">
                <asp:Button runat="server" ID="btnsubmit" class="btn btn-primary yellow" Text="Save"
                    ValidationGroup="chk" Style="border-radius: 5px;" 
                    OnClick="btnsubmit_click" meta:resourcekey="btnsubmitResource1"></asp:Button>
                <asp:Button runat="server" ID="btnCancel" class="btn btn-primary black" 
                    Text="Cancel" OnClick="btnCancel_click" meta:resourcekey="btnCancelResource1">
                </asp:Button>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            //  alert();
            setTimeout(function () {
                //ExampanCollapsMenu();
                //ExampanCollapssubMenu();
                SetExpandCollapse();
                // tabMenuClick();
               // $(".OrgJobTypeList").addClass("active_page");
            }, 500);
        });
    </script>
</asp:Content>
