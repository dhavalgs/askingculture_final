﻿using starteku_BusinessLogic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Globalization;

public partial class Organisation_Setting : System.Web.UI.Page
{
    #region Page Event
    DataTable dt = new DataTable();
    int queid = 0;
    DataSet ds;
    public static int ans = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            //Settings.InnerHtml = "Welcome   " + Convert.ToString(Session["OrgUserName"]) + "!";
            Settings.InnerHtml = GetLocalResourceObject("Welcome.Text").ToString() + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            GetUserSettingById();

        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion

    #region Methods
    private void GetUserSettingById()
    {
        try
        {
            UserBM obj = new UserBM();
            obj.SetUserId = Convert.ToInt32(Session["OrgUserID"]);
            obj.GetUserSettingById();
            DataSet ds = obj.ds;
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (Convert.ToBoolean(ds.Tables[0].Rows[0]["SetNewFileUploadwarning"]))
                {
                    rl_Newfile.SelectedIndex = 0;
                }
                else
                {
                    rl_Newfile.SelectedIndex = 1;
                }
                //---------------------------------------------------------------------------------------------------------------------------
                if (Convert.ToBoolean(ds.Tables[0].Rows[0]["SetCompetenceLevelsChange"]))
                {
                    rl_levels_Change.SelectedIndex = 0;
                }
                else
                {
                    rl_levels_Change.SelectedIndex = 1;
                }
                //---------------------------------------------------------------------------------------------------------------------------
                if (Convert.ToBoolean(ds.Tables[0].Rows[0]["SetKnowledgeSharingRequests"]))
                {
                    rl_Knowledge_sharing.SelectedIndex = 0;
                }
                else
                {
                    rl_Knowledge_sharing.SelectedIndex = 1;
                }
                //---------------------------------------------------------------------------------------------------------------------------
                if (Convert.ToBoolean(ds.Tables[0].Rows[0]["SetInvitationNotification"]))
                {
                    rl_Invitation_Notification.SelectedIndex = 0;
                }
                else
                {
                    rl_Invitation_Notification.SelectedIndex = 1;
                }
                //---------------------------------------------------------------------------------------------------------------------------
                if (Convert.ToBoolean(ds.Tables[0].Rows[0]["SetMessageNotification"]))
                {
                    rl_Message_Notification.SelectedIndex = 0;
                }
                else
                {
                    rl_Message_Notification.SelectedIndex = 1;
                }
                //---------------------------------------------------------------------------------------------------------------------------
                if (Convert.ToBoolean(ds.Tables[0].Rows[0]["SetActivityNotification"]))
                {
                    rl_Activity_Notificatoins.SelectedIndex = 0;
                }
                else
                {
                    rl_Activity_Notificatoins.SelectedIndex = 1;
                }
            }
        }
        catch (Exception ex)
        {

        }
    }
    #endregion

    #region Button
    protected void btn_save_change_Click(object sender, EventArgs e)
    {
        try
        {
            UserBM obj = new UserBM();
            obj.SetUserId = Convert.ToInt32(Session["OrgUserID"]);
            obj.SetNewFileUploadwarning = rl_Newfile.SelectedIndex == 0 ? true : false;
            obj.SetCompetenceLevelsChange = rl_levels_Change.SelectedIndex == 0 ? true : false;
            obj.SetKnowledgeSharingRequests = rl_Knowledge_sharing.SelectedIndex == 0 ? true : false;
            obj.SetInvitationNotification = rl_Invitation_Notification.SelectedIndex == 0 ? true : false;
            obj.SetMessageNotification = rl_Message_Notification.SelectedIndex == 0 ? true : false;
            obj.SetActivityNotification = rl_Activity_Notificatoins.SelectedIndex == 0 ? true : false;
            obj.SetUpdatedDate = DateTime.Now;
            obj.UpdateUserSetting();
            if (obj.ReturnBoolean)
            {
                if (Convert.ToString(Session["OrguserType"]) == "3")
                {
                    Response.Redirect("Employee_dashboard.aspx", false);
                }
                else if (Convert.ToString(Session["OrguserType"]) == "2")
                {
                    Response.Redirect("Manager-dashboard.aspx", false);
                }
                else
                {
                    Response.Redirect("Organisation-dashboard.aspx", false);
                }
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if (Convert.ToString(Session["OrguserType"]) == "3")
        {
            Response.Redirect("Employee_dashboard.aspx");
        }
        else if ((Convert.ToString(Session["OrguserType"]) == "1"))
        {
            Response.Redirect("Organisation-dashboard.aspx");
        }
        else if ((Convert.ToString(Session["OrguserType"]) == "2"))
        {
            Response.Redirect("Manager-dashboard.aspx");
        }
    }
    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Denish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion
}