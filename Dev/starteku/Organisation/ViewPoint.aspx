﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true" CodeFile="ViewPoint.aspx.cs" Inherits="Organisation_ViewPoint" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
<style type="text/css">
        .yellow
        {
            border-radius: 0px;
        }
        .tableHeaderCell {
            font-size: 18px;
            height: 55px;
        }
    </style>
      <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.js"></script>

    <script>
        $(document).ready(function () {
            SetExpandCollapse();
            setTimeout(function () {
               // ExampanCollapsMenu();

                $(".add_employee").addClass("active_page");
            }, 500);
        });
      </script>
</asp:Content>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="col-md-6">
        <div class="heading-sec">
            <h1>
            
               <%-- <%= CommonMessages.ViewPoint%>--%>
                <asp:Literal ID="Literal1" runat="server" meta:resourcekey="ViewPoint" enableviewstate="false"/>
                <i><span runat="server" id="Employee"></span></i>
               
            </h1>
        </div>
    </div>
    <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="lll">
        <ContentTemplate>
            <div class="col-md-12" style="margin-top: 20px;">
                <div id="graph-wrapper">
                    <div class="col-md-12">
                        <br />
                        <div class="chart-tab">
                            <div id="tabs-container">
                                 <asp:HiddenField ID="hdnTotal" meta:resourcekey="resourceTotal" runat="server"/>
                                 <asp:DropDownList ID="ddlDepartment" runat="server" OnSelectedIndexChanged="ddlDepartment_Change" Style="width: 25%; height: 32px;float:right;
                                    display: inline; margin-bottom: 20px;" CssClass="form-control" AutoPostBack="true">
                                 </asp:DropDownList>

                                 <asp:DropDownList ID="ddlJobType" runat="server" OnSelectedIndexChanged="ddlJobType_Change" Style="width: 25%; height: 32px;float:right;
                                    display: inline; margin-bottom: 20px; margin-right: 15px;" CssClass="form-control" AutoPostBack="true">
                                 </asp:DropDownList>

                                 <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" 
                                    meta:resourcekey="lblMsgResource1"></asp:Label>
                                    <asp:GridView ID="gvGrid" runat="server" AutoGenerateColumns="False" CellPadding="0"
                                    Width="100%" GridLines="None" DataKeyNames="userId" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                                    EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>' 
                                    AllowPaging="True" OnPageIndexChanging="gvGrid_PageIndexChanging" OnRowDataBound="gvGrid_RowDataBound"
                                    BackColor="White" meta:resourcekey="gvGridResource1" ShowFooter="true">
                                    <HeaderStyle CssClass="aa" />
                                        
                                    <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                    <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="ID" meta:resourcekey="TemplateFieldResource1">
                                            <ItemTemplate>
                                                 <asp:Label runat="server" ID="aa" Style="max-width: 15px"> <%# Container.DataItemIndex + 1 %></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow tableHeaderCell"
                                                Width="3%" ForeColor="White" BorderWidth="0px"   />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Employee Name" 
                                            meta:resourcekey="TemplateFieldResource2">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUNamer" runat="server" Text="<%# bind('name') %>" 
                                                    meta:resourcekey="lblUNamerResource1"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow tableHeaderCell"
                                                Width="10%" ForeColor="White" BorderWidth="0px"  />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ALP" meta:resourcekey="TemplateFieldResource3">
                                            <ItemTemplate>
                                                <asp:Label ID="lblALP" runat="server" Text="<%# bind('Apl') %>" 
                                                    meta:resourcekey="lblALPResource1"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow tableHeaderCell"
                                                Width="10%" ForeColor="White" BorderWidth="0px"   />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="PLP" meta:resourcekey="TemplateFieldResource4">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPLP" runat="server" Text="<%# bind('Ppl') %>" 
                                                    meta:resourcekey="lblPLPResource1"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow tableHeaderCell"
                                                Width="10%" ForeColor="White" BorderWidth="0px"   />
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="PLP" meta:resourcekey="TemplateFieldResource7">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPdp" runat="server" Text="<%# bind('pdp') %>" 
                                                    meta:resourcekey="lblPDPResource1"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow tableHeaderCell"
                                                Width="10%" ForeColor="White" BorderWidth="0px"   />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="All" meta:resourcekey="TemplateFieldResource5">
                                            <ItemTemplate>
                                                <asp:Label ID="lblALL" runat="server" Text="<%# bind('allpoint') %>" 
                                                    meta:resourcekey="lblALLResource1"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow tableHeaderCell"
                                                Width="10%" ForeColor="White" BorderWidth="0px"   />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="View Detail" 
                                            meta:resourcekey="TemplateFieldResource6">
                                            <ItemTemplate>
                                                <p>
                                                    <asp:LinkButton ID="LinkButton1" runat="server" PostBackUrl='<%# String.Format("apl_points.aspx?AllPoint={0}", Eval("userId")) %>'
                                                        ToolTip="View" meta:resourcekey="LinkButton1Resource1">View Detail</asp:LinkButton>
                                                </p>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow tableHeaderCell"
                                                Width="5%" ForeColor="White" BorderWidth="0px"   />
                                        </asp:TemplateField>
                                    </Columns>
                                       
                                </asp:GridView>

                                    <%--OnClientClick="javascript:history.back(); return false;"--%>
                                         <asp:Button runat="server" ID="Button2" class="btn btn-default black pull-right" Text="Back"
           OnClick="Button2_Click" meta:resourcekey="BackResource" style="margin-top: 10px;" ></asp:Button>
                                   <%-- <a href="#"  onclick="javascript:history.back(); return false;"  class="btn black pull-right" style="border-radius: 5px;margin-left:0px;margin-top:5px;float:right;" >
                                        <asp:Label meta:resourcekey="BackResource" ID="lblbackTxt" runat="server"></asp:Label>
                                    </a>--%>
                                   <%-- <button type="button" class="btns  yellow  lrg-btn flat-btn add_user" onclick="javascript:history.back(); return false;" style="border: 0px;margin-left:0px;margin-top:5px;float:right;">
                                        <asp:Literal ID="Literal9" runat="server"  EnableViewState="false" />
                                    </button>--%>
                                                                
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
