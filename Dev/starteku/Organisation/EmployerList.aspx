﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true" CodeFile="EmployerList.aspx.cs" Inherits="Organisation_EmployerList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/ajaxtab.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg"></asp:Label>
    <div class="col-md-6">
        <div class="heading-sec">
            <h1>Employer <i><span runat="server" id="Employer"></span></i></h1>
        </div>
    </div>
    <%--<div class="col-md-3">
        <div class="dropdown-example">
            <ul class="nav nav-pills">
                <li class="dropdown" style="float: left!important; width: 100%;"><a class="skill_dropdown"
                    id="drop7" role="button" data-toggle="dropdown" href="#">Account Department<b class="skill_caret"></b></a>
                    <ul id="menu7" class="dropdown-menu" role="menu" aria-labelledby="drop7" style="width: 100%;">
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Account Department</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Sales Department</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Human Resources Department
                        </a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Software Department</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Network Department</a></li>
                        <li role="presentation" class="divider"></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="report_all.html">All</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-md-3">
        <div class="dropdown-example">
            <ul class="nav nav-pills">
                <li class="dropdown" style="float: left!important; width: 100%;"><a class="skill_dropdown"
                    id="A1" role="button" data-toggle="dropdown" href="report_all.html">All<b class="skill_caret"></b></a>
                    <ul id="Ul1" class="dropdown-menu" role="menu" aria-labelledby="drop7" style="width: 100%;">
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="report.html">Original
                            competence level</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">New competence level</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Development points</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Knowledge share</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Potential for development</a></li>
                        <li role="presentation" class="divider"></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">All</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>--%>
    <div class="col-md-12" style="margin-top: 20px;">
        <div id="graph-wrapper">
            <div class="col-md-12">
                <a href="Registration.aspx?org=1" data-toggle="modal" title="" style="margin-bottom: 15px;">
                    <button style="border: 0px;" class="btn btn-primary yellow" type="button">
                        Add New Employer</button>
                </a>
                <br />
                <br />
                <div class="chart-tab">
                    <div id="tabs-container">
                        <cc1:TabContainer ID="TabContainer1" runat="server" CssClass="Tab">
                            <cc1:TabPanel ID="tbpnluser" runat="server">
                                <HeaderTemplate>
                                    Employer<a></a>
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <div class="invoice" style="background-color: white; margin-top: 0px;">
                                        <asp:GridView ID="gvGrid" runat="server" AutoGenerateColumns="False" CellPadding="0"
                                            Width="100%" GridLines="None" DataKeyNames="userId" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                                            EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>' OnRowCommand="gvGrid_RowCommand">
                                            <HeaderStyle CssClass="aa" />
                                            <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                            <AlternatingRowStyle BackColor="White" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Sr.No">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow" Width="10%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUNamer" runat="server" Text="<%# bind('name') %>"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow" Width="10%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Email">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUserNamer" runat="server" Text="<%# bind('userEmail') %>"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow" Width="10%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Contact">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUserNamer1" runat="server" Text="<%# bind('usercontact') %>"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow" Width="10%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="DOB">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbldate" runat="server" Text="<%# bind('userDOB') %>"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow" Width="10%" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Action" ItemStyle-Width="50px">
                                                    <ItemTemplate>
                                                        <div class="vat" style="width: 50%;">
                                                            <p>
                                                                <i class="fa fa-pencil"></i><a href="<%# String.Format("Registration.aspx?org={0}", Eval("userId")) %>"
                                                                    title="Edit">Edit</a>
                                                            </p>
                                                        </div>
                                                        <div class="total" style="width: 50%;">
                                                            <p>
                                                                <i class="fa fa-trash-o"></i>
                                                                <asp:LinkButton ID="lnkBtnName" runat="server" CommandName="archive" CommandArgument='<%# Eval("userId") %>'
                                                                    ToolTip="Archive" OnClientClick="return confirm('Are you sure you want to archive this record?');">Archive</asp:LinkButton>
                                                            </p>
                                                        </div>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow" Width="10%" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </ContentTemplate>
                            </cc1:TabPanel>
                            <cc1:TabPanel ID="tbpnlusrdetails" runat="server">
                                <HeaderTemplate>
                                    Archive
                                </HeaderTemplate>
                                <ContentTemplate>

                                    <div class="invoice" style="background-color: white; margin-top: 0px;">
                                        <asp:GridView ID="gvArchive" runat="server" AutoGenerateColumns="False" CellPadding="0"
                                            CellSpacing="0" Width="100%" GridLines="none"
                                            EmptyDataText='<%#CommonModule.msgGridRecordNotfound %>'
                                            DataKeyNames="userId" OnRowCommand="gvArchive_RowCommand"
                                            CssClass="table table-striped table-bordered table-hover table-checkable datatable">
                                            <HeaderStyle CssClass="aa" />
                                            <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                            <AlternatingRowStyle BackColor="White" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Sr.No" ItemStyle-Width="50px">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUNamer" runat="server" Text="<%# bind('name') %>"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Email">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUserNamer" runat="server" Text="<%# bind('userEmail') %>"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Create By">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUserNamer1" runat="server" Text="<%# bind('userCreateBy') %>"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="DOB">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbldate" runat="server" Text="<%# bind('userDOB') %>"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Action" ItemStyle-Width="50px">
                                                    <ItemTemplate>
                                                        <span class="btn-group">
                                                            <asp:LinkButton ID="lnkbtnrestore" runat="server" CommandName="archive"
                                                                CommandArgument='<%# Eval("userId") %>'
                                                                ToolTip="Restore"
                                                                OnClientClick="return confirm('Are you sure you want to restore this record?');"
                                                                Text="Restore"></asp:LinkButton>&nbsp;|&nbsp;
                                                    <asp:LinkButton ID="lnkBtnPermanentlydelete" runat="server" CommandName="permanentlydelete"
                                                        CommandArgument='<%# Eval("userId") %>' Text="Delete Permanently" ToolTip="Delete Permanently"
                                                        OnClientClick="return confirm('Are you sure you want to permanently delete this record?');"></asp:LinkButton>
                                                        </span>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                    <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </ContentTemplate>
                            </cc1:TabPanel>
                        </cc1:TabContainer>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
