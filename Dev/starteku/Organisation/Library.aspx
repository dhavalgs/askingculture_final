﻿<%--<%@ Page ClientIDMode="AutoID" Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true" CodeFile="Library.aspx.cs" Inherits="Organisation_Library"
    Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>--%>

<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true" CodeFile="Library.aspx.cs" Inherits="Organisation_Library"
    Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <%-- <script src="../Scripts/sMain.js?version=1.0" type="text/javascript"></script>
<script src="../assets/assets/js/libs/jquery-1.10.2.min.js"></script>

    <link href="../Styles/UpdateCustom.css" rel="stylesheet" type="text/css" />
 <link href="../assets/css/jquery.tagsinput.css" rel="stylesheet" />
   <link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/themes/start/jquery-ui.css" />

    <script src="../plugins/bootstrap-inputmask/jquery.inputmask.min.js" type="text/javascript"></script>--%>
     <%-- <link href="css/ajaxtab.css" rel="stylesheet" />
    <script src="../assets/assets/js/plugins.js" type="text/javascript"></script>
    <script src="../Scripts/sMain.js?version=1.0" type="text/javascript"></script>--%>
   
    <link href="../Styles/UpdateCustom.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .chat-widget-head {
            float: left;
            width: 100%;
        }

        .inbox-hover {
            /* right: -130px;
    width:130px;*/
        }

        .document inbox-hover black a i {
            margin-top: 9px;
            min-width: 112px;
        }

        .inbox-hover {
            right: -130px;
            width: 107px;
            min-width: 108px;
        }

        .radiobuttonlist {
            font: 12px Verdana, sans-serif;
            color: #000; /* non selected color */
        }

        .inline-rb input[type="radio"] {
            width: auto;
            margin-right: 10px;
            margin-left: 20px;
        }

        .inline-rb label {
            display: inline;
        }
    </style>
  <%--  <script src="../assets/js/libs/tagsInput.js" type="text/javascript"></script>--%>

    <script>
        function GeneratePoint(a) {

            $.ajax({
                type: "POST",
                url: 'Library.aspx/GeneratePoint',
                data: "{'docId':'" + a + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    //$("#divResult").html(msg.d);
                    //if (msg.d == "True") {
                    //    generate("warning", "Please wait, we are refreshing system.", "bottomCenter")
                    //    setTimeout(function () { location.reload(); }, 1000);
                    //} else {
                    //    generate("warning", msg.d, "bottomCenter")
                    //}
                },
                error: function (e) {
                    $("#divResult").html("WebSerivce unreachable");
                }
            });
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" meta:resourcekey="lblMsgResource1"></asp:Label>
     <asp:HiddenField runat="server" ID="tooltipdownload" meta:resourcekey="Download1" />
    <asp:HiddenField runat="server" ID="tooltipRead" meta:resourcekey="Read1" />
    <asp:HiddenField runat="server" ID="tooltipDelete" meta:resourcekey="Delete1" />
     <asp:Label ID="hdnConfirmArchive" style="display:none;" text="Are you sure to delete document?"  CssClass="hdnConfirmArchive" meta:resourcekey="ConfirmArchive" runat="server" />
    <asp:Label runat="server" ID="lblCannotdelete" meta:resourcekey="CanNotDeleteDocument" style="display:none;"/>
    <%-- <div class="wrapper">--%>
    <div class="">

            <asp:HiddenField ID="hdnSelectCategoty" runat="server" meta:resourcekey="drpSelectCategoty"></asp:HiddenField>
    <asp:HiddenField ID="hdnSelectCompetence" runat="server" meta:resourcekey="drpSelectCompetence"></asp:HiddenField>
   
     <asp:HiddenField ID="hdnSelectPerson" runat="server" meta:resourcekey="drpSelectPerson"></asp:HiddenField>
    <asp:HiddenField ID="hdnSelectDivision" runat="server" meta:resourcekey="drpSelectDivision"></asp:HiddenField>
    <asp:HiddenField ID="hdnSelectJob" runat="server" meta:resourcekey="drpSelectJob"></asp:HiddenField>
        <asp:HiddenField ID="hdnSelectDocCategory" runat="server" meta:resourcekey="drpDocCategory"></asp:HiddenField>
        <asp:HiddenField ID="hdnDateFormat" runat="server"></asp:HiddenField>

        <div class="container">
            <div class="col-md-6">
                <div class="heading-sec">
                    <h1>
                        <%--  <%= CommonMessages.Library%>--%>
                        <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Library" EnableViewState="false" />
                        <i><span runat="server" id="Library"></span></i>
                    </h1>
                </div>
            </div>
            <!-- <div class="col-md-6 range ">
				 <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                   <i class="fa fa-calendar-o icon-calendar icon-large"></i>
                   <span>August 5, 2014 - September 3, 2014</span>  <b class="caret"></b>                </div>-->
        </div>
        <div class="col-md-12">



            <%-------Pop up Add new document-------------------------------------------%>
            <div aria-hidden="true" role="dialog" tabindex="-1" class="modal fade" id="popupAddNewDoc"
                style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header blue yellow-radius">
                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                                ×
                            </button>
                            <h4 class="modal-title">
                                <%-- <%= CommonMessages.AddNewDocument%>--%>
                                <%--Add New Document<asp:Literal ID="Literal8" runat="server" meta:resourcekey="AddNewDocument"
                                    EnableViewState="false" />--%>
                                <asp:Label runat="server" ID="Label4" CssClass="" meta:resourcekey="lbladdmewdoc" Style="color: white; font-size: 19px; margin-top: 4px;"></asp:Label>
                            </h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="col-md-3" style="padding-top: 10px;">
                                    <asp:Label runat="server" ID="Label5" CssClass="" meta:resourcekey="lbldocname" Style="color: black; font-weight: bold; margin-top: 4px;"></asp:Label>
                                </div>
                                <div class="col-md-9">

                                    <asp:TextBox runat="server"  ID="txtFileName" MaxLength="50"  meta:resourcekey="FileName" />
                                    <%--meta:resourcekey="txttitleResource1"--%>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFileName"
                                        ErrorMessage="Please enter file name." CssClass="commonerrormsg" Display="Dynamic"
                                        ValidationGroup="chkdoc" meta:resourcekey="plsenterfilename"></asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-3">
                                    <asp:Label runat="server" ID="Label6" CssClass="" meta:resourcekey="lbltags" Style="color: black; font-weight: bold; margin-top: 4px;"></asp:Label>
                                </div>
                                <div class="col-md-9">
                                    <asp:TextBox Width="424" runat="server" ID="txtKeywords" CssClass="tagsinput" TextMode="SingleLine" meta:resourcekey="txtKeywords"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">


                            <div class="margin-top-10px" style="display: none">
                                <asp:RadioButtonList runat="server" ID="rdoPrivatePublic" RepeatDirection="Horizontal"
                                    TextAlign="Left" CssClass="radioButtonList form-control " Style="display: inline; width: 5px">
                                    <asp:ListItem Selected="True">Public</asp:ListItem>
                                    <asp:ListItem>Private</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                                 </div>

                            <%--Checkboxes-----------------------------------------%>
                            <style type="text/css">
                                .ddpDivision {
                                    max-height: 150px;
                                    overflow: auto;
                                    border: 1px solid #d4d4d4;
                                    padding: 5px;
                                }

                                #rdoPrivatePublic label {
                                    display: inline;
                                }

                                #ContentPlaceHolder1_rdoPrivatePublic_0 {
                                    width: 0px;
                                    margin-right: 9px;
                                }

                                #ContentPlaceHolder1_rdoPrivatePublic_1 {
                                    width: 0px;
                                    margin-right: 9px;
                                }

                                .rdoBtn {
                                    width: 0px;
                                    margin-right: 9px;
                                }
                            </style>

                            <div class="form-group" >
                                <div class="col-md-6">
                                    <div class="">
                                        <asp:Label runat="server" ID="Label21" CssClass="" meta:resourcekey="lbljobtitle" Style="color: black; font-weight: bold; margin-top: 4px;"></asp:Label>
                                       <%-- <label>
                                            Job title scope to target:
                                        </label>--%>
                                    </div>
                                    <div class="ddpDivision ">
                                        <asp:DropDownList Width="250px" ID="chkListJobtype" runat="server" CssClass="chkliststyle form-control"
                                            RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description"
                                            DataValueField="value" Style="width: 100%; overflow: auto;">
                                        </asp:DropDownList>
                                       
                                    </div>
                                </div>
                                <div class="col-md-6">
                                     <asp:Label runat="server" ID="Label22" CssClass="" meta:resourcekey="lblorgUnit" Style="color: black; font-weight: bold; margin-top: 4px;"></asp:Label>
                                    <%--<label>
                                        Organisation unit to target:
                                    </label>--%>
                                    <div class="ddpDivision ">
                                        <asp:DropDownList Width="250px" ID="chkListDivision" runat="server" CssClass="chkliststyle form-control"
                                            RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description"
                                            DataValueField="value" Style="width: 100%; overflow: auto;">
                                        </asp:DropDownList>
                                    </div>
                                    <br/>  
                                </div>
                                 
                            </div>



                            <%--Keyword Textbox ----------------------------------------------------%>

                            <div class="margin-top-10px" style="margin-left: 9px; display: none">
                                <label>
                                    Repository :</label>
                                <asp:DropDownList runat="server" ID="ddRepository">
                                    <asp:ListItem>Please Select Repository</asp:ListItem>
                                    <asp:ListItem>Document</asp:ListItem>
                                    <asp:ListItem>HR Rules</asp:ListItem>
                                    <asp:ListItem>Employee Rules</asp:ListItem>
                                    <asp:ListItem>Manpower Rules</asp:ListItem>
                                    <asp:ListItem>Marketing</asp:ListItem>
                                </asp:DropDownList>
                                <asp:CompareValidator ID="val14" runat="server" ControlToValidate="ddRepository"
                                    ErrorMessage=" Required." Operator="NotEqual" ValueToCompare="Please Select Repository"
                                    ForeColor="Red" SetFocusOnError="true" />
                                <%--Description--------------------------------------------------------%>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6">
                                    <asp:Label runat="server" ID="Label7" CssClass="" meta:resourcekey="lblcompetencedevelope" Style="color: black; font-weight: bold; margin-top: 4px;"></asp:Label><label>?</label>
                                </div>
                                <br />
                               
                                <div class="col-md-6">
                                    <asp:RadioButtonList runat="server" ID="chkIsCompDevDoc" RepeatDirection="Horizontal" CssClass="inline-rb "  TextAlign="Right">
                                        <asp:ListItem Value="Competence Development Document" meta:resourcekey="lblYes"></asp:ListItem>
                                        <asp:ListItem  Value="General"  Selected="True" meta:resourcekey="lblNo"/>
                                    </asp:RadioButtonList>
                                </div>
                            </div>



                            <%--<div class="form-group" style="margin-top: 354px; margin: 0px;">
                                <div class="col-md-3" style="padding-top: 4px;">
                                    <asp:Label runat="server" ID="Label8" CssClass="" meta:resourcekey="lblCompetence" Style="color: black; font-weight: bold; margin-top: 4px;"></asp:Label>
                                </div>
                                <div class="col-md-9">
                                    <asp:DropDownList runat="server" ID="ddlCompetences" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>--%>
                            
                             <br/><br/>
                            <div class="form-group" style="display: none;" id="doccompetence">
                                <div class="col-md-3" style="padding-top: 4px;">
                                    <asp:Label runat="server" ID="Label8" CssClass="" meta:resourcekey="lblCompetence" Style="color: black; font-weight: bold; margin-top: 4px;"></asp:Label>
                                    <br /><br/><br/>
                                </div>
                                <div class="col-md-9">
                                    <asp:DropDownList runat="server" ID="ddlCompetences"  CssClass="form-control">
                                      
                                    </asp:DropDownList>
                                </div>
                                    <div class="form-group" style="clear: both; margin-top: 400px; margin: 0px;">
                                <div class="col-md-3">
                                    <asp:Label runat="server" ID="Label10" CssClass="" meta:resourcekey="Levelname" Style="color: black; font-weight: bold; margin-top: 4px;"></asp:Label>
                                

                                </div>
                                <div class="col-md-9">
                                    <asp:RadioButtonList runat="server" ID="rdoLevel" RepeatDirection="Horizontal" RepeatLayout="Table" CssClass="inline-rb form-control"  TextAlign="Right">
                                     
                                        <asp:ListItem Text="1 to 2" Value="1 to 2" meta:resourcekey="OneToTwoResource" Selected="True">1 to 2</asp:ListItem>
                                        <asp:ListItem Text="2 to 3" Value="2 to 3" meta:resourcekey="TwoToThreeResource">2 to 3</asp:ListItem>
                                        <asp:ListItem Text="3 to 4" Value="3 to 4" meta:resourcekey="ThreeToFourResource">3 to 4</asp:ListItem>
                                        <asp:ListItem Text="4 to 5" Value="4 to 5" meta:resourcekey="FourToFiveResource">4 to 5</asp:ListItem>

                                    </asp:RadioButtonList>
                                     <br/> 
                                </div>
                            </div>
                            </div>

                            <br/><br/>
                            <div class="form-group" style="" id="docCategory">
                                <div class="col-md-3" style="padding-top: 4px;">
                                    <asp:Label runat="server" ID="Label18" CssClass="" meta:resourcekey="lblCategory" Style="color: black; font-weight: bold; margin-top: 4px;"></asp:Label>
                                    <br /><br/>
                                </div>
                                <div class="col-md-9">
                                    <asp:DropDownList runat="server" ID="ddlDocumentCategory"  CssClass="form-control">
                                      
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <br/><br/>
                        
                            <div style="clear:both;"></div>

                            <div class="form-group" style="margin-top: 489px; margin: 0px;">
                                <div class="col-md-3">
                                    <asp:Label runat="server" ID="Label9" CssClass="" meta:resourcekey="lblDescription" Style="color: black; font-weight: bold; margin-top: 4px;"></asp:Label>

                                </div>
                                <div class="col-md-9">
                                    <asp:TextBox runat="server" ID="txtDescription" MaxLength="120" style="" meta:resourcekey="txtDescription" />
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-3"></div>
                                <div class="col-md-9"></div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-3">
<%--                                    File upload------------------------------------------------%>
                                    <label for="exampleInputFile">
                                        <asp:Literal ID="Literal8" runat="server" meta:resourcekey="BrowseFiles" EnableViewState="false" />

                                        <i class="fa fa-paperclip attch_file"></i>
                                        <asp:Literal ID="Literal9" runat="server" meta:resourcekey="Attachments" EnableViewState="false" />
                                     
                            
                                        <%--<%= CommonMessages.Attachments%>--%></label>
                                </div>
                                <div class="col-md-9">
                                    <a class="btn btn-primary yellow" style="display: block; width: 120px; height: 30px;" onclick="document.getElementById('ContentPlaceHolder1_FileUpload1').click()">
                                    <asp:Literal ID="Literal21" runat="server" EnableViewState="false" meta:resourcekey="Browseres" /> </a>
                                    <asp:Label ID="Label23" runat="server" Text="No File Selected" meta:resourcekey="nofileselectedresource"></asp:Label>
                                    <div style="display: none;">
                                    <asp:FileUpload ID="FileUpload1" runat="server" onchange="getFileData(this);"/>
                                </div>
                                    
                                    <asp:RegularExpressionValidator ID="reFile1" runat="server" ControlToValidate="FileUpload1"
                                        CssClass="legend" Display="Dynamic" ErrorMessage="Please upload only .pdf file."
                                        ValidationExpression="^.*\.(PDF|pdf)$" ForeColor="Red" ValidationGroup="chkdoc"
                                        meta:resourcekey="reFile1Resource1"></asp:RegularExpressionValidator>
                                    <br />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="FileUpload1"
                                        ErrorMessage="Please select file." CssClass="commonerrormsg" Display="Dynamic"
                                        ValidationGroup="chkdoc" meta:resourcekey="RequiredFieldValidator5Resource1"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <%--meta:resourcekey="txttitleResource1"--%>
                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFileName"
                            ErrorMessage="Please enter file name." CssClass="commonerrormsg" Display="Dynamic"
                            ValidationGroup="chk" ></asp:RequiredFieldValidator>--%>

                            <%--Buttons--------------------------------------------------------%>
                            <div style="clear: both">
                            </div>
                            <div class="modal-footer">
                                <%-- <button class="btn btn-primary yellow" type="button">Submit</button>
                        <button data-dismiss="modal" class="btn btn-default black" type="button">Close </button>--%>
                                <asp:Button runat="server" ID="btnsubmitdoc" Text="Submit" CssClass="btn btn-primary yellow"
                                    type="button" ValidationGroup="chkdoc" OnClick="btnsubmitdoc_click" OnClientClick="CheckValidations('chkdoc');closeModel();"
                                    Style="border-radius: 5px; width: 100px; height: 38px; margin-left: 359px;" meta:resourcekey="btnsubmitResource1" />
                              
                                 <asp:Button runat="server" ID="Button1" Text="Close" CssClass="btn btn-default black"
                                    type="button"  OnClientClick="closeModel();"  data-dismiss="modal"
                                    Style="border-radius: 5px; width: 100px; height: 38px; margin-left: 466px; margin-top: -58px; color: white;" meta:resourcekey="btncloseResource1" />

                                <%--<input type="reset" class="btn btn-default black" runat="server"  value="Close"  aria-hidden="true" data-dismiss="modal" 
                                    style="border-radius: 5px; width: 100px; height: 38px; margin-left: 466px; margin-top: -58px; color: white;"
                                    />--%>
                                <!-- /.modal-content ---------------------------------------------------->
                                <%-- <asp:UpdatePanel runat="server" ID="UpdatePanel2" UpdateMode="Conditional">
            <ContentTemplate>--%>
                                <ul class="document">
                                    <asp:Repeater ID="Repeater1" runat="server">
                                        <ItemTemplate>
                                            <li style="padding: 0px;">
                                                <div class="document1">
                                                    <img src="images/documents_icon.png" style="height: 47px !important;">
                                                </div>
                                                <h1 class="textOverflow"><%# Eval("docFileName_Friendly")%></h1>
                                                <span><%# Eval("MinAgo") %></span> 
                                                 <i class="textOverflow"> <%# Eval("FullName") %></i>
                                                <p style="height: 60px;white-space: nowrap;overflow: auto" class="textOverflow">
                                                    <%# Eval("docDescription")%>
                                                </p>
                                                <div class="inbox-hover black">
                                                    <a href="../Log/upload/Document/<%# Eval("docattachment") %>"  title="" data-tooltip="Download" class="tooltipDownload"
                                                        data-placement="bottom" download><i class="fa fa-download" style="margin-top: 9px;"></i></a><a href="#" title="" class="tooltipRead" data-tooltip="Read"
                                                             data-placement="bottom" onclick="openPDF('../Log/upload/Document/<%# Eval("docattachment") %>?UID=<%# Eval("docUserID") %>&TOID=0&docId=<%# Eval("docID") %>');">
                                                            <i class="fa fa-eye" style="margin-top: 9px;"></i></a>
                                                    <asp:LinkButton ID="btn_delete" runat="server" CommandName='<%# Eval("docId") %>'
                                                        data-tooltip="Delete" data-placement="bottom" OnClick="btn_delete_Click"><i class="fa fa-trash-o" style="margin-top: 9px;"></i></asp:LinkButton>
                                                </div>
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                                <%-- </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="txtsearch" />
                
                
            </Triggers>
        </asp:UpdatePanel>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Resualt-->
            <div class="add-btn1" style="float: left; padding-left: 17px;">
                <a href="#popupAddNewDoc" data-toggle="modal" title="" onclick="activateTagsInput();" style="" class="btn btn-default yellow"><i class="fa fa-plus blue"></i>
                    <asp:Label runat="server" ID="Label3" CssClass="" meta:resourcekey="lbladdmewdoc" Style=" font-size: 19px; margin-top: 4px;"></asp:Label>
                </a>
            </div>
         
            <div style="clear: both;"></div>

            <div class="col-md-12">
                <div class="chat-widget widget-body" style="margin-top: 10px;">
                    <div class="chat-widget-head yellow yellow-radius" style="padding: 3px;">
                        <div class="col-md-2" style="padding: 10px;">
                            <h4>
                                <%-- <%= CommonMessages.SearchResult%>--%>
                                <asp:Literal ID="Literal6" runat="server" meta:resourcekey="SearchResult" EnableViewState="false" />

                            </h4>

                        </div>
                        <div class="col-md-3" style="">
                            <div class="form-group" style="margin-top: 8px;">
                                <div class="col-md-4">
                                    <%--<label >Competence</label>--%>
                                </div>
                                <div class="col-md-8">
                                </div>
                            </div>

                            <table class="libray_table" width="60%" border="0" style="display: none;">
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkdivision" runat="server" OnCheckedChanged="Check_Clicked" AutoPostBack="True"
                                            meta:resourcekey="chkdivisionResource1" Checked="True" />&nbsp;
                                        <%--<%= CommonMessages.Withindivision%>--%>
                                        <asp:Literal ID="Literal2" runat="server" meta:resourcekey="Withindivision" EnableViewState="false" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkDOU" runat="server" OnCheckedChanged="Check_Clicked" AutoPostBack="True"
                                            meta:resourcekey="chkDOUResource1" Checked="True" />&nbsp; DOU's
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkFull" runat="server" OnCheckedChanged="Check_Clicked" AutoPostBack="True"
                                            meta:resourcekey="chkFullResource1" Checked="True" />&nbsp;
                                        <%--<%= CommonMessages.Fulldatabase%>--%>
                                        <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Fulldatabase" EnableViewState="false" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkManuals" runat="server" OnCheckedChanged="Check_Clicked" AutoPostBack="True"
                                            meta:resourcekey="chkManualsResource1" />&nbsp;<%-- <%= CommonMessages.Manuals%>--%><asp:Literal ID="Literal4" runat="server" meta:resourcekey="Manuals" EnableViewState="false" />
                                        <asp:CheckBox ID="refresh" runat="server" OnCheckedChanged="Check_Clicked" Style="display: none"
                                            AutoPostBack="True" meta:resourcekey="chkManualsResource1" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkForum" runat="server" OnCheckedChanged="Check_Clicked" AutoPostBack="True"
                                            meta:resourcekey="chkForumResource1" Checked="True" />&nbsp; Forum
                                    </td>
                                    <td></td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-4" style="">

                            <div class="form-group" style="clear: both; margin-top: 8px;">
                                <div class="col-md-12">
                                    
                                    <button class="btn btn-primary black" onclick="GetAdvanceSettings(); return false;">
                                          <asp:Literal ID="AdvanceSettings" runat="server" meta:resourcekey="AdvanceSettings" EnableViewState="false" />
                                      
                                       
                                    </button>
                                     <asp:Button runat="server" ID="btnresetfilter" Text="<%# ResetFilter.Text %>"   OnClick="btnresetfilter_OnClick" CssClass="btn btn-primary black" 
                                         CausesValidation="False"  
                                         style="float: right; margin-bottom: 10px;" ></asp:Button>
                                    <%--  <asp:Label runat="server" ID="Label2" CssClass="" meta:resourcekey="lbllavel" Style="color: white; font-size: 19px; margin-top: 4px;"></asp:Label>--%>
                                </div>
                                <div class="col-md-8">
                                </div>
                            </div>
                        </div>
                          <div  style="display: none">
                                         <asp:Literal ID="ResetFilter" runat="server" meta:resourcekey="ResetFilter" />
                                            </div>
                        <div class="col-md-3">
                            <%--<asp:UpdatePanel runat="server" ID="UpdatePanellist" UpdateMode="Conditional">
                                        <ContentTemplate>--%>
                            <div class="col-md-12" style="float: right;">
                                <asp:TextBox ID="txtsearch" class="library_search" runat="server" AutoPostBack="True"
                                    OnTextChanged="txtsearch_TextChanged" meta:resourcekey="txtsearchResource1"></asp:TextBox>
                            </div>

                            <%-- </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="txtsearch" />
                                        </Triggers>
                                    </asp:UpdatePanel>--%>
                            <div class="col-md-10" style="margin-bottom: 20px; display: none;">
                                <a href="ForumAll.aspx" title="" class="btns  blue  col-md-11 libray_btn lbl_btn">Q & A / Forum</a>
                            </div>
                            <div class="col-md-9" style="display: none;">
                                <div class="chat-widget widget-body">
                                    <div class="chat-widget-head yellow yellow-radius">
                                        <h4 style="width: 100%">
                                            <%--  <%= CommonMessages.NewUploaedfiles%>--%>
                                            <asp:Literal ID="Literal5" runat="server" meta:resourcekey="NewUploaedfiles" EnableViewState="false" />
                                            <div class="add-btn">
                                                <a href="#popupAddNewDoc" data-toggle="modal" title="" onclick="activateTagsInput();" style="height: 24px; border-radius: 5px;"><i class="fa fa-plus blue"></i>Add New Doc</a>
                                            </div>
                                        </h4>
                                    </div>

                                    <ul id="scrollbox5" class="your-message ">
                                        <asp:Repeater ID="rep_document" runat="server">
                                            <ItemTemplate>
                                                <li style="padding: 0px;">
                                                    <div class="document1">
                                                        <img src="images/new_file.png" style="height: 47px !important;">
                                                    </div>
                                                    <h1 class="textOverflow">
                                                        <%# Eval("docFileName_Friendly") %></h1>
                                                    <i class="textOverflow">
                                                        <label style="color: #0a97c1">Competence Category: </label>
                                                        <%# Eval("doccompetence") %>
                                                    </i>
                                                    <span>
                                                        <%# Eval("MinAgo") %>
                                                    </span><i class="textOverflow">
                                                        <label style="color: #0a97c1">Upload By: </label>
                                                        <%# Eval("FullName") %>
                                                    </i>
                                                  
                                                    <i class="textOverflow">
                                                        <label style="color: #0a97c1">Level: </label>
                                                        <%# Eval("docRepository") %>
                                                    </i>
                                                    <p class="textOverflow">
                                                        <%# Eval("docDescription") %>
                                                    </p>
                                                    <div class="inbox-hover black" style="padding: 0px 4px 0px 0px;">
                                                        <a href="../Log/upload/Document/<%# Eval("docattachment") %>" title="" class="tooltipDownload" data-tooltip="Download"
                                                            data-placement="bottom"><i class="fa fa-download" style="margin-top: 9px;"></i></a><a href="#" title="" data-tooltip="Read" class="tooltipRead" data-placement="bottom" onclick="openPDF('../Log/upload/Document/<%# Eval("docattachment") %>');">
                                                                <i class="fa fa-eye" style="margin-top: 9px;"></i></a>
                                                    </div>
                                                </li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-10" style="margin: 20px 0px; display: none;">
                                <a href="#add-post-title" title="">
                                    <input type="button" value="Show/Hide All Documents" onclick="$('#showAllDoc').toggle('slow');"
                                        class="btns  blue  col-md-11 libray_btn lbl_btn" /></a>
                            </div>
                        </div>


                    </div>

                    <div class="col-md-4 filterBox" style="display: none;">
                        <ul id="Ul1">

                            <p class="help-block">
                              <asp:Label runat="server" ID="lblTimespan" meta:resourcekey="DateFrom"></asp:Label>   
                            </p>
                            <asp:TextBox runat="server" CssClass="library_search datepicker1"  ID="txtFromDate" data-mask="99/99/2099" AutoPostBack="False" Style="height: 42px;"></asp:TextBox>
                            <br />
                            <p class="help-block">
                               <asp:Label runat="server" ID="Label11" meta:resourcekey="DateTo"></asp:Label>    
                            </p>
                            <asp:TextBox ID="txtTodate" runat="server" CssClass="library_search datepicker12"  AutoPostBack="False" data-mask="99/99/2099" Style="height: 42px;"></asp:TextBox>

                            <br />
                            <p class="help-block">
                               <asp:Label runat="server" ID="Label12" meta:resourcekey="Person"></asp:Label>     
                            </p>
                            <asp:DropDownList runat="server" ID="ddSearchPerson" CssClass="form-control" Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; color: white;" AutoPostBack="False" >
                            </asp:DropDownList>
                            <br />
                            <p class="help-block">
                               <asp:Label runat="server" ID="Label13" meta:resourcekey="Division"></asp:Label>     
                            </p>
                            <asp:DropDownList runat="server" ID="ddSeachDivision" CssClass="form-control" Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; color: white;" AutoPostBack="False" >
                            </asp:DropDownList>
                            <br />
                            <p class="help-block">
                               <asp:Label runat="server" ID="Label14" meta:resourcekey="JobType"></asp:Label>    
                            </p>
                            <asp:DropDownList runat="server" ID="ddSearchJobType"
                                 CssClass="form-control" Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; color: white;"
                                AutoPostBack="False">
                            </asp:DropDownList>
                            <br />
                            <p class="help-block">
                                <asp:Label runat="server" ID="Label1" CssClass="" meta:resourcekey="lblCompetence" Style=""></asp:Label>
                            </p>
                            <asp:DropDownList runat="server" ID="ddlcompetencefilter" AutoPostBack="False" CssClass="form-control" Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; color: white;">
                            </asp:DropDownList>
                            <br />
                            
                            <p class="help-block">
                                  <asp:Label runat="server" ID="Label15" meta:resourcekey="Level"></asp:Label>    
                            </p>
                             <asp:DropDownList runat="server" ID="ddSearchLevel" CssClass="form-control" Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; color: white;" AutoPostBack="False">
                                <asp:ListItem Text="All" Value="0" meta:resourcekey="All1">All</asp:ListItem>
                                <asp:ListItem Text="1 to 2" Value="1 to 2" meta:resourcekey="OneToTwoResource">1 to 2</asp:ListItem>
                                <asp:ListItem Text="2 to 3" Value="2 to 3" meta:resourcekey="TwoToThreeResource">2 to 3</asp:ListItem>
                                <asp:ListItem Text="3 to 4" Value="3 to 4" meta:resourcekey="ThreeToFourResource">3 to 4</asp:ListItem>
                                <asp:ListItem Text="4 to 5" Value="4 to 5" meta:resourcekey="FourToFiveResource">4 to 5</asp:ListItem>
                            </asp:DropDownList>
                            <br />
                             <p class="help-block">
                                <asp:Label runat="server" ID="Label19" CssClass="" meta:resourcekey="lblCategory" Style=""></asp:Label>
                            </p>
                            <asp:DropDownList runat="server" ID="ddldocCatfilter" AutoPostBack="False" CssClass="form-control" Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; color: white;">
                            </asp:DropDownList>
                            <br />
                            
                           
                           

                            <p class="help-block">
                                 <asp:Label runat="server" ID="Label16" meta:resourcekey="SortBy"></asp:Label> 
                            </p>
                            <asp:DropDownList runat="server" ID="ddSearchSort" CssClass="form-control" Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; color: white;" AutoPostBack="False" >

                               <asp:ListItem Text="Ascending" Value="asc" meta:resourcekey="drpSortingResourse1" ></asp:ListItem>
                                                <asp:ListItem Text="Descending" Value="dsc" meta:resourcekey="drpSortingResourse2"></asp:ListItem>
                            </asp:DropDownList>
                            <br />
                            
                            
                            <br/>
                            <asp:Button runat="server" ID="btnFilter" OnClick="btnFilter_OnClick" meta:resourcekey="btnFilter" Text="Filter Now" CssClass="btn btn-default black col-md-12" CausesValidation="False"  ></asp:Button>
                        </ul>
                        
                        
                        
                    </div>

                     <div class="col-md-12 searchResultBox">
                              <asp:UpdatePanel runat="server" ID="upFilter" UpdateMode="Conditional" ChildrenAsTriggers="True" >
                <ContentTemplate>
                    <div id="NoRecords" runat="server" visible="true" style="padding-top: 20px; color: red;">
                        <asp:Label runat="server" ID="lblNorecordFound" meta:resourcekey="NoRecordFound" Text="Record Not Found."></asp:Label>
                        

                    </div>
                        <ul id="scrollbox91" visible="true" runat="server">
                            <asp:Repeater ID="rptdoc" runat="server">
                                <ItemTemplate>
                                    <li style="border-bottom: 1px solid rgba(0, 0, 0, 0.1);">
                                       <%-- <div class="col-md-10" style="width: 87%;">--%>
                                        <div style="width: 87%;text-align:left;" class="col-md-10">
                                            <div class="document1">
                                                <img src="images/documents_icon.png">
                                            </div>
                                            <h1 style="font-size: 15px;">
                                                <%# Eval("docFileName_Friendly")%>      <i class="textOverflow">
                                                    <%-- <label style="color: #0a97c1"> (  <%# Eval("doccompetence") %>)</label>--%>
                                                             
                                                </i></h1>

                                            <span>  
                                                <i class="textOverflow">
                                                    <label style="color: #0a97c1">
                                                        <asp:Label runat="server" ID="lblTimespan" meta:resourcekey="Timespan"></asp:Label> <%--Timespan : --%> 
                                                    </label>
                                                    <%# Eval("MinAgo") %>
                                            </span><i>
                                                <i class="textOverflow">
                                                    <label style="color: #0a97c1"><asp:Label runat="server" ID="Label2" meta:resourcekey="Levelname"></asp:Label> <%--Level:--%> </label>
                                                    <%# Eval("docRepository") %>
                                                </i>
                                                <asp:Label runat="server" ID="lblMsg" CssClass="" meta:resourcekey="lbluploadby" Style="color: #0a97c1"></asp:Label>
                                                <label style="color: #0a97c1">: </label>
                                                  
                                                <%# Eval("FullName") %>
                                                  <i class="textOverflow">
                                                      <asp:Label runat="server" ID="Label20" CssClass="" meta:resourcekey="lblOn" Style="color: #0a97c1"></asp:Label>
                                                      <%#string.Format("{0:"+ hdnDateFormat.Value +"}",Convert.ToDateTime(Eval("docCreatedDate")))%>
                                                       <%-- <%if(Convert.ToString(HttpContext.Current.Session["Language"]) == "da-DK"){%>
                                                            <%#string.Format("{0:"+ hdnDateFormat.Value +"}",Convert.ToDateTime(Eval("docCreatedDate")))%>
                                                      <%}%>
                                                      <% else{%>
                                                      <%#string.Format("{0:MM-dd-yyyy}",Convert.ToDateTime(Eval("docCreatedDate")))%>
                                                      <%} %>--%>
                                                    </i>
                                            </i>

                                            <p style="height: 60px; white-space: nowrap;overflow:auto">
                                                <%# Eval("docDescription")%>
                                            </p>
                                            <div class="inbox-hover black">
                                                <a  onclick="return GeneratePoint(<%# Eval("docId") %>);" href="../Log/upload/Document/<%# Eval("docattachment") %>" title="" data-tooltip="Download"
                                                    data-placement="bottom" class="tooltipDownload"><i class="fa fa-download" style="margin-top: 9px;"></i></a>
                                                <a href="#" title="" data-tooltip="Read" data-placement="bottom" class="tooltipRead" onclick="  GeneratePoint(<%# Eval("docId") %>);  openPDF('../Log/upload/Document/<%# Eval("docattachment") %>?UID=<%# Eval("docUserID") %>&TOID=0&docId=<%# Eval("docID") %>');">
                                                        <i class="fa fa-eye" style="margin-top: 9px;"></i></a>
                                                <%-- <asp:LinkButton ID="LinkButton1" runat="server" 
                                                data-tooltip="Delete" data-placement="bottom" OnClientClick=' DeleteThisDoc( <%# Eval("docId") %>)' ><i class="fa fa-trash-o" style="margin-top: 9px;"></i></asp:LinkButton>--%>
                                                <a id="deltbtn" class="deletebtn tooltipDelete"
                                                    data-tooltip="Delete" data-placement="bottom"  onclick="DeleteThisDoc(<%# Eval("docId") %>)"><i class="fa fa-trash-o" style="margin-top: 9px;"></i></a>
                                            </div>
                                            <%--<span style="color: #777777 !important; font: 1.1em/1.7 Arial,tahoma,verdana;">(Document)</span>--%>
                                            <%-- <h4 style="cursor: pointer; color: #0a97c1;" onclick="openPDF('../Log/upload/Document/<%# Eval("docattachment") %>?UID=<%# Eval("docUserID") %>&TOID=0&docId=<%# Eval("docID") %>');">
                                                <%# Eval("docFileName_Friendly")%></h4>

                                            <p>
                                                <%# Eval("docDescription") %>
                                            </p>--%>
                                        </p>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                            <asp:Repeater ID="rptfrm" runat="server">
                                <ItemTemplate>
                                    <li class="reply">
                                        <span style="color: #777777 !important; font: 1.1em/1.7 Arial,tahoma,verdana;">(Forum Post)</span>
                                        <div class="chat-thumb">
                                            <img src='../Log/upload/Userimage/<%# Eval("userImage") %>'>
                                        </div>
                                        <div class="chat-desc">
                                            <p>
                                                <a href="forumPost.aspx?fmid=<%# Eval("forumMasterId")%>">
                                                    <%# Eval("fmQuestionName")%></a>
                                            </p>
                                            <i class="chat-time">
                                                <%# Eval("minAgo")%>
                                            </i>
                                        </div>
                                    </li>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <li class="">
                                        <div class="chat-thumb">
                                            <img src='../Log/upload/Userimage/<%# Eval("userImage") %>'>
                                        </div>
                                        <div class="chat-desc">
                                            <p>
                                                <a href="forumPost.aspx?fmid=<%# Eval("forumMasterId")%>">
                                                    <%# Eval("fmQuestionName")%></a>
                                            </p>
                                            <i class="chat-time">
                                                <%# Eval("minAgo")%>
                                            </i>
                                        </div>
                                    </li>
                                </AlternatingItemTemplate>
                            </asp:Repeater> 
                           
                        </ul>

                  <%--  <script src="../Scripts/Scripts/jquery-1.4.1.min.js"></script>--%>
             </ContentTemplate>
                <Triggers>
                    <%--<asp:AsyncPostBackTrigger ControlID="btnFilter" EventName="Click"/>--%>
                    <asp:PostBackTrigger ControlID="btnFilter"/>
                    <%--<asp:PostBackTrigger ControlID="btnresetfilter"/>--%>
        </Triggers>
            </asp:UpdatePanel>
                    </div>
                     <div class="col-md-12">
                   
                    </div>
                </div>
            </div>
                    
        </div>
        <div aria-hidden="true" role="dialog" tabindex="-1" class="modal fade" id="add-post-title"
            style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header blue yellow-radius">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                            ×
                        </button>
                        <h4 class="modal-title">   <asp:Label runat="server" ID="Label17" meta:resourcekey="Forum"></asp:Label></h4>
                    </div>
                    <div class="modal-body">
                        <%--  <input type="text" placeholder="TITLE" />--%>
                        <div>
                            <asp:TextBox runat="server" placeholder="Title" ID="txtName" MaxLength="50" meta:resourcekey="txtNameResource1" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtName"
                                ErrorMessage="Please select file." CssClass="commonerrormsg" Display="Dynamic"
                                ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator5Resource1"></asp:RequiredFieldValidator>
                        </div>
                        <%-- <textarea placeholder="DESCRIPTION" rows="5"></textarea>--%>
                        <div>
                            <asp:TextBox runat="server" placeholder="Description" ID="txtdes" Rows="4" TextMode="MultiLine"
                                meta:resourcekey="txtdesResource1" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtdes"
                                ErrorMessage="Please Enter description." CssClass="commonerrormsg" Display="Dynamic"
                                ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator2Resource1"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <%--  <button class="btn btn-primary yellow" type="button">
                                    Submit</button>--%>
                        <asp:Button runat="server" ID="btnsubmit" Text="Submit" CssClass="btn btn-primary yellow"
                            ValidationGroup="chk" OnClientClick="SubmitBtnForumLibrary();return false;" meta:resourcekey="btnsubmitResource1" />
                        <button data-dismiss="modal" class="btn btn-default black" type="button" id="btnClose">
                            <%-- <%= CommonMessages.Close%>--%>
                            <asp:Literal ID="Literal7" runat="server" meta:resourcekey="Close" EnableViewState="false" />
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
        </div>
        <!-- TIME LINE -->
        <!-- Recent Post -->
        <div class="col-md-3">
        </div>
        <%--saurin : its from document list : show all documents button and repeater --%>
        <div class="container">
            <div class="col-md-12">

                <div id="showAllDoc" style="display: none">

                    <div class="col-md-3 lbl_search">
                        <%-- <input class="library_search" name="" type="text">--%>
                        <asp:UpdatePanel runat="server" ID="UpdatePanel4" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:TextBox ID="txtsearchDoc" class="library_search" runat="server" AutoPostBack="True"
                                    OnTextChanged="txtsearchDoc_TextChanged" meta:resourcekey="txtsearchResource1" Style="height: 42px;"></asp:TextBox>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="txtsearchDoc" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>

                    <asp:UpdatePanel runat="server" ID="UpdatePanellist1" UpdateMode="Conditional">
                        <ContentTemplate>
                            <ul class="document col-md-12" style="max-height: 450px; overflow: auto; margin-top: 15px;">
                                <asp:Label runat="server" ID="lblNoRecordsFound" Text="No Record Found!" Style="margin-right: 141px; float: right;"></asp:Label>
                                <asp:Repeater ID="rep_documentAll" runat="server" OnItemDataBound="rptItems_ItemDataBound">
                                    <ItemTemplate>
                                        <li>
                                            <div class="document1">
                                                <img src="images/documents_icon.png">
                                            </div>
                                            <h1>
                                                <%# Eval("docFileName_Friendly")%></h1>
                                            <span>
                                                <%# Eval("MinAgo") %>
                                            </span><i>
                                                <label style="color: #0a97c1">   <asp:Label runat="server" ID="Label15" meta:resourcekey="UploadBy"></asp:Label> </label>
                                                <%# Eval("FullName") %>
                                            </i>
                                            <p style="height: 60px;">
                                                <%# Eval("docDescription")%>
                                            </p>
                                            <div class="inbox-hover black">
                                                <a href="../Log/upload/Document/<%# Eval("docattachment") %>" title="" data-tooltip="Download"
                                                    data-placement="bottom" class="tooltipDownload">
                                                    <i class="fa fa-download" style="margin-top: 9px;"></i></a>
                                                <a href="#" title="" data-tooltip="Read" data-placement="bottom" class="tooltipRead" onclick="openPDF('../Log/upload/Document/<%# Eval("docattachment") %>?UID=<%# Eval("docUserID") %>&TOID=0&docId=<%# Eval("docID") %>');">
                                                        <i class="fa fa-eye" style="margin-top: 9px;"></i></a>
                                                <%-- <asp:LinkButton ID="LinkButton1" runat="server" 
                                                data-tooltip="Delete" data-placement="bottom" OnClientClick=' DeleteThisDoc( <%# Eval("docId") %>)' ><i class="fa fa-trash-o" style="margin-top: 9px;"></i></asp:LinkButton>--%>
                                                <a id="deltbtn" class="deletebtn"
                                                    data-tooltip="Delete" data-placement="bottom" onclick="DeleteThisDoc(<%# Eval("docId") %>)"><i class="fa fa-trash-o" style="margin-top: 9px;"></i></a>
                                            </div>
                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="txtsearchDoc" />

                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
        <!-- Twitter Widget -->
        <!-- Weather Widget -->
    </div>
    <div class="modal-footer" style="border-top: 0px !important; margin-top: -50px;">
        <%--<asp:Button runat="server" ID="btnBack" Text="Save Competence" CssClass="btn black pull-right"
                                    OnClick="btnBack_click" style="margin: 4px;" />--%>
        <asp:Button runat="server" ID="btnBack" Text="Back" CssClass="btn black pull-right"
            OnClick="btnBack_click" Style="margin: 4px; display: none" />
        <%--<a href="#" onclick="javascript:history.back(); return false;" class="btn black pull-right" style="border-radius: 5px; margin-left: 0px; margin-top: 5px; float: right;">Back</a>--%>
    </div>
    <!-- Container -->
    <!-- RAIn ANIMATED ICON-->
 <%--   <script src="../assets/js/libs/jquery-1.10.2.min.js"></script>--%>
    


    <script type="text/jscript">
        $(document).ready(function () {

            SetExpandCollapse();

            $(".tooltipDownload").attr('data-tooltip', $("#ContentPlaceHolder1_tooltipdownload").val());
            $(".tooltipRead").attr('data-tooltip', $("#ContentPlaceHolder1_tooltipRead").val());
            $(".tooltipDelete").attr('data-tooltip', $("#ContentPlaceHolder1_tooltipDelete").val());

            $("#ContentPlaceHolder1_chkIsCompDevDoc_0").click(function () {
                document.getElementById("<%= ddlDocumentCategory.ClientID %>").selectedIndex = 0;
                $("#docCategory").hide();
                $("#doccompetence").show();
            });
            $("#ContentPlaceHolder1_chkIsCompDevDoc_1").click(function () {
                document.getElementById("<%= ddlCompetences.ClientID %>").selectedIndex = 0;

                $("#docCategory").show();
                $("#doccompetence").hide();
            });
            //alert();
            HideDeleteButon();
          //  DatePickerActivate();



            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

            function EndRequestHandler(sender, args) {
                DatePickerActivate();
            }

        });
        function DatePickerActivate() {
            var jq = $.noConflict();
            jq(".datepicker1").datepicker({
                //minDate: 0,
                changeMonth: true,
                changeYear: true,
                // numberOfMonths: 1,
                dateFormat: "dd/mm/yy"
                //onClose: function (selectedDate) {
                //    jq(".datepicker12").datepicker("option", "minDate", selectedDate);
                //    jq("#jobpickdatehidden1").val(selectedDate);
                //}
            });
            jq(".datepicker12").datepicker({

                changeMonth: true,
                changeYear: true,
                //  numberOfMonths: 1,
                dateFormat: "dd/mm/yy"
                //onClose: function (selectedDate) {
                //    jq(".datepicker1").datepicker("option", "maxDate", selectedDate);
                //    jq("#jobdropdatehidden1").val(selectedDate);

                //}
            });
        }
        function activateTagsInput() {
            setTimeout(function () {
                $('#ctl00_ContentPlaceHolder1_txtKeywords').tagsInput({ onAddTag: onAddTag });
                $("#ctl00_ContentPlaceHolder1_txtKeywords_tagsinput").css("width", "423px");
            }, 500);
        }
        var delId;
        function DeleteThisDoc(uId) {

            if (!confirm($(".hdnConfirmArchive").text())) {
                return;
            }

            //if(!confirm("Are you sure to delete document?"))
            //{
            //    return ;
            //}
            delid = uId;
            //alert(uId);
            $.ajax({
                type: "POST",
                url: 'Library.aspx/DeleteThisDoc',
                data: "{'uId':'" + uId + "','deleteText':'" + $('#<%=lblCannotdelete.ClientID %>').text() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#divResult").html(msg.d);
                    if (msg.d == "True") {
                        generate("warning", "Please wait, we are refreshing system.", "bottomCenter")
                        setTimeout(function () { location.reload(); }, 1000);
                    } else {
                        generate("warning", msg.d, "bottomCenter")
                    }
                },
                error: function (e) {
                    $("#divResult").html("WebSerivce unreachable");
                }
            });
        }
        var icons = new Skycons();
        icons.set("rain", Skycons.RAIN);
        icons.play();
        function openPDF(url) {

            //url shoudl contain querystrings  >>  UID and TOID
            var uId = GetQueryParameterByName("UID", url);
            var toId = GetQueryParameterByName("TOID", url);
            var docId = GetQueryParameterByName("docId", url);
            //ajax call that give point to originator of document
            $.ajax({
                type: "POST",
                url: 'WebService1.asmx/InsertUpdatePoint',
                data: "{'uId':'" + uId + "', 'toId' :'" + toId + "', 'url' : '" + url + "','docId':'" + docId + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#divResult").html(msg.d);


                },
                error: function (e) {
                    $("#divResult").html("WebSerivce unreachable");
                }
            });


            //


            url = url.split("?")[0];
            var w = window.open(url, '_blank');
            w.focus();
        }
        function clearFields() {
            // document.getElementById("txtFileName").text("");
            document.getElementById("<%=txtFileName.ClientID %>").value = " ";
        }
        function hideAddNewDoc() {
            $(".btn.btn-default.black").click(function () { $(".modal-dialog").modal("hide"); });
            $(".modal-dialog").modal("hide");
            // document.getElementById('<%= txtFileName.ClientID %>').text = ""

        }
        function closeModel() { if (CheckValidations('chkdoc')) { $('#popupAddNewDoc').modal('hide'); } }
        function SubmitBtnForumLibrary() {


            if (!CheckValidations("chk")) return;
            var txtName = $('#<%=txtName.ClientID %>').val();
            var txtDesc = $('#<%=txtdes.ClientID %>').val();
            //var returnData = CommonAJSON("Library.aspx/BtnSubmitAJAX", '{txtName:"' + txtName + '",txtDesc:"' + txtDesc + '"}');
            var ReturnData;
            $.ajax({
                type: "POST",
                cache: false,
                async: true,
                url: "Library.aspx/BtnSubmitAJAX",
                data: '{txtName:"' + txtName + '",txtDesc:"' + txtDesc + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    ReturnData = response.d;
                    if (ReturnData == "success") {
                        $('#<%=lblMsg.ClientID %>').val('#<%=CommonModule.msgProblemInsertRecord%>');
                        alert('<%=CommonModule.msgRecordInsertedSuccessfully%>');
                        clearField();
                        $("#btnClose").click();
                        $('#<%= refresh.ClientID%>').click();
                        setTimeout(function () {
                            //$('#<%= refresh.ClientID%>').click(); 
                        }, 5000);

                    }
                },
                failure: function (msg) {

                    alert(msg);
                    ReturnData = msg;
                }
            });

        }
        function clearField() {
            $('#<%=txtName.ClientID %>').val("");
            $('#<%=txtdes.ClientID %>').val("");
        }

        var usertype
        function HideDeleteButon() {
            usertype = '<%=UserType%>';
            if (usertype == "3") {

                //  $(".deletebtn").hide();
            }
        }
        var searchToggle = false;
        function GetAdvanceSettings() {
            var clas = $(".searchResultBox").attr("class");

            $(".filterBox").slideToggle(1000);
            if (clas.indexOf("col-md-12") >= 0) {
                // debugger;
                $(".searchResultBox").removeClass("col-md-12");
                $(".searchResultBox").addClass("col-md-8");

                //$(".filterBox").show();
                searchToggle = false;

            } else {
                searchToggle = true;
                //$(".filterBox").hide();
                $(".searchResultBox").removeClass("col-md-8");
                $(".searchResultBox").addClass("col-md-12");
            }
            //return true;
            //$.confirm({
            //    title: 'Advance Filter ', content: $("#Ul1").html(), //autoClose: 'cancel|10000',
            //    //confirm: function () {
            //    //   // alert('confirmed');
            //    //},
            //    columnClass: 'col-md-4 col-md-offset-10',
            //    cancel: function () {
            //        //alert('canceled');
            //    }
            //});

        }



    </script>
    <script type="text/javascript">
        function getFileData(a) {

            //alert('hi');
            debugger;

            $('#ContentPlaceHolder1_Label23').text(a.value);

        }

</script>

</asp:Content>
