﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using System.Configuration;
using starteku_BusinessLogic.Model;
using startetku.Business.Logic;
using System.Web.UI.HtmlControls;
using System.Threading;
using System.Globalization;

public partial class Organisation_View_requestComp : System.Web.UI.Page
{
    string temp = "0";
    static string temp1 = "0";
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }

        if (!IsPostBack)
        {

            Employee.InnerHtml = GetLocalResourceObject("Welcome.Text").ToString() + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                temp = Convert.ToString(Request.QueryString["id"]);
                temp1 = temp;
                Literal1.Text = GetLocalResourceObject("EditRequest.Text").ToString();//"Edit Request";
            }
            else if (!String.IsNullOrEmpty(Request.QueryString["view"]))
            {
                temp = Convert.ToString(Request.QueryString["view"]);
                temp1 = temp;
                btnback.Visible = true;
                btnCancel.Visible = false;
                btnsubmit.Visible = false;

            }
            else if (!String.IsNullOrEmpty(Request.QueryString["rid"]))
            {
                temp = Convert.ToString(Request.QueryString["rid"]);
                temp1 = temp;
                Literal1.Text = GetLocalResourceObject("EditRequest.Text").ToString();//"Edit Request";
            }
            else if (!String.IsNullOrEmpty(Request.QueryString["Vw"]))
            {
                temp = Convert.ToString(Request.QueryString["Vw"]);
                temp1 = temp;
                btnback.Visible = true;
                btnCancel.Visible = false;
                btnsubmit.Visible = false;


                //txtComment.Enabled = false;

            }
            else if (!String.IsNullOrEmpty(Request.QueryString["not"]))
            {
                temp = Convert.ToString(Request.QueryString["not"]);
                temp1 = temp;
                updatenotification();
            }
            GetAllEmployeeList();
            //getData();
        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion

    #region method
    public void getData()
    {
        //CompetenceMasterBM obj = new CompetenceMasterBM();
        //obj.GetAllCategoryBypenddingrequest(Convert.ToInt32(temp));
        //DataSet ds = obj.ds;
        //{
        //    if (ds.Tables[0].Rows.Count > 0)
        //    {
        //        Accordion1.DataSource = ds.Tables[0].DefaultView;
        //        Accordion1.DataBind();

        //        if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[1].Rows[0]["userFirstName"])))
        //        {
        //            spname.InnerHtml = Convert.ToString(ds.Tables[1].Rows[0]["userFirstName"]);
        //        }

        //    }
        //}

    }
    protected void InsertNotification()
    {
        if (!String.IsNullOrEmpty(Request.QueryString["id"]))
        {
            temp = Convert.ToString(Request.QueryString["id"]);
            temp1 = temp;
        }
        else if (!String.IsNullOrEmpty(Request.QueryString["not"]))
        {
            temp = Convert.ToString(Request.QueryString["not"]);
            temp1 = temp;
        }
        if (!String.IsNullOrEmpty(Request.QueryString["rid"]))
        {
            temp = Convert.ToString(Request.QueryString["rid"]);
            temp1 = temp;
        }

        UserBM obj1 = new UserBM();
        obj1.SetUserId = Convert.ToInt32(temp);
        obj1.GetUserSettingById();
        DataSet ds1 = obj1.ds;
        if (ds1.Tables[0].Rows.Count > 0)
        {
            if (!String.IsNullOrEmpty(Convert.ToString(ds1.Tables[0].Rows[0]["SetInvitationNotification"])))
            {
                if (Convert.ToBoolean(ds1.Tables[0].Rows[0]["SetNewFileUploadwarning"]))
                {

                    NotificationBM obj = new NotificationBM();
                    obj.notsubject = "Has Accepted Competence.";
                    obj.notUserId = Convert.ToInt32(Session["OrgUserId"]);
                    obj.notIsActive = true;
                    obj.notIsDeleted = false;
                    obj.notCreatedDate = DateTime.Now;
                    obj.notCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
                    obj.notpage = "CompetenceSkill.aspx?id=" + Convert.ToString(Session["OrgUserId"]);
                    obj.nottype = "UserAcceptCompetence-Notification";
                    obj.notToUserId = Convert.ToInt32(temp);
                    obj.InsertNotification();
                    sendmail(obj.notToUserId);
                }

            }
        }
    }
    protected void updatenotification()
    {
        NotificationBM obj = new NotificationBM();
        obj.notIsActive = false;
        obj.notUpdatedDate = DateTime.Now;
        obj.notUserId = Convert.ToInt32(Request.QueryString["not"]);
        obj.nottype = "Notification";
        obj.UpdateNotification();
    }
    protected void SetDefaultMessage()
    {
        // lblMsg.Text = GetLocalResourceObject("lblHelloWorld.Text").ToString();

        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["msg"])))
        {
            if (Convert.ToString(Request.QueryString["msg"]) == "del")
            {
                //lblMsg.Text = CommonModule.msgRecordHasBeenDeletedSuccessfully;
                //lblMsg.Text = CommonMessages.msgRecordHasBeenDeletedSuccessfully;
                lblMsg.Text = GetLocalResourceObject("msgRecordHasBeenDeletedSuccessfully.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "ins")
            {
                //lblMsg.Text = CommonMessages.msgRecordInsertedSuccessfully;
                lblMsg.Text = GetLocalResourceObject("msgRecordInsertedSuccessfully.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "upd")
            {
                //lblMsg.Text = CommonMessages.msgRecordUpdatedSuccss;
                lblMsg.Text = GetLocalResourceObject("msgRecordUpdatedSuccss.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
          
        }

    }
    protected void GetAllEmployeeList()
    {
        try
        {
            EmployeeSkillBM obj = new EmployeeSkillBM();
            obj.skillIsActive = true;
            obj.skillIsDeleted = false;
            if (Convert.ToString(Session["OrguserType"]) == "1")
            {
                obj.skillStatus = 0;
            }
            else
            {
                obj.skillStatus = 3;
            }
            obj.skillUserId = Convert.ToInt32(temp);
            obj.GetViewSkillRequest();

            DataSet ds = obj.ds;
            ViewState["data"] = ds;
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvGrid.DataSource = ds.Tables[0];
                    gvGrid.DataBind();
                    gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;

                    //txtComment.Text = Convert.ToString(ds.Tables[0].Rows[0]["skillComment"]);
                    var isPending = false;
                    var isCancel = false;
                    for (var i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["skillStatus"])))
                        {
                            Int32 status = Convert.ToInt32(ds.Tables[0].Rows[i]["skillStatus"]);

                            if (status == 3 && isCancel == false)
                            {
                                ddlsttus.SelectedValue = "3";
                                isPending = true;
                                // Label2.Text = "Pendding";
                            }
                            else if (status == 2)
                            {
                                isCancel = true;
                                ddlsttus.SelectedValue = "2";
                                continue;
                            }
                            else if (status == 1 && isPending == false)
                            {
                                ddlsttus.SelectedValue = "1";
                                // Label2.Text = "Accepet";
                            }
                        }
                    }

                    hdnStatus12.Value = ddlsttus.SelectedValue;
                    if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[1].Rows[0]["userFirstName"])))
                    {
                        spname.InnerHtml = Convert.ToString(ds.Tables[1].Rows[0]["userFirstName"]);
                    }
                }
                else
                {
                    gvGrid.DataSource = null;
                    gvGrid.DataBind();
                }
            }
            else
            {
                gvGrid.DataSource = null;
                gvGrid.DataBind();
            }
        }
        catch
        {

        }
    }
    protected void sendmail(Int32 touserid)
    {
        #region Send mail
        // String subject = "Status Competence for " + Convert.ToString(Session["OrgUserName"]);

        Template template = CommonModule.getTemplatebyname1("Status Competence", touserid);
        string confirmMail = template.TemplateName;
        if (!String.IsNullOrEmpty(confirmMail))
        {

            string subject = GetLocalResourceObject("StatusCompetenceSubjectEngResource.Text").ToString();
            if (template.Language == "Danish")
            {
                subject = GetLocalResourceObject("StatusCompetenceSubjectDNResource.Text").ToString();
            }

            UserBM Cust = new UserBM();
            Cust.userId = Convert.ToInt32(temp);
            // Cust.SelectmanagerByUserId();
            Cust.SelectPasswordByUserId();
            DataSet ds = Cust.ds;
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    UserBM objAtt = new UserBM();
                    objAtt.userIsActive = true;
                    objAtt.userIsDeleted = false;
                    objAtt.userId = Convert.ToInt32(touserid);
                    objAtt.userCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
                    objAtt.GetEmployeeDetailbyPdf();
                    DataSet ds1 = objAtt.ds;
                    #region Competence
                    String Competence = "";
                    if (ds1.Tables[1].Rows.Count > 0)
                    {
                        if (template.Language == "Danish")
                        {
                            ds1.Tables[1].Columns["comCompetence"].ColumnName = "abcd";
                            ds1.Tables[1].Columns["comCompetenceDN"].ColumnName = "comCompetence";

                        }

                        Competence = "<table border='0' style='font-size: 20px;' width='100%'>";
                        //Competence = Competence + "<tr> <td>&nbsp;</td><td>"+ Convert.ToString(Session["hdnBasic"])+"</td><td>"+Convert.ToString(Session["hdnActual"])+"</td><td>"+Convert.ToString(Session["hdnTarget"])+"</td></tr>";
                        for (int i = 0; i <= ds1.Tables[1].Rows.Count - 1; i++)
                        {

                            Competence = Competence + "<tr style='font-family: Verdana,Arial,Helvetica,sans-serif; color: black; font-size: 16px;'><td style='width: 38%;'>" + Convert.ToString(ds1.Tables[1].Rows[i]["comCompetence"]) + "</td><td style='text-align:center;'>" + Convert.ToString(ds1.Tables[1].Rows[i]["skillLocal"]) + "</td>"
                                                    + "</td><td style='text-align:center;'>" + Convert.ToString(ds1.Tables[1].Rows[i]["skillAchive"])
                                                  + "</td><td style='text-align:center;'>" + Convert.ToString(ds1.Tables[1].Rows[i]["skilltarget"]) + "</td></tr>";
                        }
                        Competence = Competence + "</table>";
                    }

                    #endregion
                    String name = Convert.ToString(ds.Tables[0].Rows[0]["userFirstName"]);
                    string fromid = Convert.ToString(ConfigurationManager.AppSettings["fromid"]);
                    String empname = Convert.ToString(Session["OrgUserName"]);
                    String status = ddlsttus.SelectedValue;
                    if (ddlsttus.SelectedValue == "1")
                    {
                        status = "Accept";
                    }
                    else if (ddlsttus.SelectedValue == "2")
                    {
                        status = "Cancel";
                    }
                    else if (ddlsttus.SelectedValue == "3")
                    {
                        status = "Pending";
                    }
                    string tempString = confirmMail;
                    tempString = tempString.Replace("###name###", name);
                    tempString = tempString.Replace("###empname###", empname);
                    tempString = tempString.Replace("###status###", status);
                    tempString = tempString.Replace("###Table###", Competence);
                    CommonModule.SendMailToUser(Convert.ToString(ds.Tables[0].Rows[0]["userEmail"]), subject, tempString, fromid, Convert.ToString(HttpContext.Current.Session["OrgCompanyId"]));
                }
            }
        }

        #endregion
    }
    //private DataTable GetAllCompetenceAddbyComCatId(int categoryId)
    //{
    //    EmployeeSkillBM obj = new EmployeeSkillBM();
    //    obj.skillIsActive = true;
    //    obj.skillIsDeleted = false;
    //    if (Convert.ToString(Session["OrguserType"]) == "1")
    //    {
    //        obj.skillStatus = 0;
    //    }
    //    else
    //    {
    //        obj.skillStatus = 3;
    //    }
    //    obj.skillUserId = Convert.ToInt32(temp);
    //    obj.comcategoryId = categoryId;
    //    obj.GetViewSkillRequestbyAccordion();
    //    DataSet ds = obj.ds;

    //    if (ds != null)
    //    {
    //        int selectTable = 0;spname

    //        if (ds.Tables[selectTable].Rows.Count > 0)
    //        {
    //            return ds.Tables[selectTable];

    //        }
    //        else
    //        {
    //            //chkList.Items.Clear();
    //            //ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownCategory, CommonModule.dropDownZeroValue));
    //        }

    //    }
    //    return null;
    //}
    #endregion

    #region Button
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        try
        {
            Boolean id = false;
            int i = 0;
            int status = Convert.ToInt32(ddlsttus.SelectedValue);
            //if (hdnStatus12.Value.ToString() == status.ToString())
            //{

            //    Response.Redirect("pending_helprequest.aspx");
            //}


            //foreach (var gvr in Accordion1.Panes)
            //{
            //    GridView gvGrid = new GridView();
            //    gvGrid = (GridView)gvr.FindControl("gvGrid");


            for (i = 0; i < gvGrid.Rows.Count; i++)
            {


                if (gvGrid.Rows[i].RowType == DataControlRowType.DataRow)
                {
                    //
                    DropDownList ddlstastus = (DropDownList)gvGrid.Rows[i].FindControl("ddlstastus");
                    
                    TextBox txtLocal = (TextBox)gvGrid.Rows[i].FindControl("txtLocal");
                    TextBox txtAchieve = (TextBox)gvGrid.Rows[i].FindControl("txtAchieve");
                    TextBox txtTarget = (TextBox)gvGrid.Rows[i].FindControl("txtTarget");
                    TextBox txtComment = (TextBox)gvGrid.Rows[i].FindControl("txtComment");

                    TextBox txtLongTerm = (TextBox)gvGrid.Rows[i].FindControl("txtLongTerm");
                    TextBox txtMaxLevel = (TextBox)gvGrid.Rows[i].FindControl("txtMaxLevel");
                    
                    HiddenField skillComId = (HiddenField)gvGrid.Rows[i].FindControl("skillComId");
                    HiddenField skillId = (HiddenField)gvGrid.Rows[i].FindControl("skillId");

                    //Get Original Achive value 

                    var skillid = Convert.ToInt32(skillId.Value);
                    var db = new startetkuEntities1();
                    var achive =
                        (from o in db.SkillMasters where o.skillId == skillid select o.skillAchive).FirstOrDefault();

                    if (Convert.ToInt32(txtLocal.Text) != 0)
                    {
                        EmployeeSkillBM obj = new EmployeeSkillBM();
                        obj.skillId = Convert.ToInt32(skillId.Value);
                        obj.skillStatus = status;
                        obj.skillUserId = 0;
                        obj.skillLocal = Convert.ToInt32(txtLocal.Text);
                        obj.skillAchive = Convert.ToInt32(txtAchieve.Text);
                        obj.skilltarget = Convert.ToInt32(txtTarget.Text);
                        obj.skillAcceptDate = DateTime.Now;
                        obj.skillComment = txtComment.Text;

                        obj.skillLongTermTarget = Convert.ToInt32(txtLongTerm.Text);
                        obj.skillMaxLevelTarget = Convert.ToInt32(txtMaxLevel.Text);


                        obj.UpdateSkillgRequestbymanager();
                        if (obj.ReturnBoolean == true)
                        {
                            id = true;
                            //Response.Redirect("pending_request.aspx?msg=upd");
                        }

                        if (obj.skillStatus == 1)
                        {
                            var skillmasterD =
                                (from o in db.SkillMasters where o.skillId == obj.skillId select o).FirstOrDefault();

                            var dbData =
                                (from o in db.PDPPointMasters
                                 where o.pdpComId == obj.skillComId && o.pdpUserId == skillmasterD.skillUserId
                                 select o).FirstOrDefault();

                            var user =
                                (from o in db.UserMasters where o.userId == skillmasterD.skillUserId select o).FirstOrDefault();

                            if (dbData != null)
                            {
                                continue;
                            }

                            PointBM pm = new PointBM();
                            var level = obj.skillLocal;
                            if (obj.skillAchive != null && obj.skillAchive > 0)
                            {
                                level = obj.skillAchive;
                            }
                            var pointName = "Level1_0";


                            switch (level)
                            {
                                case 2:
                                    pointName = "Level1_2";
                                    break;
                                case 3:
                                    pointName = "Level2_3";
                                    break;
                                case 4:
                                    pointName = "Level3_4";
                                    break;
                                case 5:
                                    pointName = "Level4_5";
                                    break;
                            }
                            if (achive == null || achive == 0)
                            {
                                pointName = "Level1_0";
                            }
                            var point = LogMasterLogic.GetPointByPointName(pointName, Convert.ToInt32(Session["OrgCompanyId"]));
                            var Description = string.Format("{0} got PDP point {1} for moving on level {2}", user.userFirstName, point, level);
                            if (skillmasterD != null)
                            {

                                if (user != null)
                                {
                                    pm.PDPPointMasterInsert(Convert.ToInt32(user.userId), Convert.ToInt32(skillComId.Value),
                                                            Convert.ToInt32(user.userCompanyId),
                                                            Convert.ToInt32(user.userCreateBy),
                                                            Description, Convert.ToString(level), point);
                                }
                            }
                        }
                    }
                }

                //}
            }
            //if (id == true && status.ToString()=="1")
            //{
            //    if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            //    {
            //        InsertNotification();
            //        Response.Redirect("pending_request.aspx?msg=upd");
            //    }
            //    else
            //    {
            //        InsertNotification();
            //        Response.Redirect("pending_helprequest.aspx?msg=upd");
            //    }
            //}
            //else
            //{
            //    Response.Redirect("pending_helprequest.aspx?msg=upd");
            //}
            var backpage =Convert.ToString( Session["OriginPage"]);
            Response.Redirect(backpage);
        }
        catch (Exception ex)
        {

        }
    }
    protected void btnback_click(object sender, EventArgs e)
    {
        //if (!String.IsNullOrEmpty(Request.QueryString["view"]))
        //{
        //    Response.Redirect("pending_request.aspx");
        //}
        //else
        //{
        //    Response.Redirect("pending_helprequest.aspx");
        //}
        var backpage = Convert.ToString(Session["OriginPage"]);
        Response.Redirect(backpage);
    }
    protected void btnCancel_click(object sender, EventArgs e)
    {
        //if (!String.IsNullOrEmpty(Request.QueryString["id"]))
        //{
        //    Response.Redirect("pending_request.aspx");
        //}
        //else
        //{
        //    Response.Redirect("pending_helprequest.aspx");
        //}
        var backpage = Convert.ToString(Session["OriginPage"]);
        Response.Redirect(backpage);
    }
    #endregion

    #region Grid Event
    //protected void Accordion1_ItemDataBound(object sender, AjaxControlToolkit.AccordionItemEventArgs e)
    //{
    //    if (e.ItemType == AjaxControlToolkit.AccordionItemType.Content)
    //    {
    //        String categoryID = ((HiddenField)e.AccordionItem.FindControl("txt_categoryID")).Value;
    //        var data = GetAllCompetenceAddbyComCatId(Convert.ToInt32(categoryID));
    //        if (data == null) return;
    //        DataSet ds = data.DataSet;
    //        ViewState["data"] = ds;
    //        GridView grd = new GridView();

    //        grd = (GridView)e.AccordionItem.FindControl("gvGrid");
    //        grd.DataSource = ds;
    //        grd.DataBind();
    //    }
    //}
    protected void gvGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        DataSet ds = (DataSet)ViewState["data"];
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            TextBox Local = (TextBox)e.Row.FindControl("txtLocal");
            if (!String.IsNullOrEmpty(Request.QueryString["view"]))
            {

                TextBox Achieve = (TextBox)e.Row.FindControl("txtAchieve");
                TextBox Target = (TextBox)e.Row.FindControl("txtTarget");
                TextBox txtComment = (TextBox)e.Row.FindControl("txtComment");
                DropDownList ddlstastus = (DropDownList)e.Row.FindControl("ddlstastus");

                //Local.ReadOnly = true;
                //Achieve.ReadOnly = true;
                //Target.ReadOnly = true;
                txtComment.Enabled = false;
                ddlsttus.Enabled = false;
                ddlstastus.Attributes.Add("disabled", "disabled");
                ddlsttus.Visible = false;
                Achieve.Enabled = false;
                Target.Enabled = false;
                lblStatus.Visible = false;

            }
            Local.Enabled = false;
            if (!String.IsNullOrEmpty(Request.QueryString["Vw"]))
            {

                TextBox Achieve = (TextBox)e.Row.FindControl("txtAchieve");
                TextBox Target = (TextBox)e.Row.FindControl("txtTarget");
                //Local.ReadOnly = true;
                //Achieve.ReadOnly = true;
                //Target.ReadOnly = true;
                TextBox txtComment = (TextBox)e.Row.FindControl("txtComment");
                DropDownList ddlstastus = (DropDownList)e.Row.FindControl("ddlstastus");

                txtComment.Enabled = false;
                //ddlstastus.Enabled = false;
                ddlsttus.Attributes.Add("disabled", "disabled");

                Achieve.Enabled = false;
                Target.Enabled = false;
            }
            // DropDownList ddlstastus = (DropDownList)e.Row.FindControl("ddlstastus");
            TextBox txtLocal = (TextBox)e.Row.FindControl("txtLocal");
            TextBox txtAchieve = (TextBox)e.Row.FindControl("txtAchieve");
            TextBox txtTarget = (TextBox)e.Row.FindControl("txtTarget");
            HiddenField skillComId = (HiddenField)e.Row.FindControl("skillComId");

            int index = e.Row.RowIndex;

          //  for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            for (int i = index; i < ds.Tables[0].Rows.Count; i = ds.Tables[0].Rows.Count)
            {
                if (Convert.ToInt32(skillComId.Value) == Convert.ToInt32(ds.Tables[0].Rows[i]["skillComId"]))
                {
                    if (!String.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[i]["skillAchive"])))
                    {
                        if (Convert.ToString(ds.Tables[0].Rows[i]["skillAchive"]) == "0")
                        {
                            txtAchieve.Text = txtLocal.Text;
                        }
                        else
                        {
                            txtAchieve.Text = Convert.ToString(ds.Tables[0].Rows[i]["skillAchive"]);
                        }
                    }
                    else
                    {
                        txtAchieve.Text = txtLocal.Text;
                    }
                    if (!String.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[i]["skilltarget"])))
                    {
                        if (Convert.ToString(ds.Tables[0].Rows[i]["skilltarget"]) == "0")
                        {
                            txtTarget.Text = Convert.ToString(Convert.ToInt32(txtAchieve.Text));
                            if (Convert.ToInt32(txtTarget.Text) > 5)
                            {
                                txtTarget.Text = "5";
                            }
                        }
                        else
                        {
                            txtTarget.Text = Convert.ToString(ds.Tables[0].Rows[i]["skilltarget"]);
                        }

                    }
                    else
                    {
                        txtTarget.Text = Convert.ToString(Convert.ToInt32(txtAchieve.Text));
                    }
                    //
                    // ddlstastus.SelectedValue = Convert.ToString(ds.Tables[0].Rows[i]["skillStatus"]);

                }
            }


        }
    }
    #endregion

    protected void txtTarget_TextChanged(object sender, EventArgs e)
    {
        try
        {
            TextBox thisTextBox = (TextBox)sender;
            GridViewRow currentRow = (GridViewRow)thisTextBox.Parent.Parent;
            int rowindex = 0;
            rowindex = currentRow.RowIndex;
            TextBox txtAchieve = (TextBox)currentRow.FindControl("txtAchieve");
            TextBox txtTarget = (TextBox)currentRow.FindControl("txtTarget");

            Int32 Achieve = Convert.ToInt32(txtAchieve.Text);
            Int32 Target = Convert.ToInt32(txtTarget.Text);

            if (Achieve > Target)
            {
                txtTarget.Text = Convert.ToString(Achieve);
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('Please Enter greater than Achieve!');</script>", false);
            }
            if (Target <= 5)
            {
            }
            else
            {
                txtTarget.Text = Convert.ToString(Achieve);
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('Please Enter Less than Scale!');</script>", false);
            }
            if (Achieve == 5)
            {
                txtTarget.Text = txtAchieve.Text;
            }
        }
        catch
        {

        }
    }
    protected void txtAchieve_TextChanged(object sender, EventArgs e)
    {
        try
        {
            TextBox thisTextBox = (TextBox)sender;
            GridViewRow currentRow = (GridViewRow)thisTextBox.Parent.Parent;
            int rowindex = 0;
            rowindex = currentRow.RowIndex;
            TextBox txtAchieve = (TextBox)currentRow.FindControl("txtAchieve");
            TextBox txtTarget = (TextBox)currentRow.FindControl("txtTarget");

            Int32 Achieve = Convert.ToInt32(txtAchieve.Text);
            Int32 Target = Convert.ToInt32(txtTarget.Text);

            if (Achieve <= 0)
            {
                txtAchieve.Text = "";
                txtTarget.Text = "";
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('Please Enter greater than 0 !');</script>", false);

            }
            else
            {
                txtTarget.Text = Convert.ToString(Achieve);
            }
            if (Achieve > 5)
            {
                txtTarget.Text = "5";
                txtAchieve.Text = "5";
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('Please Enter Less than OR Equal 5 !');</script>", false);
            }
            else
            {
                txtTarget.Text = Convert.ToString(Achieve);
            }
            if (Achieve == 5)
            {
                txtTarget.Text = txtAchieve.Text;
            }
            //if (Achieve > Target)
            //{
            //    txtTarget.Text = txtAchieve.Text;
            //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('Please Enter greater than Achieve!');</script>", false);
            //}
            //if (Target <= 5)
            //{
            //}
            //else
            //{
            //    txtTarget.Text = txtAchieve.Text;
            //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('Please Enter Less than Scale!');</script>", false);
            //}

        }
        catch
        {

        }
    }

    #region
    [WebMethod(EnableSession = true)]
    public static comment[] GetcommentById(string comId)
    {

        DataTable dt = new DataTable();
        List<comment> cmnt = new List<comment>();

        if (string.IsNullOrWhiteSpace(comId))
        {
            return cmnt.ToArray();
        }
        CompetenceBM obj = new CompetenceBM();


        obj.GetCompetenceCommentById(Convert.ToInt32(comId), Convert.ToInt32(temp1));
        DataSet ds = obj.ds;




        if (ds.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow dtrow in ds.Tables[0].Rows)
            {
                comment comenmt = new comment();
                comenmt.compId = dtrow["compId"].ToString();
                comenmt.name = dtrow["name"].ToString();
                comenmt.Image = dtrow["userImage"].ToString();
                comenmt.comuserid = dtrow["comuserid"].ToString();
                comenmt.compcomment = dtrow["compcomment"].ToString();
                comenmt.comcreatedate = dtrow["comcreatedate"].ToString();

                cmnt.Add(comenmt);
            }
        }
        return cmnt.ToArray();
    }

    [WebMethod(EnableSession = true)]
    public static string insertmessage(string message, string comid)
    {
        string msg = string.Empty;
        CompetenceBM obj = new CompetenceBM();
        Int32 compid = Convert.ToInt32(comid);
        Int32 userid = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        Int32 touserid = Convert.ToInt32(temp1);

        if (obj.InsertCompetenceComment(compid, userid, message, touserid))
        {

            msg = "true";
        }
        else
        {
            msg = "false";
        }


        return msg;
    }


    public class comment
    {
        public string compId { get; set; }

        public string name { get; set; }
        public string comuserid { get; set; }
        public string Image { get; set; }
        public string compcomment { get; set; }
        public string comcreatedate { get; set; }

    }
    #endregion
    #region Culture
    protected override void InitializeCulture()
    {
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Danish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion
}