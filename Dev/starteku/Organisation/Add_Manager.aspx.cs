﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using System.Configuration;
using System.Threading;
using System.Globalization;
using startetku.Business.Logic;


public partial class Organisation_Add_Manager : System.Web.UI.Page
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }

        if (!IsPostBack)
        {
            //GetCheckBoxListData();
            //GetAllJobtype();
            //GetAllDepartments();
            Employee.InnerHtml = CommonMessages.Welcome + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            GetAllEmployeeList();
            //EmployeeListArchiveAll();
            SetDefaultMessage();
        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion

    #region DropDown Event
    protected void ddlcompetence_SelectedIndexChanged(object sender, EventArgs e)
    {
       // GetCheckBoxListData();
    }
    protected void ddljobtype_SelectedIndexChanged(object sender, EventArgs e)
    {
       // GetCheckBoxListData();

    }
    #endregion

    #region method

    //protected void txtEmail_TextChanged(object sender, EventArgs e)
    //{
    //    UserBM obj2 = new UserBM();
    //    obj2.useremailCheckDuplication(txtEmail.Text, -1);
    //    String returnMsg = obj2.ReturnString;
    //    if (returnMsg == "")
    //    { lblemailvalid.Text = ""; }
    //    else
    //    {
    //        if (returnMsg == "userEmail")
    //        {
    //            lblemailvalid.Text = CommonModule.msgEmailAlreadyExists;
    //        }

    //    }
    //}
    //protected void GetCheckBoxListData()
    //{
    //    CompetenceMasterBM obj = new CompetenceMasterBM();
    //    obj.catIsActive = true;
    //    obj.catIsDeleted = false;
    //    if (string.IsNullOrEmpty(ddlDepartment.SelectedValue)) return;
    //    obj.comDivId = Convert.ToInt32(ddlDepartment.SelectedValue);
    //    obj.comJobtype = Convert.ToInt32(ddljobtype.SelectedValue);
    //    obj.GetAllCategoryByJobTypeId_DivId();
    //    // lblDivIdName.Text = ddlDepartment.SelectedItem.Text;

    //    DataSet ds = obj.ds;

    //    if (ds != null)
    //    {


    //        if (ds.Tables[0].Rows.Count > 0)
    //        {
    //            repCompCat.DataSource = ds.Tables[0];
    //            //chkList.DataTextField = "catName";
    //            //chkList.DataValueField = "catId";
    //            repCompCat.DataBind();
    //            //chkList.SelectedIndex = 0;


    //            //foreach (ListItem li in chkList.Items)
    //            //{
    //            //    li.Selected = true;
    //            //}
    //        }
    //        else
    //        {
    //            //chkList.Items.Clear();
    //            //ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownCategory, CommonModule.dropDownZeroValue));
    //        }

    //    }
    //    else
    //    {
    //        //chkList.Items.Clear();
    //    }
    //}

    protected void SetDefaultMessage()
    {
        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["msg"])))
        {
            if (Convert.ToString(Request.QueryString["msg"]) == "del")
            {
                lblMsg.Text = CommonModule.msgRecordHasBeenDeletedSuccessfully;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "ins")
            {
                lblMsg.Text = CommonModule.msgRecordInsertedSuccessfully;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "upd")
            {
                lblMsg.Text = CommonModule.msgRecordUpdatedSuccss;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "arc")
            {
                lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
        }

    }
    protected void GetAllEmployeeList()
    {
        //Session["UserId"]
        //if (!String.IsNullOrEmpty(Request.QueryString["org"]))

        UserBM obj = new UserBM();
        obj.userIsActive = true;
        obj.userIsDeleted = false;
        obj.userType = 3;
        //obj.userCompanyId = Convert.ToInt32(Session["UserId"]);
        obj.userCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        //obj.GetAllEmployee();
        obj.userCreateBy = Convert.ToString(Session["OrgUserId"]);
        obj.GetAllEmployeecreatebymanager();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvGrid.DataSource = ds.Tables[0];
                gvGrid.DataBind();

                gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvGrid.DataSource = null;
                gvGrid.DataBind();
            }
        }
        else
        {
            gvGrid.DataSource = null;
            gvGrid.DataBind();
        }
    }
    //protected void EmployeeListArchiveAll()
    //{
    //    UserBM obj = new UserBM();
    //    obj.userIsActive = false;
    //    obj.userIsDeleted = false;
    //    obj.userType = 3;
    //    obj.userCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
    //    obj.userCreateBy = Convert.ToString(Session["OrgUserId"]);
    //    obj.GetAllEmployeecreatebymanager();
    //   // obj.GetAllEmployee();
    //    DataSet ds = obj.ds;

    //    if (ds != null)
    //    {
    //        if (ds.Tables[0].Rows.Count > 0)
    //        {
    //            gvArchive.DataSource = ds.Tables[0];
    //            gvArchive.DataBind();

    //            gvArchive.HeaderRow.TableSection = TableRowSection.TableHeader;
    //        }
    //        else
    //        {
    //            gvArchive.DataSource = null;
    //            gvArchive.DataBind();
    //        }
    //    }
    //    else
    //    {
    //        gvArchive.DataSource = null;
    //        gvArchive.DataBind();
    //    }
    //}
    //protected void GetAllJobtype()
    //{
    //    JobTypeBM obj = new JobTypeBM();
    //    obj.jobIsActive = true;
    //    obj.jobIsDeleted = false;
    //    obj.jobCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
    //    obj.GetAllJobType();
    //    DataSet ds = obj.ds;

    //    if (ds != null)
    //    {
    //        if (ds.Tables[0].Rows.Count > 0)
    //        {
    //            ddljobtype.Items.Clear();

    //            ddljobtype.DataSource = ds.Tables[0];
    //            ddljobtype.DataTextField = "jobName";
    //            ddljobtype.DataValueField = "jobId";
    //            ddljobtype.DataBind();

    //            ddljobtype.Items.Insert(0, new ListItem(CommonModule.dropDownSelectJobtype, CommonModule.dropDownZeroValue));
    //        }
    //        else
    //        {
    //            ddljobtype.Items.Clear();
    //            ddljobtype.Items.Insert(0, new ListItem(CommonModule.dropDownSelectJobtype, CommonModule.dropDownZeroValue));
    //        }
    //    }
    //    else
    //    {
    //        ddljobtype.Items.Clear();
    //        ddljobtype.Items.Insert(0, new ListItem(CommonModule.dropDownSelectJobtype, CommonModule.dropDownZeroValue));
    //    }

    //}
    //protected void GetAllDepartments()
    //{
    //    //DepartmentsBM obj = new DepartmentsBM();
    //    //obj.depIsActive = true;
    //    //obj.depIsDeleted = false;
    //    //obj.depCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
    //    //obj.GetAllDepartments();
    //    //DataSet ds = obj.ds;

    //    DivisionBM obj = new DivisionBM();
    //    obj.depIsActive = true;
    //    obj.depIsDeleted = false;
    //    obj.depCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
    //    obj.GetAllDivision();
    //    DataSet ds = obj.ds;

    //    if (ds != null)
    //    {
    //        if (ds.Tables[0].Rows.Count > 0)
    //        {
    //            ddlDepartment.Items.Clear();

    //            ddlDepartment.DataSource = ds.Tables[0];
    //            ddlDepartment.DataTextField = "divName";
    //            ddlDepartment.DataValueField = "divId";
    //            ddlDepartment.DataBind();

    //            ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownSelectDivisions, CommonModule.dropDownZeroValue));
    //        }
    //        else
    //        {
    //            ddlDepartment.Items.Clear();
    //            ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownSelectDivisions, CommonModule.dropDownZeroValue));
    //        }
    //    }
    //    else
    //    {
    //        ddlDepartment.Items.Clear();
    //        ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownSelectDivisions, CommonModule.dropDownZeroValue));
    //    }

    //}
    //protected void Insertuser()
    //{
    //    string filePath = "";
    //    UserBM obj2 = new UserBM();
    //    obj2.useremailCheckDuplication(txtEmail.Text, -1);
    //    String returnMsg = obj2.ReturnString;
    //    if (returnMsg == "")
    //    {
    //        UserBM obj = new UserBM();
    //        obj.userFirstName = txtName.Text;
    //        obj.userLastName = "";
    //        obj.userZip = "";
    //        obj.userAddress = txtaddress.Text;
    //        obj.userCountryId = Convert.ToInt32(0);
    //        obj.userStateId = Convert.ToInt32(0);
    //        //obj.userCityId = Convert.ToInt32(0);
    //        obj.userCityId = "";
    //        obj.usercontact = txtMobile.Text;
    //        obj.userEmail = txtEmail.Text;
    //        obj.userType = 3;

    //        //if (!String.IsNullOrEmpty(Request.QueryString["org"]))
    //        //{
    //        //    obj.userType = 2;
    //        //}
    //        //else if (!String.IsNullOrEmpty(Request.QueryString["id"]))
    //        //{
    //        //    obj.userType = 3;
    //        //}
    //        // obj.userCityId = Convert.ToInt32(ddlcity.SelectedValue);
    //        obj.userCreatedDate = DateTime.Now;
    //        obj.userIsActive = true;
    //        obj.userIsDeleted = false;
    //        obj.userCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
    //        //if (!String.IsNullOrEmpty(Request.QueryString["org"]))
    //        //{
    //        //    obj.userCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
    //        //}
    //        //else if (!String.IsNullOrEmpty(Request.QueryString["id"]))
    //        //{
    //        //    obj.userCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
    //        //}
    //        string pass = CommonModule.Generate_Random_password();
    //        obj.userPassword = CommonModule.encrypt(pass.Trim());

    //        //obj.userDOB = Convert.ToDateTime(CommonModule.FormatdtEnter(txtfromdate.Text));
    //        obj.userGender = "";
    //        obj.userImage = "ofile_img.png";
    //        //if (flupload1.HasFile)
    //        //{
    //        //    filePath = System.DateTime.Now.ToString("MMddyyhhmmss") + "_" + flupload1.PostedFile.FileName;
    //        //    flupload1.SaveAs(Server.MapPath("../") + "Log/upload/Userimage/" + System.IO.Path.GetFileName(filePath.Replace(' ', '0')));
    //        //    obj.userImage = System.IO.Path.GetFileName(filePath.Replace(' ', '0'));

    //        //}
    //        //else
    //        //{
    //        //    if (ddlgender.SelectedValue == "Male")
    //        //    {
    //        //        obj.userImage = "male.png";
    //        //    }
    //        //    else
    //        //    {//ViewState["userImage"]
    //        //        obj.userImage = "female.png";
    //        //    }
    //        //}
    //        obj.userLevel = 1;
    //        obj.userdepId = Convert.ToString(ddlDepartment.SelectedValue);
    //        obj.userJobType = Convert.ToInt32(ddljobtype.SelectedValue);
    //        obj.userCreateBy = Convert.ToString(Session["OrgUserId"]);



    //        // Get Cattegory list comma sap list. |Saurin | 20141216 |
    //        var category = string.Empty;
    //        var aa = new List<string>();
    //        foreach (RepeaterItem aItem in repCompCat.Items)
    //        {
    //            var repCompAdd = (Repeater)aItem.FindControl("repCompAdd");
    //            foreach (RepeaterItem bItem in repCompAdd.Items)
    //            {
    //                CheckBoxList chkDisplayTitle = (CheckBoxList)bItem.FindControl("chkList");
    //                Label hdnChkboxValue = (Label)bItem.FindControl("hdnChkboxValue");

    //                foreach (ListItem item in chkDisplayTitle.Items)
    //                {
    //                    if (item.Selected)
    //                    {
    //                        aa.Add(item.Value);
    //                        //string selectedValue = item.Value;
    //                    }
    //                }


    //                //if (!string.IsNullOrWhiteSpace(chkDisplayTitle.SelectedValue))
    //                //{

    //                //}
    //            }



    //        }
    //        category = string.Join(",", aa.ToArray());
    //        obj.UserCategory = category;


    //        obj.InsertOrganisation();
    //        //  DataSet ds = obj.ds;
    //        if (obj.ReturnBoolean == true)
    //        {
    //            sendmail(txtEmail.Text, txtName.Text, pass);
    //            clear();
    //            //if (!String.IsNullOrEmpty(Request.QueryString["org"]))
    //            //{
    //            //    string msg = CommonModule.msgSuccessfullyRegistered;
    //            //    redirectpage(msg, "EmployerList.aspx");
    //            //}
    //            //else
    //            //{
    //            //    string msg = CommonModule.msgSuccessfullyRegistered;
    //            //    redirectpage(msg, "EmployeeList.aspx");
    //            //}

    //        }
    //    }
    //    else
    //    {
    //        if (returnMsg == "userEmail")
    //            lblMsg.Text = CommonModule.msgEmailAlreadyExists;
    //        else
    //            lblMsg.Text = CommonModule.msgProblemInsertRecord;
    //    }
    //}
    protected void sendmail(string email, string name, string pass)
    {
        try
        {
            // Common.WriteLog("start");
            string subject = "Registration";
            string fromid = Convert.ToString(ConfigurationManager.AppSettings["fromid"]);
            CmsBM objMail = new CmsBM();
            objMail.cmsName = "Registration";
            objMail.SelectMailTemplateByName();
            DataSet dsMail = objMail.ds;
            if (dsMail.Tables[0].Rows.Count > 0)
            {
                //Common.WriteLog("start_in");
                string confirmMail = dsMail.Tables[0].Rows[0]["cmsDescription"].ToString();
                string tempString = confirmMail;
                tempString = tempString.Replace("###name###", name);
                tempString = tempString.Replace("###email###", email);
                tempString = tempString.Replace("###password###", pass);
                //SendMail(tempString, txtEmail.Text, txtFname.Text);
                CommonModule.SendMailToUser(email, subject, tempString, fromid, Convert.ToString(HttpContext.Current.Session["OrgCompanyId"]));
            }
        }
        catch (Exception ex)
        {
            Common.WriteLog("error in sendmail" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
        }

    }
    //protected void GetAllEmployeebyid(int id)
    //{
    //    UserBM obj = new UserBM();
    //    obj.userId = Convert.ToInt32(id);
    //    obj.GetAllEmployeebyid();
    //    DataSet ds = obj.ds;

    //    if (ds != null)
    //    {
    //        if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userFirstName"])))
    //            lblName.Text = Convert.ToString(ds.Tables[0].Rows[0]["userFirstName"]);

    //        if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userAddress"])))
    //            lblAddress.Text = Convert.ToString(ds.Tables[0].Rows[0]["userAddress"]);

    //        if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["usercontact"])))
    //            lblPhoneNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["usercontact"]);

    //        if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userEmail"])))
    //            lblEmail.Text = Convert.ToString(ds.Tables[0].Rows[0]["userEmail"]);

    //        //if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userPassword"])))
    //        //{
    //        //    string Password = CommonModule.decrypt(Convert.ToString(ds.Tables[0].Rows[0]["userPassword"]));
    //        //   // txtPassword.Attributes.Add("value", Password);
    //        //    lblPassword.Text = Password;
    //        //}

    //        if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userJobType"])))
    //            ddljobtype.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["userJobType"]);
    //        lblJob.Text = ddljobtype.SelectedItem.Text;

    //        if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userdepId"])))
    //            ddlDepartment.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["userdepId"]);
    //        lblDepartments.Text = ddlDepartment.SelectedItem.Text;
    //    }

    //}
    //protected void clear()
    //{
    //    txtName.Text = "";
    //    txtaddress.Text = "";
    //    txtMobile.Text = "";
    //    txtEmail.Text = "";
    //    ddlDepartment.SelectedValue = "0";
    //    ddljobtype.SelectedValue = "0";
    //    lblMsg.Text = "";
    //}

    //private DataTable GetAllCompetenceAddbyComCatId(int categoryId)
    //{
    //    CompetenceMasterBM obj = new CompetenceMasterBM();
    //    obj.catIsActive = true;
    //    obj.catIsDeleted = false;
    //    if (categoryId < 0) return null;
    //    obj.comDivId = Convert.ToInt32(ddlDepartment.SelectedValue);
    //    obj.comJobtype = Convert.ToInt32(ddljobtype.SelectedValue);
    //    obj.GetAllCompetenceAddbyComCatId(categoryId);
    //    // lblDivIdName.Text = ddlDepartment.SelectedItem.Text;

    //    DataSet ds = obj.ds;

    //    if (ds != null)
    //    {
    //        int selectTable = 0;

    //        if (ds.Tables[selectTable].Rows.Count > 0)
    //        {
    //            return ds.Tables[selectTable];

    //        }
    //        else
    //        {
    //            //chkList.Items.Clear();
    //            //ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownCategory, CommonModule.dropDownZeroValue));
    //        }

    //    }
    //    return null;
    //}
    #endregion//

    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        //Insertuser();
        GetAllEmployeeList();
    }
    protected void btnclose_click1(object sender, EventArgs e)
    {
        //clear();
    }
    #endregion

    #region grid Event
    protected void gvGrid_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "archive")
        {
            UserBM obj = new UserBM();
            obj.userId = Convert.ToInt32(e.CommandArgument);
            obj.userIsActive = false;
            obj.userIsDeleted = false;
            obj.EmployeeStatusUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    GetAllEmployeeList();
                    //EmployeeListArchiveAll();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }
        else if (e.CommandName == "View")
        {
            //GetAllEmployeebyid(Convert.ToInt32(e.CommandArgument));

            mpe.Show();

        }
        else if (e.CommandName == "Password")
        {
            GetEmployeePassword(Convert.ToInt32(e.CommandArgument));
            // Togle(false);
            Modal1.Show();
        }
    }
    public void Togle(bool value)
    {
        //lblAddress.Visible = value;
        //lblAddressLABEL.Visible = value;
        //lblEmail.Visible = value;
        //lblEmailLABEL.Visible = value;
        //lblJob.Visible = value;
        //lblJobLABEL.Visible = value;


    }
    protected void GetEmployeePassword(int id)
    {
        UserBM obj = new UserBM();
        obj.userId = Convert.ToInt32(id);
        obj.GetAllEmployeebyid();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            //if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userFirstName"])))
            //    txtName.Text = Convert.ToString(ds.Tables[0].Rows[0]["userFirstName"]);

            //if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userPassword"])))
            //    txtPassword.Text = Convert.ToString(ds.Tables[0].Rows[0]["userPassword"]);
            lblpassword.Text = CommonModule.decrypt(Convert.ToString(ds.Tables[0].Rows[0]["userPassword"]));
            //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('" + ds.Tables[0].Rows[0]["userPassword"] + "')</script>", false);

            //if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userPassword"])))
            //{
            //    string Password = CommonModule.decrypt(Convert.ToString(ds.Tables[0].Rows[0]["userPassword"]));
            //   // txtPassword.Attributes.Add("value", Password);
            //    lblPassword.Text = Password;
            //}


        }

    }


   
    protected void gvGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvGrid.PageIndex = e.NewPageIndex;
        GetAllEmployeeList();
    }
    #endregion

    

    #region Culture
    protected override void InitializeCulture()
    {
        //string language = "French";
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Danish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();
            //if (!string.IsNullOrEmpty(language))
            //{
            //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
            //    {
            //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
            //        {
            //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
            //        }
            //    }
            //    //if (language.EndsWith("Danish")) languageId = "da-DK";
            //    //else languageId = "en-GB";
            //    
            //}
            SetCulture(languageId);
        }
        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion
}