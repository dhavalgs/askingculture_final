﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using startetku.Business.Logic;

public partial class Organisation_CompanyDoc : System.Web.UI.Page
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            //Department.InnerHtml = "Welcome   " + Convert.ToString(Session["OrgUserName"]) + "!";
            GetAllDocuments();

        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion

    #region Methods
    public void GetAllDocuments()
    {
        try
        {
            companyDocBM obj = new companyDocBM();
            obj.comIsActive = true;
            obj.GetAllCompanyDocument();
            DataSet ds = obj.ds;
            if (ds.Tables[0].Rows.Count > 0)
            {
                rep_document.DataSource = ds;
                rep_document.DataBind();
            }
        }
        catch (Exception ex)
        {

        }
    }



    #endregion

    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        String filePath = "";
        companyDocBM obj = new companyDocBM();
        obj.comTitle = txttitle.Text;
        obj.comDescription = txtDESCRIPTION.Text;
        //= FileUpload1.FileName;
        if (FileUpload1.HasFile)
        {
            filePath = System.DateTime.Now.ToString("MMddyyhhmmss") + "_" + FileUpload1.PostedFile.FileName;
            FileUpload1.SaveAs(Server.MapPath("../") + "Log/upload/CompanyDoc/" + System.IO.Path.GetFileName(filePath.Replace(' ', '0')));
            obj.comattachment = System.IO.Path.GetFileName(filePath.Replace(' ', '0'));
        }
        obj.comJobType = 0;
        obj.comUserId = Convert.ToInt32(Session["OrgUserId"]);
        obj.comIsActive = true;
        obj.comIsDeleted = false;
        obj.comCreatedDate = DateTime.Now;
        obj.comCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.comDepId = 0;

        obj.InsertcompanyDoc();
        if (obj.ReturnBoolean == true)
        {
            // Response.Redirect("OrgDepartmentsList.aspx?msg=ins");
        }
        GetAllDocuments();

    }

    protected void btn_delete_Click(object sender, EventArgs e)
    {
        LinkButton lb = (LinkButton)sender;
        int id = Convert.ToInt32(lb.CommandName);
        companyDocBM obj = new companyDocBM();
        obj.comId = id;
        obj.DeleteCompanyDocumentById();
        GetAllDocuments();
    }

    #endregion

}