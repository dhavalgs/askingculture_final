﻿<%@ Page Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true" CodeFile="OrgTeam.aspx.cs" Inherits="Organisation_OrgTeam" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
<style type="text/css">
        .chat-widget-head h4
        {
            float: none !important;
        }
         .form-control {   
    text-transform: none;    
}
label
        {
            display: inline-block;
            font-size: 14px;
            font-weight: inherit;
            margin-bottom: 5px;
        }
.inline-form input, .inline-form textarea {
    font-size: 15px;    
}
    </style>
    <style type="text/css">
        
        body
        {
            font-family: Geneva,Arial,Helvetica,sans-serif;
        }
        #ContentPlaceHolder1_chkList input
        {
            width: 33px;
            margin-bottom: 0px !important;
        }
        #ContentPlaceHolder1_chkList label
        {
            margin-top: 2px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<style type="text/css">
        .scroll_checkboxes
        {
            height: 120px;
            width: 270px;
            padding: 5px;
            overflow: auto;
            border: 1px solid #ccc;
        }
        
        .FormText
        {
            font-size: 11px;
            font-family: tahoma,sans-serif;
        }
    </style>
        <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            SetExpandCollapse();
            setTimeout(function () {
                //  ExampanCollapsMenu();
                // tabMenuClick();
                $(".OrgJobTypeList").addClass("active_page");
            }, 500);
        });
    </script>
    <script language="javascript">

        var color = 'White';

        function changeColor(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=chkList.ClientID%>");
            if (color == '') {
                color = getRowColor();
            }
            if (obj.checked) {
                rowObject.style.backgroundColor = '#A3B1D8';
            }
            else {
                rowObject.style.backgroundColor = color;
                color = 'White';
            }

            // private method
            function getRowColor() {
                if (rowObject.style.backgroundColor == 'White') return parentTable.style.backgroundColor;
                else return rowObject.style.backgroundColor;
            }

        }

        // This method returns the parent row of the object

        function getParentRow(obj) {
            do {
                obj = obj.parentElement;
            }
            while (obj.tagName != "TR")
            return obj;
        }


        function TurnCheckBoixGridView(id) {
            var frm = document.forms[0];

            for (i = 0; i < frm.elements.length; i++) {
                if (frm.elements[i].type == "checkbox" && frm.elements[i].id.indexOf("<%= chkList.ClientID %>") == 0) {
                    frm.elements[i].checked = document.getElementById(id).checked;
                }
            }
        }

        function SelectAll(id) {

            var parentTable = document.getElementById("<%=chkList.ClientID%>");
            var color

            if (document.getElementById(id).checked) {
                color = '#A3B1D8'
            }
            else {
                color = 'White'
            }

            for (i = 0; i < parentTable.rows.length; i++) {
                parentTable.rows[i].style.backgroundColor = color;
            }
            TurnCheckBoixGridView(id);

        }



    </script>
    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" 
        meta:resourcekey="lblMsgResource1"></asp:Label>
    <div class="col-md-6">
        <div class="heading-sec">
            <%-- <h1>
                <asp:Label runat="server" ID="lblDataDisplayTitle" Text="Job Type"></asp:Label>
                <i>Welcome to Flat Lab </i></h1>--%>
            <h1>
                <asp:Label runat="server" meta:resourcekey="jobtype"></asp:Label> <i><span runat="server" id="JobType"></span></i>
            </h1>
        </div>
    </div>
    <br />
    <br />
    <div class="col-md-6 range ">
        <%--<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
            <i class="fa fa-calendar-o icon-calendar icon-large"></i>
            <span>August 5, 2014 - September 3, 2014</span>  <b class="caret"></b>
        </div>--%>
    </div>
    <div class="col-md-12">
        <div class="chat-widget widget-body" style="background: #fff;">
            <div class="chat-widget-head yellow yellow-radius">
                <h4>
                    <asp:Literal ID="Literal4" runat="server" meta:resourcekey="Edit_Job_Type" />
                </h4>
            </div>
            <div class="indicatesRequireFiled">
                <i>* <%--<%= CommonMessages.Indicatesrequiredfield %>--%> <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Indicatesrequiredfield" enableviewstate="false"/> </i>
            </div>
            
            <div class="col-md-6" style="width:100%";>
                <div class="inline-form">
                    <label class="c-label">
                       <asp:Literal ID="Literal5" runat="server" meta:resourcekey="Team_Name" /></label>
                    <asp:TextBox runat="server" ID="txtTeamName" MaxLength="50" 
                        CssClass="form-control" meta:resourcekey="txtJobTypeNameResource1" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtTeamName"
                        ErrorMessage="Please enter Team Name." CssClass="commonerrormsg" Display="Dynamic"
                        ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator5Resource1" style="float: left;"></asp:RequiredFieldValidator>
                </div>

                </div>

            <div class="col-md-6" style="width:100%";>
                <div class="inline-form">
                    <label class="c-label">
                         <asp:Literal ID="Literal6" runat="server" meta:resourcekey="TeamNameInDanish" /></label>
                    
                    <asp:TextBox runat="server" ID="txtTeamNameDN" MaxLength="50" 
                        CssClass="form-control" meta:resourcekey="txtTeamNameDnResource1" placeholder="Enter Team Name in Danish." />
                    
                    <asp:RequiredFieldValidator ID="txtTeamNameDnRequired" runat="server" ControlToValidate="txtTeamNameDN"
                        ErrorMessage="Please enter Team Name In Danish." CssClass="commonerrormsg" Display="Dynamic"
                        ValidationGroup="chk" meta:resourcekey="txtTeamNameDnRequiredResource1" style="float: left;"></asp:RequiredFieldValidator>
                </div>

                </div>

                <div class="col-md-6" style="width:100%";>
                <div class="widget-body custom-form" style="margin-bottom: 10px; text-transform: uppercase;display:none;">
                    <div class="sec">
                        <asp:DropDownList ID="ddlDepartment" runat="server" Style="width: 100%; height: 32px;
                            display: inline;" CssClass="form-control" 
                            meta:resourcekey="ddlDepartmentResource1">
                        </asp:DropDownList>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="ddlDepartment"
                                            ErrorMessage="Please select Department." InitialValue="0" CssClass="commonerrormsg"
                                            Display="Dynamic" ValidationGroup="chk" 
                            meta:resourcekey="RequiredFieldValidator14Resource1"></asp:RequiredFieldValidator>--%>
                    </div>
                </div>
                </div>
                <div class="col-md-6" style="width:100%";>
                <div class="widget-body custom-form" style="margin-bottom: 10px; ">
                    <div class="sec">
                      <%--  <%= CommonMessages.CATEGORY%>--%>
                        <asp:Literal ID="Literal2" runat="server" meta:resourcekey="category" enableviewstate="false"/>:<br />
                                    <asp:CheckBoxList Width="100%" ID="chkList" runat="server" CssClass="FormText" RepeatDirection="Vertical"
                                        RepeatColumns="1" BorderWidth="0" Datafield="description" DataValueField="value">
                                    </asp:CheckBoxList>
                                   <%-- <asp:CustomValidator ID="CustomValidator1" ErrorMessage="Please select at least one item."
                                        ForeColor="Red" ClientValidationFunction="ValidateCheckBoxList" runat="server" ValidationGroup="chk"/>--%>
                                    <br />
                    </div>
                </div>
                </div>
                <div class="col-md-6" style="width:100%";>
                <div class="inline-form">
                    <label class="c-label">
                       <%-- <%= CommonMessages.Description%>--%>
                         <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Description" enableviewstate="false"/>:</label>
                    <asp:TextBox runat="server" placeholder="Description" ID="txtDESCRIPTION" MaxLength="500"
                        TextMode="MultiLine" Rows="5" meta:resourcekey="txtDESCRIPTIONResource1" /><br />
                  <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDESCRIPTION"
                        ErrorMessage="Please Enter DESCRIPTION." CssClass="commonerrormsg" Display="Dynamic"
                        ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator1Resource1"></asp:RequiredFieldValidator>--%>
                </div>
                </div>
            
            <%--<div class="col-xs-12 profile_bottom">
                <asp:Button runat="server" ID="btnsubmit" Text="Save" CssClass="btn blue pull-right"
                    ValidationGroup="chk" OnClick="btnsubmit_click" />
                <asp:Button runat="server" ID="btnCancel" Text="Cancel" CssClass="btn black pull-right"
                    ValidationGroup="chk" CausesValidation="false" OnClick="btnCancel_click" />
            </div>--%>
            <div class="modal-footer" style="border: 0px;">
                <asp:Button runat="server" ID="btnsubmit" class="btn btn-primary yellow" 
                    Text="Save" OnClick="btnsubmit_click" OnClientClick="generate('warning','Please wait','bottomCenter');"  ValidationGroup="chk" 
                    meta:resourcekey="btnsubmitResource1"></asp:Button>
                 <asp:Button runat="server" ID="btnCancel" class="btn btn-primary black" 
                    Text="Cancel" OnClick="btnCancel_click" meta:resourcekey="btnCancelResource1"></asp:Button>
               <%-- <button data-dismiss="modal" class="btn btn-default black" type="button">Cancel  </button>--%>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function ValidateCheckBoxList(sender, args) {
            var checkBoxList = document.getElementById("<%=chkList.ClientID %>");
            var checkboxes = checkBoxList.getElementsByTagName("input");
            var isValid = false;
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].checked) {
                    isValid = true;
                    break;
                }
            }
            args.IsValid = isValid;
        }
    </script>
</asp:Content>

