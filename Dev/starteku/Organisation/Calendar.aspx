﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true" CodeFile="Calendar.aspx.cs" Inherits="Organisation_Calendar" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .fontcolor {
            color: blueviolet;
        }

        .blockFs {
            pointer-events: none;
            color: red;
            cursor: none;
        }
    </style>
    <link type="text/css" rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" />
    <link href="css/fullcalendar_002.css" rel="stylesheet" />
    <%-- 
   
   
   
   
    <link href="css/fullcalendar_002.css" rel="stylesheet" />
    <!-- Full calendar -->
    <!-- Script -->
    <script src="js/jquery-1.10.2.js"></script>
    <!-- Jquery -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <!-- Bootstrap -->
    <script type="text/javascript" src="js/script.js"></script>
    <!-- Script -->
    <script src="js/enscroll-0.5.2.min.js"></script>
    <!-- Custom Scroll bar -->
    <script src="js/moment.js" type="text/javascript"></script>
    <!-- Date Range Picker -->
    <script src="js/daterangepicker.js"></script>--%>
    <!-- Date Range Picker -->
     <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/1/daterangepicker-bs3.css" />
    <%--<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />--%>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="font-awesome-4.0.3/css/font-awesome.css" type="text/css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/nv.css" type="text/css" />
    <!-- VISITOR CHART -->
    <link rel="stylesheet" type="text/css" media="all" href="css/daterangepicker-bs3.css" />
    <!-- Date Range Picker -->
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <!-- Style -->
    <link rel="stylesheet" href="css/responsive.css" type="text/css" />
    <link href="css/fullcalendar_002.css" rel="stylesheet" />
    <!-- Full Calendar -->
    <link href="css/fullcalendar.css" rel="stylesheet" />
    <!-- Full calendar -->
    <link href="css/fullcalendar.print.css" rel="stylesheet" media="print" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.css" type="text/css"/>
    <link href="../Scripts/fullcalendor/fullcalendar.css" rel="stylesheet" />
    <%-- <link href="../Scripts/fullcalendor/fullcalendar.print.css" rel="stylesheet" />--%>
    <script src="../Scripts/fullcalendor/moment.min.js"></script>
    <script src="../Scripts/fullcalendor/jquery.min.js"></script>
    <script src="../Scripts/fullcalendor/fullcalendar.min.js"></script>

    <link href="../Scripts/fullcalendor/pickADate/classic.css" rel="stylesheet" />
    <link href="../Scripts/fullcalendor/pickADate/classic.date.css" rel="stylesheet" />
    <script src="../Scripts/fullcalendor/gcal.js"></script>


    <%--<script src="js/daterangepicker.js"></script>--%>
    <script src="../Scripts/fullcalendor/picker.js"></script>
    <script src="../Scripts/fullcalendor/picker.date.js"></script>
    <script src="../Scripts/fullcalendor/legacy.js"></script>
    

    
    <%--<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/3.3.2/css/bootstrap.css" />--%>

    <!-- Include Date Range Picker -->
    <script src="../Scripts/fullcalendor/DATERANGEPICKER_mAIN.js"></script>
   <%-- <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/1/daterangepicker.js"></script>--%>

    <link href="../Scripts/fullcalendor/dATEPICKER/jquery.timepicker.css" rel="stylesheet" />
    <script src="../Scripts/fullcalendor/dATEPICKER/jquery.timepicker.js"></script>

    <link href="../Scripts/fullcalendor/dATEPICKER/bootstrap-datetimepicker.css" rel="stylesheet" />
    <link href="../Scripts/fullcalendor/dATEPICKER/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <script src="../Scripts/fullcalendor/dATEPICKER/bootstrap-datetimepicker.min.js"></script>
    <style type="text/css">.datetimepicker{padding:4px;margin-top:1px;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px;direction:ltr}.datetimepicker-inline{width:220px}.datetimepicker.datetimepicker-rtl{direction:rtl}.datetimepicker.datetimepicker-rtl table tr td span{float:right}.datetimepicker-dropdown,.datetimepicker-dropdown-left{top:0;left:0}[class*=" datetimepicker-dropdown"]:before{content:'';display:inline-block;border-left:7px solid transparent;border-right:7px solid transparent;border-bottom:7px solid #ccc;border-bottom-color:rgba(0,0,0,0.2);position:absolute}[class*=" datetimepicker-dropdown"]:after{content:'';display:inline-block;border-left:6px solid transparent;border-right:6px solid transparent;border-bottom:6px solid #fff;position:absolute}[class*=" datetimepicker-dropdown-top"]:before{content:'';display:inline-block;border-left:7px solid transparent;border-right:7px solid transparent;border-top:7px solid #ccc;border-top-color:rgba(0,0,0,0.2);border-bottom:0}[class*=" datetimepicker-dropdown-top"]:after{content:'';display:inline-block;border-left:6px solid transparent;border-right:6px solid transparent;border-top:6px solid #fff;border-bottom:0}.datetimepicker-dropdown-bottom-left:before{top:-7px;right:6px}.datetimepicker-dropdown-bottom-left:after{top:-6px;right:7px}.datetimepicker-dropdown-bottom-right:before{top:-7px;left:6px}.datetimepicker-dropdown-bottom-right:after{top:-6px;left:7px}.datetimepicker-dropdown-top-left:before{bottom:-7px;right:6px}.datetimepicker-dropdown-top-left:after{bottom:-6px;right:7px}.datetimepicker-dropdown-top-right:before{bottom:-7px;left:6px}.datetimepicker-dropdown-top-right:after{bottom:-6px;left:7px}.datetimepicker>div{display:none}.datetimepicker.minutes div.datetimepicker-minutes{display:block}.datetimepicker.hours div.datetimepicker-hours{display:block}.datetimepicker.days div.datetimepicker-days{display:block}.datetimepicker.months div.datetimepicker-months{display:block}.datetimepicker.years div.datetimepicker-years{display:block}.datetimepicker table{margin:0}.datetimepicker td,.datetimepicker th{text-align:center;width:20px;height:20px;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px;border:0}.table-striped .datetimepicker table tr td,.table-striped .datetimepicker table tr th{background-color:transparent}.datetimepicker table tr td.minute:hover{background:#eee;cursor:pointer}.datetimepicker table tr td.hour:hover{background:#eee;cursor:pointer}.datetimepicker table tr td.day:hover{background:#eee;cursor:pointer}.datetimepicker table tr td.old,.datetimepicker table tr td.new{color:#999}.datetimepicker table tr td.disabled,.datetimepicker table tr td.disabled:hover{background:0;color:#999;cursor:default}.datetimepicker table tr td.today,.datetimepicker table tr td.today:hover,.datetimepicker table tr td.today.disabled,.datetimepicker table tr td.today.disabled:hover{background-color:#fde19a;background-image:-moz-linear-gradient(top,#fdd49a,#fdf59a);background-image:-ms-linear-gradient(top,#fdd49a,#fdf59a);background-image:-webkit-gradient(linear,0 0,0 100%,from(#fdd49a),to(#fdf59a));background-image:-webkit-linear-gradient(top,#fdd49a,#fdf59a);background-image:-o-linear-gradient(top,#fdd49a,#fdf59a);background-image:linear-gradient(top,#fdd49a,#fdf59a);background-repeat:repeat-x;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#fdd49a',endColorstr='#fdf59a',GradientType=0);border-color:#fdf59a #fdf59a #fbed50;border-color:rgba(0,0,0,0.1) rgba(0,0,0,0.1) rgba(0,0,0,0.25);filter:progid:DXImageTransform.Microsoft.gradient(enabled=false)}.datetimepicker table tr td.today:hover,.datetimepicker table tr td.today:hover:hover,.datetimepicker table tr td.today.disabled:hover,.datetimepicker table tr td.today.disabled:hover:hover,.datetimepicker table tr td.today:active,.datetimepicker table tr td.today:hover:active,.datetimepicker table tr td.today.disabled:active,.datetimepicker table tr td.today.disabled:hover:active,.datetimepicker table tr td.today.active,.datetimepicker table tr td.today:hover.active,.datetimepicker table tr td.today.disabled.active,.datetimepicker table tr td.today.disabled:hover.active,.datetimepicker table tr td.today.disabled,.datetimepicker table tr td.today:hover.disabled,.datetimepicker table tr td.today.disabled.disabled,.datetimepicker table tr td.today.disabled:hover.disabled,.datetimepicker table tr td.today[disabled],.datetimepicker table tr td.today:hover[disabled],.datetimepicker table tr td.today.disabled[disabled],.datetimepicker table tr td.today.disabled:hover[disabled]{background-color:#fdf59a}.datetimepicker table tr td.today:active,.datetimepicker table tr td.today:hover:active,.datetimepicker table tr td.today.disabled:active,.datetimepicker table tr td.today.disabled:hover:active,.datetimepicker table tr td.today.active,.datetimepicker table tr td.today:hover.active,.datetimepicker table tr td.today.disabled.active,.datetimepicker table tr td.today.disabled:hover.active{background-color:#fbf069}.datetimepicker table tr td.active,.datetimepicker table tr td.active:hover,.datetimepicker table tr td.active.disabled,.datetimepicker table tr td.active.disabled:hover{background-color:#006dcc;background-image:-moz-linear-gradient(top,#08c,#04c);background-image:-ms-linear-gradient(top,#08c,#04c);background-image:-webkit-gradient(linear,0 0,0 100%,from(#08c),to(#04c));background-image:-webkit-linear-gradient(top,#08c,#04c);background-image:-o-linear-gradient(top,#08c,#04c);background-image:linear-gradient(top,#08c,#04c);background-repeat:repeat-x;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#0088cc',endColorstr='#0044cc',GradientType=0);border-color:#04c #04c #002a80;border-color:rgba(0,0,0,0.1) rgba(0,0,0,0.1) rgba(0,0,0,0.25);filter:progid:DXImageTransform.Microsoft.gradient(enabled=false);color:#fff;text-shadow:0 -1px 0 rgba(0,0,0,0.25)}.datetimepicker table tr td.active:hover,.datetimepicker table tr td.active:hover:hover,.datetimepicker table tr td.active.disabled:hover,.datetimepicker table tr td.active.disabled:hover:hover,.datetimepicker table tr td.active:active,.datetimepicker table tr td.active:hover:active,.datetimepicker table tr td.active.disabled:active,.datetimepicker table tr td.active.disabled:hover:active,.datetimepicker table tr td.active.active,.datetimepicker table tr td.active:hover.active,.datetimepicker table tr td.active.disabled.active,.datetimepicker table tr td.active.disabled:hover.active,.datetimepicker table tr td.active.disabled,.datetimepicker table tr td.active:hover.disabled,.datetimepicker table tr td.active.disabled.disabled,.datetimepicker table tr td.active.disabled:hover.disabled,.datetimepicker table tr td.active[disabled],.datetimepicker table tr td.active:hover[disabled],.datetimepicker table tr td.active.disabled[disabled],.datetimepicker table tr td.active.disabled:hover[disabled]{background-color:#04c}.datetimepicker table tr td.active:active,.datetimepicker table tr td.active:hover:active,.datetimepicker table tr td.active.disabled:active,.datetimepicker table tr td.active.disabled:hover:active,.datetimepicker table tr td.active.active,.datetimepicker table tr td.active:hover.active,.datetimepicker table tr td.active.disabled.active,.datetimepicker table tr td.active.disabled:hover.active{background-color:#039}.datetimepicker table tr td span{display:block;width:23%;height:54px;line-height:54px;float:left;margin:1%;cursor:pointer;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px}.datetimepicker .datetimepicker-hours span{height:26px;line-height:26px}.datetimepicker .datetimepicker-hours table tr td span.hour_am,.datetimepicker .datetimepicker-hours table tr td span.hour_pm{width:14.6%}.datetimepicker .datetimepicker-hours fieldset legend,.datetimepicker .datetimepicker-minutes fieldset legend{margin-bottom:inherit;line-height:30px}.datetimepicker .datetimepicker-minutes span{height:26px;line-height:26px}.datetimepicker table tr td span:hover{background:#eee}.datetimepicker table tr td span.disabled,.datetimepicker table tr td span.disabled:hover{background:0;color:#999;cursor:default}.datetimepicker table tr td span.active,.datetimepicker table tr td span.active:hover,.datetimepicker table tr td span.active.disabled,.datetimepicker table tr td span.active.disabled:hover{background-color:#006dcc;background-image:-moz-linear-gradient(top,#08c,#04c);background-image:-ms-linear-gradient(top,#08c,#04c);background-image:-webkit-gradient(linear,0 0,0 100%,from(#08c),to(#04c));background-image:-webkit-linear-gradient(top,#08c,#04c);background-image:-o-linear-gradient(top,#08c,#04c);background-image:linear-gradient(top,#08c,#04c);background-repeat:repeat-x;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#0088cc',endColorstr='#0044cc',GradientType=0);border-color:#04c #04c #002a80;border-color:rgba(0,0,0,0.1) rgba(0,0,0,0.1) rgba(0,0,0,0.25);filter:progid:DXImageTransform.Microsoft.gradient(enabled=false);color:#fff;text-shadow:0 -1px 0 rgba(0,0,0,0.25)}.datetimepicker table tr td span.active:hover,.datetimepicker table tr td span.active:hover:hover,.datetimepicker table tr td span.active.disabled:hover,.datetimepicker table tr td span.active.disabled:hover:hover,.datetimepicker table tr td span.active:active,.datetimepicker table tr td span.active:hover:active,.datetimepicker table tr td span.active.disabled:active,.datetimepicker table tr td span.active.disabled:hover:active,.datetimepicker table tr td span.active.active,.datetimepicker table tr td span.active:hover.active,.datetimepicker table tr td span.active.disabled.active,.datetimepicker table tr td span.active.disabled:hover.active,.datetimepicker table tr td span.active.disabled,.datetimepicker table tr td span.active:hover.disabled,.datetimepicker table tr td span.active.disabled.disabled,.datetimepicker table tr td span.active.disabled:hover.disabled,.datetimepicker table tr td span.active[disabled],.datetimepicker table tr td span.active:hover[disabled],.datetimepicker table tr td span.active.disabled[disabled],.datetimepicker table tr td span.active.disabled:hover[disabled]{background-color:#04c}.datetimepicker table tr td span.active:active,.datetimepicker table tr td span.active:hover:active,.datetimepicker table tr td span.active.disabled:active,.datetimepicker table tr td span.active.disabled:hover:active,.datetimepicker table tr td span.active.active,.datetimepicker table tr td span.active:hover.active,.datetimepicker table tr td span.active.disabled.active,.datetimepicker table tr td span.active.disabled:hover.active{background-color:#039}.datetimepicker table tr td span.old{color:#999}.datetimepicker th.switch{width:145px}.datetimepicker th span.glyphicon{pointer-events:none}.datetimepicker thead tr:first-child th,.datetimepicker tfoot tr:first-child th{cursor:pointer}.datetimepicker thead tr:first-child th:hover,.datetimepicker tfoot tr:first-child th:hover{background:#eee}.input-append.date .add-on i,.input-prepend.date .add-on i,.input-group.date .input-group-addon span{cursor:pointer;width:14px;height:14px}

    </style>
    <script type="text/javascript">
        var ddd;
        var calendarData = "";
        $(document).ready(function () {
           // $('.timepicker1').timepicker();
            $('.timepicker1').datetimepicker({
                format: 'HH:mm'


            });
            $('.timepicker2').datetimepicker({
                format: 'HH:mm'


            });
            
            $(".drpAppointment").daterangepicker({
                format: 'DD/MM/YYYY',
                //timePickerIncrement: 30,
                //timePicker12Hour: true,
                //timePickerSeconds: false,
                //timePicker: true,
                // showDropdowns: true,
                // showWeekNumbers: true,
            });
            appointmentDatepicker();
            $(".datepicker").pickadate({
                // from: -30, to: true ,
                selectMonths: true,
                selectYears: true,
                formatSubmit: 'dd/MM/yyyy',
                onSet: function (dt) {
                    console.log('Just set stuff:', dt);
                    // alert($(".datepicker").val());



                    //var myDate = new Date("February 3 2001");


                    // alert(dtt);

                    setTimeout(function () {
                        GoToDateFC();

                    }, 1000);


                }
            });


            GetCalendarData();
            setTimeout(function () {
                DrawCalendarData();
            }, 1000);
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();


        });
        var datetimeFC;
        function GoToDateFC() {
            //alert(datetimeFC);
            var myDate = new Date($(".datepicker").val());
            var month = myDate.getMonth() + 1;
            var dtt = (myDate.getFullYear() + "/" + month + "/" + myDate.getDate());
            datetimeFC = dtt;
            //  $('#calendar1').fullCalendar('gotoDate', '2014/12/05');
            setTimeout(function () {
               // alert(datetimeFC);
                $('#calendar1').fullCalendar('gotoDate', datetimeFC);
                /* alert(datetimeFC);*/
            }, 5000);

            // alert(dtt);

        }

        function appointmentDatepicker() {
            $(".addEventDatepicker").pickadate({
                // from: -30, to: true ,
                selectMonths: true,
                selectYears: true,
                formatSubmit: 'dd/MM/yyyy',
                onSet: function (dt) {
                    $(".addEventDatepicker").val(dt.date);


                }
            });

        }

        function DrawCalendarData() {
            var calendar = $('#calendar1').fullCalendar({

                googleCalendarApiKey: 'AIzaSyDcnW6WejpTOCffshGDDb4neIrXVUA1EAE',
                overlap: true,

                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                selectable: true,
                selectHelper: true,
                select: function (start, end, allDay) {
                    var title = prompt('Event Title:');
                  // alert("s"+ start);
                    
                   // var eventTime = $('#calendar1').fullCalendar.formatDate(start, "dddd, MMMM yyyy @ h:sstt") + " to " +
              //  $('#calendar1').fullCalendar.formatDate(end, "dddd, MMMM yyyy @ h:sstt");
                    
                    //  alert("eventTime "+ eventTime);
                    
                 
                    

                   InsertData(title, new Date(start).toString(), new Date(end).toString(), allDay);

                    //webservice that save records
                    if (title) {
                        calendar.fullCalendar('renderEvent',
                            {
                                title: title,
                                start: start,
                                end: end,
                                allDay: allDay
                            },
                            true // make the event "stick"
                        );
                    }
                    calendar.fullCalendar('unselect');
                },
                editable: true,

                /*   events: 'usa__en@holiday.calendar.google.com',*/
                eventSources: [
            {
                googleCalendarId: 'usa__en@holiday.calendar.google.com',
                editable: false,
                color: '#6B6B6B',
                className: "blockFs",
                startEditable: false,
                allDay: true,
                selectable: false

            },
            {
                events: calendarData
            }
                ],


                eventRender: function (event, element) {
                    element.attr('href', 'javascript:void(0);');
                    element.click(function () {
                        if (event.message != null) {
                            $("#message").html((event.message));
                        } else {
                            $("#Message").hide();
                        }
                        if (event.start != null) {
                            $("#startTime").html((event.start).format('MMM Do h:mm A'));
                            //alert(event.start);
                        } else {
                            $("#Start").hide();
                        }
                        if (event.end != null) {
                            $("#endTime").html((event.end).format('MMM Do h:mm A'));
                        } else {
                            $("#End").hide();
                        }
                        if (event.title != null) {
                            $("#subject").html((event.title));
                        } else {
                            $("#Subject").hide();
                        }


                        // $("#endTime").html(moment(event.end).format('MMM Do h:mm A'));
                        // $("#eventInfo").html(event.description);
                        // $("#eventLink").attr('href', event.url);
                        //$("#eventContent").modalPopLite({ openButton: '#clicker', closeButton: '#close-btn' });
                        $("#eventContent").dialog({
                            show: {
                                effect: "blind",
                                duration: 1000
                            },
                            hide: {
                                effect: "fade",
                                duration: 1000
                            },

                            open: function () {
                                jQuery('.ui-widget-overlay').bind('click', function () {
                                    jQuery('.modelDilougBoxCalandar').dialog('close');
                                });
                            }

                        });
                        $('.modelDilougBoxCalandar').show();

                    });
                }
            });

        }

        function DrawCalendarData1() {
            $('#calendar').fullCalendar({

                // THIS KEY WON'T WORK IN PRODUCTION!!!
                // To make your own Google API key, follow the directions here:
                // https://fullcalendar.io/docs/google_calendar/
                googleCalendarApiKey: 'AIzaSyDcnW6WejpTOCffshGDDb4neIrXVUA1EAE',

                // US Holidays
                events: 'usa__en@holiday.calendar.google.com',

                eventClick: function (event) {
                    // opens events in a popup window
                    window.open(event.url, 'gcalevent', 'width=700,height=600');
                    return false;
                },

                loading: function (bool) {
                    $('#loading').toggle(bool);
                }

            });

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="eventContent" class="modelDilougBoxCalandar" title="Event Details" style="display: none;">
        <label class="fontcolor Subject">Subject:</label>
        <span id="subject"></span>
        <br />
        <label class="fontcolor Start">Start: </label>
        <span id="startTime"></span>
        <br />
        <label class="fontcolor End">End: </label>
        <span id="endTime"></span>
        <br />
        <br />
        <label class="fontcolor Message">Message: </label>
        <span id="message"></span>
        <br />
        <br />
        <br />

        <input type="button" value="Close" class="btn btn-primary yellow" onclick="$('#eventContent').dialog('close');" style="float: right" />
        <p id="eventInfo"></p>
        <p><strong></strong></p>
    </div>
    <div class="">
        <div class="container">
            <div class="col-md-6">
                <div class="heading-sec">
                    <h1>
                        <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Calendar" EnableViewState="false" />
                        <i><span runat="server" id="Calendar"></span></i>

                    </h1>
                </div>
            </div>
            <br />
            <div class="col-md-12" style="background: #f4f4f4; margin-top: 20px; border-radius: 5px; float: left;">
                <div class="col-md-12">
                    <div class="invite_btn">
                        <input class="datepicker btn btn-primary yellow" type="text" style="border-radius: 5px;" value="Go To Date" />
                        <a href="#add-post-title" data-toggle="modal" title="">
                            <button style="border: 0px;" class="btn btn-primary yellow" type="button">
                                
                                <asp:Literal ID="Literal2" runat="server" meta:resourcekey="AddAppointment" EnableViewState="false" />
                            </button></a>
                    </div>
                    <div aria-hidden="true" role="dialog" tabindex="-1" class="modal fade" id="add-post-title"
                        style="display: none;">
                        <div class="modal-dialog" style="margin-top: 60px;">
                            <div class="modal-content">
                                <div class="modal-header blue">
                                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                                        ×
                                    </button>
                                    <h4 class="modal-title">
                                        <asp:Literal ID="Literal3" runat="server" meta:resourcekey="SendAppointment" EnableViewState="false" /></h4>
                                </div>
                                <div class="modal-body">
                                    <%-- <input type="text" value="" placeholder="new note" id="Text1" runat="server" />--%>
                                    <asp:UpdatePanel runat="server" ID="UpdatePanel12" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:TextBox runat="server" ID="txtto" AccessKey="u" onkeyup="GetCourt(this.id);"
                                                OnTextChanged="txt_name_TextChanged" AutoPostBack="True"
                                                placeholder="To..." meta:resourcekey="txttoResource1"></asp:TextBox>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <%-- <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>--%>
                                    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/jquery-ui.min.js"></script>
                                    <script type="text/javascript">
                                        function GetCourt(id) {
                                            //var $accui = jQuery.noConflict(true);

                                            $("#" + id).autocomplete({
                                                source: function (request, response) {
                                                    $.ajax({
                                                        type: "POST",
                                                        contentType: "application/json; charset=utf-8",
                                                        url: "Calendar.aspx/GetAutoCompleteCourt",
                                                        data: "{'court':'" + document.getElementById('<%=txtto.ClientID %>').value + "'}",
                                                        dataType: "json",
                                                        success: function (data) {
                                                            response(data.d);
                                                        },
                                                        error: function (result) {
                                                            alert("Error");
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    </script>
                                    <input type="text" placeholder="Subject" id="txtsubject" runat="server" />
                                    <div style="float: left;">
                                        <div id="div" class="pull-right" style="padding: 0px; background: none !important; color: #000;">
                                          <%--  <i class="fa fa-calendar icon-calendar icon-large" style="color: #999999">Date </i>--%>
                                            <input id="Label1"  class="drpAppointment  btn btn-default " style="width: 338px;" type="text" value="Select Date Range"/>
                                         
                                           <%-- <asp:Label ID="Label1"  runat="server" Text="Label"
                                                meta:resourcekey="Label1Resource1"></asp:Label>--%>
                                            <%-- <span>August 5, 2014 - September 3, 2014</span>--%> 
                                        </div>
                                    </div>
                                    
                                     <div style="float: left;">
                                         <input type="text" class="timepicker1  btn btn-default" style="margin-left: 10px;width: 100px;" placeholder="Start Time" id="Text2"  /> 
                                     <input type="text" class="timepicker2  btn btn-default" style="margin-left: 10px;width: 100px;" placeholder="End Time" id="Text3" />
                                    </div>
                                    

                                    
                                    <textarea placeholder="Message" rows="3" id="txtmessge" runat="server"></textarea>
                                </div>
                                <div class="modal-footer">
                                    <%-- <asp:Button ID="btnSend" runat="server" Text="Send" class="btn btn-primary yellow" OnClick="btnSend_click" OnClientClick="myFunction()"/>--%>
                                    <input type="button" onclick="myFunction()" id="btnSend" value="Save" class="btn btn-primary yellow">
                                    <%--<button class="btn btn-primary yellow" type="button" OnClick="btnSend_click">
                                        Send</button>--%>
                                    <%--  <asp:LinkButton ID="LinkButton1" runat="server" CssClass="addNote animate" OnClick="btnnote_click">+
                                                </asp:LinkButton>--%>
                                    <button data-dismiss="modal" class="btn btn-default black" type="button" id="btnclose">
                                        <asp:Literal ID="Literal4" runat="server" meta:resourcekey="Close" EnableViewState="false" />
                                    </button>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                    </div>
                    
                    <div id='calendar1' style="padding-bottom: 15px;background-color:#f4f4f4">
                    </div>
                </div>
                <div class="col-md-4" style="padding: 40px 10px 10px 10px;">
                    <div class="calendar">
                        <div class="col leftCol">
                            <div class="content" style="display: none">
                                <div class="blue" style="float: left; height: 50px; width: 100%; padding: 5px; margin-bottom: 10px;" >
                                    <h1 class="date currentDate" ></h1>
                                    <script type="text/javascript">
                                        $(document).ready(function () {
                                            $(".currentDate").text($.datepicker.formatDate('d  MM yy', new Date()));
                                        });
                                    </script>

                                </div>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel11" UpdateMode="Conditional" style="display: none">
                                    <ContentTemplate>
                                        <div class="notes">
                                            <p>
                                                <input type="text" placeholder="New Note" id="txtnote" runat="server" />
                                                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtnote"
                                                    ErrorMessage="Please enter note." CssClass="commonerrormsg" Display="Dynamic"
                                                    ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator5Resource1"></asp:RequiredFieldValidator>
                                                <asp:LinkButton ID="lnkBtnName" runat="server" CssClass="addNote animate"
                                                    OnClick="btnnote_click" ValidationGroup="chk"
                                                    meta:resourcekey="lnkBtnNameResource1">+
                                                </asp:LinkButton>
                                            </p>
                                            <asp:Repeater ID="RepeaterNote" runat="server">
                                                <HeaderTemplate>
                                                    <ul class="noteList">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <li><%# Eval("noteName")%>
                                                        <asp:LinkButton ID="lnkBtnName" runat="server" CssClass="addNote animate"
                                                            CommandArgument='<%# Eval("noteId") %>' OnClick="Button_Click"
                                                            meta:resourcekey="lnkBtnNameResource2">x
                                                        </asp:LinkButton>

                                                    </li>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </ul>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- TIME LINE -->
            <!-- Twitter Widget -->
            <!-- Weather Widget -->
        </div>
        <!-- Container -->
    </div>
    <!-- Wrapper -->
    <!-- RAIn ANIMATED ICON-->
    <script type="text/javascript">
        var icons = new Skycons();
        icons.set("rain", Skycons.RAIN);
        icons.play();
    </script>
    <script type="text/javascript">
        function InsertData(title, start, end, allDay) {


           // $.fullCalendar.moment().format();
            

            if (title != '' || title != 'undefined' || title != 'null') {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Calendar.aspx/InsertDataCalendar",
                    data: "{'title':'" + title + "','start':'" + start + "','end':'" + end + "','allDay':'" + allDay + "'}",
                    dataType: "json",
                    success: function (data) {
                        var obj = data.d;
                        if (obj == 'true') {
                            $('#<%=txtto.ClientID %>').val('');
                            $('#<%=txtsubject.ClientID %>').val('');
                            $('#<%=txtmessge.ClientID %>').val('');
                            alert('Save Successfully');
                            document.getElementById('btnclose').click();
                        }
                    },
                    error: function (result) {
                        alert("Error");
                    }
                });
            }

        }

        function GetCalendarData() {
            $.ajax({
                type: "POST",
                contentType: "application/json;charset=utf-8",
                url: "Calendar.aspx/GetCalendarData",
                data: "{}",
                dataType: "json",
                success: function (msg) {
                    $("#divResult").html(msg.d);
                    var data = msg.d;
                    calendarData = data;
                    //alert(data.d);
                },
                error: function (data) {
                    alert("Error");
                }
            });
        }
        function myFunction() {

            var txtto = $('#<%=txtto.ClientID %>').val();
            var txtsubject = $('#<%=txtsubject.ClientID %>').val();
            var txtmessge = $('#<%=txtmessge.ClientID %>').val();
            var fromTime = $(".timepicker1");
            var toTime = $(".timepicker2");
            var add = $("#Label1");

            if (txtto != '' && txtsubject != '' && txtmessge != '' && add != '') {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Calendar.aspx/InsertData",
                    data: "{'txtto':'" + txtto + "','txtsubject':'" + txtsubject + "','txtmessge':'" + txtmessge + "','add':'" + add.val() + "','fromTime':'" + fromTime.val() + "','toTime':'" + toTime.val() + "'}",
                    dataType: "json",
                    success: function (data) {
                        var obj = data.d;
                        if (obj == 'true') {
                            $('#<%=txtto.ClientID %>').val('');
                            $('#<%=txtsubject.ClientID %>').val('');
                            $('#<%=txtmessge.ClientID %>').val('');
                            alert('Save Successfully');
                            document.getElementById('btnclose').click();
                        }
                    },
                    error: function (result) {
                        alert("Error");
                    }
                });
            }
            else {
                // modelPopupDilouge("Error", "Please enter all the fields", false);
                alert('Please enter all the fields');
                return false;
            }
        }

    </script>
  
</asp:Content>
