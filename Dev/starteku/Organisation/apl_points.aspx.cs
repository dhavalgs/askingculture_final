﻿using starteku_BusinessLogic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Globalization;
using startetku.Business.Logic;

public partial class Organisation_apl_points : System.Web.UI.Page
{
    #region Page Event
    DataTable dt = new DataTable();
    int queid = 0;
    DataSet ds;
    public static int ans = 0;

    protected void Page_Load(object sender, EventArgs e)
    {

        Page.ClientScript.RegisterStartupScript(Page.GetType(), "my12", " SetExpandCollapse();", true);

        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {

            //Points.InnerHtml = CommonMessages.Welcome + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            Points.InnerHtml = GetLocalResourceObject("Welcome.Text").ToString() + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            GetUserpoints();
            setDefult();
            // GetAllEmployeeList();
        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }

    #endregion


    #region Method

    private void GetUserpoints()
    {
        try
        {
            int userid = 0;

            if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["AllPoint"])))
            {
                userid = Convert.ToInt32(Request.QueryString["AllPoint"]);
            }
            else
            {
                if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
                {
                    if ((Convert.ToString(Session["OrguserType"]) == "3") || (Convert.ToString(Session["OrguserType"]) == "2"))
                    {
                        userid = Convert.ToInt32(Session["OrgUserId"]);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["All"])))
                        {
                            userid = Convert.ToInt32(Request.QueryString["All"]);
                        }

                    }
                }
            }




            PointBM obj = new PointBM();
            //obj.GetUserPoint(Convert.ToInt32(Session["OrgUserId"]), true);
            obj.GetUserPoint(userid, true);
            DataSet ds = obj.ds;



            if (ds.Tables[0].Rows.Count > 0)
            {
                //GetLocalResourceObject("Point.Text").ToString();

                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["name"])))
                {
                    allUserName.Text = Convert.ToString(ds.Tables[0].Rows[0]["name"]);
                    pdpUser.Text = Convert.ToString(ds.Tables[0].Rows[0]["name"]);
                    plpUser.Text = Convert.ToString(ds.Tables[0].Rows[0]["name"]);
                    alpUser.Text = Convert.ToString(ds.Tables[0].Rows[0]["name"]);

                }

                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["pointPLP"])))
                {
                    lblPLP.Text = Convert.ToString(ds.Tables[0].Rows[0]["pointPLP"]);
                    //PPpl.sss = CommonMessages.Point + ":    " + Convert.ToString(ds.Tables[0].Rows[0]["pointPLP"]);
                    PPpl.InnerHtml = GetLocalResourceObject("Point.Text").ToString() + ":" + Convert.ToString(ds.Tables[0].Rows[0]["pointPLP"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["pointALP"])))
                {
                    lblALP.Text = Convert.ToString(ds.Tables[0].Rows[0]["pointALP"]);
                    AAlp.InnerHtml = GetLocalResourceObject("Point.Text").ToString() + ":" + Convert.ToString(ds.Tables[0].Rows[0]["pointALP"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["pointTotal"])))
                {
                    lblTotal.Text = Convert.ToString(ds.Tables[0].Rows[0]["pointTotal"]);
                    PAll.InnerHtml = GetLocalResourceObject("Point.Text").ToString() + ":" + Convert.ToString(ds.Tables[0].Rows[0]["pointTotal"]);

                }
            }

        }
        catch (Exception ex)
        {

        }
    }
    protected void setDefult()
    {
        GetALP();
        GetPLP();
        GetALL();
        GetPDP();
        if (!String.IsNullOrEmpty(Request.QueryString["PLP"]))
        {
            PLP.Style.Add("display", "block");
        }
        else if (!String.IsNullOrEmpty(Request.QueryString["ALP"]))
        {
            ALP.Style.Add("display", "block");
        }
        else if (!String.IsNullOrEmpty(Request.QueryString["All"]))
        {
            All.Style.Add("display", "block");
        }
        else if (!String.IsNullOrEmpty(Request.QueryString["AllPoint"]))
        {
            All.Style.Add("display", "block");
        }
        else if (!String.IsNullOrEmpty(Request.QueryString["PDP"]))
        {
            PDP.Style.Add("display", "block");
        }

    }
    protected void GetALP()
    {
        Boolean plp = false;
        Boolean alp = false;
        Boolean id = false;
        plp = false;
        alp = true;
        id = false;
        int userid = 0;
        //if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        //{
        //    if ((Convert.ToString(Session["OrguserType"]) == "3") || (Convert.ToString(Session["OrguserType"]) == "2"))
        //    {
        //        userid = Convert.ToInt32(Session["OrgUserId"]);
        //    }
        //    else
        //    {
        //        userid = Convert.ToInt32(Request.QueryString["All"]);
        //    }
        //}

        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["AllPoint"])))
        {
            userid = Convert.ToInt32(Request.QueryString["AllPoint"]);
        }
        else
        {
            if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
            {
                if ((Convert.ToString(Session["OrguserType"]) == "3") || (Convert.ToString(Session["OrguserType"]) == "2"))
                {
                    userid = Convert.ToInt32(Session["OrgUserId"]);
                }
                else
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["All"])))
                    {
                        userid = Convert.ToInt32(Request.QueryString["All"]);
                    }

                }
            }
        }

        //if (!String.IsNullOrEmpty(Request.QueryString["PLP"]))
        //{
        //    plp = true;
        //    alp = false;
        //    id = false;

        //}
        //else if (!String.IsNullOrEmpty(Request.QueryString["ALP"]))
        //{
        //    plp = false;
        //    alp = true;
        //    id = false;

        //}
        //else if (!String.IsNullOrEmpty(Request.QueryString["All"]))
        //{
        //    plp = false;
        //    alp = false;
        //    id = true;
        //}
        PointBM obj = new PointBM();

        obj.GetUserPointByName(userid, plp, alp, true, id);
        // obj.GetUserPointByNameDetail(Convert.ToInt32(Session["OrgUserId"]), plp, alp, true, id);
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {

                for (var i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToString(ds.Tables[0].Rows[i]["logPointInfo"]).ToString() == "SignIn")
                    {
                        ds.Tables[0].Rows[i]["logPointInfo"] = hdnsignin.Value;
                    }
                    if (Convert.ToString(ds.Tables[0].Rows[i]["logPointInfo"]).ToString() == "UploadPublicDoc")
                    {
                        ds.Tables[0].Rows[i]["logPointInfo"] = hdnuploaddoc.Value;
                    }
                    if (Convert.ToString(ds.Tables[0].Rows[i]["logPointInfo"]).ToString() == "ReadDoc")
                    {
                        ds.Tables[0].Rows[i]["logPointInfo"] = hdnreaddoc.Value;
                    }
                    if (Convert.ToString(ds.Tables[0].Rows[i]["logPointInfo"]).ToString() == "OpenCompetencesScreen")
                    {
                        ds.Tables[0].Rows[i]["logPointInfo"] = hdnopencomscreen.Value;
                    }
                    if (Convert.ToString(ds.Tables[0].Rows[i]["logPointInfo"]).ToString() == "FinalFillCompetences")
                    {
                        ds.Tables[0].Rows[i]["logPointInfo"] = hdnfinalfillcom.Value;
                    }
                    if (Convert.ToString(ds.Tables[0].Rows[i]["logPointInfo"]).ToString() == "RequestSession")
                    {
                        ds.Tables[0].Rows[i]["logPointInfo"] = hdnreqsession.Value;
                    }
                    if (Convert.ToString(ds.Tables[0].Rows[i]["logPointInfo"]).ToString() == "SuggestSession")
                    {
                        ds.Tables[0].Rows[i]["logPointInfo"] = hdnsuggsession.Value;
                    }
                    if (Convert.ToString(ds.Tables[0].Rows[i]["logPointInfo"]).ToString() == "AcceptProvidedHelp")
                    {
                        ds.Tables[0].Rows[i]["logPointInfo"] = hdnacchelp.Value;
                    }
                    if (Convert.ToString(ds.Tables[0].Rows[i]["logPointInfo"]).ToString() == "ReadDocByOther")
                    {
                        ds.Tables[0].Rows[i]["logPointInfo"] = hdnreaddocbyother.Value;
                    }

                }

                gvGrid.DataSource = ds.Tables[0];
                gvGrid.DataBind();
                gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvGrid.DataSource = null;
                gvGrid.DataBind();
            }
        }
        else
        {
            gvGrid.DataSource = null;
            gvGrid.DataBind();
        }
    }
    protected void GetPLP()
    {
        Boolean plp = false;
        Boolean alp = false;
        Boolean id = false;
        plp = true;
        alp = false;
        id = false;
        int userid = 0;
        //if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])) )
        //{
        //    if ((Convert.ToString(Session["OrguserType"]) == "3") || (Convert.ToString(Session["OrguserType"]) == "2"))
        //    {
        //        userid = Convert.ToInt32(Session["OrgUserId"]);
        //    }
        //    else
        //    {
        //        userid = Convert.ToInt32(Request.QueryString["All"]);
        //    }
        //}

        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["AllPoint"])))
        {
            userid = Convert.ToInt32(Request.QueryString["AllPoint"]);
        }
        else
        {
            if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
            {
                if ((Convert.ToString(Session["OrguserType"]) == "3") || (Convert.ToString(Session["OrguserType"]) == "2"))
                {
                    userid = Convert.ToInt32(Session["OrgUserId"]);
                }
                else
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["All"])))
                    {
                        userid = Convert.ToInt32(Request.QueryString["All"]);
                    }

                }
            }
        }

        //if (!String.IsNullOrEmpty(Request.QueryString["PLP"]))
        //{
        //    plp = true;
        //    alp = false;
        //    id = false;

        //}
        //else if (!String.IsNullOrEmpty(Request.QueryString["ALP"]))
        //{
        //    plp = false;
        //    alp = true;
        //    id = false;

        //}
        //else if (!String.IsNullOrEmpty(Request.QueryString["All"]))
        //{
        //    plp = false;
        //    alp = false;
        //    id = true;
        //}
        PointBM obj = new PointBM();
        obj.GetUserPointByName(userid, plp, alp, true, id);
        // obj.GetUserPointByNameDetail(Convert.ToInt32(Session["OrgUserId"]), plp, alp, true, id);
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {

                for (var i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToString(ds.Tables[0].Rows[i]["logPointInfo"]).ToString() == "SignIn")
                    {
                        ds.Tables[0].Rows[i]["logPointInfo"] = hdnsignin.Value;
                    }
                    if (Convert.ToString(ds.Tables[0].Rows[i]["logPointInfo"]).ToString() == "UploadPublicDoc")
                    {
                        ds.Tables[0].Rows[i]["logPointInfo"] = hdnuploaddoc.Value;
                    }
                    if (Convert.ToString(ds.Tables[0].Rows[i]["logPointInfo"]).ToString() == "ReadDoc")
                    {
                        ds.Tables[0].Rows[i]["logPointInfo"] = hdnreaddoc.Value;
                    }
                    if (Convert.ToString(ds.Tables[0].Rows[i]["logPointInfo"]).ToString() == "OpenCompetencesScreen")
                    {
                        ds.Tables[0].Rows[i]["logPointInfo"] = hdnopencomscreen.Value;
                    }
                    if (Convert.ToString(ds.Tables[0].Rows[i]["logPointInfo"]).ToString() == "FinalFillCompetences")
                    {
                        ds.Tables[0].Rows[i]["logPointInfo"] = hdnfinalfillcom.Value;
                    }
                    if (Convert.ToString(ds.Tables[0].Rows[i]["logPointInfo"]).ToString() == "RequestSession")
                    {
                        ds.Tables[0].Rows[i]["logPointInfo"] = hdnreqsession.Value;
                    }
                    if (Convert.ToString(ds.Tables[0].Rows[i]["logPointInfo"]).ToString() == "SuggestSession")
                    {
                        ds.Tables[0].Rows[i]["logPointInfo"] = hdnsuggsession.Value;
                    }
                    if (Convert.ToString(ds.Tables[0].Rows[i]["logPointInfo"]).ToString() == "AcceptProvidedHelp")
                    {
                        ds.Tables[0].Rows[i]["logPointInfo"] = hdnacchelp.Value;
                    }
                    if (Convert.ToString(ds.Tables[0].Rows[i]["logPointInfo"]).ToString() == "ReadDocByOther")
                    {
                        ds.Tables[0].Rows[i]["logPointInfo"] = hdnreaddocbyother.Value;
                    }
                }


                gvplp.DataSource = ds.Tables[0];
                gvplp.DataBind();
                gvplp.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvplp.DataSource = null;
                gvplp.DataBind();
            }
        }
        else
        {
            gvplp.DataSource = null;
            gvplp.DataBind();
        }
    }
    protected void GetALL()
    {
        Boolean plp = false;
        Boolean alp = false;
        Boolean id = false;
        plp = false;
        alp = false;
        id = true;
        int userid = 0;
        //if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        //{
        //    if ((Convert.ToString(Session["OrguserType"]) == "3") || (Convert.ToString(Session["OrguserType"]) == "2"))
        //    {
        //        userid = Convert.ToInt32(Session["OrgUserId"]);
        //    }
        //    else
        //    {
        //        userid = Convert.ToInt32(Request.QueryString["All"]);
        //    }
        //}

        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["AllPoint"])))
        {
            userid = Convert.ToInt32(Request.QueryString["AllPoint"]);
        }
        else
        {
            if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
            {
                if ((Convert.ToString(Session["OrguserType"]) == "3") || (Convert.ToString(Session["OrguserType"]) == "2"))
                {
                    userid = Convert.ToInt32(Session["OrgUserId"]);
                }
                else
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["All"])))
                    {
                        userid = Convert.ToInt32(Request.QueryString["All"]);
                    }

                }
            }
        }

        //if (!String.IsNullOrEmpty(Request.QueryString["PLP"]))
        //{
        //    plp = true;
        //    alp = false;
        //    id = false;

        //}
        //else if (!String.IsNullOrEmpty(Request.QueryString["ALP"]))
        //{
        //    plp = false;
        //    alp = true;
        //    id = false;

        //}
        //else if (!String.IsNullOrEmpty(Request.QueryString["All"]))
        //{
        //    plp = false;
        //    alp = false;
        //    id = true;
        //}
        PointBM obj = new PointBM();
        obj.GetUserPointByName(userid, plp, alp, true, id);
        // obj.GetUserPointByNameDetail(Convert.ToInt32(Session["OrgUserId"]), plp, alp, true, id);
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {

                for (var i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToString(ds.Tables[0].Rows[i]["logPointInfo"]).ToString() == "SignIn")
                    {
                        ds.Tables[0].Rows[i]["logPointInfo"] = hdnsignin.Value;
                    }
                    if (Convert.ToString(ds.Tables[0].Rows[i]["logPointInfo"]).ToString() == "UploadPublicDoc")
                    {
                        ds.Tables[0].Rows[i]["logPointInfo"] = hdnuploaddoc.Value;
                    }
                    if (Convert.ToString(ds.Tables[0].Rows[i]["logPointInfo"]).ToString() == "ReadDoc")
                    {
                        ds.Tables[0].Rows[i]["logPointInfo"] = hdnreaddoc.Value;
                    }
                    if (Convert.ToString(ds.Tables[0].Rows[i]["logPointInfo"]).ToString() == "OpenCompetencesScreen")
                    {
                        ds.Tables[0].Rows[i]["logPointInfo"] = hdnopencomscreen.Value;
                    }
                    if (Convert.ToString(ds.Tables[0].Rows[i]["logPointInfo"]).ToString() == "FinalFillCompetences")
                    {
                        ds.Tables[0].Rows[i]["logPointInfo"] = hdnfinalfillcom.Value;
                    }
                    if (Convert.ToString(ds.Tables[0].Rows[i]["logPointInfo"]).ToString() == "RequestSession")
                    {
                        ds.Tables[0].Rows[i]["logPointInfo"] = hdnreqsession.Value;
                    }
                    if (Convert.ToString(ds.Tables[0].Rows[i]["logPointInfo"]).ToString() == "SuggestSession")
                    {
                        ds.Tables[0].Rows[i]["logPointInfo"] = hdnsuggsession.Value;
                    }
                    if (Convert.ToString(ds.Tables[0].Rows[i]["logPointInfo"]).ToString() == "AcceptProvidedHelp")
                    {
                        ds.Tables[0].Rows[i]["logPointInfo"] = hdnacchelp.Value;
                    }
                    if (Convert.ToString(ds.Tables[0].Rows[i]["logPointInfo"]).ToString() == "ReadDocByOther")
                    {
                        ds.Tables[0].Rows[i]["logPointInfo"] = hdnreaddocbyother.Value;
                    }
                }

                gvall.DataSource = ds.Tables[0];
                gvall.DataBind();
                gvall.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvall.DataSource = null;
                gvall.DataBind();
            }
        }
        else
        {
            gvall.DataSource = null;
            gvall.DataBind();
        }
    }
    protected void GetPDP()
    {
        int userid = 0;
        //if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        //{
        //    if ((Convert.ToString(Session["OrguserType"]) == "3")||(Convert.ToString(Session["OrguserType"]) == "2"))
        //    {
        //        userid = Convert.ToInt32(Session["OrgUserId"]);
        //    }
        //    else
        //    {
        //        userid = Convert.ToInt32(Request.QueryString["All"]);
        //    }
        //}

        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["AllPoint"])))
        {
            userid = Convert.ToInt32(Request.QueryString["AllPoint"]);
        }
        else
        {
            if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
            {
                if ((Convert.ToString(Session["OrguserType"]) == "3") || (Convert.ToString(Session["OrguserType"]) == "2"))
                {
                    userid = Convert.ToInt32(Session["OrgUserId"]);
                }
                else
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["All"])))
                    {
                        userid = Convert.ToInt32(Request.QueryString["All"]);
                    }

                }
            }
        }

        SubCompetenceBM obj = new SubCompetenceBM();
        obj.subIsActive = true;
        obj.subIsDeleted = false;
        obj.GetPDPpoint(Convert.ToInt32(userid));
        DataSet ds = obj.ds;
        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                grpdp.DataSource = ds.Tables[0];
                grpdp.DataBind();
                grpdp.HeaderRow.TableSection = TableRowSection.TableHeader;


                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["PDPTotal"])))
                {
                    lblpdp.Text = Convert.ToString(ds.Tables[0].Rows[0]["PDPTotal"]);
                    spdp.InnerHtml = GetLocalResourceObject("Point.Text").ToString() + ":" + Convert.ToString(ds.Tables[0].Rows[0]["PDPTotal"]);
                }
            }
            else
            {
                grpdp.DataSource = null;
                grpdp.DataBind();
            }
        }
        else
        {
            grpdp.DataSource = null;
            grpdp.DataBind();
        }
    }
    protected void GetAllEmployeeList()
    {
        Boolean plp = false;
        Boolean alp = false;
        Boolean id = false;
        if (!String.IsNullOrEmpty(Request.QueryString["PLP"]))
        {
            plp = true;
            alp = false;
            id = false;
        }
        else if (!String.IsNullOrEmpty(Request.QueryString["ALP"]))
        {
            plp = false;
            alp = true;
            id = false;
        }
        else if (!String.IsNullOrEmpty(Request.QueryString["All"]))
        {
            plp = false;
            alp = false;
            id = true;
        }
        PointBM obj = new PointBM();
        obj.GetUserPointByName(Convert.ToInt32(Session["OrgUserId"]), plp, alp, true, id);
        // obj.GetUserPointByNameDetail(Convert.ToInt32(Session["OrgUserId"]), plp, alp, true, id);
        DataSet ds = obj.ds;
        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {

                gvGrid.DataSource = ds.Tables[0];
                gvGrid.DataBind();
                gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvGrid.DataSource = null;
                gvGrid.DataBind();
            }
        }
        else
        {
            gvGrid.DataSource = null;
            gvGrid.DataBind();
        }
    }
    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Danish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();
            //if (!string.IsNullOrEmpty(language))
            //{
            //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
            //    {
            //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
            //        {
            //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
            //        }
            //    }
            //    //if (language.EndsWith("Danish")) languageId = "da-DK";
            //    //else languageId = "en-GB";
            //    
            //}
            SetCulture(languageId);
        }

        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion
    protected void btnDivision_Click(object sender, CommandEventArgs e)
    {
        //string str = Convert.ToString(Request.UrlReferrer);
        string quryStringAllpoint = Convert.ToString(Request.QueryString["AllPoint"]);
        string[] commnadArgument = Convert.ToString(e.CommandArgument).Split(',');
        string logPointInfo = commnadArgument[0];
        string logUserId = commnadArgument[1];
        if (string.IsNullOrEmpty(quryStringAllpoint))
        {
            Response.Redirect("DetailPoints.aspx?" + e.CommandName + "=" + logPointInfo + "&Uid=" + logUserId + "");
        }
        else
        {
            Response.Redirect("DetailPoints.aspx?" + e.CommandName + "=" + logPointInfo + "&Uid=" + logUserId + "&AllPoint=" + quryStringAllpoint + "");
        }
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Request.QueryString["AllPoint"]))
        {
            ////ClientScript.RegisterStartupScript(Page.GetType(), "goBack", "javascript:history.back();", true);
            //ClientScript.RegisterStartupScript(Page.GetType(), "goBack", "javascript:window.history.go(-1);", true);
            Response.Redirect("Manager-dashboard.aspx");
        }
        else
        {
            Response.Redirect("ViewPoint.aspx");
        }
    }
    protected void gvall_DataBound(object sender, EventArgs e)
    {

    }


    protected void grpdp_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Label lblCompetenceName = (Label)e.Row.FindControl("lblCompetenceName");
        Label lblCompetenceNameDN = (Label)e.Row.FindControl("lblCompetenceNameDN");
        HiddenField hdnPDPDescription = (HiddenField)e.Row.FindControl("hdnPDPDescription");

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            hdnPDPDescription.Value = hdnPDPDescription.Value.Replace("got PDP point", hdnGotPDP.Value);
            hdnPDPDescription.Value = hdnPDPDescription.Value.Replace("for moving on level", hdnMovingLevel.Value);

            if (Convert.ToString(Session["Culture"]) == "Danish")
            {
                lblCompetenceName.Visible = false;
                lblCompetenceNameDN.Visible = true;
                lblCompetenceNameDN.Text = lblCompetenceNameDN.Text + " (" + hdnPDPDescription.Value + ")";
            }
            else
            {
                lblCompetenceName.Visible = true;
                lblCompetenceNameDN.Visible = false;
                lblCompetenceName.Text = lblCompetenceName.Text + " (" + hdnPDPDescription.Value + ")";
            }
        }

    }
}