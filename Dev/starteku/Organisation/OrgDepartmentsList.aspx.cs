﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;


public partial class Organisation_OrgDepartmentsList : System.Web.UI.Page
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            Department.InnerHtml = "Welcome   " + Convert.ToString(Session["OrgUserName"]) + "!";
            PopulateRootLevel();
            DepartmentSelectAll();
            DepartmentArchiveAll();
            SetDefaultMessage();

        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion

    #region method
    protected void SetDefaultMessage()
    {
        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["msg"])))
        {
            if (Convert.ToString(Request.QueryString["msg"]) == "del")
            {
                lblMsg.Text = CommonModule.msgRecordHasBeenDeletedSuccessfully;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "ins")
            {
                lblMsg.Text = CommonModule.msgRecordInsertedSuccessfully;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "upd")
            {
                lblMsg.Text = CommonModule.msgRecordUpdatedSuccss;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "arc")
            {
                lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
        }

    }
    protected void DepartmentSelectAll()
    {
        DepartmentsBM obj = new DepartmentsBM();
        obj.depIsActive = true;
        obj.depIsDeleted = false;
        obj.depCompanyId = Convert.ToInt32(Session["OrgUserId"]);
        obj.GetAllDepartments();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvGrid.DataSource = ds.Tables[0];
                gvGrid.DataBind();

                gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvGrid.DataSource = null;
                gvGrid.DataBind();
            }
        }
        else
        {
            gvGrid.DataSource = null;
            gvGrid.DataBind();
        }
    }
    protected void DepartmentArchiveAll()
    {
        DepartmentsBM obj = new DepartmentsBM();
        obj.depIsActive = false;
        obj.depIsDeleted = false;
        obj.depCompanyId = Convert.ToInt32(Session["OrgUserId"]);
        obj.GetAllDepartments();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvArchive.DataSource = ds.Tables[0];
                gvArchive.DataBind();

                gvArchive.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvArchive.DataSource = null;
                gvArchive.DataBind();
            }
        }
        else
        {
            gvArchive.DataSource = null;
            gvArchive.DataBind();
        }
    }
    #endregion

    #region grid Event
    protected void gvGrid_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "archive")
        {
            DepartmentsBM obj = new DepartmentsBM();
            obj.depId = Convert.ToInt32(e.CommandArgument);
            obj.depIsActive = false;
            obj.depIsDeleted = false;
            obj.DepartmentsStatusUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    DepartmentSelectAll();
                    DepartmentArchiveAll();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }
    }

    protected void gvArchive_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "archive")
        {
            DepartmentsBM obj = new DepartmentsBM();
            obj.depId = Convert.ToInt32(e.CommandArgument);
            obj.depIsActive = true;
            obj.depIsDeleted = false;
            obj.DepartmentsStatusUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    DepartmentSelectAll();
                    DepartmentArchiveAll();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }
        else if (e.CommandName == "permanentlydelete")
        {
            DepartmentsBM obj = new DepartmentsBM();
            obj.depId = Convert.ToInt32(e.CommandArgument);
            obj.depIsActive = false;
            obj.depIsDeleted = true;
            obj.DepartmentsStatusUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    DepartmentSelectAll();
                    DepartmentArchiveAll();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }
    }

    #endregion

    #region TreeView
    private void FillCategoryChecklist(TreeNodeCollection nodes, int id)
    {

        foreach (TreeNode child in nodes)
        {

            if (Convert.ToString(id) == Convert.ToString(child.Value))
            {
                child.Checked = true;

            }

            FillCategoryChecklist(child.ChildNodes, id);
        }


    }
    protected void TreeView_TreeNodePopulate(object sender, TreeNodeEventArgs e)
    {
        int ParentCatagoryID = Int32.Parse(e.Node.Value);
        PopulateSubLevel(ParentCatagoryID, e.Node);
        FillCategoryChecklist(TreeView1.Nodes, Convert.ToInt32(ViewState["catParentId"]));
    }
    private void PopulateSubLevel(int ParentCatagoryID, TreeNode parentNode)
    {
        //Your sublevel Datatable ie. dtSub

        DepartmentsBM obj = new DepartmentsBM();
        obj.depPerentId = ParentCatagoryID;
        obj.depCompanyId = Convert.ToInt32(Session["OrgUserId"]);
        obj.GetDepartmentsFromParentId();
        DataSet ds = obj.ds;
        PopulateNodes(ds.Tables[0], parentNode.ChildNodes);


    }
    private void PopulateNodes(DataTable dt, TreeNodeCollection nodes)
    {
        foreach (DataRow dr in dt.Rows)
        {
            TreeNode tn = new TreeNode();
            tn.Value = dr["depId"].ToString();
            tn.Text = dr["depName"].ToString();
            nodes.Add(tn);
            //If node has child nodes, then enable on-demand populating
            tn.PopulateOnDemand = ((int)(dr["catChildCount"]) > 0);
        }
    }
    private void PopulateRootLevel()
    {
        DataTable table = new DataTable();
        DataColumn column = new DataColumn();
        column.DataType = System.Type.GetType("System.Int32");
        column.ColumnName = "depId";
        table.Columns.Add(column);

        // Create second column.
        column = new DataColumn();
        column.DataType = Type.GetType("System.String");
        column.ColumnName = "depName";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.Int32");
        column.ColumnName = "catChildCount";
        table.Columns.Add(column);

        DataRow dr = table.NewRow();
        dr["depId"] = 0;
        dr["depName"] = "Root";
        dr["catChildCount"] = 10;
        table.Rows.Add(dr);
        table.AcceptChanges();


        DepartmentsBM obj = new DepartmentsBM();
        obj.depPerentId = 0;
        obj.depCompanyId = Convert.ToInt32(Session["OrgUserId"]);
        obj.GetDepartmentsFromParentId();
        DataSet ds = obj.ds;
        PopulateNodes(table, TreeView1.Nodes);

    }
    private Int32 getCategory(TreeNodeCollection treeNode, int ret)
    {
        foreach (TreeNode child in treeNode)
        {
            if (child.Checked == true)
            {
                ret = Convert.ToInt32(child.Value);
                return ret;
            }

            ret = getCategory(child.ChildNodes, ret);
        }
        return ret;
    }

    protected Int32 getvalue()
    {

        if (TreeView1.CheckedNodes.Count > 0)
        {
            foreach (TreeNode node in TreeView1.CheckedNodes)
            {
                if (node.Parent != null)
                {
                    string checkedValue = node.Text.ToString();
                    Int32 ret = Convert.ToInt32(node.Value);
                    return ret;
                }
            }
            foreach (TreeNode node in TreeView1.Nodes)
            {
                if (node.Checked == true)
                {
                    string checkedValue = node.Text.ToString();
                    Int32 ret = Convert.ToInt32(node.Value);
                    return ret;
                }
            }
            //stablish the session variable only when the foreach has finished
            //Session["listActivity"] = listActivity;
        }
        return 0;
    }


    #endregion

    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        DepartmentsBM obj2 = new DepartmentsBM();
        obj2.DepartmentsCheckDuplication(txtDepartmentName.Text, -1);
        String returnMsg = obj2.ReturnString;
        if (returnMsg == "")
        {
            DepartmentsBM obj = new DepartmentsBM();
            obj.depName = txtDepartmentName.Text;
            //if (Convert.ToInt32(ddlDepartment.SelectedValue) == 0)
            //{
            //    obj.depPerentId = 0;
            //}
            //else
            //{
            //    obj.depPerentId = Convert.ToInt32(ddlDepartment.SelectedValue);
            //}
            obj.depPerentId = getvalue();
            obj.depCompanyId = Convert.ToInt32(Session["OrgUserId"]);
            obj.depIsActive = true;
            obj.depIsDeleted = false;
            obj.depCreatedDate = DateTime.Now;
            obj.depDepId = 0;
            obj.InsertDepartments();
            if (obj.ReturnBoolean == true)
            {
                Response.Redirect("OrgDepartmentsList.aspx?msg=ins");
            }
        }
        else
        {
            if (returnMsg == "depName")
                lblMsg.Text = CommonModule.msgNameAlreadyExists;
            else
                lblMsg.Text = CommonModule.msgProblemInsertRecord;
        }
    }
    #endregion
}