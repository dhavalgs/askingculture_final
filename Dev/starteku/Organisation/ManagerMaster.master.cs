﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using startetku.Business.Logic;
using System.Threading;
using System.Globalization;
using starteku_BusinessLogic.Model;
public partial class Organisation_ManagerMaster : System.Web.UI.MasterPage
{
    string country = "";
    string state = "";
    startetkuEntities1 db = new startetkuEntities1();
    protected void Page_Load(object sender, EventArgs e)
    {
        LogoMaster logo = db.LogoMasters.FirstOrDefault();

        var userId = Convert.ToInt32(Session["OrgUserId"]);
        hdnLoginUserIDType.Value = Convert.ToString(Session["OrguserType"]);
        Int32 userCompanyID = Convert.ToInt32(Session["OrgCompanyId"]);
        var EnaData = db.GetItemsEnableList(userId, userCompanyID).FirstOrDefault();

        Session["Aemcompid"] = EnaData.Aemcompid;
        Session["Cemcompid"] = EnaData.comdirCompID;
        Session["Compcompid"] = EnaData.compCompID;
        Session["Pointcompid"] = EnaData.pointCompID;
        Session["Quescompid"] = EnaData.quesemID;

        if (string.IsNullOrWhiteSpace(Convert.ToString(Session["Aemcompid"])))
        {
            activityPanel.Visible = false;
        }
        
        if (string.IsNullOrWhiteSpace(Convert.ToString(Session["Compcompid"])))
        {
            competencePanel.Visible = false;
          //  competenceTopPanel.Visible = false;
        }
        if (logo != null)
        {

            imgPageLogo.ImageUrl = "../Log/upload/Userimage/" + logo.PageLogo;
            ViewState["imgLoginLogo"] = logo.PageLogo;
        }
        if (!IsPostBack)
        {
            if (!String.IsNullOrEmpty(Convert.ToString(Session["OrgUserName"])))
            {
                Setlanguage();
                //rep_notificationAll();
                detdata();
            }
            GetUserpoints();
            Getpdprpoints();
        }
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrgUserName"])))
        {
            span2.InnerHtml = Convert.ToString(Session["OrgUserName"]);
            span1.InnerHtml = Convert.ToString(Session["OrgUserName"]);
            login.InnerHtml = Convert.ToString(GetLocalResourceObject("LastLoginResource.Text")) + " : " + Convert.ToString(Session["userDateTime"]);  
            login1.InnerHtml = Convert.ToString(GetLocalResourceObject("LastLoginResource.Text"))+" : " + Convert.ToString(Session["userDateTime"]);  
            span3.InnerHtml = state + "," + country;
            span4.InnerHtml = state + "," + country;

           
            
        }
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrgUserImage"])))
        {
            img.ImageUrl = "../Log/upload/Userimage/" + Convert.ToString(Session["OrgUserImage"]) + "";
            img1.ImageUrl = "../Log/upload/Userimage/" + Convert.ToString(Session["OrgUserImage"]) + "";
        }
    }
    private void GetUserpoints()
    {
        try
        {
            PointBM obj = new PointBM();
            obj.GetUserPoint(Convert.ToInt32(Session["OrgUserId"]), true);
            DataSet ds = obj.ds;
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["pointPLP"])))
                {
                    lblPLP.Text = Convert.ToString(ds.Tables[0].Rows[0]["pointPLP"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["pointALP"])))
                {
                    lblALP.Text = Convert.ToString(ds.Tables[0].Rows[0]["pointALP"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["pointTotal"])))
                {
                    lblTotal.Text = Convert.ToString(ds.Tables[0].Rows[0]["pointTotal"]);
                }

            }

        }
        catch (Exception ex)
        {

        }
    }

    private void Getpdprpoints()
    {
        try
        {
            SubCompetenceBM obj = new SubCompetenceBM();
            obj.subIsActive = true;
            obj.subIsDeleted = false;
            obj.GetPDPpoint(Convert.ToInt32(Session["OrgUserId"]));
            DataSet ds = obj.ds;
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["PDPTotal"])))
                    {
                        lblpdp.Text = Convert.ToString(ds.Tables[0].Rows[0]["PDPTotal"]);
                    }
                }

            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void detdata()
    {
        UserBM obj = new UserBM();
        obj.userId = Convert.ToInt32(Session["OrgUserId"]);
        obj.GetEmployeedetailsbyid();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["couName"])))
                    country = Convert.ToString(ds.Tables[0].Rows[0]["couName"]);

                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["staName"])))
                    state = Convert.ToString(ds.Tables[0].Rows[0]["staName"]);
            }
        }
    }
    //protected void rep_notificationAll()
    //{
    //    // int val = 0;
    //    try
    //    {

    //        NotificationBM obj = new NotificationBM();
    //        obj.notIsActive = true;
    //        obj.notIsDeleted = false;
    //        obj.userCreateBy = Convert.ToString(Session["OrgUserId"]);
    //        obj.GetAllNotification();
    //        DataSet ds = obj.ds;          
    //        ViewState["data"] = ds;

    //        if (ds != null)
    //        {
    //            if (ds.Tables[0].Rows.Count > 0)
    //            {
    //                //rep_notification.DataSource = ds;
    //                //rep_notification.DataBind();
    //            }
    //            else
    //            {
    //                //rep_notification.DataSource = null;
    //                //rep_notification.DataBind();
    //            }
    //            if (ds.Tables[1].Rows.Count > 0)
    //            {
    //                lblcountnotification.Text = Convert.ToString(ds.Tables[1].Rows[0]["count1"]);
    //                lblshownotification.Text = Convert.ToString(ds.Tables[1].Rows[0]["count1"]);
    //            }
    //            else
    //            {
    //                lblcountnotification.Text = "0";
    //            }
    //        }
    //        else
    //        {
    //            //rep_notification.DataSource = null;
    //            //rep_notification.DataBind();
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        Common.WriteLog("error in rep_notificationAll" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
    //        throw ex;
    //    }
    //}

    protected void Setlanguage()
    {
        try
        {
            string language = Convert.ToString(Session["Culture"]);
            //string language = "Denish";
            string languageId = "";
            //if (!string.IsNullOrEmpty(language))
            //{
            //    if (language.EndsWith("Denish")) languageId = "da-DK";
            //    else languageId = "en-GB";
            //    SetCulture(languageId);
            //}
            //ResourceLanguageBM obj = new ResourceLanguageBM();
            //DataSet resds = obj.GetAllResourceLanguage();
            //if (!string.IsNullOrEmpty(language))
            //{
            //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
            //    {
            //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
            //        {
            //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
            //        }
            //    }

            //    SetCulture(languageId);
            //}
            ResourceLanguageBM obj = new ResourceLanguageBM();
            if (!string.IsNullOrEmpty(language))
            {
                DataSet resds = obj.GetResourceLanguage(language);
                languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

                SetCulture(languageId);
            }

            if (Session["Language"] != null)
            {
                if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
            }
        }
        catch { }
    }
    protected void SetCulture(string languageId)
    {
        try
        {
            Session["Language"] = languageId;
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
        }
        catch { }
    }
    protected string GetToolTip(string tag)
    {
        var localResourceObject = "";
        try
        {
            localResourceObject = Convert.ToString(GetLocalResourceObject(tag));
        }
        catch (Exception)
        {
            localResourceObject = "";

        }
        return localResourceObject;
    }
}
