﻿<%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OrgProvideHelp.aspx.cs" Inherits="Organisation_OrgProvideHelp" %>--%>
<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true" CodeFile="OrgProvideHelp.aspx.cs" Inherits="Organisation_OrgProvideHelp"
    Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" meta:resourcekey="lblMsgResource1"></asp:Label>
    <asp:Label runat="server" ID="Label1" CssClass="commonerrormsg" meta:resourcekey="Label1Resource1"></asp:Label>
    <div class="col-md-6">
        <div class="heading-sec">
            <h1>
            <%--   <%= CommonMessages.Invitationrequest%>  --%>
              <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Invitationrequest" enableviewstate="false"/> <i><span runat="server" id="Eemployee"></span></i>
            </h1>
        </div>
    </div>
    <br />
    <br />
    <div class="col-md-6 range ">
    </div>
    <div class="col-md-12">
        <div class="chat-widget widget-body" style="background: #fff;">
            <asp:Label runat="server" ID="lblDataDisplayTitle" meta:resourcekey="lblDataDisplayTitleResource1"></asp:Label>
            <div class="chat-widget-head yellow">
                <h4 style="margin: -6px 0px 0px;">
                    <%--  <%= CommonMessages.PendingRequestInformatio%>--%>
                   <%-- <%= CommonMessages.Invitationrequest%> --%>
                    <asp:Literal ID="Literal2" runat="server" meta:resourcekey="Invitationrequest" enableviewstate="false"/> 
                </h4>
            </div>
            <%--<div class="indicatesRequireFiled">
                <i>*
                   $1$ <%= CommonMessages.Indicatesrequiredfield%>#1#
                    <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Indicatesrequiredfield" enableviewstate="false"/></i>
            </div>--%>
            <div id="div2" runat="server">
                <div class="col-md-6" style="width: 100%">
                    <div class="inline-form">
                        <label class="c-label" style="width: 16%;">
                           <%-- <%= CommonMessages.Competence%>--%>
                            <asp:Literal ID="Literal4" runat="server" meta:resourcekey="Competence" enableviewstate="false"/>:</label>
                        <asp:Label class="c-label" ID="lblCompetence" runat="server" Style="width: 80%;"
                            meta:resourcekey="lblCompetenceResource1"></asp:Label>
                    </div>
                </div>
                <div class="col-md-6" style="width: 100%; display: none;">
                    <div class="inline-form">
                        <label class="c-label" style="width: 16%;">
                     <%--  <%= CommonMessages.RequestDate%>--%>
                        <asp:Literal ID="Literal5" runat="server" meta:resourcekey="RequestDate" enableviewstate="false"/>:</label>
                        <asp:Label class="c-label" ID="lblRequestDate" runat="server" Style="width: 80%;"
                            meta:resourcekey="lblRequestDateResource1"> <%# Eval("invCreatedDate") %></asp:Label>
                    </div>
                </div>
                <div class="col-md-6" style="width: 100%;">
                    <div class="inline-form">
                        <label class="c-label" style="width: 16%;">
                          <%--  <%= CommonMessages.EmployeeName%>--%>
                            <asp:Literal ID="Literal6" runat="server" meta:resourcekey="EmployeeName" enableviewstate="false"/>:</label>
                        <asp:Label class="c-label" ID="lblName" runat="server" Style="width: 80%;" meta:resourcekey="lblNameResource1"></asp:Label>
                    </div>
                </div>
                <div class="col-md-6" style="width: 100%; display: none;">
                    <div class="inline-form">
                        <label class="c-label" style="width: 16%;">
                            Invitation:</label>
                        <asp:Label class="c-label" ID="lblInvitation" runat="server" Style="width: 80%; line-height: 22px;"
                            meta:resourcekey="lblInvitationResource1"></asp:Label>
                    </div>
                </div>
                <%--This   drop down will hide to user.. bu default on send help ,its value pass as accept--%>
                <div class="col-md-6" style="width: 100%; top: 0px; left: 0px; display: none">
                    <div class="inline-form">
                        <label class="c-label" style="width: 16%;">
                            Status:</label>
                        <asp:Label class="c-label" ID="Label2" runat="server" Style="width: 80%;" meta:resourcekey="Label2Resource1"></asp:Label>
                        <asp:DropDownList ID="ddlstastus" runat="server" runat="server" Style="width: 100px;
                            height: 32px; display: inline;" meta:resourcekey="ddlstastusResource1">
                            <asp:ListItem Value="1" meta:resourcekey="ListItemResource1">Accept</asp:ListItem>
                            <%--<asp:ListItem Value="2">Cancel</asp:ListItem>
                            <asp:ListItem Value="3">pendding</asp:ListItem>--%>
                        </asp:DropDownList>
                    </div>
                </div>
                <%--Provide  option to Provide Help from here |20150109|Saurin--------%>
                <div class="col-md-6" style="width: 100%; top: 0px; left: 0px;" runat="server" id="helpresponse">
                    <div class="inline-form">
                        <label class="c-label" style="width: 16%;">
                           <%-- <%= CommonMessages.HelpResponse%>--%> 
                         <asp:Literal ID="Literal7" runat="server" meta:resourcekey="HelpResponse" enableviewstate="false"/>:</label>
                        <asp:Label runat="server" ID="lblHelpResponse" Visible="False" meta:resourcekey="lblHelpResponseResource1"></asp:Label>
                        <asp:TextBox runat="server" placeholder="Help (Optional)" ID="txtInviteGiveHelp"
                            TextMode="MultiLine" meta:resourcekey="txtInviteGiveHelpResource1" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtInviteGiveHelp"
                            ErrorMessage="Please enter Name." CssClass="commonerrormsg" Display="Dynamic"
                            ValidationGroup="chhk" meta:resourcekey="RequiredFieldValidator3Resource1"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <%--Upload doc markup-----------------------------------------------------------%>
                <div class="col-md-6" style="width: 100%; top: 0px; left: 0px;" runat="server" id="uploaddoc">
                    <div class="inline-form">
                        <label class="c-label" style="width: 16%;">
                            <asp:Label ID="Label3" runat="server" meta:resourcekey="Label3Resource1"><%--<%= CommonMessages.ShareDocument%>--%> <asp:Literal ID="Literal8" runat="server" meta:resourcekey="ShareDocument" enableviewstate="false"/>: </asp:Label>
                        </label>
                        <asp:FileUpload ID="FileUpload1" runat="server" meta:resourcekey="FileUpload1Resource1" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="FileUpload1"
                            CssClass="legend" Display="Dynamic" ErrorMessage="Please upload only .pdf File."
                            ValidationExpression="^.*\.(PDF|pdf)$" ForeColor="Red" ValidationGroup="chk"
                            meta:resourcekey="reFile1Resource1"></asp:RegularExpressionValidator>
                        <%--END upload doc markup:   20150108-------------------------------------------------%>
                    </div>
                    <%--Btn SHow Hide Local doc--%>
                    <asp:UpdatePanel runat="server" ID="UpdatePanellist1" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="col-md-6" style="width: 100%; top: 0px; left: 0px;" id="Div1">
                                <div class="inline-form">
                                    <asp:Label runat="server" ID="lblLabelOfSelectedDOc" class="c-label" Style="width: 16%;"
                                        Visible="False" meta:resourcekey="lblLabelOfSelectedDOcResource1"><%--<%= CommonMessages.SelectedDocumentName%>--%> <asp:Literal ID="Literal9" runat="server" meta:resourcekey="SelectedDocumentName" enableviewstate="false"/>:</asp:Label>
                                    <asp:Label runat="server" ID="lblYourDocName" class="c-label" Style="width: 16%;
                                        color: skyblue" meta:resourcekey="lblYourDocNameResource1"></asp:Label>
                                </div>
                            </div>
                            <div class="col-md-6" style="width: 30%; top: 0px; left: -31px;" id="btnShowHideDocs">
                                <div class="inline-form">
                                    <div class="col-xs-12 profile_bottom" id="Div5" runat="server">
                                        <input id="Button2" value="Select From My Documents" runat="server" class="btn black pull-right"
                                            style="font-size: 16px;" onclick="ShowHideMyDoc()" meta:resourcekey="btnSelectMyDoc" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" style="width: 100%; top: -70px; left: 0px; display: none" id="myDocs">
                                <div class="col-md-3" style="float: right; margin-right: 40px;">
                                    <asp:UpdatePanel runat="server" ID="UpdatePanellist" UpdateMode="Conditional">
                                        <ContentTemplate>
                                          <%--  <%= CommonMessages.Search%>--%>
                                            <asp:Literal ID="Literal10" runat="server" meta:resourcekey="Search" enableviewstate="false"/> Doc :
                                            <asp:TextBox ID="txtsearch" class="library_search" runat="server" AutoPostBack="True"
                                                OnTextChanged="txtsearch_TextChanged" meta:resourcekey="txtsearchResource1"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <ul class="document">
                                    <asp:Repeater ID="rep_document" runat="server" OnItemCommand="rep_document_OnItemCommand">
                                        <ItemTemplate>
                                            <li>
                                                <div class="document1">
                                                    <img src="images/documents_icon.png">
                                                </div>
                                                <h1>
                                                    <%# Eval("docFileName_Friendly")%></h1>
                                                <span>
                                                    <%# Eval("MinAgo") %>
                                                </span><i>
                                                    <%# Eval("FullName") %>
                                                </i>
                                                <p style="height: 60px;">
                                                    <%# Eval("docDescription")%></p>
                                                <div class="inbox-hover black">
                                                    <a href="../Log/upload/Document/<%# Eval("docattachment") %>" title="" data-tooltip="Download"
                                                        data-placement="bottom" download><i class="fa fa-download" style="margin-top: 9px;">
                                                        </i></a><a href="#" title="" data-tooltip="Read" data-placement="bottom" onclick="openPDF('../Log/upload/Document/<%# Eval("docattachment") %>?UID=<%# Eval("docUserID") %>&TOID=0&docId=<%# Eval("docID") %>');">
                                                            <i class="fa fa-eye" style="margin-top: 9px;"></i></a>
                                                    <asp:LinkButton ID="lnkCatName" runat="server" Text='SELECT' CommandName="catNameClick"
                                                        CommandArgument='<%# Eval("docattachment") %>' Style="width: 71px" meta:resourcekey="lnkCatNameResource1"></asp:LinkButton>
                                                </div>
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="txtsearch" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <%--Show doc to user who wanted help--%>
                <asp:Panel runat="server" ID="pnlShowDoc" Visible="False" meta:resourcekey="pnlShowDocResource1">
                    <div class="inline-form">
                        <ul class="document">
                            <li id="doc1" runat="server">
                                <div id="Div3" class="document1" runat="server">
                                    <img src="images/documents_icon.png">
                                </div>
                                <h1>
                                    <asp:Label runat="server" ID="lblDocName" meta:resourcekey="lblDocNameResource1"></asp:Label></h1>
                                <div class="inbox-hover black">
                                    <a href="../Log/upload/Document/<%=DocName1%>" title="" data-tooltip="Download" data-placement="bottom"
                                        download><i class="fa fa-download" style="margin-top: 9px;"></i></a><a href="#" title=""
                                            data-tooltip="Read" data-placement="bottom" onclick="openPDF('../Log/upload/Document/<%=DocName1%>?UID=&TOID=0&docId=');">
                                            <i class="fa fa-eye" style="margin-top: 9px;"></i></a>
                                </div>
                            </li>
                            <li id="doc2" runat="server">
                                <div class="document2">
                                    <img src="images/documents_icon.png">
                                </div>
                                <h1>
                                    <asp:Label runat="server" ID="lblDocName2" meta:resourcekey="lblDocName2Resource1"></asp:Label></h1>
                                <div class="inbox-hover black">
                                    <a href="../Log/upload/Document/<%=DocName2%>" title="" data-tooltip="Download" data-placement="bottom"
                                        download><i class="fa fa-download" style="margin-top: 9px;"></i></a><a href="#" title=""
                                            data-tooltip="Read" data-placement="bottom" onclick="openPDF('../Log/upload/Document/<%=DocName2%>?UID=&TOID=0&docId=');">
                                            <i class="fa fa-eye" style="margin-top: 9px;"></i></a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </asp:Panel>
                <%--Buttons------------------------------------%>
                <div class="col-xs-12 profile_bottom" id="submit" runat="server">
                    <asp:Button runat="server" ID="btnCancel" Text="Cancel" CssClass="btn black pull-right"
                        ValidationGroup="chk" CausesValidation="False" OnClick="btnCancel_Click"
                        meta:resourcekey="btnCancelResource1" />
                    <asp:Button runat="server" ID="btnsubmit" Text="Save" CssClass="btn blue pull-right"
                        ValidationGroup="chk" OnClick="btnsubmit_click" meta:resourcekey="btnsubmitResource1" />
                </div>
                <div class="col-xs-12 profile_bottom" id="back" runat="server">
                    <asp:Button runat="server" ID="Button1" Text="Back" CssClass="btn black pull-right"
                        CausesValidation="False" PostBackUrl="OrgInvitation_request.aspx" meta:resourcekey="Button1Resource1" />
                </div>
            </div>
        </div>
    </div>
    <%--Repeater : Shows list of reply for particualr question...--%>
    <div class="col-md-12" id="invHelpReply" runat="server">
        <div class="chat-widget widget-body" style="background: #fff; margin-top: 36px">
            <asp:Label runat="server" ID="Label4" meta:resourcekey="Label4Resource1"></asp:Label>
            <div class="chat-widget-head yellow">
                <h4 style="margin: -6px 0px 0px;">
                    <%--  <%= CommonMessages.PendingRequestInformatio%>--%>
                   Privide Help
                </h4>
            </div>
            <asp:Repeater runat="server" ID="rptHelpReply" OnItemDataBound="rptHelpReply_OnItemDataBound">
                <ItemTemplate>
                    <hr />
                    <div class="col-md-6" style="width: 100%">
                        <div class="inline-form">
                            <label class="c-label" style="width: 16%;">
                              <%--  <%= CommonMessages.Competence%>--%>
                                <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Competence" enableviewstate="false"/></label>
                            &nbsp;<asp:Label class="c-label" ID="lbl" runat="server" Style="width: 80%;" Text='<%# Eval("comCompetence") %>'
                                meta:resourcekey="lblResource1" />
                        </div>
                    </div>
                    <div class="col-md-6" style="width: 100%">
                        <div class="inline-form">
                            <label class="c-label" style="width: 16%;">
                               <%-- <%= CommonMessages.ReplyDate %>--%>
                                 <asp:Literal ID="Literal11" runat="server" meta:resourcekey="ReplyDate" enableviewstate="false"/> :</label>
                            <asp:Label class="c-label" ID="Label7" runat="server" Style="width: 80%;" ><%# Eval("invCreatedDate") %></asp:Label>
                        </div>
                    </div> 
                    <div class="col-md-6" style="width: 100%;">
                        <div class="inline-form">
                            <label class="c-label" style="width: 16%;">
                               <%-- <%= CommonMessages.EmployeeName%>--%>
                                <asp:Literal ID="Literal12" runat="server" meta:resourcekey="EmployeeName" enableviewstate="false"/></label>
                            &nbsp;<asp:Label class="c-label" ID="Label8" runat="server" Style="width: 80%;" ><%# Eval("userFirstName") %></asp:Label>
                        </div>
                    </div>
                    <div class="col-md-6" style="width: 100%; top: 0px; left: 0px;">
                        <div class="inline-form">
                            <label class="c-label" style="width: 16%;">
                             <%-- <%= CommonMessages.HelpResponse%>--%>
                              <asp:Literal ID="Literal13" runat="server" meta:resourcekey="HelpResponse" enableviewstate="false"/> :</label>
                            <asp:Label class="c-label" runat="server" Style="width: 80%; line-height: 22px;"
                                ID="lblresponceRepeater" ><%# Eval("invsubject") %></asp:Label>
                        </div>
                    </div>
                    <asp:Panel runat="server" ID="rptPnlShowDoc" meta:resourcekey="rptPnlShowDocResource1">
                        <div class="inline-form">
                            <ul class="document">
                                <asp:Panel ID="pnlDoc1" runat="server" meta:resourcekey="pnlDoc1Resource1">
                                    <li>
                                        <div id="Div4" class="document1" runat="server">
                                            <img src="images/documents_icon.png">
                                        </div>
                                        <h1>
                                            <asp:Label runat="server" ID="rptLblDocName1" Text='<%# Eval("invDocName") %>' meta:resourcekey="rptLblDocName1Resource1" />
                                        </h1>
                                        <div class="inbox-hover black">
                                            <a href="../Log/upload/Document/<%# Eval("invDocName") %>" title="" data-tooltip="Download"
                                                data-placement="bottom" download><i class="fa fa-download" style="margin-top: 9px;">
                                                </i></a><a href="#" title="" data-tooltip="Read" data-placement="bottom" onclick="openPDF('../Log/upload/Document/<%# Eval("invDocName") %>?UID=&TOID=0&docId=');">
                                                    <i class="fa fa-eye" style="margin-top: 9px;"></i></a>
                                        </div>
                                    </li>
                                </asp:Panel>
                                <asp:Panel ID="pnlDoc2" runat="server" meta:resourcekey="pnlDoc2Resource1">
                                    <li>
                                        <div id="Div6" class="document1" runat="server">
                                            <img src="images/documents_icon.png">
                                        </div>
                                        <h1>
                                            <asp:Label runat="server" ID="rptLblDocName2" Text='<%# Eval("invDocSuggestPath") %>'
                                                meta:resourcekey="rptLblDocName2Resource1" />
                                        </h1>
                                        <div class="inbox-hover black">
                                            <a href="../Log/upload/Document/<%# Eval("invDocSuggestPath") %>" title="" data-tooltip="Download"
                                                data-placement="bottom" download><i class="fa fa-download" style="margin-top: 9px;">
                                                </i></a><a href="#" title="" data-tooltip="Read" data-placement="bottom" onclick="openPDF('../Log/upload/Document/<%# Eval("invDocSuggestPath") %>?UID=&TOID=0&docId=');">
                                                    <i class="fa fa-eye" style="margin-top: 9px;"></i></a>
                                        </div>
                                    </li>
                                </asp:Panel>
                            </ul>
                        </div>
                    </asp:Panel>
                    <div class="col-md-6" style="width: 100%;">
                        <div class="inline-form">
                            <label class="c-label" style="width: 100%; background: blanchedalmond; color: blanchedalmond">
                            </label>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
    <%--Repeater END :  : : : : ; --%>
    <script type="text/jscript">

        function ShowHideMyDoc(toggle) {
            //
            $("#myDocs").toggle('linear');

            //if (toggle) {
            //    $("#myDocs").toggle();
            //}
            //else {
            //    $("#myDocs").fadeOut();
            //}
        }
        function openPDF(url) {
            var w = window.open(url, '_blank');
            w.focus();
        }

        $(document).ready(function () {
            
            $(".myCompetence").addClass("active_page");
        });

    </script>
</asp:Content>

