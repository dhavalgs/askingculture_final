﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master" AutoEventWireup="true" CodeFile="helprequest.aspx.cs" Inherits="Organisation_helprequest" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" 
        meta:resourcekey="lblMsgResource1"></asp:Label>
    <asp:Label runat="server" ID="Label1" CssClass="commonerrormsg" 
        meta:resourcekey="Label1Resource1"></asp:Label>
    <div class="col-md-6">
        <div class="heading-sec">
            <h1>
             <asp:Literal ID="Literal5" runat="server" meta:resourcekey="PendingHeplRequest" EnableViewState="false" /><i><span runat="server" id="Eemployee"></span></i>
            </h1>
        </div>
    </div>
    <br />
    <br />
    <div class="col-md-6 range ">
    </div>
    <div class="col-md-12">
        <div class="chat-widget widget-body" style="background: #fff;">
            <asp:Label runat="server" ID="lblDataDisplayTitle" 
                meta:resourcekey="lblDataDisplayTitleResource1"></asp:Label>
            <div class="chat-widget-head yellow">
                <h4 style="margin: -6px 0px 0px;">
                   <asp:Literal ID="Literal1" runat="server" meta:resourcekey="PendingHeplRequest" EnableViewState="false" /> Information</h4>
            </div>
            <div class="indicatesRequireFiled">
                <i>* <asp:Literal ID="Literal2" runat="server" meta:resourcekey="Indicatesrequiredfield" EnableViewState="false" /> </i>
            </div>
            <div id="div2" runat="server">
                <div class="col-md-6" style="width: 100%">
                    <div class="inline-form">
                        <label class="c-label" style="width: 16%;">
                          <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Competence" EnableViewState="false" />:</label>
                        <asp:Label class="c-label" ID="lblCompetence" runat="server" 
                            Style="width: 80%;" meta:resourcekey="lblCompetenceResource1"></asp:Label>
                    </div>
                </div>
                <div class="col-md-6" style="width: 100%;">
                    <div class="inline-form">
                        <label class="c-label" style="width: 16%;">
                            Sub  <asp:Literal ID="Literal4" runat="server" meta:resourcekey="Competence" EnableViewState="false" />:</label>
                        <asp:Label class="c-label" ID="lblsubCompetence" runat="server" 
                            Style="width: 80%;" meta:resourcekey="lblsubCompetenceResource1"></asp:Label>
                    </div>
                </div>
                <div class="col-md-6" style="width: 100%;">
                    <div class="inline-form">
                        <label class="c-label" style="width: 16%;">
                        <asp:Literal ID="Literal6" runat="server" meta:resourcekey="Message" EnableViewState="false" />
                           :</label>
                        <asp:Label class="c-label" ID="lblhelpMessage" runat="server" 
                            Style="width: 80%;" meta:resourcekey="lblhelpMessageResource1"></asp:Label>
                    </div>
                </div>
                <div class="col-md-6" style="width: 100%;">
                    <div class="inline-form">
                        <label class="c-label" style="width: 16%;">
                      <asp:Literal ID="Literal7" runat="server" meta:resourcekey="EmployeeName" EnableViewState="false" />  :</label>
                        <asp:Label class="c-label" ID="lblName" runat="server" Style="width: 80%;" 
                            meta:resourcekey="lblNameResource1"></asp:Label>
                    </div>
                </div>
                 <div class="col-md-6" style="width: 100%;">
                    <div class="inline-form">
                        <label class="c-label" style="width: 16%;">
                            Heplper Name:</label>
                        <asp:Label class="c-label" ID="lblhName" runat="server" Style="width: 80%;" 
                            meta:resourcekey="lblhNameResource1"></asp:Label>
                    </div>
                </div>
                <%--<div class="col-md-6" style="width: 100%;">
                    <div class="inline-form">
                        <label class="c-label" style="width: 16%;">
                            DOC:</label>
                        <ul class="document" style="margin: -63px 0 0 158px;">
                            <asp:Repeater ID="rep_document" runat="server">
                                <ItemTemplate>
                                    <li>
                                        <div class="document1">
                                            <img src="images/documents_icon.png">
                                        </div>
                                        <h1>
                                            <%# Eval("subCompetence")%></h1>
                                        <p style="height: 60px;">
                                            <%# Eval("subDescription")%></p>
                                        <div class="inbox-hover black">
                                            <a href="../Log/upload/Document/<%# Eval("skillDoc") %>" title="" data-tooltip="Download"
                                                data-placement="bottom" download><i class="fa fa-download" style="margin-top: 9px;">
                                                </i></a><a href="#" title="" data-tooltip="Read" data-placement="bottom" onclick="openPDF('../Log/upload/Document/<%# Eval("skillDoc") %>');">
                                                    <i class="fa fa-eye" style="margin-top: 9px;"></i></a>
                                        </div>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </div>
                </div>--%>
                <div class="col-md-6" style="width: 100%; top: 0px; left: 0px;">
                    <div class="inline-form">
                        <label class="c-label" style="width: 16%;">
                            Status:</label>
                        <asp:Label class="c-label" ID="Label2" runat="server" Style="width: 80%;" 
                            meta:resourcekey="Label2Resource1"></asp:Label>
                        <asp:DropDownList ID="ddlstastus" runat="server" runat="server" Style="width: 100px;
                            height: 32px; display: inline;" meta:resourcekey="ddlstastusResource1">
                            <asp:ListItem Value="1" meta:resourcekey="ListItemResource1">Accept</asp:ListItem>
                            <asp:ListItem Value="2" meta:resourcekey="ListItemResource2">Cancel</asp:ListItem>
                            <asp:ListItem Value="3" meta:resourcekey="ListItemResource3">Pending</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-md-6" style="width: 100%;">
                    <div class="inline-form">
                        <label class="c-label" style="width: 16%;">
                            Points:</label>
                        <asp:TextBox ID="txtpoint" runat="server" 
                            Style="width: 20%;margin-left:3px;float: left;" Text="5" 
                            meta:resourcekey="txtpointResource1"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers"
                        TargetControlID="txtpoint" Enabled="True">
                    </cc1:FilteredTextBoxExtender>
                         <asp:Label class="c-label" ID="lblpoint" runat="server" Style="width: 80%;" 
                            Visible="False" meta:resourcekey="lblpointResource1"></asp:Label>
                       <%--   <input type="number" name="quantity" min="1" max="5">--%>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 profile_bottom" id="submit" runat="server">
                <asp:Button runat="server" ID="btnCancel" Text="Cancel" CssClass="btn black pull-right"
                    ValidationGroup="chk" CausesValidation="False" 
                    PostBackUrl="pending_helprequest.aspx" meta:resourcekey="btnCancelResource1" />
                <asp:Button runat="server" ID="btnsubmit" Text="Save" CssClass="btn blue pull-right"
                    ValidationGroup="chk" OnClick="btnsubmit_click" 
                    meta:resourcekey="btnsubmitResource1" />
            </div>
            <div class="col-xs-12 profile_bottom" id="back" runat="server">
                <asp:Button runat="server" ID="Button1" Text="Back" CssClass="btn black pull-right"
                    CausesValidation="False" PostBackUrl="pending_helprequest.aspx" 
                    meta:resourcekey="Button1Resource1"/>
            </div>
             <asp:HiddenField ID="huserid" runat="server" />
            <asp:HiddenField ID="hcmnyname" runat="server" />
            <asp:HiddenField ID="hskillSubId" runat="server" />
        </div>
    </div>
    <script type="text/jscript">
        function openPDF(url) {
            var w = window.open(url, '_blank');
            w.focus();
        }
    </script>
</asp:Content>

