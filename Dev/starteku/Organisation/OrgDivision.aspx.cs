﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using System.Threading;
using System.Globalization;
using startetku.Business.Logic;

public partial class Organisation_OrgDivision : System.Web.UI.Page
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {

        Page.ClientScript.RegisterStartupScript(Page.GetType(), "my", " SetExpandCollapse();", true);

        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
           // Division.InnerHtml = "Welcome   " + Convert.ToString(Session["OrgUserName"]) + "!";
            GetAllcompetenceCategory();
            Division.InnerHtml = GetLocalResourceObject("Welcome.Text").ToString() + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            ViewState["catParentId"] = "0";
            PopulateRootLevel();
            GetAllDivision();
            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                GetAllDivisionbyid();
            }
        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion

    #region method
    protected void SetDefaultMessage()
    {

        string id = "?id=";
        string childurl = "child.aspx";
        if (Request.RawUrl.ToString().ToLower().Contains(childurl.ToLower()))
        {
            if (Request.RawUrl.ToString().ToLower().Contains(id.ToLower()))
            {
               // lblDataDisplayTitle.Text = "Edit Children";
            }
            else
            {
               // lblDataDisplayTitle.Text = "Add Children";
            }
        }
    }
    protected void GetAllDivision()
    {
        DivisionBM obj = new DivisionBM();
        obj.depIsActive = true;
        obj.depIsDeleted = false;
        obj.depCompanyId = Convert.ToInt32(Session["OrgUserId"]);
        obj.GetAllDivision();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlDepartment.Items.Clear();

                ddlDepartment.DataSource = ds.Tables[0];
                ddlDepartment.DataTextField = "divName";
                ddlDepartment.DataValueField = "divId";
                ddlDepartment.DataBind();

                ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
            }
            else
            {
                ddlDepartment.Items.Clear();
                ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
            }
        }
        else
        {
            ddlDepartment.Items.Clear();
            ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
        }

    }
    protected void InsertDivision()
    {
        DivisionBM obj2 = new DivisionBM();
        obj2.DivisionCheckDuplication(txtDepartmentName.Text, -1,-1);
        String returnMsg = obj2.ReturnString;
        if (returnMsg == "")
        {
            DivisionBM obj = new DivisionBM();
            obj.depName = txtDepartmentName.Text;
            obj.depNameDN = txtDepartmentNameDN.Text;
            //if (Convert.ToInt32(ddlDepartment.SelectedValue) == 0)
            //{
            //    obj.depPerentId = 0;
            //}
            //else
            //{
            //    obj.depPerentId = Convert.ToInt32(ddlDepartment.SelectedValue);
            //}
            obj.depPerentId = getvalue();
            obj.depCompanyId = Convert.ToInt32(Session["OrgUserId"]);
            obj.depIsActive = true;
            obj.depIsDeleted = false;
            obj.depCreatedDate = DateTime.Now;
            obj.depDepId = 0;
            obj.divDescription = txtDESCRIPTION.Text;
            string str_clk_list_QualityArea = string.Empty;
            for (int i = 0; i < chkList.Items.Count; i++)
            {
                if (chkList.Items[i].Selected)
                {
                    if (string.IsNullOrEmpty(str_clk_list_QualityArea))
                    {
                        str_clk_list_QualityArea = chkList.Items[i].Value;
                    }
                    else
                    {
                        str_clk_list_QualityArea = str_clk_list_QualityArea + "," + chkList.Items[i].Value;
                    }
                }
            }
            //
            obj.divcatId = str_clk_list_QualityArea;
            obj.InsertDivision();

            DataSet ds = obj.ds;

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    TargetMaxLongBM obj1 = new TargetMaxLongBM();
                    obj1.CompanyID = Convert.ToInt32(Session["OrgCompanyId"]);
                    obj1.UserID = Convert.ToInt32(Session["OrgUserId"]);
                    obj1.tarType = "Divisions";
                    obj1.RefID = Convert.ToInt32(ds.Tables[0].Rows[0]["JobID"]);
                    obj1.ComptenceCatIDs = obj.divcatId;

                    obj1.InsertUpdateTargetMaxLong();
                }
            }

            if (obj.ReturnBoolean == true)
            {
                Response.Redirect("OrgDivisionList.aspx?msg=ins");
            }
        }
        else
        {
            if (returnMsg == "divName")
                lblMsg.Text = CommonModule.msgNameAlreadyExists;
            else
                lblMsg.Text = CommonModule.msgProblemInsertRecord;
        }
    }
    protected void updateDivision()
    {
        DivisionBM obj2 = new DivisionBM();
        obj2.DivisionCheckDuplication(txtDepartmentName.Text, Convert.ToInt32(Request.QueryString["id"]),Convert.ToInt32(Session["OrgCompanyId"]));
        String returnMsg = obj2.ReturnString;
        if (returnMsg == "")
        {
            DivisionBM obj = new DivisionBM();
            obj.depId = Convert.ToInt32(Request.QueryString["id"]);
            obj.depName = txtDepartmentName.Text;
            obj.depNameDN = txtDepartmentNameDN.Text;
            //if (Convert.ToInt32(ddlDepartment.SelectedValue) == 0)
            //{
            //    obj.depPerentId = 0;
            //}
            //else
            //{
            //    obj.depPerentId = Convert.ToInt32(ddlDepartment.SelectedValue);
            //}   
            obj.depPerentId = getvalue();
            obj.depUpdatedDate = DateTime.Now;
            obj.divDescription = txtDESCRIPTION.Text;
            //
            string str_clk_list_QualityArea = string.Empty;
            for (int i = 0; i < chkList.Items.Count; i++)
            {
                if (chkList.Items[i].Selected)
                {
                    if (string.IsNullOrEmpty(str_clk_list_QualityArea))
                    {
                        str_clk_list_QualityArea = chkList.Items[i].Value;
                    }
                    else
                    {
                        str_clk_list_QualityArea = str_clk_list_QualityArea + "," + chkList.Items[i].Value;
                    }
                }
            }
            //
            obj.divcatId = str_clk_list_QualityArea;
            obj.UpdateDivision();

            #region TargetSet
            TargetMaxLongBM obj1 = new TargetMaxLongBM();
            obj1.CompanyID = Convert.ToInt32(Session["OrgCompanyId"]);
            obj1.UserID = Convert.ToInt32(Session["OrgUserId"]);
            obj1.tarType = "Divisions";
            obj1.RefID = Convert.ToInt32(Request.QueryString["id"]);
            obj1.ComptenceCatIDs = obj.divcatId;

            obj1.InsertUpdateTargetMaxLong();
            #endregion

            if (obj.ReturnBoolean == true)
            {
                Response.Redirect("OrgDivisionList.aspx?msg=upd");
            }
        }
        else
        {
            if (returnMsg == "divName")
                lblMsg.Text = CommonModule.msgNameAlreadyExists;
            else
                lblMsg.Text = CommonModule.msgProblemInsertRecord;
        }

    }
    protected void GetAllDivisionbyid()
    {
        DivisionBM obj = new DivisionBM();
        obj.depId = Convert.ToInt32(Request.QueryString["id"]);
        obj.GetAllDivisionbyid();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["divName"])))
                txtDepartmentName.Text = Convert.ToString(ds.Tables[0].Rows[0]["divName"]);


            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["divNameDN"])))
                txtDepartmentNameDN.Text = Convert.ToString(ds.Tables[0].Rows[0]["divNameDN"]);


            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["divDescription"])))
                txtDESCRIPTION.Text = Convert.ToString(ds.Tables[0].Rows[0]["divDescription"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["divPerentId"])))
                ddlDepartment.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["divPerentId"]);
            ViewState["catParentId"] = Convert.ToString(ds.Tables[0].Rows[0]["divPerentId"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["divcatId"])))
            //var x= "1";
            {

                var catIds = Convert.ToString(ds.Tables[0].Rows[0]["divcatId"]);
                string[] list = catIds.Split(Convert.ToChar(","));
                //list.AddRange(values.Split(new char[] { ',' }));


                for (var j = 0; j <= chkList.Items.Count - 1; j++)
                {
                    foreach (var i in list)
                    {
                        if (chkList.Items[j].Value.Contains(i))
                        {
                            chkList.Items[j].Selected = true;
                            //  break;
                        }
                    }
                }
            }
        }

    }
    protected void GetAllcompetenceCategory()
    {
        CompetenceMasterBM obj = new CompetenceMasterBM();
        obj.catIsActive = true;
        obj.catIsDeleted = false;
        obj.catCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllcompetenceCategory();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (Convert.ToString(Session["Culture"]) == "Danish")
                {
                    ds.Tables[0].Columns["catName"].ColumnName = "abcd";
                    ds.Tables[0].Columns["catNameDN"].ColumnName = "catName";

                }

                chkList.DataSource = ds.Tables[0];
                chkList.DataTextField = "catName";
                chkList.DataValueField = "catId";
                chkList.DataBind();
            }
            else
            {
                chkList.Items.Clear();
                //ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownCategory, CommonModule.dropDownZeroValue));
            }

        }
        else
        {
            chkList.Items.Clear();
        }

    }
    #endregion

    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Request.QueryString["id"]))
        {
            updateDivision();
        }
        else
        {
            InsertDivision();

        }
    }
    protected void btnCancel_click(object sender, EventArgs e)
    {
        Response.Redirect("OrgDivisionList.aspx");
    }
    #endregion


    #region TreeView
    private void FillCategoryChecklist(TreeNodeCollection nodes, int id)
    {

        foreach (TreeNode child in nodes)
        {

            if (Convert.ToString(id) == Convert.ToString(child.Value))
            {
                child.Checked = true;

            }

            FillCategoryChecklist(child.ChildNodes, id);
        }


    }
    protected void TreeView_TreeNodePopulate(object sender, TreeNodeEventArgs e)
    {
        int ParentCatagoryID = Int32.Parse(e.Node.Value);
        PopulateSubLevel(ParentCatagoryID, e.Node);
        FillCategoryChecklist(TreeView1.Nodes, Convert.ToInt32(ViewState["catParentId"]));
    }
    private void PopulateSubLevel(int ParentCatagoryID, TreeNode parentNode)
    {
        //Your sublevel Datatable ie. dtSub

        DivisionBM obj = new DivisionBM();
        obj.depPerentId = ParentCatagoryID;
        obj.depCompanyId = Convert.ToInt32(Session["OrgUserId"]);
        obj.GetDivisionsFromParentId();
        DataSet ds = obj.ds;
        PopulateNodes(ds.Tables[0], parentNode.ChildNodes);


    }
    private void PopulateNodes(DataTable dt, TreeNodeCollection nodes)
    {
        foreach (DataRow dr in dt.Rows)
        {
            TreeNode tn = new TreeNode();
            tn.Value = dr["divId"].ToString();
            tn.Text = dr["divName"].ToString();
            nodes.Add(tn);
            //If node has child nodes, then enable on-demand populating
            tn.PopulateOnDemand = ((int)(dr["catChildCount"]) > 0);
        }
    }
    private void PopulateRootLevel()
    {
        DataTable table = new DataTable();
        DataColumn column = new DataColumn();
        column.DataType = System.Type.GetType("System.Int32");
        column.ColumnName = "divId";
        table.Columns.Add(column);

        // Create second column.
        column = new DataColumn();
        column.DataType = Type.GetType("System.String");
        column.ColumnName = "divName";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.Int32");
        column.ColumnName = "catChildCount";
        table.Columns.Add(column);

        DataRow dr = table.NewRow();
        dr["divId"] = 0;
        dr["divName"] = "Root";
        dr["catChildCount"] = 10;
        table.Rows.Add(dr);
        table.AcceptChanges();


        DivisionBM obj = new DivisionBM();
        obj.depPerentId = 0;
        obj.depCompanyId = Convert.ToInt32(Session["OrgUserId"]);
        obj.GetDivisionsFromParentId();
        DataSet ds = obj.ds;
        PopulateNodes(table, TreeView1.Nodes);

    }
    private Int32 getCategory(TreeNodeCollection treeNode, int ret)
    {
        foreach (TreeNode child in treeNode)
        {
            if (child.Checked == true)
            {
                ret = Convert.ToInt32(child.Value);
                return ret;
            }

            ret = getCategory(child.ChildNodes, ret);
        }
        return ret;
    }

    protected Int32 getvalue()
    {

        if (TreeView1.CheckedNodes.Count > 0)
        {

            foreach (TreeNode node in TreeView1.CheckedNodes)
            {

                if (node.Parent != null)
                {
                    string checkedValue = node.Text.ToString();
                    Int32 ret = Convert.ToInt32(node.Value);
                    return ret;
                }

            }
            foreach (TreeNode node in TreeView1.Nodes)
            {
                if (node.Checked == true)
                {
                    string checkedValue = node.Text.ToString();
                    Int32 ret = Convert.ToInt32(node.Value);
                    return ret;
                }
            }
            //stablish the session variable only when the foreach has finished
            //Session["listActivity"] = listActivity;
        }
        return 0;
    }
    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        //string language = "en-GB";
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Danish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion
}