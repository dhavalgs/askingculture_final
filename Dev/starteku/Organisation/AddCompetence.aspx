﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master" AutoEventWireup="true" CodeFile="AddCompetence.aspx.cs" Inherits="AddCompetence" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<style type="text/css">
        .yellow
        {
            border-radius: 0px;
        }
         .table > thead > tr > th
        {
            vertical-align: middle;
        }
    </style>    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <div class="col-md-6">
        <div class="heading-sec">
            <h1 style="margin-left: 15px;">
            
             <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Competence" enableviewstate="false"/>
               <%-- <%= CommonMessages.Competence%>--%>
               <i><span> <asp:Literal ID="Literal5" runat="server" meta:resourcekey="Welcome" EnableViewState="false" /></span><span runat="server" id="Competence"></span></i>
            </h1>
        </div>
    </div>
    <div class="col-md-12" style="margin-top: 20px;">
        <div id="graph-wrapper">
            <div class="col-md-12">
               <%-- <asp:Button runat="server" ID="Button1" PostBackUrl="Registration.aspx?id=1" Text="Add employee"
                    CssClass="btn btn-primary yellow" Visible="False" 
                    meta:resourcekey="Button1Resource1" />--%>
                     <asp:Button runat="server" ID="Button1" PostBackUrl="orgCompetence.aspx" Text="Add Competence"
                    class="btn btn-primary yellow lrg-btn flat-btn add_user"  style="border-radius: 5px;"
                    meta:resourcekey="Button1Resource1" />
               <%-- <a href="#add-post-title" data-toggle="modal" title="">
                    <button type="button" class="btns  yellow  lrg-btn flat-btn add_user" style="border: 0px;">
                        <%= CommonMessages.AddNew%> <%= CommonMessages.Competence%></button></a>--%>
                <br /><br/>
                <div style="clear:both;"></div>
                <br/>
                <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" 
        meta:resourcekey="lblMsgResource1"></asp:Label>
                <br />
                <asp:Label ID="hdnConfirmArchive" style="display:none;"   CssClass="abcde" meta:resourcekey="ConfirmArchive" runat="server" />
                <div class="chart-tab manager_table">
                    <div id="tabs-container manager_table">
                        <asp:GridView ID="gvGrid" runat="server" AutoGenerateColumns="False" CellPadding="0"
                            Width="100%" GridLines="None" DataKeyNames="comId" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                            EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>' OnRowCommand="gvGrid_RowCommand"
                            BackColor="White"  OnRowDataBound="gvGrid_RowDataBound"
                            meta:resourcekey="gvGridResource1">
                            <HeaderStyle CssClass="aa" />
                            <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:TemplateField HeaderText="ID" meta:resourcekey="TemplateFieldResource1">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="3%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="COMPETENCE" 
                                    meta:resourcekey="TemplateFieldResource2">
                                    <ItemTemplate>
                                        <%if (Convert.ToString(Session["Culture"]) == "Danish"){%>
                                        <asp:Label ID="lblcomCompetence" runat="server" 
                                            Text='<%#Eval("comCompetenceDN")%>' 
                                            meta:resourcekey="lblcomCompetenceResource1"></asp:Label>
                                        <% }%>
                                        <%else{ %>
                                        <asp:Label ID="Label1" runat="server" 
                                            Text='<%#Eval("comCompetence")%>'
                                            meta:resourcekey="lblcomCompetenceResource1"></asp:Label>
                                        <%} %>
                                        <asp:HiddenField ID="comId" runat="server" Value="<%# bind('comCompanyId') %>" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px"/>
                                </asp:TemplateField>
                                 
                                 <asp:TemplateField HeaderText="Category" meta:resourcekey="TemplateFieldResource4" >
                                <ItemTemplate>
                                     
                                    <asp:Label ID="lblrNamer" runat="server" Text="<%# bind('catName') %>"></asp:Label>
                                   
                                   
                                   
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Font-Size="14px" />
                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                    Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                            </asp:TemplateField>

                                <asp:TemplateField HeaderText="" ItemStyle-Width="50px" 
                                    meta:resourcekey="TemplateFieldResource3">
                                    <ItemTemplate>                                        
                                        <div class="vat" style="width: 85px" id="divEdit" runat="server">
                                            <p>
                                                     <i class="fa fa-pencil"></i><a href="<%# String.Format("OrgCompetence.aspx?id={0}", Eval("comId")) %>"
                                                title="Edit"><asp:Literal ID="Literal2" runat="server" meta:resourcekey="Edit" enableviewstate="false"/></a>
                                            </p>
                                        </div>
                                        <div class="total" style="width: 70px" id="divDeletet" runat="server">
                                            <p>
                                                <i class="fa fa-trash-o"></i>
                                                
                                                <asp:LinkButton ID="lnkBtnName" CssClass="def" OnClientClick="return xyz();" runat="server" CommandName="archive" CommandArgument='<%# Eval("comId") %>'
                                                    ToolTip="Delete"   meta:resourcekey="lnkBtnNameResource1">Delete</asp:LinkButton>
                                            </p>
                                        </div>
                                        <div class="total" style="width: 200px" id="div1" runat="server">
                                            <p>
                                                <i>
                                                    <img src="images/block.jpg" alt="" height="15px;" width="15px;" /></i>                                                    
                                      <asp:Literal ID="Literal1" runat="server" meta:resourcekey="NotEditable" enableviewstate="false"/> <%-- <%= CommonMessages.NotEditable%>--%>
                                            </p>
                                        </div>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
               
            </div>

        </div>
    </div>
      <div class="modal-footer" style="border-top: 0px !important;  margin-left: -1px;">
                                <asp:Button runat="server" ID="btnBack" Text="Back" CssClass="btn black pull-right"
                                    OnClick="btnBack_click" style="margin: 4px;display: none" />
                              <%--  <a href="#" onclick="javascript:history.back(); return false;" class="btn black pull-right" style="border-radius: 5px;margin-left:0px;margin-top:5px;float:right;">Back</a>--%>
                            </div>
      <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.js"></script>
      <script type="text/javascript" src="https://code.jquery.com/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            setTimeout(function () {
                SetExpandCollapse();
                //ExampanCollapsMenu();
                //ExampanCollapssubMenu();
                // tabMenuClick();
                // $(".Category").addClass("active_page");
            }, 500);
        });
        

        function xyz() {
            return confirm($(".abcde").text());
            //$('.def').attr("onclick", "return confirm('" + $(".abcde").text() + "')");
        }


    </script>
  
</asp:Content>

