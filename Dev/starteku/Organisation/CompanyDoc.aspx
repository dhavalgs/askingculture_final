﻿<%@ page title="" language="C#" masterpagefile="~/Organisation/OrganisationMaster.master" autoeventwireup="true" CodeFile="CompanyDoc.aspx.cs" inherits="Organisation_CompanyDoc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<style type="text/css">
        .document inbox-hover black a i {
            margin-top: 9px;
        }
    
    </style>
    <script type="text/jscript">
        function openPDF(url) {
            var w = window.open(url, '_blank');
            w.focus();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="col-md-6">
        <div class="heading-sec">
            <h1>Documents  <i>Welcome To Bette Bayer!</i></h1>
        </div>
    </div>
    <div class="col-md-12" style="margin-top: 20px;">
        <a href="#add-post-title" data-toggle="modal" title="">
            <button type="button" class="btns  yellow  lrg-btn flat-btn add_user" style="border: 0px;">Add New Document</button></a>
        <div aria-hidden="true" role="dialog" tabindex="-1" class="modal fade" id="add-post-title" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header blue">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">× </button>
                        <h4 class="modal-title">New Document</h4>
                    </div>
                    <div class="modal-body">
                        <asp:TextBox runat="server" placeholder="TITLE :" ID="txttitle" MaxLength="50" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txttitle"
                            ErrorMessage="Please enter title." CssClass="commonerrormsg" Display="Dynamic"
                            ValidationGroup="chk"></asp:RequiredFieldValidator>
                        <br />
                        <asp:TextBox runat="server" placeholder="DESCRIPTION :" ID="txtDESCRIPTION" MaxLength="50" TextMode="MultiLine" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDESCRIPTION"
                            ErrorMessage="Please enter DESCRIPTION." CssClass="commonerrormsg" Display="Dynamic"
                            ValidationGroup="chk"></asp:RequiredFieldValidator>
                        <br />

                        <label for="exampleInputFile"><i class="fa fa-paperclip attch_file"></i>Attachments...</label>
                        <asp:FileUpload ID="FileUpload1" runat="server" />
                        <asp:RegularExpressionValidator ID="reFile1" runat="server" ControlToValidate="FileUpload1"
                            CssClass="legend" Display="Dynamic" ErrorMessage="Please upload only .pdf File."
                            ValidationExpression="^.*\.(PDF|pdf)$" ForeColor="Red" ValidationGroup="chk"></asp:RegularExpressionValidator>
                        <br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="FileUpload1"
                            ErrorMessage="Please select file ." CssClass="commonerrormsg" Display="Dynamic"
                            ValidationGroup="chk"></asp:RequiredFieldValidator>
                        <br />
                    </div>
                    <div class="modal-footer">                       
                        <asp:Button runat="server" ID="btnsubmit" Text="Submit" CssClass="btn btn-primary yellow" ValidationGroup="chk" OnClick="btnsubmit_click" />
                        <button data-dismiss="modal" class="btn btn-default black" type="button">Close </button>

                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
        </div>

        <ul class="document">
            <asp:Repeater ID="rep_document" runat="server">
                <ItemTemplate>
                    <li>
                        <div class="document1">
                            <img src="images/documents_icon.png">
                        </div>
                        <h1><%# Eval("comTitle") %></h1>
                        <span><%# Eval("MinAgo") %> </span><i><%# Eval("FullName") %> </i>
                        <p style="height:60px;"><%# Eval("comDescription") %></p>
                        <div class="inbox-hover black">
                            <a href="../Log/upload/CompanyDoc/<%# Eval("docattachment") %>" title="" data-tooltip="Doenload" data-placement="bottom" download><i class="fa fa-download" style="margin-top: 9px;"></i></a>
                             <a href="#" title="" data-tooltip="Read" data-placement="bottom" onclick="openPDF('../Log/upload/CompanyDoc/<%# Eval("docattachment") %>');"><i class="fa fa-eye" style="margin-top: 9px;"></i></a>
                            <asp:LinkButton runat="server" ID="btn_delete" CommandName='<%# Eval("comId") %>' data-tooltip="Delete" data-placement="bottom" OnClick="btn_delete_Click"><i class="fa fa-trash-o" style="margin-top: 9px;"></i></asp:LinkButton>
                        </div>
                    </li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>

    </div>

</asp:Content>

