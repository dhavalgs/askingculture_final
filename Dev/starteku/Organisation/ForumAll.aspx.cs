﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using starteku_BusinessLogic.Model;
using starteku_BusinessLogic.View;
using startetku.Business.Logic;
using System.Threading;
using System.Globalization;

public partial class Organisation_ForumAll : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        btnresetfilter.Text = ResetFilter.Text;
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {

            Conatct.InnerHtml = GetLocalResourceObject("Welcome.Text").ToString() + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            ViewState["catParentId"] = "0";
            GetAllForumData("All");
            GetAllForumCategoryByCompanyId();
            GetAllUserCreatedBy();
           // GetAllForumTagsByCompanyId();
            GetAllDivisionByCompanyId();
            GetAllJobTypeByCompanyId();
            CompetenceSelectAll();
        }
    }
    protected void btnresetfilter_OnClick(object sender, EventArgs e)
    {
        Response.Redirect("ForumAll.aspx");
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    protected void CompetenceSelectAll()
    {
        CompetenceMasterBM obj = new CompetenceMasterBM();
        obj.comIsActive = true;
        obj.comIsDeleted = false;
        obj.comCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllCompetenceMasterAdd();


        DataSet ds = obj.ds;
        ViewState["ds"] = ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                var str = Session["Culture"];
                if (Convert.ToString(Session["Culture"]) == "Danish")
                {
                    ds.Tables[0].Columns["comCompetence"].ColumnName = "abcd";
                    ds.Tables[0].Columns["comCompetenceDN"].ColumnName = "comCompetence";


                }

                ddlcompetencefilter.DataTextField = "comCompetence";
                ddlcompetencefilter.DataValueField = "comId";
                ddlcompetencefilter.DataSource = ds.Tables[0];
                ddlcompetencefilter.DataBind();
                ddlcompetencefilter.Items.Insert(0, hdnSelectCompetence.Value);

            }
            else
            {
                ddlcompetencefilter.DataSource = null;
                ddlcompetencefilter.DataBind();

                //ddlcompetencefilter.DataSource = null;
                //ddlcompetencefilter.DataBind();
            }
        }
        else
        {
            ddlcompetencefilter.DataSource = null;
            ddlcompetencefilter.DataBind();


        }

        //DivisionBM obj = new DivisionBM();
        //obj.depIsActive = true;
        //obj.depIsDeleted = false;
        //obj.depCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        //obj.GetAllDocCategory();
        //DataSet ds = obj.ds;
        //ViewState["ds"] = ds;

        //if (ds != null)
        //{
        //    if (ds.Tables[0].Rows.Count > 0)
        //    {
        //        var str = Session["Culture"];
        //        if (Convert.ToString(Session["Culture"]) == "Danish")
        //        {
        //            ds.Tables[0].Columns["dcName"].ColumnName = "abcd";
        //            ds.Tables[0].Columns["dcNameDN"].ColumnName = "dcName";

        //        }

              

        //        ddlcompetencefilter.DataTextField = "dcName";
        //        ddlcompetencefilter.DataValueField = "dccatId";
        //        ddlcompetencefilter.DataSource = ds.Tables[0];
        //        ddlcompetencefilter.DataBind();
        //        ddlcompetencefilter.Items.Insert(0, "Select Competence");




        //    }
        //    else
        //    {
               

        //        ddlcompetencefilter.DataSource = null;
        //        ddlcompetencefilter.DataBind();
        //    }
        //}
        //else
        //{
          

        //    ddlcompetencefilter.DataSource = null;
        //    ddlcompetencefilter.DataBind();
        //}
    }
    protected void GetAllDivisionByCompanyId()
    {
        DivisionBM obj = new DivisionBM();
        obj.depIsActive = true;
        obj.depIsDeleted = false;
        obj.depCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);


        obj.GetAllDivisionsbyCompanyId();


        DataSet ds = obj.ds;
        if (Convert.ToString(Session["Culture"]) == "Danish")
        {
            ds.Tables[0].Columns["divName"].ColumnName = "abcd";
            ds.Tables[0].Columns["divNameDN"].ColumnName = "divName";

        }
        ViewState["ds"] = ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                

                //For Searching-filter
                ddSeachDivision.DataSource = ds.Tables[0];
                ddSeachDivision.DataTextField = "divName";
                ddSeachDivision.DataValueField = "divId";
                ddSeachDivision.DataBind();
                ddSeachDivision.Items.Insert(0, hdnSelectDivision.Value);

            }

        }
    }
    /*----------------------GET Jobtype BY Id -----------------------------------------*/
    protected void GetAllJobTypeByCompanyId()
    {
        JobTypeBM obj = new JobTypeBM();
        obj.jobIsActive = true;
        obj.jobIsDeleted = false;
        obj.jobCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);


        obj.GetAllJobTypeByCompanyId();


        DataSet ds = obj.ds;
        if (Convert.ToString(Session["Culture"]) == "Danish")
        {
            ds.Tables[0].Columns["jobName"].ColumnName = "abcd1";
            ds.Tables[0].Columns["jobNameDN"].ColumnName = "jobName";

        }
        ViewState["ds"] = ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                

                ddSearchJobType.DataSource = ds.Tables[0];
                ddSearchJobType.DataTextField = "jobName";
                ddSearchJobType.DataValueField = "jobId";
                ddSearchJobType.DataBind();
                ddSearchJobType.Items.Insert(0, hdnSelectJob.Value);
            }

        }
    }
    private void GetAllUserCreatedBy()
    {
        var db = new startetkuEntities1();
        try
        {
            var createdBy = Convert.ToString(Session["OrgCreatedBy"]);
            var companyId = Convert.ToInt32(Session["OrgCompanyId"]);

            List<UserMasterView> userList;
            if (Convert.ToString(Session["OrgUserType"]) == "1")
            {
                userList = (from o in db.UserMasters where (o.userCompanyId == companyId || o.userId == companyId) select new UserMasterView() { UserFirstName = o.userFirstName + " " + o.userLastName, UserId = o.userId }).ToList();
            }
            else
            {
                userList = (from o in db.UserMasters
                            where (o.userCreateBy == createdBy || o.userId == companyId)
                            select new UserMasterView() { UserFirstName = o.userFirstName + " " + o.userLastName, UserId = o.userId }).ToList();
            }
            if (userList != null && userList.Any())
            {


                ddSearchPerson.DataSource = userList;
                ddSearchPerson.DataTextField = "UserFirstName";
                ddSearchPerson.DataValueField = "UserId";
                ddSearchPerson.DataBind();
            }
            else
            {
                ddSearchPerson.DataSource = null;
                ddSearchPerson.DataBind();
            }
            ddSearchPerson.Items.Insert(0, hdnSelectPerson.Value);

        }
        catch (Exception e)
        {

            ExceptionLogger.LogException(e, Convert.ToInt32(Session["OrgUserId"]), "Error In Library function GetAllUserCreatedBy");
        }
    }
    public void GetAllForumData(string categoryName)
    {
        try
        {
            ForumMasterBM obj = new ForumMasterBM();
            obj.fmIsActive = true;
            obj.fmIsDeleted = false;
            obj.fmCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            // obj.userCreateBy = Convert.ToString(Session["OrgUserId"]);
            obj.GetAllForum();
            DataSet ds = obj.ds;
            if (Convert.ToString(Session["Culture"]) == "Danish")
            {
                ds.Tables[0].Columns["fmCategoryName"].ColumnName = "abcd12";
                ds.Tables[0].Columns["fmCategoryNameDN"].ColumnName = "fmCategoryName";

            }
            ViewState["GetAllForumData"] = ds;

            DataView dv = new DataView();
            dv = ds.Tables[0].DefaultView;
            if (!string.IsNullOrWhiteSpace(txtsearch.Text))
            {

                dv.RowFilter = "(fmQuestionName LIKE '%" + txtsearch.Text + "%')";
                dv.RowFilter += "or (fmTagNames LIKE '%" + txtsearch.Text + "%')";
            }
            if (!string.IsNullOrWhiteSpace(categoryName) && categoryName != "All")
            {
                dv.RowFilter = "(fmCategoryName LIKE '%" + categoryName + "%')";

            }

            DataTable dtitm = dv.ToTable();
            DataSet ds1 = new DataSet();
            ds1.Tables.Add(dtitm);

            foreach (DataTable table in ds1.Tables)
            {
                foreach (DataRow dr in table.Rows)
                {
                    dr["MinAgo"] = dr["MinAgo"].ToString().Replace("Second", GetLocalResourceObject("Second.Text").ToString());
                    dr["MinAgo"] = dr["MinAgo"].ToString().Replace("Minutes", GetLocalResourceObject("Minutes.Text").ToString());
                    dr["MinAgo"] = dr["MinAgo"].ToString().Replace("Hours", GetLocalResourceObject("Hours.Text").ToString());
                    dr["MinAgo"] = dr["MinAgo"].ToString().Replace("Days", GetLocalResourceObject("Days.Text").ToString());

                }
            }


            ViewState["data"] = ds1;


            if (ds1.Tables[0].Rows.Count > 0)
            {
                rptForumAll.DataSource = ds1;
                rptForumAll.DataBind();
                lblNoRecordsFound.Visible = false;
            }
            else
            {
                rptForumAll.DataSource = null;
                rptForumAll.DataBind();
                lblNoRecordsFound.Visible = true;
            }
        }
        catch (DataException ex)
        {
            Common.WriteLog("error in GetAllForumData" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
        }
    }

    private void GetAllForumCategoryByCompanyId()
    {
        try
        {
            //CategoryBM obj = new CategoryBM();
            //obj.depIsActive = true;
            //obj.depIsDeleted = false;
            //obj.depCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            //obj.GetAllCompetenceCategory();

            CompetenceMasterBM obj = new CompetenceMasterBM();
            obj.comIsActive = true;
            obj.comIsDeleted = false;
            obj.comCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            obj.GetAllCompetenceMasterAdd();

            DataSet ds = obj.ds;

            ViewState["ds"] = ds;

            if (ds != null)
            {
                if (Convert.ToString(Session["Culture"]) == "Danish")
                {
                    ds.Tables[0].Columns["comCompetence"].ColumnName = "abcd12";
                    ds.Tables[0].Columns["comCompetenceDN"].ColumnName = "comCompetence";

                }
                ddForumCat.Items.Clear();
                ddForumCat.DataSource = ds.Tables[0];
                ddForumCat.DataTextField = "comCompetence";
                ddForumCat.DataValueField = "comId";
                ddForumCat.DataBind();
                ddForumCat.Items.Insert(0, new ListItem("All", CommonModule.dropDownZeroValue));

            }
        }
        catch (Exception ex)
        {
            Common.WriteLog("error in GetAllCompetenceMasterAdd" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));

        }
        finally
        {

        }
    }
    //private void GetAllForumTagsByCompanyId()
    //{
    //    try
    //    {
    //        return;
    //        var ds = (DataSet) ViewState["GetAllForumData"];

    //        if (ds != null)
    //        {
    //            /*ddForumTags.Items.Clear();
    //            ddForumTags.DataSource = ds.Tables[0].Rows[];
    //            ddForumTags.DataTextField = "fmTagNames";
    //            ddForumTags.DataValueField = "forumMasterId";
    //            ddForumTags.DataBind();
    //            ddForumTags.Items.Insert(0, new ListItem("All", CommonModule.dropDownZeroValue));*/

    //        }
    //    }
    //    catch (Exception ex)
    //    {


    //    }
    //    finally
    //    {

    //    }
    //}
    protected void txtsearch_TextChanged(object sender, EventArgs e)
    {
        
        GetAllForumData("");
    }

    protected void ddForumCat_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        GetAllForumData(ddForumCat.SelectedItem.Text);
    }
    #region SearchFilter
    protected void ddSearchJob_Change(object sender, EventArgs e)
    {
        FilterResult();
    }

    private void FilterResult()
    {
        var db = new startetkuEntities1();
        try
        {
            var compaynId = Convert.ToInt32(Session["OrgCompanyId"]);
            var documentList = (from o in db.GetAllForum(false, true,compaynId) select o).ToList();

            
            // LEVEL
            if (!string.IsNullOrEmpty(txtsearch.Text))
            {

                documentList = documentList.Where(o => o.fmQuestionName.ToLower().Contains(txtsearch.Text.ToLower())).ToList();

            }

            // LEVEL
            var ddSearchLevelV = Convert.ToString(ddSearchLevel.SelectedItem.Value);
            if (ddSearchLevelV != "0")
            {
                documentList = documentList.Where(o => o.fmLevel == ddSearchLevelV).ToList();

            }


            //Date - From

            if (!string.IsNullOrEmpty(txtFromDate.Text))
            {
                var fromDt = Convert.ToDateTime(txtFromDate.Text);
                documentList = documentList.Where(o => o.fmCreatedDate >= fromDt).ToList();

            }

            //Date - To 

            if (!string.IsNullOrEmpty(txtTodate.Text))
            {
                var toDt = Convert.ToDateTime(txtTodate.Text);
                documentList = documentList.Where(o => o.fmCreatedDate <= toDt).ToList();

            }

            // Job Type
            var ddSearchJobId = Convert.ToString(ddSearchJobType.SelectedItem.Value);
            if (ddSearchJobId != hdnSelectJob.Value)
            {
                documentList = documentList.Where(o => o.fmJobTypeId == ddSearchJobId).ToList();

            }

            //ddSeachDivision
            var divId = Convert.ToString(ddSeachDivision.SelectedItem.Value);
            if (divId != hdnSelectDivision.Value)
            {
                documentList = documentList.Where(o => o.fmDivId == divId).ToList();

            }

            //Competence

            var compId = Convert.ToString(ddlcompetencefilter.SelectedItem.Value);
            if (compId != hdnSelectCompetence.Value)
            {
                var compIdInt = Convert.ToInt32(compId);
                documentList = documentList.Where(o => o.fmCategoryId == compIdInt).ToList();

            }

            //Person
            var personId = Convert.ToString(ddSearchPerson.SelectedItem.Value);
            if (personId != hdnSelectPerson.Value)
            {
                var intPersonId = Convert.ToInt32(personId);
                documentList = documentList.Where(o => o.fmCreateByUserID == intPersonId).ToList();
            }

            //Competence
            var doccompetence = Convert.ToString(ddlcompetencefilter.SelectedItem.Value);
            if (doccompetence != hdnSelectCompetence.Value)
            {
                var intCompId = Convert.ToInt32(doccompetence);
                documentList = documentList.Where(o => o.fmCompId == intCompId).ToList();
            }

            //SORT
            var ddSearchSortVal = Convert.ToString(ddSearchSort.SelectedItem.Value);
            if (ddSearchSortVal == "asc")
            {
                documentList = documentList.OrderByDescending(o => o.fmCreatedDate).ToList();
            }
            else
            {
                documentList = documentList.OrderBy(o => o.fmCreatedDate).ToList();

            }

            if (documentList != null && documentList.Any())
            {
                rptForumAll.DataSource = documentList;
                rptForumAll.DataBind();
            }
            else
            {
                rptForumAll.DataSource = null;
                rptForumAll.DataBind();
            }

        }
        catch (Exception e)
        {

            ExceptionLogger.LogException(e);
        }
        finally
        {
            db.Dispose();
        }


    }
    #endregion

    protected void btnFilter_OnClick(object sender, EventArgs e)
    {
        FilterResult();
       // upFilter.Update();
    }

    #region Culture
    protected override void InitializeCulture()
    {
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Danish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion
}