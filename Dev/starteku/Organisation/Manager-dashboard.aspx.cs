﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using System.Configuration;
using startetku.Business.Logic;
using System.Web.Services;
using System.Web.Script.Services;
using System.Threading;
using System.Globalization;
using starteku_BusinessLogic.Model;

public partial class Organisation_Manager_dashboard : System.Web.UI.Page
{
    String Email;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "my", " SetExpandCollapse1();", true);

            if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
            {
                Response.Redirect("login.aspx");
            }
            
            if (!IsPostBack)
            {
                //Dashboard.InnerHtml = CommonMessages.Welcome + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
                Dashboard.InnerHtml = "  " + Convert.ToString(Session["OrgUserName"]) + "!";

                //GetAllEmployeeList();
                //get_ds_to_do_list();
                //GetTO_DO_LISTbyid();
                //   GetAllContact();
                StartJoyRideTour();
                GetAllJobtype();
                GetAllDepartments();
                GetAllTeam();
                GetAllTeamFilter();
                ResourceLanguageBM obj = new ResourceLanguageBM();
                DataSet resds = obj.GetAllResourceLanguage();
                ddlLanguage.DataSource = resds;
                ddlLanguage.DataTextField = "resLanguage";
                ddlLanguage.DataValueField = "resCulture";
                ddlLanguage.DataBind();
                ddlLanguage.Items.Insert(0, new ListItem(GetLocalResourceObject("selectlan.Text").ToString(), "0"));
                ddlLanguage.SelectedValue = "0";
                //ddlLanguage.SelectedIndex =
                //     ddlLanguage.Items.IndexOf(ddlLanguage.Items.FindByValue("Select Language"));

                ddlgender.Items.Insert(0, new ListItem(hdnSelectGender.Value, CommonModule.dropDownZeroValue));
                GetCheckBoxListData();

                if (Request.QueryString["user"] == "saveSuccess")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Redirect", "generate('information','" + hdnSystemUpdated.Text + "','bottomCenter');", true);
                }
                if (Session["isLastLoginSeen"] == null)
                {
                    Session["isLastLoginSeen"] = "true";

                    Page.ClientScript.RegisterStartupScript(this.GetType(), "",
                                                            "setTimeout(function(){generate('warning','" + hdnLastlogin.Value +
                                                            Convert.ToString(Session["userDateTime"]) +
                                                            ".','bottomRight');},990);", true);
                }
            }
            Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "OnLoadCall();", true);

            if (!IsPostBack)
            {
                EmployeeSkillBM obj = new EmployeeSkillBM();

                var userid = 0;
                var usertype = 3;

                userid = Convert.ToInt32(Session["OrgUserId"]);
                usertype = Convert.ToInt32(Session["OrguserType"]);

                obj.skillIsActive = true;
                obj.skillIsDeleted = false;
                obj.skillUserId = userid;
                obj.skillCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);

                obj.temp = 0;
                obj.mulsel = "0";

                obj.SpEmpAvgSkillByManager();

                DataSet resds = obj.ds;

                managercat.DataSource = resds;
                if (Convert.ToString(Session["Culture"]) == "Danish")
                {
                    managercat.DataTextField = "comCompetenceDN";

                }
                else
                {
                    managercat.DataTextField = "comCompetence";

                }
                managercat.DataValueField = "skillComId";
                managercat.DataBind();
            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "Organisation_Manager_dashboard->Page_Load");
        }
    }

    #region StartJoyRideTour

    private void StartJoyRideTour()
    {
        var obj = new JoyrideTourBM();
        try
        {

            var uid = Convert.ToInt32(Session["OrgUserId"]);
            if (uid <= 0) return;

            DataSet ds = obj.GetJoyrideTourByUserId(uid);


            if (ds == null) return;

            var dataRowCollection = ds.Tables[0].Rows;
            if (dataRowCollection != null)
            {
                var isAlreadyTakenTour = dataRowCollection[0]["isTourOff"];
                if (isAlreadyTakenTour != null)
                {
                    var x = Convert.ToInt32(dataRowCollection[0]["isTourOff"]);
                    if (x > 0) return;
                }
            }

            //DateTime date=DateTime.Now.AddDays(-2);
            //if (Session["userDateTime"] != null)
            //{
            //     date = Convert.ToDateTime(Session["userDateTime"]).Date;
            //}

            //var todaysDate = DateTime.Now.Date;

            //if (date == todaysDate)
            //{
            //    return;
            //}
            if (Session["isJoyRideTaken"] == null)
            {
                CmsBM objMail = new CmsBM();
                objMail.cmsName = "WelcomeMessage";

                objMail.SelectMailTemplateByName();
                DataSet dsMail = objMail.ds;

                if (dsMail.Tables[0].Rows.Count > 0)
                {
                    string confirmMail = "";
                    if (Convert.ToString(Session["Culture"]) == "English")
                    {
                        confirmMail = dsMail.Tables[0].Rows[0]["cmsDescription"].ToString();

                    }
                    else
                    {
                        confirmMail = dsMail.Tables[0].Rows[0]["cmsDescriptionDN"].ToString();

                    }
                    welcomeMsg.InnerHtml = confirmMail;
                }
                Session["isJoyRideTaken"] = "true";

                Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "JoyrideStart();", true);
            }
            //  ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "JoyrideStart();", false);
        }
        catch (Exception)
        {


        }
    }
    #endregion
    #region DropDown Event
    protected void ddlcompetence_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetCheckBoxListData();


    }
    protected void ddljobtype_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetCheckBoxListData();

    }

    protected void GetAllTeam()
    {
        JobTypeBM obj = new JobTypeBM();
        obj.TeamIDs = "0";
        obj.jobIsActive = true;
        obj.jobIsDeleted = false;
        obj.jobCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllTeam();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (Convert.ToString(Session["Culture"]) == "Danish")
                {

                    ddlTeam.Items.Clear();

                    ddlTeam.DataSource = ds.Tables[0];
                    ddlTeam.DataTextField = "TeamNameDN";
                    ddlTeam.DataValueField = "TeamID";
                    ddlTeam.DataBind();

                    ddlTeam.Items.Insert(0, new ListItem(hdnSelectTeam.Value, CommonModule.dropDownZeroValue));
                }

                else
                {
                    ddlTeam.Items.Clear();

                    ddlTeam.DataSource = ds.Tables[0];
                    ddlTeam.DataTextField = "TeamName";
                    ddlTeam.DataValueField = "TeamID";
                    ddlTeam.DataBind();

                    ddlTeam.Items.Insert(0, new ListItem(hdnSelectTeam.Value, CommonModule.dropDownZeroValue));

                }
            }
            else
            {
                ddlTeam.Items.Clear();
                ddlTeam.Items.Insert(0, new ListItem(hdnSelectTeam.Value, CommonModule.dropDownZeroValue));
            }
        }
        else
        {
            ddlTeam.Items.Clear();
            ddlTeam.Items.Insert(0, new ListItem(hdnSelectTeam.Value, CommonModule.dropDownZeroValue));
        }

    }
    protected void GetAllTeamFilter()
    {
        JobTypeBM obj = new JobTypeBM();
        obj.TeamIDs = "0";
        obj.jobIsActive = true;
        obj.jobIsDeleted = false;
        obj.jobCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllTeam();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (Convert.ToString(Session["Culture"]) == "Danish")
                {

                    ddlTeamFilter.Items.Clear();

                    ddlTeamFilter.DataSource = ds.Tables[0];
                    ddlTeamFilter.DataTextField = "TeamNameDN";
                    ddlTeamFilter.DataValueField = "TeamID";
                    ddlTeamFilter.DataBind();

                    ddlTeamFilter.Items.Insert(0, new ListItem(hdnSelectTeamFilter.Value, CommonModule.dropDownZeroValue));
                }

                else
                {
                    ddlTeamFilter.Items.Clear();

                    ddlTeamFilter.DataSource = ds.Tables[0];
                    ddlTeamFilter.DataTextField = "TeamName";
                    ddlTeamFilter.DataValueField = "TeamID";
                    ddlTeamFilter.DataBind();

                    ddlTeamFilter.Items.Insert(0, new ListItem(hdnSelectTeamFilter.Value, CommonModule.dropDownZeroValue));

                }
            }
            else
            {
                ddlTeamFilter.Items.Clear();
                ddlTeamFilter.Items.Insert(0, new ListItem(hdnSelectTeamFilter.Value, CommonModule.dropDownZeroValue));
            }
        }
        else
        {
            ddlTeamFilter.Items.Clear();
            ddlTeamFilter.Items.Insert(0, new ListItem(hdnSelectTeamFilter.Value, CommonModule.dropDownZeroValue));
        }

    }
    #endregion

    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        //Insertuser();
    }

    protected void btnserch_click(object sender, EventArgs e)
    {
        GetAllContact();
    }
    protected void TextBox1_TextChanged(object sender, EventArgs e)
    {
        insert_to_do_list();
    }
    protected void Check_Clicked(Object sender, EventArgs e)
    {
        string id = "0";
        CheckBox chk = (CheckBox)sender;
        id = chk.Text;
        delete(Convert.ToInt32(id));

    }
    protected void LinkButton1_click(object sender, EventArgs e)
    {
        GetTO_DO_LISTbyid();
    }
    #endregion

    #region method
    protected void GetCheckBoxListData()
    {
        CompetenceMasterBM obj = new CompetenceMasterBM();
        obj.catIsActive = true;
        obj.catIsDeleted = false;
        if (string.IsNullOrEmpty(ddlDepartment.SelectedValue)) return;
        obj.comDivId = Convert.ToInt32(ddlDepartment.SelectedValue);
        obj.comJobtype = Convert.ToInt32(ddljobtype.SelectedValue);
        obj.GetAllCategoryByJobTypeId_DivId();
        // lblDivIdName.Text = ddlDepartment.SelectedItem.Text;

        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                repCompCat.DataSource = ds.Tables[0];
                //chkList.DataTextField = "catName";
                //chkList.DataValueField = "catId";
                repCompCat.DataBind();
                //chkList.SelectedIndex = 0;

                NoRecords.Visible = false;
                //foreach (ListItem li in chkList.Items)
                //{
                //    li.Selected = true;
                //}
            }
            else
            {
                NoRecords.Visible = true;
                repCompCat.DataSource = null;
                //chkList.DataTextField = "catName";
                //chkList.DataValueField = "catId";
                repCompCat.DataBind();
                //chkList.Items.Clear();
                //ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownCategory, CommonModule.dropDownZeroValue));
            }

        }
        else
        {
            NoRecords.Visible = true;
            //chkList.Items.Clear();
            repCompCat.DataSource = null;
            //chkList.DataTextField = "catName";
            //chkList.DataValueField = "catId";
            repCompCat.DataBind();
        }
    }
    protected void get_ds_to_do_list()
    {
        TO_DO_LISTBM obj = new TO_DO_LISTBM();
        obj.listIsActive = true;
        obj.listIsDeleted = false;
        obj.listUserId = Convert.ToInt32(Session["OrgUserId"]);
        obj.GetTO_DO_LISTbyid();
        DataSet ds = obj.ds;
        ViewState["dolist"] = ds;
        GetTO_DO_LISTbyid();
    }
    protected void insert_to_do_list()
    {
        DateTime startdate;
        DateTime Enddate;
        String s = HiddenField1.Value;
        if (s != "0")
        {
            string[] str = s.Split('-');

            startdate = Convert.ToDateTime(str[0]);
            Enddate = Convert.ToDateTime(str[1]);
        }
        else
        {
            startdate = Convert.ToDateTime(DateTime.Now).AddDays(-30);
            Enddate = Convert.ToDateTime(DateTime.Now);
        }

        TO_DO_LISTBM obj = new TO_DO_LISTBM();
        obj.listUserId = Convert.ToInt32(Session["OrgUserId"]);
        obj.listsubject = TextBox1.Text;
        obj.listCreateBy = 0;
        obj.listCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.listIsActive = true;
        obj.listIsDeleted = false;
        obj.listCreatedDate = Enddate;
        obj.InsertTO_DO_LIST();
        if (obj.ReturnBoolean == true)
        {
            get_ds_to_do_list();
            //GetTO_DO_LISTbyid();
            TextBox1.Text = "";
        }
        else
        {
            lblMsg.Text = CommonModule.msgProblemInsertRecord;
        }
    }
    protected void GetTO_DO_LISTbyid()
    {
        DateTime startdate;
        DateTime Enddate;
        String s = HiddenField1.Value;
        if (s != "0")
        {
            string[] str = s.Split('-');

            startdate = Convert.ToDateTime(str[0]);
            Enddate = Convert.ToDateTime(str[1]);
        }
        else
        {
            startdate = Convert.ToDateTime(DateTime.Now).AddDays(-30);
            Enddate = Convert.ToDateTime(DateTime.Now);
        }


        DataSet ds = (DataSet)ViewState["dolist"];
        DataView dv = new DataView();
        dv = ds.Tables[0].DefaultView;
        dv.RowFilter = "listCreatedDate >= '" + startdate.ToString("MM-dd-yyyy") + "' AND listCreatedDate <= '" + Enddate.ToString("MM-dd-yyyy") + "'";
        //dv.RowFilter = "listCreatedDate BETWEEN '" + startdate + "' AND '" + Enddate + "'";

        DataTable dtitm = dv.ToTable();
        DataSet ds1 = new DataSet();
        ds1.Tables.Add(dtitm);

        if (ds1 != null)
        {
            if (ds1.Tables[0].Rows.Count > 0)
            {
                Repeaterlist.DataSource = ds1.Tables[0];
                Repeaterlist.DataBind();

                // Repeateremp.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                Repeaterlist.DataSource = null;
                Repeaterlist.DataBind();
            }
        }
        else
        {
            Repeaterlist.DataSource = null;
            Repeaterlist.DataBind();
        }

    }

    protected void GetAllEmployeeList()
    {
        UserBM obj = new UserBM();
        obj.userIsActive = true;
        obj.userIsDeleted = false;
        obj.userType = 3;
        obj.userCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        // obj.GetAllEmployee();
        obj.userCreateBy = Convert.ToString(Session["OrgUserId"]);
        obj.GetAllEmployeecreatebymanager();
        DataSet ds = obj.ds;

        //Repeater1.DataSource = dt;
        // Repeater1.DataBind();

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                //Repeateremp.DataSource = ds.Tables[0];
                //Repeateremp.DataBind();

                // Repeateremp.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                //Repeateremp.DataSource = null;
                //Repeateremp.DataBind();
            }
        }
        else
        {
            //Repeateremp.DataSource = null;
            //Repeateremp.DataBind();
        }
    }

    public static DataView GetView(DataSet ds, string filter, string sort)
    {
        try
        {
            DataView dv = new DataView(ds.Tables[0]);
            dv.Sort = sort;
            dv.RowFilter = filter;
            return dv;
        }
        catch (Exception ex)
        {

            Common.WriteLog("GetView===============Error============" + ex.StackTrace);
            return null;
        }

    }
    protected void GetContactList()
    {
        UserBM obj = new UserBM();
        obj.userIsActive = true;
        obj.userIsDeleted = false;
        obj.userType = 3;
        obj.userCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        // obj.GetAllEmployee();
        obj.userCreateBy = Convert.ToString(Session["OrgUserId"]);
        obj.GetAllEmployeecreatebymanager();
        DataSet ds = obj.ds;

        //Repeater1.DataSource = dt;
        // Repeater1.DataBind();

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                //    Repeateremp.DataSource = ds.Tables[0];
                //    Repeateremp.DataBind();

                // Repeateremp.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                //Repeateremp.DataSource = null;
                //Repeateremp.DataBind();
            }
        }
        else
        {
            //Repeateremp.DataSource = null;
            //Repeateremp.DataBind();
        }
    }
    protected void GetAllContact()
    {
        try
        {
            ContactBM obj = new ContactBM();
            obj.UserId = Convert.ToInt32(Session["OrgUserId"]);
            obj.ContactIsActive = true;
            obj.ContactIsDeleted = false;
            obj.GetAllContact();
            DataSet ds = obj.ds;

            DataView dv = new DataView();
            dv = ds.Tables[0].DefaultView;
            if (txtserch.Value != "")
            {
                //dv.RowFilter = ("userFirstName =  '" + Convert.ToString(txtserch.Value) + "'");
                dv.RowFilter = string.Concat("userFullname LIKE '%", Convert.ToString(txtserch.Value), "%'");
            }
            DataTable dtitm = dv.ToTable();
            DataSet ds1 = new DataSet();
            ds1.Tables.Add(dtitm);

            if (ds1 != null)
            {
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    Repeatercontct.DataSource = ds1.Tables[0];
                    Repeatercontct.DataBind();
                    //lblmailall.Text = Email + "mytest@gmail.comkamlesh@gmail.comchintan@gmail.comsdfn@gmail.com";
                    lblmailall.Text = Email;
                    // Repeateremp.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                else
                {
                    Repeatercontct.DataSource = null;
                    Repeatercontct.DataBind();
                }
            }
            else
            {
                Repeatercontct.DataSource = null;
                Repeatercontct.DataBind();
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void GetAllJobtype()
    {
        JobTypeBM obj = new JobTypeBM();
        obj.jobIsActive = true;
        obj.jobIsDeleted = false;
        if (string.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("Login.aspx");
        }
        obj.jobCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllJobType();


        if (string.IsNullOrWhiteSpace(Convert.ToString(Session["userJobType"])))
        {



            DataSet ds = obj.ds;

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    if (Convert.ToString(Session["Culture"]) == "Danish")
                    {
                        ds.Tables[0].Columns["jobName"].ColumnName = "abcd";
                        ds.Tables[0].Columns["jobNameDN"].ColumnName = "jobName";
                    }

                    ddljobtype.Items.Clear();

                    ddljobtype.DataSource = ds.Tables[0];
                    ddljobtype.DataTextField = "jobName";
                    ddljobtype.DataValueField = "jobId";
                    ddljobtype.DataBind();

                    ddljobtype.Items.Insert(0, new ListItem(hdnSelectJob.Value, CommonModule.dropDownZeroValue));
                    ddljobtype.Items.Insert(1, new ListItem("None", "-1"));
                }
                else
                {
                    ddljobtype.Items.Clear();
                    ddljobtype.Items.Insert(0, new ListItem(hdnSelectJob.Value, CommonModule.dropDownZeroValue));
                    ddljobtype.Items.Insert(1, new ListItem("None", "-1"));
                }
            }
            else
            {
                ddljobtype.Items.Clear();
                ddljobtype.Items.Insert(0, new ListItem(hdnSelectJob.Value, CommonModule.dropDownZeroValue));
                ddljobtype.Items.Insert(1, new ListItem("None", "-1"));
            }

        }
        else
        {
            var filter = "jobId=" + Convert.ToString(Session["userJobType"]);

            DataView dv = GetView(obj.ds, filter, "");
            if (dv == null) return;
            var data = dv.ToTable();

            DataSet ds = obj.ds;

            if (data != null)
            {
                if (data.Rows.Count > 0)
                {
                    if (Convert.ToString(Session["Culture"]) == "Danish")
                    {
                        ds.Tables[0].Columns["jobName"].ColumnName = "abcd";
                        ds.Tables[0].Columns["jobNameDN"].ColumnName = "jobName";
                    }

                    ddljobtype.Items.Clear();

                    ddljobtype.DataSource = ds.Tables[0];
                    ddljobtype.DataTextField = "jobName";
                    ddljobtype.DataValueField = "jobId";
                    ddljobtype.DataBind();

                    ddljobtype.Items.Insert(0, new ListItem(hdnSelectJob.Value, CommonModule.dropDownZeroValue));
                    //ddljobtype.Items.Insert(1, new ListItem("None", "-1"));
                }
                else
                {
                    ddljobtype.Items.Clear();
                    ddljobtype.Items.Insert(0, new ListItem(hdnSelectJob.Value, CommonModule.dropDownZeroValue));
                    //ddljobtype.Items.Insert(1, new ListItem("None", "-1"));
                }
            }
            else
            {
                ddljobtype.Items.Clear();
                ddljobtype.Items.Insert(0, new ListItem(hdnSelectJob.Value, CommonModule.dropDownZeroValue));
            }

        }

    }
    protected void GetAllDepartments()
    {
        //DepartmentsBM obj = new DepartmentsBM();
        //obj.depIsActive = true;
        //obj.depIsDeleted = false;
        //obj.depCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        //obj.GetAllDepartments();
        //DataSet ds = obj.ds;
        try
        {


            DivisionBM obj = new DivisionBM();
            obj.depIsActive = true;
            obj.depIsDeleted = false;
            obj.depCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            obj.GetAllDivision();
            DataSet ds = obj.ds;

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    if (Convert.ToString(Session["Culture"]) == "Danish")
                    {
                        ds.Tables[0].Columns["divName"].ColumnName = "abcd";
                        ds.Tables[0].Columns["divNameDN"].ColumnName = "divName";
                    }

                    ddlDepartment.Items.Clear();

                    ddlDepartment.DataSource = ds.Tables[0];
                    ddlDepartment.DataTextField = "divName";
                    ddlDepartment.DataValueField = "divId";
                    ddlDepartment.DataBind();
                    ddlDepartment.Items.Insert(0, new ListItem(hdnSelectDivisions.Value, CommonModule.dropDownZeroValue));
                    //ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownSelectDivisions, CommonModule.dropDownZeroValue));
                    //ddlDepartment.Items.Insert(1, new ListItem("None", "-1"));
                }
                else
                {
                    ddlDepartment.Items.Clear();
                    ddlDepartment.Items.Insert(0, new ListItem(hdnSelectDivisions.Value, CommonModule.dropDownZeroValue));
                    // ddlDepartment.Items.Insert(1, new ListItem("None", "-1"));
                }
            }
            else
            {
                ddlDepartment.Items.Clear();
                ddlDepartment.Items.Insert(0, new ListItem(hdnSelectDivisions.Value, CommonModule.dropDownZeroValue));
                //ddlDepartment.Items.Insert(1, new ListItem("None", "-1"));
            }
        }
        catch (Exception ex)
        {

            //ExceptionLogger.LogException(ex,  Convert.ToInt32(Session["OrgUserId"]),lblMsg.Text);
        }

    }
    //protected void Insertuser()
    //{
    //    string filePath = "";
    //    UserBM obj2 = new UserBM();
    //    obj2.useremailCheckDuplication(txtEmail.Text, -1);
    //    String returnMsg = obj2.ReturnString;
    //    if (returnMsg == "")
    //    {
    //        UserBM obj = new UserBM();
    //        obj.userFirstName = txtName.Text;
    //        obj.userLastName = "";
    //        obj.userZip = "";
    //        obj.userAddress = txtaddress.Text;
    //        obj.userCountryId = Convert.ToInt32(0);
    //        obj.userStateId = Convert.ToInt32(0);
    //        //obj.userCityId = Convert.ToInt32(0);
    //        obj.userCityId = "";
    //        obj.usercontact = txtMobile.Text;
    //        obj.userEmail = txtEmail.Text;
    //        obj.userType = 3;
    //        obj.userCreatedDate = DateTime.Now;
    //        obj.userIsActive = true;
    //        obj.userIsDeleted = false;
    //        obj.userCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
    //        string pass = CommonModule.Generate_Random_password();
    //        obj.userPassword = CommonModule.encrypt(pass.Trim());
    //        obj.userGender = "";
    //        obj.userImage = "ofile_img.png";
    //        obj.userLevel = 1;
    //        obj.userdepId = Convert.ToString(ddlDepartment.SelectedValue);
    //        obj.userJobType = Convert.ToInt32(ddljobtype.SelectedValue);
    //        obj.userCreateBy = Convert.ToString(Session["OrgUserId"]);
    //        // Get Cattegory list comma sap list. |Saurin | 20141216 |
    //        var category = string.Empty;
    //        var aa = new List<string>();
    //        foreach (RepeaterItem aItem in repCompCat.Items)
    //        {
    //            var repCompAdd = (Repeater)aItem.FindControl("repCompAdd");
    //            foreach (RepeaterItem bItem in repCompAdd.Items)
    //            {
    //                CheckBoxList chkDisplayTitle = (CheckBoxList)bItem.FindControl("chkList");
    //                Label hdnChkboxValue = (Label)bItem.FindControl("hdnChkboxValue");

    //                foreach (ListItem item in chkDisplayTitle.Items)
    //                {
    //                    if (item.Selected)
    //                    {
    //                        aa.Add(item.Value);
    //                        //string selectedValue = item.Value;
    //                    }
    //                }

    //            }



    //        }
    //        category = string.Join(",", aa.ToArray());
    //        obj.UserCategory = category;

    //        obj.InsertOrganisation();
    //        if (obj.ReturnBoolean == true)
    //        {
    //            sendmail(txtEmail.Text, txtName.Text, pass);
    //            clear();
    //            GetAllEmployeeList();
    //            Response.Redirect("Manager-dashboard.aspx?user=saveSuccess");
    //        }
    //    }
    //    else
    //    {
    //        if (returnMsg == "userEmail")
    //            lblMsg.Text = CommonModule.msgEmailAlreadyExists;
    //        else
    //            lblMsg.Text = CommonModule.msgProblemInsertRecord;
    //    }
    //}

    protected void sendmail(string email, string name, string pass, int userId)
    {
        try
        {
            // Common.WriteLog("start");
            //string subject = "Registration";
            string fromid = Convert.ToString(ConfigurationManager.AppSettings["fromid"]);

            //String confirmMail =
            //    CommonModule.getTemplatebyname("Registration", userId);
            Template template = CommonModule.getTemplatebyname1("Registration", userId);
            String confirmMail = template.TemplateName;
            if (!String.IsNullOrEmpty(confirmMail))
            {

                string subject = HttpContext.GetLocalResourceObject("~/Organisation/Organisation-dashboard.aspx", "RegistrationSubjectEng.Text").ToString();
                if (template.Language == "Danish")
                {
                    subject = HttpContext.GetLocalResourceObject("~/Organisation/Organisation-dashboard.aspx", "RegistrationSubjectDn.Text").ToString();
                }
                string tempString = confirmMail;
                tempString = tempString.Replace("###name###", name);
                tempString = tempString.Replace("###email###", email);
                tempString = tempString.Replace("###password###", pass);
                //SendMail(tempString, txtEmail.Text, txtFname.Text);
                CommonModule.SendMailToUser(email, subject, tempString, fromid, Convert.ToString(HttpContext.Current.Session["OrgCompanyId"]));
            }
        }
        catch (Exception ex)
        {
            Common.WriteLog("error in sendmail" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
        }


    }
    protected void clear()
    {
        txtName.Text = "";
        txtaddress.Text = "";
        txtMobile.Text = "";
        txtEmail.Text = "";
        ddlDepartment.SelectedValue = "0";
        ddljobtype.SelectedValue = "0";
    }
    private DataTable GetAllCompetenceAddbyComCatId(int categoryId)
    {
        CompetenceMasterBM obj = new CompetenceMasterBM();
        obj.catIsActive = true;
        obj.catIsDeleted = false;
        if (categoryId < 0) return null;
        obj.comDivId = Convert.ToInt32(ddlDepartment.SelectedValue);
        obj.comJobtype = Convert.ToInt32(ddljobtype.SelectedValue);
        obj.comCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllCompetenceAddbyComCatId(categoryId);
        // lblDivIdName.Text = ddlDepartment.SelectedItem.Text;

        DataSet ds = obj.ds;

        if (ds != null)
        {
            int selectTable = 0;

            if (ds.Tables[selectTable].Rows.Count > 0)
            {
                return ds.Tables[selectTable];

            }
            else
            {
                //chkList.Items.Clear();
                //ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownCategory, CommonModule.dropDownZeroValue));
            }

        }
        return null;
    }
    protected void delete(Int32 id)
    {
        TO_DO_LISTBM obj = new TO_DO_LISTBM();
        obj.listId = Convert.ToInt32(id);
        obj.listIsActive = false;
        obj.listIsDeleted = false;
        obj.GetTO_DO_LISTUpdatestatus();

        DataSet ds = obj.ds;
        if (ds != null)
        {
            string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
            if (returnMsg == "success")
            {
                //GetTO_DO_LISTbyid();
                get_ds_to_do_list();

                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('" + CommonModule.msgRecordDeletedSuccss + "');</script>", false);
            }
            else
            {
                //lblMsg.Text = CommonModule.msgSomeProblemOccure;
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('" + CommonModule.msgSomeProblemOccure + "');</script>", false);
            }
        }
        else
        {
            //lblMsg.Text = CommonModule.msgSomeProblemOccure;
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('" + CommonModule.msgSomeProblemOccure + "');</script>", false);
        }
    }
    #endregion
    #region repeater
    protected void Repeater1_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DataRowView drv = (DataRowView)e.Item.DataItem;
            int categoryID = Convert.ToInt32(drv["catId"]);
            Label compCategoryName = (Label)e.Item.FindControl("lblCompCategoryName");
            var categoryName = Convert.ToString(drv["catName"]);
            var categoryNameDN = Convert.ToString(drv["catNameDN"]);

            if (Convert.ToString(Session["Culture"]) == "Danish")
            {
                compCategoryName.Text = categoryNameDN;

            }
            else
            {
                compCategoryName.Text = categoryName;
            }
            Repeater Repeater2 = (Repeater)e.Item.FindControl("repCompAdd");

            Repeater2.DataSource = GetAllCompetenceAddbyComCatId(categoryID);
            Repeater2.DataBind();



            foreach (RepeaterItem bItem in Repeater2.Items)
            {
                CheckBoxList chk = (CheckBoxList)bItem.FindControl("chkList");


                var data = GetAllCompetenceAddbyComCatId(categoryID);
                if (data == null) return;
                DataSet ds = data.DataSet;
                var catIds = string.Empty;

                for (int intCount = 0; intCount < ds.Tables[0].Rows.Count; intCount++)
                {

                    if (Convert.ToString(Session["Culture"]) == "Danish")
                    {
                        ds.Tables[0].Rows[intCount][3] += "  (" + categoryNameDN + ")";
                    }
                    else
                    {
                        ds.Tables[0].Rows[intCount][1] += "  (" + categoryName + ")";
                    }

                }

                ds.Tables[0].AcceptChanges();


                /////////////////
                if (Convert.ToInt32(ds.Tables[0].Rows[0]["catId"]) == categoryID)
                {
                    chk.DataSource = data;
                    if (Convert.ToString(Session["Culture"]) == "Danish")
                    {
                        chk.DataTextField = "catNameDN";
                    }
                    else
                    {
                        chk.DataTextField = "catName";
                    }
                    // chk.DataTextField = "catName";
                    chk.DataValueField = "comId";

                    chk.DataBind();
                    foreach (ListItem li in chk.Items)
                    {
                        if (string.IsNullOrWhiteSpace(catIds))
                        {
                          //  li.Selected = true;
                        }

                        else
                        {
                            foreach (var j in catIds.Split(','))
                            {

                                if (li.Value == j)
                                {
                                    li.Selected = true;
                                    break;
                                }
                                else
                                {
                                    li.Selected = false;
                                }
                            }

                        }




                    }

                    break;

                }
            }
        }
    }

    protected void Repeater2_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //{
        //    DataRowView drv = (DataRowView)e.Item.DataItem;
        //    int categoryID = Convert.ToInt32(drv["catId"]);
        //    CheckBoxList chk = (CheckBoxList)e.Item.FindControl("chkList");
        //    var data = GetAllCompetenceAddbyComCatId(categoryID);
        //    if (data == null) return;
        //    DataSet ds = data.DataSet;
        //    if (Convert.ToInt32(ds.Tables[0].Rows[0]["catId"]) == categoryID)
        //    {
        //        chk.DataSource = data;
        //        chk.DataTextField = "catName";
        //        chk.DataValueField = "catId";
        //        chk.DataBind();
        //    }
        //}
    }

    protected void Repeatercontct_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField userEmail = e.Item.FindControl("userEmail") as HiddenField;

                // userEmailaLL
                if (String.IsNullOrEmpty(Email))
                {
                    Email = userEmail.Value;
                }
                else
                {
                    Email = Email + "," + userEmail.Value;
                }
            }

        }
        catch (Exception ex)
        {
            Common.WriteLog("error in Repeatercontct_ItemDataBound" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }


    #endregion
    #region Culture
    protected override void InitializeCulture()
    {
        //string language = "French";
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Danish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion

    #region WebMethod
    [WebMethod(EnableSession = true)]
    public static string Sendmail(string txtsubject, string txtmessge, string to)
    {
        string msg = string.Empty;
        #region Send mail
        String subject = txtsubject;
        String confirmMail = CommonModule.getTemplatebyname(Convert.ToString(HttpContext.Current.Session["Inviteformail"]), 0);
        string fromid = Convert.ToString(ConfigurationManager.AppSettings["fromid"]);
        string tempString = txtmessge;
        CommonModule.SendMailToUser(to, subject, tempString, fromid, Convert.ToString(HttpContext.Current.Session["OrgCompanyId"]));
        #endregion
        msg = "true";
        return msg;
    }

    protected void Updatelanguagemaster(int OrgUserId, string Language)
    {
        try
        {


            UserBM obj = new UserBM();
            obj.languageUserId = Convert.ToInt32(OrgUserId);
            String lag = Convert.ToString(Language);
            obj.languageName = Language;// ddlLanguage.SelectedItem.Text;
            obj.languageCulture = Language;//ddlLanguage.SelectedItem.Value;
            if (lag == "English")
            {
                obj.languageName = "English";
                obj.languageCulture = "en-GB";


            }
            //else
            //{
            //    obj.languageName = "Danish";
            //    obj.languageCulture = "da-DK";


            //}
            //obj.userGender = ddlgender.SelectedValue;
            obj.Updatelanguagemaster();
        }
        catch (Exception)
        {


        }

    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string AddNewEmp(string email, string address, string mobile, string userdepId, string jobtype, string selectedValues, string Name, string lastName, string Language, string gender, int TeamID)
    {
        UserBM obj2 = new UserBM();
        obj2.useremailCheckDuplication(email, -1);
        String returnMsg = obj2.ReturnString;
        if (returnMsg == "")
        {
            UserBM obj = new UserBM();
            obj.userFirstName = Name;
            obj.userLastName = lastName;
            obj.userZip = "";
            obj.userAddress = address;
            obj.userCountryId = Convert.ToInt32(0);
            obj.userStateId = Convert.ToInt32(0);
            obj.userCityId = "";
            obj.usercontact = mobile;
            obj.userEmail = email.Trim();
            obj.userType = 3;
            obj.userCreatedDate = DateTime.Now;
            obj.userIsActive = true;
            obj.userIsDeleted = false;
            obj.userCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
            string pass = CommonModule.Generate_Random_password();
            obj.userPassword = CommonModule.encrypt(pass.Trim());
            obj.userGender = gender;
            obj.userImage = "ofile_img.png";
            obj.userLevel = 1;
            obj.userdepId = userdepId;
            obj.userJobType = Convert.ToInt32(jobtype);
            obj.userCreateBy = Convert.ToString(HttpContext.Current.Session["OrgUserId"]);
            // Get Cattegory list comma sap list. |Saurin | 20141216 |
            obj.UserCategory = selectedValues;
            obj.TeamID = TeamID;
            obj.InsertOrganisation();
            int UserId = 0;
            DataSet ds = obj.ds;
            if (ds != null)
            {
                if (ds.Tables.Count > 0)
                {
                    UserId = Convert.ToInt32(ds.Tables[0].Rows[0]["userId"]);
                    Organisation_Manager_dashboard ob = new Organisation_Manager_dashboard();
                    ob.Updatelanguagemaster(UserId, Language);

                    if (!string.IsNullOrEmpty(selectedValues))
                    {
                        //string[] str = selectedValues.Split(',');
                        //if (str.Length > 0)
                        //{
                        //    try
                        //    {


                        //        foreach (var comId in str)
                        //        {
                        //            EmployeeSkillBM skillobj = new EmployeeSkillBM();
                        //            skillobj.skillComId = Convert.ToInt32(comId);
                        //            skillobj.skillStatus = 3;
                        //            skillobj.skillUserId = Convert.ToInt32(ds.Tables[0].Rows[0]["userId"]);
                        //            skillobj.skillIsActive = true;
                        //            skillobj.skillIsDeleted = false;
                        //            skillobj.skillCreatedDate = DateTime.Now;
                        //            skillobj.skillCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
                        //            skillobj.skillLocal = 0;
                        //            skillobj.skillAchive = 0;
                        //            skillobj.skilltarget = 0;
                        //            skillobj.skillComment = string.Empty;
                        //            skillobj.InsertSkillMaster();
                        //        }
                        //    }
                        //    catch (Exception)
                        //    {


                        //    }


                        //}


                        var db = new startetkuEntities1();
                        db.InsertIntoSkillMaster(3, Convert.ToInt32(ds.Tables[0].Rows[0]["userId"]), true, false, DateTime.Now, Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]), 0, 0, 0, string.Empty, true, selectedValues);



                    }
                }
            }

            if (obj.ReturnBoolean == true)
            {
                Organisation_Manager_dashboard x = new Organisation_Manager_dashboard();
                x.sendmail(email.Trim(), Name, pass, UserId);
            }
        }
        else
        {
            if (returnMsg == "userEmail")
            {
                return CommonModule.msgEmailAlreadyExists; ;
            }
            return CommonModule.msgProblemInsertRecord;
        }

        //var category = string.Empty;
        //var aa = new List<string>();
        //foreach (RepeaterItem aItem in repCompCat.Items)
        //{
        //    var repCompAdd = (Repeater)aItem.FindControl("repCompAdd");
        //    foreach (RepeaterItem bItem in repCompAdd.Items)
        //    {
        //        CheckBoxList chkDisplayTitle = (CheckBoxList)bItem.FindControl("chkList");
        //        Label hdnChkboxValue = (Label)bItem.FindControl("hdnChkboxValue");

        //        foreach (ListItem item in chkDisplayTitle.Items)
        //        {
        //            if (item.Selected)
        //            {
        //                aa.Add(item.Value);
        //                //string selectedValue = item.Value;
        //            }
        //        }

        //    }

        //}
        //category = string.Join(",", aa.ToArray());


        return "success";

    }





    #endregion
}
