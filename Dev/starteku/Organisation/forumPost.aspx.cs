﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using starteku_BusinessLogic.View;
using System.Threading;
using System.Globalization;

public partial class Organisation_forumPost : System.Web.UI.Page
{
    #region define object
    private ForumMasterBM obj = new ForumMasterBM();
    #endregion
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            Conatct.InnerHtml = GetLocalResourceObject("Welcome.Text").ToString() + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            ViewState["catParentId"] = "0";
            GetForumTagsByforumCompanyId();
            GetAllForumCategoryByCompanyId();
            GetForumMasterById();
            GetForumReplyByForumMasterId();
        }

    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }


    private void GetForumReplyByForumMasterId()
    {
            obj.fmCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            obj.fmIsActive = true;
            obj.fmIsDeleted = false;
            obj.forumMasterId = Convert.ToInt32(Request.QueryString["fmid"]);
            obj.GetForumReplyByForumMasterId();
            
            DataSet ds = obj.ds;

            foreach (DataTable table in ds.Tables)
            {
                foreach (DataRow dr in table.Rows)
                {
                    dr["MinAgo"] = dr["MinAgo"].ToString().Replace("Second", GetLocalResourceObject("Second.Text").ToString());
                    dr["MinAgo"] = dr["MinAgo"].ToString().Replace("Minutes", GetLocalResourceObject("Minutes.Text").ToString());
                    dr["MinAgo"] = dr["MinAgo"].ToString().Replace("Hours", GetLocalResourceObject("Hours.Text").ToString());
                    dr["MinAgo"] = dr["MinAgo"].ToString().Replace("Days", GetLocalResourceObject("Days.Text").ToString());
                    
                }
            }

        if (ds.Tables[0].Rows.Count > 0)
        {
            rptReply.DataSource = ds;
            rptReply.DataBind();
           
            lblRecentReply.Text = lblRecentReply.Text+ @" :";
        }
        else
        {
            lblRecentReply.Text =  lblRecentReply.Text+ @"  0";
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<JsonView> GetForumTagsByforumCompanyId()
    {
        try
        {
            var obj1 = new ForumMasterBM();
            obj1.forumCatCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
            obj1.forumCatIsActive = true;
            obj1.forumCatIsDeleted = false;
            obj1.GetForumTagsByforumCompanyId();
            DataSet ds = obj1.ds;

            List<JsonView> tag = new List<JsonView>();
            if (ds.Tables[0].Rows.Count > 0)
            {
                var myTaglist = ds.Tables[0].AsEnumerable()
                          .Select(r => new UserMasterView
                          {
                              UserId = r.Field<int>("tagId"),
                              UserCategory = r.Field<string>("tagName")
                          });


                foreach (var i in myTaglist)
                {
                    tag.Add(GetViewJsonData(i));
                }
                return tag.ToList();

            }
            return null;
        }
        catch (Exception ex)
        {

            return null;

        }
        finally
        {

        }
    }

    private static JsonView GetViewJsonData(UserMasterView userMasterView)
    {
        var view = new JsonView();
        view.id = userMasterView.UserId;
        view.label = userMasterView.UserCategory;
        view.value = userMasterView.UserCategory;
        return view;
    }

   
    #endregion

    #region method
    private void GetAllForumCategoryByCompanyId()
    {
        try
        {
            return;
            obj.forumCatCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            obj.forumCatIsActive = true;
            obj.forumCatIsDeleted = false;
            obj.GetForumCategoryByforumCompanyId();
            DataSet ds = obj.ds;

            if (ds.Tables[0].Rows.Count > 0)
            {
               /* ddForumCat.Items.Clear();
                ddForumCat.DataSource = ds.Tables[0];
                ddForumCat.DataTextField = "forumCatName";
                ddForumCat.DataValueField = "forumCatId";
                ddForumCat.DataBind();
                ddForumCat.Items.Insert(0, new ListItem(CommonModule.dropDownSelectForumCategory, CommonModule.dropDownZeroValue));*/

            }
        }
        catch (Exception ex)
        {


        }
        finally
        {

        }
    }
    private void GetForumMasterById()
    {
        try
        {
            obj.fmCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            obj.fmIsActive = true;
            obj.fmIsDeleted = false;
            obj.forumMasterId = Convert.ToInt32(Request.QueryString["fmid"]);
            obj.GetForumMasterById();
            DataSet ds = obj.ds;
            if (ds.Tables[0].Rows.Count > 0)
            {
                txtName.Text = Convert.ToString(ds.Tables[0].Rows[0]["fmQuestionName"]);
                txtDescription.Text = Convert.ToString(ds.Tables[0].Rows[0]["fmDescription"]);
                tag1.Text = Convert.ToString(ds.Tables[0].Rows[0]["fmtagNames"]);
                lblForumCategoryId.Text = Convert.ToString(ds.Tables[0].Rows[0]["fmCategoryId"]);
                lblForumCategory.Text = Convert.ToString(ds.Tables[0].Rows[0]["fmCategoryName"]);
                lblTags.Text = Convert.ToString(ds.Tables[0].Rows[0]["fmTagNames"]);

            }
        }
        catch (Exception ex)
        {

        }
        finally
        {

        }
    }



    #endregion

    #region Button Event
    //protected void btnsubmit_click(object sender, EventArgs e)
    //{
    //    if (string.IsNullOrWhiteSpace(txtReply.Text))
    //    {
    //        Label1.Text = @"Please enter reply.";
    //    }
    //    Label1.Text = string.Empty;
    //    obj.fmCategoryId = Convert.ToInt32(lblForumCategoryId.Text);
    //    obj.fmCategoryName = lblForumCategory.Text;
    //    obj.fmCreatedDate = DateTime.Now;
    //    obj.fmIsActive = true;
    //    obj.fmIsDeleted = false;
    //    obj.fmQuestionName = txtName.Text;
    //    obj.fmDescription = txtReply.Text;
    //    obj.fmCreateByUserID = Convert.ToInt32(Session["OrgUserId"]);
    //    obj.fmCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
    //    obj.fmTagNames = tag1.Text;
    //    obj.forumMasterId = Convert.ToInt32(Request.QueryString["fmid"]);


    //    obj.InsertForumReply();

    //    txtReply.Text = string.Empty;
    //    GetForumReplyByForumMasterId();



    //}
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrWhiteSpace(txtReply.Text))
        {
            Label1.Text = @"Please enter reply.";
        }
        Label1.Text = string.Empty;
        obj.fmCategoryId = Convert.ToInt32(lblForumCategoryId.Text);
        obj.fmCategoryName = lblForumCategory.Text;
        obj.fmCreatedDate = DateTime.Now;
        obj.fmIsActive = true;
        obj.fmIsDeleted = false;
        obj.fmQuestionName = txtName.Text;
        obj.fmDescription = txtReply.Text;
        obj.fmCreateByUserID = Convert.ToInt32(Session["OrgUserId"]);
        obj.fmCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.fmTagNames = tag1.Text;
        obj.forumMasterId = Convert.ToInt32(Request.QueryString["fmid"]);


        obj.InsertForumReply();

        txtReply.Text = string.Empty;
        GetForumReplyByForumMasterId();


    }

    protected void btnCancel_click(object sender, EventArgs e)
    {

    }

    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Denish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion
}