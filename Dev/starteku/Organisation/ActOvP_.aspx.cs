﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using starteku_BusinessLogic.Model;

public partial class Organisation_ActOvP : System.Web.UI.Page
{
    public int ResLangId = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        ResLangId = Convert.ToInt32(Session["resLangID"]);
        if (string.IsNullOrWhiteSpace(Convert.ToString(Session["Aemcompid"])))
        {

            Response.Redirect("login.aspx");
        }

        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            GetAllActivityCategory();
            GetAllJobTypeByCompanyId();
            GetAllDivisionByCompanyId();
            GetAllTeamCompanyId();

            ActivityOverview();

        }
        Competence.InnerHtml = "  " + Convert.ToString(Session["OrgUserName"]) + "!";

    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
    }

    #region TAB1
    protected void ActivityOverview()
    {
        try
        {
            var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
            var userId = Convert.ToInt32(Session["OrgUserId"]);
            var actObj = new GetActivityLists_Result();
            actObj.ActCatRefId = 0;
            try
            {
                actObj.ActCatRefId = Convert.ToInt32(ddActCate.SelectedItem.Value);
                actObj.ActJobId = Convert.ToInt32(ddlJobType.SelectedItem.Value);
                actObj.ActDivId = Convert.ToInt32(ddlDepartment.SelectedItem.Value);

                actObj.userTeamID = Convert.ToInt32(ddlTeam.SelectedItem.Value);

                if (!string.IsNullOrWhiteSpace(txtOvpStartDate.Text))
                {
                    try
                    {
                        actObj.ActStartDate = Convert.ToDateTime(txtOvpStartDate.Text);
                    }
                    catch (Exception)
                    {
                    }
                }
                if (!string.IsNullOrWhiteSpace(txtOvpEndDate.Text))
                {
                    try
                    {
                        actObj.ActEndDate = Convert.ToDateTime(txtOvpEndDate.Text);
                    }
                    catch (Exception)
                    {
                    }
                }

            }
            catch (Exception)
            {
            }

            actObj.ActCompanyId = companyId;
            actObj.ActCreatedBy = userId;
            //actObj.ActReqUserId = userId;
            var actCatList = ActivityOverviewLogic.GetActivityOverviewList(actObj);


            gvActCat.DataSource = actCatList;
            gvActCat.DataBind();

        }
        catch (Exception ex)
        {

            ExceptionLogger.LogException(ex);
        }

    }
    protected void ActivityOverviewCount()
    {
        try
        {
            var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
            var userId = Convert.ToInt32(Session["OrgUserId"]);
            var actObj = new GetActivityLists_Result();
            actObj.ActCatRefId = 0;

            try
            {
                actObj.ActCatRefId = Convert.ToInt32(ddActCate.SelectedItem.Value);
                actObj.ActJobId = Convert.ToInt32(ddlJobType.SelectedItem.Value);
                actObj.ActDivId = Convert.ToInt32(ddlDepartment.SelectedItem.Value);
                actObj.userTeamID = Convert.ToInt32(ddlTeam.SelectedItem.Value);
            }
            catch (Exception)
            {
            }

            actObj.ActCompanyId = companyId;
            actObj.ActCreatedBy = userId;
            // actObj.ActReqUserId = userId;
            var actCatList = ActivityOverviewLogic.GetActivityOverviewList_Total(actObj);

            gvActCat.DataSource = actCatList;
            gvActCat.DataBind();

        }
        catch (Exception ex)
        {

            ExceptionLogger.LogException(ex);
        }

    }


    protected int loclCompletedCount = 0;
    protected int loclcount = 0;
    protected int loclRequestCount = 0;
    protected int loclApprovedCount = 0;
    protected int loclOngoingCount = 0;
    protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
    {

        try
        {


            var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
            var userId = Convert.ToInt32(Session["OrgUserId"]);

            try
            {

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string hdnActId = ((HiddenField)e.Row.FindControl("hdnActId")).Value;


                    string id = gvActCat.DataKeys[e.Row.RowIndex].Value.ToString();

                    var actObj = new GetActivityLists_Result();
                    actObj.ActCompanyId = companyId;
                    actObj.ActReqUserId = userId;
                    actObj.ActId = Convert.ToInt32(hdnActId);
                    actObj.ActJobId = Convert.ToInt32(ddlJobType.SelectedItem.Value);
                    actObj.ActDivId = Convert.ToInt32(ddlDepartment.SelectedItem.Value);
                    actObj.userTeamID = Convert.ToInt32(ddlTeam.SelectedItem.Value);
                    if (!string.IsNullOrWhiteSpace(txtOvpStartDate.Text))
                    {
                        try
                        {
                            actObj.ActStartDate = Convert.ToDateTime(txtOvpStartDate.Text);
                        }
                        catch (Exception)
                        {
                        }

                    }
                    if (!string.IsNullOrWhiteSpace(txtOvpEndDate.Text))
                    {
                        try
                        {
                            actObj.ActEndDate = Convert.ToDateTime(txtOvpEndDate.Text);
                        }
                        catch (Exception)
                        {
                        }

                    }

                    var actCatList = new List<Overview_GetPersonStatus_Result>();
                    if (dd_Cout_Count_Tab1.SelectedItem.Value == "1")
                    {
                        actCatList = ActivityOverviewLogic.GetPersonOverviewList(actObj, 0);
                    }
                    else
                    {
                        actCatList = ActivityOverviewLogic.GetPersonOverviewList(actObj, 1);
                    }
                    if (!actCatList.Any())
                    {
                        actCatList = new List<Overview_GetPersonStatus_Result>();

                    }

                    GridView gvPer = e.Row.FindControl("gvPer") as GridView;
                    if (gvPer != null)
                    {
                        gvPer.DataSource = actCatList;
                        gvPer.DataBind();

                        //if (gvPer.HeaderRow != null) gvPer.HeaderRow.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex, 0, "DataControlRowType.DataRow 1");
            }
            //////////////////////////////////////////////////////////////////////////////////Grid=TOTAL CODE/////////////////////////////////////////////////////////////////////////////////   
            try
            {


                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //string sponsorBonus = ((Label)e.Row.FindControl("Label2")).Text;
                    string count = ((HiddenField)e.Row.FindControl("hdnTotal")).Value;
                    string CompletedCount = ((HiddenField)e.Row.FindControl("hdnCompletedCount")).Value;
                    string OngoingCount = ((HiddenField)e.Row.FindControl("hdnOngoingCount")).Value;
                    string RequestCount = ((HiddenField)e.Row.FindControl("hdnRequestCount")).Value;
                    string ApprovedCount = ((HiddenField)e.Row.FindControl("hdnApprovedCount")).Value;


                    if (!string.IsNullOrWhiteSpace(CompletedCount))
                    {
                        loclCompletedCount = loclCompletedCount + Int32.Parse(CompletedCount);
                    }

                    if (!string.IsNullOrWhiteSpace(count))
                    {
                        loclcount = loclcount + Int32.Parse(count);
                    }

                    if (!string.IsNullOrWhiteSpace(OngoingCount))
                    {
                        loclOngoingCount = loclOngoingCount + Int32.Parse(OngoingCount);
                    }

                    if (!string.IsNullOrWhiteSpace(RequestCount))
                    {
                        loclRequestCount = loclRequestCount + Int32.Parse(RequestCount);
                    }

                    if (!string.IsNullOrWhiteSpace(ApprovedCount))
                    {
                        loclApprovedCount = loclApprovedCount + Int32.Parse(ApprovedCount);
                    }

                    // total = total + Int32.Parse(OngoingCount);
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex, 0, "DataControlRowType.DataRow 1");
            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {

                Label lblTotalCount = (Label)e.Row.FindControl("lblTotalCount");
                lblTotalCount.Text = loclcount.ToString();

                Label lglhdnCompletedCount = (Label)e.Row.FindControl("lglhdnCompletedCount");
                lglhdnCompletedCount.Text = loclCompletedCount.ToString();

                Label lblhdnOngoingCount = (Label)e.Row.FindControl("lblhdnOngoingCount");
                lblhdnOngoingCount.Text = loclOngoingCount.ToString();

                Label lblRequested = (Label)e.Row.FindControl("lblRequested");
                lblRequested.Text = loclRequestCount.ToString();

                Label lblApproved = (Label)e.Row.FindControl("lblApproved");
                lblApproved.Text = loclApprovedCount.ToString();


                gvActCat.ShowFooter = true;

            }
            ///////////////////////////////////////////////////////////////Grid=TOTAL CODE ///////////////////////////////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////





        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "OnRowDataBound");
        }




    }

    #endregion

    protected void txtOvpStartDate_TextChanged(object sender, EventArgs e)
    {

        var a = txtOvpEndDate.Text;
        var a1 = txtOvpStartDate.Text;
        DateTime dtStartDate = Convert.ToDateTime(txtOvpStartDate.Text);
        var hdnVal = Hdntaginfo.Value;



        if (hdnVal == "2")
        {
            PersonOverviewTab2();
        }
        else
        {
            ActivityOverview();

        }

        ScriptManager.RegisterStartupScript(updtPnl, updtPnl.GetType(), "ActivateDateTime", " ActivateDateTime();TabVisiblity(" + hdnVal + ");", true);

    }
    protected void txtOvpEndDate_TextChanged(object sender, EventArgs e)
    {
        var a = txtOvpEndDate.Text;
        var a1 = txtOvpStartDate.Text;
    }

    #region TAB2
    protected void PersonOverviewTab2()
    {
        try
        {


            var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
            var userId = Convert.ToInt32(Session["OrgUserId"]);
            var actObj = new GetActivityLists_Result();


            actObj.ActCatRefId = 0;


            if (!string.IsNullOrWhiteSpace(txtOvpStartDate.Text))
            {
                try
                {
                    actObj.ActStartDate = Convert.ToDateTime(txtOvpStartDate.Text);
                }
                catch (Exception)
                {
                }

            }
            if (!string.IsNullOrWhiteSpace(txtOvpEndDate.Text))
            {
                try
                {
                    actObj.ActEndDate = Convert.ToDateTime(txtOvpEndDate.Text);
                }
                catch (Exception)
                {
                }

            }

            try
            {
                actObj.ActCatRefId = Convert.ToInt32(ddActCate.SelectedItem.Value);
                actObj.ActJobId = Convert.ToInt32(ddlJobType.SelectedItem.Value);
                actObj.ActDivId = Convert.ToInt32(ddlDepartment.SelectedItem.Value);
                actObj.userTeamID = Convert.ToInt32(ddlTeam.SelectedItem.Value);
            }
            catch (Exception)
            {


            }

            actObj.ActCompanyId = companyId;
            actObj.ActReqUserId = userId;
            var actCatList = ActivityOverviewLogic.GetPersonOverviewListTab2(actObj);


            gv_Person_Master.DataSource = actCatList;
            gv_Person_Master.DataBind();

        }
        catch (Exception ex)
        {

            ExceptionLogger.LogException(ex);
        }

    }

    protected void PersonOverviewTab2_Count()
    {
        try
        {


            var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
            var userId = Convert.ToInt32(Session["OrgUserId"]);
            var actObj = new GetActivityLists_Result();


            actObj.ActCatRefId = 0;

            try
            {
                actObj.ActCatRefId = Convert.ToInt32(ddActCate.SelectedItem.Value);

            }
            catch (Exception)
            {


            }
            try
            {
                actObj.ActCatRefId = Convert.ToInt32(ddActCate.SelectedItem.Value);
                actObj.ActJobId = Convert.ToInt32(ddlJobType.SelectedItem.Value);
                actObj.ActDivId = Convert.ToInt32(ddlDepartment.SelectedItem.Value);
                actObj.userTeamID = Convert.ToInt32(ddlTeam.SelectedItem.Value);
            }
            catch (Exception)
            {


            }
            actObj.ActCompanyId = companyId;
            actObj.ActReqUserId = userId;
            var actCatList = ActivityOverviewLogic.GetPersonOverviewListTab2_Total(actObj);


            gv_Person_Master.DataSource = actCatList;
            gv_Person_Master.DataBind();

        }
        catch (Exception ex)
        {

            ExceptionLogger.LogException(ex);
        }

    }

    protected int loclCompletedCountp = 0;
    protected int loclcountp = 0;
    protected int loclRequestCountp = 0;
    protected int loclApprovedCountp = 0;
    protected int loclOngoingCountp = 0;
    protected void gv_Person_Master_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(Session["OrgUserId"]);

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string hdnActId = ((HiddenField)e.Row.FindControl("hdnActId")).Value;


            string id = gv_Person_Master.DataKeys[e.Row.RowIndex].Value.ToString();

            var actObj = new GetActivityLists_Result();
            actObj.ActCompanyId = companyId;
            actObj.ActReqUserId = Convert.ToInt32(id);
            actObj.ActCreatedBy = userId;
            //actObj.ActReqActId = Convert.ToInt32(id);


            try
            {
                actObj.ActJobId = Convert.ToInt32(ddlJobType.SelectedItem.Value);
                actObj.ActDivId = Convert.ToInt32(ddlDepartment.SelectedItem.Value);
                actObj.userTeamID = Convert.ToInt32(ddlTeam.SelectedItem.Value);
            }
            catch (Exception ex)
            {
            }
            var actCatList = new List<Overview_GetActivityStatus_Result>();
            var actCatList_Total = new List<Overview_GetActivityStatus_Total_Result>();
            if (dd_Cout_Count_Tab2.SelectedItem.Value == "1")
            {
                actCatList = ActivityOverviewLogic.GetActivityOverviewList(actObj);
            }
            else
            {
                actCatList_Total = ActivityOverviewLogic.GetActivityOverviewList_Total(actObj);
            }
            if (!actCatList.Any())
            {
                actCatList = new List<Overview_GetActivityStatus_Result>();
            }
            if (!actCatList_Total.Any())
            {
                actCatList_Total = new List<Overview_GetActivityStatus_Total_Result>();
            }
            if (dd_Cout_Count_Tab2.SelectedItem.Value == "1")
            {
                GridView gv_Person_Child = e.Row.FindControl("gv_Person_Child") as GridView;
                if (gv_Person_Child != null)
                {
                    gv_Person_Child.DataSource = actCatList;
                    gv_Person_Child.DataBind();

                    //if (gvPer.HeaderRow != null) gvPer.HeaderRow.Visible = false;
                }
            }
            else
            {
                GridView gv_Person_Child = e.Row.FindControl("gv_Person_Child") as GridView;
                if (gv_Person_Child != null)
                {
                    gv_Person_Child.DataSource = actCatList_Total;
                    gv_Person_Child.DataBind();

                    //if (gvPer.HeaderRow != null) gvPer.HeaderRow.Visible = false;
                }
            }
        }

        /////////////////////////////////////////////////////////////////////Grid=TOTAL CODE///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //string sponsorBonus = ((Label)e.Row.FindControl("Label2")).Text;
            string countp = ((HiddenField)e.Row.FindControl("hdnTotalp")).Value;
            string CompletedCountp = ((HiddenField)e.Row.FindControl("hdnCompletedCountp")).Value;
            string OngoingCountp = ((HiddenField)e.Row.FindControl("hdnOngoingCountp")).Value;
            string RequestCountp = ((HiddenField)e.Row.FindControl("hdnRequestCountp")).Value;
            string ApprovedCountp = ((HiddenField)e.Row.FindControl("hdnApprovedCountp")).Value;


            if (!string.IsNullOrWhiteSpace(CompletedCountp))
            {
                loclCompletedCountp = loclCompletedCountp + Int32.Parse(CompletedCountp);
            }

            if (!string.IsNullOrWhiteSpace(countp))
            {
                loclcountp = loclcountp + Int32.Parse(countp);
            }

            if (!string.IsNullOrWhiteSpace(OngoingCountp))
            {
                loclOngoingCountp = loclOngoingCountp + Int32.Parse(OngoingCountp);
            }

            if (!string.IsNullOrWhiteSpace(RequestCountp))
            {
                loclRequestCountp = loclRequestCountp + Int32.Parse(RequestCountp);
            }

            if (!string.IsNullOrWhiteSpace(ApprovedCountp))
            {
                loclApprovedCountp = loclApprovedCountp + Int32.Parse(ApprovedCountp);
            }

            // total = total + Int32.Parse(OngoingCount);



        }

        if (e.Row.RowType == DataControlRowType.Footer)
        {

            Label lblTotalp = (Label)e.Row.FindControl("lblTotalp");
            lblTotalp.Text = loclcountp.ToString();

            Label lblCompletedCountp = (Label)e.Row.FindControl("lblCompletedCountp");
            lblCompletedCountp.Text = loclCompletedCountp.ToString();

            Label lblOngoingCountp = (Label)e.Row.FindControl("lblOngoingCountp");
            lblOngoingCountp.Text = loclOngoingCountp.ToString();

            Label lbRequestCountp = (Label)e.Row.FindControl("lbRequestCountp");
            lbRequestCountp.Text = loclRequestCountp.ToString();

            Label lblApprovedCountp = (Label)e.Row.FindControl("lblApprovedCountp");
            lblApprovedCountp.Text = loclApprovedCountp.ToString();


            gv_Person_Master.ShowFooter = true;

        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }
    #endregion


    #region Tab4

    protected void Tab4_OnClick(object sender, EventArgs e)
    {
        GetActivityList_Tab4();

        ScriptManager.RegisterStartupScript(updtPnl, updtPnl.GetType()
                                                   , "TabVisiblity", "TabVisiblity(4);", true);


    }
    public void GetActivityList_Tab4()
    {
        var db = new startetkuEntities1();
        try
        {
            var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
            var userId = Convert.ToInt32(Session["OrgUserId"]);

            var actObj = new GetActivityLists_Result();
            actObj.ActCompanyId = companyId;
            actObj.ActReqUserId = userId;

            try
            {
                actObj.ActJobId = Convert.ToInt32(ddlJobType.SelectedItem.Value);
                actObj.ActDivId = Convert.ToInt32(ddlDepartment.SelectedItem.Value);
                actObj.userTeamID = Convert.ToInt32(ddlTeam.SelectedItem.Value);
            }
            catch (Exception ex)
            {
            }

            var activityList = ActivityMasterLogic.GetActivityList_Tab4(actObj);

            //Get user type

            var logginUserType = Convert.ToInt32(Session["OrguserType"]);
            if (logginUserType == 3)
            {

                var emplsManagerId = Convert.ToInt32(Session["OrgCreatedBy"]);//Mang Id

                activityList = activityList.Where(o => o.ActCreatedBy == emplsManagerId || o.ActCreatedBy == userId || o.ActCreatedBy == companyId).ToList();


            }
            if (logginUserType == 2)
            {

                var managerCompId = Convert.ToInt32(Session["OrgCreatedBy"]); //compyId

                activityList = activityList.Where(o => o.ActCreatedBy == managerCompId || o.ActCreatedBy == userId || o.ActCreatedBy == companyId).ToList();


            }
            if (activityList.Any())
            {
                gvGrid_ActivityStatus_Tab4.DataSource = activityList;
                gvGrid_ActivityStatus_Tab4.DataBind();

                if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["OrguserType"])))
                {
                    var type = Convert.ToInt32(Session["OrguserType"]);

                    //if (type == 1)
                    //{
                    //    gvGrid_ActivityStatus_Tab4.Columns[10].Visible = false;
                    //}
                }
            }
            else
            {
                gvGrid_ActivityStatus_Tab4.DataSource = null;
                gvGrid_ActivityStatus_Tab4.DataBind();
            }

        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex);
        }
        finally
        {
            CommonAttributes.DisposeDBObject(ref db);
        }


    }
    protected void grd_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }

    protected void gvGrid_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            foreach (TableCell cell in e.Row.Cells)
            {
                cell.BorderStyle = BorderStyle.None;
            }
        }
    }

    public int total;
    protected void gvGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            //string sponsorBonus = ((Label)e.Row.FindControl("Label2")).Text;

            //string count = ((HiddenField)e.Row.FindControl("hdnCost")).Value;
            string actReqStatus = ((HiddenField)e.Row.FindControl("hdnActReqStatus")).Value;
            Label lbl = (Label)e.Row.FindControl("lblActReqStatus");
            //lbl.Text = GetActivityStatusByStatusId(actReqStatus,false);



            /*Priority*/
            string hdnActPriority = ((HiddenField)e.Row.FindControl("hdnActPriority")).Value;
            Label lblActPriority = (Label)e.Row.FindControl("lblActPriority");


            lblActPriority.Text = ActivityRequestLogic.GetActivityPriorityStatusByNumber(hdnActPriority);

            try
            {
                lbl.Text = GetActivityStatusByStatusId(actReqStatus);

            }
            catch (Exception ex)
            {

            }
            //if (!string.IsNullOrWhiteSpace(count))
            //{
            //    if (actReqStatus == "2" || actReqStatus == "3" || actReqStatus == "5")
            //    {
            //        total = total + Int32.Parse(count);
            //    }

            //}

        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {

            try
            {
                var type = Convert.ToInt32(Session["OrguserType"]);
                Label lblTotalCost = (Label)e.Row.FindControl("lblTotalCost");
                lblTotalCost.Text = total.ToString();
                if (type != 1)
                {
                    gvGrid_ActivityStatus_Tab4.ShowFooter = true;
                }
            }
            catch (Exception)
            {


            }

        }


    }
    public string GetActivityStatusByStatusId(string actReqStatus)
    {
        /*
     * About Activity Request Stauts:  2017 04 01
     
     * We need to change status on an activity
    Available” ==1
            Activity can be requested
            Should be auto set when creating a public activity
    Requested   ==2
            Activity have been requested by user
            Should be auto set when user request a public activity
    Request approved ==3
            Manager have approved requested activity
            Should be auto set when manager approve users activity request
    Ongoing ==4
            Should be auto set when start date have been reached
    Completed ==5
            employee flag activity has been completed
            Should be set manuel by user (or manager)
    Completed approval ==6
            Manager have approved that activity have been completed by user
            Should be set manual by manager (or system owner)
     * 
     * REJECTED == 7
    */
        //var status = isPrivate ? StringFor.CREATED : StringFor.Available;

        GetResource();
        //  var status = isPrivate ? StringFor.CREATED : StringFor.Available;

        var status = "";
        switch (actReqStatus)
        {
            case "2":
                {
                    status = REQUESTED;//getResource.GetResource("REQUESTED.Text");//StringFor.REQUESTED;
                    break;
                }
            case "3":
                {
                    status = APPROVED;// getResource.GetResource("REQUESTED.Text"); //StringFor.APPROVED;
                    break;
                }
            case "4":
                {
                    status = ONGOING;// getResource.GetResource("REQUESTED.Text"); //StringFor.ONGOING;
                    break;
                }
            case "5":
                {
                    status = COMPLETED;// getResource.GetResource("REQUESTED.Text"); //StringFor.COMPLETED;
                    break;
                }
            case "6":
                {
                    status = COMPLETED_APPROVE;//getResource.GetResource("REQUESTED.Text"); //StringFor.COMPLETED_APPROVE;
                    break;
                }
            case "7":
                {
                    status = REJECTED;//getResource.GetResource("REQUESTED.Text"); //StringFor.REJECTED;
                    break;
                }

            case "8":
                {
                    status = CREATED;//getResource.GetResource("CREATED.Text"); //StringFor.CREATED;
                    break;
                }
            case "9":
                {
                    status = AVAILABLE;//getResource.GetResource("AVAILABLE.Text"); //StringFor.AVAILABLE;
                    break;
                }


        }
        return status;
    }

    public string REQUESTED { get; set; }
    public string APPROVED { get; set; }
    public string ONGOING { get; set; }

    public string COMPLETED { get; set; }
    public string COMPLETED_APPROVE { get; set; }
    public string REJECTED { get; set; }

    public string CREATED { get; set; }
    public string AVAILABLE { get; set; }
    public string ENABLED { get; set; }
    public string DISABLED { get; set; }

    public string ASSIGNED { get; set; }

    private void GetResource()
    {
        REQUESTED = GetLocalResourceObject("Requested1.Text").ToString();
        APPROVED = GetLocalResourceObject("Approved1.Text").ToString();
        ONGOING = GetLocalResourceObject("Ongoing1.Text").ToString();

        COMPLETED = GetLocalResourceObject("Completed1.Text").ToString();
        COMPLETED_APPROVE = GetLocalResourceObject("COMPLETED_APPROVE1.Text").ToString();
        REJECTED = GetLocalResourceObject("REJECTED1.Text").ToString();

        CREATED = GetLocalResourceObject("CREATED1.Text").ToString();
        AVAILABLE = GetLocalResourceObject("AVAILABLE1.Text").ToString();

        ASSIGNED = GetLocalResourceObject("Assigned1.Text").ToString();
    }
    protected void gvGrid_RowCommand(object sender, GridViewCommandEventArgs e)
    {


    }
    #endregion


    protected void gvActCat_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }

    protected void GetAllActivityCategory()
    {
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);


        var db = new startetkuEntities1();
        var getAllActCat = ActivityCategoryLogic.GetActivityCategoryList(ResLangId, companyId, 1, -1);// db.ActivityCategoryMasters.ToList();

        if (getAllActCat.Any())
        {

            ddActCate.DataSource = getAllActCat;
            ddActCate.DataTextField = "ActCatName";
            ddActCate.DataValueField = "ActCatId";
            ddActCate.DataBind();

            //ListItem li=new ListItem("All","0");
            //ddActCate.Items.Insert(0, new ListItem("All", "0"));
            ddActCate.Items.Insert(0, new ListItem(GetLocalResourceObject("All.Text").ToString(), "0"));


        }
    }

    protected void ddActCate_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            ActivityOverview();
            PersonOverviewTab2();
            GetActivityList_Tab4();
            var hdnVal = Hdntaginfo.Value;

            if (hdnVal == "2")
            {
                PersonOverviewTab2();
            }
            ScriptManager.RegisterStartupScript(updtPnl, updtPnl.GetType()
                                                    , "TabVisiblity", "TabVisiblity(" + hdnVal + ");", true);
        }
        catch (Exception ex)
        {

            ExceptionLogger.LogException(ex, 0, "ddActCate_OnSelectedIndexChanged");
        }
    }

    protected void ActivityVsActivityStatus_OnClick(object sender, EventArgs e)
    {
        ActivityOverview();
        ScriptManager.RegisterStartupScript(updtPnl, updtPnl.GetType()
                                                     , "TabVisiblity", "TabVisiblity(1);", true);
    }

    protected void PersonVsActivityStatus_OnClick(object sender, EventArgs e)
    {
        PersonOverviewTab2();
        ScriptManager.RegisterStartupScript(updtPnl, updtPnl.GetType()
                                                     , "alert", "TabVisiblity('2');", true);
    }

    protected void dd_Cout_Count_Tab1_OnSelectedIndexChanged(object sender, EventArgs e)
    {

        if (dd_Cout_Count_Tab1.SelectedItem.Value == "1")
        {
            ActivityOverview();
            ScriptManager.RegisterStartupScript(updtPnl, updtPnl.GetType()
                                                   , "TabVisiblity", "$('.doller').show();", true);
        }
        else
        {
            ActivityOverviewCount();
            ScriptManager.RegisterStartupScript(updtPnl, updtPnl.GetType()
                                                   , "TabVisiblity", "$('.doller').hide();", true);

        }

    }

    protected void dd_Cout_Count_Tab2_OnSelectedIndexChanged(object sender, EventArgs e)
    {

        ClientScript.RegisterStartupScript(this.GetType(), "ModelPopup2", "setTimeout(function(){Tab2Visible('Tab2');},500);", true);

        if (dd_Cout_Count_Tab2.SelectedItem.Value == "1")
        {
            PersonOverviewTab2();
            ScriptManager.RegisterStartupScript(updtPnl, updtPnl.GetType()
                                                    , "TabVisiblity", "TabVisiblity(2);$('.doller').hide();", true);
        }
        else
        {
            PersonOverviewTab2_Count();
            ScriptManager.RegisterStartupScript(updtPnl, updtPnl.GetType()
                                                    , "TabVisiblity", "TabVisiblity(2);$('.doller').hide()", true);

        }
        ScriptManager.RegisterStartupScript(updtPnl, updtPnl.GetType()
                                                     , "TabVisiblity", "TabVisiblity(2);", true);
    }


    #region Culture
    protected override void InitializeCulture()
    {
        // string language = "French";
        string language = Convert.ToString(Session["Culture"]);

        string languageId = "";

        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }

        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion

    protected void GetAllJobTypeByCompanyId()
    {
        JobTypeBM obj = new JobTypeBM();
        obj.jobIsActive = true;
        obj.jobIsDeleted = false;
        obj.jobCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);


        obj.GetAllJobTypeByCompanyId();


        DataSet ds = obj.ds;
        if (Convert.ToString(Session["Culture"]) == "Danish")
        {
            ds.Tables[0].Columns["jobName"].ColumnName = "abcd1";
            ds.Tables[0].Columns["jobNameDN"].ColumnName = "jobName";

        }
        ViewState["ds"] = ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlJobType.DataSource = ds.Tables[0];
                ddlJobType.DataTextField = "jobName";
                ddlJobType.DataValueField = "jobId";
                ddlJobType.DataBind();
                // ddlJobType.Items.Insert(0, "Selcet jobtype");
                ddlJobType.Items.Insert(0, new ListItem(GetLocalResourceObject("chkListJobtype.Text").ToString(), "0")); // jainam shah 14-4-2017

                // chkListDivision.Items.Insert(0, "All");


            }

        }
    }

    protected void GetAllDivisionByCompanyId()
    {
        DivisionBM obj = new DivisionBM();
        obj.depIsActive = true;
        obj.depIsDeleted = false;
        obj.depCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);


        obj.GetAllDivisionsbyCompanyId();


        DataSet ds = obj.ds;
        if (Convert.ToString(Session["Culture"]) == "Danish")
        {
            ds.Tables[0].Columns["divName"].ColumnName = "abcd";
            ds.Tables[0].Columns["divNameDN"].ColumnName = "divName";

        }
        ViewState["ds"] = ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlDepartment.DataSource = ds.Tables[0];
                ddlDepartment.DataTextField = "divName";
                ddlDepartment.DataValueField = "divId";
                ddlDepartment.DataBind();
                // li.Text = GetLocalResourceObject("SelectDivision.Text").ToString();
                // ddlDepartment.Items.Insert(0, " Select Department"); // jainam shah 14-4-2017
                ddlDepartment.Items.Insert(0, new ListItem(GetLocalResourceObject("SelectDivision.Text").ToString(), "0"));



            }

        }
    }

    protected void GetAllTeamCompanyId()
    {
        try
        {
            JobTypeBM obj = new JobTypeBM();
            obj.TeamIDs = "0";
            obj.jobIsActive = true;
            obj.jobIsDeleted = false;
            obj.jobCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            obj.GetAllTeam();
            DataSet ds = obj.ds;

            ViewState["ds"] = ds;

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlTeam.DataSource = ds.Tables[0];
                    if (Convert.ToString(Session["Culture"]) == "Danish")
                    {
                        ddlTeam.DataTextField = "TeamNameDN";
                    }
                    else
                    {
                        ddlTeam.DataTextField = "TeamName";
                    }
                    ddlTeam.DataValueField = "TeamID";
                    ddlTeam.DataBind();
                    ddlTeam.Items.Insert(0, new ListItem(GetLocalResourceObject("SelectTeam.Text").ToString(), "0"));
                }

            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "GetAllTeamCompanyId");
        }
    }


}