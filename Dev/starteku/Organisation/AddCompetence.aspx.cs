﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using startetku.Business.Logic;
using System.Web.UI.HtmlControls;
using System.Threading;
using System.Globalization;
using System.Resources;
using System.Reflection;

public partial class AddCompetence : System.Web.UI.Page
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
           // GetAllJobType();
            //Competence.InnerHtml = CommonMessages.Welcome + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            Competence.InnerHtml = "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            CompetenceSelectAll();

            SetDefaultMessage();

        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion

    #region method
    protected void clear()
    {
      
    }
    protected void SetDefaultMessage()
    {
       // lblMsg.Text = GetLocalResourceObject("lblHelloWorld.Text").ToString();

        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["msg"])))
        {
            if (Convert.ToString(Request.QueryString["msg"]) == "del")
            {
                //lblMsg.Text = CommonModule.msgRecordHasBeenDeletedSuccessfully;
                //lblMsg.Text = CommonMessages.msgRecordHasBeenDeletedSuccessfully;
                lblMsg.Text = GetLocalResourceObject("msgRecordHasBeenDeletedSuccessfully.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "ins")
            {
                //lblMsg.Text = CommonMessages.msgRecordInsertedSuccessfully;
                lblMsg.Text = GetLocalResourceObject("msgRecordInsertedSuccessfully.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "upd")
            {
                //lblMsg.Text = CommonMessages.msgRecordUpdatedSuccss;
                lblMsg.Text = GetLocalResourceObject("msgRecordUpdatedSuccss.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "arc")
            {
               // lblMsg.Text = CommonMessages.msgRecordArchiveSucces;
                lblMsg.Text = GetLocalResourceObject("CommonMessages.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
        }

    }
    protected void CompetenceSelectAll()
    {
        CompetenceMasterBM obj = new CompetenceMasterBM();
        obj.comIsActive = true;
        obj.comIsDeleted = false;
        obj.comCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllCompetenceMasterAdd();
        DataSet ds = obj.ds;
        if (Convert.ToString(Session["Culture"]) == "Danish")
        {
            ds.Tables[0].Columns["catName"].ColumnName = "abcd";
            ds.Tables[0].Columns["catNameDN"].ColumnName = "catName";

        }
        ViewState["ds"] = ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvGrid.DataSource = ds.Tables[0];
                gvGrid.DataBind();

                gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvGrid.DataSource = null;
                gvGrid.DataBind();
            }
        }
        else
        {
            gvGrid.DataSource = null;
            gvGrid.DataBind();
        }
    }
    //protected void InsertCompetenceMaster()
    //{
    //    CompetenceMasterBM obj = new CompetenceMasterBM();
    //    obj.comCompetence = txtName.Text;
    //    obj.comUserId = Convert.ToInt32(Session["OrgUserId"]);
    //    obj.comIsActive = true;
    //    obj.comIsDeleted = false;
    //    obj.comCreatedDate = DateTime.Now;
    //    obj.comCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
    //    obj.comCreateBy = 0;
    //    obj.comDivId = 0;
    //    obj.comJobtype = Convert.ToInt32(ddljobtype.SelectedValue);
    //    obj.InsertCompetenceMaster();
    //    if (obj.ReturnBoolean == true)
    //    {
    //        clear();
    //    }

    //    else
    //    {
    //        lblMsg.Text = CommonModule.msgProblemInsertRecord;
    //    }
    //}
    //protected void UpdateCompetenceMaster(int id)
    //{
    //    CompetenceMasterBM obj = new CompetenceMasterBM();
    //    obj.comId = id;
    //    obj.comCompetence = txtcname.Text;
    //    obj.comJobtype = Convert.ToInt32(ddljob.SelectedValue);
    //    obj.comUpdatedDate = DateTime.Now;
    //    obj.UpdateCompetenceMaster();
    //    if (obj.ReturnBoolean == true)
    //    {
    //        clear();
    //    }
    //    else
    //    {
    //        lblMsg.Text = CommonModule.msgProblemInsertRecord;
    //    }
    //}
    //protected void GetAllEmployeebyid(int id)
    //{
    //    ViewState["id"] = Convert.ToString(id);
    //    DataSet ds1 = (DataSet)ViewState["ds"];
    //    DataView dv = new DataView();

    //    dv = ds1.Tables[0].DefaultView;

    //    dv.RowFilter = ("comId =  '" + Convert.ToInt32(id) + "'");
    //    DataTable dtitm = dv.ToTable();
    //    DataSet ds = new DataSet();
    //    ds.Tables.Add(dtitm);
    //    if (ds != null)
    //    {
    //        if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["comCompetence"])))
    //            txtcname.Text = Convert.ToString(ds.Tables[0].Rows[0]["comCompetence"]);

    //        if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["comJobtype"])))
    //            ddljob.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["comJobtype"]);
    //    }

    //}
    //protected void GetAllJobType()
    //{
    //    JobTypeBM obj = new JobTypeBM();
    //    obj.jobIsActive = true;
    //    obj.jobIsDeleted = false;
    //    obj.jobCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
    //    obj.GetAllJobType();
    //    DataSet ds = obj.ds;

    //    if (ds != null)
    //    {
    //        if (ds.Tables[0].Rows.Count > 0)
    //        {
    //            ddljobtype.Items.Clear();
    //            ddljobtype.DataSource = ds.Tables[0];
    //            ddljobtype.DataTextField = "jobName";
    //            ddljobtype.DataValueField = "jobId";
    //            ddljobtype.DataBind();
    //            ddljobtype.Items.Insert(0, new ListItem(CommonModule.dropDownSelectJobtype, CommonModule.dropDownZeroValue));


    //            //ddljob.Items.Clear();
    //            //ddljob.DataSource = ds.Tables[0];
    //            //ddljob.DataTextField = "jobName";
    //            //ddljob.DataValueField = "jobId";
    //            //ddljob.DataBind();
    //            //ddljob.Items.Insert(0, new ListItem(CommonModule.dropDownSelectJobtype, CommonModule.dropDownZeroValue));
    //        }
    //        else
    //        {
    //            //ddljobtype.Items.Clear();
    //            //ddljobtype.Items.Insert(0, new ListItem(CommonModule.dropDownSelectJobtype, CommonModule.dropDownZeroValue));
    //            //ddljob.Items.Clear();
    //            //ddljob.Items.Insert(0, new ListItem(CommonModule.dropDownSelectJobtype, CommonModule.dropDownZeroValue));
    //        }
    //    }
    //    else
    //    {
    //        ddljobtype.Items.Clear();
    //        ddljobtype.Items.Insert(0, new ListItem(CommonModule.dropDownSelectDepartments, CommonModule.dropDownZeroValue));
    //        ddljob.Items.Clear();
    //        ddljob.Items.Insert(0, new ListItem(CommonModule.dropDownSelectJobtype, CommonModule.dropDownZeroValue));
    //    }

    //}
    #endregion

    #region grid Event
    protected void gvGrid_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "archive")
        {
            CompetenceMasterBM obj = new CompetenceMasterBM();
            obj.comId = Convert.ToInt32(e.CommandArgument);
            obj.comIsActive = false;
            obj.comIsDeleted = false;
            obj.CompetenceMasterAddUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    CompetenceSelectAll();

                    lblMsg.Text = CommonModule.msgRecordDeletedSuccss;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }
        //else if (e.CommandName == "View")
        //{
        //    GetAllEmployeebyid(Convert.ToInt32(e.CommandArgument));
        //    mpe.Show();
        //}
    }
    protected void gvGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        DataSet ds = (DataSet)ViewState["ds"];
        //foreach (TableCell tc in e.Row.Cells)
        //{
        //    tc.BorderStyle = BorderStyle.None;
        //}
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        HiddenField comId = e.Row.FindControl("comId") as HiddenField;
                        HtmlGenericControl divEdit = (HtmlGenericControl)e.Row.FindControl("divEdit");
                        HtmlGenericControl divDeletet = (HtmlGenericControl)e.Row.FindControl("divDeletet");
                        HtmlGenericControl div1 = (HtmlGenericControl)e.Row.FindControl("div1");
                        //HtmlGenericControl divsub = (HtmlGenericControl)e.Row.FindControl("divsub");
                        if (Convert.ToInt32(comId.Value) == 0)
                        {
                            divEdit.Visible = false;
                            divDeletet.Visible = false;
                          //  divsub.Visible = false;
                        }
                        else
                        {
                            div1.Visible = false;
                        }
                    }
                }
            }
        }


    }
    #endregion


    #region Button Event
    protected void btnBack_click(object sender, EventArgs e)
    {
        switch (Convert.ToInt32(Session["OrguserType"]))
        {
            case 2:
                Response.Redirect("Manager-dashboard.aspx");
                break;
            case 1:
                Response.Redirect("Organisation-dashboard.aspx");
                break;
            default:
                Response.Redirect("Employee_dashboard.aspx");
                break;
        }
    }
    //protected void btnsubmit_click(object sender, EventArgs e)
    //{
    //    InsertCompetenceMaster();
    //    CompetenceSelectAll();
    //}
    //protected void btnupdate_click(object sender, EventArgs e)
    //{

    //    Int32 id = Convert.ToInt32(ViewState["id"]);
    //    UpdateCompetenceMaster(id);
    //    CompetenceSelectAll();
    //}
    //protected void btnclose_click1(object sender, EventArgs e)
    //{
    //    //clear();
    //}
    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        // string language = "French";
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    //if (language.EndsWith("French")) languageId = "da-DK";
        //    if (language.EndsWith("Danish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();
            //if (!string.IsNullOrEmpty(language))
            //{
            //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
            //    {
            //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
            //        {
            //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
            //        }
            //    }
            //    //if (language.EndsWith("Danish")) languageId = "da-DK";
            //    //else languageId = "en-GB";
            //    
            //}
            SetCulture(languageId);
        }

        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion
}