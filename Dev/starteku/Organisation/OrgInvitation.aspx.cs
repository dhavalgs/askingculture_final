﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using System.Configuration;
using startetku.Business.Logic;
using System.Threading;
using System.Globalization;
using starteku_BusinessLogic.Model;

public partial class Organisation_OrgInvitation : System.Web.UI.Page
{
    string temp = "0";
    startetkuEntities1 db = new startetkuEntities1();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "my", " SetExpandCollapse();", true);

        try
        {

           // aa();
            if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
            {
                Response.Redirect("login.aspx");
            }
            if (!IsPostBack)
            {
                
                ViewState["PreviousPage"] = Request.UrlReferrer;
                Eemployee.InnerHtml = GetLocalResourceObject("Welcome.Text").ToString() + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
                if (!String.IsNullOrEmpty(Request.QueryString["id"]))
                {


                    temp = Convert.ToString(Request.QueryString["id"]);

                    Invition invt = db.Invitions.Find(Convert.ToInt32(temp));
                    if (invt != null)
                    {
                        if (invt.invStatus == 1)
                        {
                            btnsubmit.Visible = false;
                            uploaddoc.Visible = false;
                            helpresponse.Visible = false;
                            invdiv.Attributes.Remove("class");
                            invdiv.Attributes.Add("class", "col-md-6");
                            GetHelpResponseByTrackId(Convert.ToInt32(invt.invHelpTrackId), "Reply Help");


                        }
                        else
                        {
                            uploaddoc.Visible = true;
                            helpresponse.Visible = true;
                            invdiv.Attributes.Remove("class");
                            invdiv.Attributes.Add("class", "col-md-12");
                        }



                    }
                    Label2.Visible = false;

                    pnlShowDoc.Visible = false;



                    back.Visible = false;
                }
                else if (!String.IsNullOrEmpty(Request.QueryString["view"]))
                {

                    temp = Convert.ToString(Request.QueryString["view"]);
                    ddlstastus.Visible = false;
                    submit.Visible = false;
                    uploaddoc.Visible = false;
                    string QueryString = "0";
                    QueryString = Convert.ToString(Request.QueryString["View"]);
                    InvitationBM obj1 = new InvitationBM();
                    obj1.invId = Convert.ToInt32(QueryString);
                    obj1.GetAllInvitionbyid();
                    DataSet dsinvi = obj1.ds;

                    if (dsinvi != null)
                    {
                        if (dsinvi.Tables[0].Rows.Count > 0)
                        {
                            int count = Convert.ToInt32(dsinvi.Tables[0].Rows[0]["invstatus"]);
                            if (count != 1)
                            {
                                if (Convert.ToString(dsinvi.Tables[0].Rows[0]["invfromUserId"]) != Convert.ToString(Session["OrgUserId"]))
                                {
                                    hdnHelptreckId.Value = Convert.ToString(dsinvi.Tables[0].Rows[0]["invHelpTrackId"]);
                                    Accept.Visible = true;


                                }


                                invdiv.Attributes.Remove("class");
                                invdiv.Attributes.Add("class", "col-md-12");
                            }
                            else
                            {
                                if (Convert.ToString(dsinvi.Tables[0].Rows[0]["invtype"]) == "ProHelp")
                                {
                                    invdiv.Attributes.Remove("class");
                                    invdiv.Attributes.Add("class", "col-md-6");

                                    
                                    invHelpReply.Attributes.Remove("class");
                                    invHelpReply.Attributes.Add("class", "col-md-6");
                                    GetHelpResponseByTrackId(Convert.ToInt32(dsinvi.Tables[0].Rows[0]["invHelpTrackId"]), "AcptHelp");
                                }
                                else
                                {
                                    invdiv.Attributes.Remove("class");
                                    invdiv.Attributes.Add("class", "col-md-6");
                                }
                            }
                        }
                    }




                    // pnlShowDoc.Visible = false;
                    helpresponse.Visible = false;
                }
                if (String.IsNullOrEmpty(Request.QueryString["HelpTrackId"]))
                {


                    //  invHelpReply.Visible = false;

                }
                else
                {
                    GetHelpResponseByTrackId(0, "Reply Help");
                }

                GetAllEmployeebyid();
                GetAllDocuments();


                //Update notification counter, and notification list =2015-11-16 Saurin
                var notId = Request.QueryString["notIf"];
                if (!string.IsNullOrWhiteSpace(notId))
                {
                    NotificationBM obj = new NotificationBM();
                    obj.notId = Convert.ToInt32(notId);
                    obj.notPopUpStatus = false;
                    obj.UpdateNotificationbyid();
                }

            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex);
            //Response.Write(ex);

        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }

    #region method
    public string DocName1 = "";
    public string DocName2 = "";
    protected void GetAllEmployeebyid()
    {

        // Response.Write("In GetAllEmployeebyid");

        try
        {
            hdnfromId.Text = "sp";
            InvitationBM obj = new InvitationBM();
            obj.invId = Convert.ToInt32(temp);
            //Response.Write("InvId"+obj.invId);
            obj.GetAllInvitionbyid();
            DataSet ds = obj.ds;

            if (ds != null)
            {
                //Response.Write("DS Not Null");
                if (!(ds.Tables[0].Rows.Count > 0))
                {
                    //Response.Write("DS C"+ds.Tables[0].Rows.Count);
                    return;
                }

                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["invComId"])))
                {
                    //Response.Write("DS C" + Convert.ToString(ds.Tables[0].Rows[0]["invComId"]));
                    Session["InviteComId"] = Convert.ToString(ds.Tables[0].Rows[0]["invComId"]);
                    Session["InviteComId1"] = Convert.ToString(ds.Tables[0].Rows[0]["invComId"]);

                }
                else
                {

                   
                    Session["InviteComId"] = 0;
                }

                Session["FromUserid"] = Convert.ToString(ds.Tables[0].Rows[0]["invfromUserId"]);
                //Response.Write("DS C" + Convert.ToString(ds.Tables[0].Rows[0]["invfromUserId"]));
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["name"])))
                {
                    //Response.Write("DS C" + Convert.ToString(ds.Tables[0].Rows[0]["name"]));
                    lblName.Text = Convert.ToString(ds.Tables[0].Rows[0]["name"]);
                }

                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["Toname"])))
                {
                    //Response.Write("DS C" + Convert.ToString(ds.Tables[0].Rows[0]["Toname"]));
                    lblNameTo.Text = Convert.ToString(ds.Tables[0].Rows[0]["Toname"]);
                }

                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["invfromUserId"])))
                {
                    //Response.Write("DS C" + Convert.ToString(ds.Tables[0].Rows[0]["invfromUserId"]));
                    hdnfromId.Text = Convert.ToString(ds.Tables[0].Rows[0]["invfromUserId"]);
                }
                else
                {
                    //Response.Write("DS C" + Convert.ToString(ds.Tables[0].Rows[0]["invfromUserId"]));
                    hdnfromId.Text = "0";
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["comCompetence"])))
                {
                    //Response.Write("DS C" + Convert.ToString(ds.Tables[0].Rows[0]["comCompetence"]));
                    lblCompetence.Text = Convert.ToString(ds.Tables[0].Rows[0]["comCompetence"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["invsubject"])))
                {
                    //Response.Write("DS C" + Convert.ToString(ds.Tables[0].Rows[0]["invsubject"]));
                    lblInvitation.Text = Convert.ToString(ds.Tables[0].Rows[0]["invsubject"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["invCreatedDate"])))
                {
                    //Response.Write("DS C" + Convert.ToString(ds.Tables[0].Rows[0]["invCreatedDate"]));
                    lblRequestDate.Text = Convert.ToString(ds.Tables[0].Rows[0]["invCreatedDate"]);
                }

                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["invfromUserId"])))
                {
                    //Response.Write("DS C" + Convert.ToString(ds.Tables[0].Rows[0]["invfromUserId"]));
                    Session["invfromUserId"] = Convert.ToString(ds.Tables[0].Rows[0]["invfromUserId"]);
                    ViewState["invfromUserId1"] = Convert.ToString(ds.Tables[0].Rows[0]["invfromUserId"]);
                    //  Session["invfromUserId"] = Convert.ToString(ds.Tables[0].Rows[0]["invfromUserId"]); // A user who wants Help
                }
                //Doc show/hide
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["invDocName"])))
                {
                    //Response.Write("DS C" + Convert.ToString(ds.Tables[0].Rows[0]["invDocName"]));
                    DocName1 = Convert.ToString(ds.Tables[0].Rows[0]["invDocName"]);
                    lblDocName.Text = Convert.ToString(ds.Tables[0].Rows[0]["invDocName"]);
                    pnlShowDoc.Visible = true;
                    helpresponse.Visible = false;
                }
                else
                {
                    doc1.Visible = false;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["invDocSuggestPath"])))
                {
                    //Response.Write("DS C" + Convert.ToString(ds.Tables[0].Rows[0]["invDocSuggestPath"]));
                    DocName2 = Convert.ToString(ds.Tables[0].Rows[0]["invDocSuggestPath"]);
                    lblDocName2.Text = Convert.ToString(ds.Tables[0].Rows[0]["invDocSuggestPath"]);
                    pnlShowDoc.Visible = true;
                    helpresponse.Visible = false;
                }
                else
                {
                    //Response.Write("DS " + Convert.ToString(ds.Tables[0].Rows[0]["invDocSuggestPath"]));
                    doc2.Visible = false;

                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["invHelpTrackId"])))
                {
                    //Response.Write("DS C" + Convert.ToString(ds.Tables[0].Rows[0]["invHelpTrackId"]));
                    Session["invHelpTrackId"] = Convert.ToString(ds.Tables[0].Rows[0]["invHelpTrackId"]);
                }

                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["invStatus"])))
                {
                    //Response.Write("DS C" + Convert.ToString(ds.Tables[0].Rows[0]["invStatus"]));
                    Int32 status = Convert.ToInt32(ds.Tables[0].Rows[0]["invStatus"]);
                    //Response.Write("DS C" + Convert.ToString(ddlstastus.SelectedValue));
                    if (status == 3)
                    {
                        ddlstastus.SelectedValue = "3";
                        Label2.Text = @"Pending";
                    }
                    else if (status == 2)
                    {
                        ddlstastus.SelectedValue = "2";
                        Label2.Text = @"Cancel";
                    }
                    else if (status == 1)
                    {
                        ddlstastus.SelectedValue = "1";
                        Label2.Text = @"Accept";
                    }
                }
            }

        }
        catch (Exception ex)
        {

            ExceptionLogger.LogException(ex);
            //Response.Write(ex);
        }

    }

    protected void UpdateUser()
    {
        string QueryString = "0";
        QueryString = Convert.ToString(Request.QueryString["id"]);
        InvitationBM obj = new InvitationBM();
        obj.invId = Convert.ToInt32(QueryString);
        if (ddlstastus.SelectedValue == "1")
        {
            obj.invStatus = 1;
            //obj.skillPoint = Convert.ToInt32(txtpoint.Text);
        }
        else if (ddlstastus.SelectedValue == "2")
        {
            obj.invStatus = 2;
        }
        else if (ddlstastus.SelectedValue == "3")
        {
            obj.invStatus = 3;
        }
        obj.UpdateInvitionRequest();
        if (obj.ReturnBoolean == true)
        {
            //if (ddlstastus.SelectedValue == "1")
            //{
            //    Updatepoint();
            //}
            //insertskillchild();
            //   Response.Redirect("OrgInvitation_request.aspx?msg=upd");
            Response.Redirect(Convert.ToString(ViewState["PreviousPage"]));

        }

    }

    /*Saurin|20150109|*/
    protected void SubmitHelpDetails()
    {
        GetSessionData();
        String filePath = "";
        DocumentBM obj = new DocumentBM();
        DataSet invids = new DataSet();
        var docFileNameFriendly = string.Empty;
        var docAttachmentName = string.Empty;

        obj.docDescription = txtInviteGiveHelp.Text;

        if (FileUpload1.HasFile)
        {
            filePath = System.DateTime.Now.ToString("MMddyyhhmmss") + "_" + FileUpload1.PostedFile.FileName;

            FileUpload1.SaveAs(Server.MapPath("../") + "Log/upload/Document/" + System.IO.Path.GetFileName(Common.CleanFileName(filePath)));
            docAttachmentName = System.IO.Path.GetFileName(Common.CleanFileName(filePath));
            docFileNameFriendly = docAttachmentName;
            Session["docFileName"] = docAttachmentName;
        }
        else
        {
            //InsertUpdateLog(Convert.ToInt32(Session["invfromUserId"]));
            if (!string.IsNullOrWhiteSpace(txtInviteGiveHelp.Text))
            {
                invids = WebService1.Invitation(txtInviteGiveHelp.Text, Convert.ToInt32(Request.QueryString["id"]), GetLocalResourceObject("InviteSubjectEngResource.Text").ToString(), GetLocalResourceObject("InviteSubjectDnResource.Text").ToString());
                if (invids != null)
                {
                    if (invids.Tables[2].Rows.Count > 0)
                    {
                        if (!String.IsNullOrEmpty(Convert.ToString(invids.Tables[2].Rows[0]["HelpProbivdeId"])))
                        {
                            int count = Convert.ToInt32(invids.Tables[2].Rows[0]["HelpProbivdeId"]);

                            if (count == 0)
                            {
                                InsertUpdateLog(Convert.ToInt32(ViewState["invfromUserId1"]));
                            }

                        }
                    }
                }

            }
            //Response.Redirect("OrgInvitation_request.aspx?msg=upd");


            return;
        }
        var docFileNameInSystem = Common.CleanFileName(docAttachmentName);
        docAttachmentName = Common.CleanFileName(docAttachmentName);
        const string docTypes = "pdf";
        var userId = Convert.ToInt32(Session["OrgUserId"]);

        var docCreatedDt = DateTime.Now;
        var docCompnyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.docDepId = 0;
        obj.docApprovedStatus = false;
        obj.doccompetence = txtInviteGiveHelp.Text;


        DateTime docUpdatedDate = new DateTime();

        string docCreatedBy = string.Empty;
        int docDepIds = 0;
        string docTitle = docAttachmentName;
        bool docApprovedStatus = false;

        //
        var aa = new List<string>();

        string divisionIds = string.Join(",", aa.ToArray());
        aa.Clear();
        //

        string jobTypeIds = string.Join(",", aa.ToArray());


        var isPublic = false;

        var keywords = string.Empty;

        var repository = string.Empty;

        var txtDescriptions = txtInviteGiveHelp.Text;

        var docShareToId = Convert.ToInt32(Session["invfromUserId"]);

        obj.InsertDocument(docFileNameFriendly, docFileNameInSystem, docAttachmentName, docTypes, userId, docCreatedDt, docUpdatedDate, false, true, docCompnyId, docCreatedBy, docDepIds, docTitle, string.Empty, docApprovedStatus, divisionIds, jobTypeIds, isPublic, keywords, repository, txtDescriptions, docShareToId, 0, "");

        //WebService1.Invitation(txtInviteGiveHelp.Text);

        invids = WebService1.Invitation(txtInviteGiveHelp.Text, Convert.ToInt32(Request.QueryString["id"]), GetLocalResourceObject("InviteSubjectEngResource.Text").ToString(), GetLocalResourceObject("InviteSubjectDnResource.Text").ToString());
        if (invids != null)
        {
            if (invids.Tables[2].Rows.Count > 0)
            {
                if (!String.IsNullOrEmpty(Convert.ToString(invids.Tables[2].Rows[0]["HelpProbivdeId"])))
                {
                    int count = Convert.ToInt32(invids.Tables[2].Rows[0]["HelpProbivdeId"]);

                    if (count == 0)
                    {
                        InsertUpdateLog(docShareToId);
                    }

                }
            }
        }


    }

    private void GetSessionData()
    {
        /*Session["InviteToUserId"] =temp;*/
        Session["Inviteformail"] = "Provide Help Competence";
        Session["Invitemessge"] = "Reply Help";

        //Session["InviteComId"] = Request.QueryString["Lid"];
        Session["notificationmsg"] = "sent you invitation to provide help"; //--2015-12-07
        Session["notificationmsgReply"] = "reply you on your invitation to provide help";


        // txtInviteGiveHelp.Text =  @" have sent you Invitation";

    }
    protected void insertKnowledge_Point(Int32 point)
    {
        // Session["InviteComId"];
        Knowledge_PointBM obj = new Knowledge_PointBM();
        obj.Knowledge_ComId = Convert.ToInt32(Session["InviteComId1"]);
        obj.Knowledge_CompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.Knowledge_point = Convert.ToString(point);
        obj.Knowledge_UserId = Convert.ToInt32(Session["OrgUserId"]);
        obj.CreatedDate = DateTime.Now;
        obj.IsDeleted = false;
        obj.IsActive = true;
        obj.InsertKnowledge_Point();

    }

    protected void InsertUpdateLog(int needHelpUserID)
    {

        //Givehelp  point  to   loggedin user
        var db = new startetkuEntities1();
        var username = (from user in db.UserMasters where user.userId == needHelpUserID select new { user.userFirstName, user.userLastName }).FirstOrDefault();
        LogMasterLogic obj = new LogMasterLogic();
        obj.LogDescription = string.Format("{0} Provided Help  to  userID : {1} ", Convert.ToString(Session["OrgUserName"]), username.userFirstName + " " + username.userLastName);   // User log description..
        //obj.LogDescription = string.Format("{0} Provide Help to  userID : {1} ", Convert.ToString(Session["OrgUserName"]), needHelpUserID);   // User log description..

        var userID = Convert.ToInt32(Session["OrgUserId"]);

        var isPointAllow = true;

        var point = LogMasterLogic.InsertUpdateLogParam(userID, Common.ALP(), Common.ReadDocByOther(), obj.LogDescription, isPointAllow, needHelpUserID, 0);





        if (isPointAllow)
            PointBM.InsertUpdatePoint(userID, 0, point);//check parameter position >> point :::  3rd pos : ALP, 2nd : PLP  -----------------

        //Provide Point if suggested a doc file

        /* var isPointForSUggestDoc=
         var pointForDocSuggest = LogMaster.InsertUpdateLogParam(userID, Common.ALP(), Common.SuggestSession(), obj.LogDescription, isPointAllow);

         if (isPointAllow)
             PointBM.InsertUpdatePoint(userID, 0, point);//check parameter position >> point :::  3rd pos : ALP, 2nd : PLP*/


        //Give point  to  user who need  help

        //obj.LogDescription = string.Format("userID : {0} Take help from {1} ", needHelpUserID, Convert.ToString(Session["OrgUserName"]));   // User log description..

        //point = LogMaster.InsertUpdateLogParam(needHelpUserID, Common.ALP(), Common.RequestSession(), obj.LogDescription, isPointAllow);

        //if (isPointAllow)
        //    PointBM.InsertUpdatePoint(needHelpUserID, 0, point); //check parameter position >> point :::  3rd pos : ALP, 2nd : PLP   -------- AS per requirement Updated by suhani 27/1/2016 ------ Only help provider get point for same requsting perosn and same competence

        if (point > 0)
        {
            insertKnowledge_Point(point);
        }
    }
    protected void txtsearch_TextChanged(object sender, EventArgs e)
    {
        GetAllDocuments();
        //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>AnotherFunction();</script>", false);

    }
    public void GetAllDocuments()
    {
        try
        {
            DocumentBM obj = new DocumentBM();
            obj.docIsActive = true;
            obj.GetAllDocument();
            DataSet ds = obj.ds;

            DataView dv = new DataView();
            dv = ds.Tables[0].DefaultView;

            // dv.RowFilter = ("docUserId =  '" + Convert.ToInt32(Session["OrgUserId"]) + "'");

            dv.RowFilter = "(docUserId = '" + Convert.ToInt32(Session["OrgUserId"]) + "' or docIsPublic = 'True'  ) ";


            //dv.RowFilter = "(docUserId = '" + Convert.ToInt32(Session["OrgUserId"]) + "' or docIsPublic = 1  ) ";

            dv.RowFilter += "and (docFileName_Friendly LIKE '%" + Convert.ToString(txtsearch.Text.Trim()) + "%' OR docKeywords like '%" + Convert.ToString(txtsearch.Text.Trim()) + "%') ";
            // dv.RowFilter = string.Concat("userFullname LIKE '%", Convert.ToString(txtserch.Value), "%'");

            DataTable dtitm = dv.ToTable();
            DataSet ds1 = new DataSet();
            ds1.Tables.Add(dtitm);

            if (ds1.Tables[0].Rows.Count > 0)
            {
                rep_document.DataSource = ds1;
                rep_document.DataBind();
            }
        }
        catch (Exception ex)
        {

        }
    }

    #endregion

    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtInviteGiveHelp.Text) && !FileUpload1.HasFile && string.IsNullOrEmpty(lblYourDocName.Text))
        {
            return;
            //Add condition if Helptext or DocumentSharing missing then do not add entry in database.. Use RETURN. !Saurin ! 20150112
        }


        SubmitHelpDetails();
        if (!String.IsNullOrEmpty(Request.QueryString["id"]))
        {
            UpdateUser();
        }

    }
    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Danish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion

    public void GetHelpResponseByTrackId(int t,string invtype)
    {
        var trackId = 0;
        if (t == 0)
        {
            if (String.IsNullOrEmpty(Request.QueryString["HelpTrackId"]))
            {
                rptHelpReply.DataSource = null;
                rptHelpReply.DataBind();
                return;
            }
            else
            {
                trackId = Convert.ToInt32(Request.QueryString["HelpTrackId"]);
            }

        }
        else
        {
            trackId = t;

        }

        InvitationBM obj = new InvitationBM();
        obj.invIsActive = true;
        obj.invIsDeleted = false;
        obj.invFromUserId = Convert.ToInt32(Session["OrgUserId"]);
        obj.invCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.invHelpTrackId = trackId;
        obj.invType = invtype;
        obj.GetHelpResponseByTrackId();

        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                invHelpReply.Visible = true;
                rptHelpReply.DataSource = ds.Tables[0];
                rptHelpReply.DataBind();
                // rptHelpReply.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                invHelpReply.Visible = false;
                rptHelpReply.DataSource = null;
                rptHelpReply.DataBind();
            }
        }
        else
        {
            rptHelpReply.DataSource = null;
            rptHelpReply.DataBind();
            invHelpReply.Visible = false;
        }
    }


    #region Repeater




    protected void rep_document_OnItemCommand(object source, RepeaterCommandEventArgs e)
    {
        //


        lblLabelOfSelectedDOc.Visible = true;
        lblYourDocName.Text = e.CommandArgument.ToString();
        Session["lblDocSpecificPathName"] = lblYourDocName.Text;
    }

    #endregion

    protected void rptHelpReply_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            Label docname1 = e.Item.FindControl("rptLblDocName1") as Label;
            Label docname11 = e.Item.FindControl("lbl") as Label;


            Panel docs1 = e.Item.FindControl("pnlDoc1") as Panel;
            Panel docs2 = e.Item.FindControl("pnlDoc2") as Panel;

            if (docname1 != null && string.IsNullOrWhiteSpace(docname1.Text))
            {
                if (docs1 != null) docs1.Visible = false;
            }
            var docname2 = e.Item.FindControl("rptLblDocName2") as Label;
            if (docname2 != null && string.IsNullOrWhiteSpace(docname2.Text))
            {
                if (docs2 != null) docs2.Visible = false;
            }
        }
    }

    

    protected void Button4_OnClick(object sender, EventArgs e)
    {

        string QueryString = "0";
        QueryString = Convert.ToString(Request.QueryString["View"]);
        InvitationBM obj1 = new InvitationBM();
        obj1.invId = Convert.ToInt32(QueryString);
        obj1.invStatus = 1;
        obj1.UpdateInvitionRequest();

        Int32 fromUserid = Convert.ToInt32(Session["FromUserid"]);
        Int32 compId = Convert.ToInt32(Session["InviteComId"]);
        var userID = Convert.ToInt32(Session["OrgUserId"]);
        int status = 1;
        string type = "ProHelp";


        InvitationBM objAtt = new InvitationBM();
        objAtt.invfromUserId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        objAtt.invToUserId = fromUserid;
        objAtt.invsubject = txtAcceptHelp.Text;
        objAtt.invCreatedDate = DateTime.Now;
        objAtt.invIsDeleted = false;
        objAtt.invIsActive = true;
        objAtt.invCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
        objAtt.invtype = "AcptHelp";
        objAtt.invComId = compId;
        objAtt.invStatus = Convert.ToInt32(1);

        objAtt.invHelpTrackId =Convert.ToInt32(hdnHelptreckId.Value);

        //Add docfilename and suggestpath
        objAtt.invDocSuggestPath = "";
        objAtt.invDocName = "";
        
        objAtt.InsertInvition();
        


        var db = new startetkuEntities1();

        var data = (from o in db.Invitions
                    where
                        o.invComId == compId && o.invfromUserId == fromUserid && o.invToUserId == userID &&
                        o.invStatus == status && o.invtype == type
                    select o).ToList();
        if (obj1.ReturnBoolean == true)
        {
            if (data.Count <= 1)
            {
                LogMasterLogic obj = new LogMasterLogic();

                obj.LogDescription = string.Format("{0} " + Accept_Help_from_userID.Text + " {1} ", Convert.ToString(Session["OrgUserName"]), lblName.Text);

                var isPointAllow = true;
                var point = LogMasterLogic.InsertUpdateLogParam(userID, Common.ALP(), Common.AcceptProvidedHelp(), obj.LogDescription, isPointAllow, Convert.ToInt32(hdnfromId.Text), 0);
                if (isPointAllow)
                    PointBM.InsertUpdatePoint(userID, 0, point);
                if (point > 0)
                {
                    insertKnowledge_Point(point);
                }
            }
            #region Notification

            UserBM objuser = new UserBM();
            objuser.SetUserId = Convert.ToInt32(fromUserid);
            objuser.GetUserSettingById();
            DataSet ds1 = objuser.ds;
            if (ds1.Tables[0].Rows.Count > 0)
            {
                if (!String.IsNullOrEmpty(Convert.ToString(ds1.Tables[0].Rows[0]["SetInvitationNotification"])))
                {
                    if (Convert.ToBoolean(ds1.Tables[0].Rows[0]["SetNewFileUploadwarning"]))
                    {
                        NotificationBM obj = new NotificationBM();
                        obj.notsubject = Convert.ToString("accepted your help");
                        obj.notUserId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
                        obj.notIsActive = true;
                        obj.notIsDeleted = false;
                        obj.notPopUpStatus = true;
                        obj.notCreatedDate = DateTime.Now;
                        obj.notCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);

                        obj.notpage = "OrgInvitation.aspx?view=" + Request.QueryString["view"];
                        //obj.notpage = "OrgInvitation_request.aspx";
                        obj.nottype = "Accepted";
                        obj.notToUserId = Convert.ToInt32(fromUserid);
                        obj.InsertNotification();


                    }
                }
            }

            #endregion

            #region Send mail
            //String subject = "Accept to be helped";
            Template template = CommonModule.getTemplatebyname1("Accept to be helped", fromUserid);
            //String confirmMail = CommonModule.getTemplatebyname("Accept to be helped", fromUserid);
            string confirmMail = template.TemplateName;
            if (!String.IsNullOrEmpty(confirmMail))
            {

                string subject = GetLocalResourceObject("AceeptToHelpedEngSubject.Text").ToString();
                if (template.Language == "Danish")
                {
                    subject = GetLocalResourceObject("AceeptToHelpedDnSubject.Text").ToString();
                }
                UserBM Cust = new UserBM();
                Cust.userId = Convert.ToInt32(fromUserid);
                // Cust.SelectmanagerByUserId();
                Cust.SelectPasswordByUserId();
                DataSet ds = Cust.ds;
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        String name = Convert.ToString(ds.Tables[0].Rows[0]["userFirstName"]);
                        string fromid = Convert.ToString(ConfigurationManager.AppSettings["fromid"]);
                        String empname = Convert.ToString(Session["OrgUserName"]);
                        string tempString = confirmMail;
                        tempString = tempString.Replace("###name###", name);
                        tempString = tempString.Replace("###empname###", empname);
                        CommonModule.SendMailToUser(Convert.ToString(ds.Tables[0].Rows[0]["userEmail"]), subject, tempString, fromid, Convert.ToString(HttpContext.Current.Session["OrgCompanyId"]));
                    }
                }
            }

            #endregion

            //Response.Redirect("OrgInvitation_request.aspx?msg=upd");
            Response.Redirect(Convert.ToString(ViewState["PreviousPage"]) + "?msg=upd");
        }










    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        //switch (Convert.ToInt32(Session["OrguserType"]))
        //{
        //    case 2:
        //        Response.Redirect("Manager-dashboard.aspx");
        //        break;
        //    case 1:
        //        Response.Redirect("Organisation-dashboard.aspx");
        //        break;
        //    default:
        //        Response.Redirect("Employee_dashboard.aspx");
        //        break;
        //}
        Response.Redirect(Convert.ToString(ViewState["PreviousPage"]));

    }
}