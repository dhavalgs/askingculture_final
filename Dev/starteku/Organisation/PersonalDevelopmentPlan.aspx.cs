﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Web.Services;
using starteku_BusinessLogic.Model;
using startetku.Business.Logic;
using System.Configuration;
using LogMaster = starteku_BusinessLogic.LogMasterLogic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;
using System.Web.Script.Services;
using System.IO;
using Winnovative.PdfCreator;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.html;
using System.Resources;
using System.Collections;
using System.Drawing;

public partial class Organisation_PersonalDevelopmentPlan : System.Web.UI.Page
{
    Boolean checksetbtn = false;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
    }

    public int ResLangId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Convert.ToInt32(Request.QueryString["umId"]) == null)
        //{
        //    Response.Redirect("login.aspx");
        //}
        try
        {
            var tmpQuesID = Convert.ToInt32(hdnQuestionID.Value);
            var id = Convert.ToInt32(Request.QueryString["umId"]);

            DataSet ds1 = new DataSet();
            var userId = Convert.ToInt32(Session["OrgUserId"]);
            ds1 = GetQuestionPersonalComment(0, Convert.ToInt32(id), Convert.ToInt32(hdnqpercomID.Value), userId);
            // ExceptionLogger.LogException(new Exception(), 0, "id:" + Convert.ToString(ds1.Tables[0].Rows[0]["qpercomID"]));
            if (ds1 != null)
            {
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    hdnIsDisplayInReport.Value = Convert.ToString(ds1.Tables[0].Rows[0]["IsDisplayInReport"]);
                    hdnIsPDFPersonalComment.Value = Convert.ToString(ds1.Tables[0].Rows[0]["IsPDPApproved"]);
                    hdnqpercomID.Value = Convert.ToString(ds1.Tables[0].Rows[0]["qpercomID"]);
                }
            }
            Int32 userCompanyID = 0;
            Int32 type = 0;
            var userDetail = UserBM.GetUserByIdLinq(id);
            if (userDetail != null)
            {
                type = Convert.ToInt32(userDetail.userType);
                userCompanyID = Convert.ToInt32(userDetail.userCompanyId);
            }
            if (!IsPostBack)
            {
                var db = new startetkuEntities1();
                var loginuserId = Convert.ToInt32(Session["OrgUserId"]);
                if (userDetail != null)
                {
                    lblddlUserName.Text = userDetail.userFirstName + " " + userDetail.userLastName;

                    Session["UserFullName"] = lblddlUserName.Text;

                }
                hdnQueryUmID.Value = Convert.ToString(id);

                Session["QueryStringID"] = Request.QueryString["umId"];
                hdnUserType.Value = Convert.ToString(Session["OrguserType"]);
                gridMeetingDocumentBind();
                BindStatus();
                GetAllUserQuestion_PDPList();
                GetUserAssignDirectionList_PDP();
                //ClientScript.RegisterStartupScript(this.GetType(), "", "DisplayTab(1);", true);
                Hdn_Lang_id.Value = Convert.ToString(Session["Culture"]);

                var EnaData = db.GetItemsEnableList(userId, userCompanyID).FirstOrDefault();

                Session["Aemcompid"] = EnaData.Aemcompid;
                Session["Cemcompid"] = EnaData.comdirCompID;
                Session["Compcompid"] = EnaData.compCompID;
                Session["Pointcompid"] = EnaData.pointCompID;
                Session["Quescompid"] = EnaData.quesemID;

                //var actEnaData = db.ActicityEnableMasters.FirstOrDefault(o => o.Aemcompid == userId || o.Aemcompid == userCompanyID);
                //if (actEnaData != null)
                //{
                //    Session["Aemcompid"] = actEnaData.Aemcompid;
                //}
                //else
                //{
                //    Session["Aemcompid"] = null;
                //}

                //var comDirEnaData = db.CompanyDirectionEnableMasters.FirstOrDefault(o => o.comdirCompID == userId || o.comdirCompID == userCompanyID);
                //if (comDirEnaData != null)
                //{
                //    Session["Cemcompid"] = comDirEnaData.comdirCompID;
                //}
                //else
                //{
                //    Session["Cemcompid"] = null;
                //}
                //var compEnaData = db.CompetenceEnableMasters.FirstOrDefault(o => o.compCompID == userId || o.compCompID == userCompanyID);
                //if (compEnaData != null)
                //{
                //    Session["Compcompid"] = compEnaData.compCompID;
                //}
                //else
                //{
                //    Session["Compcompid"] = null;
                //}
                //var pointEnaData = db.PointEnableMasters.FirstOrDefault(o => o.pointCompID == userId || o.pointCompID == userCompanyID);
                //if (pointEnaData != null)
                //{
                //    Session["Pointcompid"] = pointEnaData.pointCompID;
                //}
                //else
                //{
                //    Session["Pointcompid"] = null;
                //}
                if (string.IsNullOrWhiteSpace(Convert.ToString(Session["Compcompid"])))
                {
                    hdnCompetenceEnableVal.Text = "0";
                }
                if (string.IsNullOrWhiteSpace(Convert.ToString(Session["Pointcompid"])))
                {
                    hdnPointEnableVal.Text = "0";
                }
                if (string.IsNullOrWhiteSpace(Convert.ToString(Session["Quescompid"])))
                {
                    hdnQuestionEnableVal.Text = "0";
                    // questionPanel.Visible = false;
                }
                if (string.IsNullOrWhiteSpace(Convert.ToString(Session["Aemcompid"])))
                {
                    hdnActivityVal.Text = "0";
                    Button1.Visible = false;
                    Button5.Visible = false;
                }
                if (!String.IsNullOrEmpty(Request.QueryString["notif"]))
                {
                    var pdphistory = db.GetPDPHistoryByNotification(id, userId, Convert.ToInt32(Session["OrguserType"]), Convert.ToInt32(Request.QueryString["notif"])).FirstOrDefault();
                    if (pdphistory != null)
                    {
                        hdnPDPHistoryIDNotification.Value = Convert.ToString(pdphistory.Value);
                    }
                    UpdateNotification();
                }
                Personaldevelopment.InnerHtml = GetLocalResourceObject("Welcome.Text").ToString() + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
                ResLangId = Convert.ToInt32(Session["resLangID"]);





                hdnLoginUserID.Value = Convert.ToString(loginuserId);


                var img = db.UserMasters.FirstOrDefault(o => o.userId == id);
                try
                {

                    userImg.ImageUrl = "../Log/upload/Userimage/" + img.userImage;
                }
                catch (Exception)
                {

                    Response.Redirect("login.aspx");
                }




                UserBM objAtt = new UserBM();
                objAtt.userIsActive = true;
                objAtt.userIsDeleted = false;
                objAtt.userId = Convert.ToInt32(id);
                objAtt.userCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
                objAtt.GetEmployeeDetailbyPdf();

                //----------------------------------------------if else------------------------------------------------
                DataSet ds = objAtt.ds;
                DataTable dt = ds.Tables[0];
                DataTable dt1 = ds.Tables[1];
                try
                {
                    Lblcom.Text = Convert.ToString(ds.Tables[0].Rows[0]["comname"]);
                }
                catch (Exception)
                {

                    Response.Redirect("login.aspx");
                }
                Lblcom.Text = Convert.ToString(ds.Tables[0].Rows[0]["comname"]);
                Lbldiv.Text = Convert.ToString(ds.Tables[0].Rows[0]["divName"]);
                LdlEmpna.Text = Convert.ToString(ds.Tables[0].Rows[0]["name"]);
                LblJob.Text = Convert.ToString(ds.Tables[0].Rows[0]["jobName"]);
                lblTeamName.Text = Convert.ToString(ds.Tables[0].Rows[0]["TeamName"]);

                LblEmail.Text = Convert.ToString(ds.Tables[0].Rows[0]["userEmail"]);
                Lblpnum.Text = Convert.ToString(ds.Tables[0].Rows[0]["usercontact"]);
                if ((dt.Rows.Count > 0))
                {

                    LblAlp.Text = Convert.ToString(ds.Tables[0].Rows[0]["Apl"]);


                    LblPpl.Text = Convert.ToString(ds.Tables[0].Rows[0]["Ppl"]);


                    Lblallpoint.Text = Convert.ToString(ds.Tables[0].Rows[0]["allpoint"]);


                }

                if ((dt1.Rows.Count > 0))
                {

                    LblPDPTotal.Text = Convert.ToString(ds.Tables[1].Rows[0]["PDPTotal"]);

                }

                ClientScript.RegisterStartupScript(this.GetType(), "", "LoadChartFirstTime();", true);
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "", "LoadChartFirstTime();", true);

                //string pdp = Convert.ToString(ds.Tables[1].Rows[0]["pdp"]);
                //VoteUpOff.Attributes["src"] = ResolveUrl("\"" + pdp + "\"style=\"width:950px;height:270px;\"/");


                GetAproveDetails();
                GetActivityCategory();
                getalldt();
                GetAllpointSummery();
                //   ------------------------------------------------------------------approved
                //  var db = new startetkuEntities1();
                //   db.PDPHistories.FirstOrDefault(o => o.PdpManagerID==Convert.ToInt32(Session["OrgUserId"]));








                //   ------------------------------------------------------------------------------------------------------------------

                String Competence = "";
                if (ds.Tables[1].Rows.Count > 0)
                {
                    if (Convert.ToString(Session["Culture"]) == "Danish")
                    {
                        ds.Tables[1].Columns["comCompetence"].ColumnName = "abcd";
                        ds.Tables[1].Columns["comCompetenceDN"].ColumnName = "comCompetence";

                    }
                    bool flag = false;
                    string Basic = GetLocalResourceObject("Basic.Text").ToString();
                    string Actual = GetLocalResourceObject("Actual.Text").ToString();

                    string Target = GetLocalResourceObject("Target.Text").ToString();
                    string path = "<a href=>";

                    Competence = "<table width='100%' border='0' style='font-size:22px;'>";
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        Competence = Competence + "<tr style='font-size:22px;font-weight:bold;font-family:Verdana, Arial, Helvetica, sans-serif; color: #fcfcfc; background: #038ff5;'> <th>&nbsp;</th><th><center>" + Convert.ToString(Basic) + "</center></th><th><center>" + Convert.ToString(Actual) + "</center></th><th><center>" + Convert.ToString(Target) + "</center></th></tr>";
                    }
                    lblcompetenceDevelopment.Text = "<table style='width:100%;border-collapse: collapse;'>";
                    lblcompetenceDevelopmentPoint.Text = "<table style='width:100%;border-collapse: collapse;'><tr><td colspan='4' style='border: solid #000000 1px;'><span style='font-size:Larger;font-weight:bold;'><h3>" + GetLocalResourceObject("CompetenceOverview.Text").ToString() + "</h3></span></td></tr>";
                    lblcompetenceDevelopmentPoint.Text = lblcompetenceDevelopmentPoint.Text + "<tr><td style='border: solid #000000 1px;'>&nbsp;</td><td style='text-align:center;border: solid #000000 1px;'>" + Basic + "</td><td style='text-align:center;border: solid #000000 1px;'>" + Actual + "</td><td style='text-align:center;border: solid #000000 1px;'>" + Target + "</td></tr>";
                    string jsFunction = "";
                    try
                    {
                        for (int i = 0; i <= ds.Tables[1].Rows.Count - 1; i++)
                        {
                            int skillAchive = 0;
                            int skilltarget = 0;
                            int skillLocal = 0;
                            try
                            {
                                skillAchive = Convert.ToInt32(ds.Tables[1].Rows[i]["skillAchive"]);
                                skilltarget = Convert.ToInt32(ds.Tables[1].Rows[i]["skilltarget"]);
                                skillLocal = Convert.ToInt32(ds.Tables[1].Rows[i]["skillLocal"]);
                            }
                            catch (Exception)
                            {
                            }

                            if (skillAchive < skilltarget)
                            {

                                var compDatamy = GetCompDesc(Convert.ToInt32(ds.Tables[1].Rows[i]["comId"]), Convert.ToInt32(skillAchive));
                                string tmpName1 = "";
                                string tmpName = "";
                                if (Convert.ToString(Session["Culture"]) == "English")
                                {
                                    tmpName = compDatamy.FirstOrDefault().comchildlevel;
                                }
                                else
                                {
                                    tmpName = compDatamy.FirstOrDefault().comchildlevelDN;
                                }
                                if ((skillAchive) != 5)
                                {
                                    var compDatamy1 = GetCompDesc(Convert.ToInt32(ds.Tables[1].Rows[i]["comId"]), Convert.ToInt32(skillAchive) + 1);

                                    if (Convert.ToString(Session["Culture"]) == "English")
                                    {
                                        tmpName1 = compDatamy.FirstOrDefault().comchildlevel;
                                    }
                                    else
                                    {
                                        tmpName1 = compDatamy.FirstOrDefault().comchildlevelDN;
                                    }
                                }
                                //lblcompetenceDevelopment.Text = lblcompetenceDevelopment.Text + "<h2 class=\"step_titel\" style=\"z-index: 0 !important; font-size: 18px;\"><span>" + GetLocalResourceObject("CurrentCompetencelevelDescription.Text") +
                                //    "</span>:<span>" + Convert.ToString(ds.Tables[1].Rows[i]["comCompetence"]) + "</span></h2><br/>" + tmpName + "<br/>" +
                                //   "<h2 class=\"step_titel\" style=\"z-index: 0 !important; font-size: 18px;\"><span>" + GetLocalResourceObject("NewCompetencelevelDescription.Text") +
                                //  "</span>:<span>" + Convert.ToString(ds.Tables[1].Rows[i]["comCompetence"]) + "</span></h2><br/>" + tmpName1 + "<br/>";

                                lblcompetenceDevelopment.Text = lblcompetenceDevelopment.Text + "<tr><td colspan='2' style='border: solid #000000 1px;'>" + Convert.ToString(ds.Tables[1].Rows[i]["comCompetence"]) + "</td></tr><tr>";
                                lblcompetenceDevelopment.Text = lblcompetenceDevelopment.Text + "<td style='border: solid #000000 1px;'>" + GetLocalResourceObject("Actuallevel.Text").ToString() + ":" + Convert.ToString(skillAchive) + "</td>";
                                lblcompetenceDevelopment.Text = lblcompetenceDevelopment.Text + "<td style='border: solid #000000 1px;'>" + GetLocalResourceObject("Targetlevel.Text").ToString() + ":" + Convert.ToString(skilltarget) + "</td>";
                                lblcompetenceDevelopment.Text = lblcompetenceDevelopment.Text + "</tr><tr>";
                                string tmpname_t = tmpName;
                                string tmpname_t2 = tmpName1;
                                tmpname_t = tmpname_t.Replace("<p>\r\n\t", "");
                                tmpname_t2 = tmpname_t2.Replace("<p>\r\n\t", "");
                                tmpname_t = tmpname_t.Replace("<br />\r\n\t&nbsp;</p>\r\n", "");
                                tmpname_t2 = tmpname_t2.Replace("<br />\r\n\t&nbsp;</p>\r\n", "");

                                Common.WriteLog("t1 :" + tmpName);
                                Common.WriteLog("t2 :" + tmpName1);

                                lblcompetenceDevelopment.Text = lblcompetenceDevelopment.Text + "<td style='border: solid #000000 1px; width:250px;word-wrap: break-word;'>" + tmpname_t + "</td>";
                                lblcompetenceDevelopment.Text = lblcompetenceDevelopment.Text + "<td style='border: solid #000000 1px;width:250px;word-wrap: break-word;'>" + tmpname_t2 + "</td>";
                                lblcompetenceDevelopment.Text = lblcompetenceDevelopment.Text + "</tr>";


                                lblcompetenceDevelopmentPoint.Text = lblcompetenceDevelopmentPoint.Text + "<tr style='font-size:16px;font-weight:bold;font-family:Verdana, Arial, Helvetica, sans-serif; color: black; background-color: white;'><td style='border: solid #000000 1px;'>" + Convert.ToString(ds.Tables[1].Rows[i]["comCompetence"]) + "</td>";
                                lblcompetenceDevelopmentPoint.Text = lblcompetenceDevelopmentPoint.Text + "<td style='text-align:center;border: solid #000000 1px;'>" + Convert.ToString(skillLocal) + "</td>";
                                lblcompetenceDevelopmentPoint.Text = lblcompetenceDevelopmentPoint.Text + "<td style='text-align:center;border: solid #000000 1px;'>" + Convert.ToString(skillAchive) + "</td>";
                                lblcompetenceDevelopmentPoint.Text = lblcompetenceDevelopmentPoint.Text + "<td style='text-align:center;border: solid #000000 1px;'>" + Convert.ToString(skilltarget) + "</td>";
                                lblcompetenceDevelopmentPoint.Text = lblcompetenceDevelopmentPoint.Text + "</tr>";

                                if (flag == false)
                                {
                                    jsFunction = @"GetCompDesc_FirstTime(" + ds.Tables[1].Rows[i]["comId"] + "," + Convert.ToString(skillAchive) + ",this);";
                                    Label6.Text = Convert.ToString(ds.Tables[1].Rows[i]["comCompetence"]);
                                    Label7.Text = Convert.ToString(ds.Tables[1].Rows[i]["comCompetence"]);
                                    flag = true;

                                    Competence = Competence + "<tr style='font-size:16px;font-weight:bold;font-family:Verdana, Arial, Helvetica, sans-serif; color: black; background-color: lightyellow;'>"
                                        + "<td style=''>" + "<a href='#' onclick='GetCompDesc(" + ds.Tables[1].Rows[i]["comId"] + "," + Convert.ToString(skillAchive) + ",this);'>" + Convert.ToString(ds.Tables[1].Rows[i]["comCompetence"]) + "</td>"
                                        + "<td style='text-align:center;'>" + Convert.ToString(skillLocal) + "</td>"
                                        + "<td style='text-align:center;'>" + Convert.ToString(skillAchive)
                                             + "</td><td style='text-align:center;'>"
                                              + Convert.ToString(skilltarget) + "</a></td></tr>";
                                }
                                else
                                {
                                    Competence = Competence + "<tr style='font-size:16px;font-weight:bold;font-family:Verdana, Arial, Helvetica, sans-serif; color: black; background-color: white;'>"
                                      + "<td style=''>" + "<a href='#' onclick='GetCompDesc(" + ds.Tables[1].Rows[i]["comId"] + "," + Convert.ToString(skillAchive) + ",this);'>" + Convert.ToString(ds.Tables[1].Rows[i]["comCompetence"]) + "</td>"
                                      + "<td style='text-align:center;'>" + Convert.ToString(skillLocal) + "</td>"
                                      + "<td style='text-align:center;'>" + Convert.ToString(skillAchive)
                                           + "</td><td style='text-align:center;'>"
                                            + Convert.ToString(skilltarget) + "</a></td></tr>";
                                }
                            }
                            //Competence = Competence + "<tr> <td>&nbsp;</td><td>"+ Convert.ToString(Session["hdnBasic"])+"</td><td>"+Convert.ToString(Session["hdnActual"])+"</td><td>"+Convert.ToString(Session["hdnTarget"])+"</td></tr>";
                            else
                            {

                                lblcompetenceDevelopmentPoint.Text = lblcompetenceDevelopmentPoint.Text + "<tr><td style='border: solid #000000 1px;'>" + Convert.ToString(ds.Tables[1].Rows[i]["comCompetence"]) + "</td>";
                                lblcompetenceDevelopmentPoint.Text = lblcompetenceDevelopmentPoint.Text + "<td style='text-align:center;border: solid #000000 1px;'>" + Convert.ToString(skillLocal) + "</td>";
                                lblcompetenceDevelopmentPoint.Text = lblcompetenceDevelopmentPoint.Text + "<td style='text-align:center;border: solid #000000 1px;'>" + Convert.ToString(skillAchive) + "</td>";
                                lblcompetenceDevelopmentPoint.Text = lblcompetenceDevelopmentPoint.Text + "<td style='text-align:center;border: solid #000000 1px;'>" + Convert.ToString(skilltarget) + "</td>";
                                lblcompetenceDevelopmentPoint.Text = lblcompetenceDevelopmentPoint.Text + "</tr>";

                                Competence = Competence + "<tr style='font-size:16px;font-family:Verdana, Arial, Helvetica, sans-serif; color: black; background-color: white;'>"
                                    + " <td style=''>" + "<a href='#' onclick='GetCompDesc(" + ds.Tables[1].Rows[i]["comId"] + "," + Convert.ToString(skillAchive) + ",this); ' >" + Convert.ToString(ds.Tables[1].Rows[i]["comCompetence"]) + "</td>"
                                    + "<td style='text-align:center;'>" + Convert.ToString(skillLocal) + "</td>"
                                    + "<td style='text-align:center;'>" + Convert.ToString(skillAchive)
                                         + "</td><td style='text-align:center;'>"
                                          + Convert.ToString(skilltarget) + "</a></td></tr>";

                            }


                        }
                        if (flag == false && ds.Tables[1].Rows.Count > 0)
                        {
                            int skillAchive1 = Convert.ToInt32(ds.Tables[1].Rows[0]["skillAchive"]);
                            jsFunction = @"GetCompDesc_FirstTime(" + ds.Tables[1].Rows[0]["comId"] + "," + Convert.ToString(skillAchive1) + ",this);";
                        }

                    }


                    catch (Exception ex)
                    {

                        ExceptionLogger.LogException(ex);
                    }
                    lblcompetenceDevelopment.Text = lblcompetenceDevelopment.Text + "</table>";
                    lblcompetenceDevelopmentPoint.Text = lblcompetenceDevelopmentPoint.Text + "</table>";
                    test.InnerHtml = Competence + "</table>";
                    ClientScript.RegisterStartupScript(this.GetType(), "ModelPopup", jsFunction, true);
                    #region Comment
                    //string strmail = "";
                    //var path = ConfigurationManager.AppSettings["Htmlpath"].ToString() + "template/Report.html";
                    //strmail = System.IO.File.ReadAllText(path);
                    //strmail = strmail.Replace("##logo##", "<img src=\"" + ConfigurationManager.AppSettings["Htmlpath"].ToString() + "/upload/Userimage/LoginLogo.png" + "\"style=\"width:237px;height:62px;\"/>");
                    //strmail = strmail.Replace("###CompCatName###", Convert.ToString(Session["hdnCompCatNaME"]));
                    //strmail = strmail.Re  ("###ApproveBy###", Convert.ToString(Session["hdnreportApproveBy"]));
                    //strmail = strmail.Replace("##reportGraphdesc##", Convert.ToString(Session["hdnReportGraph"]));
                    //strmail = strmail.Replace("###Conclusion###", " ");
                    //strmail = strmail.Replace("##imgchartpdp##", "");
                    //strmail = strmail.Replace("##pdphead##", Convert.ToString(Session["hdnPdpChart"]));
                    //strmail = strmail.Replace("##ALPhead##", Convert.ToString(Session["hdnAlpPlpChart"]));

                    //String PdfReportDetail = CommonModule.getTemplatebyname("PdfReport", 0);
                    //PdfReportDetail = PdfReportDetail.Replace(" ###name###", Convert.ToString(ds.Tables[0].Rows[0]["name"]));

                    //String PdfReportPurpose = CommonModule.getTemplatebyname("Report PDP Purpose", 0);
                    //strmail = strmail.Replace("###OwnerReport###", PdfReportDetail);
                    //strmail = strmail.Replace("###ReportPurpose###", PdfReportPurpose);
                    //strmail = strmail.Replace("##Name##", Convert.ToString(ds.Tables[0].Rows[0]["name"]));
                    //strmail = strmail.Replace("##Jobtitle##", Convert.ToString(ds.Tables[0].Rows[0]["jobName"]));
                    //strmail = strmail.Replace("##Division##", Convert.ToString(ds.Tables[0].Rows[0]["divName"]));
                    //strmail = strmail.Replace("##Companyname##", Convert.ToString(ds.Tables[0].Rows[0]["comname"]));
                    //strmail = strmail.Replace("##Email##", Convert.ToString(ds.Tables[0].Rows[0]["userEmail"]));
                    //strmail = strmail.Replace("##Phonenumber##", Convert.ToString(ds.Tables[0].Rows[0]["usercontact"]));
                    //strmail = strmail.Replace("##ALP##", Convert.ToString(ds.Tables[0].Rows[0]["Apl"]));
                    //strmail = strmail.Replace("##PLP##", Convert.ToString(ds.Tables[0].Rows[0]["Ppl"]));
                    //strmail = strmail.Replace("##Total##", Convert.ToString(ds.Tables[0].Rows[0]["allpoint"]));
                    //strmail = strmail.Replace("##PDP##", Convert.ToString(ds.Tables[1].Rows[0]["PDPTotal"]));
                    //strmail = strmail.Replace("##Date##", DateTime.Now.ToString("dd/MM/yy"));
                    //strmail = strmail.Replace("##Managername##", Convert.ToString(Session["OrgUserName"]));

                    //String Content = strmail;
                    //string[] CntArr = Content.Split('`');

                    //Winnovative.PdfCreator.Document document = new Winnovative.PdfCreator.Document();
                    //Winnovative.PdfCreator.PdfPage page = document.Pages.AddNewPage(Winnovative.PdfCreator.PageSize.A3, new Margins(3), PageOrientation.Portrait);
                    //HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(10, 10, 830, CntArr[0], null);
                    //htmlToPdfElement.ActiveXEnabledInImage = true;
                    //htmlToPdfElement.AvoidImageBreak = true;
                    //htmlToPdfElement.AvoidTextBreak = true;
                    //htmlToPdfElement.FitWidth = true;
                    //htmlToPdfElement.StretchToFit = true;
                    //ExplicitDestination pageDestination = new ExplicitDestination(page);
                    //pageDestination.ZoomPercentage = 100;
                    //pageDestination.DestPage = page;
                    //AddElementResult addResult;
                    //addResult = page.AddElement(htmlToPdfElement);
                    //page.Orientation = PageOrientation.Portrait;

                    //if (CntArr.Length > 1)
                    //{
                    //    Winnovative.PdfCreator.PdfPage page1 = document.Pages.AddNewPage(Winnovative.PdfCreator.PageSize.A3, new Margins(3), PageOrientation.Portrait);
                    //    HtmlToPdfElement htmlToPdfElement1 = new HtmlToPdfElement(10, 10, 830, CntArr[1], null);
                    //    htmlToPdfElement1.ActiveXEnabledInImage = true;
                    //    htmlToPdfElement1.AvoidImageBreak = true;
                    //    htmlToPdfElement1.AvoidTextBreak = true;
                    //    htmlToPdfElement1.FitWidth = true;
                    //    htmlToPdfElement1.StretchToFit = true;
                    //    ExplicitDestination pageDestination1 = new ExplicitDestination(page1);
                    //    pageDestination.ZoomPercentage = 100;
                    //    pageDestination.DestPage = page1;
                    //    AddElementResult addResult1;
                    //    addResult1 = page1.AddElement(htmlToPdfElement1);
                    //    page1.Orientation = PageOrientation.Portrait;
                    //}

                    //document.CompressionLevel = CompressionLevel.BestCompression;
                    //// send the generated PDF document to client browser
                    //string PdfName = DateTime.Now.ToString("ddMMyyyyhhmmmss");

                    //string path1 = ConfigurationManager.AppSettings["Htmlpath"].ToString() + "html/" + PdfName + ".pdf";












                    //if (Request.QueryString["sel"] != null)
                    //{



                    //    var getdata = db.PDPHistories.Where(o => o.PdpUserID == id && (o.Pdpapprovman != null && o.Pdpapprovemp != null) || (o.Pdpapprovcom != null && o.Pdpapprovman != null)).ToList(); //.OrderByDescending(o.PdpHisDate)
                    //    if (getdata != null)
                    //    {
                    //        // var getdata12 = db.PDPHistories.Where(o => o.PdpUserID == id && o.PpdHisIsActive==true).ToList();

                    //        // int iw = getdata12.Count();

                    //        // string newdd = getdata12[iw - 1].PdpHisID.ToString();

                    //        //ddActCate.ClearSelection(); //making sure the previous selection has been cleared
                    //        //ddActCate.Items.FindByText(newdd).Selected = true;

                    //        // ddActCate.SelectedIndex =1;

                    //        //save.Visible = false;

                    //    }

                    //}
                    #endregion

                }



            }
            ApproveButtonVisible(Convert.ToInt32(type), id);
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "Organisation_PersonalDevelopmentPlan->Page_Load");
        }
    }
    //
    public void ApproveButtonVisible(int type, int id)
    {
        var db = new startetkuEntities1();

        var getdata11 = db.PDPHistories.OrderByDescending(o => o.PdpHisID).FirstOrDefault(o => o.PdpUserID == id && o.PpdHisIsActive == false);
        var getdata112 = db.PDPHistories.OrderByDescending(o => o.PdpHisID).FirstOrDefault(o => o.PdpUserID == id);
        //Common.WriteLog("type :" + Convert.ToString(type));
        //Common.WriteLog("id :" + Convert.ToString(id));
        //Common.WriteLog("hdnPDPHistoryID :" + Convert.ToString(hdnPDPHistoryID.Value));
        if (hdnPDPHistoryID.Value != "0")
        {
            EmpAppText empapptextobj = new EmpAppText();
            if (getdata112 != null)
            {

                if (type == 3 && (getdata112.manaptime != null || getdata112.empaptime != null || getdata112.comaptime != null))
                {
                    if (getdata112.Pdpapprovemp != null && getdata112.PdpManagerID > 0)
                    {
                        var lowerAuthDetails = UserBM.GetUserByIdLinq(Convert.ToInt32(getdata112.PdpUserID));
                        empapptextobj.lowerAuthDetails = lowerAuthDetails.userFirstName + " " + lowerAuthDetails.userLastName + " ";
                        empapptextobj.empaptime = CommonUtilities.GetCurrentDateTime().ToString("dd-MM-yyyy HH:mm:ss").Replace("-", "/");
                        Label4.Visible = true;
                    }

                    if (getdata112.PdpManagerID > 0)
                    {
                        var highAuthDetails = UserBM.GetUserByIdLinq(Convert.ToInt32(getdata112.PdpManagerID));
                        empapptextobj.highAuthDetails = highAuthDetails.userFirstName + " " + highAuthDetails.userLastName + " ";
                        empapptextobj.manaptime = (getdata112.manaptime).ToString();
                    }
                }
                else if (type == 2 && (getdata112.manaptime != null || getdata112.empaptime != null || getdata112.comaptime != null))
                {
                    if (getdata112.Pdpapprovman != null && getdata112.PdpManagerID > 0)
                    {
                        var lowerAuthDetails = UserBM.GetUserByIdLinq(Convert.ToInt32(getdata112.PdpUserID));
                        empapptextobj.lowerAuthDetails = lowerAuthDetails.userFirstName + " " + lowerAuthDetails.userLastName + " ";
                        empapptextobj.empaptime = CommonUtilities.GetCurrentDateTime().ToString("dd-MM-yyyy HH:mm:ss").Replace("-", "/");
                        Label4.Visible = true;

                        if (getdata112.PdpManagerID > 0)
                        {
                            var highAuthDetails = UserBM.GetUserByIdLinq(Convert.ToInt32(getdata112.PdpCompanyID));
                            empapptextobj.highAuthDetails = highAuthDetails.userFirstName + " " + highAuthDetails.userLastName + " ";
                            empapptextobj.manaptime = (getdata112.comaptime).ToString();
                        }

                    }
                    else if (getdata112.PdpManagerID > 0)
                    {
                        var highAuthDetails = UserBM.GetUserByIdLinq(Convert.ToInt32(getdata112.PdpCompanyID));
                        empapptextobj.highAuthDetails = highAuthDetails.userFirstName + " " + highAuthDetails.userLastName + " ";
                        //empapptextobj.comaptime = (getdata112.comaptime).ToString();
                        empapptextobj.manaptime = (getdata112.comaptime).ToString();
                        btnManap.Visible = false;
                        Label3.Visible = true;
                    }
                }
            }

            lblHigherAuthName.Text = empapptextobj.highAuthDetails;
            lblLowerAuthName.Text = empapptextobj.lowerAuthDetails;
            lowdate.Text = empapptextobj.empaptime;
            hidate.Text = empapptextobj.manaptime;
        }

        if (hdnPDPHistoryID.Value != "0")
        {
            // SaveHtmlDataUpdate_New(Convert.ToInt32( hdnPDPHistoryID.Value));
            ClientScript.RegisterStartupScript(this.GetType(), "", "SaveAgainHtmlData(" + Convert.ToInt32(hdnPDPHistoryID.Value) + ");", true);
            hdnPDPHistoryID.Value = "0";
            getalldt();
        }


        bool gt;
        //  if (getdata11.PpdHisIsActive == false)
        {
            if (getdata11 != null)
            {
                if ((getdata11.Pdpapprovman != null && getdata11.Pdpapprovemp != null) || (getdata11.Pdpapprovcom != null && getdata11.Pdpapprovman != null))
                {
                    gt = true;
                }
                else
                {
                    gt = false;
                }

            }
            else
            {
                gt = false;
            }

            if (type == 3)//
            {
                //  var getdata = db.PDPHistories.Where(o => o.PdpUserID == id && o.PpdHisIsActive == false).ToList().OrderByDescending(o => o.PdpHisID);
                var getdata = db.PDPHistories.OrderByDescending(o => o.PdpHisID).FirstOrDefault(o => o.PdpUserID == id && o.PpdHisIsActive == false);
                if (getdata == null || gt == true)//
                {
                    if (Convert.ToInt32(Session["OrguserType"]) == 2)
                    {
                        //Label5.Visible = true;
                        Label3.Visible = true;

                        btnManap.Visible = true;
                        Label14.Visible = true;
                        Button5.Visible = true;
                    }
                    else
                    {
                        // Label3.Visible = true;
                        btnManap.Visible = false;
                        // Label14.Visible = false;
                        // Label3.Visible = false;
                    }
                    //if (Convert.ToInt32(Session["OrguserType"]) == 3)
                    //{
                    //    btnEmpap.Visible = true;
                    //}
                }

                bool checkManIsnull;
                if (getdata != null)
                {
                    if (getdata.Pdpapprovman != null) // ja db ma entry mali and company e approve karelu to true
                    {
                        checkManIsnull = true;
                    }
                    else
                    {

                        checkManIsnull = false;

                    }
                }
                else
                {

                    checkManIsnull = false;

                }
                //var checkManIsnull = getdata.Where(o => o.Pdpapprovman != null).ToList();

                if (checkManIsnull == false)
                {
                    if (Convert.ToInt32(Session["OrguserType"]) == 2)
                    {
                        //Label5.Visible = true;
                        Label3.Visible = true;
                        btnManap.Visible = true;
                        Label14.Visible = true;
                        Button5.Visible = true;
                    }
                    else
                    {
                        btnManap.Visible = false;
                    }
                }
                else
                {
                    Label3.Visible = true;
                    btnManap.Visible = false;
                }

                bool checkEmpIsnull;
                if (getdata != null)
                {
                    if (getdata.Pdpapprovemp != null) // jo manager e approve karelu to true
                    {
                        checkEmpIsnull = true;
                    }
                    else
                    {

                        checkEmpIsnull = false;
                    }
                }
                else
                {

                    checkEmpIsnull = false;
                }

                bool checkEmpIsnull1;
                if (getdata != null)
                {
                    if (getdata.Pdpapprovman != null) // jo company e approve karelu to true
                    {
                        checkEmpIsnull1 = true;
                    }
                    else
                    {

                        checkEmpIsnull1 = false;
                    }
                }
                else
                {

                    checkEmpIsnull1 = false;
                }



                //var checkEmpIsnull = getdata.Where(o => o.Pdpapprovemp != null).ToList();
                //var checkEmpIsnull1 = getdata.Where(o => o.Pdpapprovman != null).ToList();
                if (checkEmpIsnull == false)
                {
                    if (Convert.ToInt32(Session["OrguserType"]) == 3 && checkEmpIsnull1 == true)
                    {
                        //Label5.Visible = true;
                        Label3.Visible = true;
                        Label4.Visible = true;
                        btnEmpap.Visible = true;
                        Label14.Visible = true;
                        Button5.Visible = false;

                    }
                    else
                    {
                        btnEmpap.Visible = false;
                        //Label4.Visible = false;
                    }

                }
                else
                {

                    btnEmpap.Visible = false;
                    // Label4.Visible = false;
                }

            }
            else if (type == 2)//  company and manager nu combination 
            {
                // Common.WriteLog("PDPHistories 4 :" + Convert.ToString(type));
                var getdata = db.PDPHistories.OrderByDescending(o => o.PdpHisID).FirstOrDefault(o => o.PdpUserID == id && o.PpdHisIsActive == false);
                // Common.WriteLog("PDPHistories 5 :" + Convert.ToString(getdata.PdpHisID));
                if (getdata == null || gt == true)  // dbma koi entry nai or bane e approve karelu to 
                {
                    // Common.WriteLog("PDPHistories 6 :" + Convert.ToString(Session["OrguserType"]));
                    if (Convert.ToInt32(Session["OrguserType"]) == 1)
                    {
                        //Label5.Visible = true;
                        Label3.Visible = true;
                        btnManap.Visible = true;
                        Label14.Visible = true;
                        Button5.Visible = true;
                    }
                    else
                    {
                        Label3.Visible = true;
                        btnManap.Visible = false;
                    }
                    //if (Convert.ToInt32(Session["OrguserType"]) == 2)
                    //{
                    //    btnEmpap.Visible = true;
                    //}
                }
                //else
                //{
                //    btnManap.Visible = false;
                //}

                bool checkManIsnull;
                if (getdata != null)
                {
                    if (getdata.Pdpapprovcom != null) // ja db ma entry mali and company e approve karelu to true
                    {
                        checkManIsnull = true;
                    }
                    else
                    {

                        checkManIsnull = false;

                    }
                }
                else
                {

                    checkManIsnull = false;

                }

                // var checkManIsnull = getdata.Where(o => o.Pdpapprovcom != null).ToList().OrderByDescending(o => o.PdpHisID);
                //   Common.WriteLog("checkManIsnull 6 :" + Convert.ToString(checkManIsnull));
                if (checkManIsnull == false)//db ma entry mali and jo approve na karelu to button visible
                {
                    if (Convert.ToInt32(Session["OrguserType"]) == 1)
                    {
                        //Label5.Visible = true;
                        Label3.Visible = true;
                        btnManap.Visible = true;
                        Label14.Visible = true;
                        Button5.Visible = true;
                    }
                    else
                    {
                        Label3.Visible = true;
                        btnManap.Visible = false;
                    }
                }
                else
                {
                    Label3.Visible = true;
                    btnManap.Visible = false;
                }

                bool checkEmpIsnull;
                if (getdata != null)
                {
                    if (getdata.Pdpapprovman != null) // jo manager e approve karelu to true
                    {
                        checkEmpIsnull = true;
                    }
                    else
                    {

                        checkEmpIsnull = false;
                    }
                }
                else
                {

                    checkEmpIsnull = false;
                }

                bool checkEmpIsnull1;
                if (getdata != null)
                {
                    if (getdata.Pdpapprovcom != null) // jo company e approve karelu to true
                    {
                        checkEmpIsnull1 = true;
                    }
                    else
                    {

                        checkEmpIsnull1 = false;
                    }
                }
                else
                {

                    checkEmpIsnull1 = false;
                }

                // var checkEmpIsnull = getdata.Where(o => o.Pdpapprovman != null).ToList().OrderByDescending(o => o.PdpHisID);
                //var checkEmpIsnull1 = getdata.Where(o => o.Pdpapprovcom != null).ToList().OrderByDescending(o => o.PdpHisID);

                if (checkEmpIsnull == false) //compy e approve karelu hovu joiye and pachi man e na karelu to visible 
                {
                    if (Convert.ToInt32(Session["OrguserType"]) == 2 && checkEmpIsnull1 == true)
                    {
                        //Label5.Visible = true;
                        Label3.Visible = true;
                        Label4.Visible = true;
                        btnEmpap.Visible = true;
                        Label14.Visible = true;
                        Button5.Visible = true;

                    }
                    else
                    {
                        btnEmpap.Visible = false;
                        //Label4.Visible = false;
                    }
                }
                else
                {
                    btnEmpap.Visible = false;
                    //  Label4.Visible = false;

                }




            }



        }
    }
    protected void gvGridLog_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {


            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lbl = (Label)e.Row.FindControl("lblDescriptionLog");

                Label logPointInfo = (Label)e.Row.FindControl("lblUNamer");

                HiddenField lblinfo = (HiddenField)e.Row.FindControl("lbllogpointinfo");
                HiddenField lblFromName = (HiddenField)e.Row.FindControl("hdnlblFromname");
                HiddenField lblToName = (HiddenField)e.Row.FindControl("hdnlblToname");
                HiddenField lbllongDesc = (HiddenField)e.Row.FindControl("hdnlbllogDescription");
                HiddenField lblrdDoc = (HiddenField)e.Row.FindControl("hdnlblrdDoc");
                HiddenField lbldocFileName_Friendly = (HiddenField)e.Row.FindControl("hdnlbldocFileName_Friendly");
                HiddenField lbldocFileName_System = (HiddenField)e.Row.FindControl("hdnlbldocFileName_System");
                HyperLink linkDocOpen = (HyperLink)e.Row.FindControl("linkDocumentOpen");

                linkDocOpen.Visible = false;
                if (lblinfo.Value == "AcceptProvidedHelp")
                {
                    logPointInfo.Text = GetLocalResourceObject("AcceptProvidedHelp.Text").ToString();
                    lbl.Text = lblFromName.Value + " " + GetLocalResourceObject("AcceptHelpFrom.Text").ToString() + " " + lblToName.Value;
                }
                else if (lblinfo.Value == "RequestSession")
                {
                    logPointInfo.Text = GetLocalResourceObject("RequestSession.Text").ToString();
                    lbl.Text = lblFromName.Value + " " + GetLocalResourceObject("TakeHelpFrom.Text").ToString() + " " + lblToName.Value;
                }
                else if (lblinfo.Value == "ReadDocByOther")
                {
                    logPointInfo.Text = GetLocalResourceObject("ReadDocByOther.Text").ToString();
                    lbl.Text = lblFromName.Value + " " + GetLocalResourceObject("ProvideHelpRequest.Text").ToString() + " " + lblToName.Value;
                }
                else if (lblinfo.Value == "SuggestSession")
                {
                    logPointInfo.Text = GetLocalResourceObject("SuggestSession.Text").ToString();
                    lbl.Text = lblFromName.Value + " " + GetLocalResourceObject("OfferHelp.Text").ToString() + " " + lblToName.Value;
                }
                else if (lblinfo.Value == "ReadDoc")
                {
                    logPointInfo.Text = GetLocalResourceObject("ReadDoc.Text").ToString();
                    lbl.Text = lblFromName.Value + " " + GetLocalResourceObject("ReadDownloadDoc.Text").ToString() + " " + lblrdDoc.Value;
                }
                else if (lblinfo.Value == "UploadPublicDoc")
                {
                    logPointInfo.Text = GetLocalResourceObject("UploadPublicDoc.Text").ToString();
                    linkDocOpen.Visible = true;
                    linkDocOpen.NavigateUrl = String.Format("{0}", ConfigurationSettings.AppSettings["siteurl"].ToString() + "Log/upload/Document/" + lbldocFileName_System.Value);
                    lbl.Text = lblFromName.Value + " " + GetLocalResourceObject("HasUploadDoc.Text").ToString() + " " + lbldocFileName_Friendly.Value;
                }
                else if (lblinfo.Value == "OpenCompetencesScreen")
                {
                    logPointInfo.Text = GetLocalResourceObject("OpenCompetencesScreen.Text").ToString();
                    lbl.Text = lblFromName.Value + " " + GetLocalResourceObject("OpenCompetencesScreen.Text").ToString();
                }
                else if (lblinfo.Value == "SignIn")
                {
                    logPointInfo.Text = GetLocalResourceObject("SignIn.Text").ToString();
                    lbl.Text = lblFromName.Value + " " + GetLocalResourceObject("SignIn.Text").ToString();// " sign in";
                }
                else if (lblinfo.Value == "FinalFillCompetences")
                {
                    logPointInfo.Text = GetLocalResourceObject("FinalFillCompetences.Text").ToString();
                    lbl.Text = lblFromName.Value + " " + GetLocalResourceObject("FinalFillCompetences.Text").ToString();//" fill final competence";
                }
                else
                {
                    logPointInfo.Text = lblinfo.Value;
                    lbl.Text = lbllongDesc.Value;
                }

            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "gvGridLog_RowDataBound");
        }

    }
    protected void reac(object sender, EventArgs e)
    {
        Response.Redirect("ActivityLists.aspx");
    }

    protected void OpenUploadDocument(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lblQuestionCat').click();", true);
    }
    protected void OpenAddMeeting(object sender, EventArgs e)
    {
        hdnQuesID.Value = "";
        txtdescription.Text = "";
        txtdescriptionDN.Text = "";
        txtsearch.Text = "";
        txtdate.Text = "";
        listresult.Items.Clear();
        listparticipants.Items.Clear();
        btnUpdate.Visible = false;
        btnsave.Visible = true;

        ClientScript.RegisterStartupScript(this.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lblMeeting').click();", true);
    }
    private static string CleanFileName(string fileName)
    {


        var s =
            fileName.Replace(' ', '_')
                .Replace('^', '_')
                .Replace('%', '_')
                .Replace('*', '_')
                .Replace('(', '_')
                .Replace(')', '_')
                .Replace('{', '_')
                .Replace('}', '_')
                .Replace('@', '_')
                .Replace('$', '_')
                .Replace('+', '_')
                .Replace('-', '_')
                .Replace('`', '_')
                .Replace('?', '_')
                .Replace('&', '_')
                .Replace('#', '_');

        return s;
    }
    protected void btnsubmitdoc_click(object sender, EventArgs e)
    {
        String filePath = "";

        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var LoginuserId = Convert.ToInt32(Session["OrgUserId"]);
        //var userId = Convert.ToInt32(hdnQueryUmID.Value);

        int umid = Convert.ToInt32(hdnQueryUmID.Value);
        var docAttachmentName = string.Empty;

        var userId = Convert.ToInt32(Session["OrgUserId"]);

        var docCompnyId = Convert.ToInt32(Session["OrgCompanyId"]);


        if (FileUpload1.HasFile)
        {
            filePath = System.DateTime.Now.ToString("MMddyyhhmmss") + "_" + FileUpload1.PostedFile.FileName;
            FileUpload1.SaveAs(Server.MapPath("../") + "Log/upload/Document/" +
                               System.IO.Path.GetFileName(CleanFileName(filePath)));
            docAttachmentName = System.IO.Path.GetFileName(CleanFileName(filePath));

        }
        var docFileNameInSystem = CleanFileName(docAttachmentName);
        docAttachmentName = CleanFileName(docAttachmentName);

        PersonalDevelopmentPlanBM obj = new PersonalDevelopmentPlanBM();
        obj.DocumentName = txtDocumentName.Text;
        obj.meetDocID = 0;
        obj.DocumentPath = docAttachmentName;
        obj.metCreateDate = DateTime.Now;
        obj.metUserId = umid;
        obj.metCompanyID = docCompnyId;
        obj.OrignalDocumentName = String.IsNullOrEmpty(FileUpload1.PostedFile.FileName) ? "" : FileUpload1.PostedFile.FileName;
        obj.InsertMeetingDocument();

        DataSet ds = new DataSet();
        ds = obj.ds;

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            var db = new startetkuEntities1();
            try
            {
                db.InsertNotification_Publish("MeetingDoc", companyId, LoginuserId, obj.meetDocID, "metPubDoc", "New Meeting Document Publish");

                db.InsertUpdateEmailData(docCompnyId, userId, Convert.ToString(ds.Tables[0].Rows[0]["metParticipantsId"]), obj.meetDocID, "MeetDoc", obj.OrignalDocumentName);
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex, 0, "PersonalDevelopmentPlan->btnsave_Click->btnsubmitdoc_click");
            }
        }

        gridMeetingDocumentBind();

        Response.Redirect("PersonalDevelopmentPlan.aspx?umId=" + umid);
        //  Response.Redirect(Request.Url.AbsolutePath);  
    }

    protected DataSet GetMeetingDocData()
    {
        PersonalDevelopmentPlanBM obj = new PersonalDevelopmentPlanBM();
        var userId = Convert.ToInt32(Session["OrgUserId"]);

        var docCompnyId = Convert.ToInt32(Session["OrgCompanyId"]);

        //obj.metUserId = userId;
        obj.metUserId = Convert.ToInt32(Session["QueryStringID"]);
        obj.metCompanyID = docCompnyId;

        obj.GetAllMeetingDocument();

        DataSet ds = obj.ds;

        return ds;
    }
    protected void gridMeetingDocumentBind()
    {

        DataSet ds = GetMeetingDocData();
        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                gridMeetingDocument.DataSource = ds.Tables[0];
                gridMeetingDocument.DataBind();

                gridMeetingDocument.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                DataTable dt = ds.Tables[0];
                DataRow dr = dt.NewRow();
                dr["CreateDate"] = "";
                dr["DocumentName"] = "";
                dr["meetDocID"] = "0";
                dr["DocumentPath"] = "";
                dt.Rows.Add(dr);

                gridMeetingDocument.DataSource = dt;
                gridMeetingDocument.DataBind();
            }
        }
        else
        {
            DataTable dt = new DataTable();
            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            gridMeetingDocument.DataSource = dt;
            gridMeetingDocument.DataBind();
        }
    }
    protected void gridMeetingDocument_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "archive")
        {
            PersonalDevelopmentPlanBM obj = new PersonalDevelopmentPlanBM();
            obj.metID = Convert.ToInt32(e.CommandArgument);

            obj.DeleteMeetingDocumentData();

            //DataSet ds = obj.ds;
            //if (ds != null)
            //{
            //    string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
            //    if (returnMsg == "success")
            //    {
            //        //GetAllElementList();
            //        lblMsg.Text = CommonModule.msgRecordArchiveSucces;
            //        lblMsg.ForeColor = System.Drawing.Color.White;
            //    }
            //    else
            //    {
            //        lblMsg.Text = CommonModule.msgSomeProblemOccure;
            //    }
            //}
            //else
            //{
            //    lblMsg.Text = CommonModule.msgSomeProblemOccure;
            //}
            gridMeetingDocumentBind();

        }

    }

    private void UpdateNotification()
    {
        var db = new startetkuEntities1();
        try
        {
            if (!String.IsNullOrEmpty(Request.QueryString["notif"]))
            {
                var notId = Convert.ToInt32(Request.QueryString["notif"]);
                var notObj = db.Notifications.FirstOrDefault(o => o.notId == notId);
                if (notObj != null)
                {
                    notObj.notIsActive = false;
                    db.SaveChanges();
                }
            }

        }
        catch (Exception)
        {

            throw;
        }
    }

    #region CompanyDirection
    protected void GetUserAssignDirectionList_PDP()
    {
        var id = Convert.ToInt32(Request.QueryString["umId"]);
        try
        {
            CompanyDirectionElement obj = new CompanyDirectionElement();
            obj.UserID = Convert.ToInt32(id);
            obj.dirCompanyID = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
            obj.Status = "Publish";
            obj.dirCreateDate = DateTime.Now;
            int ResLangId = Convert.ToInt32(HttpContext.Current.Session["resLangID"]);
            obj.dirIndex = ResLangId;
            DataSet ds = new DataSet();
            obj.GetAllUserDirectionList_PDP();
            ds = obj.ds;

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gridPublishDirectionElement.DataSource = ds.Tables[0];
                    gridPublishDirectionElement.DataBind();
                }
                else
                {
                    gridPublishDirectionElement.DataSource = null;
                    gridPublishDirectionElement.DataBind();
                }
            }
            else
            {
                gridPublishDirectionElement.DataSource = null;
                gridPublishDirectionElement.DataBind();
            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "GetUserAssignDirectionList_PDP");
        }
    }
    protected void gridPublishDirectionElement_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(Session["OrgUserId"]);
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField CatID = (HiddenField)e.Row.FindControl("hdngridPublishcatID");

                DataSet dst = new DataSet();
                CompanyDirectionElement obj = new CompanyDirectionElement();
                obj.UserID = Convert.ToInt32(Request.QueryString["umId"]);
                obj.dirCompanyID = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
                obj.Status = "Publish";
                obj.dirCreateDate = DateTime.Now;
                obj.dirCatID = Convert.ToString(CatID.Value);

                obj.GetUserAssignDirectionElementList_PDP();
                dst = obj.ds;

                GridView gvPublishElement_AssignList = e.Row.FindControl("gvPublishElement_AssignList") as GridView;

                if (dst != null)
                {
                    if (dst.Tables[0].Rows.Count > 0)
                    {
                        gvPublishElement_AssignList.DataSource = dst.Tables[0];
                        gvPublishElement_AssignList.DataBind();
                    }
                    else
                    {
                        gvPublishElement_AssignList.DataSource = null;
                        gvPublishElement_AssignList.DataBind();
                    }
                }
                else
                {
                    gvPublishElement_AssignList.DataSource = null;
                    gvPublishElement_AssignList.DataBind();
                }

            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "gridPublishDirectionElement_RowDataBound");
        }
    }
    private DataSet GetStatus()
    {
        PersonalDevelopmentPlanBM obj = new PersonalDevelopmentPlanBM();
        obj.metCreateDate = DateTime.Now; ;
        // obj.metUserId = Convert.ToInt32(Session["OrgUserId"]);
        obj.metUserId = Convert.ToInt32(Session["QueryStringID"]);
        obj.metCompanyID = Convert.ToInt32(Session["OrgCompanyId"]);

        obj.GetStatusOfMeeting();
        DataSet ds = obj.ds;
        return ds;
    }
    protected void BindStatus()
    {
        //ddlQuesCat
        try
        {
            DataSet ds = GetStatus();

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvStatus_Display.DataSource = ds.Tables[0];
                    gvStatus_Display.DataBind();

                    gvStatus_Display.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                else
                {
                    DataTable dt = ds.Tables[0];
                    DataRow dr = dt.NewRow();
                    dr["Date_part"] = "";
                    dr["Participants"] = "";
                    dr["metStatus"] = "";
                    dr["metID"] = "0";
                    dt.Rows.Add(dr);
                    gvStatus_Display.DataSource = dt;
                    gvStatus_Display.DataBind();
                }
            }
            else
            {
                DataTable dt = new DataTable();
                DataRow dr = dt.NewRow();
                dt.Rows.Add(dr);
                gvStatus_Display.DataSource = dt;
                gvStatus_Display.DataBind();
            }

        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "BindStatus");
        }
    }
    public void btnsearch_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "model", "openModal();", true);

        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        try
        {
            UserBM obj = new UserBM();
            obj.userIsActive = true;
            obj.userIsDeleted = false;
            obj.userCompanyId = companyId;
            obj.userFirstName = txtsearch.Text;

            obj.SearchUser();

            DataSet ds = obj.ds;


            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    listresult.Items.Clear();

                    listresult.DataSource = ds.Tables[0];
                    listresult.DataTextField = "userFirstName";
                    listresult.DataValueField = "userId";
                    listresult.DataBind();

                    listresult.Items.Insert(0, new ListItem(hdnSelectCat.Value, CommonModule.dropDownZeroValue));

                }

                else
                {
                    listresult.Items.Clear();
                    listresult.Items.Insert(0, new ListItem(hdnSelectCat.Value, CommonModule.dropDownZeroValue));
                }
            }
            else
            {
                listresult.Items.Clear();
                listresult.Items.Insert(0, new ListItem(hdnSelectCat.Value, CommonModule.dropDownZeroValue));
            }
            // return ds;
        }

        catch (Exception)
        {

            throw;
        }

    }


    public void btnadd_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "model", "openModal();", true);

        ArrayList arraylist1 = new ArrayList();
        ArrayList arraylist2 = new ArrayList();

        if (listresult.SelectedIndex >= 0)
        {

            for (int i = 0; i < listresult.Items.Count; i++)
            {
                if (listresult.Items[i].Selected)
                {

                    if (!arraylist1.Contains(listresult.Items))
                    {
                        arraylist1.Add(listresult.Items[i]);

                    }
                }
            }

            for (int i = 0; i < arraylist1.Count; i++)
            {
                if (!listparticipants.Items.Contains(((ListItem)arraylist1[i])))
                {
                    listparticipants.Items.Add(((ListItem)arraylist1[i]));
                }
                listresult.Items.Remove(((ListItem)arraylist1[i]));
            }
            // listparticipants.SelectedIndex = -1;
        }




    }


    public void btnsave_Click(object sender, EventArgs e)
    {


        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var LoginuserId = Convert.ToInt32(Session["OrgUserId"]);
        var userId = Convert.ToInt32(hdnQueryUmID.Value);
        try
        {
            PersonalDevelopmentPlanBM obj = new PersonalDevelopmentPlanBM();
            obj.metID = 0;
            obj.metDate = Convert.ToDateTime(txtdate.Text);
            obj.metDescription = txtdescription.Text;
            obj.metDescriptionDN = txtdescriptionDN.Text;


            string listBox2Values = string.Empty;
            string column2 = string.Empty;

            foreach (ListItem item in this.listparticipants.Items)
            {
                // if (item.Selected)
                {
                    listBox2Values = listBox2Values + item.Value + ",";
                }
            }
            obj.metParticipantsId = listBox2Values;
            obj.metCreateDate = DateTime.Now;
            obj.metUpdateDate = DateTime.Now;
            obj.metCompanyID = companyId;
            obj.metUserId = LoginuserId;
            obj.metIsActive = true;
            obj.metIsDelete = false;
            obj.metStatus = "Planned";

            obj.InsertMeetingPDP();

            DataSet ds = new DataSet();
            ds = obj.ds;
            int MeetingID = 0;
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    MeetingID = Convert.ToInt32(ds.Tables[0].Rows[0]["metID"]);
                }
            }
            if (MeetingID > 0)
            {
                var db = new startetkuEntities1();
                db.InsertNotification_Publish("Meeting", companyId, LoginuserId, MeetingID, "metPub", "New Meeting Publish");
                try
                {
                    db.InsertUpdateEmailData(companyId, LoginuserId, Convert.ToString(obj.metParticipantsId), MeetingID, "PDPMeeting", obj.metDescription);
                }
                catch (Exception ex)
                {
                    ExceptionLogger.LogException(ex, 0, "PersonalDevelopmentPlan->btnsave_Click->InsertUpdateEmailData");
                }

            }


            BindStatus();
            txtdescription.Text = "";
            txtdescriptionDN.Text = "";
            txtsearch.Text = "";
            txtdate.Text = "";
            listresult.Items.Clear();
            listparticipants.Items.Clear();
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "btnsave_Click");
        }
    }

    public void btnremove_Click(object sender, EventArgs e)
    {
        try
        {
            int[] itemsSelected = listparticipants.GetSelectedIndices();
            Array.Reverse(itemsSelected);
            foreach (int index in itemsSelected)
            {
                listparticipants.Items.RemoveAt(index);
            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "btnremove_Click");
        }

    }

    protected DataSet GetMeetingDataById(int id)
    {
        PersonalDevelopmentPlanBM obj = new PersonalDevelopmentPlanBM();
        obj.metID = id;

        obj.MeetingStatusEdit();
        DataSet ds = obj.ds;
        return ds;
    }

    protected void ParticipantList(int mid)
    {
        PersonalDevelopmentPlanBM obj = new PersonalDevelopmentPlanBM();
        obj.metID = mid;
        obj.ParticipantListBindData();
        DataSet ds = obj.ds;
        //return ds;

        try
        {

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    listparticipants.DataSource = ds.Tables[0];


                    listparticipants.DataTextField = "userFirstName";
                    listparticipants.DataValueField = "userId";
                    listparticipants.DataBind();

                    //listparticipants.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                else
                {
                    listparticipants.DataSource = null;
                    listparticipants.DataBind();
                }
            }
            else
            {
                listparticipants.DataSource = null;
                listparticipants.DataBind();
            }

        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "ParticipantList");
        }

    }

    protected void gvStatus_Display_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "archive")
        {
            PersonalDevelopmentPlanBM obj = new PersonalDevelopmentPlanBM();
            obj.metID = Convert.ToInt32(e.CommandArgument);

            obj.MeetingStatusDelete();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    //GetAllElementList();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
            BindStatus();

        }
        else if (e.CommandName == "EditRow")
        {
            try
            {


                ClientScript.RegisterStartupScript(this.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lblMeeting').click();", true);
                btnsave.Visible = false;
                btnUpdate.Visible = true;

                lblCompanyDirModalHeader.Text = "Edit Meeting";

                int metID = Convert.ToInt32(e.CommandArgument);


                DataSet ds = GetMeetingDataById(metID);

                hdnQuesID.Value = Convert.ToString(metID);

                //txtmetId.Text = Convert.ToString(ds.Tables[0].Rows[0]["metID"]);
                txtdate.Text = Convert.ToString(ds.Tables[0].Rows[0]["mDate"]);

                txtdescriptionDN.Text = Convert.ToString(ds.Tables[0].Rows[0]["metDescriptionDN"]);
                txtdescription.Text = Convert.ToString(ds.Tables[0].Rows[0]["metDescription"]);

                ParticipantList(metID);

            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex, 0, "gvStatus_Display_RowCommand->EditRow");
            }

        }
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(Session["OrgUserId"]);
        try
        {
            PersonalDevelopmentPlanBM obj = new PersonalDevelopmentPlanBM();
            obj.metID = Convert.ToInt32(hdnQuesID.Value);
            obj.metUserId = userId;
            obj.metDate = Convert.ToDateTime(txtdate.Text);
            obj.metDescription = txtdescription.Text;
            obj.metDescriptionDN = txtdescriptionDN.Text;


            string listBox2Values = string.Empty;
            string column2 = string.Empty;

            foreach (ListItem item in this.listparticipants.Items)
            {
                //if (item.Selected)
                {
                    listBox2Values = listBox2Values + item.Value + ",";
                }
            }
            obj.metParticipantsId = listBox2Values;
            obj.metCreateDate = DateTime.Now;
            obj.metUpdateDate = DateTime.Now;
            obj.metCompanyID = companyId;
            obj.metUserId = userId;
            obj.metIsActive = true;
            obj.metIsDelete = false;
            obj.metStatus = "Planned";

            obj.InsertMeetingPDP();

            BindStatus();

        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "btnUpdate_Click");
        }
    }


    protected void gvStatus_Display_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(Session["OrgUserId"]);
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField IsTmpMandatory = (HiddenField)e.Row.FindControl("hdnMetStatus");
                Label lblMetStatus = (Label)e.Row.FindControl("lblMetStatus");

                if (!string.IsNullOrEmpty(IsTmpMandatory.Value))
                {
                    if (IsTmpMandatory.Value == "Planned")
                    {
                        lblMetStatus.Text = Convert.ToString(GetLocalResourceObject("Planned.Text"));
                    }
                    else
                    {
                        lblMetStatus.Text = Convert.ToString(GetLocalResourceObject("COMPLETED.Text"));

                    }
                }
            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "gvQuesTemplate->OnRowDataBound");
        }
    }
    #endregion

    #region QuestionModule
    protected DataSet GetQuestionPersonalComment(Int32 Index, int umId, int hdnqpercomID, int loginUserID)
    {
        //Common.WriteLog("GetQuestionPersonalComment Index:" + Convert.ToString(Index));
        //Common.WriteLog("GetQuestionPersonalComment umId:" + Convert.ToString(umId));
        //Common.WriteLog("GetQuestionPersonalComment hdnqpercomID:" + Convert.ToString(hdnqpercomID));
        //Common.WriteLog("GetQuestionPersonalComment loginUserID:" + Convert.ToString(loginUserID));

        DataSet ds1 = new DataSet();
        try
        {
            QuestionBM obj = new QuestionBM();

            obj.UserId = Convert.ToInt32(umId);
            obj.queCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
            obj.qpercomID = Convert.ToInt32(hdnqpercomID);
            obj.Index = Index;
            if (umId == loginUserID)
            {
                // hdnIsPDFPersonalComment.Value = "1";  
                //  ClientScript.RegisterStartupScript(this.GetType(), "", "HidePersonalComment();", true);
                obj.IsDisplayInReport = false;
                // btnSaveComment.Visible = true;
            }
            else
            {
                obj.IsDisplayInReport = true;
                // ClientScript.RegisterStartupScript(this.GetType(), "", "HideSaveCommentButton();", true);
                //btnSaveComment.Visible = false;
            }
            obj.loginuserId = loginUserID;
            obj.GetQuestionPersonalComment();
            ds1 = obj.ds;

            if (ds1 != null)
            {
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    if (Convert.ToBoolean(ds1.Tables[0].Rows[0]["IsDisplayInReport"]) == true)
                    {
                        // hdnIsPDFPersonalComment.Value = "1";
                        ClientScript.RegisterStartupScript(this.GetType(), "", "$('#ContentPlaceHolder1_hdnIsPDFPersonalComment').val('1');", true);
                    }
                }
            }
            //if (ds1 != null)
            //{
            //    if (ds1.Tables[0].Rows.Count > 0)
            //    {
            //        hdnqpercomID = Convert.ToInt32(ds1.Tables[0].Rows[0]["qpercomID"]);
            //        txtPersonalComment.Text = Convert.ToString(ds1.Tables[0].Rows[0]["PersonalComment"]);
            //    }
            //}
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "GetQuestionPersonalComment");
        }
        return ds1;
    }

    protected DataSet SetQuestionPersonalCommentPublic(int hdnqpercomID, int umID, int PrivatePublic)
    {
        // ExceptionLogger.LogException(new Exception(), 0, "hdnqpercomID1:" + Convert.ToString(hdnqpercomID));
        DataSet ds1 = new DataSet();
        try
        {
            var loginuserId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
            var userid = umID;
            //if (loginuserId == userid)
            {
                QuestionBM obj = new QuestionBM();
                obj.qpercomID = Convert.ToInt32(hdnqpercomID);
                if (PrivatePublic == 1)
                {
                    obj.IsDisplayInReport = true;
                    // hdnIsPDFPersonalComment.Value = "1";
                }
                else
                {
                    obj.IsDisplayInReport = false;
                    //   hdnIsPDFPersonalComment.Value = "0";
                }
                obj.IsPDPApproved = false;
                obj.UpdateQuestionPersonalCommentStatus();
                ds1 = obj.ds;

            }
            //  ExceptionLogger.LogException(new Exception(), 0, "hdnqpercomID2:" + Convert.ToString(hdnqpercomID));
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "SetQuestionPersonalCommentPublic");
        }
        return ds1;
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static PersonalCommentData GetQuestionPersonalCommentData(int Index, int umId, int hdnqpercomID)
    {
        Organisation_PersonalDevelopmentPlan tmp = new Organisation_PersonalDevelopmentPlan();

        DataSet ds = new DataSet();
        int ispdp = 0;
        int submit = 0;
        var userId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        // umId = userId;
        if (umId == userId)
        {
            ispdp = 1;
            submit = 1;
        }
        else
        {
            submit = 0;
        }

        ds = tmp.GetQuestionPersonalComment(Index, umId, hdnqpercomID, userId);

        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            var commentList = ds.Tables[0].AsEnumerable().Select(dataRow => new PersonalCommentData
            {
                PersonalComment = dataRow.Field<string>("PersonalComment"),
                qpercomID = dataRow.Field<int>("qpercomID"),
                IsDisplayInReportComment = dataRow.Field<bool>("IsDisplayInReport"),
                IsPDFPersonalComment = ispdp,
                btnSaveComment = submit
            }).FirstOrDefault();
            return commentList;
        }
        else
        {
            var data = new PersonalCommentData();
            data.btnSaveComment = submit;
            data.IsPDFPersonalComment = 0;
            data.qpercomID = 0;
            data.IsDisplayInReportComment = true;
            data.PersonalComment = "";

            return data;
        }

    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string SetQuestionPersonalCommentDataPublic(int hdnqpercomID, int umID, int PrivatePublic)
    {
        Organisation_PersonalDevelopmentPlan tmp = new Organisation_PersonalDevelopmentPlan();
        DataSet ds = new DataSet();
        try
        {

            ds = tmp.SetQuestionPersonalCommentPublic(hdnqpercomID, umID, PrivatePublic);

        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "SetQuestionPersonalCommentDataPublic");
        }
        return "success";
    }


    protected void GetAllUserQuestion_PDPList()
    {
        var id = Convert.ToInt32(Request.QueryString["umId"]);
        hdnQueryUmID.Value = Convert.ToString(id);


        try
        {
            QuestionBM obj = new QuestionBM();
            obj.UserId = Convert.ToInt32(id);
            obj.queCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
            obj.Status = "Publish";
            obj.queCreatedDate = DateTime.Now;

            DataSet ds = new DataSet();
            obj.GetAllUserQuestion_PDP();
            ds = obj.ds;
            /////////////////////////
            DataSet ds1 = new DataSet();
            var userId = Convert.ToInt32(Session["OrgUserId"]);
            ds1 = GetQuestionPersonalComment(0, Convert.ToInt32(id), Convert.ToInt32(hdnqpercomID.Value), userId);
            // ExceptionLogger.LogException(new Exception(), 0, "id:" + Convert.ToString(ds1.Tables[0].Rows[0]["qpercomID"]));
            if (ds1 != null)
            {
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    hdnIsDisplayInReport.Value = Convert.ToString(ds1.Tables[0].Rows[0]["IsDisplayInReport"]);
                    hdnIsPDFPersonalComment.Value = Convert.ToString(ds1.Tables[0].Rows[0]["IsPDPApproved"]);
                    hdnqpercomID.Value = Convert.ToString(ds1.Tables[0].Rows[0]["qpercomID"]);
                    txtPersonalComment.Text = Convert.ToString(ds1.Tables[0].Rows[0]["PersonalComment"]);
                }
            }
            else
            {
                txtPersonalComment.Text = "";
            }
            ////////////////////////

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvGrid_Question.DataSource = ds.Tables[0];
                    gvGrid_Question.DataBind();
                }
                else
                {
                    gvGrid_Question.DataSource = null;
                    gvGrid_Question.DataBind();
                }
            }
            else
            {
                gvGrid_Question.DataSource = null;
                gvGrid_Question.DataBind();
            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "GetAllUserQuestion_PDPList");
        }
    }
    protected void gvGrid_Question_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditRow")
        {
            ClientScript.RegisterStartupScript(this.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lblQuestionAnswer').click();", true);
            ClientScript.RegisterStartupScript(this.GetType(), "", "DisplayTab(6);", true);

            GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);
            HiddenField QuesTmpID = (HiddenField)row.FindControl("hdnAnswerText");
            HiddenField QuesTmpIDQues = (HiddenField)row.FindControl("hdnQuesText");
            string argumentValue = Convert.ToString(e.CommandArgument);
            string[] args = argumentValue.Split(',');

            hdnQuestionID.Value = Convert.ToString(args[0]);
            txtQuestion.Text = Convert.ToString(QuesTmpIDQues.Value);
            txtAnswer.Text = Convert.ToString(QuesTmpID.Value);
        }

    }

    // SaveQuestionAnswer_Click
    protected void SaveQuestionAnswer_Click(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "", "DisplayTab(6);", true);
        var companyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        try
        {
            QuestionBM obj = new QuestionBM();
            obj.UserId = userId;
            obj.queCompanyId = companyId;
            obj.queId = Convert.ToInt32(hdnQuestionID.Value);
            obj.Answer = txtAnswer.Text;
            obj.queCreatedDate = DateTime.Now;
            obj.queUpdatedDate = DateTime.Now;
            obj.InsertQuestionAnswer();
            GetAllUserQuestion_PDPList();

        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "SaveQuestionAnswer_Click");
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string btnSaveCommentQuestion(string PersonalComment, string umId)
    {
        var companyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        var id = Convert.ToInt32(umId);
        var hdnqpercomID = "0";
        try
        {
            QuestionBM obj = new QuestionBM();
            //obj.qpercomID = Convert.ToInt32(hdnqpercomID.Value);  //hidden value
            obj.qpercomID = 0;
            obj.UserId = userId;
            obj.queCompanyId = companyId;
            obj.Comment = PersonalComment;
            obj.queCreatedDate = CommonUtilities.GetCurrentDateTime();
            obj.IsPDPApproved = false;
            obj.IsDisplayInReport = false;
            obj.PDPUserID = id;


            obj.InsertQuestionPersonalComment();

            DataSet ds1 = new DataSet();
            ds1 = obj.ds;
            if (ds1 != null)
            {
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    hdnqpercomID = Convert.ToString(ds1.Tables[0].Rows[0]["qpercomID"]);
                    //txtPersonalComment.Text = Convert.ToString(ds1.Tables[0].Rows[0]["PersonalComment"]);
                }
            }
            else
            {
                // txtPersonalComment.Text = "";
            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "btnSaveComment_Click");
        }
        return hdnqpercomID;
    }


    protected void btnSaveComment_Click_old(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "", "DisplayTab(6);", true);
        var companyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        var id = Convert.ToInt32(Request.QueryString["umId"]);
        try
        {
            QuestionBM obj = new QuestionBM();
            //obj.qpercomID = Convert.ToInt32(hdnqpercomID.Value);  //hidden value
            obj.qpercomID = 0;
            obj.UserId = userId;
            obj.queCompanyId = companyId;
            obj.Comment = txtPersonalComment.Text;
            obj.queCreatedDate = CommonUtilities.GetCurrentDateTime();
            obj.IsPDPApproved = false;
            obj.IsDisplayInReport = false;
            obj.PDPUserID = id;


            obj.InsertQuestionPersonalComment();

            DataSet ds1 = new DataSet();
            ds1 = obj.ds;
            if (ds1 != null)
            {
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    hdnqpercomID.Value = Convert.ToString(ds1.Tables[0].Rows[0]["qpercomID"]);
                    txtPersonalComment.Text = Convert.ToString(ds1.Tables[0].Rows[0]["PersonalComment"]);
                }
            }
            else
            {
                txtPersonalComment.Text = "";
            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "btnSaveComment_Click");
        }

    }
    protected void UpdateQuestionPersonalComment_PDPApprovedStatus()
    {
        var companyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        try
        {
            QuestionBM obj = new QuestionBM();
            obj.qpercomID = Convert.ToInt32(hdnqpercomID.Value);  //hidden value
            obj.UserId = userId;
            obj.queCompanyId = companyId;
            obj.Comment = txtPersonalComment.Text;
            obj.queCreatedDate = CommonUtilities.GetCurrentDateTime();
            obj.IsPDPApproved = true;
            obj.IsDisplayInReport = false;
            obj.InsertQuestionPersonalComment();

            DataSet ds1 = new DataSet();
            ds1 = obj.ds;
            if (ds1 != null)
            {
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    hdnqpercomID.Value = "0";
                    txtPersonalComment.Text = ""; ;
                }
            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "UpdateQuestionPersonalComment_PDPApprovedStatus");
        }
    }

    protected void ApproveQuestionPersonalComment(int id)
    {
        var companyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        try
        {
            QuestionBM obj = new QuestionBM();
            obj.qpercomID = 0;  //hidden value
            obj.IsPDPApproved = true;
            obj.UserId = id;
            obj.queCompanyId = companyId;
            // Common.WriteLog("Company ID :" + Convert.ToString(companyId));
            obj.ApproveQuestionPersonalCommentStatus();

            DataSet ds1 = new DataSet();
            //ds1 = obj.ds;
            //if (ds1 != null)
            //{
            //    if (ds1.Tables[0].Rows.Count > 0)
            //    {
            //        hdnqpercomID.Value = "0";
            //        txtPersonalComment.Text = ""; ;
            //    }
            //}
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "ApproveQuestionPersonalComment");
        }
    }

    protected void gvGrid_Question_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {

        var id = Convert.ToInt32(Request.QueryString["umId"]);
        var userId = Convert.ToInt32(Session["OrgUserId"]);

        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                //                LinkButton lnkEdit = (LinkButton)e.Row.FindControl("lnkEdit");
                var editPanel = (Panel)e.Row.FindControl("EditQuesPanel");
                if (id == userId)
                {
                    editPanel.Visible = true;
                }
                else
                {
                    editPanel.Visible = false;
                }
            }

        }
        catch (Exception ex)
        {

            ExceptionLogger.LogException(ex, 0, "gvGrid_Question_OnRowDataBound");
        }
    }
    #endregion


    //DROPDOWN
    protected void getalldt()
    {
        var id = Convert.ToInt32(Request.QueryString["umId"]);
        var db = new startetkuEntities1();
        ddActCate.Items.Insert(0, new ListItem(GetLocalResourceObject("Current.Text").ToString() + "   " + lblddlUserName.Text, "0"));
        if (Convert.ToInt32(Session["OrguserType"]) == 1)
        {
            int userCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);

            // var getdata = db.PDPHistories.Where(o => o.PdpCompanyID == userCompanyId && (o.PpdHisIsActive == true && o.PdpUserID == id)).ToList();
            var getdata = db.GetPDPHistoryLogDetail(1, userCompanyId, id, 0).ToList();

            if (getdata.Any())
            {
                // ddActCate.DataSource = getdata.OrderByDescending(o => o.PdpHisID);
                ddActCate.DataSource = getdata;
                ddActCate.DataTextField = "PdpHisDate";
                ddActCate.DataValueField = "PdpHisID";
                ddActCate.DataBind();
                ddActCate.Items.Insert(0, new ListItem(GetLocalResourceObject("Current.Text").ToString() + "  " + lblddlUserName.Text, "0"));

                /*ddActCate.Items.Insert("1");*/

                // ListItem li = new ListItem();
                // li.Text
                //
            }

        }

        else if (Convert.ToInt32(Session["OrguserType"]) == 2)
        {
            int userManId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);

            //var getdata = db.PDPHistories.Where(o => o.PdpManagerID == userManId && (o.PpdHisIsActive == true && o.PdpUserID == id)).OrderByDescending(o => o.PdpHisID).ToList();
            var getdata = db.GetPDPHistoryLogDetail(2, 0, id, userManId).ToList();

            if (getdata.Any())
            {
                ddActCate.DataSource = getdata;
                ddActCate.DataTextField = "PdpHisDate";
                ddActCate.DataValueField = "PdpHisID";
                ddActCate.DataBind();
                ddActCate.Items.Insert(0, new ListItem(GetLocalResourceObject("Current.Text").ToString() + "  " + lblddlUserName.Text, "0"));
                // ListItem li = new ListItem();
                // li.Text
                //
            }

        }
        else if (Convert.ToInt32(Session["OrguserType"]) == 3)
        {
            int userEmpId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);

            //var getdata = db.PDPHistories.Where(o => o.PdpUserID == userEmpId && (o.PpdHisIsActive == true && o.PdpUserID == id)).ToList();
            var getdata = db.GetPDPHistoryLogDetail(3, 0, id, 0).ToList();

            if (getdata.Any())
            {
                //ddActCate.DataSource = getdata.OrderByDescending(o => o.PdpHisID); ;
                ddActCate.DataSource = getdata;
                ddActCate.DataTextField = "PdpHisDate";
                ddActCate.DataValueField = "PdpHisID";
                ddActCate.DataBind();
                ddActCate.Items.Insert(0, new ListItem(GetLocalResourceObject("Current.Text").ToString() + "  " + lblddlUserName.Text, "0"));
                // ListItem li = new ListItem();
                // li.Text
                //
            }

        }



    }
    //

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static String datedata(int histId)
    {

        var db = new startetkuEntities1();
        String str = "";
        var getdata = db.PDPHistories.FirstOrDefault(o => o.PdpHisID == histId);

        if (String.IsNullOrEmpty(getdata.PDPHistoryFileName))
        {
            str = getdata.PdpHisContent;
        }
        else
        {
            str = Common.ReadFile(getdata.PDPHistoryFileName);
        }
        return str;

        //  var fet = JSON.parse(getdata.PdpHisContent);

    }

    protected static void InsertNotification(int umId, int PDPHistoryID)
    {
        NotificationBM obj = new NotificationBM();
        obj.notsubject = "PDP Approve";
        obj.notUserId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        obj.notIsActive = true;
        obj.notIsDeleted = false;
        obj.notCreatedDate = DateTime.Now;
        obj.notCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
        obj.notpage = "PersonalDevelopmentPlan.aspx?umId=" + umId;  //loginuser id
        obj.nottype = "pdpReq";

        if (umId == Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]))
        {
            obj.notToUserId = Convert.ToInt32(HttpContext.Current.Session["OrgCreatedBy"]);
        }
        else
        {
            obj.notToUserId = umId;
        }

        obj.notPDPHistoryID = Convert.ToString(PDPHistoryID);
        // emp  // Man 
        obj.InsertNotification();
        //chek user type of login

        var p = new Organisation_PersonalDevelopmentPlan();

        p.sendmail(Convert.ToInt32(HttpContext.Current.Session["OrgCreatedBy"]), umId); //  emp // mang // comp
    }

    protected void sendmail(Int32 touserid, int umId)
    {
        var id = umId;
        var link = umId;
        string email;
        string name;
        Template template = new Template();
        if (id != Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]))
        {
            var userDetail = UserBM.GetUserByIdLinq(id);
            name = userDetail.userFirstName;
            email = userDetail.userEmail;

          //  template = CommonModule.getTemplatebyname1("Approve PDP By Higher Authority", Convert.ToInt32(Session["OrgCreatedBy"]));
            template = CommonModule.getTemplatebyname1("Approve PDP By Higher Authority", Convert.ToInt32(id));
        }
        else
        {
            id = Convert.ToInt32(HttpContext.Current.Session["OrgCreatedBy"]);
            var userDetail = UserBM.GetUserByIdLinq(id);
            name = userDetail.userFirstName;
            email = userDetail.userEmail;


            template = CommonModule.getTemplatebyname1("Approve PDP By Lower Authority", Convert.ToInt32(id));

        }

        #region Send mail
        // String subject = "Set Competence for " + Convert.ToString(Session["OrgUserName"]);

        //man
        //
        // 
        //String confirmMail = CommonModule.getTemplatebyname("Set Competence", touserid);
        String confirmMail = template.TemplateName;
        if (!String.IsNullOrEmpty(confirmMail))
        {
            string subject = string.Empty;
            try
            {

               // var data = HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Welcome.Text").ToString(); //GetLocalResourceObject("SetCompetenceSubjectEngResource.Text").ToString();
                var data = HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "PDPApprove_Email_Subject.Text").ToString(); //GetLocalResourceObject("SetCompetenceSubjectEngResource.Text").ToString();
                subject = data;
            }
            catch (Exception ex)
            {
                subject = "PDPApprove_Email_Subject";
            }

            /* if (template.Language == "Danish")
             {
                 subject = GetLocalResourceObject("SetCompetenceSubjectDNResource.Text").ToString();
             }*/

            UserBM Cust = new UserBM();
            Cust.userId = Convert.ToInt32(Session["OrgCreatedBy"]);
            Cust.SelectmanagerByUserId();
            //  Cust.SelectPasswordByUserId();
            DataSet ds = Cust.ds;
            if (ds != null)
            {

                if (ds.Tables[0].Rows.Count > 0)
                {
                    // String name = Convert.ToString(ds.Tables[0].Rows[0]["userFirstName"]);
                    string fromid = Convert.ToString(ConfigurationManager.AppSettings["fromid"]);
                    String empname = Convert.ToString(Session["OrgUserName"]);
                    string tempString = confirmMail;
                    tempString = tempString.Replace("###name###", name);
                    tempString = tempString.Replace("###LINK###", Convert.ToString(link));
                    //  Convert.ToString(ds.Tables[0].Rows[0]["userEmail"])
                    CommonModule.SendMailToUser(email, subject, tempString, fromid, Convert.ToString(HttpContext.Current.Session["OrgCompanyId"]));

                    //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'> generate('information', '<b>'" + RequestSentSuccessful.Value + "'</b><br> ', 'bottomCenter');</script>", false);
                }
            }
        }

        #endregion
    }

    //protected void Approve(object sender, EventArgs e)
    //{
    //    var db = new startetkuEntities1();
    //    var PDPH = new PDPHistory();
    //    PDPH.PdpManagerID = Convert.ToInt32(Session["OrgUserId"]);
    //    PDPH.PdpCompanyID = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
    //    PDPH.PdpUserID = Convert.ToInt32(Session["OrgUserId"]);
    //    PDPH.PdpHisContent =
    //    PDPH.PdpHisDate=
    //    PDPH.PpdHisIsActive=
    //    db.PDPHistories.Add(PDPH);
    //    db.SaveChanges();
    //}

    protected void R1_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {

        try
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField comId = e.Item.FindControl("comId") as HiddenField;
                Label lbpoint = e.Item.FindControl("lbpoint") as Label;
                Label lbllocal = e.Item.FindControl("lbllocal") as Label;
                Label lblAchive = e.Item.FindControl("lblAchive") as Label;
                Label lbltarget = e.Item.FindControl("lbltarget") as Label;
                Label lblset = e.Item.FindControl("lblset") as Label;
                HiddenField hdnSet = e.Item.FindControl("hdnSet") as HiddenField;
                DataSet ds = (DataSet)ViewState["data"];




                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {

                        DataRow[] row = ds.Tables[0].Select("comId=" + comId.Value);
                        foreach (var i in row)
                        {
                            lbllocal.Text = (Convert.ToString(i["skillLocal"]));
                            if (!String.IsNullOrEmpty((Convert.ToString(i["skillAchive"]))))
                            {
                                lblAchive.Text = (Convert.ToString(i["skillAchive"]));
                            }
                            if (!String.IsNullOrEmpty((Convert.ToString(i["skilltarget"]))))
                            {
                                lbltarget.Text = (Convert.ToString(i["skilltarget"]));
                            }
                            if (!String.IsNullOrEmpty((Convert.ToString(i["skillIsApproved"]))))
                            {
                                //lblset.Text = (Convert.ToString(ds.Tables[0].Rows[i]["skillSet"]));
                                if (Convert.ToBoolean(i["skillIsApproved"]))
                                {
                                    if (lblset != null)
                                    {
                                        //lblset.Text = hdnYes.Value;
                                        hdnSet.Value = "Yes";
                                    }
                                }

                            }
                        }

                    }



                }

                if (checksetbtn == false)
                {
                    // btnsetsave.Visible = false;
                }

            }

        }
        catch (Exception ex)
        {
            Common.WriteLog("error in R1_ItemDataBound1" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }

    protected void Repeater1_OnItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Local")
        {
            try
            {
                string col1 = ((Label)e.Item.FindControl("lbpoint")).Text;
                Int32 sid = Convert.ToInt32(e.CommandArgument);
                DataSet dtlocal = new DataSet();
                dtlocal = (DataSet)ViewState["point"];
                DataView dv = new DataView();
                dv = dtlocal.Tables[0].DefaultView;
                //dv.RowFilter = "userJobType = '" + Convert.ToInt32(Session["userJobType"]) + "'";
                dv.RowFilter = "userJobType = '" + Convert.ToInt32(Session["userJobType"]) + "' AND PointAnsCount < '" + Convert.ToInt32(col1) + "' AND pintSubCompetance = '" + Convert.ToInt32(sid) + "'";
                DataTable dtitm = dv.ToTable();
                DataSet ds = new DataSet();
                ds.Tables.Add(dtitm);
                //if (Common.IsDatasetvalid(ds))
                //{
                //rptlocal.DataSource = ds;
                // rptlocal.DataBind();
                // mpe.Show();

            }
            catch (Exception ex)
            {
                Common.WriteLog("error in Repeater1_OnItemCommand" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            }
        }

        else if (e.CommandName == "All")
        {
            string col1 = ((Label)e.Item.FindControl("lbpoint")).Text;
            Int32 sid = Convert.ToInt32(e.CommandArgument);

            DataSet dtlocal = new DataSet();
            dtlocal = (DataSet)ViewState["point"];
            DataView dv = new DataView();
            dv = dtlocal.Tables[0].DefaultView;
            dv.RowFilter = "PointAnsCount < '" + Convert.ToInt32(col1) + "' AND pintSubCompetance = '" + Convert.ToInt32(sid) + "'";
            DataTable dtitm = dv.ToTable();
            DataSet ds = new DataSet();
            ds.Tables.Add(dtitm);

            //rptlocal.DataSource = ds;
            // rptlocal.DataBind();
            //mpe.Show();

        }
        else if (e.CommandName == "catNameClick")
        {
            Session["IsNull"] = "true";
            string col1 = ((Label)e.Item.FindControl("lbpoint")).Text;
            LinkButton lk = (LinkButton)e.Item.FindControl("lnkCatName");
            Int32 sid = Convert.ToInt32(e.CommandArgument);
            Session["comId"] = sid;

            Session["newcomId"] = sid;
            CompetenceMasterBM obj = new CompetenceMasterBM();
            obj.comIsActive = true;
            obj.comIsDeleted = false;
            obj.comchildcomID = Convert.ToInt32(sid);
            obj.comUserId = Convert.ToInt32(Session["OrgUserId"]);
            obj.comId = sid;
            obj.GetAllchildCompetenceskill();
            DataSet ds = obj.ds;
            ViewState["rep_content"] = ds;

            DataSet dscontent = (DataSet)ViewState["data"];
            // rep_competence.DataSource = dscontent;
            //rep_competence.DataBind();

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {




                }
            }




            //Timer1.Enabled = true;
        }
    }

    protected void rep_competencehigh_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        try
        {
            //if (rep_competencehigh.Items.Count < 1)
            //   {
            if (e.Item.ItemType == ListItemType.Footer)
                //     {
                //         HtmlTableRow tr = (HtmlTableRow)e.Item.FindControl("Emptyrow");
                //tr.Visible = true;
                //  //    }
                //  }
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HiddenField comId = e.Item.FindControl("comId") as HiddenField;
                    Label lbpoint = e.Item.FindControl("lbpoint") as Label;
                    DataSet ds = (DataSet)ViewState["data"];
                    if (ds != null)
                    {
                        if (ds.Tables[1].Rows.Count > 0)
                        {
                            for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                            {                            //comId
                                if (Convert.ToInt32(comId.Value) == Convert.ToInt32(ds.Tables[1].Rows[i]["pintSubCompetance"]))
                                {
                                    lbpoint.Text = (Convert.ToString(ds.Tables[1].Rows[i]["PointAnsCount"]));
                                }
                            }
                        }

                    }
                }




        }
        catch (Exception ex)
        {
            Common.WriteLog("error in R1_ItemDataBound" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }

    protected void rep_competencehigh_OnItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Local")
        {
            try
            {
                string col1 = ((Label)e.Item.FindControl("lbpoint")).Text;
                Int32 sid = Convert.ToInt32(e.CommandArgument);
                DataSet dtlocal = new DataSet();
                dtlocal = (DataSet)ViewState["point"];
                DataView dv = new DataView();
                dv = dtlocal.Tables[0].DefaultView;
                //dv.RowFilter = "userJobType = '" + Convert.ToInt32(Session["userJobType"]) + "'";
                dv.RowFilter = "userJobType = '" + Convert.ToInt32(Session["userJobType"]) + "' AND PointAnsCount > '" + Convert.ToInt32(col1) + "' AND pintSubCompetance = '" + Convert.ToInt32(sid) + "'";
                DataTable dtitm = dv.ToTable();
                DataSet ds = new DataSet();
                ds.Tables.Add(dtitm);
                //if (Common.IsDatasetvalid(ds))
                //{
                //  rpllocalhigh.DataSource = ds;
                //  rpllocalhigh.DataBind();
                //mpe.Show();
                // ModalPopupExtender1.Show();
                //}


            }
            catch (Exception ex)
            {
                Common.WriteLog("error in Repeater1_OnItemCommand" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            }
        }

        else if (e.CommandName == "All")
        {
            string col1 = ((Label)e.Item.FindControl("lbpoint")).Text;
            Int32 sid = Convert.ToInt32(e.CommandArgument);

            DataSet dtlocal = new DataSet();
            dtlocal = (DataSet)ViewState["point"];
            DataView dv = new DataView();
            dv = dtlocal.Tables[0].DefaultView;
            dv.RowFilter = "PointAnsCount > '" + Convert.ToInt32(col1) + "' AND pintSubCompetance = '" + Convert.ToInt32(sid) + "'";
            DataTable dtitm = dv.ToTable();
            DataSet ds = new DataSet();
            ds.Tables.Add(dtitm);
            //if (Common.IsDatasetvalid(ds))
            //{
            // rpllocalhigh.DataSource = ds;
            // rpllocalhigh.DataBind();
            //  ModalPopupExtender1.Show();
            //}

        }
        else if (e.CommandName == "catNameClick")
        {
            string col1 = ((Label)e.Item.FindControl("lbpoint")).Text;
            LinkButton lk = (LinkButton)e.Item.FindControl("lnkCatName");
            Int32 sid = Convert.ToInt32(e.CommandArgument);
            Session["comId"] = sid;
            CompetenceMasterBM obj = new CompetenceMasterBM();
            obj.comIsActive = true;
            obj.comIsDeleted = false;
            obj.comchildcomID = Convert.ToInt32(sid);
            //obj.comId=sid;
            obj.GetAllchildCompetenceskill();
            DataSet ds = obj.ds;
            //ViewState["data"] = ds;
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    //txtComment.Text = Convert.ToString(ds.Tables[0].Rows[0]["skillComment"]);
                    //txtCommentDummy.Text = Convert.ToString(ds.Tables[0].Rows[0]["skillComment"]);
                    // rep_tabmenu.DataSource = ds;
                    // rep_tabmenu.DataBind();
                    // rep_content.DataSource = ds;
                    // rep_content.DataBind();

                }
            }





            // Timer1.Enabled = true;
        }
    }

    #region Competence

    #endregion


    public Winnovative.PdfCreator.PdfPage page { get; set; }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<GetComChild_Result> GetCompDesc(int comId, int comActual)
    {



        var GetCompDescList = ComchildBM.GetComChildRequestList(comId, comActual);

        //  return HttpContext.Current.Regex.Replace(GetCompDescList, "<.*?>", String.Empty);
        // var GetCompDescList1 = GetCompDescList.InnerText;

        return GetCompDescList;

    }

    [WebMethod(EnableSession = true)]
    //Jainam Shah Apr 2017
    public static string insertmessage(string message, int umId)
    {
        string msg = string.Empty;
        var id = umId;
        try
        {
            var actCat = new PersonalDevelopmentPlan();
            // WriteLog("ENTRY ");


            CompetenceBM obj = new CompetenceBM();
            var db = new startetkuEntities1();

            id = umId;
            actCat.PerCompanyID = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
            actCat.PerComment = message;
            actCat.PerCreaterId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
            actCat.PerCommentToUserID = id;// Emp or man id : which is passed in query string 
            actCat.PerComActReqId = null;
            actCat.PerIsActive = true;
            actCat.PerIsdeleted = false;
            actCat.PerComCreateDate = CommonUtilities.GetCurrentDateTime();
            actCat.PerComUpdateDate = CommonUtilities.GetCurrentDateTime();
            actCat.PerUserTypeId = Convert.ToInt32(HttpContext.Current.Session["OrguserType"]);
            //

            db.PersonalDevelopmentPlans.Add(actCat);

            db.SaveChanges();
            msg = "true";
            //WriteLog("insertmessage PASSED ");

        }
        catch (Exception ex)
        {

            ExceptionLogger.LogException(ex);
        }
        GetPersonal(umId);

        return msg;

    }

    [WebMethod(EnableSession = true)]
    public static string insertmessageQuestion(string message, int umId)
    {
        string msg = string.Empty;
        // var id = umId;
        try
        {


            int ToUserID = 0;


            QuestionBM obj = new QuestionBM();
            if (umId == Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]))
            {
                obj.UserId = Convert.ToInt32(HttpContext.Current.Session["OrgCreatedBy"]);
                ToUserID = obj.UserId;
            }
            else
            {
                ToUserID = umId;
                obj.UserId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
            }
            //obj.UserId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
            obj.queCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
            obj.ToUserID = Convert.ToInt32(umId);
            obj.Comment = message;
            obj.UserType = Convert.ToInt32(HttpContext.Current.Session["OrguserType"]);
            obj.queCreatedDate = CommonUtilities.GetCurrentDateTime();
            obj.loginuserId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);

            obj.InsertQuestionComment();



            DataTable dt = new DataTable();
            dt = obj.ds.Tables[0];

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    var db = new startetkuEntities1();
                    db.InsertNotification_Publish("QuesDiag", Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]), Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]), Convert.ToInt32(dt.Rows[0]["qcomID"]), "quesDia", "General Dialogue");
                    try
                    {
                        db.InsertUpdateEmailData(Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]), Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]), Convert.ToString(ToUserID), Convert.ToInt32(dt.Rows[0]["qcomID"]), "QuesDialog", obj.Comment);
                    }
                    catch (Exception ex)
                    {
                        ExceptionLogger.LogException(ex, 0, "PersonalDevelopmentPlan->btnsave_Click->insertmessageQuestion");
                    }

                }
            }
            msg = "true";



        }
        catch (Exception ex)
        {

            ExceptionLogger.LogException(ex);
        }
        // GetPersonal(umId);

        return msg;

    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    //Jainam Shah Apr 2017
    public static List<GetPersonalDevelopmentPlanCom_Result> GetPersonal(int umId)
    {


        var id = umId;
        var userDetail = UserBM.GetUserByIdLinq(id);
        var type = userDetail.userType;
        if (type == 3)
        {


        }

        //else if (type == 2)
        //{
        //    var GetPersonal = GetPersonalDevelopmentPlanComBM.GetPersonalDevelopmentPlanComRequestList(id);
        //    return GetPersonal;

        //}

        var GetPersonal = GetPersonalDevelopmentPlanComBM.GetPersonalDevelopmentPlanComRequestList(id);
        return GetPersonal;



    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<CommentList> GetQuestionCommentList(int umId)
    {
        //var id = umId;
        //var userDetail = UserBM.GetUserByIdLinq(id);
        //var type = userDetail.userType;

        QuestionBM obj = new QuestionBM();
        obj.UserId = umId;
        obj.queCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);

        if (umId == Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]))
        {
            obj.ToUserID = Convert.ToInt32(HttpContext.Current.Session["OrgCreatedBy"]);
        }
        else
        {
            obj.ToUserID = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        }
        obj.GetQuestionComment();
        DataSet ds = new DataSet();


        ds = obj.ds;

        var userList = ds.Tables[0].AsEnumerable().Select(dataRow => new CommentList
        {
            Comment = dataRow.Field<string>("Comment"),
            CreateDate = dataRow.Field<DateTime>("CreateDate"),
            fromuserImage = dataRow.Field<string>("fromuserImage")
        }).ToList();

        return userList;
    }

    //[WebMethod(EnableSession = true)]
    //public static void SaveHtmlData(string data)
    //{
    //    DataSet ds = new DataSet();

    //    PDPHistoryBM obj = new PDPHistoryBM();
    //    obj.PdpCompanyID = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
    //    obj.PdpManagerID = Convert.ToInt32(HttpContext.Current.Session["OrgCreatedBy"]); // if login by emp then its Manager's ID who created this emp.
    //    obj.PdpUserID = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
    //    obj.PdpHisContent = data;
    //    obj.InsertPDPHistory();
    //    ds = obj.ds;
    //}
    [WebMethod(EnableSession = true)]
    //Jainam Shah Apr 2017
    public static void SaveHtmlData(string data, int umId)
    {
        DataSet ds = new DataSet();
        var id = umId;
        var id1 = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        var userDetail = UserBM.GetUserByIdLinq(id);
        var type = userDetail.userType;

        PDPHistoryBM obj = new PDPHistoryBM();
        PDPHistory checkDup = new PDPHistory();
        var db = new startetkuEntities1();
        db.PDPHistories.Add(checkDup);
        checkDup.PdpCompanyID = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);

        obj.PdpCompanyID = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
        if (type == 3)
        {
            if (Convert.ToInt32(HttpContext.Current.Session["OrguserType"]) == 2 && umId != Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]))
            {
                obj.PdpManagerID = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
                checkDup.PdpManagerID = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
            }
            else if (Convert.ToInt32(HttpContext.Current.Session["OrguserType"]) == 3 && umId == Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]))
            {
                obj.PdpManagerID = Convert.ToInt32(HttpContext.Current.Session["OrgCreatedBy"]);
                checkDup.PdpManagerID = Convert.ToInt32(HttpContext.Current.Session["OrgCreatedBy"]);
            }

        }// if login by emp then its Manager's ID who created this emp.
        else if (type == 2)
        {
            if (Convert.ToInt32(HttpContext.Current.Session["OrguserType"]) == 2 && umId == Convert.ToInt32(HttpContext.Current.Session["OrgUserId"])) { obj.PdpManagerID = umId; }
            else if (Convert.ToInt32(HttpContext.Current.Session["OrguserType"]) == 1 && umId != Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]))
            {
                obj.PdpManagerID = umId;
                checkDup.PdpManagerID = umId;
            }

        }
        obj.PdpUserID = umId;
        // obj.PpdHisIsActive = true;

        checkDup.PdpHisDate = CommonUtilities.GetCurrentDateTime();
        checkDup.PdpUserID = umId;
        string pdpfilepath = Common.CreateTextFile(data);
        checkDup.PDPHistoryFileName = pdpfilepath;
        checkDup.PdpHisContent = "";
        checkDup.PpdHisIsActive = false;
        checkDup.manaptime = CommonUtilities.GetCurrentDateTime();
        db.SaveChanges();

        //obj.PdpHisContent = data;  //swati
        //obj.InsertPDPHistory();
        //ds = obj.ds;

        //   new Organisation_PersonalDevelopmentPlan().getalldt();
        //  HttpContext.Current.Response.Redirect("PersonalDevelopmentPlan.aspx?umId=" + umId);
        //var GetPersonal = GetPersonalDevelopmentPlanComBM.GetPersonalDevelopmentPlanComRequestList(id);


        if (umId != null)
        {
            // var db = new startetkuEntities1();
            db.PersonalDevelopmentPlan_UpdateToHistoric(umId);

        }

    }


    [WebMethod(EnableSession = true)]
    public static void SaveHtmlDataUpdate(string data, int PDPHistID)
    {
        var db = new startetkuEntities1();
        var pdpData = db.PDPHistories.Find(PDPHistID);
        data = "<style>.HideControl {display:none !important;}</style>" + data;
        string pdpfilepath = Common.CreateTextFile(data);
        pdpData.PDPHistoryFileName = pdpfilepath;
        db.SaveChanges();
        var umId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);

    }

    public void SaveHtmlDataUpdate_New(int PDPHistID)
    {
        var db = new startetkuEntities1();
        var pdpData = db.PDPHistories.Find(PDPHistID);
        if (pdpData != null)
        {
            string data = htmlData.InnerText;
            data = "<style>.HideControl {display:none !important;}</style>" + data;
            string pdpfilepath = Common.CreateTextFile(data);
            pdpData.PDPHistoryFileName = pdpfilepath;
            db.SaveChanges();
            var umId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        }
    }

    [WebMethod(EnableSession = true)]
    //SwatiApprove
    public static String Approve(string strdata, int umId)
    {
        //if there is already approved by manager then duplicate entry should not happen

        var db = new startetkuEntities1();
        var PDPH = new PDPHistory();
        var id = umId;
        var userDetail = UserBM.GetUserByIdLinq(id);
        var type = userDetail.userType;

        strdata = "<style>.HideControl {display:none !important;}</style>" + strdata;

        try
        {

            //  Common.WriteLog("Approve : type :" + Convert.ToString(type));
            if (true)
            {
                if (type == 3)  //if user is employee
                {

                    if (Convert.ToInt32(HttpContext.Current.Session["OrguserType"]) == 2 &&
                        umId != Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]))
                    {

                        //      Common.WriteLog("Approve : umId1 :" + Convert.ToString(umId));
                        //   var checkDup = db.PDPHistories.FirstOrDefault(o => o.PdpUserID == umId);
                        var checkDup = db.PDPHistories.OrderByDescending(o => o.PdpHisID).FirstOrDefault(o => o.PdpUserID == umId && o.PpdHisIsActive == false);
                        // if (checkDup == null)
                        //  {
                        checkDup = new PDPHistory();

                        db.PDPHistories.Add(checkDup);
                        checkDup.PdpHisDate = CommonUtilities.GetCurrentDateTime();

                        //  }

                        checkDup.PdpManagerID = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
                        checkDup.PdpCompanyID = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);

                        checkDup.PdpUserID = umId;

                        string pdpfilepath = Common.CreateTextFile(strdata);

                        checkDup.PDPHistoryFileName = pdpfilepath;

                        //checkDup.PdpHisContent = strdata;  //swati
                        checkDup.PdpHisContent = "";



                        //checkDup.PpdHisIsActive = false;
                        checkDup.PpdHisIsActive = false;
                        checkDup.manaptime = CommonUtilities.GetCurrentDateTime();
                        checkDup.Pdpapprovman = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);

                        //PDPH.Pdpapprovemp=
                        //PDPH.Pdpapprovcom =

                        db.SaveChanges();

                        PDPH = checkDup;  //Swati added on 17/01/2018

                        InsertNotification(umId, PDPH.PdpHisID);


                    }


                    if (Convert.ToInt32(HttpContext.Current.Session["OrguserType"]) == 3 &&
                        umId == Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]))
                    {

                        // Common.WriteLog("Approve : umId2 :" + Convert.ToString(umId));
                        //  var checkDup = db.PDPHistories.FirstOrDefault(o => o.PdpUserID == umId);
                        var checkDup = db.PDPHistories.OrderByDescending(o => o.PdpHisID).FirstOrDefault(o => o.PdpUserID == umId && o.PpdHisIsActive == false);

                        if (checkDup == null)
                        {
                            checkDup = new PDPHistory();
                            db.PDPHistories.Add(checkDup);

                        }
                        checkDup.empaptime = CommonUtilities.GetCurrentDateTime();
                        checkDup.Pdpapprovemp = umId;
                        checkDup.PpdHisIsActive = true;

                        string pdpfilepath = Common.CreateTextFile(strdata);

                        checkDup.PDPHistoryFileName = pdpfilepath;

                        db.SaveChanges();

                        PDPH = checkDup;  //Swati added on 17/01/2018

                        InsertNotification(umId, PDPH.PdpHisID);

                        try
                        {
                            Organisation_PersonalDevelopmentPlan tData = new Organisation_PersonalDevelopmentPlan();
                            tData.ApproveQuestionPersonalComment(umId);
                        }
                        catch (Exception ex)
                        {
                            ExceptionLogger.LogException(ex, 0, "Approve->ApproveQuestionPersonalComment");
                        }
                    }
                    //HttpContext.Current.Response.Redirect("PersonalDevelopmentPlan.aspx?ddac=" + true);
                    //HttpContext.Current.Response.Redirect("PersonalDevelopmentPlan.aspx?umId=" + um);


                }

                else if (type == 2) //if user is manager
                {

                    if (Convert.ToInt32(HttpContext.Current.Session["OrguserType"]) == 1 &&
                        umId != Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]))
                    {

                        //Common.WriteLog("Approve : umId3 :" + Convert.ToString(umId));
                        // var checkDup = db.PDPHistories.FirstOrDefault(o => o.PdpUserID == umId);
                        var checkDup = db.PDPHistories.OrderByDescending(o => o.PdpHisID).FirstOrDefault(o => o.PdpUserID == umId && o.PpdHisIsActive == false);

                        //   if (checkDup == null)
                        //  { 
                        checkDup = new PDPHistory();
                        db.PDPHistories.Add(checkDup);

                        checkDup.PdpHisDate = CommonUtilities.GetCurrentDateTime();

                        //   }
                        try
                        {
                            //db.PDPHistories.Add(checkDup);
                            checkDup.PdpManagerID = umId;
                            checkDup.PdpCompanyID = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);

                            checkDup.PdpUserID = umId;

                            string pdpfilepath = Common.CreateTextFile(strdata);

                            checkDup.PDPHistoryFileName = pdpfilepath;
                            checkDup.PdpHisContent = "";
                            // checkDup.PdpHisContent = strdata;

                            checkDup.PpdHisIsActive = false;
                            checkDup.comaptime = CommonUtilities.GetCurrentDateTime();
                            checkDup.Pdpapprovcom = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
                            //PDPH.Pdpapprovemp=
                            //PDPH.Pdpapprovcom =

                            db.SaveChanges();

                            PDPH = checkDup;  //Swati added on 17/01/2018
                        }
                        catch (Exception ex)
                        {

                            ExceptionLogger.LogException(ex, 0, "Approve type:" + Convert.ToString(type));
                        }

                        InsertNotification(umId, PDPH.PdpHisID);
                    }


                    if (Convert.ToInt32(HttpContext.Current.Session["OrguserType"]) == 2 &&
                        umId == Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]))
                    {

                        //Common.WriteLog("Approve : umId4 :" + Convert.ToString(umId));
                        // var checkDup = db.PDPHistories.FirstOrDefault(o => o.PdpUserID == umId);
                        var checkDup = db.PDPHistories.OrderByDescending(o => o.PdpHisID).FirstOrDefault(o => o.PdpUserID == umId && o.PpdHisIsActive == false);
                        if (checkDup == null)
                        {
                            checkDup = new PDPHistory();
                            db.PDPHistories.Add(checkDup);

                        }
                        checkDup.manaptime = CommonUtilities.GetCurrentDateTime();
                        checkDup.Pdpapprovman = umId;
                        checkDup.PpdHisIsActive = true;

                        string pdpfilepath = Common.CreateTextFile(strdata);

                        checkDup.PDPHistoryFileName = pdpfilepath;

                        db.SaveChanges();

                        PDPH = checkDup;  //Swati added on 17/01/2018

                        try
                        {
                            Organisation_PersonalDevelopmentPlan tData = new Organisation_PersonalDevelopmentPlan();
                            tData.ApproveQuestionPersonalComment(umId);
                        }
                        catch (Exception ex)
                        {
                            ExceptionLogger.LogException(ex, 0, "Approve->ApproveQuestionPersonalComment");
                        }
                    }

                    //HttpContext.Current.Response.Redirect("PersonalDevelopmentPlan.aspx?umId=" + um);
                }


            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "Approve");
        }

        //return "PersonalDevelopmentPlan.aspx?umId=" + umId + "&sel=" + "true";

        return Convert.ToString(PDPH.PdpHisID);




    }

    protected void GetAproveDetails()
    {

        var db = new startetkuEntities1();

        //Get pdphistory records by Employee/manager

        var idOfLowerUser = 0;

        //if (Convert.ToInt32(HttpContext.Current.Session["OrguserType"]) == 2)
        //{

        //     idOfLowerUser = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        //}
        //else if (Convert.ToInt32(HttpContext.Current.Session["OrguserType"]) == 3)
        //{

        //    idOfLowerUser = Convert.ToInt32(HttpContext.Current.Session["userCreateBy"]);
        //}
        //else if (Convert.ToInt32(HttpContext.Current.Session["OrguserType"]) == 1)
        //{

        //    idOfLowerUser = Convert.ToInt32(Request.QueryString["umId"]);
        //}
        try
        {

            // Common.WriteLog("GetAproveDetails 1");
            idOfLowerUser = Convert.ToInt32(Request.QueryString["umId"]);
            // var pdpHist = db.PDPHistories.FirstOrDefault(o => o.PdpUserID == idOfLowerUser);

            //var pdpHist = db.PDPHistories.OrderByDescending(o => o.PdpHisID).FirstOrDefault(o => o.PdpUserID == idOfLowerUser && o.PpdHisIsActive == false);
            var pdpHist = db.PDPHistories.OrderByDescending(o => o.PdpHisID).FirstOrDefault(o => o.PdpUserID == idOfLowerUser);
            //Common.WriteLog("pdpHist1 :" + Convert.ToString(pdpHist.PdpHisID));
            var userDetail = UserBM.GetUserByIdLinq(idOfLowerUser);
            var type = userDetail.userType;
            //  Common.WriteLog("type :" + Convert.ToString(type));
            //  Common.WriteLog("GetAproveDetails 2 type : " + Convert.ToString(type));

            if (pdpHist != null)
            {
                //Common.WriteLog("pdpHist2 :" + Convert.ToString(pdpHist.PdpHisID));
                //Common.WriteLog("GetAproveDetails 3 type : " + Convert.ToString(pdpHist.Pdpapprovman));
                //Common.WriteLog("GetAproveDetails 4 type : " + Convert.ToString(pdpHist.Pdpapprovemp));
                //Common.WriteLog("GetAproveDetails 5 type : " + Convert.ToString(pdpHist.Pdpapprovcom));
                //Common.WriteLog("GetAproveDetails 6 type : " + Convert.ToString(pdpHist.Pdpapprovman));

                if (((pdpHist.Pdpapprovman != null && pdpHist.Pdpapprovemp != null) || (pdpHist.Pdpapprovcom != null && pdpHist.Pdpapprovman != null)) != true)
                {
                    //Common.WriteLog("pdpHist222 :" + Convert.ToString(pdpHist.Pdpapprovemp));
                    if (type == 3 && (pdpHist.manaptime != null || pdpHist.empaptime != null || pdpHist.comaptime != null))
                    {
                        if (pdpHist == null)
                        {
                            return;
                            //That means no records in pdpHist and no one (d this report

                        }
                        else
                        {
                            //Common.WriteLog("Pdpapprovemp :" + Convert.ToString(pdpHist.Pdpapprovemp));
                            // Common.WriteLog("PdpManagerID :" + Convert.ToString(pdpHist.PdpManagerID));
                            if (pdpHist.Pdpapprovemp != null && pdpHist.PdpManagerID > 0)
                            {
                                //that means both have approved this PDP
                                var lowerAuthDetails = UserBM.GetUserByIdLinq(Convert.ToInt32(pdpHist.PdpUserID));
                                lblLowerAuthName.Text = lowerAuthDetails.userFirstName + " " + lowerAuthDetails.userLastName + " ";
                                lowdate.Text = (pdpHist.empaptime).ToString();
                                Label14.Visible = true;
                            }
                            else
                            {


                            }

                            if (pdpHist.PdpManagerID > 0)
                            {
                                var highAuthDetails = UserBM.GetUserByIdLinq(Convert.ToInt32(pdpHist.PdpManagerID));

                                lblHigherAuthName.Text = highAuthDetails.userFirstName + " " + highAuthDetails.userLastName + " ";

                                hidate.Text = (pdpHist.manaptime).ToString();
                                Label14.Visible = true;

                            }

                        }

                        //int PdpManagerID = Convert.ToInt32(HttpContext.Current.Session["OrgCreatedBy"]);


                        //var actCatList = from UserMasters in db.UserMasters
                        //                 join PDPHistories in db.PDPHistories on new { userId = UserMasters.userId } equals new { userId = (int)PDPHistories.PdpManagerID } into PDPHistories_join
                        //                 from PDPHistories in PDPHistories_join.DefaultIfEmpty()
                        //                 select new 
                        //                 {
                        //                    UserMasters,PDPHistories
                        //                 };

                        //Apprbyhigh.Text = actCatList.FirstOrDefault(o => o.UserMasters.userId == getManFrmHist).UserMasters.userFirstName;


                        //var GetUserById = UserBM.GetUserByIdLinq(139);

                        ///////// 10-1-18    According to New PDP after approval of employee and manager should be saved as historica
                        //  Tab 1 Info & Management:
                        //* "Meeting status" values to be cleared
                        //* "Related document:" values to be cleared
                        //* "Approvals" values
                        //* "General dialogue" values to be cleared
                        //* "Personal comment" values to be cleared

                    }



                    else if (type == 2 && (pdpHist.manaptime != null || pdpHist.empaptime != null || pdpHist.comaptime != null))
                    {

                        if (pdpHist == null)
                        {
                            return;
                            //That means no records in pdpHist and no one approved this report

                        }
                        else
                        {
                            //Common.WriteLog("Pdpapprovman :" + Convert.ToString(pdpHist.Pdpapprovman));
                            // Common.WriteLog("PdpManagerID :" + Convert.ToString(pdpHist.PdpManagerID));
                            if (pdpHist.Pdpapprovman != null && pdpHist.PdpManagerID > 0)
                            {
                                //that means both have approved this PDP
                                var lowerAuthDetails = UserBM.GetUserByIdLinq(Convert.ToInt32(pdpHist.PdpUserID));
                                lblLowerAuthName.Text = lowerAuthDetails.userFirstName + " " + lowerAuthDetails.userLastName + " ";
                                lowdate.Text = (pdpHist.empaptime).ToString();
                            }
                            else
                            {


                            }


                            if (pdpHist.PdpManagerID > 0)
                            {
                                var highAuthDetails = UserBM.GetUserByIdLinq(Convert.ToInt32(pdpHist.PdpCompanyID));
                                lblHigherAuthName.Text = highAuthDetails.userFirstName + " " + highAuthDetails.userLastName + " ";
                                hidate.Text = (pdpHist.comaptime).ToString();
                            }

                        }
                    }

                }

            }
            //Common.WriteLog("GetAproveDetails 7");
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "GetAproveDetails");
        }
    }

    [WebMethod(EnableSession = true)]
    public static EmpAppText GetAproveDetailsemp(int umId)
    {
        EmpAppText empapptextobj = new EmpAppText();
        var db = new startetkuEntities1();

        //Get pdphistory records by Employee/manager

        var idOfLowerUser = 0;

        //if (Convert.ToInt32(HttpContext.Current.Session["OrguserType"]) == 2)
        //{

        //     idOfLowerUser = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        //}
        //else if (Convert.ToInt32(HttpContext.Current.Session["OrguserType"]) == 3)
        //{

        //    idOfLowerUser = Convert.ToInt32(HttpContext.Current.Session["userCreateBy"]);
        //}
        //else if (Convert.ToInt32(HttpContext.Current.Session["OrguserType"]) == 1)
        //{

        //    idOfLowerUser = Convert.ToInt32(Request.QueryString["umId"]);
        //}
        try
        {


            idOfLowerUser = umId;

            var pdpHist = db.PDPHistories.FirstOrDefault(o => o.PdpUserID == idOfLowerUser);
            var userDetail = UserBM.GetUserByIdLinq(idOfLowerUser);
            var type = userDetail.userType;
            if (pdpHist != null)
            {

                if (type == 3 && (pdpHist.manaptime != null || pdpHist.empaptime != null || pdpHist.comaptime != null))
                {
                    if (pdpHist.Pdpapprovemp != null && pdpHist.PdpManagerID > 0)
                    {



                        //that means both have approved this PDP
                        var lowerAuthDetails = UserBM.GetUserByIdLinq(Convert.ToInt32(pdpHist.PdpUserID));
                        //lblLowerAuthName.Text =; 
                        //lowdate.Text = ;







                        empapptextobj.lowerAuthDetails = lowerAuthDetails.userFirstName + " " + lowerAuthDetails.userLastName + " ";
                        //empapptextobj.empaptime = (pdpHist.empaptime).ToString();
                        empapptextobj.empaptime = CommonUtilities.GetCurrentDateTime().ToString("dd-MM-yyyy HH:mm:ss").Replace("-", "/");

                        // return empapptextobj;
                    }



                    if (pdpHist.PdpManagerID > 0)
                    {
                        var highAuthDetails = UserBM.GetUserByIdLinq(Convert.ToInt32(pdpHist.PdpManagerID));

                        //lblHigherAuthName.Text = highAuthDetails.userFirstName;
                        //hidate.Text = (pdpHist.manaptime).ToString();

                        //var dict = new Dictionary<int, string>();
                        //dict.Add(1, highAuthDetails.userFirstName);
                        //dict.Add(2, (pdpHist.manaptime).ToString());
                        //var output1 = Newtonsoft.Json.JsonConvert.SerializeObject(dict);



                        empapptextobj.highAuthDetails = highAuthDetails.userFirstName + " " + highAuthDetails.userLastName + " ";
                        empapptextobj.manaptime = (pdpHist.manaptime).ToString();
                        //return empapptextobj;
                        //DataSet ds = obj._ds;
                        //return CommonModule.ConverTableToJson(ds);
                    }

                    //int PdpManagerID = Convert.ToInt32(HttpContext.Current.Session["OrgCreatedBy"]);


                    //var actCatList = from UserMasters in db.UserMasters
                    //                 join PDPHistories in db.PDPHistories on new { userId = UserMasters.userId } equals new { userId = (int)PDPHistories.PdpManagerID } into PDPHistories_join
                    //                 from PDPHistories in PDPHistories_join.DefaultIfEmpty()
                    //                 select new 
                    //                 {
                    //                    UserMasters,PDPHistories
                    //                 };

                    //Apprbyhigh.Text = actCatList.FirstOrDefault(o => o.UserMasters.userId == getManFrmHist).UserMasters.userFirstName;


                    //var GetUserById = UserBM.GetUserByIdLinq(139);


                }



                else if (type == 2 && (pdpHist.manaptime != null || pdpHist.empaptime != null || pdpHist.comaptime != null))
                {
                    if (pdpHist.Pdpapprovman != null && pdpHist.PdpManagerID > 0)
                    {
                        //that means both have approved this PDP
                        var lowerAuthDetails = UserBM.GetUserByIdLinq(Convert.ToInt32(pdpHist.PdpUserID));
                        //lblLowerAuthName.Text = lowerAuthDetails.userFirstName;
                        //lowdate.Text = (pdpHist.empaptime).ToString();
                        //var dict = new Dictionary<int, string>();
                        //dict.Add(1, lowerAuthDetails.userFirstName);
                        //dict.Add(2, (pdpHist.empaptime).ToString());
                        //var output1 = Newtonsoft.Json.JsonConvert.SerializeObject(dict);



                        empapptextobj.lowerAuthDetails = lowerAuthDetails.userFirstName + " " + lowerAuthDetails.userLastName + " ";
                        //  empapptextobj.empaptime = (pdpHist.empaptime).ToString();
                        empapptextobj.empaptime = CommonUtilities.GetCurrentDateTime().ToString("dd-MM-yyyy HH:mm:ss").Replace("-", "/");


                        if (pdpHist.PdpManagerID > 0)
                        {
                            var highAuthDetails = UserBM.GetUserByIdLinq(Convert.ToInt32(pdpHist.PdpCompanyID));
                            //lblHigherAuthName.Text = highAuthDetails.userFirstName;
                            //hidate.Text = (pdpHist.comaptime).ToString();
                            //var dict = new Dictionary<int, string>();
                            //dict.Add(1, highAuthDetails.userFirstName);
                            //dict.Add(2, (pdpHist.comaptime).ToString());
                            //var output1 = Newtonsoft.Json.JsonConvert.SerializeObject(dict);




                            empapptextobj.highAuthDetails = highAuthDetails.userFirstName + " " + highAuthDetails.userLastName + " ";
                            empapptextobj.comaptime = (pdpHist.comaptime).ToString();


                            //return empapptextobj;

                        }




                    }



                    //if (pdpHist.PdpManagerID > 0)
                    //{
                    //    var highAuthDetails = UserBM.GetUserByIdLinq(Convert.ToInt32(pdpHist.PdpCompanyID));
                    //    //lblHigherAuthName.Text = highAuthDetails.userFirstName;
                    //    //hidate.Text = (pdpHist.comaptime).ToString();
                    //    //var dict = new Dictionary<int, string>();
                    //    //dict.Add(1, highAuthDetails.userFirstName);
                    //    //dict.Add(2, (pdpHist.comaptime).ToString());
                    //    //var output1 = Newtonsoft.Json.JsonConvert.SerializeObject(dict);




                    //    empapptextobj.highAuthDetails = highAuthDetails.userFirstName;
                    //    empapptextobj.comaptime = (pdpHist.comaptime).ToString();


                    //    return empapptextobj;

                    //}
                }


            }

        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "GetAproveDetailsemp");
        }
        return empapptextobj;
    }

    [WebMethod(EnableSession = true)]
    public static EmpAppText GetAproveDetailsemp_Detail(int PDPHistID)
    {
        EmpAppText empapptextobj = new EmpAppText();
        var db = new startetkuEntities1();

        //Get pdphistory records by Employee/manager

        var idOfLowerUser = 0;

        try
        {

            var pdpHist = db.PDPHistories.FirstOrDefault(o => o.PdpHisID == PDPHistID);
            var userDetail = UserBM.GetUserByIdLinq(Convert.ToInt32(pdpHist.PdpUserID));
            var type = userDetail.userType;
            if (pdpHist != null)
            {

                if (type == 3 && (pdpHist.manaptime != null || pdpHist.empaptime != null || pdpHist.comaptime != null))
                {
                    if (pdpHist.Pdpapprovemp != null && pdpHist.PdpManagerID > 0)
                    {
                        var lowerAuthDetails = UserBM.GetUserByIdLinq(Convert.ToInt32(pdpHist.PdpUserID));
                        empapptextobj.lowerAuthDetails = lowerAuthDetails.userFirstName + " " + lowerAuthDetails.userLastName + " ";
                        empapptextobj.empaptime = CommonUtilities.GetCurrentDateTime().ToString("dd-MM-yyyy HH:mm:ss").Replace("-", "/");
                    }

                    if (pdpHist.PdpManagerID > 0)
                    {
                        var highAuthDetails = UserBM.GetUserByIdLinq(Convert.ToInt32(pdpHist.PdpManagerID));
                        empapptextobj.highAuthDetails = highAuthDetails.userFirstName + " " + highAuthDetails.userLastName + " ";
                        empapptextobj.manaptime = (pdpHist.manaptime).ToString();
                    }
                }
                else if (type == 2 && (pdpHist.manaptime != null || pdpHist.empaptime != null || pdpHist.comaptime != null))
                {
                    if (pdpHist.Pdpapprovman != null && pdpHist.PdpManagerID > 0)
                    {
                        var lowerAuthDetails = UserBM.GetUserByIdLinq(Convert.ToInt32(pdpHist.PdpUserID));
                        empapptextobj.lowerAuthDetails = lowerAuthDetails.userFirstName + " " + lowerAuthDetails.userLastName + " ";
                        empapptextobj.empaptime = CommonUtilities.GetCurrentDateTime().ToString("dd-MM-yyyy HH:mm:ss").Replace("-", "/");


                        if (pdpHist.PdpManagerID > 0)
                        {
                            var highAuthDetails = UserBM.GetUserByIdLinq(Convert.ToInt32(pdpHist.PdpCompanyID));
                            empapptextobj.highAuthDetails = highAuthDetails.userFirstName + " " + highAuthDetails.userLastName + " ";
                            empapptextobj.comaptime = (pdpHist.comaptime).ToString();
                        }




                    }
                }


            }

        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "GetAproveDetailsemp");
        }
        return empapptextobj;
    }

    #region Tab4
    /// <summary>
    /// Jainam 2017 ma
    /// </summary>
    protected void GetActivityCategory()
    {
        var actCatList = ActivityCategoryLogic.GetActivityCategoryList(ResLangId, Convert.ToInt32(Session["OrgCompanyId"])).Where(o => o.ActCatDeveplan == true).ToList();

        //foreach (var i in actCatList) { }
        for (int i = actCatList.Count() - 1; i >= 0; i--)
        {
            if (actCatList[i] != null)
            {
                var hdnActCatId = actCatList[i].ActCatId;
                var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
                var userId = Convert.ToInt32(Request.QueryString["umId"]);


                var actObj = new GetActivityLists_Result();
                actObj.ActCompanyId = companyId;
                actObj.ActReqUserId = userId;
                actObj.ActCatRefId = Convert.ToInt32(hdnActCatId);
                var activityList1 = ActivityMasterLogic.GetActivityList(actObj, 1, 1).Where(o => o.ActReqStatus != null).ToList();


                if (activityList1.Count() == 0)
                {
                    actCatList.RemoveAt(i);

                }

            }


        }



        gvGridCategory.DataSource = actCatList;

        gvGridCategory.DataBind();




    }

    protected void gvActCat_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(Request.QueryString["umId"]);

        try
        {


            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string hdnActCatId = ((HiddenField)e.Row.FindControl("hdnActCatId")).Value;


                //string id = gvGridCategory.DataKeys[e.Row.RowIndex].Value.ToString();


                var actObj = new GetActivityLists_Result();
                actObj.ActCompanyId = companyId;
                actObj.ActReqUserId = userId;
                actObj.ActCatRefId = Convert.ToInt32(hdnActCatId);
                var activityList = ActivityMasterLogic.GetActivityList(actObj, 1, 1).Where(o => o.ActReqStatus != null).ToList();


                GridView gvGrid_ActivityStatus_Tab4 = e.Row.FindControl("gvGrid_ActivityStatus_Tab4") as GridView;
                if (gvGrid_ActivityStatus_Tab4 != null)
                {
                    gvGrid_ActivityStatus_Tab4.DataSource = activityList;
                    gvGrid_ActivityStatus_Tab4.DataBind();


                    //This will add the <thead> and <tbody> elements
                    gvGrid_ActivityStatus_Tab4.HeaderRow.TableSection = TableRowSection.TableHeader;

                    //This adds the <tfoot> element. 
                    //Remove if you don't have a footer row
                    gvGrid_ActivityStatus_Tab4.FooterRow.TableSection = TableRowSection.TableFooter;
                }

            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "gvActCat_OnRowDataBound");
        }
    }


    //Upload Points 5 2017 Jainam Shah
    protected void GetAllpointSummery()
    {
        Boolean plp = false;
        Boolean alp = false;
        Boolean id = false;
        String name = "";
        plp = false;
        alp = false;
        id = true;


        int userid = 0;
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if ((Convert.ToString(Session["OrguserType"]) == "3"))
            {
                userid = Convert.ToInt32(Session["OrgUserId"]);
            }
            else
            {
                userid = Convert.ToInt32(Request.QueryString["umId"]);
            }
        }
        if (!String.IsNullOrEmpty(Request.QueryString["PLP"]))
        {
            // Points.InnerText = Convert.ToString(Request.QueryString["PLP"]);
            name = Convert.ToString(Request.QueryString["PLP"]);
        }

        else if (!String.IsNullOrEmpty(Request.QueryString["ALP"]))
        {
            //  Points.InnerText = Convert.ToString(Request.QueryString["ALP"]);

            name = Convert.ToString(Request.QueryString["ALP"]);
        }
        else if (!String.IsNullOrEmpty(Request.QueryString["All"]))
        {
            var querystring = Convert.ToString(Request.QueryString["All"]);
            //  Points.InnerText = Regex.Replace(querystring, @"((?<=\p{Ll})\p{Lu})|((?!\A)\p{Lu}(?>\p{Ll}))", " $0");
            name = querystring;
        }
        //   Points.InnerText = "UploadPublicDoc";
        // name = "UploadPublicDoc";
        name = "All";
        PointBM obj = new PointBM();
        obj.GetUserPointByNameDetail(userid, plp, alp, true, id, name);
        DataSet ds = obj.ds;
        if (ds != null)
        {
            if (ds.Tables[1].Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[1].Rows[0]["name"])))
                {
                    lblUserName.Text = Convert.ToString(ds.Tables[1].Rows[0]["name"]);
                }
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                ViewState["logpointinfo"] = ds.Tables[0].Rows[0]["logPointInfo"];

                string strName = Convert.ToString(ds.Tables[0].Rows[0]["logPointInfo"]);



                if (name == "AcceptProvidedHelp")
                {
                    strName = lblAcceptProvidedHelp.Value;
                }
                else if (name == "ReadDoc")
                {
                    strName = lblReadDoc.Value;
                }
                else if (name == "ReadDocByOther")
                {
                    strName = lblReadDocByOther.Value;
                }
                else if (name == "RequestSession")
                {
                    strName = lblRequestSession.Value;
                }
                else if (name == "SuggestSession")
                {
                    strName = lblSuggestSession.Value;
                }

                else if (name == "UploadPublicDoc")
                {
                    strName = "Upload Public document";
                }
                else if (name == "FinalFillCompetences")
                {
                    strName = lblFinalFillCompetences.Value;
                }
                else if (name == "OpenCompetencesScreen")
                {
                    strName = lblOpenCompetencesScreen.Value;
                }
                else if (name == "SignIn")
                {
                    strName = lblSignIn.Value;
                }


                // Points.InnerText = strName;

                //ds.Tables[0].AsEnumerable().ToList().ForEach(row => row["logPointInfo"] = strName);
                //ds.Tables[0].AcceptChanges();
                gvGrid.DataSource = ds.Tables[0];
                gvGrid.DataBind();
                gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;

                AAlp.InnerHtml = Convert.ToString(ds.Tables[1].Rows[0]["TotalPoint"]);
            }
            else
            {
                gvGrid.DataSource = null;
                gvGrid.DataBind();
            }
        }
        else
        {
            gvGrid.DataSource = null;
            gvGrid.DataBind();
        }
    }
    //

    protected void grd_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }

    protected void gvGrid_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            foreach (TableCell cell in e.Row.Cells)
            {
                cell.BorderStyle = BorderStyle.None;
            }
        }
    }



    public int total;
    protected void gvGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            //string sponsorBonus = ((Label)e.Row.FindControl("Label2")).Text;

            //string count = ((HiddenField)e.Row.FindControl("hdnCost")).Value;
            string actReqStatus = ((HiddenField)e.Row.FindControl("hdnActReqStatus")).Value;
            Label lbl = (Label)e.Row.FindControl("lblActReqStatus");
            lbl.Text = GetActivityStatusByStatusId(actReqStatus, false);



            /*Priority*/
            //string hdnActPriority = ((HiddenField)e.Row.FindControl("hdnActPriority")).Value;
            //Label lblActPriority = (Label)e.Row.FindControl("lblActPriority");


            //lblActPriority.Text = ActivityRequestLogic.GetActivityPriorityStatusByNumber(hdnActPriority);


            //if (!string.IsNullOrWhiteSpace(count))
            //{
            //    if (actReqStatus == "2" || actReqStatus == "3" || actReqStatus == "5")
            //    {
            //        total = total + Int32.Parse(count);
            //    }

            //}

        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            //var type = Convert.ToInt32(Session["OrguserType"]);
            //Label lblTotalCost = (Label)e.Row.FindControl("lblTotalCost");
            //lblTotalCost.Text = total.ToString();
            //if (type != 1)
            //{
            //    gvGrid_ActivityStatus_Tab4.ShowFooter = true;
            //}
        }


    }
    protected void gvGrid_RowCommand(object sender, GridViewCommandEventArgs e)
    {


    }

    public string GetActivityStatusByStatusId(string actReqStatus, bool isPrivate)
    {
        /*
     * About Activity Request Stauts:  2017 04 01
     
     * We need to change status on an activity
    Available” ==1
            Activity can be requested
            Should be auto set when creating a public activity
    Requested   ==2
            Activity have been requested by user
            Should be auto set when user request a public activity
    Request approved ==3
            Manager have approved requested activity
            Should be auto set when manager approve users activity request
    Ongoing ==4
            Should be auto set when start date have been reached
    Completed ==5
            employee flag activity has been completed
            Should be set manuel by user (or manager)
    Completed approval ==6
            Manager have approved that activity have been completed by user
            Should be set manual by manager (or system owner)
     * 
     * REJECTED == 7
    */
        //var status = isPrivate ? StringFor.CREATED : StringFor.Available;

        GetResource();
        var status = false ? StringFor.CREATED : StringFor.Available;

        switch (actReqStatus)
        {
            case "2":
                {
                    status = REQUESTED;//getResource.GetResource("REQUESTED.Text");//StringFor.REQUESTED;
                    break;
                }
            case "3":
                {
                    status = APPROVED;// getResource.GetResource("REQUESTED.Text"); //StringFor.APPROVED;
                    break;
                }
            case "4":
                {
                    status = ONGOING;// getResource.GetResource("REQUESTED.Text"); //StringFor.ONGOING;
                    break;
                }
            case "5":
                {
                    status = COMPLETED;// getResource.GetResource("REQUESTED.Text"); //StringFor.COMPLETED;
                    break;
                }
            case "6":
                {
                    status = COMPLETED_APPROVE;//getResource.GetResource("REQUESTED.Text"); //StringFor.COMPLETED_APPROVE;
                    break;
                }
            case "7":
                {
                    status = REJECTED;//getResource.GetResource("REQUESTED.Text"); //StringFor.REJECTED;
                    break;
                }


        }
        return status;
    }

    public string REQUESTED { get; set; }
    public string APPROVED { get; set; }
    public string ONGOING { get; set; }

    public string COMPLETED { get; set; }
    public string COMPLETED_APPROVE { get; set; }
    public string REJECTED { get; set; }

    private void GetResource()
    {
        REQUESTED = GetLocalResourceObject("REQUESTED.Text").ToString();
        APPROVED = GetLocalResourceObject("APPROVED.Text").ToString();
        ONGOING = GetLocalResourceObject("ONGOING.Text").ToString();

        COMPLETED = GetLocalResourceObject("COMPLETED.Text").ToString();
        COMPLETED_APPROVE = GetLocalResourceObject("COMPLETED_APPROVE.Text").ToString();
        REJECTED = GetLocalResourceObject("REJECTED.Text").ToString();
    }


    #endregion


    #region Culture
    protected override void InitializeCulture()
    {
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Denish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {


        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion

    #region OLDPDFCode
    [WebMethod(EnableSession = true)]
    public static string SaveHtmlDataAsPDF_old(string t1, string t2, string t3, string t4, string t5, string styleData)
    {
        //string   strimg = "<img src='" + ImgData1 + "' width='100px' heigth='100px'/>";
        String str = pdf(t1, t2, t3, t4, t5, styleData);
        return str;
        // Jainam Shah -----------code maintainance at ----- 25/4/2017----------------------------------
    }
    //public static string ReadFile(string fileName)
    //{
    //    var returnString = "";

    //    try
    //    {
    //        var path = ConfigurationManager.AppSettings["assetpath"].ToString();
    //        fileName = path + "\\" + fileName;
    //        using (StreamReader stringRead = new StreamReader(fileName))
    //        {
    //            returnString = stringRead.ReadLine();
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        ExceptionLogger.LogException(ex);
    //        returnString = "";
    //    }
    //    return returnString;

    //}
    [WebMethod(EnableSession = true)]
    public static string SaveQuestionHtmlDataAsPDF_old(string t1, string t5, string t6)
    {
        //string tmp = "<style>";
        //tmp = tmp + ReadFile("css\\style.min.css");
        //tmp = tmp + ReadFile("responsive.dataTables.min.css");
        //tmp = tmp + "</style>";
        //tmp = tmp + "<script>";
        //tmp = tmp + ReadFile("dataTables.responsive.min.js");
        //tmp = tmp + ReadFile("jquery.dataTables.min.js");
        //tmp = tmp + "</script>";
        //string tmp = "<style>.chat-thumb > img{width: 15px;height: 15px;float: right;}</style>";
        //t5= tmp + t5;




        QuestionBM obj = new QuestionBM();

        obj.UserId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        obj.queCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
        obj.qpercomID = Convert.ToInt32(0);
        obj.Index = 0;
        obj.IsDisplayInReport = true;
        obj.loginuserId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]); ;
        obj.GetQuestionPersonalComment();
        DataSet ds1 = obj.ds;

        if (ds1 != null)
        {
            if (ds1.Tables[0].Rows.Count > 0)
            {
                if (Convert.ToBoolean(ds1.Tables[0].Rows[0]["IsDisplayInReport"]) == false)
                {
                    t6 = "";
                }
            }
        }
        else
        {
            t6 = "";
        }
        string sitepath = Convert.ToString(ConfigurationManager.AppSettings["imagepath_pdf"]);
        string sitepath1 = Convert.ToString(ConfigurationManager.AppSettings["siteurl"]);


        string tmp = "<style>.grEditRow{display:none;}</style>";
        t1 = tmp + t1;
        t5 = t5.Replace("alt=\"\"", "style=\"width: 40px;height: 40px;\"");
        //t5 = t5.Replace("/starteku/organisation/images", "http://localhost:56482/starteku/organisation/images");
        t5 = t5.Replace("/starteku", sitepath1);
        t5 = t5.Replace("../Log/upload/Userimage", sitepath);

        // t1 = t1 + "<br//><hr//>" + t2 + "<br//><hr//>" + t3 + "<br//><hr//>"  + t5;
        t1 = t1 + "<br/><hr/>" + t5 + "<br/><hr/>" + t6;
        //  t1 = tmp + t1;
        String str = Tab_PDF(t1);
        return str;
    }
    [WebMethod(EnableSession = true)]
    public static string SavePointHtmlDataAsPDF_old(string t1)
    {

        String str = Tab_PDF(t1);
        return str;
    }
    [WebMethod(EnableSession = true)]
    public static string SaveCompetenceHtmlDataAsPDF_old(string t1, string t2)
    {

        t1 = t1 + "<br/><br/>" + t2;
        String str = Tab_PDF(t1);
        return str;
    }
    [WebMethod(EnableSession = true)]
    // public static string SaveIntroManagementDataPDF(string t1, string metTitle, string docTitle, string approvalData)
    public static string SaveIntroManagementDataPDF_oldNew(string t1, string t2, string t3, string t4)
    {
        string docTitle = t3; string approvalData = t4; string metTitle = t2;
        string sitepath = Convert.ToString(ConfigurationManager.AppSettings["imagepath_pdf"]);
        string sitepath1 = Convert.ToString(ConfigurationManager.AppSettings["siteurl"]);

        t1 = t1.Replace("class=\"DirectionElement\"", "style=\"padding: 8px;\"");
        //t1 = t1.Replace("/starteku/organisation/images", "http://localhost:56482/starteku/organisation/images");
        t1 = t1.Replace("/starteku", sitepath1);
        t1 = t1.Replace("../Log/upload/Userimage", sitepath);

        Organisation_PersonalDevelopmentPlan o = new Organisation_PersonalDevelopmentPlan();

        String strMet = "";
        String strDoc = "";
        String strCompDir = "";
        try
        {


            DataSet metStatus = new DataSet();
            metStatus = o.GetStatus();
            if (metStatus != null)
            {

                strMet = "<span style='font-weight:bold; margin-left: -220px;'>" + metTitle + "</span><br/><br/><table border='1' width='800px'>";
                for (int i = 0; i < metStatus.Tables[0].Rows.Count; i++)
                {
                    strMet = strMet + "<tr>";
                    for (int j = 0; j < metStatus.Tables[0].Columns.Count - 1; j++)
                    {
                        strMet = strMet + "<td>" + Convert.ToString(metStatus.Tables[0].Rows[i][j]) + "</td>";
                    }

                    strMet = strMet + "</tr>";

                }
                strMet = strMet + "</table>";
            }

            DataSet docStatus = new DataSet();
            docStatus = o.GetMeetingDocData();
            if (docStatus != null)
            {

                strDoc = "<span style='font-weight:bold;float:left;'>" + docTitle + "</span><br/><br/><table border='1' width='800px'>";
                for (int i = 0; i < docStatus.Tables[0].Rows.Count; i++)
                {
                    strDoc = strDoc + "<tr>";
                    strDoc = strDoc + "<td>" + Convert.ToString(docStatus.Tables[0].Rows[i][1]) + "</td>";
                    strDoc = strDoc + "<td style='width: 33.33%;'>" + (String.IsNullOrEmpty(Convert.ToString(docStatus.Tables[0].Rows[i][3])) ? "&nbsp;" : Convert.ToString(docStatus.Tables[0].Rows[i][3])) + "</td>";
                    //for (int j = 0; j < docStatus.Tables[0].Columns.Count - 1; j++)
                    //{
                    //    strDoc = strDoc + "<td>" + Convert.ToString(docStatus.Tables[0].Rows[i][j]) + "</td>";
                    //}

                    strDoc = strDoc + "</tr>";

                }
                strDoc = strDoc + "</table>";
            }

            CompanyDirectionElement obj = new CompanyDirectionElement();
            obj.UserID = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
            obj.dirCompanyID = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
            obj.Status = "Publish";
            obj.dirCreateDate = DateTime.Now;
            int ResLangId = Convert.ToInt32(HttpContext.Current.Session["resLangID"]);
            obj.dirIndex = ResLangId;
            DataSet ds = new DataSet();
            obj.GetAllUserDirectionList_PDP();
            ds = obj.ds;

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    strCompDir = "<span style='font-weight:bold;float:left;'>" + docTitle + "</span><br/><br/><table width='800px'>";
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        strCompDir = strCompDir + "<tr>";
                        strCompDir = strCompDir + "<td style=\"font-weight: bolder;\">" + Convert.ToString(ds.Tables[0].Rows[i]["CatNameInLang"]) + "</td></tr><tr>";
                        strCompDir = strCompDir + "<td>";

                        DataSet dst = new DataSet();
                        CompanyDirectionElement obj1 = new CompanyDirectionElement();
                        obj1.UserID = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
                        obj1.dirCompanyID = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
                        obj1.Status = "Publish";
                        obj1.dirCreateDate = DateTime.Now;
                        obj1.dirCatID = Convert.ToString(ds.Tables[0].Rows[i]["catID"]);

                        obj1.GetUserAssignDirectionElementList_PDP();
                        dst = obj1.ds;
                        strCompDir = strCompDir + "<table width='800px' style='margin:20px'>";
                        for (int ii = 0; ii < dst.Tables[0].Rows.Count; ii++)
                        {

                            strCompDir = strCompDir + "<tr>";
                            strCompDir = strCompDir + "<td>" + Convert.ToString(dst.Tables[0].Rows[ii]["dirDescription"]) + "</td>";
                            strCompDir = strCompDir + "</tr>";
                        }
                        strCompDir = strCompDir + "</table>";
                        strCompDir = strCompDir + "</td></tr>";
                        //for (int j = 0; j < docStatus.Tables[0].Columns.Count - 1; j++)
                        //{
                        //    strDoc = strDoc + "<td>" + Convert.ToString(docStatus.Tables[0].Rows[i][j]) + "</td>";
                        //}

                        strCompDir = strCompDir + "</tr>";

                    }
                    strCompDir = strCompDir + "</table>";
                }
            }

        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex);
        }
        //  Common.WriteLog(t1);
        t1 = "<div style=\"font-family: Geneva, Arial, Helvetica, sans-serif;\">" + t1 + "<br/><br/><br/><br/><div class=\"col-md-12\" style=\"margin-top:10px;\">" + strMet + "<br/><br/>" + strDoc + "<br/><br/>" + approvalData + "</div></div><br/><br/>";
        t1 = t1 + strCompDir;
        String str = Tab_PDF(t1);
        return str;
    }
    [WebMethod(EnableSession = true)]
    public static string SaveActivityHtmlDataAsPDF_old(string t1)
    {

        string sitepath = Convert.ToString(ConfigurationManager.AppSettings["imagepath_pdf"]);
        string sitepath1 = Convert.ToString(ConfigurationManager.AppSettings["siteurl"]);
        t1 = t1.Replace("/starteku", sitepath1);
        t1 = t1.Replace("../Log/upload/Userimage", sitepath);
        String str = Tab_PDF(t1);
        return str;
    }
    //protected void btnPDF_Click(object sender, EventArgs e)
    //{
    //    /*  HtmlToPdf("images\\", ".pdf", new[] {""});
    //      return;*/

    //    Button btn = (Button)sender;
    //    var head = string.Empty;
    //    var repeatername = "";
    //    switch (btn.CommandName)
    //    {
    //        case "Original":
    //            head = LearningPointLevel.Text;
    //            repeatername = "rowRepeater";
    //            break;

    //    }

    //    Response.ContentType = "application/pdf";
    //    Response.AddHeader("content-disposition", "attachment;filename=Report.pdf");
    //    Response.Cache.SetCacheability(HttpCacheability.NoCache);
    //    StringWriter sw = new StringWriter();
    //    sw.Write("<div style='margin-bottom:10px;'><h1 style='margin:10px;font-size:15px'>" + head + "</h1></div><br/>");
    //    HtmlTextWriter hw = new HtmlTextWriter(sw);

    //    //this.Page.RenderControl(hw); 

    //    switch (btn.CommandName)
    //    {
    //        case "Original":
    //            this.rowRepeater.RenderControl(hw);
    //            break;

    //    }

    //    StringReader sr = new StringReader(sw.ToString().Replace("\r", "").Replace("\n", "").Replace("  ", "").Replace("<img src='../images/wait.gif'/>", ""));

    //    iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.LEGAL);
    //    // pdfDoc.o

    //    pdfDoc.HtmlStyleClass = "body{color:red}";
    //    HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
    //    htmlparser.Style = GenerateStyleSheet();
    //    PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
    //    pdfDoc.Open();
    //    htmlparser.Parse(sr);
    //    pdfDoc.Close();
    //    Response.Write(pdfDoc);
    //    Response.End();
    //    // HtmlToPdf.ConvertUrl(urlOrHtmlFile, outputFileName);
    //}
    //private static void pdf(DataSet ds)
    //{
    //    string strmail = System.IO.File.ReadAllText(ConfigurationManager.AppSettings["Htmlpath"].ToString() + "template/Report.html");
    //    ConfigurationManager.AppSettings["pdfkey"].ToString();

    //    HttpContext.Current.Response.ContentType = "application/pdf";
    //    HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=Report.pdf");

    //    HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
    //    StringReader sr = new StringReader(strmail.ToString().Replace("\r", "").Replace("\n", "").Replace("  ", "").Replace("<img src='../images/wait.gif'/>", ""));

    //    iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.LEGAL);


    //    HTMLWorker htmlparser = new HTMLWorker(pdfDoc);

    //    iTextSharp.text.pdf.PdfWriter.GetInstance(pdfDoc, HttpContext.Current.Response.OutputStream);
    //    pdfDoc.Open();
    //    htmlparser.Parse(sr);
    //    pdfDoc.Close();
    //    HttpContext.Current.Response.Write(pdfDoc);
    //    HttpContext.Current.Response.End();

    //}
    #endregion

    #region pdf create
    [WebMethod(EnableSession = true)]
    public static string SaveHtmlDataAsPDF(string t1, string t2, string t3, string t4, string t5, string t6, string t7, string t8, string t9, string t10, string t11, string t12, string t13, string t14, string t15, string t16, string t17, string t18, string t19, string t20)
    {

        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Convert.ToString(HttpContext.Current.Session["Language"]));
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(Convert.ToString(HttpContext.Current.Session["Language"]));
        string sitepath = Convert.ToString(ConfigurationManager.AppSettings["imagepath_pdf"]);
        string sitepath1 = Convert.ToString(ConfigurationManager.AppSettings["siteurl"]);

        string Page1 = string.Empty;
        string Page2 = string.Empty;
        string Page3 = string.Empty;
        string Page4 = string.Empty;
        string Page5 = string.Empty;
        //First Tab
        Organisation_PersonalDevelopmentPlan o = new Organisation_PersonalDevelopmentPlan();
        string MainPDF = string.Empty;

        try
        {
            //------------------------------------First Tab------------------------------------------------------------------
            MainPDF = MainPDF + "<span style='color:red;'>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "CONFIDENTIAL.Text")) + "</span><br/><br/>";
            MainPDF = MainPDF + "<h1 style='font-size: 23px;font-family: Verdana, Arial, Helvetica, sans-serif;color: #5781bd;'>" + t1 + "</h1><br/><br/>";
            MainPDF = MainPDF + "<table><tr><td>" + t2 + "</td><td><table><tr><td>" + t3 + "/" + t4
                + "</td></tr><tr><td>" + t5 + "</td></tr><tr><td>"
                + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Jobtitle.Text")) + ":\t\t\t\t" + t6 + "</td></tr>"
                + "<tr><td>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Team.Text")) + ":\t\t\t\t" + t7 + "</td></tr>"
                + "<tr><td>" + t8 + "/" + t9 + "</td></tr></table></td></tr></table>";

            // MainPDF = MainPDF + "<br/><br/>";
            MainPDF = MainPDF + "<br/><br/><table width='100%'><tr><td  style='border: solid #000000 1px;'><span style='font-weight:bold;float:left;font-size: larger;'><h2>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "BasicInfo.Text")) + "</h2></span></td></tr></table><br/><br/>";
            MainPDF = MainPDF + "<span style='font-weight:bold;float:left;font-size: larger;'><h3>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "BasicInfo1.Text")) + "</h3></span><br/><br/>";
            String strMet = "";
            String strDoc = "";
            String strCompDir = "";
            try
            {
                string metstrData = "";
                DataSet metStatus = new DataSet();
                metStatus = o.GetStatus();
                if (metStatus != null)
                {
                    strMet = "<br/><br/><table width='100%'><tr><td colspan='3' style='border: solid #000000 1px;'><span style='font-weight:bold;'><h4>" + t10 + "</h4></span></td></tr>";
                    if (metStatus.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < metStatus.Tables[0].Rows.Count; i++)
                        {
                            strMet = strMet + "<tr>";
                            for (int j = 0; j < metStatus.Tables[0].Columns.Count - 1; j++)
                            {
                                //strMet = strMet + "<td style='border: solid #000000 1px;'>" + Convert.ToString(metStatus.Tables[0].Rows[i][j]) + "</td>";
                                metstrData = Convert.ToString(metStatus.Tables[0].Rows[i][j]);
                                if (metstrData == "Completed" || metstrData == "COMPLETED")
                                {
                                    metstrData = Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "COMPLETED.Text"));
                                }
                                else if (metstrData == "Planned" || metstrData == "PLANNED")
                                {
                                    metstrData = Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Planned.Text"));
                                }

                                strMet = strMet + "<td style='border: solid #000000 1px;'>" + metstrData + "</td>";
                            }
                        }
                    }
                    else
                    {
                        strMet = strMet + "<tr style='height:20px;'><td style='border: solid #000000 2px;'>&nbsp;</td><td style='border: solid #000000 2px;'>&nbsp;</td><td style='border: solid #000000 2px;'>&nbsp;</td></tr>";
                    }

                    strMet = strMet + "</table>";
                }
                DataSet docStatus = new DataSet();
                docStatus = o.GetMeetingDocData();
                if (docStatus != null)
                {
                    strDoc = "<br/><br/><table width='100%'><tr><td colspan='3' style='border: solid #000000 1px;'><span style='font-weight:bold;float:left;'><h4>" + t11 + "</h4></span></td></tr>";
                    if (docStatus.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < docStatus.Tables[0].Rows.Count; i++)
                        {
                            strDoc = strDoc + "<tr>";
                            strDoc = strDoc + "<td style='border: solid #000000 1px;width: 33.33%;'>" + Convert.ToString(docStatus.Tables[0].Rows[i]["CreateDate"]) + "</td>";
                            strDoc = strDoc + "<td style='border: solid #000000 1px;width: 33.33%;'>" + Convert.ToString(docStatus.Tables[0].Rows[i]["DocumentName"]) + "</td>";
                            strDoc = strDoc + "<td style='border: solid #000000 1px;width: 33.33%;'>" + (String.IsNullOrEmpty(Convert.ToString(docStatus.Tables[0].Rows[i]["DocumentOrignalName"])) ? "&nbsp;" : Convert.ToString(docStatus.Tables[0].Rows[i]["DocumentOrignalName"])) + "</td>";
                            strDoc = strDoc + "</tr>";
                        }
                    }
                    else
                    {
                        strDoc = strDoc + "<tr style='height:20px;'><td style='border: solid #000000 1px;'>&nbsp;</td><td style='border: solid #000000 1px;'>&nbsp;</td><td style='border: solid #000000 1px;'>&nbsp;</td></tr>";
                    }
                    strDoc = strDoc + "</table>";
                }

                CompanyDirectionElement obj = new CompanyDirectionElement();
                obj.UserID = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
                obj.dirCompanyID = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
                obj.Status = "Publish";
                obj.dirCreateDate = DateTime.Now;
                int ResLangId = Convert.ToInt32(HttpContext.Current.Session["resLangID"]);
                obj.dirIndex = ResLangId;
                DataSet ds = new DataSet();
                obj.GetAllUserDirectionList_PDP();
                ds = obj.ds;

                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        strCompDir = "<span style='font-weight:bold;float:left;font-size: 26px;'><h3>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Organisationaldirection.Text")) + "<h3></span><br/><br/><br/><table width='800px'>";
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            strCompDir = strCompDir + "<tr>";
                            // strCompDir = strCompDir + "<td style=\"font-weight: bolder;\">" + Convert.ToString(ds.Tables[0].Rows[i]["catName"]) + "</td></tr><tr>";
                            if (Convert.ToString(HttpContext.Current.Session["Culture"]) == "Danish")
                            {
                                strCompDir = strCompDir + "<td style=\"font-weight: bolder;\"><h4>" + Convert.ToString(ds.Tables[0].Rows[i]["CatNameInLang"]) + "</h4></td></tr><tr>";
                            }
                            else
                            {
                                strCompDir = strCompDir + "<td style=\"font-weight: bolder;\"><h4>" + Convert.ToString(ds.Tables[0].Rows[i]["CatNameInLang"]) + "</h4></td></tr><tr>";
                            }
                            strCompDir = strCompDir + "<td>";

                            DataSet dst = new DataSet();
                            CompanyDirectionElement obj1 = new CompanyDirectionElement();
                            obj1.UserID = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
                            obj1.dirCompanyID = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
                            obj1.Status = "Publish";
                            obj1.dirCreateDate = DateTime.Now;
                            obj1.dirCatID = Convert.ToString(ds.Tables[0].Rows[i]["catID"]);

                            obj1.GetUserAssignDirectionElementList_PDP();
                            dst = obj1.ds;
                            strCompDir = strCompDir + "<table width='800px' style='margin:20px'>";
                            for (int ii = 0; ii < dst.Tables[0].Rows.Count; ii++)
                            {

                                strCompDir = strCompDir + "<tr>";
                                strCompDir = strCompDir + "<td>" + Convert.ToString(dst.Tables[0].Rows[ii]["dirDescription"]) + "</td>";
                                strCompDir = strCompDir + "</tr>";
                            }
                            strCompDir = strCompDir + "</table>";
                            strCompDir = strCompDir + "</td></tr>";
                            strCompDir = strCompDir + "</tr>";
                        }
                        strCompDir = strCompDir + "</table>";
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
            }
            t12 = t12.Replace("margin-left: -17px;", "");
            t12 = t12.Replace("margin-left: -45px;", "");
            t12 = t12.Replace("margin-left: -50px;", "");
            t12 = t12.Replace("margin-left: -34px;", "");
            t12 = t12.Replace("margin-left: -30px;", "");
            t12 = t12.Replace("margin-left: -20px;", "");
            t12 = t12.Replace("margin-top: -10px;", "");
            t12 = t12.Replace("<span id=\"ContentPlaceHolder1_Label14\" style=\"font-weight: bolder;\">", "<span id=\"ContentPlaceHolder1_Label14\" style=\"font-weight: bolder;\"><h4>");
            t12 = t12.Replace("</span>\n                                                                </div>\n                                                                <div class=\"col-sm-8 enddiv\">", "</h4></span></div><div class=\"col-sm-8 enddiv\">");

            MainPDF = "<div style=\"font-family: Geneva, Arial, Helvetica, sans-serif;\">" + MainPDF + strMet + strDoc + "<br/><br/>" + t12 + "<br/><br/>" + strCompDir + "</div>";
            Page1 = MainPDF;
            //------------------------------------First Tab------------------------------------------------------------------
            //-----------------------------------Second Tab------------------------------------------------------------------
            Page2 = "<span style='color:red;'>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "CONFIDENTIAL.Text")) + "</span><br/><br/>";
            Page2 = Page2 + "<br/><br/><table width='100%'><tr><td  style='border: solid #000000 1px;'><span style='font-weight:bold;float:left;font-size: larger;'><h2>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Question.Text")) + "</h2></span></td></tr></table><br/><br/>";

            t14 = t14.Replace("<span id=\"ContentPlaceHolder1_lbl3\">", "<span style='font-weight:bold;float:left;font-size: larger;'><h3>");
            t14 = t14.Replace("</span>", "</h3></span>");
            t14 = t14.Replace("scope=\"col\" valign=\"top\" align=\"left\"", "style='border: solid #000000 1px;width:50px;' scope=\"col\" valign=\"top\" align=\"left\"");
            t14 = t14.Replace("class=\"hidden-xs ItemStyle\" style=\"width:5%;\" valign=\"top\" align=\"left\"", "style='border: solid #000000 1px;width:5%;' valign=\"top\" align=\"left\"");
            t14 = t14.Replace("class=\"hidden-xs ItemStyle\" style=\"width:85%;\" valign=\"top\" align=\"left\"", "style='border: solid #000000 1px;width:95%;' valign=\"top\" align=\"left\"");

            t13 = t13.Replace("<ul id=\"txtCmtQuestion\">", "<table width='100%' style='border-collapse: collapse;'>");

            t13 = t13.Replace("<span id=\"ContentPlaceHolder1_lbl4\" style=\"font-size:Larger;font-weight:bold;\">", "<span id=\"ContentPlaceHolder1_lbl4\" style=\"font-size:Larger;font-weight:bold;\"><h3>");
            t13 = t13.Replace("</span>", "</h3></span>");

            t13 = t13.Replace("<li class=\"reply\">", "<tr><td class=\"reply\" style='border: solid #000000 1px;width:80%;'>");
            t13 = t13.Replace("</li>", "</td></tr>");
            t13 = t13.Replace("</ul>", "</table>");

            t13 = t13.Replace("<li <tr=\"\"></li>", "");
            t13 = t13.Replace("&gt;", "");
            t13 = t13.Replace("alt=\"\"", "style='width:80px;height:80px;float:left;'");
            t13 = t13.Replace("<p>", "<p style='line-height: 25px;margin: 5px 0 0;'>").Replace("<i class=\"chat-time\">", "<i class=\"chat-time\" style='line-height: 10px;padding: 6px 0px;position: relative;right: 0;'>");//.Replace("</p>", "");

            //t15 = "<span style='font-weight:bold;float:left;font-size: larger;'>" + t15;
            //t15 = t15.Replace("<br/><br/>", "</span><br/><br/><br/>");

            t15 = "<span style='font-weight:bold;float:left;font-size: larger;'><h3>" + t15;
            t15 = t15.Replace("<br/><br/>", "</h3></span><br/><br/><br/>");

            Page2 = Page2 + "<br/><br/>" + t14 + "<br/><br/>" + t13 + "<br/><br/>" + t15;
            MainPDF = MainPDF + "<br/><br/>" + t13 + "<br/><br/>" + t14 + "<br/><br/>" + t15;
            //-----------------------------------Second Tab------------------------------------------------------------------
            //-----------------------------------Third Tab------------------------------------------------------------------
            Page3 = "<span style='color:red;'>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "CONFIDENTIAL.Text")) + "</span><br/><br/>";
            Page3 = Page3 + "<br/><br/><table width='100%'><tr><td  style='border: solid #000000 1px;'><span style='font-weight:bold;float:left;font-size: larger;'><h2>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Points.Text")) + "</h2></span></td></tr></table><br/><br/>";
            Page3 = Page3 + "<br/><br/><table width='100%'><tr><td><span style='font-weight:bold;float:left;'><h3>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Points1.Text")) + "</h3></span></td></tr></table><br/><br/>";

            t17 = t17.Replace("color: white;", "");
            t17 = t17.Replace("color:White;", "");
            t17 = t17.Replace("border-width:0px;", "border-width:1px;border: solid #000000 1px;");
            t17 = t17.Replace("border-width: 0px;", "border-width:1px;border: solid #000000 1px;");
            t17 = t17.Replace("class=\"hidden-xs\" style=\"width:50px;\" align=\"left\"", "style='border: solid #000000 1px;width:50px;'");
            t17 = t17.Replace("class=\"hidden-xs\" align=\"left\"", "style='border: solid #000000 1px;'");
            t17 = t17.Replace("class=\"hidden-xs sorting_1\" style=\"width: 50px;\" tabindex=\"0\" align=\"left", "style='border-width:1px;border: solid #000000 1px;width: 50px;'");

            t17 = "<table style='width: 100%;'><tr><td style='border-width:1px;border: solid #000000 1px;'><span style='font-size:Larger;font-weight:bold;'><h3>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Overview.Text")) + "</h3></span></td></tr>" +
                "<tr><td>" + t17 + "</td></tr></table>";
            t17 = t17.Replace("background-width:100%;", "width: 100%;");

            Page3 = Page3 + t16 + "<br/><br/>" + t17;
            MainPDF = MainPDF + "<br/><br/><span style='font-size:Larger;font-weight:bold;'>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "OverviewPointTitle.Text")) + "</span><br/><br/>" + t16 + "<br/><br/>" + t17;
            //-----------------------------------Third Tab------------------------------------------------------------------
            //-----------------------------------Fourth Tab------------------------------------------------------------------

            Page4 = "<span style='color:red;'>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "CONFIDENTIAL.Text")) + "</span><br/><br/>";
            Page4 = Page4 + "<br/><br/><table width='100%'><tr><td  style='border: solid #000000 1px;'><span style='font-weight:bold;float:left;font-size: larger;'><h2>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "CompetenceDevelopment.Text")) + "</h2></span></td></tr></table><br/><br/>";
            t19 = t19.Replace("width:218pt", "");
            t19 = t19.Replace("width:902px", "");
            t19 = t19.Replace("width:876px", "");
            t19 = t19.Replace("width:1085pt", "");
            t19 = t19.Replace("width=\"290\"", "");
            t19 = t19.Replace("width=\"1447\"", "");
            t19 = t19.Replace("<colgroup>", "");
            t19 = t19.Replace("<col style=\"width:1085pt\" width=\"1447\" />", "");
            t19 = t19.Replace("</colgroup>", "");
            t19 = t19.Replace("<tbody>", "");
            t19 = t19.Replace("\r", "");
            t19 = t19.Replace("\n", "");
            t19 = t19.Replace("\t", "");
            t19 = t19.Replace("width=\"1447\"", "");
            t19 = t19.Replace("<p>", "");
            t19 = t19.Replace("</p>", "");

            Page4 = Page4 + t18 + "<br/><br/>" + t19;
            MainPDF = MainPDF + "<br/><br/>" + t18 + "<br/><br/>" + t19;
            //-----------------------------------Fourth Tab------------------------------------------------------------------
            //------------------------------------5th Tab-------------------------------------------------------------------

            Page5 = "<span style='color:red;'>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "CONFIDENTIAL.Text")) + "</span><br/><br/>";
            Page5 = Page5 + "<br/><br/><table width='100%'><tr><td  style='border: solid #000000 1px;'><span style='font-weight:bold;float:left;font-size: larger;'><h2>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Activitites.Text")) + "</h2></span></td></tr></table><br/><br/>";
            t20 = t20.Replace("<h1>", "<h1 style=\"margin-top: 25px;\">");
            t20 = t20.Replace("color: white;", "");
            t20 = t20.Replace("color:White;", "");
            t20 = t20.Replace("background- width: 100%;", "width: 100%;");
            t20 = t20.Replace("border-width:0px;", "border-width:1px;border: solid #000000 1px;");
            t20 = t20.Replace("border-width: 0px;", "border-width:1px;border: solid #000000 1px;");
            t20 = t20.Replace("class=\"hidden-xs\" style=\"width:50px;\" align=\"left\"", "style='border: solid #000000 1px;width:50px;'");
            t20 = t20.Replace("class=\"hidden-xs\" align=\"left\"", "style='border: solid #000000 1px;'");
            t20 = t20.Replace("class=\"hidden-xs sorting_1\" style=\"width: 50px;\" tabindex=\"0\" align=\"left", "style='border-width:1px;border: solid #000000 1px;width: 50px;'");

            t20 = "<table style='width: 100%;'><tr><td style='border-width:1px;border: solid #000000 1px;'><span style='font-size:Larger;font-weight:bold;'>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Overview.Text")) + "</span></td></tr>" +
               "<tr><td>" + t20 + "</td></tr></table>";
            t20 = t20.Replace("background-width:100%;", "width: 100%;");

            t20 = t20 + "<br/><br/><br/><table width='100%'><tr><td>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Dateres.Text")) + ":</td><td>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Dateres.Text")) + ":</td></tr><tr><td>____________________</td><td>____________________</td></tr><tr><td>" +
                Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Empres.Text")) + "</td><td>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Mangres.Text")) + "</td></tr></table>";//Employee</td><td>Manager</td></tr></table>";


            Page5 = Page5 + t20;
            //------------------------------------5th Tab-------------------------------------------------------------------
            MainPDF = MainPDF.Replace("/askingculture", sitepath1);
            MainPDF = MainPDF.Replace("../Log/upload/Userimage", sitepath);

            Page1 = Page1.Replace("/askingculture", sitepath1);
            Page1 = Page1.Replace("../Log/upload/Userimage", sitepath);

            Page2 = Page2.Replace("/askingculture", sitepath1);
            Page2 = Page2.Replace("../Log/upload/Userimage", sitepath);

            Page3 = Page3.Replace("/askingculture", sitepath1);
            Page3 = Page3.Replace("../Log/upload/Userimage", sitepath);

            Page4 = Page4.Replace("/askingculture", sitepath1);
            Page4 = Page4.Replace("../Log/upload/Userimage", sitepath);

            Page5 = Page5.Replace("/askingculture", sitepath1);
            Page5 = Page5.Replace("../Log/upload/Userimage", sitepath);
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "SaveHtmlDataAsPDF");
        }
        String str = Fullpdf(Page1, Page2, Page3, Page4, Page5);
        //  String str = Tab_PDF(MainPDF);
        //  String str = pdf_Competence(MainPDF, t20);
        return str;
    }
    [WebMethod(EnableSession = true)]
    public static string SaveQuestionHtmlDataAsPDF(string t1, string t2, string t3, string t4, string t5, string t6, string t7, string t8, string t9, string t10, string t11, string t12)
    {
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Convert.ToString(HttpContext.Current.Session["Language"]));
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(Convert.ToString(HttpContext.Current.Session["Language"]));
        Organisation_PersonalDevelopmentPlan o = new Organisation_PersonalDevelopmentPlan();
        string MainPDF = string.Empty;
        string sitepath = Convert.ToString(ConfigurationManager.AppSettings["imagepath_pdf"]);
        string sitepath1 = Convert.ToString(ConfigurationManager.AppSettings["siteurl"]);

        try
        {
            MainPDF = MainPDF + "<span style='color:red;'>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "CONFIDENTIAL.Text")) + "</span><br/><br/>";
            MainPDF = MainPDF + "<h1 style='font-size: 23px;font-family: Verdana, Arial, Helvetica, sans-serif;color: #5781bd;'>" + t1 + "</h1><br/><br/>";

            MainPDF = MainPDF + "<table><tr><td>" + t2 + "</td><td><table><tr><td>" + t3 + "/" + t4
                + "</td></tr><tr><td>" + t5 + "</td></tr><tr><td>"
                + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Jobtitle.Text")) + ":\t\t\t\t" + t6 + "</td></tr>"
                + "<tr><td>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Team.Text")) + ":\t\t\t\t" + t7 + "</td></tr>"
                + "<tr><td>" + t8 + "/" + t9 + "</td></tr></table></td></tr></table>";

            MainPDF = MainPDF + "<br/><br/><table width='100%'><tr><td  style='border: solid #000000 1px;'><span style='font-weight:bold;float:left;font-size: larger;'><h2>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Question.Text")) + "</h2></span></td></tr></table><br/><br/>";

            t10 = t10.Replace("<span id=\"ContentPlaceHolder1_lbl3\">", "<span style='font-weight:bold;float:left;font-size: larger;'><h3>");
            t10 = t10.Replace("</span>", "</h3></span>");
            t10 = t10.Replace("scope=\"col\" valign=\"top\" align=\"left\"", "style='border: solid #000000 1px;width:50px;' scope=\"col\" valign=\"top\" align=\"left\"");
            t10 = t10.Replace("class=\"hidden-xs ItemStyle\" style=\"width:5%;\" valign=\"top\" align=\"left\"", "style='border: solid #000000 1px;width:5%;' valign=\"top\" align=\"left\"");
            t10 = t10.Replace("class=\"hidden-xs ItemStyle\" style=\"width:85%;\" valign=\"top\" align=\"left\"", "style='border: solid #000000 1px;width:95%;' valign=\"top\" align=\"left\"");

            t11 = t11.Replace("<ul id=\"txtCmtQuestion\">", "<table width='100%' style='border-collapse: collapse;'>");
            t11 = t11.Replace("<span id=\"ContentPlaceHolder1_lbl4\" style=\"font-size:Larger;font-weight:bold;\">", "<span id=\"ContentPlaceHolder1_lbl4\" style=\"font-size:Larger;font-weight:bold;\"><h3>");
            t11 = t11.Replace("</span>", "</h3></span>");
            t11 = t11.Replace("<li class=\"reply\">", "<tr><td class=\"reply\" style='border: solid #000000 1px;width:80%;'>");
            t11 = t11.Replace("</li>", "</td></tr>");
            t11 = t11.Replace("</ul>", "</table>");

            t11 = t11.Replace("<li <tr=\"\"></li>", "");
            t11 = t11.Replace("&gt;", "");
            t11 = t11.Replace("alt=\"\"", "style='width:80px;height:80px;float:left;'");
            t11 = t11.Replace("<p>", "<p style='line-height: 25px;margin: 5px 0 0;'>").Replace("<i class=\"chat-time\">", "<i class=\"chat-time\" style='line-height: 10px;padding: 6px 0px;position: relative;right: 0;'>");//.Replace("</p>", "");


            t12 = "<span style='font-weight:bold;float:left;font-size: larger;'><h3>" + t12;
            t12 = t12.Replace("<br/><br/>", "</h3></span><br/><br/><br/>");

            MainPDF = MainPDF + "<br/><br/>" + t10 + "<br/><br/>" + t11 + "<br/><br/>" + t12;
            MainPDF = MainPDF.Replace("/askingculture", sitepath1);
            MainPDF = MainPDF.Replace("../Log/upload/Userimage", sitepath);
        }
        catch (Exception ex)
        {
        }
        String str = Tab_PDF(MainPDF);
        return str;
    }
    [WebMethod(EnableSession = true)]
    public static string SavePointHtmlDataAsPDF(string t1, string t2, string t3, string t4, string t5, string t6, string t7, string t8, string t9, string t10, string t11)
    {
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Convert.ToString(HttpContext.Current.Session["Language"]));
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(Convert.ToString(HttpContext.Current.Session["Language"]));
        Organisation_PersonalDevelopmentPlan o = new Organisation_PersonalDevelopmentPlan();
        string MainPDF = string.Empty;
        string sitepath = Convert.ToString(ConfigurationManager.AppSettings["imagepath_pdf"]);
        string sitepath1 = Convert.ToString(ConfigurationManager.AppSettings["siteurl"]);

        try
        {
            MainPDF = MainPDF + "<span style='color:red;'>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "CONFIDENTIAL.Text")) + "</span><br/><br/>";
            MainPDF = MainPDF + "<h1 style='font-size: 23px;font-family: Verdana, Arial, Helvetica, sans-serif;color: #5781bd;'>" + t1 + "</h1><br/><br/>";



            MainPDF = MainPDF + "<table><tr><td>" + t2 + "</td><td><table><tr><td>" + t3 + "/" + t4
                + "</td></tr><tr><td>" + t5 + "</td></tr><tr><td>"
                + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Jobtitle.Text")) + ":\t\t\t\t" + t6 + "</td></tr>"
                + "<tr><td>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Team.Text")) + ":\t\t\t\t" + t7 + "</td></tr>"
                + "<tr><td>" + t8 + "/" + t9 + "</td></tr></table></td></tr></table>";

            MainPDF = MainPDF + "<br/><br/><table width='100%'><tr><td  style='border: solid #000000 1px;'><span style='font-weight:bold;float:left;font-size: larger;'><h2>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Points.Text")) + "</h2></span></td></tr></table><br/><br/>";

            t11 = t11.Replace("color: white;", "");
            t11 = t11.Replace("color:White;", "");
            t11 = t11.Replace("border-width:0px;", "border-width:1px;border: solid #000000 1px;");
            t11 = t11.Replace("border-width: 0px;", "border-width:1px;border: solid #000000 1px;");
            t11 = t11.Replace("class=\"hidden-xs\" style=\"width:50px;\" align=\"left\"", "style='border: solid #000000 1px;width:50px;'");
            t11 = t11.Replace("class=\"hidden-xs\" align=\"left\"", "style='border: solid #000000 1px;'");
            t11 = t11.Replace("class=\"hidden-xs sorting_1\" style=\"width: 50px;\" tabindex=\"0\" align=\"left", "style='border-width:1px;border: solid #000000 1px;width: 50px;'");

            t11 = "<table style='width: 100%;'><tr><td style='border-width:1px;border: solid #000000 1px;'><span style='font-size:Larger;font-weight:bold;'><h3>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Overview.Text")) + "</h3></span></td></tr>" +
                "<tr><td>" + t11 + "</td></tr></table>";
            t11 = t11.Replace("background-width:100%;", "width: 100%;");

            MainPDF = MainPDF + "<br/><br/><span style='font-size:Larger;font-weight:bold;'>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "OverviewPointTitle.Text")) + "</span><br/><br/>" + t10 + "<br/><br/>" + t11;
            MainPDF = MainPDF.Replace("/askingculture", sitepath1);
            MainPDF = MainPDF.Replace("../Log/upload/Userimage", sitepath);
        }
        catch (Exception ex)
        {
        }
        String str = Tab_PDF(MainPDF);
        return str;
    }
    [WebMethod(EnableSession = true)]
    public static string SaveCompetenceHtmlDataAsPDF(string t1, string t2, string t3, string t4, string t5, string t6, string t7, string t8, string t9, string t10, string t11, string t12, string t13, string t14, string t15)
    {
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Convert.ToString(HttpContext.Current.Session["Language"]));
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(Convert.ToString(HttpContext.Current.Session["Language"]));
        Organisation_PersonalDevelopmentPlan o = new Organisation_PersonalDevelopmentPlan();
        string MainPDF = string.Empty;
        string sitepath = Convert.ToString(ConfigurationManager.AppSettings["imagepath_pdf"]);
        string sitepath1 = Convert.ToString(ConfigurationManager.AppSettings["siteurl"]);

        try
        {
            MainPDF = MainPDF + "<span style='color:red;'>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "CONFIDENTIAL.Text")) + "</span><br/><br/>";
            MainPDF = MainPDF + "<h1 style='font-size: 23px;font-family: Verdana, Arial, Helvetica, sans-serif;color: #5781bd;'>" + t1 + "</h1><br/><br/>";



            MainPDF = MainPDF + "<table><tr><td>" + t2 + "</td><td><table><tr><td>" + t3 + "/" + t4
                + "</td></tr><tr><td>" + t5 + "</td></tr><tr><td>"
                + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Jobtitle.Text")) + ":\t\t\t\t" + t6 + "</td></tr>"
                + "<tr><td>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Team.Text")) + ":\t\t\t\t" + t7 + "</td></tr>"
                + "<tr><td>" + t8 + "/" + t9 + "</td></tr></table></td></tr></table>";


            MainPDF = MainPDF + "<br/><br/>";
            MainPDF = MainPDF + "<br/><br/><table width='100%'><tr><td  style='border: solid #000000 1px;'><span style='font-weight:bold;float:left;font-size: larger;'><h2>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "CompetenceDevelopment.Text")) + "</h2></span></td></tr></table><br/><br/>";

            MainPDF = MainPDF + "<span style='font-weight:bold;float:left;font-size: larger;'><h3>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "BasicInfo1.Text")) + "</h3></span>";
            String strMet = "";
            String strDoc = "";
            String strCompDir = "";
            try
            {

                string metstrData = "";
                DataSet metStatus = new DataSet();
                metStatus = o.GetStatus();
                if (metStatus != null)
                {

                    strMet = "<br/><br/><table width='100%' style='border-collapse: collapse;'><tr><td colspan='3'><span style='font-weight:bold;'><h4>" + t10 + "</h4></span></td></tr>";
                    if (metStatus.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < metStatus.Tables[0].Rows.Count; i++)
                        {
                            strMet = strMet + "<tr>";
                            for (int j = 0; j < metStatus.Tables[0].Columns.Count - 1; j++)
                            {
                                // strMet = strMet + "<td style='border: solid #000000 2px;'>" + Convert.ToString(metStatus.Tables[0].Rows[i][j]) + "</td>";
                                metstrData = Convert.ToString(metStatus.Tables[0].Rows[i][j]);
                                if (metstrData == "Completed" || metstrData == "COMPLETED")
                                {
                                    metstrData = Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "COMPLETED.Text"));
                                }
                                else if (metstrData == "Planned" || metstrData == "PLANNED")
                                {
                                    metstrData = Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Planned.Text"));
                                }

                                strMet = strMet + "<td style='border: solid #000000 1px;'>" + metstrData + "</td>";
                            }

                            //   strMet = strMet + "<td style='border: solid #000000 2px;'></td></tr>";

                        }
                    }
                    else
                    {
                        strMet = strMet + "<tr style='height:20px;'><td style='border: solid #000000 2px;'>&nbsp;</td><td style='border: solid #000000 2px;'>&nbsp;</td><td style='border: solid #000000 2px;'>&nbsp;</td></tr>";
                    }

                    strMet = strMet + "</table>";
                }



                DataSet docStatus = new DataSet();
                docStatus = o.GetMeetingDocData();
                if (docStatus != null)
                {

                    strDoc = "<br/><br/><table width='100%' style='border-collapse: collapse;'><tr><td colspan='3'><span style='font-weight:bold;float:left;'><h4>" + t11 + "</h4></span></td></tr>";
                    if (docStatus.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < docStatus.Tables[0].Rows.Count; i++)
                        {
                            strDoc = strDoc + "<tr>";
                            strDoc = strDoc + "<td style='border: solid #000000 1px;width: 33.33%;'>" + Convert.ToString(docStatus.Tables[0].Rows[i]["CreateDate"]) + "</td>";
                            strDoc = strDoc + "<td style='border: solid #000000 1px;width: 33.33%;'>" + Convert.ToString(docStatus.Tables[0].Rows[i]["DocumentName"]) + "</td>";
                            strDoc = strDoc + "<td style='border: solid #000000 1px;width: 33.33%;'>" + (String.IsNullOrEmpty(Convert.ToString(docStatus.Tables[0].Rows[i]["DocumentOrignalName"])) ? "&nbsp;" : Convert.ToString(docStatus.Tables[0].Rows[i]["DocumentOrignalName"])) + "</td>";
                            strDoc = strDoc + "</tr>";
                            //for (int j = 0; j < docStatus.Tables[0].Columns.Count - 1; j++)
                            //{
                            //    strDoc = strDoc + "<td>" + Convert.ToString(docStatus.Tables[0].Rows[i][j]) + "</td>";
                            //}

                            strDoc = strDoc + "</tr>";

                        }
                    }
                    else
                    {
                        strDoc = strDoc + "<tr style='height:20px;'><td style='border: solid #000000 1px;'>&nbsp;</td><td style='border: solid #000000 1px;'>&nbsp;</td><td style='border: solid #000000 1px;'>&nbsp;</td></tr>";
                    }
                    strDoc = strDoc + "</table>";
                }

                t12 = t12.Replace("margin-left: -17px;", "");
                t12 = t12.Replace("margin-left: -50px;", "");
                t12 = t12.Replace("<span id=\"ContentPlaceHolder1_Label14\" style=\"font-weight: bolder;\">", "<span id=\"ContentPlaceHolder1_Label14\" style=\"font-weight: bolder;\"><h4>");
                t12 = t12.Replace("</span>\n                                                                </div>\n                                                                <div class=\"col-sm-8 enddiv\">", "</h4></span></div><div class=\"col-sm-8 enddiv\">");

                t14 = t14.Replace("width:218pt", "");
                t14 = t14.Replace("width:902px", "");
                t14 = t14.Replace("width:876px", "");
                t14 = t14.Replace("width:1085pt", "");
                t14 = t14.Replace("width=\"290\"", "");
                t14 = t14.Replace("width=\"1447\"", "");
                t14 = t14.Replace("<colgroup>", "");
                t14 = t14.Replace("<col style=\"width:1085pt\" width=\"1447\" />", "");
                t14 = t14.Replace("</colgroup>", "");
                t14 = t14.Replace("<tbody>", "");
                t14 = t14.Replace("\r", "");
                t14 = t14.Replace("\n", "");
                t14 = t14.Replace("\t", "");
                t14 = t14.Replace("width=\"1447\"", "");
                t14 = t14.Replace("<p>", "");
                t14 = t14.Replace("</p>", "");


                t15 = t15.Replace("background-width:100%;", "width: 100%;");
                t15 = t15.Replace("<h1>", "<h1 style=\"margin-top: 25px;\">");
                t15 = t15.Replace("color: white;", "");
                t15 = t15.Replace("color:White;", "");
                t15 = t15.Replace("border-width:0px;", "border-width:1px;border: solid #000000 1px;");
                t15 = t15.Replace("border-width: 0px;", "border-width:1px;border: solid #000000 1px;");
                t15 = t15.Replace("class=\"hidden-xs\" style=\"width:50px;\" align=\"left\"", "style='border: solid #000000 1px;width:50px;'");
                t15 = t15.Replace("class=\"hidden-xs\" align=\"left\"", "style='border: solid #000000 1px;'");
                t15 = t15.Replace("class=\"hidden-xs sorting_1\" style=\"width: 50px;\" tabindex=\"0\" align=\"left", "style='border-width:1px;border: solid #000000 1px;width: 50px;'");

                t15 = "<span style='font-size: x-large;;font-weight:bold;'><h3>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Activitites.Text")) + "</h3><br/><br/><br/>" + t15;
                t15 = t15.Replace("background-width:100%;", "width: 100%;");
                t15 = t15.Replace("background- width: 100%;", "width: 100%;");

                t15 = t15 + "<br/><br/><br/><table width='100%'><tr><td>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Dateres.Text")) + ":</td><td>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Dateres.Text")) + ":</td></tr><tr><td>____________________</td><td>____________________</td></tr><tr><td>" +
                    Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Empres.Text")) + "</td><td>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Mangres.Text")) + "</td></tr></table>";

                MainPDF = "<div style=\"font-family: Geneva, Arial, Helvetica, sans-serif;\">" + MainPDF + strMet + "<br/><br/>" + strDoc + "<br/><br/>" + t12 + "<br/><br/>"
                    + t13 + "<br/><br/>" + t14 + "<br/><br/>" + "</div>";


                //t1 = "<div style=\"font-family: Geneva, Arial, Helvetica, sans-serif;\">" + t1 + "<br/><br/><br/><br/><div class=\"col-md-12\" style=\"margin-top:10px;\">" + strMet + "<br/><br/>" + strDoc + "<br/><br/>" + approvalData + "</div></div><br/><br/>";
                //t1 = t1 + strCompDir;
                MainPDF = MainPDF.Replace("/askingculture", sitepath1);
                MainPDF = MainPDF.Replace("../Log/upload/Userimage", sitepath);
            }
            catch (Exception ex)
            {
            }
        }
        catch (Exception ex)
        {
        }
        String str = pdf_Competence(MainPDF, t15);
        // String str = Tab_PDF(MainPDF);
        return str;
    }
    [WebMethod(EnableSession = true)]
    public static string SaveIntroManagementDataPDF(string t1, string t2, string t3, string t4, string t5, string t6, string t7, string t8, string t9, string t10, string t11, string t12)
    {
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Convert.ToString(HttpContext.Current.Session["Language"]));
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(Convert.ToString(HttpContext.Current.Session["Language"]));
        Organisation_PersonalDevelopmentPlan o = new Organisation_PersonalDevelopmentPlan();
        string MainPDF = string.Empty;
        string sitepath = Convert.ToString(ConfigurationManager.AppSettings["imagepath_pdf"]);
        string sitepath1 = Convert.ToString(ConfigurationManager.AppSettings["siteurl"]);

        try
        {
            //MainPDF = "<body><div id=\"header\"> <h2>Annual Report of our Company</h2>   </div><div id=\"footer\"><span class=\"custom-footer-page-number\">Number: </span></div> <div class=\"custom-page-start\" style=\"page-break-before: always;\">";
            //MainPDF=MainPDF+"<style> body { font: 12pt Georgia, \"Times New Roman\", Times, serif;  line-height: 1.3;}  @page {     /* switch to landscape */   size: landscape;    /* set page margins */    margin: 0.5cm;    /* Default footers */    @bottom-left {      content: \"Department of Strategy\";    }    @bottom-right {      content: counter(page) \" of \" counter(pages);  }}";
            //MainPDF=MainPDF+"#header {   position: fixed;  width: 100%;   top: 0;   left: 0;   right: 0;}#footer {  position: fixed;   width: 100%;   bottom: 0;   left: 0;  right: 0;}";
            //MainPDF=MainPDF+"body {    padding-top: 50px;}.custom-page-start {    margin-top: 50px;}";
            //MainPDF=MainPDF+".custom-footer-page-number:after {  content: counter(page);}</style>";

            //  MainPDF = MainPDF + "<span style='color:red;'>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "CONFIDENTIAL.Text")) + "</span><br/><br/>";
            MainPDF = MainPDF + "<h1 style='font-size: 23px;font-family: Verdana, Arial, Helvetica, sans-serif;color: #5781bd;'>" + t1 + "</h1><br/><br/>";



            MainPDF = MainPDF + "<table><tr><td>" + t2 + "</td><td><table><tr><td>" + t3 + "/" + t4
                + "</td></tr><tr><td>" + t5 + "</td></tr><tr><td>"
                + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Jobtitle.Text")) + ":\t\t\t\t" + t6 + "</td></tr>"
                + "<tr><td>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Team.Text")) + ":\t\t\t\t" + t7 + "</td></tr>"
                + "<tr><td>" + t8 + "/" + t9 + "</td></tr></table></td></tr></table>";

            MainPDF = MainPDF + "<br/><br/><table width='100%'><tr><td  style='border: solid #000000 1px;'><span style='font-weight:bold;float:left;font-size: larger;'><h2>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "BasicInfo.Text")) + "</h2></span></td></tr></table>";
            MainPDF = MainPDF + "<br/><br/>";
            MainPDF = MainPDF + "<span style='font-weight:bold;float:left;font-size: larger;'><h3>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "BasicInfo1.Text")) + "</h3></span>";
            String strMet = "";
            String strDoc = "";
            String strCompDir = "";
            try
            {
                string metstrData = "";
                DataSet metStatus = new DataSet();
                metStatus = o.GetStatus();
                if (metStatus != null)
                {
                    strMet = "<br/><br/><table width='100%'><tr><td colspan='3' style='border: solid #000000 1px;'><span style='font-weight:bold;'><h4>" + t10 + "</h4></span></td></tr>";
                    if (metStatus.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < metStatus.Tables[0].Rows.Count; i++)
                        {
                            strMet = strMet + "<tr>";
                            for (int j = 0; j < metStatus.Tables[0].Columns.Count - 1; j++)
                            {
                                metstrData = Convert.ToString(metStatus.Tables[0].Rows[i][j]);
                                if (metstrData == "Completed" || metstrData == "COMPLETED")
                                {
                                    metstrData = Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "COMPLETED.Text"));
                                }
                                else if (metstrData == "Planned" || metstrData == "PLANNED")
                                {
                                    metstrData = Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Planned.Text"));
                                }

                                strMet = strMet + "<td style='border: solid #000000 1px;'>" + metstrData + "</td>";
                            }
                            //   strMet = strMet + "<td style='border: solid #000000 2px;'></td></tr>";
                        }
                    }
                    else
                    {
                        strMet = strMet + "<tr style='height:20px;'><td style='border: solid #000000 2px;'>&nbsp;</td><td style='border: solid #000000 2px;'>&nbsp;</td><td style='border: solid #000000 2px;'>&nbsp;</td></tr>";
                    }

                    strMet = strMet + "</table>";
                }
                DataSet docStatus = new DataSet();
                docStatus = o.GetMeetingDocData();
                if (docStatus != null)
                {

                    strDoc = "<br/><br/><table width='100%'><tr><td colspan='3' style='border: solid #000000 1px;'><span style='font-weight:bold;float:left;'><h4>" + t11 + "</h4></span></td></tr>";
                    if (docStatus.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < docStatus.Tables[0].Rows.Count; i++)
                        {
                            strDoc = strDoc + "<tr>";
                            strDoc = strDoc + "<td style='border: solid #000000 1px;width: 33.33%;'>" + Convert.ToString(docStatus.Tables[0].Rows[i]["CreateDate"]) + "</td>";
                            strDoc = strDoc + "<td style='border: solid #000000 1px;width: 33.33%;'>" + Convert.ToString(docStatus.Tables[0].Rows[i]["DocumentName"]) + "</td>";
                            strDoc = strDoc + "<td style='border: solid #000000 1px;width: 33.33%;'>" + (String.IsNullOrEmpty(Convert.ToString(docStatus.Tables[0].Rows[i]["DocumentOrignalName"])) ? "&nbsp;" : Convert.ToString(docStatus.Tables[0].Rows[i]["DocumentOrignalName"])) + "</td>";
                            //for (int j = 0; j < docStatus.Tables[0].Columns.Count - 1; j++)
                            //{
                            //    strDoc = strDoc + "<td>" + Convert.ToString(docStatus.Tables[0].Rows[i][j]) + "</td>";
                            //}

                            strDoc = strDoc + "</tr>";

                        }
                    }
                    else
                    {
                        strDoc = strDoc + "<tr style='height:20px;'><td style='border: solid #000000 1px;'>&nbsp;</td><td style='border: solid #000000 1px;'>&nbsp;</td><td style='border: solid #000000 1px;'>&nbsp;</td></tr>";
                    }
                    strDoc = strDoc + "</table>";
                }

                CompanyDirectionElement obj = new CompanyDirectionElement();
                obj.UserID = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
                obj.dirCompanyID = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
                obj.Status = "Publish";
                obj.dirCreateDate = DateTime.Now;

                int ResLangId = Convert.ToInt32(HttpContext.Current.Session["resLangID"]);
                obj.dirIndex = ResLangId;

                DataSet ds = new DataSet();
                obj.GetAllUserDirectionList_PDP();
                ds = obj.ds;

                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        strCompDir = "<span style='font-weight:bold;float:left;font-size: 26px;'><h3>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Organisationaldirection.Text")) + "</h3></span><br/><br/><br/><table width='800px'>";
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            strCompDir = strCompDir + "<tr>";
                            if (Convert.ToString(HttpContext.Current.Session["Culture"]) == "Danish")
                            {
                                strCompDir = strCompDir + "<td style=\"font-weight: bolder;\"><h4>" + Convert.ToString(ds.Tables[0].Rows[i]["CatNameInLang"]) + "</h4></td></tr><tr>";
                            }
                            else
                            {
                                strCompDir = strCompDir + "<td style=\"font-weight: bolder;\"><h4>" + Convert.ToString(ds.Tables[0].Rows[i]["CatNameInLang"]) + "</h4></td></tr><tr>";
                            }
                            strCompDir = strCompDir + "<td>";

                            DataSet dst = new DataSet();
                            CompanyDirectionElement obj1 = new CompanyDirectionElement();
                            obj1.UserID = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
                            obj1.dirCompanyID = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
                            obj1.Status = "Publish";
                            obj1.dirCreateDate = DateTime.Now;
                            obj1.dirCatID = Convert.ToString(ds.Tables[0].Rows[i]["catID"]);

                            obj1.GetUserAssignDirectionElementList_PDP();
                            dst = obj1.ds;
                            strCompDir = strCompDir + "<table width='800px' style='margin:20px'>";
                            for (int ii = 0; ii < dst.Tables[0].Rows.Count; ii++)
                            {

                                strCompDir = strCompDir + "<tr>";
                                strCompDir = strCompDir + "<td>" + Convert.ToString(dst.Tables[0].Rows[ii]["dirDescription"]) + "</td>";
                                strCompDir = strCompDir + "</tr>";
                            }
                            strCompDir = strCompDir + "</table>";
                            strCompDir = strCompDir + "</td></tr>";
                            //for (int j = 0; j < docStatus.Tables[0].Columns.Count - 1; j++)
                            //{
                            //    strDoc = strDoc + "<td>" + Convert.ToString(docStatus.Tables[0].Rows[i][j]) + "</td>";
                            //}

                            strCompDir = strCompDir + "</tr>";

                        }
                        strCompDir = strCompDir + "</table>";
                    }
                }

            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
            }
            //  Common.WriteLog(t1);
            t12 = t12.Replace("margin-left: -17px;", "");
            t12 = t12.Replace("margin-left: -45px;", "");
            t12 = t12.Replace("margin-left: -50px;", "");
            t12 = t12.Replace("margin-left: -34px;", "");
            t12 = t12.Replace("margin-left: -30px;", "");
            t12 = t12.Replace("margin-left: -20px;", "");
            t12 = t12.Replace("margin-top: -10px;", "");

            t12 = t12.Replace("<span id=\"ContentPlaceHolder1_Label14\" style=\"font-weight: bolder;\">", "<span id=\"ContentPlaceHolder1_Label14\" style=\"font-weight: bolder;\"><h4>");
            t12 = t12.Replace("</span>\n                                                                </div>\n                                                                <div class=\"col-sm-8 enddiv\">", "</h4></span></div><div class=\"col-sm-8 enddiv\">");

            MainPDF = "<div style=\"font-family: Geneva, Arial, Helvetica, sans-serif;\">" + MainPDF + strMet + strDoc + "<br/><br/>" + t12 + "<br/><br/>" + strCompDir + "</div>";

            //t1 = "<div style=\"font-family: Geneva, Arial, Helvetica, sans-serif;\">" + t1 + "<br/><br/><br/><br/><div class=\"col-md-12\" style=\"margin-top:10px;\">" + strMet + "<br/><br/>" + strDoc + "<br/><br/>" + approvalData + "</div></div><br/><br/>";
            //t1 = t1 + strCompDir;
            MainPDF = MainPDF.Replace("/askingculture", sitepath1);
            MainPDF = MainPDF.Replace("../Log/upload/Userimage", sitepath);

            //   MainPDF = MainPDF + "</body>";

        }
        catch (Exception ex)
        {
        }
        String str = Tab_PDF(MainPDF);
        return str;
        //String str = CreateNewPDF(MainPDF);
        //return str;

    }
    [WebMethod(EnableSession = true)]
    public static string SaveActivityHtmlDataAsPDF(string t1, string t2, string t3, string t4, string t5, string t6, string t7, string t8, string t9, string t10)
    {
        Organisation_PersonalDevelopmentPlan o = new Organisation_PersonalDevelopmentPlan();
        string MainPDF = string.Empty;
        string sitepath = Convert.ToString(ConfigurationManager.AppSettings["imagepath_pdf"]);
        string sitepath1 = Convert.ToString(ConfigurationManager.AppSettings["siteurl"]);

        try
        {

            t10 = t10.Replace("color: white;", "");
            t10 = t10.Replace("color:White;", "");
            t10 = t10.Replace("border-width:0px;", "border-width:1px;border: solid #000000 1px;");
            t10 = t10.Replace("border-width: 0px;", "border-width:1px;border: solid #000000 1px;");
            t10 = t10.Replace("class=\"hidden-xs\" style=\"width:50px;\" align=\"left\"", "style='border: solid #000000 1px;width:50px;'");
            t10 = t10.Replace("class=\"hidden-xs\" align=\"left\"", "style='border: solid #000000 1px;'");
            t10 = t10.Replace("class=\"hidden-xs sorting_1\" style=\"width: 50px;\" tabindex=\"0\" align=\"left", "style='border-width:1px;border: solid #000000 1px;width: 50px;'");

            MainPDF = MainPDF + "<span style='color:red;'>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "CONFIDENTIAL.Text")) + "</span><br/><br/>";
            MainPDF = MainPDF + "<h1 style='font-size: 23px;font-family: Verdana, Arial, Helvetica, sans-serif;color: #5781bd;'>" + t1 + "</h1><br/><br/>";



            MainPDF = MainPDF + "<table><tr><td>" + t2 + "</td><td><table><tr><td>" + t3 + "/" + t4
                + "</td></tr><tr><td>" + t5 + "</td></tr><tr><td>"
                + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Jobtitle.Text")) + ":\t\t\t\t" + t6 + "</td></tr>"
                + "<tr><td>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Team.Text")) + ":\t\t\t\t" + t7 + "</td></tr>"
                + "<tr><td>" + t8 + "/" + t9 + "</td></tr></table></td></tr></table>";


            MainPDF = MainPDF + "<br/><br/><table width='100%'><tr><td  style='border: solid #000000 1px;'><span style='font-weight:bold;float:left;font-size: larger;'><h2>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Activitites.Text")) + "</h2></span></td></tr></table><br/><br/>";

            t10 = "<span style='font-size: x-large;font-weight:bold;font'><h3>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Activitites.Text")) + "</h3></span><br/><br/>" + t10;
            t10 = t10.Replace("background-width:100%;", "width: 100%;");
            t10 = t10.Replace("background- width: 100%;", "width: 100%;");

            //MainPDF = MainPDF + "<br/><br/><h2 style='font-weight:bold;float:left;'>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Activitites.Text")) + "</h2><br/><br/>";
            // MainPDF = MainPDF + "<br/><br/><h2 style='font-weight:bold;float:left;'>" + Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Activitites.Text")) + "</h2><br/><br/>";
            MainPDF = MainPDF + "<br/><br/><br/>";
            MainPDF = MainPDF + t10;

        }
        catch (Exception ex)
        {
        }

        MainPDF = MainPDF.Replace("/askingculture", sitepath1);
        MainPDF = MainPDF.Replace("../Log/upload/Userimage", sitepath);
        String str = Tab_PDF(MainPDF);
        return str;
    }

    #region PDF
    private static Winnovative.WnvHtmlConvert.PdfConverter GetPDFConverter(string Header, string Footer)
    {
        int pageWidth = 0;
        int pageHeight = 0;

        // set common properties
        //if (!radioButtonAutodetect.Checked)
        //{
        //    pageWidth = int.Parse(textBoxWebPageWidth.Text.Trim());
        //    pageHeight = int.Parse(textBoxWebPageHeight.Text.Trim());
        //}

        // create the PDF converter
        LicensingManager.LicenseKey = ConfigurationManager.AppSettings["pdfkey"].ToString();
        Winnovative.WnvHtmlConvert.PdfConverter pdfConverter = new Winnovative.WnvHtmlConvert.PdfConverter(820, 1000);

        pdfConverter.LicenseKey = ConfigurationManager.AppSettings["pdfkey"].ToString();

        // set PDF options
        pdfConverter.PdfDocumentOptions.PdfPageSize =
           (Winnovative.WnvHtmlConvert.PdfPageSize)Enum.Parse(typeof(Winnovative.WnvHtmlConvert.PdfPageSize), "A4");

        //pdfConverter.PdfDocumentOptions.PdfCompressionLevel = (Winnovative.WnvHtmlConvert.PdfCompressionLevel)
        //   Enum.Parse(typeof(Winnovative.WnvHtmlConvert.PdfCompressionLevel),
        //   ddlCompression.SelectedItem.ToString());

        pdfConverter.PdfDocumentOptions.ShowHeader = true;
        pdfConverter.PdfDocumentOptions.ShowFooter = true;

        pdfConverter.PdfDocumentOptions.LeftMargin = int.Parse(
                       "10");
        pdfConverter.PdfDocumentOptions.RightMargin = int.Parse(
                      "5");
        pdfConverter.PdfDocumentOptions.TopMargin = int.Parse(
                      "10");
        pdfConverter.PdfDocumentOptions.BottomMargin = int.Parse(
                       "5");

        // pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = cbUseMetafileFormat.Checked;

        pdfConverter.PdfHeaderOptions.HeaderText = Header;
        pdfConverter.PdfHeaderOptions.HeaderTextColor = Color.FromKnownColor(
           (KnownColor)Enum.Parse(typeof(KnownColor),
          "Red"));
        //pdfConverter.PdfHeaderOptions.HeaderSubtitleText = textBoxHeadeSubtitle.Text;
        pdfConverter.PdfHeaderOptions.DrawHeaderLine = true;

        pdfConverter.PdfFooterOptions.FooterText = Footer;
        //pdfConverter.PdfFooterOptions.FooterTextColor = Color.FromKnownColor(
        //   (KnownColor)Enum.Parse(typeof(KnownColor),
        //   ddlFooterColor.SelectedItem.ToString()));
        pdfConverter.PdfFooterOptions.DrawFooterLine = true;
        pdfConverter.PdfFooterOptions.PageNumberText = "";
        pdfConverter.PdfFooterOptions.ShowPageNumber = true;

        return pdfConverter;
    }
    public static string CreateNewPDF(string data)
    {
        //http://www.winnovative-software.com/getting%20started%20with%20html%20to%20pdf%20converter.aspx

        string UserName = Convert.ToString(HttpContext.Current.Session["UserFullName"]);

        string Header = Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "CONFIDENTIAL.Text"));
        string Footer = "                " + DateTime.Now.ToShortDateString() + "        " + UserName;

        LicensingManager.LicenseKey = ConfigurationManager.AppSettings["pdfkey"].ToString();
        Winnovative.WnvHtmlConvert.PdfConverter pdfConverter = GetPDFConverter(Header, Footer);
        string PdfName = DateTime.Now.ToString("ddMMyyyyhhmmmss");
        string path = ConfigurationManager.AppSettings["Htmlpath"].ToString() + "html/" + PdfName + ".pdf";

        try
        {
            // pdfConverter.SavePdfFromUrlToFile(textBoxWebPageURL.Text, path);
            pdfConverter.SavePdfFromHtmlStringToFile(data, path);
            string pdfPathForPrint = PdfName + ".pdf";
            string pdfDownloadURL = ConfigurationManager.AppSettings["siteurl"].ToString() + "Log/html/" + PdfName + ".pdf";
            return pdfDownloadURL;
        }
        catch (Exception ex)
        {
            // MessageBox.Show(ex.Message);
            return "";
        }
    }
    private static StyleSheet GenerateStyleSheet()
    {
        /* FontFactory.Register(@"c:\windows\fonts\gara.ttf", "Garamond");
         FontFactory.Register(@"c:\windows\fonts\garabd.ttf");
         FontFactory.Register(@"c:\windows\fonts\garait.ttf");*/

        StyleSheet css = new StyleSheet();

        css.LoadTagStyle("body", "face", "Garamond");
        css.LoadTagStyle("body", "encoding", "Identity-H");
        css.LoadTagStyle("body", "size", "13pt");
        css.LoadTagStyle("h1", "size", "30pt");

        css.LoadTagStyle("h1", "style", "line-height:30pt;font-weight:bold;margin-bottom:50px;");
        css.LoadTagStyle("h2", "size", "22pt");
        css.LoadTagStyle("h2", "style", "line-height:30pt;font-weight:bold;margin-top:5pt;margin-bottom:12pt;");
        css.LoadTagStyle("h3", "size", "15pt");
        css.LoadTagStyle("h3", "style", "line-height:25pt;font-weight:bold;margin-top:1pt;margin-bottom:15pt;");
        css.LoadTagStyle("h4", "size", "13pt");
        css.LoadTagStyle("h4", "style", "line-height:23pt;margin-top:1pt;margin-bottom:15pt;");
        css.LoadTagStyle("hr", "width", "100%");
        css.LoadTagStyle("a", "style", "text-decoration:underline;");
        css.LoadTagStyle(HtmlTags.HEADERCELL, HtmlTags.BORDERWIDTH, "0.5");
        css.LoadTagStyle(HtmlTags.HEADERCELL, HtmlTags.BORDERCOLOR, "#333");
        css.LoadTagStyle(HtmlTags.HEADERCELL, HtmlTags.BACKGROUNDCOLOR, "#cccccc");
        css.LoadTagStyle(HtmlTags.CELL, HtmlTags.BACKGROUNDCOLOR, "#EFEFEF");
        css.LoadTagStyle(HtmlTags.CELL, HtmlTags.BORDERWIDTH, "0.5");
        css.LoadTagStyle(HtmlTags.CELL, HtmlTags.BORDERCOLOR, "#333");

        return css;
    }
    public static String Tab_PDF(string TabData)
    {
        try
        {
            string newPDFData = string.Empty;
            string[] CntArr = { TabData };
            LicensingManager.LicenseKey = ConfigurationManager.AppSettings["pdfkey"].ToString();
            Winnovative.PdfCreator.Document document = new Winnovative.PdfCreator.Document();

            ExplicitDestination pageDestination = null;
            var count = 0;

            foreach (var PdfData in CntArr)
            {
                newPDFData = PdfData;

                count++;

                Winnovative.PdfCreator.PdfPage page = document.Pages.AddNewPage(Winnovative.PdfCreator.PageSize.A3, new Margins(2), PageOrientation.Portrait);
                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(15, 15, 820, newPDFData, null);

                HtmlToPdfElement footer = new HtmlToPdfElement(15, 15, 820, "Page", null);

                page.ShowFooterTemplate = true;

                

                htmlToPdfElement.ActiveXEnabledInImage = true;
                htmlToPdfElement.AvoidImageBreak = true;
                htmlToPdfElement.AvoidTextBreak = true;

                htmlToPdfElement.Paginate = true;
                htmlToPdfElement.ScriptsEnabled = true;

                htmlToPdfElement.FitWidth = false;
                htmlToPdfElement.StretchToFit = false;


                pageDestination = new ExplicitDestination(page);
                pageDestination.ZoomPercentage = 90;
                pageDestination.DestPage = page;

                AddElementResult addResult;
                addResult = page.AddElement(htmlToPdfElement);

                page.Orientation = PageOrientation.Portrait;
                page.ShowFooterTemplate = true;

             //   document.FooterTemplate.AddElement(footer);

            }

            document.CompressionLevel = CompressionLevel.BestCompression;
            string PdfName = DateTime.Now.ToString("ddMMyyyyhhmmmss");
            string path = ConfigurationManager.AppSettings["Htmlpath"].ToString() + "html/" + PdfName + ".pdf";
            Bookmark bm = document.AddBookmark("Pdf Page", pageDestination);
            document.Save(path);
            string pdfPathForPrint = PdfName + ".pdf";
            string pdfDownloadURL = ConfigurationManager.AppSettings["siteurl"].ToString() + "Log/html/" + PdfName + ".pdf";
            return pdfDownloadURL;
        }
        catch (Exception ex)
        {
            Common.WriteLog("error pdf" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }
    public static String Tab_PDF_old_9_3_2018(string TabData)
    {
        try
        {
            //     TabData = TabData + "<htmlpagefooter name='MyCustomFooter'><table style='vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: bold; font-style: italic;' width='100%'>" +
            //"<tbody><tr><td width='33%'><span style='font-weight: bold; font-style: italic;'>{DATE j-m-Y}</span></td><td style='font-weight: bold; font-style: italic;' align='center' width='33%'>{PAGENO}/{nbpg}</td>" +
            //        "<td style='text-align: right;' width='33%'>My document</td></tr></tbody></table></htmlpagefooter><style>@page { header: html_MyCustomHeader; footer: html_MyCustomFooter; }</style><sethtmlpagefooter name='Footer' value='on' /> ";

            string StartStr = "";//"<html><head>  <style>    @page { margin: 100px 25px; }    header { position: fixed; top: -60px; left: 0px; right: 0px; background-color: lightblue; height: 50px; }    footer { position: fixed; bottom: 60px; left: 0px; right: 0px; background-color: lightblue; height: 50px; }    p { page-break-after: always; }    p:last-child { page-break-after: never; }  </style></head><body>  <header>header on each page</header>  <footer>footer on each page</footer>  <main>";
            //string EndStr = "<htmlpagefooter name=\"MyCustomFooter\">"+
            //    "<table style=\"vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: bold; font-style: italic;\" width=\"100%\">"+
            //    "<tbody>	<tr>	<td width=\"33%\"><span style=\"font-weight: bold; font-style: italic;\">DATE " + DateTime.Now.ToShortDateString() + "</span></td><td style=\"font-weight: bold; font-style: italic;\" align=\"center\" width=\"33%\"></td>	<td style=\"text-align: right;\" width=\"33%\">{PAGENO}/{nbpg}</td>	</tr></tbody></table></htmlpagefooter>"+
            //"<style>@page { header: html_MyCustomHeader; footer: html_MyCustomFooter; }</style><sethtmlpagefooter name='Footer' value='on' />";
            //"</main></body></html>";
            //string footer = " <footer>      This is the text that goes at the bottom of every page.    </footer>";

            //string EndStr="<style>#content {    display: table;} #pageFooter {    display: table-footer-group;}#pageFooter:after {    counter-increment: page;    content: counter(page);}</style>";
            //EndStr=EndStr+"<div id=\"content\">  <div id=\"pageFooter\">Page </div>";

            //string EndStr="@page {        margin-top: 149px;        margin-left: 2px;        margin-bottom: 40px;        margin-right: 2px;        size: landscape;        counter-increment: page;     @bottom-right {padding-right:20px;        content: \"Page \" counter(page);      }    }";

            string EndStr = "<html><head><title></title><style>html {  height: 100%;} body {  min-height: 100%;} div.test:before{  counter-increment: page;    content:'Page ' counter(page); }</style></head><body>";
            string footer = "<footer> <table style=\"vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: bold; font-style: italic;\" width=\"100%\">";
            footer = footer + "<tbody>	<tr>	<td width=\"33%\"><span style=\"font-weight: bold; font-style: italic;\">DATE " + DateTime.Now.ToShortDateString() + "</span></td><td style=\"font-weight: bold; font-style: italic;\" align=\"center\" width=\"10%\"></td>	<td style=\"text-align: center;\" width=\"33%\"><div class=\"test\"></div></td>	</tr></tbody></table></footer>";



            string newPDFData = string.Empty;
            string[] CntArr = { TabData };
            // ConfigurationManager.AppSettings["Htmlpath"].ToString() + "template/Report.html"
            LicensingManager.LicenseKey = ConfigurationManager.AppSettings["pdfkey"].ToString();
            Winnovative.PdfCreator.Document document = new Winnovative.PdfCreator.Document();

            ExplicitDestination pageDestination = null;
            var count = 0;

            foreach (var PdfData in CntArr)
            {
                // newPDFData = PdfData;
                // newPDFData = StartStr + PdfData + EndStr;

                newPDFData = EndStr + PdfData + "</body>" + footer + "</html>";
                //  newPDFData = extraStr + PdfData + "</body></html>";
                count++;




                Winnovative.PdfCreator.PdfPage page = document.Pages.AddNewPage(Winnovative.PdfCreator.PageSize.A3, new Margins(2), PageOrientation.Portrait);
                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(15, 15, 820, newPDFData, null);

                page.ShowFooterTemplate = true;

                //Winnovative.WnvHtmlConvert.PdfConverter pdfConverter = new Winnovative.WnvHtmlConvert.PdfConverter();
                //pdfConverter.PdfDocumentOptions.ShowHeader = true;
                //pdfConverter.PdfDocumentOptions.ShowFooter = true;
                //pdfConverter.PdfFooterOptions.FooterHeight = 60;
                //PdfFont titleFont = page.AddFont(new Font("Times New Roman", 10, FontStyle.Bold, GraphicsUnit.Point));
                //TextElement footerTextElement = new TextElement(0, 30, "This is page &p; of &P;  ",new PdfFont());
                //footerTextElement.TextAlign = HorizontalTextAlign.Right;
                //pdfConverter.PdfFooterOptions.AddElement(footerTextElement);

                //// set the footer HTML area
                //HtmlToPdfElement footerHtml = new HtmlToPdfElement(0, 0, 0, pdfConverter.PdfFooterOptions.FooterHeight,
                //    headerAndFooterHtmlUrl, 1024, 0);
                //footerHtml.FitHeight = true;
                //pdfConverter.PdfFooterOptions.AddElement(footerHtml);


                htmlToPdfElement.ActiveXEnabledInImage = true;
                htmlToPdfElement.AvoidImageBreak = true;
                htmlToPdfElement.AvoidTextBreak = true;

                htmlToPdfElement.Paginate = true;
                htmlToPdfElement.ScriptsEnabled = true;


                htmlToPdfElement.FitWidth = false;
                htmlToPdfElement.StretchToFit = false;

                pageDestination = new ExplicitDestination(page);
                pageDestination.ZoomPercentage = 90;
                pageDestination.DestPage = page;


                AddElementResult addResult;
                addResult = page.AddElement(htmlToPdfElement);




                page.Orientation = PageOrientation.Portrait;
                page.ShowFooterTemplate = true;

            }

            //HtmlToPdfElement FooterhtmlToPdfElement = new HtmlToPdfElement(15, 15, 820, "swati", null);
            //document.FooterTemplate.AddElement(FooterhtmlToPdfElement);


            document.CompressionLevel = CompressionLevel.BestCompression;
            // send the generated PDF document to client browser
            string PdfName = DateTime.Now.ToString("ddMMyyyyhhmmmss");

            string path = ConfigurationManager.AppSettings["Htmlpath"].ToString() + "html/" + PdfName + ".pdf";

            WriteLog("REP6 document.Save(path); path : " + path);

            //string path = @"F:\Dharmesh\2-05-2015 work\starteku\starteku\Log\template\Report.html";

            Bookmark bm = document.AddBookmark("Pdf Page", pageDestination);

            //if (!System.IO.File.Exists(path))
            document.Save(path);
            //  Session["pdfPathForPrint"] = PdfName + ".pdf";
            string pdfPathForPrint = PdfName + ".pdf";
            string pdfDownloadURL = ConfigurationManager.AppSettings["siteurl"].ToString() + "Log/html/" + PdfName + ".pdf";

            WriteLog("REP7 pdfDownloadURL : " + pdfDownloadURL);

            return pdfDownloadURL;
        }
        catch (Exception ex)
        {
            Common.WriteLog("error pdf" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }
    public static String pdf_Competence(string t1, string t2)
    {
        try
        {

            string[] CntArr = { t1, t2 };
            LicensingManager.LicenseKey = ConfigurationManager.AppSettings["pdfkey"].ToString();
            Winnovative.PdfCreator.Document document = new Winnovative.PdfCreator.Document();

            ExplicitDestination pageDestination = null;
            var count = 0;
            foreach (var PdfData in CntArr)
            {
                Winnovative.PdfCreator.PdfPage page = document.Pages.AddNewPage(Winnovative.PdfCreator.PageSize.A3, new Margins(1), PageOrientation.Portrait);
                // HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(15, 15,820, styleData + PdfData, null);Fta
                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(15, 15, 820, PdfData, null);
                htmlToPdfElement.ScriptsEnabled = true;
                htmlToPdfElement.ActiveXEnabledInImage = true;
                htmlToPdfElement.AvoidImageBreak = true;
                htmlToPdfElement.AvoidTextBreak = true;
                htmlToPdfElement.FitWidth = false;
                htmlToPdfElement.StretchToFit = false;
                pageDestination = new ExplicitDestination(page);
                pageDestination.ZoomPercentage = 90;
                pageDestination.DestPage = page;
                AddElementResult addResult;
                addResult = page.AddElement(htmlToPdfElement);
                page.Orientation = PageOrientation.Portrait;
            }
            document.CompressionLevel = CompressionLevel.BestCompression;
            // send the generated PDF document to client browser
            string PdfName = DateTime.Now.ToString("ddMMyyyyhhmmmss");

            string path = ConfigurationManager.AppSettings["Htmlpath"].ToString() + "html/" + PdfName + ".pdf";
            Bookmark bm = document.AddBookmark("Pdf Page", pageDestination);
            document.Save(path);
            string pdfPathForPrint = PdfName + ".pdf";
            string pdfDownloadURL = ConfigurationManager.AppSettings["siteurl"].ToString() + "Log/html/" + PdfName + ".pdf";
            return pdfDownloadURL;
        }
        catch (DataException ex)
        {
            Common.WriteLog("error pdf" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }
    public static String Fullpdf(string t1, string t2, string t3, string t4, string t5)
    {
        try
        {
            string[] CntArr = { t1, t2, t3, t4, t5 };
            LicensingManager.LicenseKey = ConfigurationManager.AppSettings["pdfkey"].ToString();
            Winnovative.PdfCreator.Document document = new Winnovative.PdfCreator.Document();

            ExplicitDestination pageDestination = null;
            var count = 0;
            foreach (var PdfData in CntArr)
            {
                count++;
                if (count == 2)
                {
                    if (string.IsNullOrWhiteSpace(Convert.ToString(HttpContext.Current.Session["Quescompid"])))
                    {
                        continue;
                    }
                }
                else if (count == 3)
                {
                    if (string.IsNullOrWhiteSpace(Convert.ToString(HttpContext.Current.Session["Pointcompid"])))
                    {
                        continue;
                    }
                }
                else if (count == 4)
                {
                    if (string.IsNullOrWhiteSpace(Convert.ToString(HttpContext.Current.Session["Compcompid"])))
                    {
                        continue;
                    }
                }
                else if (count == 5)
                {
                    if (string.IsNullOrWhiteSpace(Convert.ToString(HttpContext.Current.Session["Aemcompid"])))
                    {
                        continue;
                    }
                }
                Winnovative.PdfCreator.PdfPage page = document.Pages.AddNewPage(Winnovative.PdfCreator.PageSize.A3, new Margins(1), PageOrientation.Portrait);
                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(15, 15, 820, PdfData, null);
                htmlToPdfElement.ScriptsEnabled = true;
                htmlToPdfElement.ActiveXEnabledInImage = true;
                htmlToPdfElement.AvoidImageBreak = true;
                htmlToPdfElement.AvoidTextBreak = true;
                htmlToPdfElement.FitWidth = false;
                htmlToPdfElement.StretchToFit = false;
                pageDestination = new ExplicitDestination(page);
                pageDestination.ZoomPercentage = 90;
                pageDestination.DestPage = page;
                AddElementResult addResult;
                addResult = page.AddElement(htmlToPdfElement);
                page.Orientation = PageOrientation.Portrait;
            }
            document.CompressionLevel = CompressionLevel.BestCompression;
            string PdfName = DateTime.Now.ToString("ddMMyyyyhhmmmss");
            string path = ConfigurationManager.AppSettings["Htmlpath"].ToString() + "html/" + PdfName + ".pdf";
            Bookmark bm = document.AddBookmark("Pdf Page", pageDestination);
            document.Save(path);
            string pdfPathForPrint = PdfName + ".pdf";
            string pdfDownloadURL = ConfigurationManager.AppSettings["siteurl"].ToString() + "Log/html/" + PdfName + ".pdf";
            return pdfDownloadURL;
        }
        catch (DataException ex)
        {
            Common.WriteLog("error pdf" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }
    public static String pdf(string t1, string t2, string t3, string t4, string t5, string styleData)
    {
        try
        {

            string[] CntArr = { t1, t2, t3, t4, t5 };
            // ConfigurationManager.AppSettings["Htmlpath"].ToString() + "template/Report.html"
            LicensingManager.LicenseKey = ConfigurationManager.AppSettings["pdfkey"].ToString();
            Winnovative.PdfCreator.Document document = new Winnovative.PdfCreator.Document();

            ExplicitDestination pageDestination = null;
            var count = 0;
            foreach (var PdfData in CntArr)
            {
                count++;
                if (count == 4)
                {
                    if (string.IsNullOrWhiteSpace(Convert.ToString(HttpContext.Current.Session["Aemcompid"])))
                    {
                        continue;
                    }
                }
                Winnovative.PdfCreator.PdfPage page = document.Pages.AddNewPage(Winnovative.PdfCreator.PageSize.A3, new Margins(1), PageOrientation.Portrait);
                // HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(15, 15,820, styleData + PdfData, null);Fta
                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(15, 15, 820, PdfData, null);
                htmlToPdfElement.ScriptsEnabled = true;
                htmlToPdfElement.ActiveXEnabledInImage = true;
                htmlToPdfElement.AvoidImageBreak = true;
                htmlToPdfElement.AvoidTextBreak = true;
                htmlToPdfElement.FitWidth = false;
                htmlToPdfElement.StretchToFit = false;
                pageDestination = new ExplicitDestination(page);
                pageDestination.ZoomPercentage = 90;
                pageDestination.DestPage = page;
                AddElementResult addResult;
                addResult = page.AddElement(htmlToPdfElement);
                page.Orientation = PageOrientation.Portrait;
            }

            //if (CntArr.Length > 1)
            //{

            //Winnovative.PdfCreator.PdfPage page1 = document.Pages.AddNewPage(Winnovative.PdfCreator.PageSize.A3, new Margins(3), PageOrientation.Portrait);
            //HtmlToPdfElement htmlToPdfElement1 = new HtmlToPdfElement(10, 10, 830, styleData + CntArr[1], null);
            //htmlToPdfElement1.ActiveXEnabledInImage = true;
            //htmlToPdfElement1.AvoidImageBreak = true;
            //htmlToPdfElement1.AvoidTextBreak = true;
            //htmlToPdfElement1.FitWidth = true;
            //htmlToPdfElement1.StretchToFit = true;
            //ExplicitDestination pageDestination1 = new ExplicitDestination(page1);
            //pageDestination.ZoomPercentage = 100;
            //pageDestination.DestPage = page1;
            //AddElementResult addResult1;
            //addResult1 = page1.AddElement(htmlToPdfElement1);
            //page1.Orientation = PageOrientation.Portrait;


            document.CompressionLevel = CompressionLevel.BestCompression;
            // send the generated PDF document to client browser
            string PdfName = DateTime.Now.ToString("ddMMyyyyhhmmmss");

            string path = ConfigurationManager.AppSettings["Htmlpath"].ToString() + "html/" + PdfName + ".pdf";

            WriteLog("REP6 document.Save(path); path : " + path);

            //string path = @"F:\Dharmesh\2-05-2015 work\starteku\starteku\Log\template\Report.html";

            Bookmark bm = document.AddBookmark("Pdf Page", pageDestination);
            //if (!System.IO.File.Exists(path))
            document.Save(path);
            //  Session["pdfPathForPrint"] = PdfName + ".pdf";
            string pdfPathForPrint = PdfName + ".pdf";
            string pdfDownloadURL = ConfigurationManager.AppSettings["siteurl"].ToString() + "Log/html/" + PdfName + ".pdf";

            WriteLog("REP7 pdfDownloadURL : " + pdfDownloadURL);

            return pdfDownloadURL;
        }
        catch (DataException ex)
        {
            Common.WriteLog("error pdf" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }



    #endregion
    #endregion


    public static void WriteLog(string functionName)
    {
        string writelogtest = Convert.ToString(ConfigurationSettings.AppSettings["writelogstatus"]).ToLower();
        //string writelogtest = "true";
        if (!string.IsNullOrEmpty(writelogtest))
        {
            if (writelogtest == "true")
            {
                string fileName = DateTime.Today.ToString("dd_MMM_yyyy");
                string path = ConfigurationSettings.AppSettings["logpath"].ToString() + fileName + ".txt";
                //string path = "c:/inetpub/wwwroot/starteku/Log/" + fileName + ".txt";
                //
                if (!System.IO.File.Exists(path))
                {
                    using (FileStream fs = File.Create(path))
                    {
                        Byte[] info = new UTF8Encoding(true).GetBytes("");
                        fs.Write(info, 0, 0);
                    }
                }
                TextWriter tw1 = new StreamWriter(path, true);
                tw1.WriteLine(functionName);
                tw1.Close();
                tw1.Dispose();
            }
        }
    }
    #region Meeting


    #endregion
}

public partial class CommentList
{
    public string Comment { get; set; }
    public DateTime CreateDate { get; set; }
    public string fromuserImage { get; set; }
}

public partial class PersonalCommentData
{
    public int qpercomID { get; set; }
    public string PersonalComment { get; set; }

    public int IsPDFPersonalComment { get; set; }
    public bool IsDisplayInReportComment { get; set; }
    public int btnSaveComment { get; set; }
}