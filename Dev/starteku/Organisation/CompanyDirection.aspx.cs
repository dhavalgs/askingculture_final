﻿using App_code;
using starteku_BusinessLogic;
using starteku_BusinessLogic.Model;
using startetku.Business.Logic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.UI.WebControls;

public partial class Organisation_CompanyDirection : System.Web.UI.Page
{
    public static int ResLangId = 0;
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GetAllCompanyCategory();
            GetAllElementList();
            GetAllCompanyCategoryTab2();
            GetAssignTeamList();
            GetAllAssignDirectionList();
            GetPublishDirectionUserList();

            if (!String.IsNullOrEmpty(Request.QueryString["notif"]))
            {
                UpdateNotification();
            }

            List<CatEle> temp = new List<CatEle>();
            CatEle t = new CatEle();
            t.CatID = 0;
            t.EleIDs = new List<EleIDs>();
            temp.Add(t);
            Session["ListCat"] = temp;

            List<TeamUser> tempuser = new List<TeamUser>();
            TeamUser tt = new TeamUser();
            tt.TeamID = 0;
            tt.UserIDs = new List<UserIDs>();
            tempuser.Add(tt);
            Session["ListTeamUser"] = tempuser;
        }
        btns.Visible = true;
        btnUp.Visible = false;
        lblCreatorName.Visible = false;
        txtOwnerName.Visible = false;
        txtCreateDate.Visible = false;
        lblCreateDate.Visible = false;
    }

    #region ManageCompanyDirection

    protected void OpenCreateCompanyDirCat(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lblQuestionCat').click();", true);
        Session["CompCatID"] = 0;
        ResourceLanguageBM obj = new ResourceLanguageBM();
        DataSet ds = new DataSet();
        obj.LangCompanyID = Convert.ToInt32(Session["OrgUserId"]);
        obj.LangActCatId = Convert.ToInt32(Session["CompCatID"]);
        obj.LangType = "compcat";
        ds = obj.GetAllResourceLangTran();

        gridTranslate.DataSource = ds.Tables[0];
        gridTranslate.DataBind();
    }
    public void InsertCategory()
    {
        try
        {
            DirectionCategory obj = new DirectionCategory();

            var CompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            var LoggedUserId = Convert.ToInt32(Session["OrgUserId"]);

            obj.catName = txtname.Text;
            obj.catNameDN = txtnameDN.Text;
            obj.catDescription = txtdescription.Text;
            obj.catIndex = Convert.ToInt32(txtno.Text);
            obj.catCreatedBy = LoggedUserId;
            obj.catCreateDate = DateTime.Now;
            obj.catUpdateDate = DateTime.Now;
            obj.catIsActive = true;
            obj.catCompanyID = CompanyId;
            obj.catIsDelete = false;

            obj.InsertDirectionCategory();

            DataSet ds = obj.ds;

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    int CompCatID = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
                    LangTranslationMasterBM lobj = new LangTranslationMasterBM();
                    string resLangID, LangText;
                    for (int i = 0; i < gridTranslate.Rows.Count; i++)
                    {
                        resLangID = ((HiddenField)gridTranslate.Rows[i].Cells[0].FindControl("hdnResLangID")).Value;
                        LangText = ((TextBox)gridTranslate.Rows[i].Cells[0].FindControl("txtTranValue")).Text;
                        lobj.LangType = "compcat";
                        lobj.LangActCatId = CompCatID;
                        lobj.ResLangID = Convert.ToInt32(resLangID);
                        lobj.LangText = LangText;
                        lobj.LangCompanyID = LoggedUserId;
                        lobj.InsertLangResource();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex);
        }
    }

    public void InsertCategoryElement()
    {
        try
        {
            //  Common.WriteLog("InsertCategoryElement 1");
            CompanyDirectionElement obj = new CompanyDirectionElement();
            var CompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            var LoggedUserId = Convert.ToInt32(Session["OrgUserId"]);

            obj.dirID = 0;
            obj.dirCatID = ddldirCat.SelectedValue;
            obj.dirName = txttitlename.Text;
            obj.dirNameDN = txttitlenameDN.Text;
            obj.dirDescription = txtdetails.Text;
            obj.dirIndex = Convert.ToInt32(txtn.Text);
            obj.dirCreatedBy = LoggedUserId;
            obj.dirCreateDate = DateTime.Now;
            obj.dirUpdateDate = DateTime.Now;
            obj.dirIsActive = true;
            obj.dirCompanyID = CompanyId;
            obj.dirIsDelete = false;
            obj.dirIsMandatory = Convert.ToBoolean(roIsMandatory.SelectedItem.Value);
            //  Common.WriteLog("InsertCategoryElement 2");
            obj.InsertCompanyDirectionElement();
            // Common.WriteLog("InsertCategoryElement 3");


        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "InsertCategoryElement");
        }
    }

    protected void GetAllElementList()
    {
        var logginUserType = Convert.ToInt32(Session["OrguserType"]);
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(Session["OrgUserId"]);
        ResLangId = Convert.ToInt32(Session["resLangID"]);

        CompanyDirectionElement obj = new CompanyDirectionElement();
        obj.dirIDs = "0";
        //obj.UserId = userId;
         obj.dirCompanyID = companyId;
        //obj.dirCreatedBy = userId;
         obj.LangID = ResLangId;
        obj.dirIsActive = true;
        obj.dirIsDelete = false;
        obj.GetAllElementList();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvGrid_Element.DataSource = ds.Tables[0];
                gvGrid_Element.DataBind();

                gvGrid_Element.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvGrid_Element.DataSource = null;
                gvGrid_Element.DataBind();
            }
        }
        else
        {
            gvGrid_Element.DataSource = null;
            gvGrid_Element.DataBind();
        }
    }

    protected DataSet GetElementByID(string ID)
    {

        CompanyDirectionElement obj = new CompanyDirectionElement();
        try
        {
            
            var logginUserType = Convert.ToInt32(Session["OrguserType"]);
            var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
            var logginUserId = Convert.ToInt32(Session["OrgUserId"]);
            ResLangId = Convert.ToInt32(Session["resLangID"]);
            obj.LangID = ResLangId;
            obj.dirIDs = ID;
            obj.dirIsActive = true;
            obj.dirIsDelete = false;
            obj.dirCompanyID = companyId;
            obj.GetAllElementList();

        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex);
        }

        DataSet ds = obj.ds;

        return ds;
    }

    protected void GetAllCompanyCategory()
    {
        //ddlQuesCat
        ResLangId = Convert.ToInt32(Session["resLangID"]);
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        DirectionCategory obj = new DirectionCategory();
        obj.catIsActive = true;
        obj.catIsDelete = false;
        obj.catID = "0";
        obj.LangID = ResLangId;
        obj.catCompanyID = companyId;
        obj.GetAllCompanyCategoryName();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddldirCat.Items.Clear();
                ddlFilterDirectionCategory.Items.Clear();

                ddldirCat.DataSource = ds.Tables[0];
                ddlFilterDirectionCategory.DataSource = ds.Tables[0];
                if (Convert.ToString(Session["Culture"]) == "Danish")
                {
                    ddldirCat.DataTextField = "catNameInLang";
                }
                else
                {
                    ddldirCat.DataTextField = "catNameInLang";
                }

//                ddldirCat.DataTextField = "catNameInLang";
                ddldirCat.DataValueField = "catID";
                ddldirCat.DataBind();

                ddldirCat.DataSource = ds.Tables[0];
//                ddlFilterDirectionCategory.DataSource = ds.Tables[0];
                if (Convert.ToString(Session["Culture"]) == "Danish")
                {
                    ddldirCat.DataTextField = "catNameInLang";
                }
                else
                {
                    ddldirCat.DataTextField = "catNameInLang";
                }
                ddldirCat.DataValueField = "catID";
                ddldirCat.DataBind();
                ddldirCat.Items.Insert(0, new ListItem(hdnSelectCat.Value, CommonModule.dropDownZeroValue));
                ddldirCat.SelectedIndex = 0;


                //if (Convert.ToString(Session["Culture"]) == "Danish")
                //{
                //    ddlFilterDirectionCategory.DataTextField = "catNameDN";
                //}
                //else
                //{
                //    ddlFilterDirectionCategory.DataTextField = "catName";
                //}
//                ddlFilterDirectionCategory.DataTextField = "catNameInLang";
//                ddlFilterDirectionCategory.DataValueField = "catID";
//                ddlFilterDirectionCategory.DataBind();

                ddlFilterDirectionCategory.Items.Insert(0, new ListItem(hdnAll.Value, CommonModule.dropDownZeroValue));
                ddlFilterDirectionCategory.SelectedIndex = 0;
            }
            else
            {
                ddldirCat.Items.Clear();
                ddldirCat.Items.Insert(0, new ListItem(hdnSelectCat.Value, CommonModule.dropDownZeroValue));
                ddldirCat.SelectedIndex = 0;

                ddlFilterDirectionCategory.Items.Clear();
                ddlFilterDirectionCategory.Items.Insert(0, new ListItem(hdnAll.Value, CommonModule.dropDownZeroValue));
                ddlFilterDirectionCategory.SelectedIndex = 0;
            }
        }
        else
        {
            ddldirCat.Items.Clear();
            ddldirCat.Items.Insert(0, new ListItem(hdnSelectCat.Value, CommonModule.dropDownZeroValue));
            ddldirCat.SelectedIndex = 0;

            ddlFilterDirectionCategory.Items.Clear();
            ddlFilterDirectionCategory.Items.Insert(0, new ListItem(hdnAll.Value, CommonModule.dropDownZeroValue));
            ddlFilterDirectionCategory.SelectedIndex = 0;
        }

    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        try
        {
            InsertCategory();
            GetAllCompanyCategory();
            ddldirCat.SelectedIndex = 0;

            txtname.Text = "";
            txtnameDN.Text = "";
            txtdescription.Text = "";
            txtno.Text = "";
            Response.Redirect("CompanyDirection.aspx");
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "btnsave_Click");
        }


    }

    protected void btns_Click(object sender, EventArgs e)
    {
        try
        {
            //btns.Visible = true;
            //btnUp.Visible = false;
            // Common.WriteLog("btns_Click 1");
            InsertCategoryElement();
            //   Common.WriteLog("btns_Click 2");
            GetAllElementList();
            //  Common.WriteLog("GetAllElementList 1");
            ddldirCat.SelectedIndex = 0;
            txttitlename.Text = "";
            txttitlenameDN.Text = "";
            txtdetails.Text = "";
            txtn.Text = "";
            Response.Redirect("CompanyDirection.aspx");
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "btns_Click");
        }

    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            //btns.Visible = true;
            //btnUp.Visible = false;

            CompanyDirectionElement obj = new CompanyDirectionElement();
            var CompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            var LoggedUserId = Convert.ToInt32(Session["OrgUserId"]);

            obj.dirID = Convert.ToInt32(hdnQuesID.Value);
            obj.dirCatID = ddldirCat.SelectedValue;
            obj.dirName = txttitlename.Text;
            obj.dirNameDN = txttitlenameDN.Text;
            obj.dirDescription = txtdetails.Text;
            obj.dirIndex = Convert.ToInt32(txtn.Text);
            obj.dirCreatedBy = LoggedUserId;
            obj.dirCreateDate = DateTime.Now;
            obj.dirUpdateDate = DateTime.Now;
            obj.dirIsActive = true;
            obj.dirCompanyID = CompanyId;
            obj.dirIsDelete = false;
            obj.dirIsMandatory = Convert.ToBoolean(roIsMandatory.SelectedItem.Value);

            obj.InsertCompanyDirectionElement();
            ddldirCat.SelectedIndex = 0;
            txttitlename.Text = "";
            txttitlenameDN.Text = "";
            txtdetails.Text = "";
            txtn.Text = "";
            txtname.Text = "";
            txtnameDN.Text = "";
            txtdescription.Text = "";
            txtno.Text = "";
            btns.Visible = true;
            btnUp.Visible = false;
            GetAllElementList();
            Response.Redirect("CompanyDirection.aspx");
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "btnUpdate_Click");
        }

    }

    protected void gvGrid_Element_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        // int dirID = Convert.ToInt32(gvGrid_Element.DataKeys[e.RowIndex].Values[0]);
        if (e.CommandName == "archive")
        {
            CompanyDirectionElement obj = new CompanyDirectionElement();
            obj.dirID = Convert.ToInt32(e.CommandArgument);
            obj.dirIsActive = true;
            obj.dirIsDelete = false;
            obj.ElementDelete();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    //GetAllElementList();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
            GetAllElementList();
        }
        else if (e.CommandName == "EditRow")
        {
            ClientScript.RegisterStartupScript(this.GetType(), "", " $('.hover_bkgr_fricc').show();", true);
            //lblCompanyDirModalHeader.Text = "Edit Company Direction Element";
            lblCompanyDirModalHeader.Text = Label9.Text;
            // GetAllElementList();

            string dirID = (e.CommandArgument).ToString();

            DataSet ds = GetElementByID(dirID);
            btns.Visible = false;
            btnUp.Visible = true;
            hdnQuesID.Value = Convert.ToString(dirID);
            if (ddldirCat.Items.FindByValue(Convert.ToString(ds.Tables[0].Rows[0]["dirCatID"]).Trim()) != null)
            {
                ddldirCat.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["dirCatID"]).Trim();
                //  ddldirCat.SelectedIndex = Convert.ToInt32(ds.Tables[0].Rows[0]["dirCatID"]);
            }

            txttitlename.Text = Convert.ToString(ds.Tables[0].Rows[0]["dirName"]);
            txttitlenameDN.Text = Convert.ToString(ds.Tables[0].Rows[0]["dirNameDN"]);
            txttitlename.Text = Convert.ToString(ds.Tables[0].Rows[0]["catName"]);
            txtnameDN.Text = Convert.ToString(ds.Tables[0].Rows[0]["catNameDN"]);
            txtdetails.Text = Convert.ToString(ds.Tables[0].Rows[0]["dirDescription"]);
            txtn.Text = Convert.ToString(ds.Tables[0].Rows[0]["dirIndex"]);
            lblCreatorName.Visible = false;
            txtOwnerName.Visible = false;
            txtCreateDate.Visible = false;
            lblCreateDate.Visible = false;
        }
        else if (e.CommandName == "ViewRow")
        {
            ClientScript.RegisterStartupScript(this.GetType(), "", " $('.hover_bkgr_fricc').show();", true);
            // GetAllElementList();
//            lblCompanyDirModalHeader.Text = "View Company Direction Element";
            string dirID = (e.CommandArgument).ToString();

            DataSet ds = GetElementByID(dirID);
            //btnUp.Visible = false;
            btns.Visible = false;
            hdnQuesID.Value = Convert.ToString(dirID);
            if (ddldirCat.Items.FindByValue(Convert.ToString(ds.Tables[0].Rows[0]["dirCatID"]).Trim()) != null)
            {
                ddldirCat.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["dirCatID"]).Trim();
                //  ddldirCat.SelectedIndex = Convert.ToInt32(ds.Tables[0].Rows[0]["dirCatID"]);
            }

            txttitlename.Text = Convert.ToString(ds.Tables[0].Rows[0]["dirName"]);
            txttitlenameDN.Text = Convert.ToString(ds.Tables[0].Rows[0]["dirNameDN"]);
            txtdetails.Text = Convert.ToString(ds.Tables[0].Rows[0]["dirDescription"]);
            txtn.Text = Convert.ToString(ds.Tables[0].Rows[0]["dirIndex"]);

            lblCreatorName.Visible = true;
            txtOwnerName.Visible = true;
            txtCreateDate.Visible = true;
            lblCreateDate.Visible = true;
            txtOwnerName.Text = Convert.ToString(ds.Tables[0].Rows[0]["userFirstName"]) + " " + Convert.ToString(ds.Tables[0].Rows[0]["userLastName"]);
            txtCreateDate.Text = Convert.ToDateTime(ds.Tables[0].Rows[0]["dirCreatedDate"]).ToShortDateString();

        }
    }


    protected void ddlFilterDirectionCategory_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "", "DisplayTab(1);", true);

        try
        {
            DataSet ds = GetCategoryElementByID(ddlFilterDirectionCategory.SelectedValue);
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvGrid_Element.DataSource = ds.Tables[0];
                    gvGrid_Element.DataBind();

                    gvGrid_Element.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                else
                {
                    gvGrid_Element.DataSource = null;
                    gvGrid_Element.DataBind();
                }
            }
            else
            {
                gvGrid_Element.DataSource = null;
                gvGrid_Element.DataBind();
            }

        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "ddlFilterDirectionCategory_OnSelectedIndexChanged");
        }
    }
    #endregion


    #region AssignCompanyDirection

    protected DataSet GetCategoryById(string TmpIDs)
    {
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        ResLangId = Convert.ToInt32(Session["resLangID"]);
        DirectionCategory obj = new DirectionCategory();
        obj.catID = TmpIDs;
        obj.catIsActive = true;
        obj.catIsDelete = false;
        obj.LangID = ResLangId;
        obj.catCompanyID = companyId;
        obj.GetAllCompanyCategoryName();
        DataSet ds = obj.ds;

        return ds;
    }

    protected void GetAllCompanyCategoryTab2()
    {
        //ddlQuesCat
        ResLangId = Convert.ToInt32(Session["resLangID"]);
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        try
        {
            DirectionCategory obj = new DirectionCategory();
            obj.catIsActive = true;
            obj.catIsDelete = false;
            obj.catID = "0";
            obj.LangID = ResLangId;
            obj.catCompanyID = companyId;
            obj.GetAllCompanyCategoryName();
            DataSet ds = obj.ds;

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddldirCat2.Items.Clear();
                    ddlFilterDirectionCategory.Items.Clear();

                    ddldirCat2.DataSource = ds.Tables[0];
                    ddlFilterDirectionCategory.DataSource = ds.Tables[0];
                    if (Convert.ToString(Session["Culture"]) == "Danish")
                    {
                        ddldirCat2.DataTextField = "catNameInLang";
                        ddlFilterDirectionCategory.DataTextField = "catNameInLang";
                    }
                    else
                    {
                        ddldirCat2.DataTextField = "catNameInLang";
                        ddlFilterDirectionCategory.DataTextField = "catNameInLang";
                    }
                    ddlFilterDirectionCategory.DataTextField = "catNameInLang";
                    ddldirCat2.DataTextField = "catNameInLang";
                    ddldirCat2.DataValueField = "catID";
                    ddldirCat2.DataBind();

                    ddldirCat2.Items.Insert(0, new ListItem(hdnSelectCat2.Value, "-1"));
                    ddldirCat2.SelectedIndex = 0;


                    ddlFilterDirectionCategory.DataValueField = "catID";
                    ddlFilterDirectionCategory.DataBind();

                    ddlFilterDirectionCategory.Items.Insert(0, new ListItem(hdnAll.Value, CommonModule.dropDownZeroValue));
                    ddlFilterDirectionCategory.SelectedIndex = 0;
                }
                else
                {
                    ddldirCat2.Items.Clear();
                    ddldirCat2.Items.Insert(0, new ListItem(hdnSelectCat2.Value, "-1"));
                    ddldirCat2.SelectedIndex = 0;

                    ddlFilterDirectionCategory.Items.Clear();
                    ddlFilterDirectionCategory.Items.Insert(0, new ListItem(hdnAll.Value, CommonModule.dropDownZeroValue));
                    ddlFilterDirectionCategory.SelectedIndex = 0;
                }
            }
            else
            {
                ddldirCat2.Items.Clear();
                ddldirCat2.Items.Insert(0, new ListItem(hdnSelectCat2.Value, "-1"));
                ddldirCat2.SelectedIndex = 0;

                ddlFilterDirectionCategory.Items.Clear();
                ddlFilterDirectionCategory.Items.Insert(0, new ListItem(hdnAll.Value, CommonModule.dropDownZeroValue));
                ddlFilterDirectionCategory.SelectedIndex = 0;
            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "GetAllCompanyCategoryTab2");
        }
    }

    private DataSet GetTeamListByIDs(string TeamIDs)
    {
        JobTypeBM obj = new JobTypeBM();
        obj.TeamIDs = TeamIDs;
        obj.jobIsActive = true;
        obj.jobIsDeleted = false;
        obj.jobCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllTeam();
        DataSet ds = obj.ds;

        var userList = ds.Tables[0].AsEnumerable().Select(dataRow => new UserIDs
               {
                   userID = dataRow.Field<int>("TeamID"),

               }).ToList();
        string strTeamIDs = string.Join(", ", userList.Select(o => o.userID).ToArray());
        hdnAllTeamIDs.Value = strTeamIDs;
        return ds;
    }

    private DataSet GetUserByTeamID(Int32 TeamID, string UserIDs)
    {
        UserBM obj = new UserBM();
        obj.TeamID = TeamID;
        obj.UserIDs = UserIDs;
        obj.GetUserByTeamID();
        DataSet ds = obj.ds;
        return ds;
    }

    protected void GetAssignTeamList()
    {
        //JobTypeBM obj = new JobTypeBM();
        //obj.jobIsActive = true;
        //obj.jobIsDeleted = false;
        //obj.jobCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        //obj.GetAllTeam();
        //DataSet ds = obj.ds;

        DataSet ds = GetTeamListByIDs("0");




        #region AssignTab
        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (Convert.ToString(Session["Culture"]) == "Danish")
                {

                    ddlAssignTeam.Items.Clear();

                    ddlAssignTeam.DataSource = ds.Tables[0];
                    ddlAssignTeam.DataTextField = "TeamNameDN";
                    ddlAssignTeam.DataValueField = "TeamID";
                    ddlAssignTeam.DataBind();

                    ddlAssignTeam.Items.Insert(0, new ListItem(hdnSelectTeam.Value, CommonModule.dropDownZeroValue));
                }

                else
                {
                    ddlAssignTeam.Items.Clear();

                    ddlAssignTeam.DataSource = ds.Tables[0];
                    ddlAssignTeam.DataTextField = "TeamName";
                    ddlAssignTeam.DataValueField = "TeamID";
                    ddlAssignTeam.DataBind();

                    ddlAssignTeam.Items.Insert(0, new ListItem(hdnSelectTeam.Value, CommonModule.dropDownZeroValue));

                }
            }
            else
            {
                ddlAssignTeam.Items.Clear();
                ddlAssignTeam.Items.Insert(0, new ListItem(hdnSelectTeam.Value, CommonModule.dropDownZeroValue));
            }
        }
        else
        {
            ddlAssignTeam.Items.Clear();
            ddlAssignTeam.Items.Insert(0, new ListItem(hdnSelectTeam.Value, CommonModule.dropDownZeroValue));
        }
        #endregion
    }

    protected DataSet GetCategoryElementByID(string selID)
    {
        CompanyDirectionElement obj = new CompanyDirectionElement();
        try
        {
            var logginUserType = Convert.ToInt32(Session["OrguserType"]);
            var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
            var logginUserId = Convert.ToInt32(Session["OrgUserId"]);

            obj.dirCatID = selID;
            obj.dirIsActive = true;
            obj.dirIsDelete = false;
            obj.GetElementByID();

        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex);
        }

        DataSet ds = obj.ds;

        return ds;
    }

    protected void ddlCatEle_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "", "DisplayTab(2);", true);

        try
        {
            DataSet ds = GetCategoryElementByID(ddldirCat2.SelectedValue);

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddldirele.Items.Clear();

                    ddldirele.DataSource = ds.Tables[0];
                    if (Convert.ToString(Session["Culture"]) == "Danish")
                    {
                        ddldirele.DataTextField = "dirNameDN";
                    }
                    else
                    {
                        ddldirele.DataTextField = "dirName";
                    }
                    ddldirele.DataValueField = "dirID";
                    ddldirele.DataBind();

                    ddldirele.DataSource = ds.Tables[0];
                    if (Convert.ToString(Session["Culture"]) == "Danish")
                    {
                        ddldirele.DataTextField = "dirNameDN";
                    }
                    else
                    {
                        ddldirele.DataTextField = "dirName";
                    }
                    ddldirele.DataValueField = "dirID";
                    ddldirele.DataBind();

                    ddldirele.Items.Insert(0, new ListItem(hdnSelectdirele.Value, CommonModule.dropDownZeroValue));
                }
                else
                {
                    ddldirele.Items.Clear();
                    ddldirele.Items.Insert(0, new ListItem(hdnSelectdirele.Value, CommonModule.dropDownZeroValue));
                }
            }
            else
            {
                ddldirele.Items.Clear();
                ddldirele.Items.Insert(0, new ListItem(hdnSelectdirele.Value, CommonModule.dropDownZeroValue));
            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "ddlCatEle_OnSelectedIndexChanged");
        }
    }

    private DataSet GetElementBy_Category(string dirCatID, string dirIds)
    {
        CompanyDirectionElement obj = new CompanyDirectionElement();
        obj.dirCatID = dirCatID;

        obj.dirIDs = dirIds;
        obj.GetElementBy_Category();
        DataSet ds = obj.ds;
        return ds;

    }

    protected void ddlAssignTeam_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "", "DisplayTab(2);", true);
        //UserBM obj = new UserBM();
        //obj.TeamID = Convert.ToInt32(ddlAssignTeam.SelectedValue);
        //obj.UserIDs = "0";
        //obj.GetUserByTeamID();
        //DataSet ds = obj.ds;
        if (Convert.ToInt32(ddlAssignTeam.SelectedValue) != 0)
        {
            DataSet ds = GetUserByTeamID(Convert.ToInt32(ddlAssignTeam.SelectedValue), "0");

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlAssignUser.Items.Clear();

                    ddlAssignUser.DataSource = ds.Tables[0];
                    if (Convert.ToString(Session["Culture"]) == "Danish")
                    {
                        ddlAssignUser.DataTextField = "userFirstName";
                    }
                    else
                    {
                        ddlAssignUser.DataTextField = "userFirstName";
                    }
                    ddlAssignUser.DataValueField = "userId";
                    ddlAssignUser.DataBind();

                    ddlAssignUser.Items.Insert(0, new ListItem(hdnSelectUser.Value, CommonModule.dropDownZeroValue));
                }
                else
                {
                    ddlAssignUser.Items.Clear();
                    ddlAssignUser.Items.Insert(0, new ListItem(hdnSelectUser.Value, CommonModule.dropDownZeroValue));
                }
            }
            else
            {
                ddlAssignUser.Items.Clear();
                ddlAssignUser.Items.Insert(0, new ListItem(hdnSelectUser.Value, CommonModule.dropDownZeroValue));
            }
        }
        else
        {
            ddlAssignUser.Items.Clear();
            ddlAssignUser.Items.Insert(0, new ListItem(hdnSelectUser.Value, CommonModule.dropDownZeroValue));
        }

    }

    protected void GetGridAssignCategoryList(string strEleIDs)
    {
        try
        {
            var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
            var userId = Convert.ToInt32(Session["OrgUserId"]);
            try
            {
                DataSet ds = GetCategoryById(strEleIDs);

                gvCatTemplate.DataSource = ds.Tables[0];
                gvCatTemplate.DataBind();
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex);
        }

    }

    protected void GetGridAssignCategoryElementList(string TmpIDs)
    {
        try
        {
            var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
            var userId = Convert.ToInt32(Session["OrgUserId"]);
            try
            {
                DataSet ds = GetCategoryById(TmpIDs);

                gvCatTemplate.DataSource = ds.Tables[0];
                gvCatTemplate.DataBind();
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex);
        }

    }

    protected void gvCatTemplate_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(Session["OrgUserId"]);
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                List<CatEle> data = new List<CatEle>();
                try
                {
                    data = (List<CatEle>)Session["ListCat"];
                }
                catch (Exception ex)
                {
                    ExceptionLogger.LogException(ex, 0, "gvCatTemplate_OnRowDataBound");
                }

                // HiddenField direle = (HiddenField)e.Row.FindControl("hdnID");
                HiddenField dirIDs = (HiddenField)e.Row.FindControl("hdndirID");
                //HiddenField IsTmpMandatory = (HiddenField)e.Row.FindControl("hdnIsTmpMandatory");
                Label NoRecordFound = (Label)e.Row.FindControl("lblNoRecordFoundCat");
                GridView gvelement = e.Row.FindControl("gvelement") as GridView;

                Int32 qTmpID = Convert.ToInt32(dirIDs.Value);
                string strTmpQues = "0";

                var search = data.FirstOrDefault(o => o.CatID == qTmpID);
                if (search != null)
                {
                    strTmpQues = string.Join(",", search.EleIDs.Select(o => o.EleID).ToArray());
                }

                if (gvelement != null)
                {


                    DataSet ds = GetElementBy_Category(Convert.ToString(qTmpID), strTmpQues);
                    if (ds != null)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            gvelement.DataSource = ds.Tables[0];
                            gvelement.DataBind();
                            NoRecordFound.Visible = false;
                        }
                        else
                        {
                            NoRecordFound.Visible = true;
                        }
                    }
                    else
                    {
                        NoRecordFound.Visible = true;
                    }

                }
            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "gvCatTemplate_OnRowDataBound");
        }


    }


    protected void btnAssignSelectCat_Click(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "", "DisplayTab(2);", true);
        try
        {


            List<CatEle> data = (List<CatEle>)Session["ListCat"];
            if (data == null)
            {
                data = new List<CatEle>();
            }
            Int32 catID = Convert.ToInt32(ddldirCat2.SelectedValue);

            var searchTemp = data.FirstOrDefault(o => o.CatID == catID);
            if (searchTemp == null)
            {
                DataSet ds = GetCategoryElementByID(ddldirCat2.SelectedValue);

                var eleList = ds.Tables[0].AsEnumerable().Select(dataRow => new EleIDs
                {
                    EleID = dataRow.Field<int>("dirID"),

                }).ToList();

                searchTemp = new CatEle();
                searchTemp.CatID = catID;
                // searchTemp.EleIDs = new List<EleIDs>();
                searchTemp.EleIDs = eleList;
                data.Add(searchTemp);
                Session["ListCat"] = data;
            }

            string strEleIDs = string.Join(", ", data.Select(o => o.CatID).ToArray());
            GetGridAssignCategoryList(strEleIDs);
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "btnAssignSelectCatTemplate_Click");
        }


    }

    protected void btnAssignResetCatTemplate_Click(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "", "DisplayTab(2);", true);
        try
        {
            List<CatEle> temp = new List<CatEle>();
            CatEle t = new CatEle();
            t.CatID = -1;
            t.EleIDs = new List<EleIDs>();
            temp.Add(t);
            Session["ListCat"] = temp;

            string strEleIDs = string.Join(", ", temp.Select(o => o.CatID).ToArray());
            GetGridAssignCategoryList(strEleIDs);
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "btnAssignResetCatTemplate_Click");
        }


    }

    protected void btnAssignSelectElement_Click(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "", "DisplayTab(2);", true);
        try
        {


            List<CatEle> data = (List<CatEle>)Session["ListCat"];
            if (data == null)
            {
                data = new List<CatEle>();
            }
            Int32 catID = Convert.ToInt32(ddldirCat2.SelectedValue);
            Int32 EleIDs = Convert.ToInt32(ddldirele.SelectedValue);

            var searchTemp = data.FirstOrDefault(o => o.CatID == catID);
            if (searchTemp == null)
            {
                searchTemp = new CatEle();
                searchTemp.CatID = catID;
                searchTemp.EleIDs = new List<EleIDs>();

                List<EleIDs> qtm = new List<EleIDs>();
                EleIDs qtmtmp = new EleIDs();
                qtmtmp.EleID = EleIDs;
                qtm.Add(qtmtmp);
                searchTemp.EleIDs = qtm;
                //EleIDs te = new EleIDs();
                //te.EleID = EleIDs;
                //searchTemp.EleIDs.Add(te);
                data.Add(searchTemp);
            }
            else
            {
                if (!searchTemp.EleIDs.Any(o => o.EleID == EleIDs))
                {
                    EleIDs q = new EleIDs();
                    q.EleID = EleIDs;
                    searchTemp.EleIDs.Add(q);
                }
                data.Remove(searchTemp);
                data.Add(searchTemp);
            }

            Session["ListCat"] = data;

            string strTempIDs = string.Join(", ", data.Select(o => o.CatID).ToArray());

            //string part2 = strTempIDs.Split(',')[1];            
            // GetElementBy_Category(part2,"0");

            GetGridAssignCategoryList(strTempIDs);

        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "btnAssignSelectElement_Click");
        }

    }


    protected void btnAssignSelectTeam_Click(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "", "DisplayTab(2);", true);
        try
        {
            List<TeamUser> data = (List<TeamUser>)Session["ListTeamUser"];
            if (data == null)
            {
                data = new List<TeamUser>();
            }
            Int32 tmpID = Convert.ToInt32(ddlAssignTeam.SelectedValue);

            var searchTemp = data.FirstOrDefault(o => o.TeamID == tmpID);
            if (searchTemp == null)
            {
                DataSet ds = GetUserByTeamID(tmpID, "0");

                var userList = ds.Tables[0].AsEnumerable().Select(dataRow => new UserIDs
                {
                    userID = dataRow.Field<int>("userId"),

                }).ToList();

                searchTemp = new TeamUser();
                searchTemp.TeamID = tmpID;
                // searchTemp.UserIDs = new List<UserIDs>();
                searchTemp.UserIDs = userList;
                data.Add(searchTemp);

                Session["ListTeamUser"] = data;
            }
            else
            {
                //DataSet ds = GetUserByTeamID(tmpID, "0");

                //var userList = ds.Tables[0].AsEnumerable().Select(dataRow => new UserIDs
                //{
                //    userID = dataRow.Field<int>("userId"),

                //}).ToList();

                GetTeamListByIDs(Convert.ToString(searchTemp.TeamID));
                if (!String.IsNullOrEmpty(hdnAllTeamIDs.Value))
                {
                    var str = hdnAllTeamIDs.Value;
                    string[] strtmp = str.Split(',');
                    foreach (var i in strtmp)
                    {
                        DataSet ds = GetUserByTeamID(Convert.ToInt32(i), "0");

                        var userList = ds.Tables[0].AsEnumerable().Select(dataRow => new UserIDs
                        {
                            userID = dataRow.Field<int>("userId"),

                        }).ToList();

                        searchTemp = new TeamUser();
                        searchTemp.TeamID = Convert.ToInt32(i); ;
                        // searchTemp.UserIDs = new List<UserIDs>();
                        searchTemp.UserIDs = userList;
                        data.Add(searchTemp);
                    }
                }

                Session["ListTeamUser"] = data;

            }
            string strTeamIDs = string.Join(", ", data.Select(o => o.TeamID).ToArray());
            GetGridAssignTeamList(strTeamIDs);
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "btnAssignSelectTeam_Click");
        }

    }
    protected void btnAssignResetTeam_Click(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "", "DisplayTab(2);", true);
        try
        {
            List<TeamUser> tempuser = new List<TeamUser>();
            TeamUser tt = new TeamUser();
            tt.TeamID = -1;
            tt.UserIDs = new List<UserIDs>();
            tempuser.Add(tt);
            Session["ListTeamUser"] = tempuser;

            string strTeamIDs = string.Join(", ", tempuser.Select(o => o.TeamID).ToArray());
            GetGridAssignTeamList(strTeamIDs);
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "btnAssignResetTeam_Click");
        }

    }
    protected void btnAssignSelectTeamUser_Click(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "", "DisplayTab(2);", true);
        try
        {
            List<TeamUser> data = (List<TeamUser>)Session["ListTeamUser"];
            if (data == null)
            {
                data = new List<TeamUser>();
            }
            Int32 tmpID = Convert.ToInt32(ddlAssignTeam.SelectedValue);
            Int32 UserID = Convert.ToInt32(ddlAssignUser.SelectedValue);

            var searchTemp = data.FirstOrDefault(o => o.TeamID == tmpID);
            if (searchTemp == null)
            {
                searchTemp = new TeamUser();
                searchTemp.TeamID = tmpID;
                // searchTemp.UserIDs = new List<UserIDs>();
                List<UserIDs> tmpu = new List<UserIDs>();
                UserIDs tmputmp = new UserIDs();
                tmputmp.userID = UserID;
                tmpu.Add(tmputmp);

                searchTemp.UserIDs = tmpu;
                data.Add(searchTemp);
            }
            else
            {
                if (!searchTemp.UserIDs.Any(o => o.userID == UserID))
                {
                    UserIDs q = new UserIDs();
                    q.userID = UserID;
                    searchTemp.UserIDs.Add(q);
                }
                data.Remove(searchTemp);
                data.Add(searchTemp);
            }

            Session["ListTeamUser"] = data;

            string strTeamIDs = string.Join(", ", data.Select(o => o.TeamID).ToArray());
            GetGridAssignTeamList(strTeamIDs);
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "btnAssignSelectTeamUser_Click");
        }
    }

    protected void GetGridAssignTeamList(string TeamIDs)
    {
        DataSet ds = GetTeamListByIDs(TeamIDs);

        //if (Convert.ToString(Session["Culture"]) == "Danish")
        //{
        //    ds.Tables[0].Columns["TeamName"].ColumnName = "abcd";
        //    ds.Tables[0].Columns["TeamNameDN"].ColumnName = "jobName";

        //}

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                gridTeamList.DataSource = ds.Tables[0];
                gridTeamList.DataBind();

                gridTeamList.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gridTeamList.DataSource = null;
                gridTeamList.DataBind();
            }
        }
        else
        {
            gridTeamList.DataSource = null;
            gridTeamList.DataBind();
        }
    }
    protected void gridTeamList_OnRowDataBoundTeam(object sender, GridViewRowEventArgs e)
    {
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(Session["OrgUserId"]);
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                List<TeamUser> data = new List<TeamUser>();
                try
                {
                    data = (List<TeamUser>)Session["ListTeamUser"];
                }
                catch (Exception ex)
                {
                    ExceptionLogger.LogException(ex, 0, "gridTeamList_OnRowDataBoundTeam");
                }


                HiddenField UserTeamID = (HiddenField)e.Row.FindControl("hdnTeamID");
                Label NoRecordFound = (Label)e.Row.FindControl("lblNoRecordFound");
                GridView gvTeamUser = e.Row.FindControl("gvTeamUser") as GridView;

                Int32 qTmpID = Convert.ToInt32(UserTeamID.Value);
                string strTmpUser = "0";

                var search = data.FirstOrDefault(o => o.TeamID == qTmpID);
                if (search != null)
                {
                    strTmpUser = string.Join(",", search.UserIDs.Select(o => o.userID).ToArray());
                }

                if (gvTeamUser != null)
                {
                    DataSet ds = GetUserByTeamID(Convert.ToInt32(UserTeamID.Value), strTmpUser);

                    if (ds != null)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            gvTeamUser.DataSource = ds.Tables[0];
                            gvTeamUser.DataBind();
                            NoRecordFound.Visible = false;
                        }
                        else
                        {
                            NoRecordFound.Visible = true;
                        }
                    }
                    else
                    {
                        NoRecordFound.Visible = true;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "gvQues->OnRowDataBoundTeam");
        }


    }
    #endregion


    #region AssignTab
    protected void btnAssign_click(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "", "DisplayTab(2);", true);

        var logginUserType = Convert.ToInt32(Session["OrguserType"]);
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(Session["OrgUserId"]);

        //DateTime stdt = CommonUtility.ConvertStringToDateTimewithformay(txtSearchStartDate_Public.Text);
        //return;

        try
        {
            string controlID = string.Empty;
            string innercontrolID = string.Empty;
            List<CatEle> qData = new List<CatEle>();
            List<CatEle> tmpqData = new List<CatEle>();
            qData = (List<CatEle>)Session["ListCat"];


            List<TeamUser> tData = new List<TeamUser>();
            List<TeamUser> tmptData = new List<TeamUser>();
            tData = (List<TeamUser>)Session["ListTeamUser"];

            #region Template
            foreach (var a in qData)
            {
                CatEle tq = new CatEle();
                tq.CatID = a.CatID;
                tq.EleIDs = new List<EleIDs>();

                foreach (GridViewRow row in gvCatTemplate.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        //  var mp = ((HiddenField)row.FindControl("hdnChkMainQuesTemp"));

                        var mp = (row.Cells[2].FindControl("hdnChkMainQuesTemp") as HiddenField);
                        GridView gvQues = (row.Cells[0].FindControl("gvelement") as GridView);

                        foreach (var b in a.EleIDs)
                        {
                            foreach (GridViewRow innerrow in gvQues.Rows)
                            {
                                if (row.RowType == DataControlRowType.DataRow)
                                {
                                    var p = (innerrow.Cells[1].FindControl("hdnChkQuesTemp") as HiddenField);
                                    var pQues = (innerrow.Cells[1].FindControl("hdnID") as HiddenField);

                                    string[] pQuesIds = pQues.Value.Split(',');

                                    if (pQuesIds.Contains(Convert.ToString(b.EleID)))
                                    {
                                        if (p.Value == "1,1" || p.Value == "1")
                                        {
                                            if (!tq.EleIDs.Any(o => o.EleID == b.EleID))
                                            {
                                                tq.EleIDs.Add(b);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
                if (tq.EleIDs.Count > 0)
                {
                    tmpqData.Add(tq);
                }
            }
            #endregion

            #region Team
            foreach (var a in tData)
            {
                TeamUser tq = new TeamUser();
                tq.TeamID = a.TeamID;
                tq.UserIDs = new List<UserIDs>();

                foreach (GridViewRow row in gridTeamList.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        //  var mp = ((HiddenField)row.FindControl("hdnChkMainQuesTemp"));
                        var mp = (row.Cells[2].FindControl("hdnChkMainTeam") as HiddenField);
                        GridView gvTeamUser = (row.Cells[0].FindControl("gvTeamUser") as GridView);

                        foreach (var b in a.UserIDs)
                        {
                            foreach (GridViewRow innerrow in gvTeamUser.Rows)
                            {
                                if (row.RowType == DataControlRowType.DataRow)
                                {
                                    var p = (innerrow.Cells[1].FindControl("hdnChkTeamUser") as HiddenField);
                                    var pUser = (innerrow.Cells[1].FindControl("hdnUsernoID") as HiddenField);
                                    string[] pUserIds = pUser.Value.Split(',');

                                    if (pUserIds.Contains(Convert.ToString(b.userID)))
                                    {
                                        if (p.Value == "1,1" || p.Value == "1" || p.Value=="1,0")
                                        {
                                            if (!tq.UserIDs.Any(o => o.userID == b.userID))
                                            {
                                                tq.UserIDs.Add(b);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
                if (tq.UserIDs.Count > 0)
                {
                    tmptData.Add(tq);
                }
            }
            #endregion

            if (tmpqData.Count() > 0 && tmptData.Count() > 0)
            {
                foreach (var x in tmpqData)
                {
                    string quesIDs = string.Join(",", x.EleIDs.Select(o => o.EleID).ToArray());
                    foreach (var y in tmptData)
                    {
                        string userIDs = string.Join(",", y.UserIDs.Select(o => o.userID).ToArray());
                        CompanyDirectionElement q = new CompanyDirectionElement();
                        q.dassignID = 0;
                        q.UserID = userId;
                        q.dirCompanyID = companyId;
                        q.StartDate = CommonUtility.ConvertStringToDateTimewithformay(txtSearchStartDate_Public.Text);
                        q.EndDate = CommonUtility.ConvertStringToDateTimewithformay(txtSearchEndDate_Public.Text);
                        q.dirIsActive = true;
                        q.dirIsDelete = false;
                        q.Status = "Assign";
                        q.dirCatID = Convert.ToString(x.CatID);
                        q.ElementIDs = quesIDs;
                        q.TeamID = y.TeamID;
                        q.UserIDs = userIDs;

                        q.InsertDirectionAssign();
                    }
                }

                GetAllAssignDirectionList();

                //Note: After saving Data reinitialize the List
                List<CatEle> temp = new List<CatEle>();
                CatEle t = new CatEle();
                t.CatID = -1;
                t.EleIDs = new List<EleIDs>();
                temp.Add(t);
                Session["ListCat"] = temp;
                string strEleIDs = string.Join(", ", temp.Select(o => o.CatID).ToArray());
                GetGridAssignCategoryList(strEleIDs);

                List<TeamUser> tempuser = new List<TeamUser>();
                TeamUser tt = new TeamUser();
                tt.TeamID = -1;
                tt.UserIDs = new List<UserIDs>();
                tempuser.Add(tt);
                string strTeamIDs = string.Join(", ", tempuser.Select(o => o.TeamID).ToArray());
                GetGridAssignTeamList(strTeamIDs);

                TeamUser tt1 = new TeamUser();
                tt1.TeamID = 0;
                tt1.UserIDs = new List<UserIDs>();
                tempuser.Add(tt1);
                Session["ListTeamUser"] = tempuser;
            }

        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "btnAssign_click");
        }
    }
    private DataSet GetAssignDirectionList()
    {
        var logginUserType = Convert.ToInt32(Session["OrguserType"]);
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(Session["OrgUserId"]);
        ResLangId = Convert.ToInt32(Session["resLangID"]);

        CompanyDirectionElement obj = new CompanyDirectionElement();
        obj.UserID = userId;
        obj.dirCompanyID = companyId;
        obj.dirIsActive = true;
        obj.dirIsDelete = false;
        obj.LangID = ResLangId;
        obj.GetAllAssignDirectionList();
        DataSet ds = obj.ds;
        return ds;
    }
    protected void GetAllAssignDirectionList()
    {
        try
        {
            DataSet ds = GetAssignDirectionList();

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    grid_DirectionAssign.DataSource = ds.Tables[0];
                    grid_DirectionAssign.DataBind();

                    gridPublishDirection.DataSource = ds.Tables[0];
                    gridPublishDirection.DataBind();

                    grid_DirectionAssign.HeaderRow.TableSection = TableRowSection.TableHeader;

                    gridPublishDirection.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                else
                {
                    grid_DirectionAssign.DataSource = null;
                    grid_DirectionAssign.DataBind();

                    gridPublishDirection.DataSource = null;
                    gridPublishDirection.DataBind();
                }
            }
            else
            {
                grid_DirectionAssign.DataSource = null;
                grid_DirectionAssign.DataBind();

                gridPublishDirection.DataSource = null;
                gridPublishDirection.DataBind();
            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "GetAllAssignDirectionList");
        }
    }


    protected void grid_DirectionAssign_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(Session["OrgUserId"]);
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField AssignID = (HiddenField)e.Row.FindControl("hdngridgridPublishDirection_RowDataBound");
                HiddenField TemplateID = (HiddenField)e.Row.FindControl("hdngridAssignDirCatID");
                HiddenField TeamID = (HiddenField)e.Row.FindControl("hdngridAssignTeamID");

                HiddenField QuesIDs = (HiddenField)e.Row.FindControl("hdngridAssignElementIDs");
                HiddenField UserIDs = (HiddenField)e.Row.FindControl("hdngridAssignUserIDs");

                DataSet dst = new DataSet();
                //  dst = GetQuesByTemplateID_CategoryID(Convert.ToInt32(TemplateID.Value), 0, true, QuesIDs.Value);
                dst = GetElementBy_Category(TemplateID.Value, QuesIDs.Value);

                DataSet dsTeam = new DataSet();
                dsTeam = GetUserByTeamID(Convert.ToInt32(TeamID.Value), UserIDs.Value);
                GridView gvQues_AssignList = e.Row.FindControl("gvQues_AssignList") as GridView;

                GridView gvTeamUser_AssignList = e.Row.FindControl("gvTeamUser_AssignList") as GridView;
                if (dst != null)
                {
                    if (dst.Tables[0].Rows.Count > 0)
                    {
                        gvQues_AssignList.DataSource = dst.Tables[0];
                        gvQues_AssignList.DataBind();

                        gvQues_AssignList.HeaderRow.TableSection = TableRowSection.TableHeader;
                    }
                    else
                    {
                        gvQues_AssignList.DataSource = null;
                        gvQues_AssignList.DataBind();
                    }
                }
                else
                {
                    gvQues_AssignList.DataSource = null;
                    gvQues_AssignList.DataBind();
                }

                if (dsTeam != null)
                {
                    if (dsTeam.Tables[0].Rows.Count > 0)
                    {
                        gvTeamUser_AssignList.DataSource = dsTeam.Tables[0];
                        gvTeamUser_AssignList.DataBind();

                        gvTeamUser_AssignList.HeaderRow.TableSection = TableRowSection.TableHeader;
                    }
                    else
                    {
                        gvTeamUser_AssignList.DataSource = null;
                        gvTeamUser_AssignList.DataBind();
                    }
                }
                else
                {
                    gvTeamUser_AssignList.DataSource = null;
                    gvTeamUser_AssignList.DataBind();
                }

            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "grid_DirectionAssign_RowDataBound");
        }
    }
    protected void grid_DirectionAssign_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "", "DisplayTab(2);", true);
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(Session["OrgUserId"]);
        if (e.CommandName == "archive")
        {
            CompanyDirectionElement obj = new CompanyDirectionElement();
            obj.dassignID = Convert.ToInt32(e.CommandArgument);

            obj.DeleteDirectionAssign();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    GetAllAssignDirectionList();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }

    }
    //protected void grid_QuestionAssign_RowCommand(object sender, GridViewCommandEventArgs e)
    //{
    //    ClientScript.RegisterStartupScript(this.GetType(), "", "DisplayTab(2);", true);
    //    var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
    //    var userId = Convert.ToInt32(Session["OrgUserId"]);
    //    if (e.CommandName == "archive")
    //    {
    //        QuestionBM obj = new QuestionBM();
    //        obj.AssignID = Convert.ToInt32(e.CommandArgument);

    //        obj.DeleteQuestionAssign();

    //        DataSet ds = obj.ds;
    //        if (ds != null)
    //        {
    //            string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
    //            if (returnMsg == "success")
    //            {
    //                GetAllAssignQuestionList();
    //                lblMsg.Text = CommonModule.msgRecordArchiveSucces;
    //                lblMsg.ForeColor = System.Drawing.Color.White;
    //            }
    //            else
    //            {
    //                lblMsg.Text = CommonModule.msgSomeProblemOccure;
    //            }
    //        }
    //        else
    //        {
    //            lblMsg.Text = CommonModule.msgSomeProblemOccure;
    //        }
    //    }


    //}
    #endregion


    #region Publish
    protected void gridPublishDirection_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(Session["OrgUserId"]);
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField AssignID = (HiddenField)e.Row.FindControl("hdngridPublishAssignID");
                HiddenField TemplateID = (HiddenField)e.Row.FindControl("hdngridPublishCatID");
                HiddenField TeamID = (HiddenField)e.Row.FindControl("hdngridPublishTeamID");

                HiddenField QuesIDs = (HiddenField)e.Row.FindControl("hdngridPublishElementIDs");
                HiddenField UserIDs = (HiddenField)e.Row.FindControl("hdngridPublishUserIDs");



                HiddenField hdnStatusPublish = (HiddenField)e.Row.FindControl("hdnStatusPublish");
                Label lblStatusPublish = (Label)e.Row.FindControl("lblStatusPublish");
                LinkButton lbkPublishQues = (LinkButton)e.Row.FindControl("lbkPublishDirection");



                if (Convert.ToString(hdnStatusPublish.Value) == "Assign")
                {
                    lblStatusPublish.Text = GetLocalResourceObject("assignres1.Text").ToString();
                }
                else if (Convert.ToString(hdnStatusPublish.Value) == "Publish")
                {
                    lblStatusPublish.Text = GetLocalResourceObject("publishres1.Text").ToString();
                }
                else
                {
                    lblStatusPublish.Text = GetLocalResourceObject("unpublishres1.Text").ToString();
                }
                if (Convert.ToString(hdnStatusPublish.Value) == "Publish")
                {
                    lbkPublishQues.Text = GetLocalResourceObject("unpublishres.Text").ToString();
                }
                else
                {
                    lbkPublishQues.Text = GetLocalResourceObject("publishres.Text").ToString();
                }

                DataSet dst = new DataSet();
                dst = GetElementBy_Category(Convert.ToString(TemplateID.Value), QuesIDs.Value);

                DataSet dsTeam = new DataSet();
                dsTeam = GetUserByTeamID(Convert.ToInt32(TeamID.Value), UserIDs.Value);
                GridView gvPublishQues_AssignList = e.Row.FindControl("gvPublishDirection_AssignList") as GridView;

                GridView gvPublishTeamUser_AssignList = e.Row.FindControl("gvPublishTeamUser_AssignList") as GridView;
                if (dst != null)
                {
                    if (dst.Tables[0].Rows.Count > 0)
                    {
                        gvPublishQues_AssignList.DataSource = dst.Tables[0];
                        gvPublishQues_AssignList.DataBind();

                        gvPublishQues_AssignList.HeaderRow.TableSection = TableRowSection.TableHeader;
                    }
                    else
                    {
                        gvPublishQues_AssignList.DataSource = null;
                        gvPublishQues_AssignList.DataBind();
                    }
                }
                else
                {
                    gvPublishQues_AssignList.DataSource = null;
                    gvPublishQues_AssignList.DataBind();
                }

                if (dsTeam != null)
                {
                    if (dsTeam.Tables[0].Rows.Count > 0)
                    {
                        gvPublishTeamUser_AssignList.DataSource = dsTeam.Tables[0];
                        gvPublishTeamUser_AssignList.DataBind();

                        gvPublishTeamUser_AssignList.HeaderRow.TableSection = TableRowSection.TableHeader;
                    }
                    else
                    {
                        gvPublishTeamUser_AssignList.DataSource = null;
                        gvPublishTeamUser_AssignList.DataBind();
                    }
                }
                else
                {
                    gvPublishTeamUser_AssignList.DataSource = null;
                    gvPublishTeamUser_AssignList.DataBind();
                }

            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "gridPublishDirection_RowDataBound");
        }
    }
    protected void gridPublishDirection_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "", "DisplayTab(3);", true);
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(Session["OrgUserId"]);
        if (e.CommandName == "archive")
        {
            CompanyDirectionElement obj = new CompanyDirectionElement();
            obj.dassignID = Convert.ToInt32(e.CommandArgument);

            obj.DeleteDirectionAssign();
            
            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    GetAllAssignDirectionList();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }
        else if (e.CommandName == "Publish")
        {
            try
            {
                CompanyDirectionElement obj = new CompanyDirectionElement();
                obj.dassignID = Convert.ToInt32(e.CommandArgument);
                obj.Status = "Publish";
                obj.UpdateDirectionAssignStatus();

                DataSet ds = obj.ds;
                try
                {
                    if (ds != null)
                    {
                        string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                        if (returnMsg == "success")
                        {
                            var db = new startetkuEntities1();
                            db.InsertUpdateEmailData(companyId,userId,"0",obj.dassignID,"CompanyDir","");
                            db.InsertNotification_Publish("CompanyDir", companyId, userId, obj.dassignID, "comdirPub", "New Company Direction Publish");
                            GetAllAssignDirectionList();
                            GetPublishDirectionUserList();
                        }
                    }
                }
                catch (Exception ex)
                {
                    ExceptionLogger.LogException(ex, 0, "com Publish->InsertNotification_Publish");
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex, 0, "gridPublishDirection_OnRowCommand->Publish");
            }

        }
        else if (e.CommandName == "UnPublish")
        {
            try
            {
                CompanyDirectionElement obj = new CompanyDirectionElement();
                obj.dassignID = Convert.ToInt32(e.CommandArgument);
                obj.Status = "UnPublish";
                obj.UpdateDirectionAssignStatus();

                DataSet ds = obj.ds;
                if (ds != null)
                {
                    string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                    if (returnMsg == "success")
                    {
                        GetAllAssignDirectionList();
                        GetPublishDirectionUserList();
                        GetPublishDirectionUserWiseElementList(0);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex, 0, "gridPublishDirection_OnRowCommand->UnPublish");
            }
        }


    }
    private void GetPublishDirectionUserWiseElementList(int assignUserID)
    {
        DataSet ds = GetPublishUserDirectionList(assignUserID);


        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvTeamUser_PublishQuesList.DataSource = ds.Tables[0];
                gvTeamUser_PublishQuesList.DataBind();

                gvTeamUser_PublishQuesList.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvTeamUser_PublishQuesList.DataSource = null;
                gvTeamUser_PublishQuesList.DataBind();
            }
        }
        else
        {
            gvTeamUser_PublishQuesList.DataSource = null;
            gvTeamUser_PublishQuesList.DataBind();
        }
    }
    protected void GetPublishDirectionUserList()
    {
        var logginUserType = Convert.ToInt32(Session["OrguserType"]);
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(Session["OrgUserId"]);
        try
        {


            CompanyDirectionElement obj = new CompanyDirectionElement();
            obj.UserID = userId;
            obj.dirCompanyID = companyId;

            obj.GetPublishDirectionUserList();
            DataSet ds = obj.ds;

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvTeamUser_PublishList.DataSource = ds.Tables[0];
                    gvTeamUser_PublishList.DataBind();

                    gvTeamUser_PublishList.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                else
                {
                    gvTeamUser_PublishList.DataSource = null;
                    gvTeamUser_PublishList.DataBind();
                }
            }
            else
            {
                gvTeamUser_PublishList.DataSource = null;
                gvTeamUser_PublishList.DataBind();
            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "GetPublishDirectionUserList");
        }

    }

    protected void gvTeamUser_PublishList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "", "DisplayTab(3);", true);
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(Session["OrgUserId"]);

        if (e.CommandName == "ViewRow")
        {
            hdnPublishTeamUserID.Value = Convert.ToString(e.CommandArgument);
            Int32 assignUserID = Convert.ToInt32(e.CommandArgument);

            GetPublishDirectionUserWiseElementList(assignUserID);
            //DataSet ds = GetPublishUserDirectionList(assignUserID);


            //if (ds != null)
            //{
            //    if (ds.Tables[0].Rows.Count > 0)
            //    {
            //        gvTeamUser_PublishQuesList.DataSource = ds.Tables[0];
            //        gvTeamUser_PublishQuesList.DataBind();

            //        gvTeamUser_PublishQuesList.HeaderRow.TableSection = TableRowSection.TableHeader;
            //    }
            //    else
            //    {
            //        gvTeamUser_PublishQuesList.DataSource = null;
            //        gvTeamUser_PublishQuesList.DataBind();
            //    }
            //}
            //else
            //{
            //    gvTeamUser_PublishQuesList.DataSource = null;
            //    gvTeamUser_PublishQuesList.DataBind();
            //}
        }
    }
    private DataSet GetPublishUserDirectionList(int assignuserID)
    {
        var logginUserType = Convert.ToInt32(Session["OrguserType"]);
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(Session["OrgUserId"]);
        ResLangId = Convert.ToInt32(Session["resLangID"]);
        DataSet ds = new DataSet();
        try
        {
            CompanyDirectionElement obj = new CompanyDirectionElement();
            obj.UserID = userId;
            obj.dirCompanyID = companyId;
            obj.dassignID = assignuserID;
            obj.Status = ddlFilterAssignPublishDirection.SelectedValue;
            obj.LangID = ResLangId;
            obj.GetPublishUserDirectionList();
            ds = obj.ds;
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "GetPublishUserDirectionList");
        }
        return ds;
    }

    protected void ddlFilterAssignPublishDirection_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "", "DisplayTab(3);", true);
        try
        {

            Int32 assignUserID = Convert.ToInt32(hdnPublishTeamUserID.Value);
            GetPublishDirectionUserWiseElementList(assignUserID);
            //DataSet ds = GetPublishUserDirectionList(assignUserID);


            //if (ds != null)
            //{
            //    if (ds.Tables[0].Rows.Count > 0)
            //    {
            //        gvTeamUser_PublishQuesList.DataSource = ds.Tables[0];
            //        gvTeamUser_PublishQuesList.DataBind();

            //        gvTeamUser_PublishQuesList.HeaderRow.TableSection = TableRowSection.TableHeader;
            //    }
            //    else
            //    {
            //        gvTeamUser_PublishQuesList.DataSource = null;
            //        gvTeamUser_PublishQuesList.DataBind();
            //    }
            //}
            //else
            //{
            //    gvTeamUser_PublishQuesList.DataSource = null;
            //    gvTeamUser_PublishQuesList.DataBind();
            //}
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "ddlFilterAssignPublishDirection_OnSelectedIndexChanged");
        }
    }
    #endregion

    #region Notification
    private void UpdateNotification()
    {
        var db = new startetkuEntities1();
        try
        {
            if (!String.IsNullOrEmpty(Request.QueryString["notif"]))
            {
                var notId = Convert.ToInt32(Request.QueryString["notif"]);
                var notObj = db.Notifications.FirstOrDefault(o => o.notId == notId);
                if (notObj != null)
                {
                    notObj.notIsActive = false;
                    db.SaveChanges();
                }
            }

        }
        catch (Exception)
        {

            throw;
        }
    }
    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        // string language = "French";
        string language = Convert.ToString(Session["Culture"]);

        string languageId = "";

        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }

        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion
}
public partial class CatEle
{
    public int CatID;
    public List<EleIDs> EleIDs;
}

public partial class EleIDs
{
    public int EleID { get; set; }
}

