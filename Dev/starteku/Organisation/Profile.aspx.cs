﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using System.Threading;
using System.Globalization;
using starteku_BusinessLogic.Model;

public partial class Organisation_Profile : System.Web.UI.Page
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
         Page.ClientScript.RegisterStartupScript(Page.GetType(), "my", " SetExpandCollapse();", true);
        txtfromdate.Attributes.Add("readonly", "readonly");
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!String.IsNullOrEmpty(Request.QueryString["view"]))
        {
            edit.Visible = false;
        }
        else
        {
            view.Visible = false;
        }
        if (!IsPostBack)
        {
            Profile1.InnerHtml = GetLocalResourceObject("Welcome.Text").ToString() + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            var db = new startetkuEntities1();

            var resds = db.ResourceLanguages.Where(o => o.resIsActive == true).ToList();


            //ResourceLanguageBM obj = new ResourceLanguageBM();
            //DataSet resds = obj.GetAllResourceLanguage();
            ddllanguages.DataSource = resds;
            ddllanguages.DataTextField = "resLanguage";
            ddllanguages.DataValueField = "resCulture";
            ddllanguages.DataBind();
            GetAllCountry();
            state.Attributes.Add("style", "display:block");
            GetAllEmployeebyid();

        }
        

    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion

    #region method
    protected void SetDefaultMessage()
    {
        string id = "?id=";
        string childurl = "child.aspx";
        //if (Request.RawUrl.ToString().ToLower().Contains(childurl.ToLower()))
        //{
        //    if (Request.RawUrl.ToString().ToLower().Contains(id.ToLower()))
        //    {
        //        lblDataDisplayTitle.Text = "Edit Children";

        //    }
        //    else
        //    {
        //        lblDataDisplayTitle.Text = "Add Children";
        //    }
        //}
    }
    protected void GetAllCountry()
    {
        CountryBM obj = new CountryBM();
        obj.couIsActive = true;
        obj.couIsDeleted = false;
        obj.GetAllCountrymaster();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlcountry.Items.Clear();
                ddlcountry.DataSource = ds.Tables[0];
                ddlcountry.DataTextField = "couName";
                ddlcountry.DataValueField = "couId";
                ddlcountry.DataBind();
                ddlcountry.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
            }
            else
            {
                ddlcountry.Items.Clear();
                ddlcountry.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
            }
        }
        else
        {
            ddlcountry.Items.Clear();
            ddlcountry.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
        }


    }
    protected void GetState(int contryid)
    {
        state.Attributes.Add("style", "display:block");

        StateBM obj = new StateBM();
        obj.staCountryId = contryid;
        obj.staIsActive = true;
        obj.staIsDeleted = false;
        obj.GetStatebyCountryidMaster();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlstate.Items.Clear();
                ddlstate.DataSource = ds.Tables[0];
                ddlstate.DataTextField = "staName";
                ddlstate.DataValueField = "staId";
                ddlstate.DataBind();

                //  ddlstate.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
            }
            else
            {
                ddlstate.Items.Clear();
                ddlstate.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
            }
        }
        else
        {
            ddlstate.Items.Clear();
            ddlstate.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
        }
        // Getcity(Convert.ToInt32(ddlstate.SelectedValue));
    }
    //protected void Getcity(int stateid)
    //{
    //    city.Attributes.Add("style", "display:block");

    //    CityBM obj = new CityBM();
    //    obj.citStateId = stateid;
    //    obj.citIsActive = true;
    //    obj.citIsDeleted = false;
    //    obj.Getcitybystateid();
    //    DataSet ds = obj.ds;

    //    if (ds != null)
    //    {
    //        if (ds.Tables[0].Rows.Count > 0)
    //        {
    //            ddlcity.Items.Clear();
    //            ddlcity.DataSource = ds.Tables[0];
    //            ddlcity.DataTextField = "citName";
    //            ddlcity.DataValueField = "citId";
    //            ddlcity.DataBind();

    //            //  ddlstate.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
    //        }
    //        else
    //        {
    //            ddlcity.Items.Clear();
    //            ddlcity.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
    //        }
    //    }
    //    else
    //    {
    //        ddlcity.Items.Clear();
    //        ddlcity.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
    //    }

    //}
    protected void redirectpage(string msg, string paage)
    {
        //string message = "You will now be redirected to ASPSnippets Home Page.";
        string message = msg;
        //string url = "Login.aspx?msg=ins";
        string url = paage;
        string script = "window.onload = function(){ alert('";
        script += message;
        script += "');";
        script += "window.location = '";
        script += url;
        script += "'; }";
        ClientScript.RegisterStartupScript(this.GetType(), "Redirect", script, true);

    }
    protected void Insertuser()
    {
        string filePath = "";
        UserBM obj2 = new UserBM();
        obj2.useremailCheckDuplication(txtEmail.Text, -1);
        String returnMsg = obj2.ReturnString;
        if (returnMsg == "")
        {
            UserBM obj = new UserBM();
            obj.userFirstName = txtfName.Text;
            obj.userLastName = txtlname.Text;
            obj.userZip = txtzip.Text;
            obj.userAddress = txtaddress.Text;
            obj.userCountryId = Convert.ToInt32(ddlcountry.SelectedValue);
            obj.userStateId = Convert.ToInt32(ddlstate.SelectedValue);
            //obj.userCityId = Convert.ToInt32(ddlcity.SelectedValue);
            obj.userCityId = txtcity.Text;
            obj.usercontact = txtMobile.Text;
            obj.userEmail = txtEmail.Text;
            if (!String.IsNullOrEmpty(Request.QueryString["org"]))
            {
                obj.userType = 1;
            }
            else if (!String.IsNullOrEmpty(Request.QueryString["emp"]))
            {
                obj.userType = 2;
            }
            //obj.userCityId = Convert.ToInt32(ddlcity.SelectedValue);
            obj.userCreatedDate = DateTime.Now;
            obj.userIsActive = true;
            obj.userIsDeleted = false;
            if (!String.IsNullOrEmpty(Request.QueryString["org"]))
            {
                obj.userCompanyId = 0;
            }
            else if (!String.IsNullOrEmpty(Request.QueryString["emp"]))
            {
                obj.userCompanyId = 0;
            }
            obj.userPassword = CommonModule.encrypt(txtPassword.Text.Trim());
            obj.userDOB = Convert.ToDateTime(CommonModule.FormatdtEnter(txtfromdate.Text));
            obj.userGender = ddlgender.SelectedValue;
            if (flupload1.HasFile)
            {
                filePath = System.DateTime.Now.ToString("MMddyyhhmmss") + "_" + flupload1.PostedFile.FileName;
                flupload1.SaveAs(Server.MapPath("../") + "Log/upload/Userimage/" + System.IO.Path.GetFileName(filePath.Replace(' ', '0')));
                obj.userImage = System.IO.Path.GetFileName(filePath.Replace(' ', '0'));

            }
            else
            {
                if (ddlgender.SelectedValue == "Male")
                {
                    obj.userImage = "male.png";
                }
                else
                {//ViewState["userImage"]
                    obj.userImage = "female.png";
                }
            }
            obj.InsertUser();
            //  DataSet ds = obj.ds;
            if (obj.ReturnBoolean == true)
            {
                if (!String.IsNullOrEmpty(Request.QueryString["org"]))
                {
                    string msg = CommonModule.msgSuccessfullyRegistered;
                    redirectpage(msg, "Login.aspx");
                }

            }
        }
        else
        {
            if (returnMsg == "userEmail")
                lblMsg.Text = CommonModule.msgEmailAlreadyExists;
            else
                lblMsg.Text = CommonModule.msgProblemInsertRecord;
        }
    }
    protected void Updatelanguagemaster(int OrgUserId)
    {
        UserBM obj = new UserBM();
        obj.languageUserId = Convert.ToInt32(OrgUserId);
        String lag = Convert.ToString(ddllanguages.SelectedValue);
        //if (lag == "English")
        //{
        //    obj.languageName = "English";
        //    obj.languageCulture = "en-GB";
        //    Session["Culture"] = "English";
        //    setlanguage("English");
        //}
        //else
        //{
        //    obj.languageName = "Denish";
        //    obj.languageCulture = "da-DK";
        //    Session["Culture"] = "Denish";
        //    setlanguage("Denish");
        //}
        obj.languageName = ddllanguages.SelectedItem.Text;
        obj.languageCulture = ddllanguages.SelectedItem.Value;
        Session["Culture"] = ddllanguages.SelectedItem.Text;
        setlanguage(ddllanguages.SelectedItem.Text);
        SetLangId(Convert.ToString(ddllanguages.SelectedItem.Value));
        //obj.userGender = ddlgender.SelectedValue;
        obj.Updatelanguagemaster();

    }
    public void SetLangId(string culture)
    {
        var db = new startetkuEntities1();
        try
        {
            var getId = db.ResourceLanguages.FirstOrDefault(o => o.resCulture.ToLower() == culture);
            if (getId != null)
            {
                Session["resLangID"] = getId.resLangID;

            }

        }
        catch (Exception)
        {

           
        }
    }
    protected void UpdateUser(int OrgUserId)
    {
        string filePath = "";
        UserBM obj2 = new UserBM();
        obj2.useremailCheckDuplication(txtEmail.Text, OrgUserId);
        String returnMsg = obj2.ReturnString;
        if (returnMsg == "")
        {
            UserBM obj = new UserBM();
            obj.userId = Convert.ToInt32(OrgUserId);
            obj.userFirstName = txtfName.Text;
            obj.userLastName = txtlname.Text;
            obj.userZip = txtzip.Text;
            obj.userAddress = txtaddress.Text;
            obj.userCountryId = Convert.ToInt32(ddlcountry.SelectedValue);
            obj.userStateId = Convert.ToInt32(ddlstate.SelectedValue);
            //obj.userCityId = Convert.ToInt32(ddlcity.SelectedValue);
            obj.usercontact = txtMobile.Text;
            obj.userEmail = txtEmail.Text;
            obj.userCityId = txtcity.Text;
            obj.userUpdatedDate = DateTime.Now;
            obj.userPassword = CommonModule.encrypt(txtPassword.Text.Trim());
            if (txtfromdate.Text != "")
            {
               // obj.userDOB = Convert.ToDateTime(CommonModule.FormatdtEnter(txtfromdate.Text));
                //Saurin | added try cathc after found unexpected converttodatetime error for string. | 20150120 
                try
                {
                    //obj.userDOB = DateTime.ParseExact(txtfromdate.Text, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                    obj.userDOB = DateTime.Now;
                }
                catch (Exception ex)
                {
                    obj.userDOB = Convert.ToDateTime(txtfromdate.Text);
                    Common.WriteLog("error in UpdateUser (profile.aspx)" + "error message " + ex.Message + "error stack trace " + ex.StackTrace + " " + ex.Source + " inner exception" + Convert.ToString(ex.InnerException));
                }//Try catch not less then god!!!
                
            }

            obj.userGender = ddlgender.SelectedValue;
            if (flupload1.HasFile)
            {

                filePath = System.DateTime.Now.ToString("MMddyyhhmmss") + "_" + flupload1.PostedFile.FileName;
                flupload1.SaveAs(Server.MapPath("../") + "Log/upload/Userimage/" + System.IO.Path.GetFileName(filePath.Replace(' ', '0')));
                obj.userImage = System.IO.Path.GetFileName(filePath.Replace(' ', '0'));
                Session["OrgUserImage"] = obj.userImage;
            }
            else
            {
                
                obj.userImage = Convert.ToString(ViewState["userImage"]);

            }
            if (hdnIsRemoveImage.Value == "1")// user request to remove image on his profile.
            {
                obj.userImage = "ofile_img.png";
            }
            Session["OrgUserImage"] = obj.userImage;// Update user image.
            obj.userNumber = txtEmployeeNumber.Text;
            obj.UpdateUsernew();
            if (obj.ReturnBoolean == true)
            {
               

                
                if (Convert.ToString(Session["OrguserType"]) == "3")
                {
                    Response.Redirect("Employee_dashboard.aspx?user=saveSuccess");
                }
                else if ((Convert.ToString(Session["OrguserType"]) == "1"))
                {
                    Response.Redirect("Organisation-dashboard.aspx?user=saveSuccess");
                }
                else if ((Convert.ToString(Session["OrguserType"]) == "2"))
                {
                    Response.Redirect("Manager-dashboard.aspx?user=saveSuccess");
                }
            }
        }
        else
        {
            if (returnMsg == "userEmail")
                lblMsg.Text = CommonModule.msgEmailAlreadyExists;
            else
                lblMsg.Text = CommonModule.msgProblemInsertRecord;
        }
    }
    protected void GetAllEmployeebyid()
    {
        UserBM obj = new UserBM();
        if (!String.IsNullOrEmpty(Request.QueryString["view"]))
        {
            obj.userId = Convert.ToInt32(Request.QueryString["view"]);
        }
        else if (!string.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            obj.userId = Convert.ToInt32(Session["OrgUserId"]);
        }
        obj.GetAllEmployeebyid();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userFirstName"])))
                txtfName.Text = Convert.ToString(ds.Tables[0].Rows[0]["userFirstName"]);
            lblfname.Text = Convert.ToString(ds.Tables[0].Rows[0]["userFirstName"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userLastName"])))
                txtlname.Text = Convert.ToString(ds.Tables[0].Rows[0]["userLastName"]);
            lbllname.Text = Convert.ToString(ds.Tables[0].Rows[0]["userLastName"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userZip"])))
                txtzip.Text = Convert.ToString(ds.Tables[0].Rows[0]["userZip"]);
            lblzip.Text = Convert.ToString(ds.Tables[0].Rows[0]["userZip"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userAddress"])))
                txtaddress.Text = Convert.ToString(ds.Tables[0].Rows[0]["userAddress"]);
            lbladdress.Text = Convert.ToString(ds.Tables[0].Rows[0]["userAddress"]);


            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userCountryId"])))
                ddlcountry.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["userCountryId"]);
            lblcountry.Text = Convert.ToString(ddlcountry.SelectedItem);
            GetState(Convert.ToInt32(ddlcountry.SelectedValue));

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userStateId"])))
                ddlstate.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["userStateId"]);
            lblstate.Text = Convert.ToString(ddlstate.SelectedItem);

            //Getcity(Convert.ToInt32(ddlstate.SelectedValue));
            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userCityId"])))
                //ddlcity.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["userCityId"]);
                txtcity.Text = Convert.ToString(ds.Tables[0].Rows[0]["userCityId"]);
            lblcity.Text = Convert.ToString(ds.Tables[0].Rows[0]["userCityId"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["comphone"])))
                txtMobile.Text = Convert.ToString(ds.Tables[0].Rows[0]["comphone"]);
            lblphone.Text = Convert.ToString(ds.Tables[0].Rows[0]["comphone"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userEmail"])))
                txtEmail.Text = Convert.ToString(ds.Tables[0].Rows[0]["userEmail"]);
            lblemail.Text = Convert.ToString(ds.Tables[0].Rows[0]["userEmail"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userDOB"])))
                txtfromdate.Text = Convert.ToString(ds.Tables[0].Rows[0]["userDOB"]);
            lbldbo.Text = Convert.ToString(ds.Tables[0].Rows[0]["userDOB"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userPassword"])))
            {
                string Password = CommonModule.decrypt(Convert.ToString(ds.Tables[0].Rows[0]["userPassword"]));
                txtPassword.Attributes.Add("value", Password);
                txtConfirmPassword.Attributes.Add("value", Password);
            }

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userGender"])))
                ddlgender.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["userGender"]);

            fluploadText.Text = "No file chosen";

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userImage"])))
            {
                ViewState["userImage"] = Convert.ToString(ds.Tables[0].Rows[0]["userImage"]);
                fluploadText.Text = Convert.ToString(ds.Tables[0].Rows[0]["userImage"]);
            }

            img_profile.ImageUrl = "../Log/upload/Userimage/" + Convert.ToString(ds.Tables[0].Rows[0]["userImage"]) + "";
            Image1.ImageUrl = "../Log/upload/Userimage/" + Convert.ToString(ds.Tables[0].Rows[0]["userImage"]) + "";

            try
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["languageName"])))
                    ddllanguages.Items.FindByText(Convert.ToString(ds.Tables[0].Rows[0]["languageName"])).Selected = true;
                //ddllanguages.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["languageName"]);
                lblLanguage.Text = Convert.ToString(ds.Tables[0].Rows[0]["languageName"]);
            }
            catch (Exception ex) { }
               if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userNumber"])))
                   txtEmployeeNumber.Text = Convert.ToString(ds.Tables[0].Rows[0]["userNumber"]);
               lblempnumber.Text = Convert.ToString(ds.Tables[0].Rows[0]["userNumber"]);


        }

    }
   
    protected void setlanguage(String name)
    {
        HttpCookie cookie = new HttpCookie("setCulturee");
        cookie.Values.Add("languagename", name);
        cookie.Expires = DateTime.Now.AddDays(30);
        Response.Cookies.Add(cookie);
    }
    #endregion

    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {

        int OrgUserId = Convert.ToInt32(Session["OrgUserId"]);
        if (OrgUserId != 0)
        {
            Session["OrgUserName"] = txtfName.Text;
            Updatelanguagemaster(OrgUserId);
            UpdateUser(OrgUserId);
            
        }

    }
    protected void btnCancel_click(object sender, EventArgs e)
    {
        // Response.Redirect("Login.aspx");
        if (Convert.ToString(Session["OrguserType"]) == "3")
        {
            Response.Redirect("Employee_dashboard.aspx");
        }
        else if ((Convert.ToString(Session["OrguserType"]) == "1"))
        {
            Response.Redirect("Organisation-dashboard.aspx");
        }
        else if ((Convert.ToString(Session["OrguserType"]) == "2"))
        {
            Response.Redirect("Manager-dashboard.aspx");
        }
    }
    protected void btnback_click(object sender, EventArgs e)
    {
        if (Convert.ToString(Session["OrguserType"]) == "3")
        {
            Response.Redirect("contact-colleagus.aspx");
        }

        else if ((Convert.ToString(Session["OrguserType"]) == "2"))
        {
            Response.Redirect("ContactList.aspx");
        }
    }
    #endregion

    #region SelectedIndexChanged
    protected void ddlcountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetState(Convert.ToInt32(ddlcountry.SelectedValue));
    }
    protected void ddlstate_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Getcity(Convert.ToInt32(ddlstate.SelectedValue));
    }
    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        //string language = "French";
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Denish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion

    protected void btnRemoveImg_Click(object sender, EventArgs e)
    {
        hdnIsRemoveImage.Value = "1";
        ViewState["userImage"] = Convert.ToString("../Log/upload/Userimage/ofile_img.png");
        img_profile.ImageUrl = "../Log/upload/Userimage/ofile_img.png";
        Image1.ImageUrl = "../Log/upload/Userimage/ofile_img.png";
    }
}