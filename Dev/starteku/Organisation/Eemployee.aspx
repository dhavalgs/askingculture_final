﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true" CodeFile="Eemployee.aspx.cs" Inherits="Organisation_Eemployee"
    Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .inline-form input, .inline-form textarea {
            font-size: 15px;
        }

        #ContentPlaceHolder1_chkList input {
            width: 33px;
            margin-bottom: 0px !important;
        }

        #ContentPlaceHolder1_chkList label {
            margin-top: 2px;
        }

        .scroll_checkboxes {
            height: 120px; /*width: 100%;*/
            padding: 5px;
            overflow: auto;
            border: 1px solid #ccc;
        }

        .contentsss {
            height: 100%;
        }

        #accordion-resizer {
            padding: 10px;
            width: 350px;
            height: 220px;
        }
    </style>

    <link href="../Styles/UpdateCustom.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/sMain.js?version=1.0" type="text/javascript"></script>
    <%--  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css"/>
        <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script type="text/javascript" src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            accordian();
            setTimeout(function () {
                ExampanCollapsMenu();
                // tabMenuClick();
                $(".add_employee").addClass("active_page");
            }, 500);
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:HiddenField ID="hdnSelectGender" runat="server" Value="Select Gender" meta:resourcekey="SelectGenderResource" />
    <asp:HiddenField runat="server" ID="hdnteam" Value="Select Team" meta:resourcekey="selectteamres"/>
    <asp:HiddenField runat="server" ID="hdnjobtype" Value="Select JobType" meta:resourcekey="selectjobtyperes"/>
    <asp:HiddenField runat="server" ID="hdndivision" Value="Select division" meta:resourcekey="selectdivisionres"/>

    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" meta:resourcekey="lblMsgResource1"></asp:Label>
    <asp:Label runat="server" ID="Label1" CssClass="commonerrormsg" meta:resourcekey="Label1Resource1"></asp:Label>
    <div class="col-md-6">
        <div class="heading-sec">
            <h1>
                <%--   <%= CommonMessages.Employee%>--%>
                <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Employee" EnableViewState="false" />
                <i><span runat="server" id="Eemployee"></span></i>
            </h1>
        </div>
    </div>
    <br />
    <br />
    <div class="col-md-6 range ">
    </div>
    <div class="col-md-12">
        <div class="chat-widget widget-body" style="background: #fff;">
            <asp:Label runat="server" ID="lblDataDisplayTitle" meta:resourcekey="lblDataDisplayTitleResource1"></asp:Label>
            <div class="chat-widget-head yellow yellow-radius" style="height: 60px;">
                <h4>
                    <%--   <%= CommonMessages.EditEemployeeInformation%>--%>
                    <asp:Literal ID="Literal2" runat="server" meta:resourcekey="EditEemployeeInformation" EnableViewState="false" />
                </h4>
            </div>
            <div class="indicatesRequireFiled">
                <i>*
                   <%-- <%= CommonMessages.Indicatesrequiredfield%>--%>
                    <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Indicatesrequiredfield" EnableViewState="false" />
                </i>
            </div>
            <div id="div1" runat="server" class="otherDiv">
                <div class="col-md-6">
                    <div class="inline-form">
                        <label class="c-label">
                            <%-- <%= CommonMessages.Name%>  --%>
                            <asp:Literal ID="Literal4" runat="server" meta:resourcekey="Name" EnableViewState="false" />:</label>
                        <asp:TextBox runat="server" ID="txtName" MaxLength="50" meta:resourcekey="txtNameResource1" /><br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName"
                            ErrorMessage="Please enter Name." CssClass="commonerrormsg" Display="Dynamic"
                            ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator5Resource1"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="inline-form">
                        <label class="c-label">
                            <%-- <%= CommonMessages.Name%>  --%>
                            <asp:Literal ID="Literal11" runat="server" meta:resourcekey="LastName" EnableViewState="false" />:</label>
                        <asp:TextBox runat="server" ID="txtlastName" MaxLength="50" meta:resourcekey="txtNameResource2" /><br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtName"
                            ErrorMessage="Please enter Name." CssClass="commonerrormsg" Display="Dynamic"
                            ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator5Resource1"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="inline-form">
                        <label class="c-label">
                            <%--  <%= CommonMessages.Address%>--%>
                            <asp:Literal ID="Literal5" runat="server" meta:resourcekey="Address" EnableViewState="false" /></label>
                        <asp:TextBox runat="server" ID="txtaddress" Rows="3" TextMode="MultiLine"
                            meta:resourcekey="txtaddressResource1" /><br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtaddress"
                            ErrorMessage="Please enter Address." CssClass="commonerrormsg" Display="Dynamic"
                            ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator1Resource1"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="inline-form">
                        <label class="c-label">
                            <asp:Label runat="server" meta:resourcekey="emailres"></asp:Label></label>
                        <asp:TextBox runat="server" ID="txtEmail" MaxLength="50" meta:resourcekey="txtEmailResource1" /><br />
                        <asp:RequiredFieldValidator ID="rfvEmail" runat="server" CssClass="commonerrormsg"
                            ControlToValidate="txtEmail" Display="Dynamic" ValidationGroup="chk" ErrorMessage="Please enter email."
                            meta:resourcekey="revEmailResource1"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revEmail" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                            ControlToValidate="txtEmail" CssClass="commonerrormsg" Display="Dynamic" ValidationGroup="chk"
                            ErrorMessage="Please enter valid email." meta:resourcekey="revEmailResource1"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="inline-form">
                        <label class="c-label">
                            <%-- <%= CommonMessages.PhoneNo%> --%><asp:Literal ID="Literal6" runat="server" meta:resourcekey="PhoneNo" EnableViewState="false" />:</label>
                        <asp:TextBox runat="server" ID="txtMobile" MaxLength="15"
                            meta:resourcekey="txtMobileResource1" /><br />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtMobile"
                            ErrorMessage="Minimum Phone No length is 10." Display="Dynamic" CssClass="commonerrormsg"
                            ValidationGroup="chk" ValidationExpression=".{10}.*" meta:resourcekey="RegularExpressionValidator6Resource1" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtMobile"
                            ErrorMessage="Please enter Phone No." CssClass="commonerrormsg" Display="Dynamic"
                            ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator6Resource1"></asp:RequiredFieldValidator>
                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Custom, Numbers"
                            ValidChars=" ,+,(,),-" TargetControlID="txtMobile" Enabled="True">
                        </cc1:FilteredTextBoxExtender>
                    </div>
                </div>
            </div>
            <asp:UpdatePanel runat="server" ID="UpdatePanedrop" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="col-md-6 otherDiv">
                        <div class="inline-form">
                            <label class="c-label" style="width: 100%;">
                                <asp:Label runat="server" meta:resourcekey="divisionres"></asp:Label></label>
                            <asp:DropDownList ID="ddlDepartment" runat="server" Style="width: 100%; height: 32px; display: inline; border: 1px solid #d4d4d4;"
                                meta:resourcekey="ddlDepartmentResource1"
                                OnSelectedIndexChanged="ddlcompetence_SelectedIndexChanged" AutoPostBack="true" class="form-control">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorddlDepartment1" runat="server" ControlToValidate="ddlDepartment"
                                ErrorMessage="Please select Division." InitialValue="0" CssClass="commonerrormsg"
                                Display="Dynamic" ValidationGroup="chk" meta:resourcekey="RequiredFieldValidatorddlDepartmentResource1"></asp:RequiredFieldValidator>
                            
                          

                            
                        </div>
                    </div>
                    <div class="col-md-6 otherDiv">
                        <div class="inline-form">
                            <label class="c-label" style="width: 100%;">
                                <asp:Label runat="server" meta:resourcekey="jobtyperes"></asp:Label></label>
                            <asp:DropDownList ID="ddljobtype" runat="server" Style="width: 100%; height: 32px; display: inline; border: 1px solid #d4d4d4;"
                                meta:resourcekey="ddljobtypeResource1"
                                OnSelectedIndexChanged="ddljobtype_SelectedIndexChanged" AutoPostBack="true" class="form-control">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorddljobtype" runat="server" ControlToValidate="ddljobtype"
                                ErrorMessage="Please select job type." InitialValue="0" CssClass="commonerrormsg"
                                Display="Dynamic" ValidationGroup="chk" meta:resourcekey="RequiredFieldValidatorddljobtypeResource1">
                            </asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="col-md-6 otherDiv">
                        <div class="inline-form">
                            <label class="c-label" style="width: 100%;">
                                <asp:Label runat="server" meta:resourcekey="teamres"></asp:Label></label>
                            <asp:DropDownList ID="ddlTeam" runat="server" Style="width: 100%; height: 32px; display: inline; border: 1px solid #d4d4d4;"
                                meta:resourcekey="ddlTeamResource1"
                                class="form-control">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorddlTeam" runat="server" ControlToValidate="ddlTeam"
                                ErrorMessage="Please select Team." InitialValue="0" CssClass="commonerrormsg"
                                Display="Dynamic" ValidationGroup="chk" meta:resourcekey="RequiredFieldValidatorddlTeameResource1">
                            </asp:RequiredFieldValidator>

                        </div>

                    </div>
                    <div class="col-md-6 otherDiv">
                        <div class="inline-form">
                            <label class="c-label" style="width: 100%;">
                                <asp:Label runat="server" meta:resourcekey="genderres"></asp:Label></label>
                            <asp:DropDownList ID="ddlgender" runat="server" CssClass="form-control" Style="width: 100%; height: 36px; border: 1px solid #d4d4d4; margin-bottom: 20px; display: inline;"
                                meta:resourcekey="ddlgenderResource1">
                                <asp:ListItem Value="Male" Text="Male" meta:resourcekey="ListItemResource1"></asp:ListItem>
                                <asp:ListItem Value="Female" Text="Female" meta:resourcekey="ListItemResource2"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorddlgender" runat="server" ControlToValidate="ddlgender"
                                ErrorMessage="Please select gender." InitialValue="0" CssClass="commonerrormsg"
                                Display="Dynamic" ValidationGroup="chk" meta:resourcekey="selectgenderres"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div id="div2" runat="server" class="otherDiv">
                        <div class="col-md-6">
                            <div class="inline-form">
                                <label class="c-label" style="width: 100%;">
                                    <%-- <%= CommonMessages.Name%>--%>
                                    <asp:Literal ID="Literal7" runat="server" meta:resourcekey="Name" EnableViewState="false" />:</label>
                                <asp:Label class="c-label" ID="lblName" runat="server" meta:resourcekey="lblNameResource1"></asp:Label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="inline-form">
                                <label class="c-label" style="width: 100%;">
                                    <%-- <%= CommonMessages.Name%>--%>
                                    <asp:Literal ID="Literal12" runat="server" meta:resourcekey="LastName" EnableViewState="false" />:</label>
                                <asp:Label class="c-label" ID="lblLastName" runat="server" meta:resourcekey="lblNameResource1"></asp:Label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="inline-form">
                                <label class="c-label" style="width: 100%;">
                                    <%-- <%= CommonMessages.Address%>--%>
                                    <asp:Literal ID="Literal8" runat="server" meta:resourcekey="Address" EnableViewState="false" />:</label>
                                <asp:Label class="c-label" ID="lblAddress" runat="server" meta:resourcekey="lblAddressResource1"></asp:Label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="inline-form">
                                <label class="c-label" style="width: 100%;">
                                    Email:</label>
                                <asp:Label class="c-label" ID="lblEmail" runat="server" meta:resourcekey="lblEmailResource1"></asp:Label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="inline-form">
                                <label class="c-label" style="width: 100%;">
                                    <%-- <%= CommonMessages.PhoneNo%> --%><asp:Literal ID="Literal9" runat="server" meta:resourcekey="PhoneNo" EnableViewState="false" />:</label>
                                <asp:Label class="c-label" ID="lblPhoneNo" runat="server" meta:resourcekey="lblPhoneNoResource1"></asp:Label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="inline-form">
                                <label class="c-label" style="width: 100%;">
                                    Departments:</label>
                                <asp:Label class="c-label" ID="lblDepartment" runat="server" meta:resourcekey="lblDepartmentResource1"></asp:Label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="inline-form">
                                <label class="c-label" style="width: 100%;">
                                    Job type:</label>
                                <asp:Label class="c-label" ID="lbljob" runat="server" meta:resourcekey="lbljobResource1"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <%-------------------------------Checkboxlist--------------------------------------%>
                    <div class="scroll_checkboxes" style="display: none; height: 400px; overflow: auto;" id="CompetenceCheckboxes">
                        <label class="c-label" style="width: 100%;">
                            <%--<%= CommonMessages.Competence%>--%><asp:Literal ID="Literal10" runat="server" meta:resourcekey="Competence" EnableViewState="false" />:</label><br />
                        <br />
                        <br />


                        <div id="accordion">
                            <div id="NoRecords" runat="server" visible="false">
                                None.
                            </div>
                            <asp:Repeater ID="repCompCat" runat="server" OnItemDataBound="Repeater1_ItemDataBound">
                                <HeaderTemplate>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div>

                                        <asp:Label runat="server" ID="lblDivIdName" Visible="false"><%# DataBinder.Eval(Container.DataItem, "catName")%> </asp:Label>
                                        <asp:Label runat="server" ID="lblDivIdName1" Visible="false"><%# DataBinder.Eval(Container.DataItem, "catNameDN")%> </asp:Label>
                                        <asp:Label runat="server" ID="lblCompCategoryName"> </asp:Label>
                                    </div>
                                    <div>
                                        <asp:Repeater ID="repCompAdd" runat="server" OnItemDataBound="Repeater2_ItemDataBound">
                                            <HeaderTemplate>
                                                <table>
                                                    <%-- <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                            </tr>
                                        </thead>--%>
                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>


                                                    <td>
                                                        <asp:CheckBoxList Width="100%" ID="chkList" runat="server" CssClass=" chkliststyle"
                                                            RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description"
                                                            DataValueField="value" Style="width: 100%; overflow: auto;">
                                                        </asp:CheckBoxList>
                                                        <asp:Label runat="server" ID="hdnChkboxValue" Text='<%# DataBinder.Eval(Container.DataItem, "comId")%>' Style="display: none"></asp:Label>
                                                    </td>
                                                    <%--<asp:CustomValidator ID="CustomValidator1" ErrorMessage="Please select at least one item."
                                    ForeColor="Red" runat="server"
                                    ValidationGroup="chk" />
                                                    --%>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </tbody>
                                    </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </ItemTemplate>
                                <FooterTemplate>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                        <%-- <div id="accordion">
                           <h3><asp:Label runat="server" ID="lblDivIdName"></asp:Label></h3>
                               <div class="contentss">
                                   
                                          <asp:CheckBoxList Width="100%" ID="chkList" runat="server" CssClass="FormText scroll_checkboxes"
                                    RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description"
                                    DataValueField="value" Style="width: 100%; overflow: auto; border: 1px solid #d4d4d4;">
                                </asp:CheckBoxList>
                                <asp:CustomValidator ID="CustomValidator1" ErrorMessage="Please select at least one item."
                                    ForeColor="Red" runat="server"
                                    ValidationGroup="chk" />
                              </div>
                           
                            <h3><asp:Label runat="server" ID="lblJobtype"></asp:Label></h3>
                               
                                  <div  class="contentss">
                                             <asp:CheckBoxList Width="100%" ID="CheckBoxList1" runat="server" CssClass="FormText scroll_checkboxes"
                                        RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description"
                                        DataValueField="value" Style="width: 100%; overflow: auto; border: 1px solid #d4d4d4;">
                                    </asp:CheckBoxList>
                                    <asp:CustomValidator ID="CustomValidator2" ErrorMessage="Please select at least one item."
                                        ForeColor="Red" runat="server"
                                        ValidationGroup="chk" />
                                   </div>
                               
                       
                        </div>--%>



                        <div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <%------------------------------NEXT Button------------------------------%>
            <div class="modal-footer" id="nextButtons">
                <%-- <asp:Button runat="server" ID="btnNext" Text="Next" CssClass="btn btn-primary yellow"
                        ValidationGroup="chk" OnClientClick="ShowCompotenceCategory(true);" Style="border-radius: 5px;" />--%>
                <input id="btnNext" type="button" onclick="CallCompetence();" value="Next" class="btn btn-primary yellow"
                    style="border-radius: 5px;" validationgroup="chk" runat="server" />
                <asp:Button runat="server" ID="Button1" Text="Cancel" CssClass="btn btn-primary black" OnClick="btnCancel_OnClick"
                    CausesValidation="False" meta:resourcekey="btnCancelResource1" />

                <%-- <button id="btnxxx" type="button" class="btns  yellow  lrg-btn flat-btn add_user" style="border: 0px;display:none">
                        <%= CommonMessages.AddNewEmployee%></button>--%>
            </div>

            <%------------------------------Save Button------------------------------%>
            <div class="modal-footer" id="submitButtons" style="display: none">
                <input id="Button2" type="button" onclick="ShowCompotenceCategory(false);" value="Back"
                    class="btn btn-primary yellow" style="border-radius: 5px;" validationgroup="chk" runat="server" />
                <asp:Button runat="server" ID="btnsubmit" Text="Save" CssClass="btn btn-primary yellow"
                    ValidationGroup="chk" OnClick="btnsubmit_click" meta:resourcekey="btnsubmitResource1" />
                <asp:Button runat="server" ID="btnCancel" Text="Cancel" CssClass="btn btn-primary black"
                    CausesValidation="False" meta:resourcekey="btnCancelResource1" OnClick="btnCancel_OnClick" />
            </div>
        </div>
    </div>
    <%--<script type="text/javascript">
         $(document).ready(function () {
             $("#btnNext").val('<%= CommonMessages.Next%>');
             $("#Button2").val('<%= CommonMessages.Back%>');

            
         });
            </script>--%>

    <script type="text/javascript">
        function CallCompetence() {
           
            if ($('#ContentPlaceHolder1_txtName').val() == '' || $('#ContentPlaceHolder1_txtlastName').val() == ''
                || $('#ContentPlaceHolder1_txtaddress').val() == '' || $('#ContentPlaceHolder1_txtEmail').val() == '' || $('#ContentPlaceHolder1_ddlgender')[0].selectedIndex == 0
                || $('#ContentPlaceHolder1_txtMobile').val() == ''
                || $('#ContentPlaceHolder1_ddlDepartment')[0].selectedIndex == 0
                || $('#ContentPlaceHolder1_ddljobtype')[0].selectedIndex == 0
                || $('#ContentPlaceHolder1_ddlTeam')[0].selectedIndex == 0) {

               
                ValidatorEnable($("[id$='RequiredFieldValidator1']")[0], true);
                ValidatorEnable($("[id$='RequiredFieldValidator2']")[0], true);
                ValidatorEnable($("[id$='RequiredFieldValidator3']")[0], true);
                ValidatorEnable($("[id$='RequiredFieldValidator4']")[0], true);
                ValidatorEnable($("[id$='RequiredFieldValidatorddlDepartment1']")[0], true);
                ValidatorEnable($("[id$='RequiredFieldValidatorddljobtype']")[0], true);
              
                ValidatorEnable($("[id$='RequiredFieldValidatorddlTeam']")[0], true);
                ValidatorEnable($("[id$='RequiredFieldValidatorddlgender']")[0], true);
                ValidatorEnable($("[id$='rfvEmail']")[0], true);
                ValidatorEnable($("[id$='revEmail']")[0], true);
            }
            else {
                ShowCompotenceCategory(true);
            }
        }
    </script>
</asp:Content>
