﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using System.Configuration;
using startetku.Business.Logic;
using System.Web.UI.HtmlControls;
using System.Threading;
using System.Globalization;

public partial class Organisation_OrgInvitation_request : System.Web.UI.Page
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }


        if (!IsPostBack)
        {
            ViewState["PreviousPage"] = Request.UrlReferrer;
            Employee.InnerHtml = GetLocalResourceObject("Welcome.Text").ToString() + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            //sp1.InnerHtml = CommonMessages.Competence;
            sp1.InnerHtml = "Notification Center";
            GetAllInvitionRequest();
            GetHelpRequestedByUserId();
            SetDefaultMessage();
            GetHelpReplyForUserId();
            GetAllNotification();
            GetHelpProvidedByOtherUser();
            GetHelpSuggestedToOther();
            var notId = Request.QueryString["notIf"];
            if (!string.IsNullOrWhiteSpace(notId))
            {
                NotificationBM obj = new NotificationBM();
                obj.notId = Convert.ToInt32(notId);
                obj.notPopUpStatus = false;
                obj.UpdateNotificationbyid();
            }

            if (Request.QueryString["status"] == "Accepted")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>showHideDiv('SuggestedHelpdiv');</script>", false);
            }
            else if (Request.QueryString["status"] == "Invitation")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>showHideDiv('SendHelpDiv');</script>", false);
            }
            else if (Request.QueryString["status"] == "Invite")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>showHideDiv('ReqestDiv');</script>", false);
            }
            else if (Request.QueryString["status"] == "ProHelp")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>showHideDiv('ProvidedHelpbyOtherdiv');</script>", false);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'>showHideDiv('All');</script>", false);
            }
        }


        //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "onLoad();", false);// btn.Visible = false;
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion

    #region method
    protected void SetDefaultMessage()
    {
        // lblMsg.Text = GetLocalResourceObject("lblHelloWorld.Text").ToString();

        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["msg"])))
        {
            if (Convert.ToString(Request.QueryString["msg"]) == "del")
            {
                //lblMsg.Text = CommonModule.msgRecordHasBeenDeletedSuccessfully;
                //lblMsg.Text = CommonMessages.msgRecordHasBeenDeletedSuccessfully;
                lblMsg.Text = GetLocalResourceObject("msgRecordHasBeenDeletedSuccessfully.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "ins")
            {
                //lblMsg.Text = CommonMessages.msgRecordInsertedSuccessfully;
                lblMsg.Text = GetLocalResourceObject("msgRecordInsertedSuccessfully.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "upd")
            {
                //lblMsg.Text = CommonMessages.msgRecordUpdatedSuccss;
                lblMsg.Text = GetLocalResourceObject("msgRecordUpdatedSuccss.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "arc")
            {
                // lblMsg.Text = CommonMessages.msgRecordArchiveSucces;
                lblMsg.Text = GetLocalResourceObject("CommonMessages.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
        }

    }
    protected void GetAllInvitionRequest()
    {
        InvitationBM obj = new InvitationBM();
        obj.invIsActive = true;
        obj.invIsDeleted = false;
        obj.invToUserId = Convert.ToInt32(Session["OrgUserId"]);
        obj.invCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllInvitionRequest();

        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {


                gvGrid.DataSource = ds.Tables[0];
                gvGrid.DataBind();
                gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;

                /*  gvRequestData.DataSource = ds.Tables[0];
                  gvRequestData.DataBind();
                  gvRequestData.HeaderRow.TableSection = TableRowSection.TableHeader;*/
            }
            else
            {
                gvGrid.DataSource = null;
                gvGrid.DataBind();
            }
        }
        else
        {
            gvGrid.DataSource = null;
            gvGrid.DataBind();
        }
    }

    protected void GetHelpReplyForUserId()
    {
        InvitationBM obj = new InvitationBM();
        obj.invIsActive = true;
        obj.invIsDeleted = false;
        obj.invFromUserId = Convert.ToInt32(Session["OrgUserId"]);
        obj.invCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetHelpReplyForUserId();

        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {


                grdViewHelpList.DataSource = ds.Tables[0];
                grdViewHelpList.DataBind();
                grdViewHelpList.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                grdViewHelpList.DataSource = null;
                grdViewHelpList.DataBind();
            }
        }
        else
        {
            grdViewHelpList.DataSource = null;
            grdViewHelpList.DataBind();
        }
    }

    protected void GetHelpRequestedByUserId()
    {
        InvitationBM obj = new InvitationBM();
        obj.invIsActive = true;
        obj.invIsDeleted = false;
        obj.invFromUserId = Convert.ToInt32(Session["OrgUserId"]);
        obj.invCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetHelpRequestedByUserId();

        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {


                gvRequestData.DataSource = ds.Tables[0];
                gvRequestData.DataBind();
                gvRequestData.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvRequestData.DataSource = null;
                gvRequestData.DataBind();
            }
        }
        else
        {
            gvRequestData.DataSource = null;
            gvRequestData.DataBind();
        }
    }

    protected void GetAllNotification()
    {
        try
        {
            NotificationBM obj = new NotificationBM();
            obj.notIsActive = true;
            obj.notIsDeleted = false;
            obj.userCreateBy = Convert.ToString(Session["OrgUserId"]);
            obj.GetAllNotificationFULL();
            DataSet ds = obj.ds;
            var table = ds.Tables[0];

            //foreach (DataRow dr in table.Rows)
            //{
            //    dr["MinAgo"] = dr["MinAgo"].ToString().Replace("Second", GetLocalResourceObject("Second.Text").ToString());
            //    dr["MinAgo"] = dr["MinAgo"].ToString().Replace("Minutes", GetLocalResourceObject("Minutes.Text").ToString());
            //    dr["MinAgo"] = dr["MinAgo"].ToString().Replace("Hours", GetLocalResourceObject("Hours.Text").ToString());
            //    dr["MinAgo"] = dr["MinAgo"].ToString().Replace("Days", GetLocalResourceObject("Days.Text").ToString());

            //}

            ViewState["data"] = ds;

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    grdNotification.DataSource = ds;

                    grdNotification.DataBind();


                }
                else
                {
                    grdNotification.DataSource = null;
                    grdNotification.DataBind();

                }

            }
            else
            {


                grdNotification.DataSource = null;
                grdNotification.DataBind();
            }
        }
        catch (Exception ex)
        {
            Common.WriteLog("error in GetAllNotification" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));

        }
    }
    protected void grdNotification_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //Checking the RowType of the Row  
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            try
            {

                Label test = (Label)e.Row.FindControl("lblMinAgoText");

                if ((test.Text == "Days"))
                {
                    ((Label)e.Row.FindControl("lblMinAgoText")).Text = GetLocalResourceObject("lblDays.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }

                if ((test.Text == "Hours"))
                {
                    ((Label)e.Row.FindControl("lblMinAgoText")).Text = GetLocalResourceObject("Hours.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }

                if ((test.Text == "Minutes"))
                {
                    ((Label)e.Row.FindControl("lblMinAgoText")).Text = GetLocalResourceObject("Minutes.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }
                if ((test.Text == "Second"))
                {
                    ((Label)e.Row.FindControl("lblMinAgoText")).Text = GetLocalResourceObject("Second.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }
            }
            catch (Exception ex)
            {
                if ((((Label)(e.Row.FindControl("lblMinAgoText"))).Text == "Days"))
                {
                    ((Label)e.Row.FindControl("lblMinAgoText")).Text = GetLocalResourceObject("lblDays.Text").ToString();
                }
            }

            try
            {
                Label test1 = (Label)e.Row.FindControl("lblUserNamer140");

                if ((test1.Text == "pdpReq"))
                {
                    ((Label)e.Row.FindControl("lblUserNamer140")).Text = GetLocalResourceObject("pdpReq.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }

                if ((test1.Text == "Message"))
                {
                    ((Label)e.Row.FindControl("lblUserNamer140")).Text = GetLocalResourceObject("Message.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }

                if ((test1.Text == "Notificati"))
                {
                    ((Label)e.Row.FindControl("lblUserNamer140")).Text = GetLocalResourceObject("Notificati.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }

                if ((test1.Text == "ActReq"))
                {
                    ((Label)e.Row.FindControl("lblUserNamer140")).Text = GetLocalResourceObject("ActReq.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }
                if ((test1.Text == "UserAccept"))
                {
                    ((Label)e.Row.FindControl("lblUserNamer140")).Text = GetLocalResourceObject("UserAccept.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }
                if ((test1.Text == "Invitation"))
                {
                    ((Label)e.Row.FindControl("lblUserNamer140")).Text = GetLocalResourceObject("Invitation.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }
                if ((test1.Text == "Accepted"))
                {
                    ((Label)e.Row.FindControl("lblUserNamer140")).Text = GetLocalResourceObject("Accepted.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }
                if ((test1.Text == "Invite"))
                {
                    ((Label)e.Row.FindControl("lblUserNamer140")).Text = GetLocalResourceObject("Invite.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }
                if ((test1.Text == "ProHelp"))
                {
                    ((Label)e.Row.FindControl("lblUserNamer140")).Text = GetLocalResourceObject("ProHelp.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }

            }
            catch (Exception ex)
            {
                if ((((Label)(e.Row.FindControl("lblMinAgoText"))).Text == "Days"))
                {
                    ((Label)e.Row.FindControl("lblMinAgoText")).Text = GetLocalResourceObject("lblDays.Text").ToString();
                }
            }

            try
            {
                Label test2 = (Label)e.Row.FindControl("lblUNamer5");

                if ((test2.Text == "sent you invitation for needs help"))
                {
                    ((Label)e.Row.FindControl("lblUNamer5")).Text = GetLocalResourceObject("sentneedhelp.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }

                if ((test2.Text == "Has Set Reqest."))
                {
                    ((Label)e.Row.FindControl("lblUNamer5")).Text = GetLocalResourceObject("setreq.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }
                if ((test2.Text == "Has Completed Activity"))
                {
                    ((Label)e.Row.FindControl("lblUNamer5")).Text = GetLocalResourceObject("HasCompletedAct.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }
                if ((test2.Text == "Has Accepted Competence."))
                {
                    ((Label)e.Row.FindControl("lblUNamer5")).Text = GetLocalResourceObject("acccom.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }

                if ((test2.Text == "Has Set Competence."))
                {
                    ((Label)e.Row.FindControl("lblUNamer5")).Text = GetLocalResourceObject("setcompetence.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }
                if ((test2.Text == "PDP Approve"))
                {
                    ((Label)e.Row.FindControl("lblUNamer5")).Text = GetLocalResourceObject("pdpapprove.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }



            }
            catch (Exception ex)
            {
                if ((((Label)(e.Row.FindControl("lblMinAgoText"))).Text == "Days"))
                {
                    ((Label)e.Row.FindControl("lblMinAgoText")).Text = GetLocalResourceObject("lblDays.Text").ToString();
                }
            }


        }
    }




    protected void GetHelpProvidedByOtherUser()
    {
        InvitationBM obj = new InvitationBM();
        obj.invIsActive = true;
        obj.invIsDeleted = false;
        obj.invToUserId = Convert.ToInt32(Session["OrgUserId"]);
        obj.invCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetHelpProvidedByOtherUser();

        DataSet ds = obj.ds;

        // var table = ds.Tables[0];
        //ViewState["data"] = ds;
        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {


                GridView1.DataSource = ds.Tables[0];
                GridView1.DataBind();
                GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                GridView1.DataSource = null;
                GridView1.DataBind();
            }
        }
        else
        {
            GridView1.DataSource = null;
            GridView1.DataBind();
        }
    }

    protected void GetHelpSuggestedToOther()
    {
        InvitationBM obj = new InvitationBM();
        obj.invIsActive = true;
        obj.invIsDeleted = false;
        obj.invFromUserId = Convert.ToInt32(Session["OrgUserId"]);
        obj.invCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetHelpSuggestedToOther();

        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {


                GridView2.DataSource = ds.Tables[0];
                GridView2.DataBind();
                GridView2.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                GridView2.DataSource = null;
                GridView2.DataBind();
            }
        }
        else
        {
            GridView2.DataSource = null;
            GridView2.DataBind();
        }
    }


    #endregion

    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {

    }

    #endregion

    #region grid Event
    protected void gvGrid_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "archive")
        {
            InvitationBM obj = new InvitationBM();
            obj.invId = Convert.ToInt32(e.CommandArgument);
            obj.invIsActive = false;
            obj.invIsDeleted = false;
            obj.InvitionStatusUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    GetAllInvitionRequest();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }

    }

    protected void gvGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {


            try
            {

                Label test1 = (Label)e.Row.FindControl("lblUserNamer19");

                if ((test1.Text == "Accepted"))
                {
                    ((Label)e.Row.FindControl("lblUserNamer19")).Text = GetLocalResourceObject("Accepted.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }

                if ((test1.Text == "Cancel"))
                {
                    ((Label)e.Row.FindControl("lblUserNamer19")).Text = GetLocalResourceObject("Cancel.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }

                if ((test1.Text == "Pending"))
                {
                    ((Label)e.Row.FindControl("lblUserNamer19")).Text = GetLocalResourceObject("pending.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            try
            {

                Label test1 = (Label)e.Row.FindControl("lblUserNamer121");

                if ((test1.Text == "Need Help"))
                {
                    ((Label)e.Row.FindControl("lblUserNamer121")).Text = GetLocalResourceObject("needhelp.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }

                if ((test1.Text == "ProHelp"))
                {
                    ((Label)e.Row.FindControl("lblUserNamer121")).Text = GetLocalResourceObject("ProHelp.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }

                if ((test1.Text == "Reply Help"))
                {
                    ((Label)e.Row.FindControl("lblUserNamer121")).Text = GetLocalResourceObject("rehelp.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    protected void GvGridHelpList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {


            try
            {

                Label test1 = (Label)e.Row.FindControl("lblUserNamer138");

                if ((test1.Text == "Accept"))
                {
                    ((Label)e.Row.FindControl("lblUserNamer138")).Text = GetLocalResourceObject("Accepted.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }

                if ((test1.Text == "Cancel"))
                {
                    ((Label)e.Row.FindControl("lblUserNamer138")).Text = GetLocalResourceObject("Cancel.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }

                if ((test1.Text == "Pending"))
                {
                    ((Label)e.Row.FindControl("lblUserNamer138")).Text = GetLocalResourceObject("pending.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            //try
            //{

            //    Label test1 = (Label)e.Row.FindControl("lblUserNamer121");

            //    if ((test1.Text == "Need Help"))
            //    {
            //        ((Label)e.Row.FindControl("lblUserNamer121")).Text = GetLocalResourceObject("needhelp.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
            //    }

            //    if ((test1.Text == "ProHelp"))
            //    {
            //        ((Label)e.Row.FindControl("lblUserNamer121")).Text = GetLocalResourceObject("ProHelp.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
            //    }

            //    if ((test1.Text == "Reply Help"))
            //    {
            //        ((Label)e.Row.FindControl("lblUserNamer121")).Text = GetLocalResourceObject("rehelp.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
            //    }

            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
        }
    }

    protected void gvRequestData_RowDataBound(object sender, GridViewRowEventArgs e)
    {


        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            try
            {

                Label test = (Label)e.Row.FindControl("lblUserNamer135");
                if (test != null)
                {
                    if ((test.Text == "Pending"))
                    {
                        ((Label)e.Row.FindControl("lblUserNamer135")).Text = GetLocalResourceObject("pending.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                    }
                    if ((test.Text == "Accepted"))
                    {
                        ((Label)e.Row.FindControl("lblUserNamer135")).Text = GetLocalResourceObject("Accepted.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                    }

                    if ((test.Text == "Cancel"))
                    {
                        ((Label)e.Row.FindControl("lblUserNamer135")).Text = GetLocalResourceObject("Cancel.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                if ((((Label)(e.Row.FindControl("lblMinAgoText"))).Text == "Pending"))
                {
                    ((Label)e.Row.FindControl("lblUserNamer135")).Text = GetLocalResourceObject("pending.Text").ToString();
                }
            }
        }
    }

    protected void GvColleaguesInv_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            try
            {

                Label test = (Label)e.Row.FindControl("lblMinAgo");

                if ((test.Text == "Days"))
                {
                    ((Label)e.Row.FindControl("lblMinAgo")).Text = GetLocalResourceObject("lblDays.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }

                if ((test.Text == "Hours"))
                {
                    ((Label)e.Row.FindControl("lblMinAgo")).Text = GetLocalResourceObject("Hours.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }

                if ((test.Text == "Minutes"))
                {
                    ((Label)e.Row.FindControl("lblMinAgo")).Text = GetLocalResourceObject("Minutes.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }
                if ((test.Text == "Second"))
                {
                    ((Label)e.Row.FindControl("lblMinAgo")).Text = GetLocalResourceObject("Second.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }
            }
            catch (Exception ex)
            {
                if ((((Label)(e.Row.FindControl("lblMinAgo"))).Text == "Days"))
                {
                    ((Label)e.Row.FindControl("lblMinAgo")).Text = GetLocalResourceObject("lblDays.Text").ToString();
                }
            }

            try
            {

                Label test1 = (Label)e.Row.FindControl("lblUserNamer13");
                if (test1 != null)
                {
                    if ((test1.Text == "Pending"))
                    {
                        ((Label)e.Row.FindControl("lblUserNamer13")).Text = GetLocalResourceObject("pending.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                    }
                    if ((test1.Text == "Accepted"))
                    {
                        ((Label)e.Row.FindControl("lblUserNamer13")).Text = GetLocalResourceObject("Accepted.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                    }

                    if ((test1.Text == "Cancel"))
                    {
                        ((Label)e.Row.FindControl("lblUserNamer13")).Text = GetLocalResourceObject("Cancel.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                if ((((Label)(e.Row.FindControl("lblUserNamer13"))).Text == "Pending"))
                {
                    ((Label)e.Row.FindControl("lblUserNamer13")).Text = GetLocalResourceObject("pending.Text").ToString();
                }
            }

        }

    }

    protected void GvOfferHelp_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            try
            {

                Label test = (Label)e.Row.FindControl("lbldaytime");

                if ((test.Text == "Days"))
                {
                    ((Label)e.Row.FindControl("lbldaytime")).Text = GetLocalResourceObject("lblDays.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }

                if ((test.Text == "Hours"))
                {
                    ((Label)e.Row.FindControl("lbldaytime")).Text = GetLocalResourceObject("Hours.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }

                if ((test.Text == "Minutes"))
                {
                    ((Label)e.Row.FindControl("lbldaytime")).Text = GetLocalResourceObject("Minutes.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }
                if ((test.Text == "Second"))
                {
                    ((Label)e.Row.FindControl("lbldaytime")).Text = GetLocalResourceObject("Second.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }
            }
            catch (Exception ex)
            {
                if ((((Label)(e.Row.FindControl("lbldaytime"))).Text == "Days"))
                {
                    ((Label)e.Row.FindControl("lbldaytime")).Text = GetLocalResourceObject("lblDays.Text").ToString();
                }
            }

            try
            {

                Label test1 = (Label)e.Row.FindControl("lblUserNamer16");
                if (test1 != null)
                {
                    if ((test1.Text == "Pending"))
                    {
                        ((Label)e.Row.FindControl("lblUserNamer16")).Text = GetLocalResourceObject("pending.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                    }
                    if ((test1.Text == "Accepted"))
                    {
                        ((Label)e.Row.FindControl("lblUserNamer16")).Text = GetLocalResourceObject("Accepted.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                    }

                    if ((test1.Text == "Cancel"))
                    {
                        ((Label)e.Row.FindControl("lblUserNamer16")).Text = GetLocalResourceObject("Cancel.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                if ((((Label)(e.Row.FindControl("lblUserNamer16"))).Text == "Pending"))
                {
                    ((Label)e.Row.FindControl("lblUserNamer16")).Text = GetLocalResourceObject("pending.Text").ToString();
                }
            }

        }

    }

    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Denish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion

    protected void btnBack_click(object sender, EventArgs e)
    {
        switch (Convert.ToInt32(Session["OrguserType"]))
        {
            case 2:
                Response.Redirect("Manager-dashboard.aspx");
                break;
            case 1:
                Response.Redirect("Organisation-dashboard.aspx");
                break;
            default:
                Response.Redirect("Employee_dashboard.aspx");
                break;
        }
        //  Response.Redirect(Convert.ToString(ViewState["PreviousPage"]));
    }
}