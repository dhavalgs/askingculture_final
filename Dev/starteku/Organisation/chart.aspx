﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true" CodeFile="chart.aspx.cs" Inherits="Organisation_chart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    
    <link href="../assets/css/perfect-scrollbar.min.css" rel="stylesheet" />


      <link href="../assets/assets/css/plugins/jquery-ui.css" rel="stylesheet" />

    <script src="../assets/assets/js/libs/jquery-1.10.2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/themes/start/jquery-ui.css" />
    <script src="../plugins/bootstrap-inputmask/jquery.inputmask.min.js" type="text/javascript"></script>

    <link href="../Scripts/fullcalendor/dATEPICKER/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <script src="../Scripts/fullcalendor/dATEPICKER/bootstrap-datetimepicker.min.js"></script>

    <style type="text/css">
        .chat-widget-head h4 {
            float: none;
        }
        .backgroundChange {
            background: #00bfff ;
        }
        .currentuser {
            background:#ff6a00;
        }

        .ActiveUser {
            background-color:antiquewhite;
        }
        
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            setTimeout(function () {
                //ExampanCollapsMenu();
                // tabMenuClick();
                //$(".myCompetence").addClass("active_page");
                SetMessgeTotal();
            }, 500);
        });
        function SetMessgeTotal() {
            return;
           // alert();
            if ($("#lblmessge").text() == "0") return;
            $("#lblmessge").text("0");
            $("#lblshowmessge").text("0");
            
            var a = "a";
            $.ajax({
                type: "POST",
                url: 'WebService1.asmx/UpdateMessge',
                data: "{'notIdQ':'" + a + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#divResult").html(msg.d);
                },
                error: function (e) {
                    $("#divResult").html("WebSerivce unreachable");
                }
            });
        }
        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server" onsubmit="return alert('');">

    <asp:HiddenField runat="server" ID="hdnNoRecord" meta:resourcekey="Norecord1"/>
    <asp:HiddenField runat="server" ID="Oops_You_have_missed_text" meta:resourcekey="Oops_You_have_missed_text"/>
    <asp:HiddenField runat="server" ID="Please_Select_Contact" meta:resourcekey="Please_Select_Contact"/>
    <input type="hidden" id="hdnActiveUser" name="hdnActiveUser" />
     <input type="hidden" id="hdnClientDate" name="hdnClientDate" />
    <input type="hidden" id="hdnClientTime" name="hdnClientTime" />
    
    <div class="col-md-6" style="margin-left: 28px;">
        <div class="heading-sec">
            <h1>
                
                <asp:Literal ID="Literal5" runat="server" EnableViewState="false" Text="Chat" meta:resourcekey="Chat"/><i>
                    <asp:Literal ID="Literal1" runat="server" EnableViewState="false"  meta:resourcekey="ResWelCome"/>
                    <span runat="server" id="Conatct"></span></i>
            </h1>
        </div>
    </div>
    <br />
    <br />
    <div class="">
        <div class="container">
            <div class="col-md-12">
                <div class="col-md-3" style="margin-top: 0px; margin-bottom: 20px;">
                    <div class="contact-list1 widget-body">
                        <div class="timeline-head yellow">
                            <asp:HiddenField ID="hdnChat12" runat="server" meta:resourcekey="ResChat"/>
                            <span><asp:Literal ID="Literal2" runat="server" EnableViewState="false" Text="My Contact List" meta:resourcekey="MyContactList"/></span>
                            <%--<input type="text" class="form-control userSearch" onchange="SearchContactList(this)"/>--%>
                        </div>
                       
                        <ul id="name_scroll">
                            <li><asp:Literal ID="Literal3" runat="server" EnableViewState="false" Text="No Record Found. " meta:resourcekey="Norecord"/></li>
                        </ul>
                           
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="chat-widget widget-body">
                        <div class="chat-widget-head yellow">
                            <h4><asp:Literal ID="Literal6" runat="server" EnableViewState="false" Text="Chat" meta:resourcekey="Chat"/> 
                                <asp:Label ID="chat1" runat="server"></asp:Label></h4>
                        </div>
                        <ul id="scrollbox6">
                            <li class="reply"><asp:Literal ID="Literal4" runat="server" EnableViewState="false" Text="No Record Found. " meta:resourcekey="Norecord"/></li>
                        </ul>

                        <div class="reply-sec">
                            <div class="reply-sec-MESSAGE" style="margin-left: 20px;">
                                <asp:TextBox placeholder="TYPE YOUR MESSAGE HERE" id="message" class="txtMsg" style="padding: 10px;" meta:resourcekey="TypeMessage"  runat="server"></asp:TextBox>
                                <%--<input type="text" placeholder="TYPE YOUR MESSAGE HERE" id="message" class="txtMsg" style="padding: 10px;" />--%>



                                <a href="#" title="" class="black" onclick="insertmessage()"><i class="fa fa-comments-o"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- TIME LINE -->
            <!-- Chat Widget -->
            <!-- Recent Post -->
            <div class="col-md-3">
            </div>  
            <!-- Twitter Widget -->
            <!-- Weather Widget -->
        </div>
        <!-- Container -->
    </div>
    <!-- Wrapper -->
    <!-- RAIn ANIMATED ICON-->
    <script src="../assets/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="../assets/js/perfect-scrollbar.min.js"></script>
    
       <script>
        var icons = new Skycons();
        icons.set("rain", Skycons.RAIN);
        icons.play();
   
        function SearchContactList(tag) {
           // alert(tag.text());
            $(".contact_name").each(function() {
               
            });
        }
        //$("#name_scroll").perfectScrollbar();
        //$("#scrollbox6").perfectScrollbar();
        //         window.onload = function () {
        //             alert("sd");
        //             Userdetails();
        //         };
        $(document).keypress(function (e) {

           

            if (e.keyCode === 13) {
                insertmessage();
                e.preventDefault();
                return false;
            }
        });
        function getQuerystring(name)
        {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
            

        }
        $(document).ready(function () {
            var umId= getQuerystring("mes");
            GetmessgeById(umId);
            
            
        });
        var imageto; var subjectto;

        function Default_Userdetails() {
            interval_Userdetails();
            //interval_GetmessgeById();          

        }

        function SubmitMsg() {
            $(".txtMsg").bind('keyup', function (e) {

                if (e.keyCode == 13) {
                    insertmessage();
                }
                return false;
            });
           
        }

        function click() {
            //debugger;
            
            var c = $("#ContentPlaceHolder1_hdnChat12").val();
           
            if (String(FromUser) != "")
            {
                $(".chat-widget-head h4").text(c + ": " + FromUser);
            }
            
            //$(".contact_name").click(function (a) {
            //    //alert($(this).text());
            //  $(".chat-widget-head h4").text(c + ": " + ($(this).text()));

            //});
            //$(".contact_name").click(function () { $(".contact_name").removeClass("backgroundChange"); $(this).addClass("ActiveUser"); });
        }

        function ChangeName(a)
        {

            FromUser = "";
            var c = $("#ContentPlaceHolder1_hdnChat12").val();
            $(".chat-widget-head h4").text(c + ": " + ($(a).find('.chat_name').text()));
        }

        function set_DownScrol() {
            $("#scrollbox6").animate({ scrollTop: $("#scrollbox6").height() + 200000 }, "slow");
        }
     
        var interval1 = 0;
        function interval_Userdetails() {
            var Count;
            var notificationCount = [];
            Userdetails();           
            interval1 = setInterval(function () {
                if (ajex != null) {
                    ajex.abort();
                }
                ajex = Userdetails();
            }, 40000);
        }

        var interval2 = 0;
        function interval_GetmessgeById() {
            var Count;
            var notificationCount = [];
           GetmessgeById(touserid);
            interval2 = setTimeout(function () {
                if (ajex != null) {
                    ajex.abort();
                }
                //ajex = GetmessgeById(touserid);
            }, 10000);
        }

        var touserid = '0';
        function Userdetails() {
            var html = "";
            var acUSer = $("#hdnActiveUser").val();
            $("#name_scroll").html('');
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "chart.aspx/Userdetails",
                data: "{}",
                dataType: "json",
                success: function (data) {
                  
                    if (data.d == null)
                    {
                        window.location.href = "chart.aspx";
                    }
                    cat = data.d;
                    for (var i = 0; i < data.d.length; i++) {
                        imageto = data.d[i].sessionimage;
                        
                        if (acUSer != "") {
                            if (String(acUSer) == String(data.d[i].userId)) {
                                html += "<li class='contact_name ActiveUser' id='contact_" + data.d[i].userId + "' onclick ='ChangeName(this); GetmessgeById(" + data.d[i].userId + ");'><div class='col-md-3 chat_img'>";
                            }
                            else {
                                html += "<li class='contact_name' id='contact_" + data.d[i].userId + "' onclick ='ChangeName(this); GetmessgeById(" + data.d[i].userId + ");'><div class='col-md-3 chat_img'>";
                            }

                        }

                        else {
                            html += "<li class='contact_name' id='contact_" + data.d[i].userId + "' onclick ='ChangeName(this); GetmessgeById(" + data.d[i].userId + ");'><div class='col-md-3 chat_img'>";
                        }
                        
                        if (data.d[i].notification != 0) {
                            html += "<div class='chat_notification'>" + data.d[i].notification + "</div>";
                        }
                        html += "<img src='../Log/upload/Userimage/" + data.d[i].Image + "' alt='' onerror='../Organisation/images/sign-in.jpg'/></div>";
                        html += "<div class='col-md-6 chat_name'><a href='#' title='' >" + data.d[i].name + "</a>";
                        html += "</div></li>";
                    }
                    if (data.d.length == 0) {
                        
                        //html += "No records found.";
                        html += $("#ContentPlaceHolder1_hdnNoRecord").val();
                    }
                    $("#name_scroll").html('');
                    $("#name_scroll").append(html);
                   click();
                 
                },
                error: function (result) {
                    //alert("Error");
                   
                }
            });
        }

        var FromUser = "";
        function GetmessgeById(userId) {
           
            if (userId != '0') {
                click();
                $(".contact_name").removeClass("ActiveUser");
                $("#contact_"+userId).addClass("ActiveUser");
                $("#hdnActiveUser").val(userId);
                touserid = userId;
                var html = "";
              
                $("#scrollbox6").html('');
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "chart.aspx/GetmessgeById",
                    data: "{'userId':'" + userId + "'}",
                    dataType: "json",
                    success: function (data) {
                        
                        cat = data.d;
                        
                        for (var i = 0; i < data.d.length; i++) {
                            var cls = "reply";
                            if (data.d[i].mesfromUserId == data.d[i].sessionUserId) {
                                cls = "reply1";
                            }
                            else {
                                
                                FromUser = data.d[i].name;
                               
                            }   
                            html += "<li class='" + cls + "'><div class='chat-thumb'><img src='../Log/upload/Userimage/" + data.d[i].Image + "' alt='' onerror='../Organisation/images/sign-in.jpg'/></div>";
                            html += "<div class='chat-desc'><p>" + data.d[i].messubject + "</p><i class='chat-time'>" + data.d[i].masCreatedDate + "</i>";
                            html += "</div></li>";
                        }
                        if (data.d.length > 0) {
                            //alert(data.d.name);
                            //var c = $("#ContentPlaceHolder1_hdnChat12").val();
                            //$(".chat-widget-head h4").text(c+": " + FromUser);
                        }
                        
                        Userdetails();
                        notificationShowHide();
                        notification_Count();
                        notificationChecker();

                       

                        setInterval(function () {
                            GetmessgeById_afterinsert(touserid);
                        }, 10000);

                        if (data.d.length == 0) {

                            html += $("#ContentPlaceHolder1_hdnNoRecord").val();
                            //html += "No records found.";
                        }
                        
                        $("#scrollbox6").html('');
                        $("#scrollbox6").append(html);
                        set_DownScrol();
                    },
                    error: function (result) {
                        //alert("Error");
                       
                    }
                });
            }
        }


        function insertmessage() {
            debugger;
            subjectto = '';
            var now = new Date();
            $('#hdnClientDate').val(now.getDate() + '/' + (now.getMonth() + 1) + '/' + now.getFullYear());
            $('#hdnClientTime').val(now.getHours() + "-" + now.getMinutes() + "-" + now.getSeconds());
            var message = document.getElementById("ContentPlaceHolder1_message").value;
            if (touserid != '0') {
                if (message != '') {
                    subjectto = message;
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "chart.aspx/insertmessage",
                        // data: "{'message':'" + message + "','touserid':'" + touserid + "','currentdate':'"+$('#hdnClientDate').val()+"','currenttime':'"+$('#hdnClientTime').val()+"'}",
                        data: "{'message':'" + message + "','touserid':'" + touserid + "'}",
                        dataType: "json",
                        success: function (data) {
                            var obj = data.d;
                         
                            var s = String(obj).split(',');
                          
                            if (String(s[0]) == 'true') {
                           
                                document.getElementById("ContentPlaceHolder1_message").value = '';
                                // GetmessgeById(touserid);
                                //GetmessgeById_afterinsert(touserid)
                                binddata(String(s[1]));
                            }
                        },
                        error: function (result) {                           
                           // alert("Error");
                        }
                    });
                }
                else {
                    generate("warning", $("#ContentPlaceHolder1_Oops_You_have_missed_text").val(), "bottomCenter");//"Oops! You have missed text to send a massage."
                    return false;
                }
            }
            else {
                generate("warning", $("#ContentPlaceHolder1_Please_Select_Contact").val(), "bottomCenter");//"Please Select Contact"
                
                document.getElementById("ContentPlaceHolder1_message").value = '';
                return false;
            }
        }


        function GetmessgeById_afterinsert(userId) {

            if (userId != '0') {
                touserid = userId;
                var html = "";
                
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "chart.aspx/GetAllmessagebyId_stauts",
                    data: "{'userId':'" + userId + "'}",
                    dataType: "json",
                    success: function (data) {
                        cat = data.d;
                        for (var i = 0; i < data.d.length; i++) {
                            var cls = "reply";
                            if (data.d[i].mesfromUserId == data.d[i].sessionUserId) {
                                cls = "reply1";
                            }
                            html += "<li class='" + cls + "'><div class='chat-thumb'><img src='../Log/upload/Userimage/" + data.d[i].Image + "' alt='' onerror='../Organisation/images/sign-in.jpg'/></div>";
                            html += "<div class='chat-desc'><p>" + data.d[i].messubject + "</p><i class='chat-time'>" + data.d[i].masCreatedDate + "</i>";
                            html += "</div></li>";
                        }
                        
                        $("#scrollbox6").append(html);
                        set_DownScrol();
                    },
                    error: function (result) {                       
                       // alert("Error");
                       
                    }
                });
            }
        }

        function binddata(date1) {
            //var now = new Date();
            
            var now = new Date();
            var date = now.format("dd/M/yy h:mm tt");
            
            var html = "";
            html += "<li class='reply1'><div class='chat-thumb'><img src='../Log/upload/Userimage/" + imageto + "' alt='' onerror='../Organisation/images/sign-in.jpg'/></div>";
            html += "<div class='chat-desc'><p>" + subjectto + "</p><i class='chat-time'>" + date + "</i>";
            html += "</div></li>";
            $("#scrollbox6").append(html);
            set_DownScrol();
        }
                    
    </script>
</asp:Content>
