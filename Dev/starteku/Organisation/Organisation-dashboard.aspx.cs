﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using starteku_BusinessLogic.Model;
using startetku.Business.Logic;
using System.Web.UI;
namespace Organisation
{
    public partial class Organisation_Organisation_dashboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Organisation_OrganisationMaster master = (Organisation_OrganisationMaster)Page.Master;
            //string str ="Expand";
            //master.HiddenValue = "Expand";
            try
            {

              
                if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
                {
                    Response.Redirect("login.aspx");
                }
                
                // replyChat.Visible = false;
                if (!IsPostBack)
                {
                   
                    StartJoyRideTour();
                   
                    //Dashboard.InnerHtml = CommonMessages.Welcome + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
                    Dashboard.InnerHtml = "   " + Convert.ToString(Session["OrgUserName"]) + "!";
                    //GetAllEmployeeList();
                    /* GetAllmessagebyId(Convert.ToInt32(Session["OrgUserId"]));*/
                    //GetAllEmployeeContactList();

                   
                    if (Request.QueryString["user"] == "saveSuccess")
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Redirect", "generate('information','" + hdnSystemUpdated.Text + "','bottomCenter');", true);
                    }
                    
                    GetAllDepartments();
                   
                    GetAllJobTypes();
                   
                    GetAllJobtype();

                
                    ResourceLanguageBM obj = new ResourceLanguageBM();
                    DataSet resds = new DataSet();
                    try
                    {
                        resds = obj.GetAllResourceLanguage();
                    }
                    catch (Exception ex)
                    {
                        ExceptionLogger.LogException(ex, 0, "GetAllResourceLanguage");
                    }
                   

                    ddlLanguage.DataSource = resds;
                    ddlLanguage.DataTextField = "resLanguage";
                    ddlLanguage.DataValueField = "resCulture";
                    ddlLanguage.DataBind();

                    // HttpContext.GetLocalResourceObject("~/Organisation/Organisation-dashboard.aspx", "selectlan.Text").ToString();
                    string testmy=HttpContext.GetLocalResourceObject("~/Organisation/Organisation-dashboard.aspx", "selectlan.Text").ToString();
                    ddlLanguage.Items.Insert(0, new ListItem(testmy, "0"));//
                    ddlLanguage.SelectedValue = "0";

                    ddlgender.Items.Insert(0, new ListItem(hdnSelectGender.Value, CommonModule.dropDownZeroValue));
                    GetCheckBoxListData();
                    try
                    {
                        GetAllTeam();
                    }
                    catch (Exception ex)
                    {
                        ExceptionLogger.LogException(ex, 0, "Organisation-dashboard->PageLoad->GetAllTeam");
                    }
                    
                }
                ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "OnLoadCall();", true);
                GetResource();
              
                lan.Value = Convert.ToString(Session["Culture"]);
                //var db = new startetkuEntities1();
                //lan.Value= db.ResourceLanguages.;
               
                if (!IsPostBack)
                {
                    EmployeeSkillBM obj = new EmployeeSkillBM();

                    var userid = 0;
                    var usertype = 3;

                    userid = Convert.ToInt32(Session["OrgUserId"]);
                    usertype = Convert.ToInt32(Session["OrguserType"]);

                    obj.skillIsActive = true;
                    obj.skillIsDeleted = false;
                    obj.skillUserId = userid;
                    obj.skillCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);

                    obj.temp = 0;
                    obj.mulsel = "0";
                  
                    try
                    {
                        obj.SpEmpAvgSkillByManager();
                    }
                    catch (Exception ex)
                    {
                        ExceptionLogger.LogException(ex, 0, "SpEmpAvgSkillByManager");
                    }
                    DataSet resds = obj.ds;

                    managercat.DataSource = resds;
                    if (Convert.ToString(Session["Culture"]) == "Danish")
                    {
                        managercat.DataTextField = "comCompetenceDN";

                    }
                    else
                    {
                        managercat.DataTextField = "comCompetence";

                    }

                    managercat.DataValueField = "skillComId";
                    managercat.DataBind();

                  
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex, 0, "Organisation-dashboard->PageLoad");
            }
        }
        private void StartJoyRideTour()
        {
            var obj = new JoyrideTourBM();
            try
            {

                var uid = Convert.ToInt32(Session["OrgUserId"]);
                if (uid <= 0) return;

                DataSet ds = obj.GetJoyrideTourByUserId(uid);


                if (ds == null) return;

                var dataRowCollection = ds.Tables[0].Rows;
                if (dataRowCollection != null)
                {
                    var isAlreadyTakenTour = dataRowCollection[0]["isTourOff"];
                    if (isAlreadyTakenTour != null)
                    {
                        var x = Convert.ToInt32(dataRowCollection[0]["isTourOff"]);
                        if (x > 0) return;
                    }
                }

                //DateTime date=DateTime.Now.AddDays(-2);
                //if (Session["userDateTime"] != null)
                //{
                //     date = Convert.ToDateTime(Session["userDateTime"]).Date;
                //}

                //var todaysDate = DateTime.Now.Date;

                //if (date == todaysDate)
                //{
                //    return;
                //}
                if (Session["isJoyRideTaken"] == null)
                {
                    CmsBM objMail = new CmsBM();
                    objMail.cmsName = "WelcomeMessage";

                    objMail.SelectMailTemplateByName();
                    DataSet dsMail = objMail.ds;

                    if (dsMail.Tables[0].Rows.Count > 0)
                    {
                        string confirmMail = "";
                        if (Convert.ToString(Session["Culture"]) == "English")
                        {
                            confirmMail = dsMail.Tables[0].Rows[0]["cmsDescription"].ToString();

                        }
                        else
                        {
                            confirmMail = dsMail.Tables[0].Rows[0]["cmsDescriptionDN"].ToString();

                        }
                        welcomeMsg.InnerHtml = confirmMail;
                    }
                    Session["isJoyRideTaken"] = "true";

                    Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "JoyrideStart();", true);
                }
                //  ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "JoyrideStart();", false);
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex, 0, "StartJoyRideTour");

            }
        }
        #region Button Event
        protected void btnsubmit_click(object sender, EventArgs e)
        {
            Insertuser();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //InsertMessage();
            //GetAllmessagebyId(Convert.ToInt32(ViewState["touser"]));
            //replyChat.Visible = true;
        }

        #endregion

        #region method
        //protected void InsertMessage()
        //{
        //    NotificationBM obj = new NotificationBM();
        //    obj.mesfromUserId = Convert.ToInt32(Session["OrgUserId"]);
        //    obj.masToUserId = Convert.ToInt32(ViewState["touser"]);
        //    obj.messubject = txtmessage.Text;
        //    obj.masIsActive = true;
        //    obj.masIsDeleted = false;
        //    obj.masCreatedDate = DateTime.Now;
        //    obj.masCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        //    obj.mastype = "";
        //    if (obj.InsertMessage())
        //    {
        //        txtmessage.Text = "";
        //    }
        //    // insertChild(id, level);
        //    //if (id > 0)
        //    //{
        //    //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('Save SussessFully !');</script>", false);
        //    //}

        //}
        //protected void GetAllmessagebyId(Int32 id)
        //{
        //    NotificationBM obj = new NotificationBM();
        //    obj.mesfromUserId = id;
        //    obj.masToUserId = Convert.ToInt32(Session["OrgUserId"]); 
        //    obj.GetAllmessagebyId();
        //    DataSet ds = obj.ds;
        //    ViewState["chat"] = ds;
        //    if (ds != null)
        //    {
        //        if (ds.Tables[0].Rows.Count > 0)
        //        {
        //            rpt_chat.DataSource = ds.Tables[0];
        //            rpt_chat.DataBind();
        //        }
        //        else
        //        {
        //            rpt_chat.DataSource = null;
        //            rpt_chat.DataBind();
        //        }
        //    }
        //    else
        //    {
        //        rpt_chat.DataSource = null;
        //        rpt_chat.DataBind();
        //    }
        //}
        protected void txtEmail_TextChanged(object sender, EventArgs e)
        {
            //Obsolute function ?? We have used webservice ajx instead of this |Saurin |  20150302
            UserBM obj2 = new UserBM();
            obj2.useremailCheckDuplication(txtEmail.Text, -1);
            String returnMsg = obj2.ReturnString;
            if (returnMsg == "")
            {
            }
            else
            {
                if (returnMsg == "userEmail")
                    lblEmail.Text = CommonModule.msgEmailAlreadyExists;

            }
        }
        /*protected void GetAllEmployeeList()
    {
     *  //Obsolute function ?? We have used webservice ajx instead of this |Saurin |  20150302
        UserBM obj = new UserBM();
        obj.userIsActive = true;
        obj.userIsDeleted = false;
        obj.userType = 2;
        obj.userCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllEmployee();
        DataSet ds = obj.ds;

        //Repeater1.DataSource = dt;
        // Repeater1.DataBind();

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                Repeaterman.DataSource = ds.Tables[0];
                Repeaterman.DataBind();

                // Repeateremp.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                Repeaterman.DataSource = null;
                Repeaterman.DataBind();
            }
        }
        else
        {
            Repeaterman.DataSource = null;
            Repeaterman.DataBind();
        }
    }*/
        protected void Insertuser()
        {

            //Obsolute function ?? We have used webservice ajx instead of this |Saurin |  20150302
            string filePath = "";
            UserBM obj2 = new UserBM();
            obj2.useremailCheckDuplication(txtEmail.Text, -1);
            String returnMsg = obj2.ReturnString;
            if (returnMsg == "")
            {

                UserBM obj = new UserBM();
                obj.userFirstName = txtName.Text;
                obj.userLastName = txtLastname.Text;
                obj.userZip = "";
                obj.userAddress = txtaddress.Text;
                obj.userCountryId = Convert.ToInt32(0);
                obj.userStateId = Convert.ToInt32(0);
                //obj.userCityId = Convert.ToInt32(0);
                obj.userCityId = "";
                obj.comphone = txtMobile.Text;
                obj.usercontact = txtMobile.Text;
                obj.userEmail = txtEmail.Text;
                obj.userType = 2;
                obj.userCreatedDate = DateTime.Now;
                obj.userIsActive = true;
                obj.userIsDeleted = false;
                obj.userCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
                string pass = CommonModule.Generate_Random_password();
                obj.userPassword = CommonModule.encrypt(pass.Trim());
                obj.userGender = "";
                obj.userImage = "ofile_img.png";
                obj.userLevel = 1;
                obj.userdepId = Convert.ToString(ddlDepartment.SelectedValue); //Saurin :20150429
                obj.userJobType = 0;
                obj.InsertOrganisation();
                if (obj.ReturnBoolean == true)
                {
                    sendmail(txtEmail.Text, txtName.Text, pass, 0);
                    // GetAllEmployeeList();
                }
            }
            else
            {
                if (returnMsg == "userEmail")
                    lblMsg.Text = CommonModule.msgEmailAlreadyExists;
                else
                    lblMsg.Text = CommonModule.msgProblemInsertRecord;
            }
        }
        protected void sendmail(string email, string name, string pass, int userId)
        {
            try
            {
                // Common.WriteLog("start");
                //string subject = "Registration";
                string fromid = Convert.ToString(ConfigurationManager.AppSettings["fromid"]);

                //String confirmMail =
                //    CommonModule.getTemplatebyname("Registration", userId);
                Template template = CommonModule.getTemplatebyname1("Registration", userId);
                String confirmMail = template.TemplateName;
                if (!String.IsNullOrEmpty(confirmMail))
                {

                    string subject = HttpContext.GetLocalResourceObject("~/Organisation/Organisation-dashboard.aspx", "RegistrationSubjectEng.Text").ToString();
                    if (template.Language == "Danish")
                    {
                        subject = HttpContext.GetLocalResourceObject("~/Organisation/Organisation-dashboard.aspx", "RegistrationSubjectDn.Text").ToString();
                    }
                    string tempString = confirmMail;
                    tempString = tempString.Replace("###name###", name);
                    tempString = tempString.Replace("###email###", email);
                    tempString = tempString.Replace("###password###", pass);
                    //SendMail(tempString, txtEmail.Text, txtFname.Text);
                    CommonModule.SendMailToUser(email, subject, tempString, fromid, Convert.ToString(HttpContext.Current.Session["OrgCompanyId"]));
                }
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in sendmail" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            }


        }
        //protected void GetAllEmployeeContactList()
        //{
        //    UserBM obj = new UserBM();
        //    obj.userIsActive = true;
        //    obj.userIsDeleted = false;
        //    obj.userType = 2;
        //    obj.userCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        //    obj.userId = Convert.ToInt32(Session["OrgUserId"]);
        //    obj.GetAllEmployeebymessge();
        //    DataSet ds = obj.ds;

        //    if (ds != null)
        //    {
        //        if (ds.Tables[0].Rows.Count > 0)
        //        {
        //            lst_contacts.DataSource = ds.Tables[0];
        //            lst_contacts.DataBind();
        //        }
        //        else
        //        {
        //            lst_contacts.DataSource = null;
        //            lst_contacts.DataBind();
        //        }
        //    }
        //    else
        //    {
        //        lst_contacts.DataSource = null;
        //        lst_contacts.DataBind();
        //    }
        //}
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            //string language = "French";
            string language = Convert.ToString(Session["Culture"]);
            string languageId = "";
            //if (!string.IsNullOrEmpty(language))
            //{
            //    if (language.EndsWith("Danish")) languageId = "da-DK";
            //    else languageId = "en-GB";
            //    SetCulture(languageId);
            //}
            //ResourceLanguageBM obj = new ResourceLanguageBM();
            //DataSet resds = obj.GetAllResourceLanguage();
            //if (!string.IsNullOrEmpty(language))
            //{
            //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
            //    {
            //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
            //        {
            //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
            //        }
            //    }

            //    SetCulture(languageId);
            //}
            ResourceLanguageBM obj = new ResourceLanguageBM();
            if (!string.IsNullOrEmpty(language))
            {
                DataSet resds = obj.GetResourceLanguage(language);
                languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

                SetCulture(languageId);
            }


            if (Session["Language"] != null)
            {
                if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
            }

            base.InitializeCulture();
        }
        protected void SetCulture(string languageId)
        {
            Session["Language"] = languageId;
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);


        }
        #endregion

        #region repeater-itembound
        protected void lst_contacts_OnItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Message")
            {
                try
                {
                    if (!String.IsNullOrEmpty(Convert.ToString(e.CommandArgument)))
                    {
                        LinkButton lb_contact = e.Item.FindControl("lb_contact") as LinkButton;
                        // chat.InnerHtml = lb_contact.Text;

                        //ViewState["touser"] = e.CommandArgument;
                        //GetAllmessagebyId(Convert.ToInt32(e.CommandArgument));
                        //lblDefaultText.Visible = false;
                        //replyChat.Visible = true;
                        //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>setTimeout(function () { HighlighThis(); }, 500);</script>", false);
                    }
                    else
                    {
                        //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>Please select User Name;</script>", false);
                    }
                    //DataSet ds = (DataSet)ViewState["chat"];
                    //if (ds != null)
                    //{
                    //    if (ds.Tables[0].Rows.Count > 0)
                    //    {
                    //        rpt_chat.DataSource = ds;
                    //        rpt_chat.DataBind();
                    //        //dvsubretr.Visible = true;
                    //    }
                    //    else
                    //    {
                    //        // dvsubretr.Visible = false;
                    //    }
                    //}
                    //else
                    //{
                    //    // dvsubretr.Visible = false;
                    //}
                }
                catch (Exception ex)
                {
                    Common.WriteLog("error in total" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                }

            }
        }
        protected void rpt_chat_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    DataSet ds = (DataSet)ViewState["chat"];
                    HiddenField mesfromUserId = e.Item.FindControl("mesfromUserId") as HiddenField;
                    if (ds != null)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                if (Convert.ToInt32(mesfromUserId.Value) == Convert.ToInt32(Session["OrgUserId"]))
                                {
                                    HtmlGenericControl myLi = (HtmlGenericControl)e.Item.FindControl("listItem");
                                    myLi.Attributes.Add("class", "reply1");
                                }
                            }

                        }
                    }


                    //Label lbpoint = e.Item.FindControl("lbpoint") as Label;
                    //Label lbllocal = e.Item.FindControl("lbllocal") as Label;
                    //Label lblAchive = e.Item.FindControl("lblAchive") as Label;
                    //Label lbltarget = e.Item.FindControl("lbltarget") as Label;
                    //Label lblset = e.Item.FindControl("lblset") as Label;
                    //DataSet ds = (DataSet)ViewState["data"];
                    //if (ds != null)
                    //{
                    //if (ds.Tables[0].Rows.Count > 0)
                    //{
                    //    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    //    {                            //comId
                    //        if (Convert.ToInt32(comId.Value) == Convert.ToInt32(ds.Tables[0].Rows[i]["skillComId"]))
                    //        {
                    //            if (!String.IsNullOrEmpty((Convert.ToString(ds.Tables[0].Rows[i]["skillLocal"]))))
                    //            {
                    //                lbllocal.Text = (Convert.ToString(ds.Tables[0].Rows[i]["skillLocal"]));
                    //            }
                    //            if (!String.IsNullOrEmpty((Convert.ToString(ds.Tables[0].Rows[i]["skillAchive"]))))
                    //            {
                    //                lblAchive.Text = (Convert.ToString(ds.Tables[0].Rows[i]["skillAchive"]));
                    //            }
                    //            if (!String.IsNullOrEmpty((Convert.ToString(ds.Tables[0].Rows[i]["skilltarget"]))))
                    //            {
                    //                lbltarget.Text = (Convert.ToString(ds.Tables[0].Rows[i]["skilltarget"]));
                    //            }
                    //            if (!String.IsNullOrEmpty((Convert.ToString(ds.Tables[0].Rows[i]["skillSet"]))))
                    //            {
                    //                //lblset.Text = (Convert.ToString(ds.Tables[0].Rows[i]["skillSet"]));
                    //                lblset.Text = "Yes";
                    //            }
                    //        }
                    //    }
                    //}

                    //
                    // }
                }

            }
            catch (Exception ex)
            {
                Common.WriteLog("error in rpt_chat_ItemDataBound" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;
            }
        }
        #endregion

        protected void OnTick_RefreshChatList(object sender, EventArgs e)
        {

            if (string.IsNullOrWhiteSpace(Convert.ToString(ViewState["touser"])))
            {
                return;
            }
            //GetAllmessagebyId(Convert.ToInt32(ViewState["touser"]));
        }

        #region Dropdown
        protected void GetAllJobTypes()
        {
            try
            {


                JobTypeBM obj = new JobTypeBM();
                obj.jobIsActive = true;
                obj.jobIsDeleted = false;
                obj.jobCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
                obj.GetAllJobType();
                DataSet ds = obj.ds;

                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddlJobType.Items.Clear();

                        ddlJobType.DataSource = ds.Tables[0];
                        if (Convert.ToString(Session["Culture"]) == "Danish")
                        {
                            ddlJobType.DataTextField = "jobNameDN";
                        }
                        else
                        {
                            ddlJobType.DataTextField = "jobName";
                        }
                        ddlJobType.DataValueField = "jobId";
                        ddlJobType.DataBind();

                        ddlJobType.Items.Insert(0, new ListItem(hdnSelectJob.Value, CommonModule.dropDownZeroValue));
                    }
                    else
                    {
                        ddlJobType.Items.Clear();
                        ddlJobType.Items.Insert(0, new ListItem(hdnSelectJob.Value, CommonModule.dropDownZeroValue));
                    }
                }
                else
                {
                    ddlJobType.Items.Clear();
                    ddlJobType.Items.Insert(0, new ListItem(hdnSelectJob.Value, CommonModule.dropDownZeroValue));
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex, 0, "GetAllJobTypes");
            }

        }
        #region DropDown Event
        protected void ddlcompetence_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetCheckBoxListData();


        }
        protected void ddljobtype_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetCheckBoxListData();

        }
        protected void GetCheckBoxListData()
        {
            try
            {


                CompetenceMasterBM obj = new CompetenceMasterBM();
                obj.catIsActive = true;
                obj.catIsDeleted = false;
                if (string.IsNullOrEmpty(ddlDepartment.SelectedValue))
                {
                    NoRecords.Visible = true;
                    return;
                }
                obj.comDivId = Convert.ToInt32(ddlDepartment.SelectedValue);
                obj.comJobtype = Convert.ToInt32(ddljobtype1.SelectedValue);
                obj.GetAllCategoryByJobTypeId_DivId();
                // lblDivIdName.Text = ddlDepartment.SelectedItem.Text;

                DataSet ds = obj.ds;

                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        repCompCat.DataSource = ds.Tables[0];
                        //chkList.DataTextField = "catName";
                        //chkList.DataValueField = "catId";
                        repCompCat.DataBind();
                        //chkList.SelectedIndex = 0;
                        NoRecords.Visible = false;

                        //foreach (ListItem li in chkList.Items)
                        //{
                        //    li.Selected = true;
                        //}
                    }
                    else
                    {
                        NoRecords.Visible = true;
                        //chkList.Items.Clear();
                        //ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownCategory, CommonModule.dropDownZeroValue));
                    }

                }
                else
                {
                    NoRecords.Visible = true;
                    //chkList.Items.Clear();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex, 0, "GetCheckBoxListData");
            }
        }
        protected void GetAllJobtype()
        {
            try
            {


                JobTypeBM obj = new JobTypeBM();
                obj.jobIsActive = true;
                obj.jobIsDeleted = false;
                obj.jobCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
                obj.GetAllJobType();
                DataSet ds = obj.ds;

                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddljobtype1.Items.Clear();


                        ddljobtype1.DataSource = ds.Tables[0];
                        if (Convert.ToString(Session["Culture"]) == "Danish")
                        {
                            ddljobtype1.DataTextField = "jobNameDN";
                        }

                        else
                        {
                            ddljobtype1.DataTextField = "jobName";
                        }
                        ddljobtype1.DataValueField = "jobId";
                        ddljobtype1.DataBind();

                        ddljobtype1.Items.Insert(0, new ListItem(hdnSelectJob.Value, CommonModule.dropDownZeroValue));
                    }
                    else
                    {
                        ddljobtype1.Items.Clear();
                        ddljobtype1.Items.Insert(0, new ListItem(hdnSelectJob.Value, CommonModule.dropDownZeroValue));
                    }
                }
                else
                {
                    ddljobtype1.Items.Clear();
                    ddljobtype1.Items.Insert(0, new ListItem(hdnSelectJob.Value, CommonModule.dropDownZeroValue));
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex, 0, "GetAllJobtype");
            }
        }
        #endregion
        protected void GetAllDepartments()
        {
            //DepartmentsBM obj = new DepartmentsBM();
            //obj.depIsActive = true;
            //obj.depIsDeleted = false;
            //obj.depCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            //obj.GetAllDepartments();
            //DataSet ds = obj.ds;
            try
            {


                DivisionBM obj = new DivisionBM();
                obj.depIsActive = true;
                obj.depIsDeleted = false;
                obj.depCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
                obj.GetAllDivision();
                DataSet ds = obj.ds;

                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {

                        //in graph
                        ddldropdownDivisionGraph.Items.Clear();

                        ddldropdownDivisionGraph.DataSource = ds.Tables[0];

                        if (Convert.ToString(Session["Culture"]) == "Danish")
                        {

                            ddldropdownDivisionGraph.DataTextField = "divNameDN";
                        }

                        else
                        {
                            ddldropdownDivisionGraph.DataTextField = "divName";
                        }
                        ddldropdownDivisionGraph.DataValueField = "divId";
                        ddldropdownDivisionGraph.DataBind();

                        ddldropdownDivisionGraph.Items.Insert(0, new ListItem(hdnSelectDivisions.Value, CommonModule.dropDownZeroValue));

                        //

                        ddlDepartment.Items.Clear();

                        ddlDepartment.DataSource = ds.Tables[0];
                        if (Convert.ToString(Session["Culture"]) == "Danish")
                        {
                            ddlDepartment.DataTextField = "divNameDN";
                        }
                        else
                        {
                            ddlDepartment.DataTextField = "divName";
                        }

                        ddlDepartment.DataValueField = "divId";
                        ddlDepartment.DataBind();

                        ddlDepartment.Items.Insert(0, new ListItem(hdnSelectDivisions.Value, CommonModule.dropDownZeroValue));
                    }
                    else
                    {
                        ddlDepartment.Items.Clear();
                        ddlDepartment.Items.Insert(0, new ListItem(hdnSelectDivisions.Value, CommonModule.dropDownZeroValue));
                    }
                }
                else
                {
                    ddlDepartment.Items.Clear();
                    ddlDepartment.Items.Insert(0, new ListItem(hdnSelectDivisions.Value, CommonModule.dropDownZeroValue));
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex, 0, "GetAllDepartments");
            }
        }

        protected void GetAllTeam()
        {
            JobTypeBM obj = new JobTypeBM();
            obj.TeamIDs = "0";
            obj.jobIsActive = true;
            obj.jobIsDeleted = false;
            obj.jobCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            obj.GetAllTeam();
            DataSet ds = obj.ds;

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (Convert.ToString(Session["Culture"]) == "Danish")
                    {

                        ddlTeam.Items.Clear();

                        ddlTeam.DataSource = ds.Tables[0];
                        ddlTeam.DataTextField = "TeamNameDN";
                        ddlTeam.DataValueField = "TeamID";
                        ddlTeam.DataBind();

                        ddlTeam.Items.Insert(0, new ListItem(hdnSelectTeam.Value, CommonModule.dropDownZeroValue));
                    }

                    else
                    {
                        ddlTeam.Items.Clear();

                        ddlTeam.DataSource = ds.Tables[0];
                        ddlTeam.DataTextField = "TeamName";
                        ddlTeam.DataValueField = "TeamID";
                        ddlTeam.DataBind();

                        ddlTeam.Items.Insert(0, new ListItem(hdnSelectTeam.Value, CommonModule.dropDownZeroValue));

                    }
                }
                else
                {
                    ddlTeam.Items.Clear();
                    ddlTeam.Items.Insert(0, new ListItem(hdnSelectTeam.Value, CommonModule.dropDownZeroValue));
                }
            }
            else
            {
                ddlTeam.Items.Clear();
                ddlTeam.Items.Insert(0, new ListItem(hdnSelectTeam.Value, CommonModule.dropDownZeroValue));
            }

        }
        #endregion

        #region WebMethod

        [WebMethod(EnableSession = true)]
        public static User[] Userdetails()
        {
            DataTable dt = new DataTable();
            List<User> Userdetails = new List<User>();

            UserBM obj = new UserBM();
            obj.userIsActive = true;
            obj.userIsDeleted = false;
            obj.userType = 2;
            obj.userCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
            obj.userId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
            if (obj.userId == 0)
            {

                return null;

            }
            else
            {
                obj.GetAllEmployeebymessge();
                DataSet ds = obj.ds;

                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dtrow in ds.Tables[0].Rows)
                    {

                        try
                        {
                            //logged in user should not visible in Chat box
                            if (obj.userId == Convert.ToInt32(dtrow["userId"]))
                            {
                                continue;
                            }
                        }
                        catch (Exception)
                        {
                            continue;
                        }

                        User User = new User();
                        User.name = dtrow["name"].ToString();
                        User.userId = dtrow["userId"].ToString();
                        User.Image = dtrow["userImage"].ToString();
                        User.notification = dtrow["notification"].ToString();
                        User.sessionimage = Convert.ToString(HttpContext.Current.Session["OrgUserImage"]);
                        Userdetails.Add(User);
                    }
                }
                return Userdetails.ToArray();
            }

        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string AddNewEmp(string email, string address, string mobile, string userdepId, string jobtype, string selectedValues, string Name, string LastName, string Language, string Team, string gender)
        {
            UserBM obj2 = new UserBM();
            obj2.useremailCheckDuplication(email, -1);
            String returnMsg = obj2.ReturnString;
            if (returnMsg == "")
            {
                UserBM obj = new UserBM();
                obj.userFirstName = Name;
                obj.userLastName = LastName;
                obj.userZip = "";
                obj.userAddress = address;
                obj.userCountryId = Convert.ToInt32(0);
                obj.userStateId = Convert.ToInt32(0);
                obj.userCityId = "";
                obj.usercontact = mobile;
                obj.userEmail = email.Trim();
                obj.userType = 2;
                obj.userCreatedDate = DateTime.Now;
                obj.userIsActive = true;
                obj.userIsDeleted = false;
                obj.userCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
                string pass = CommonModule.Generate_Random_password();
                obj.userPassword = CommonModule.encrypt(pass.Trim());
                obj.userGender = gender;
                obj.userImage = "ofile_img.png";
                obj.userLevel = 1;
                obj.userdepId = userdepId;
                obj.userJobType = Convert.ToInt32(jobtype);
                obj.userCreateBy = Convert.ToString(HttpContext.Current.Session["OrgUserId"]);
                // Get Cattegory list comma sap list. |Saurin | 20141216 |
                obj.UserCategory = selectedValues;

                obj.TeamID =Convert.ToInt32(Team);
                obj.InsertOrganisation();
                int UserId = 0;
                DataSet ds = obj.ds;

                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        UserId = Convert.ToInt32(ds.Tables[0].Rows[0]["userId"]);
                        Organisation_Organisation_dashboard ob = new Organisation_Organisation_dashboard();
                        ob.Updatelanguagemaster(UserId, Language);

                        if (!string.IsNullOrEmpty(selectedValues))
                        {
                            //string[] str = selectedValues.Split(',');
                            //if (str.Length > 0)
                            //{
                            //    foreach (var comId in str)
                            //    {

                            //        Int32 CompId = 0;
                            //        if (!string.IsNullOrEmpty(comId))
                            //        {
                            //            CompId = Convert.ToInt32(comId);
                            //        }

                            //        EmployeeSkillBM skillobj = new EmployeeSkillBM();
                            //        skillobj.skillComId = CompId;
                            //        skillobj.skillStatus = 3;
                            //        skillobj.skillUserId = Convert.ToInt32(ds.Tables[0].Rows[0]["userId"]);
                            //        skillobj.skillIsActive = true;
                            //        skillobj.skillIsDeleted = false;
                            //        skillobj.skillCreatedDate = DateTime.Now;
                            //        skillobj.skillCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
                            //        skillobj.skillLocal = 0;
                            //        skillobj.skillAchive = 0;
                            //        skillobj.skilltarget = 0;
                            //        skillobj.skillComment = string.Empty;
                            //        skillobj.InsertSkillMaster();
                            //    }


                            //}

                            var db = new startetkuEntities1();
                            db.InsertIntoSkillMaster(3, Convert.ToInt32(ds.Tables[0].Rows[0]["userId"]), true, false, DateTime.Now, Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]), 0, 0, 0, string.Empty, true, selectedValues);

                        }
                    }
                }




                //ddlcompetence.Items.Insert(0, new ListItem(CommonModule.dropDownSelectDepartments, CommonModule.dropDownZeroValue));



                if (obj.ReturnBoolean == true)
                {
                    Organisation_Organisation_dashboard x = new Organisation_Organisation_dashboard();
                    x.sendmail(email.Trim(), Name, pass, UserId);
                }
            }
            else
            {
                if (returnMsg == "userEmail")
                {
                    return CommonModule.msgEmailAlreadyExists; ;
                }
                return CommonModule.msgProblemInsertRecord;
            }

            //var category = string.Empty;
            //var aa = new List<string>();
            //foreach (RepeaterItem aItem in repCompCat.Items)
            //{
            //    var repCompAdd = (Repeater)aItem.FindControl("repCompAdd");
            //    foreach (RepeaterItem bItem in repCompAdd.Items)
            //    {
            //        CheckBoxList chkDisplayTitle = (CheckBoxList)bItem.FindControl("chkList");
            //        Label hdnChkboxValue = (Label)bItem.FindControl("hdnChkboxValue");

            //        foreach (ListItem item in chkDisplayTitle.Items)
            //        {
            //            if (item.Selected)
            //            {
            //                aa.Add(item.Value);
            //                //string selectedValue = item.Value;
            //            }
            //        }

            //    }

            //}
            //category = string.Join(",", aa.ToArray());


            return "success";

        }

        protected void Updatelanguagemaster(int OrgUserId, string Language)
        {
            try
            {


                UserBM obj = new UserBM();
                obj.languageUserId = Convert.ToInt32(OrgUserId);
                //String lag = Convert.ToString(Language);
                obj.languageName = Language;// ddlLanguage.SelectedItem.Text;
                try
                {
                    obj.languageCulture = ddlLanguage.SelectedItem.Value;

                }
                catch (Exception)
                {


                }
                if (Language == "English")
                {
                    obj.languageName = "English";
                    obj.languageCulture = "en-GB";


                }
                //else
                //{
                //    obj.languageName = "Danish";
                //    obj.languageCulture = "da-DK";


                //}
                //obj.userGender = ddlgender.SelectedValue;
                obj.Updatelanguagemaster();
            }
            catch (Exception ex)
            {

                ExceptionLogger.LogException(ex);
            }


        }

        public static string days { get; set; }
        public static string hours { get; set; }
        public static string and { get; set; }
        public static string minutes { get; set; }
        public static string ago { get; set; }

        private void GetResource()
        {
            try
            {


                days = GetLocalResourceObject("days.Text").ToString();
                hours = GetLocalResourceObject("hours.Text").ToString();
                and = GetLocalResourceObject("and.Text").ToString();

                minutes = GetLocalResourceObject("minutes.Text").ToString();
                ago = GetLocalResourceObject("ago.Text").ToString();
                //REJECTED = GetLocalResourceObject("REJECTED.Text").ToString();
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex, 0, "GetResource");
            }
        }

        [WebMethod(EnableSession = true)]
        public static User[] GetmessgeById(string userId)
        {
            DataTable dt = new DataTable();
            List<User> messge = new List<User>();

            NotificationBM obj = new NotificationBM();
            obj.mesfromUserId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
            obj.masToUserId = Convert.ToInt32(userId);
            obj.GetAllmessagebyId();
            DataSet ds = obj.ds;



            NotificationBM obj1 = new NotificationBM();
            obj1.notUserId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
            obj1.masStatus = false;
            obj1.UpdatemessagebyId_stauts();

            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dtrow in ds.Tables[0].Rows)
                {
                    User User = new User();
                    User.messubject = dtrow["messubject"].ToString();
                    User.mesfromUserId = dtrow["mesfromUserId"].ToString();
                    User.Image = dtrow["img1"].ToString();
                    //User.masCreatedDate = dtrow["masCreatedDate"].ToString();
                    User.masCreatedDate = dtrow["ago"].ToString();
                    User.masCreatedDate = User.masCreatedDate.Replace("days", days);
                    User.masCreatedDate = User.masCreatedDate.Replace("ago", ago);
                    User.masCreatedDate = User.masCreatedDate.Replace("minutes", minutes);
                    User.masCreatedDate = User.masCreatedDate.Replace("and", and);
                    User.masCreatedDate = User.masCreatedDate.Replace("hours", hours);
                    User.sessionUserId = Convert.ToString(HttpContext.Current.Session["OrgUserId"]);

                    messge.Add(User);
                }
            }
            return messge.ToArray();
        }
        [WebMethod(EnableSession = true)]
        public static string insertmessage(string message, string touserid)
        {
            string msg = string.Empty;
            NotificationBM obj = new NotificationBM();
            obj.mesfromUserId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
            obj.masToUserId = Convert.ToInt32(touserid);
            obj.messubject = message;
            obj.masIsActive = true;
            obj.masIsDeleted = false;
            obj.masCreatedDate = DateTime.Now;
            obj.masCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
            obj.mastype = "";
            if (obj.InsertMessage())
            {
                msg = "true";
            }
            else
            {
                msg = "false";
            }

            return msg;
        }
        [WebMethod(EnableSession = true)]
        public static User[] GetAllmessagebyId_stauts(string userId)
        {
            DataTable dt = new DataTable();
            List<User> messge = new List<User>();

            NotificationBM obj = new NotificationBM();
            obj.mesfromUserId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
            obj.masToUserId = Convert.ToInt32(userId);
            obj.masStatus = true;
            obj.GetAllmessagebyId_stauts();
            DataSet ds = obj.ds;

            NotificationBM obj1 = new NotificationBM();
            obj1.notUserId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
            obj1.masStatus = false;
            obj1.UpdatemessagebyId_stauts();

            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dtrow in ds.Tables[0].Rows)
                {
                    if (dtrow["mesfromUserId"].ToString() != Convert.ToString(HttpContext.Current.Session["OrgUserId"]))
                    {
                        User User = new User();
                        User.messubject = dtrow["messubject"].ToString();
                        User.mesfromUserId = dtrow["mesfromUserId"].ToString();
                        User.Image = dtrow["img1"].ToString();
                        User.masCreatedDate = dtrow["masCreatedDate"].ToString();
                        User.sessionUserId = Convert.ToString(HttpContext.Current.Session["OrgUserId"]);
                        messge.Add(User);
                    }
                }
            }
            return messge.ToArray();
        }
        public class User
        {
            public string name { get; set; }
            public string userId { get; set; }
            public string Image { get; set; }
            public string messubject { get; set; }
            public string masCreatedDate { get; set; }
            public string mesfromUserId { get; set; }
            public string sessionUserId { get; set; }
            public string notification { get; set; }
            public string sessionimage { get; set; }
        }
        #endregion

        #region repeater
        private DataTable GetAllCompetenceAddbyComCatId(int categoryId)
        {
            CompetenceMasterBM obj = new CompetenceMasterBM();
            obj.catIsActive = true;
            obj.catIsDeleted = false;
            if (categoryId < 0) return null;
            obj.comDivId = Convert.ToInt32(ddlDepartment.SelectedValue);
            obj.comJobtype = Convert.ToInt32(ddljobtype1.SelectedValue);
            obj.comCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            obj.GetAllCompetenceAddbyComCatId(categoryId);
            // lblDivIdName.Text = ddlDepartment.SelectedItem.Text;

            DataSet ds = obj.ds;

            if (ds != null)
            {
                int selectTable = 0;

                if (ds.Tables[selectTable].Rows.Count > 0)
                {
                    return ds.Tables[selectTable];

                }
                else
                {
                    //chkList.Items.Clear();
                    //ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownCategory, CommonModule.dropDownZeroValue));
                }

            }
            return null;
        }
        protected void Repeater1_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView drv = (DataRowView)e.Item.DataItem;
                int categoryID = Convert.ToInt32(drv["catId"]);
                Label compCategoryName = (Label)e.Item.FindControl("lblCompCategoryName");
                var categoryName = Convert.ToString(drv["catName"]);
                var categoryNameDN = Convert.ToString(drv["catNameDN"]);

                if (Convert.ToString(Session["Culture"]) == "Danish")
                {
                    compCategoryName.Text = categoryNameDN;

                }
                else
                {
                    compCategoryName.Text = categoryName;
                }
                Repeater Repeater2 = (Repeater)e.Item.FindControl("repCompAdd");

                Repeater2.DataSource = GetAllCompetenceAddbyComCatId(categoryID);
                Repeater2.DataBind();



                foreach (RepeaterItem bItem in Repeater2.Items)
                {
                    CheckBoxList chk = (CheckBoxList)bItem.FindControl("chkList");


                    var data = GetAllCompetenceAddbyComCatId(categoryID);
                    if (data == null) return;
                    DataSet ds = data.DataSet;
                    var catIds = string.Empty;

                    for (int intCount = 0; intCount < ds.Tables[0].Rows.Count; intCount++)
                    {

                        if (Convert.ToString(Session["Culture"]) == "Danish")
                        {
                            ds.Tables[0].Rows[intCount][3] += "  (" + categoryNameDN + ")";
                        }
                        else
                        {
                            ds.Tables[0].Rows[intCount][1] += "  (" + categoryName + ")";
                        }

                    }

                    ds.Tables[0].AcceptChanges();


                    /////////////////
                    if (Convert.ToInt32(ds.Tables[0].Rows[0]["catId"]) == categoryID)
                    {
                        chk.DataSource = data;
                        if (Convert.ToString(Session["Culture"]) == "Danish")
                        {
                            chk.DataTextField = "catNameDN";
                        }
                        else
                        {
                            chk.DataTextField = "catName";
                        }
                        chk.DataValueField = "comId";

                        chk.DataBind();
                        foreach (ListItem li in chk.Items)
                        {
                            if (string.IsNullOrWhiteSpace(catIds))
                            {
                            //    li.Selected = true;
                            }

                            else
                            {
                                foreach (var j in catIds.Split(','))
                                {

                                    if (li.Value == j)
                                    {
                                        li.Selected = true;
                                        break;
                                    }
                                    else
                                    {
                                        li.Selected = false;
                                    }
                                }

                            }




                        }

                        break;

                    }
                }
            }
        }

        protected void Repeater2_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    DataRowView drv = (DataRowView)e.Item.DataItem;
            //    int categoryID = Convert.ToInt32(drv["catId"]);
            //    CheckBoxList chk = (CheckBoxList)e.Item.FindControl("chkList");
            //    var data = GetAllCompetenceAddbyComCatId(categoryID);
            //    if (data == null) return;
            //    DataSet ds = data.DataSet;
            //    if (Convert.ToInt32(ds.Tables[0].Rows[0]["catId"]) == categoryID)
            //    {
            //        chk.DataSource = data;
            //        chk.DataTextField = "catName";
            //        chk.DataValueField = "catId";
            //        chk.DataBind();
            //    }
            //}
        }

        #endregion
    }
}