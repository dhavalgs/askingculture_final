﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true" CodeFile="OrgState.aspx.cs" Inherits="Organisation_OrgState" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg"></asp:Label>
    <div class="col-md-6">
        <div class="heading-sec">
            <h1>
                State <i><span runat="server" id="State"></span> </i>
            </h1>
        </div>
    </div>
    <br />
    <br />
    <div class="col-md-6 range ">
        <%--<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
            <i class="fa fa-calendar-o icon-calendar icon-large"></i>
            <span>August 5, 2014 - September 3, 2014</span>  <b class="caret"></b>
        </div>--%>
    </div>
    <div class="col-md-12">
        <div class="chat-widget widget-body" style="background: #fff;">
            <div class="chat-widget-head yellow">
                <h4>
                    Add State
                </h4>
            </div>
            <div class="indicatesRequireFiled">
                <i>* Indicates required field</i>
            </div>
            <div class="col-md-6">
                <div class="inline-form" >
                    <label class="col-md-2 control-label" style="width: 100%;">
                        Country Name:<span class="starValidation">*</span></label>
                    <asp:DropDownList ID="ddlcountry" runat="server" Style="width: 200px; height: 32px;
                        display: inline;">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlcountry"
                        ErrorMessage="Please select country." InitialValue="0" CssClass="commonerrormsg"
                        Display="Dynamic" ValidationGroup="chk"></asp:RequiredFieldValidator>
                </div>
                <div class="inline-form" style="width: 100%;">
                    <label class="col-md-2 control-label" style="width: 100%;">
                        State Name:<span class="starValidation">*</span></label>
                    <asp:TextBox runat="server" Text="" ID="txtstateName" MaxLength="50" CssClass="form-control" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtstateName"
                        ErrorMessage="Please enter first name." CssClass="commonerrormsg" Display="Dynamic"
                        ValidationGroup="chk"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="col-xs-12 profile_bottom">
                <asp:Button runat="server" ID="btnsubmit" Text="Save" CssClass="btn btn-success"
                    ValidationGroup="chk" OnClick="btnsubmit_click" />
                <asp:Button runat="server" ID="btnCancel" Text="Cancel" CssClass="btn" ValidationGroup="chk"
                    CausesValidation="false" OnClick="btnCancel_click" />
            </div>
        </div>
    </div>
</asp:Content>
