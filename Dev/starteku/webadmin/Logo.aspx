﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Logo.aspx.cs" MasterPageFile="~/webadmin/AdminMasterPage.master" Inherits="webadmin_Logo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .ProfileImage {
            width: 150px;
            height: 150px;
            border-radius: 100px;
            border: 1px solid blue;
        }
    </style>
   <link href="css/jquery-ui.css" rel="stylesheet" type="text/css" />
  
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
    <!-- Bootstrap -->
    <link rel="stylesheet" href="font-awesome-4.0.3/css/font-awesome.css" type="text/css" />
    <!-- Font Awesome -->
  <%--  <link rel="stylesheet" href="css/nv.css" type="text/css" />--%>
    <!-- VISITOR CHART -->
    <link rel="stylesheet" type="text/css" media="all" href="css/daterangepicker-bs3.css" />
    <!-- Date Range Picker -->
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <!-- Style -->
    <link rel="stylesheet" href="css/responsive.css" type="text/css" />
  
    <style type="text/css">
        .amaran.user img
        {
            height: 100%;
            max-width: 100%;
            background: white;
        }
    </style>
    <!-- Responsive -->
    <!-- Script -->
   

    <script src="../Scripts/sMain.js?version=1.0" type="text/javascript"></script>
    <script src="js/jquery.amaran.min.js"></script>
        <script src="../Scripts/notify.js"></script>
  <%--  <script src="http://hakanersu.github.io/AmaranJS/dist/js/jquery.amaran.js"></script>--%>
          <script src="../assets/js/jquery.noty.packaged.js"></script>
    <script>
       
            function generateTimer(type, text, layout, timeout) {
                var n = noty({
                    text: text,
                    type: type,
                    dismissQueue: true,
                    timeout: timeout,
                    closeWith: ['click'],
                    layout: layout,
                    theme: 'defaultTheme',
                    maxVisible: 20
                });
                console.log('html: ' + n.options.id);
            }
           
       
        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <%--<asp:Label runat="server" ID="lblMsg" CssClass="indicatesRequireFiled" meta:resourcekey="lblMsgResource1"></asp:Label>--%>
    
         <div class="row">
        <div class="col-md-12">
            <div class="col-md-12">
                <div class="indicatesRequireFiled">
                    *  <%--<%= CommonMessages.Indicatesrequiredfield%>--%>
                    <asp:Literal ID="Literal9" runat="server" meta:resourcekey="lblMsgResource1" enableviewstate="false"/>
                    <asp:HiddenField ID="hdnLogoId" runat="server"></asp:HiddenField>
                </div>
                <div class="widget box">
                 
                    <div class="widget-header">
                        <h4>
                            <i class="icon-reorder"></i>
                            <asp:Label runat="server" ID="lblDataDisplayTitle" Text="Competence" 
                                meta:resourcekey="lblDataDisplayTitleResource1"></asp:Label>
                        </h4>
                    </div>
                    <div class="widget-content">
                        <%--   <form class="form-horizontal row-border" action="#">--%>
                        <div class="form-horizontal row-border">

                             <div class="form-group">
                                <label class="col-md-2 control-label">
                                   <%-- <%= CommonMessages.CATEGORY%>--%>
                                    <asp:Literal ID="Literal1" runat="server" meta:resourcekey="lblLoginLogo" enableviewstate="false"/><span class="starValidation">*</span></label>
                                <div class="col-md-3">
                                    <asp:FileUpload ID="flupload1" runat="server" 
                            meta:resourcekey="flupload1Resource1" style="color: black; background-color: transparent;" CssClass="btn btn-primary yellow" onchange="readURL(this)">

                                    </asp:FileUpload>
                                    <%--<asp:RequiredFieldValidator CssClass="commonerrormsg" ID="reqFile1" ControlToValidate="flupload1" ValidationGroup="chk" meta:resourcekey="reqFile1Resource1" runat="server" ErrorMessage="Plese Select file."></asp:RequiredFieldValidator>--%>
                                    <asp:RegularExpressionValidator ID="reFile1" runat="server" ControlToValidate="flupload1"
                            CssClass="legend commonerrormsg" Display="Dynamic" ErrorMessage="Please upload only .gif | .jpeg | .jpg   | .png image."
                            ValidationExpression="^.*\.(JPG|jpg|jpeg|PNG|png|JPEG)$" ForeColor="Red"
                            ValidationGroup="chk" meta:resourcekey="reFile1Resource1"></asp:RegularExpressionValidator>
                                </div>
                                 <div class="col-md-6">
                                     <asp:Image ID="imgLoginLogo" style="height: 100px; width: 150px;" runat="server" ImageUrl="../images/strateku_centreret_fv.png"></asp:Image>
                                     <%--<img alt="" src="../images/strateku_centreret_fv.png" style="height: 100px; width: 150px;" id="imgLoginLogo" runat="server" />--%>
                                     </div>
                            </div>
                            

                           <div class="form-group">
                                <label class="col-md-2 control-label">
                                   <%-- <%= CommonMessages.CATEGORY%>--%>
                                    <asp:Literal ID="Literal2" runat="server" meta:resourcekey="lblPagelogo" enableviewstate="false"/><span class="starValidation">*</span></label>
                                <div class="col-md-3">
                                    <asp:FileUpload ID="flupload2" style="color: black; background-color: transparent;" CssClass="btn btn-primary yellow" runat="server" 
                            meta:resourcekey="flupload1Resource1" onchange="readURL(this)">

                                    </asp:FileUpload>
                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" CssClass="commonerrormsg" ControlToValidate="flupload2" ValidationGroup="chk" meta:resourcekey="reqFile1Resource1" runat="server" ErrorMessage="Plese Select file."></asp:RequiredFieldValidator>--%>
                                    <asp:RegularExpressionValidator ID="reFile2" runat="server" ControlToValidate="flupload2"
                            CssClass="legend commonerrormsg" Display="Dynamic" ErrorMessage="Please upload only .gif | .jpeg | .jpg   | .png image."
                            ValidationExpression="^.*\.(JPG|jpg|jpeg|PNG|png|JPEG)$" ForeColor="Red"
                            ValidationGroup="chk" meta:resourcekey="reFile2Resource1"></asp:RegularExpressionValidator>
                                </div>
                                <div class="col-md-6">
                                    <asp:Image ID="imgPageLogo" style="height: 100px; width: 150px;" runat="server"  ImageUrl="../images/strateku_centreret_fv.png"></asp:Image>
                                     <%--<img alt="" src="../images/strateku_centreret_fv.png" style="height: 100px; width: 150px;" id="imgPageLogo" runat="server" />--%>
                                     </div>
                            </div>
                           
                           <div class="form-group">
                                <label class="col-md-2 control-label">
                                   <%-- <%= CommonMessages.CATEGORY%>--%>
                                    <asp:Literal ID="Literal3" runat="server" meta:resourcekey="LblStystemnameResourse1" enableviewstate="false"/><span class="starValidation">*</span></label>
                                <div class="col-md-10">
                                    <asp:TextBox ID="txtSystemname" CssClass="form-control" runat="server" meta:resourcekey="txtSystemnameResourse1"></asp:TextBox>
                                    <asp:RequiredFieldValidator CssClass="commonerrormsg" ControlToValidate="txtSystemname" ValidationGroup="chk" ID="reqtxtSystemname" meta:resourcekey="reqtxtSystemnameResourse1" runat="server" ErrorMessage="Please Enter System Name."></asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">
                                   <%-- <%= CommonMessages.CATEGORY%>--%>
                                    <asp:Literal ID="Literal4" runat="server" meta:resourcekey="LblStystemnameDnResourse1" enableviewstate="false"/><span class="starValidation">*</span></label>
                                <div class="col-md-10">
                                    <asp:TextBox ID="txtSystemnameDN" CssClass="form-control" runat="server" meta:resourcekey="txtSystemnameDNResourse1"></asp:TextBox>
                                    <asp:RequiredFieldValidator CssClass="commonerrormsg" ControlToValidate="txtSystemnameDN" ValidationGroup="chk" ID="RequiredFieldValidator2" meta:resourcekey="reqtxtSystemnameDNResourse1" runat="server" ErrorMessage="Please Enter System Name."></asp:RequiredFieldValidator>
                                </div>
                            </div>
 <%--Submit buttons---------------------------------------------------------------------------------------%>
                           

                            <div class="form-horizontal row-border">
                                <div class="btn-toolbar">                                    
                                    <asp:Button runat="server" ID="btnsubmit" Text="Save" CssClass="btn btn-success"
                                         ValidationGroup="chk" OnClick="btnsubmit_Click" 
                                        meta:resourcekey="btnsubmitResource1"/>
                                    <asp:Button runat="server" ID="btnCancel" Text="Cancel" CssClass="btn" ValidationGroup="chk"
                                        CausesValidation="False" OnClick="btnCancel_Click" 
                                        meta:resourcekey="btnCancelResource1" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--=== no-padding ===-->
    <div class="row">
    </div>
    <div class="row">
    </div>
    <!-- /no-padding and table-tabletools -->
    <!--=== no-padding and table-colvis ===-->
    <div class="row">
    </div>
    <!-- /no-padding and table-colvis -->
    <!--=== Horizontal Scrolling ===-->
    <div class="row">
    </div>
   
 
    <script type="text/javascript">
     function checkDate(sender, args) {
         // debugger;
         //            if (sender._selectedDate = new Date()) {
         //                alert("You cannot select a current Date!");
         //                var date = new Date();
         //                date.setDate(date.getDate() - 1);
         //                sender._selectedDate = date;
         //                // set the date back to the current date
         //                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
         //            }
         //            else 
         if (sender._selectedDate > new Date()) {
             alert("You cannot select a future Date!");
             var date = new Date();
             date.setDate(date.getDate() - 2);
             sender._selectedDate = date;

             // set the date back to the current date
             sender._textbox.set_Value(sender._selectedDate.format(sender._format))
         }

     }
     function readURL(input) {

         if (input.files && input.files[0]) {
             var reader = new FileReader();

             reader.onload = function (e) {
                 var id = input.id;
                 if (id == "ContentPlaceHolder1_flupload1") {
                     $('#ContentPlaceHolder1_imgLoginLogo').attr('src', e.target.result);
                 }
                 if (id == "ContentPlaceHolder1_flupload2") {
                     $('#ContentPlaceHolder1_imgPageLogo').attr('src', e.target.result);
                 }


                 
             };

             reader.readAsDataURL(input.files[0]);
         }
     }
     function removeImg() {
         $('#ContentPlaceHolder1_img_profile').attr("src", '~/Organisation/images/ofile_img.png');
         $("#hdnIsRemoveImage").val('1');
     }
    </script>
    </asp:Content>