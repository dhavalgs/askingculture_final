﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Globalization;
using starteku_BusinessLogic;
using starteku_BusinessLogic.Model;
using System.Data;

public partial class webadmin_AdminMasterPage : System.Web.UI.MasterPage
{
    startetkuEntities1 db = new startetkuEntities1();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["UserName"])))
        {
            //spanWelComeMsg.InnerHtml = CommonModule.welcomMsg(Convert.ToString(Session["UserName"]));
            span1.InnerHtml = Convert.ToString(Session["UserName"]);
            Setlanguage();
        }

         LogoMaster logo = db.LogoMasters.FirstOrDefault();
            if (logo != null)
            {

                imgPageLogo.ImageUrl = "../Log/upload/Userimage/" + logo.PageLogo;
                ViewState["imgLoginLogo"] = logo.PageLogo;
            }
      
        
        
    }
    protected void Setlanguage()
    {
        try
        {
            string language = Convert.ToString(Session["Culture"]);
            //string language = "Denish";
            string languageId = "";
            //if (!string.IsNullOrEmpty(language))
            //{
            //    if (language.EndsWith("Denish")) languageId = "da-DK";
            //    else languageId = "en-GB";
            //    SetCulture(languageId);
            //}
            //ResourceLanguageBM obj = new ResourceLanguageBM();
            //DataSet resds = obj.GetAllResourceLanguage();
            //if (!string.IsNullOrEmpty(language))
            //{
            //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
            //    {
            //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
            //        {
            //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
            //        }
            //    }

            //    SetCulture(languageId);
            //}
            ResourceLanguageBM obj = new ResourceLanguageBM();
            if (!string.IsNullOrEmpty(language))
            {
                DataSet resds = obj.GetResourceLanguage(language);
                languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

                SetCulture(languageId);
            }


            if (Session["Language"] != null)
            {
                if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
            }
        }
        catch { }
    }
    protected void SetCulture(string languageId)
    {
        try
        {
            Session["Language"] = languageId;
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
        }
        catch { }
    }
}
