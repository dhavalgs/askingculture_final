﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/webadmin/AdminMasterPage.master" CodeFile="WebEmployeeList.aspx.cs" Inherits="webadmin_WebEmployeeList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" meta:resourcekey="lblMsgResource1"></asp:Label>
    <div class="row">
        <div class="col-md-12">
            <div class="widget box">
                <div class="widget-header">
                    <h4>
                        <i class="icon-reorder"></i><%-- <%= CommonMessages.Company%>--%>
                        <asp:Literal ID="Literal9" runat="server" meta:resourcekey="Manager" enableviewstate="false"/></h4>
                    <div class="toolbar no-padding">
                        <div class="btn-group">
                            <span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
                        </div>
                    </div>
                </div>
                <div class="widget-content no-padding">
                    <div class="btn-toolbar" style="text-align: right;">
                    </div>
                    <cc1:TabContainer ID="TabContainer1" runat="server" CssClass="fancy fancy-green"
                        ActiveTaEvalex="0" meta:resourcekey="TabContainer1Resource1">
                        <cc1:TabPanel ID="tbpnluser" runat="server" meta:resourcekey="tbpnluserResource1">
                            <HeaderTemplate>
                               <%-- <%= CommonMessages.Company%>--%>
                                <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Company" enableviewstate="false"/>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:GridView ID="gvGrid" runat="server" AutoGenerateColumns="False" CellPadding="0"
                                    Width="100%" GridLines="None" DataKeyNames="userId" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                                     EmptyDataText="No Record Found" OnRowCommand="gvGrid_RowCommand"
                                    meta:resourcekey="gvGridResource1">
                                    <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sr.No" meta:resourcekey="TemplateFieldResource1">
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                        </asp:TemplateField>
                                      <%--  <asp:TemplateField HeaderText="company Name" meta:resourcekey="TemplateFieldResource2">
                                            <ItemTemplate>
                                                <asp:Label ID="lblcname" runat="server" Text='<%# Eval("comname") %>' meta:resourcekey="lblcnameResource1"></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="User Name" meta:resourcekey="TemplateFieldResource3">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUNamer" runat="server" Text='<%# Eval("name") %>' meta:resourcekey="lblUNamerResource1"></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Email" meta:resourcekey="TemplateFieldResource4">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUserNamer" runat="server" Text='<%# Eval("userEmail") %>' meta:resourcekey="lblUserNamerResource1"></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Contact" meta:resourcekey="TemplateFieldResource5">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUserNamer1" runat="server" Text='<%# Eval("comphone") %>' meta:resourcekey="lblUserNamer1Resource1"></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        </asp:TemplateField>

                                          

                                        <asp:TemplateField HeaderText="Action" meta:resourcekey="TemplateFieldResource6">
                                            <ItemTemplate>
                                                
                                               <%-- <span class="btn-group"><a href="<%# String.Format("Registration.aspx?emp={0}", Eval("userId")) %>"
                                                    class="bs-tooltip" title="Edit" class="btn btn-xs"><i class="icon-pencil"></i></a>--%>
                                                    <asp:LinkButton ID="lnkBtnName" runat="server" CommandName="archive" CommandArgument='<%# Eval("userId") %>'
                                                        CssClass="icon-trash" ToolTip="Archive" OnClientClick="return confirm('Are you sure you want to archive this record?');"
                                                        meta:resourcekey="lnkBtnNameResource1"></asp:LinkButton>
                                                </span>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="10%" />
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                    <HeaderStyle CssClass="aa" />
                                </asp:GridView>
                            </ContentTemplate>
                        </cc1:TabPanel>
                        <cc1:TabPanel ID="tbpnlusrdetails" runat="server" meta:resourcekey="tbpnlusrdetailsResource1">
                            <HeaderTemplate>
                              <%-- <%= CommonMessages.Archive%>--%>
                               <asp:Literal ID="Literal2" runat="server" meta:resourcekey="Archive" enableviewstate="false"/>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:GridView ID="gvArchive" runat="server" AutoGenerateColumns="False" CellPadding="0"
                                    Width="100%" GridLines="None"   EmptyDataText="No Record Found"
                                    DataKeyNames="userId" OnRowCommand="gvArchive_RowCommand" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                                    meta:resourcekey="gvArchiveResource1">
                                    <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sr.No" meta:resourcekey="TemplateFieldResource7">
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                        </asp:TemplateField>
                                       <%-- <asp:TemplateField HeaderText="company Name" meta:resourcekey="TemplateFieldResource8">
                                            <ItemTemplate>
                                                <asp:Label ID="lblcname" runat="server" Text='<%#Eval("comname")%>' ></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Name" meta:resourcekey="TemplateFieldResource9">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUNamer" runat="server" Text='<%# Eval("name")%>' ></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Email" meta:resourcekey="TemplateFieldResource10">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUserNamer" runat="server" Text='<%# Eval("userEmail") %>' ></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        </asp:TemplateField>
                                         

                                        <asp:TemplateField HeaderText="Action" meta:resourcekey="TemplateFieldResource11">
                                            <ItemTemplate>
                                                <span class="btn-group">
                                                   
                                                    <asp:LinkButton ID="lnkbtnrestore" runat="server" CommandName="restore" CommandArgument='<%# Eval("userId") %>'
                                                        ToolTip="Restore" OnClientClick="return confirm('Are you sure you want to restore this record?');"
                                                        Text="Restore" meta:resourcekey="lnkbtnrestoreResource1"></asp:LinkButton>
                                                    &#160;|&#160;
                                                    <asp:LinkButton ID="lnkBtnPermanentlydelete" runat="server" CommandName="permanentlydelete"
                                                        CommandArgument='<%# Eval("userId") %>' Text="Delete Permanently" ToolTip="Delete Permanently"
                                                        OnClientClick="return confirm('Are you sure you want to permanently delete this record?');"
                                                        meta:resourcekey="lnkBtnPermanentlydeleteResource1"></asp:LinkButton>
                                                </span>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="10%" />
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                    <HeaderStyle CssClass="aa" />
                                </asp:GridView>
                            </ContentTemplate>
                        </cc1:TabPanel>
                    </cc1:TabContainer>
                </div>
            </div>
            <div class="btn-toolbar" style="text-align: right;">
                <div>

                    <%--<a href='managerlist.aspx?ut=2&ucId=<%#Request.QueryString["ucId"]%>' class="btn btn-success" id="managerlist">Manager List</a>--%>

                    <asp:Button runat="server" ID="btnAdd"  Text="Manager List"
                        CssClass=" btn btn-success" meta:resourcekey="btnAddResource1" OnClick="btnAdd_Click" />
                </div>
            </div>
        </div>
    </div>
    <!-- /no-padding -->
    <!--=== no-padding and table-tabletools ===-->
    <div class="row">
    </div>
    <!-- /no-padding and table-tabletools -->
    <!--=== no-padding and table-colvis ===-->
    <div class="row">
    </div>
    <!-- /no-padding and table-colvis -->
    <!--=== Horizontal Scrolling ===-->
    <div class="row">
    </div>
    <!-- /Normal -->
    <!-- /Page Content -->
    <script>
        $(document).ready(function () {
            alert(Request.QueryString["ucId"]);
        });
        
    </script>
</asp:Content>