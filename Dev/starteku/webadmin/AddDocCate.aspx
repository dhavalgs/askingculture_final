﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webadmin/AdminMasterPage.master" AutoEventWireup="true" CodeFile="AddDocCate.aspx.cs" Inherits="webadmin_AddDocCate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" meta:resourcekey="lblMsgResource1"></asp:Label>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-12">
                <div class="indicatesRequireFiled">
                    * <asp:Literal ID="Literal5" runat="server" meta:resourcekey="Indicatesrequiredfield" EnableViewState="false" />
                 
                </div>
                <div class="widget box">
                    <div class="widget-header">
                        <h4>
                            <i class="icon-reorder"></i>
                            <asp:Label runat="server" ID="lblDataDisplayTitle" Text="Division" meta:resourcekey="lblDataDisplayTitleResource1"></asp:Label>
                        </h4>
                    </div>
                    <div class="widget-content">
                        <div class="form-horizontal row-border">
                            <div class="form-group" style="display: none;">
                                <label class="col-md-2 control-label">
                                    Division:<span class="starValidation"></span></label>
                                <div class="col-md-10">
                                    <asp:DropDownList ID="ddlDepartment" runat="server" Style="width: 100px; height: 32px;
                                        display: inline;" Visible="False" meta:resourcekey="ddlDepartmentResource1">
                                    </asp:DropDownList>
                                    <asp:TreeView ID="TreeView1" runat="server" ShowCheckBoxes="All" onclick="client_OnTreeNodeChecked(event)"
                                        OnTreeNodePopulate="TreeView_TreeNodePopulate" Style="cursor: pointer" ShowLines="True"
                                        CollapseImageUrl="~/images/index.jpg" ExpandImageUrl="~/images/index2.jpg" LineImagesFolder="~/TreeLineImages"
                                        meta:resourcekey="TreeView1Resource1" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">
                                    Division <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Name" EnableViewState="false" />:<span class="starValidation">*</span></label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="txtDepartmentName" MaxLength="50" CssClass="form-control"
                                        meta:resourcekey="txtDepartmentNameResource1" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtDepartmentName"
                                        ErrorMessage="Please enter Department name." CssClass="commonerrormsg" Display="Dynamic"
                                        ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator5Resource1"></asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">
                                   <asp:Literal ID="Literal4" runat="server" meta:resourcekey="NameDN" EnableViewState="false" />:<span class="starValidation">*</span></label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="txtDepartmentNameDN" MaxLength="50" CssClass="form-control"
                                        meta:resourcekey="txtDepartmentNameDNResource1" />
                                    <asp:RequiredFieldValidator ID="txtDepartmentNameDNValidator1" runat="server" ControlToValidate="txtDepartmentNameDN"
                                        ErrorMessage="Please enter Department name." CssClass="commonerrormsg" Display="Dynamic"
                                        ValidationGroup="chk" meta:resourcekey="txtDepartmentNameDNValidator1Resource1"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <%--internal style--%>
                            <style type="text/css">
                                .mycheckbox input[type="checkbox"]
                                {
                                    margin-right: 5px;
                                }
                            </style>
                            <div class="form-group">
                                <label class="col-md-2 control-label">
                                  <asp:Literal ID="Literal2" runat="server" meta:resourcekey="CATEGORY" EnableViewState="false" />:<span class="starValidation">*</span></label>
                                <div class="col-md-10" style="overflow: auto; border: 1px solid #ccc; max-height: 200px;
                                    width: 68%; margin-left: 13px;">
                                    <asp:CheckBoxList Width="100%" ID="chkList" runat="server" CssClass="FormText mycheckbox"
                                        RepeatColumns="1" Datafield="description" DataValueField="value" meta:resourcekey="chkListResource1">
                                    </asp:CheckBoxList>
                                    <asp:CustomValidator ID="CustomValidator1" ErrorMessage="Please select at least one item."
                                        ForeColor="Red" ClientValidationFunction="ValidateCheckBoxList" runat="server"
                                        ValidationGroup="chk" meta:resourcekey="CustomValidator1Resource1" />
                                </div>
                                <label class="col-md-4 control-label" style="color:red;">
                                   <asp:Literal ID="Literal6" runat="server" meta:resourcekey="chkvalidator" EnableViewState="false" Visible="false"/></label>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">
                                    <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Description" EnableViewState="false" />:<%--<span class="starValidation">*</span>--%></label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="txtDESCRIPTION" MaxLength="500" CssClass="form-control"
                                        TextMode="MultiLine" Rows="5" meta:resourcekey="txtDESCRIPTIONResource1" />
                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDESCRIPTION"
                                        ErrorMessage="Please Enter DESCRIPTION." CssClass="commonerrormsg" Display="Dynamic"
                                        ValidationGroup="chk"></asp:RequiredFieldValidator>--%>
                                </div>
                            </div>
                            <div class="form-horizontal row-border">
                                <div class="btn-toolbar">
                                    <asp:Button runat="server" ID="btnsubmit" Text="Save" CssClass="btn btn-success"
                                        ValidationGroup="chk" OnClick="btnsubmit_click" meta:resourcekey="btnsubmitResource1" />
                                    <asp:Button runat="server" ID="btnCancel" Text="Cancel" CssClass="btn" ValidationGroup="chk"
                                        CausesValidation="False" OnClick="btnCancel_click" meta:resourcekey="btnCancelResource1" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--=== no-padding ===-->
    <div class="row">
    </div>
    <div class="row">
    </div>
    <!-- /no-padding and table-tabletools -->
    <!--=== no-padding and table-colvis ===-->
    <div class="row">
    </div>
    <!-- /no-padding and table-colvis -->
    <!--=== Horizontal Scrolling ===-->
    <div class="row">
    </div>
    <!-- /Normal -->
    <!-- /Page Content -->
    <script>
        function client_OnTreeNodeChecked(event) {

            var treeNode = event.srcElement || event.target;
            if (treeNode.tagName == "INPUT" && treeNode.type == "checkbox") {
                if (treeNode.checked) {
                    uncheckOthers(treeNode.id);
                }
            }
        }

        function uncheckOthers(id) {
            var elements = document.getElementsByTagName('input');
            // loop through all input elements in form
            for (var i = 0; i < elements.length; i++) {
                if (elements.item(i).type == "checkbox") {
                    if (elements.item(i).id != id) {
                        elements.item(i).checked = false;
                    }
                }
            }
        }
    </script>
    <script>
        $(document).ready(function () {
            "use strict";

            App.init(); // Init layout and core plugins
            Plugins.init(); // Init all plugins
            FormComponents.init(); // Init all form-specific plugins
        });
    </script>

</asp:Content>

