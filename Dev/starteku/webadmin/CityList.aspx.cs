﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;


public partial class webadmin_CityList : System.Web.UI.Page
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["UserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            GetAllcity();
            citySelectArchiveAll();
            SetDefaultMessage();
        }
    }
    #endregion

    #region method
    protected void SetDefaultMessage()
    {
        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["msg"])))
        {
            if (Convert.ToString(Request.QueryString["msg"]) == "del")
            {
                lblMsg.Text = CommonModule.msgRecordHasBeenDeletedSuccessfully;
                lblMsg.ForeColor = System.Drawing.Color.Green;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "ins")
            {
                lblMsg.Text = CommonModule.msgRecordInsertedSuccessfully;
                lblMsg.ForeColor = System.Drawing.Color.Green;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "upd")
            {
                lblMsg.Text = CommonModule.msgRecordUpdatedSuccss;
                lblMsg.ForeColor = System.Drawing.Color.Green;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "arc")
            {
                lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                lblMsg.ForeColor = System.Drawing.Color.Green;
            }
        }

    }
    protected void GetAllcity()
    {
        CityBM obj = new CityBM();
        obj.citIsActive = true;
        obj.citIsDeleted = false;
        obj.citCompanyId = 0;
        obj.GetAllcity();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvGrid.DataSource = ds.Tables[0];
                gvGrid.DataBind();

                gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvGrid.DataSource = null;
                gvGrid.DataBind();
            }
        }
        else
        {
            gvGrid.DataSource = null;
            gvGrid.DataBind();
        }
    }
    protected void citySelectArchiveAll()
    {
        CityBM obj = new CityBM();
        obj.citIsActive = false;
        obj.citIsDeleted = false;
        obj.citCompanyId = 0;
        obj.GetAllcity();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvArchive.DataSource = ds.Tables[0];
                gvArchive.DataBind();

                gvArchive.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvArchive.DataSource = null;
                gvArchive.DataBind();
            }
        }
        else
        {
            gvArchive.DataSource = null;
            gvArchive.DataBind();
        }
    }
    #endregion

    #region grid Event
    protected void gvGrid_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "archive")
        {
            CityBM obj = new CityBM();
            obj.citId = Convert.ToInt32(e.CommandArgument);
            obj.citIsActive = false;
            obj.citIsDeleted = false;
            obj.CityStatusUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    GetAllcity();
                    citySelectArchiveAll();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.Green;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }
    }

    protected void gvArchive_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "archive")
        {
            CityBM obj = new CityBM();
            obj.citId = Convert.ToInt32(e.CommandArgument);
            obj.citIsActive = true;
            obj.citIsDeleted = false;
            obj.CityStatusUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {

                    GetAllcity();
                    citySelectArchiveAll();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.Green;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }
        else if (e.CommandName == "permanentlydelete")
        {
            CityBM obj = new CityBM();
            obj.citId = Convert.ToInt32(e.CommandArgument);
            obj.citIsActive = false;
            obj.citIsDeleted = true;
            obj.CityStatusUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    GetAllcity();
                    citySelectArchiveAll();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.Green;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }
    }

    #endregion

}