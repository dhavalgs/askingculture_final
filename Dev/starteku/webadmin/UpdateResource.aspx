﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webadmin/AdminMasterPage.master"
    AutoEventWireup="true" CodeFile="UpdateResource.aspx.cs" Inherits="webadmin_UpdateResource" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg"></asp:Label>
    <div class="row">
        <div class="col-md-12">
            <div class="widget box">
                <div class="widget-header">
                    <h4>
                        <i class="icon-reorder"></i>Update Resource</h4>
                    <div class="toolbar no-padding">
                        <div class="btn-group">
                            <span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
                        </div>
                    </div>
                </div>
                <div class="widget-content no-padding">
                    <div class="btn-toolbar" style="text-align: right;">
                    </div>
                    <%-- <asp:DropDownList ID="cmbResources" runat="server" Width="275px" AutoPostBack="true"
                        OnSelectedIndexChanged="cmbResources_SelectedIndexChanged">
                    </asp:DropDownList>--%>
                    <asp:DropDownList ID="localResources" runat="server" Width="275px" AutoPostBack="true"
                        OnSelectedIndexChanged="localResources_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:DropDownList ID="webResources" runat="server" Width="275px" AutoPostBack="true"
                        OnSelectedIndexChanged="webResources_SelectedIndexChanged">
                    </asp:DropDownList>
                     <asp:DropDownList ID="language" runat="server" Width="275px" AutoPostBack="true"
                        OnSelectedIndexChanged="language_SelectedIndexChanged">
                    </asp:DropDownList>
                  <%--  <asp:Button ID="btnAddNewFile" runat="server" Text="Add new file" OnClick="btnAddNewFile_Click"></asp:Button>
                    <asp:Button ID="btnGetValues" runat="server" Text="Get Data" OnClick="btnGetValues_Click"></asp:Button>--%>
                </div>
                <br />
                <br />
                <%--     <asp:DataGrid ID="gridView1" runat="server" AutoGenerateColumns="False" CellPadding="4"
                    CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                    GridLines="None">
                    <HeaderStyle CssClass="aa" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="No">
                            <ItemTemplate>
                                <%= ++indexNum %>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                            <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Key">
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container,"DataItem.Key") %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Value">
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container,"DataItem.Value") %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Edit">
                            <ItemTemplate>
                                <a href='Eeditresource.aspx?key=<%# DataBinder.Eval(Container,"DataItem.Key") %>&file=<%=cmbResources.SelectedItem.Text %>&id=<%=indexNum - 1 %>'>
                                    Edit</a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <%-- <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <SelectedItemStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                    <AlternatingItemStyle BackColor="White" />
                    <ItemStyle BackColor="#FFFBD6" ForeColor="#333333" Font-Size="Small" Font-Names="verdana" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                </asp:DataGrid>--%>
                <div class="form-horizontal row-border" style="display:none;">
                <div class="btn-toolbar">
                    <asp:Panel ID="pnlAddnew" runat="server"  Visible="false">
                      <div class="form-group">
                                <label class="col-md-1 control-label">
                                    <%-- <%= CommonMessages.Country%>--%>
                                     <asp:Literal ID="Literal1" runat="server"  enableviewstate="false"/>Key</label>
                                <div class="col-md-2">
                                    <asp:TextBox runat="server" ID="txtKey" CssClass="form-control" ValidationGroup="addnewRes" />
                                   
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtKey"
                                        ErrorMessage="Please Enter Field." CssClass="commonerrormsg" Display="Dynamic"
                                        ValidationGroup="addnewRes"></asp:RequiredFieldValidator>
                                </div>
                             <label class="col-md-1 control-label">
                                    <%-- <%= CommonMessages.Country%>--%>
                                     <asp:Literal ID="Literal2" runat="server"  enableviewstate="false"/>English Value</label>
                                <div class="col-md-2">
                                    <asp:TextBox runat="server" ID="txtEngValue" CssClass="form-control" ValidationGroup="addnewRes" />
                                   
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEngValue"
                                        ErrorMessage="Please Enter Field." CssClass="commonerrormsg" Display="Dynamic"
                                        ValidationGroup="addnewRes"></asp:RequiredFieldValidator>
                                </div>
                             <label class="col-md-1 control-label">
                                    <%-- <%= CommonMessages.Country%>--%>
                                     <asp:Literal ID="Literal4" runat="server"  enableviewstate="false"/>Danish Value</label>
                                <div class="col-md-2">
                                    <asp:TextBox runat="server" ID="txtDnValue" CssClass="form-control" ValidationGroup="addnewRes" />
                                   
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtDnValue"
                                        ErrorMessage="Please Enter Field." CssClass="commonerrormsg" Display="Dynamic"
                                        ValidationGroup="addnewRes"></asp:RequiredFieldValidator>
                                </div>

                          <label class="col-md-1 control-label">
                                    <%-- <%= CommonMessages.Country%>--%>
                                     <asp:Literal ID="lblLanguageSelect" runat="server"  enableviewstate="false"/></label>
                                <div class="col-md-2">
                                    <asp:TextBox runat="server" ID="txtLanguage" CssClass="form-control" ValidationGroup="addnewRes" />
                                   
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtLanguage"
                                        ErrorMessage="Please Enter Field." CssClass="commonerrormsg" Display="Dynamic"
                                        ValidationGroup="addnewRes"></asp:RequiredFieldValidator>
                                </div>

                          <div class="col-md-2">
                     <asp:Button runat="server" ID="AddnewResource" Text="Add New" CssClass="btn btn-success"
                        ValidationGroup="addnewRes" OnClick="AddnewResource_Click" style="float: right;"/>
                             </div>

                            </div>
                     </asp:Panel>
                    
                   
                </div>
            </div>
                <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" CellPadding="4"
                    CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                    GridLines="None">
                    <HeaderStyle CssClass="aa" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="No">
                            <ItemTemplate>
                                <%= ++indexNum %>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                            <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Key">
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container,"DataItem.Key") %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Value">
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container,"DataItem.Value") %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Edit">
                            <ItemTemplate>
                                <a href='Eeditresource.aspx?key=<%# DataBinder.Eval(Container,"DataItem.Key") %>&file=<%=localResources.SelectedItem.Text %>&id=<%=indexNum - 1 %>&Lid=<%=localResources.SelectedIndex %>'>
                                    Edit</a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <%--<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <SelectedItemStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                    <AlternatingItemStyle BackColor="White" />
                    <ItemStyle BackColor="#FFFBD6" ForeColor="#333333" Font-Size="Small" Font-Names="verdana" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />--%>
                </asp:DataGrid>
                <asp:DataGrid ID="DataGrid2" runat="server" AutoGenerateColumns="False" CellPadding="4"
                    CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                    GridLines="None">
                    <HeaderStyle CssClass="aa" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="No">
                            <ItemTemplate>
                                <%= ++indexNum %>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                            <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Key">
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container,"DataItem.Key") %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Value">
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container,"DataItem.Value") %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Edit">
                            <ItemTemplate>
                                <a href='Eeditresource.aspx?key=<%# DataBinder.Eval(Container,"DataItem.Key") %>&file=<%=webResources.SelectedItem.Text %>&id=<%=indexNum - 1 %>&Bid=<%=webResources.SelectedIndex %>'>
                                    Edit</a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <%--<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <SelectedItemStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                    <AlternatingItemStyle BackColor="White" />
                    <ItemStyle BackColor="#FFFBD6" ForeColor="#333333" Font-Size="Small" Font-Names="verdana" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />--%>
                </asp:DataGrid>
                <asp:DataGrid ID="DataGrid32" runat="server" AutoGenerateColumns="False" CellPadding="4"
                    CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                    GridLines="None">
                    <HeaderStyle CssClass="aa" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="No">
                            <ItemTemplate>
                                <%= ++indexNum %>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                            <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Key">
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container,"DataItem.Key") %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="English">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtValue" TextMode="MultiLine" CssClass="textBoxEnabled" runat="server"
                                    Text='<%# Bind("Value") %>' Width="100%" Height="150px" meta:resourcekey="txtValueResource1"></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container,"DataItem.ValueEN") %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Danish">
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container,"DataItem.ValueDA") %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Edit">
                            <ItemTemplate>
                                <a href='Eeditresource.aspx?key=<%# DataBinder.Eval(Container,"DataItem.Key") %>&file=<%=webResources.SelectedItem.Text %>&id=<%=indexNum - 1 %>&Bid=<%=webResources.SelectedIndex %>'>
                                    Edit</a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <%--<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <SelectedItemStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                    <AlternatingItemStyle BackColor="White" />
                    <ItemStyle BackColor="#FFFBD6" ForeColor="#333333" Font-Size="Small" Font-Names="verdana" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />--%>
                </asp:DataGrid>
                <asp:HiddenField ID="lbllanguageSelected" runat="server"/>
                <asp:GridView ID="DataGrid3" runat="server" AutoGenerateColumns="False" Width="100%"
                    OnRowCommand="gvGrid_RowCommand" GridLines="None" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                    EmptyDataText="No Record Found" >
                    <Columns>
                        <asp:TemplateField HeaderText="Key">
                            <ItemTemplate>
                                <asp:Label ID="lblKey" runat="server" Text='<%# Bind("Key") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="gridText tenXPadding bottomBorder rightBorder" Wrap="True" VerticalAlign="Top"
                                Width="10%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ENGLISH">
                            <HeaderStyle />
                            <ItemTemplate>
                                <asp:TextBox ID="txtValueEN" runat="server" Width="100%" Text='<%# Bind("ValueEN") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                      
                         <asp:TemplateField HeaderText="Danish"> 
                            <HeaderStyle />
                            <ItemTemplate>
                                <asp:TextBox ID="txtValueDA" runat="server" Width="100%" Text='<%# Bind("ValueDA") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--  <asp:CommandField ButtonType="Button" ItemStyle-CssClass="tenXPadding bottomBorder"
                    ShowEditButton="True" ControlStyle-CssClass="buttonEnabled" ItemStyle-VerticalAlign="Top"
                    HeaderStyle-BorderColor="White" HeaderStyle-BorderWidth="0" HeaderStyle-BorderStyle="None"
                    ItemStyle-Width="20%" >
                    <ControlStyle CssClass="buttonEnabled"></ControlStyle>
                    <HeaderStyle BorderColor="White" BorderWidth="0px" BorderStyle="None"></HeaderStyle>
                    <ItemStyle VerticalAlign="Top" CssClass="tenXPadding bottomBorder" Width="20%"></ItemStyle>
                </asp:CommandField>--%>
                    </Columns>
                </asp:GridView>
                <br />
            </div>
            <div class="form-horizontal row-border">
                <div class="btn-toolbar" style="float: right;">
                    <asp:Button runat="server" ID="btnsubmit" Text="Save" CssClass="btn btn-success"
                        ValidationGroup="chk" OnClick="btnsubmit_click" Visible="False" />
                    <asp:Button runat="server" ID="btnCancel" Text="Cancel" CssClass="btn" CausesValidation="False"
                        OnClick="btnCancel_click" />
                </div>
            </div>

            

        </div>
    </div>
    <!-- /no-padding -->
    <!--=== no-padding and table-tabletools ===-->
    <div class="row">
    </div>
    <!-- /no-padding and table-tabletools -->
    <!--=== no-padding and table-colvis ===-->
    <div class="row">
    </div>
    <!-- /no-padding and table-colvis -->
    <!--=== Horizontal Scrolling ===-->
    <div class="row">
    </div>
    <!-- /Normal -->
    <!-- /Page Content -->
</asp:Content>
