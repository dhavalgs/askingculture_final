﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic.Model;
using System.IO;
using System.Resources;
using System.Collections;

public partial class webadmin_AddLanguage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["UserId"])))
        {
            Response.Redirect("login.aspx");
        }
        var db = new startetkuEntities1();
        //List<string> list = new List<string>();
        //foreach (System.Globalization.CultureInfo ci in System.Globalization.CultureInfo.GetCultures(System.Globalization.CultureTypes.AllCultures))
        //{
        //    string uiculture = "no one";
        //    try { uiculture = System.Globalization.CultureInfo.CreateSpecificCulture(ci.Name).Name; }
        //    catch { }
        //    list.Add("<tr><td>" + ci.Name + "</td><td>" + ci.EnglishName +"</td><td>" + uiculture + "</td><td>"+ci.DateTimeFormat+"</td></tr>");
        //    Language_Master lm = new Language_Master();
        //     lm.lng_CreateDate = DateTime.Now;
        //     lm.lng_Culture = uiculture;
        //     lm.lng_DateTimeFormat = "dd/MM/yyyy";
        //     lm.lng_Extension = "." + ci.Name;
        //     lm.lng_IsActive = true;
        //     lm.lng_Name = ci.EnglishName;
        //     db.Language_Master.Add(lm);
        //     db.SaveChanges();
        //}

        //list.Sort();
        //Response.Write("<table>");
        //Response.Write("<tr><th>Culture</th><th>English Name</th><th>Ui Culture</th><th>Format</th></tr>");
        //foreach (string str in list)
        //    Response.Write(str);
        //Response.Write("</table>");
        if (!IsPostBack)
        {
            List<Int32?> LngId = db.ResourceLanguages.Select(x => x.resRefLangId).ToList();
            ddlLanguage.DataSource = db.Language_Master.Where(x => !LngId.Contains(x.lng_ID) && x.lng_IsActive == true).ToList().OrderBy(x => x.lng_Name);
            ddlLanguage.DataTextField = "lng_Name";
            ddlLanguage.DataValueField = "lng_ID";
            ddlLanguage.DataBind();
            ddlLanguage.Items.Insert(0, new ListItem("--Select Language--", ""));

            var LangData = db.Language_Master.Where(o => o.lng_Name != "English" && o.lng_IsActive == true).ToList();
            gvLanguage.DataSource = LangData;
            gvLanguage.DataBind();

           
        }
        var SysLangData = db.ResourceLanguages.ToList();
        gvSystemLang.DataSource = SysLangData;
        gvSystemLang.DataBind();
        
    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        var db=new startetkuEntities1();
        var MasterLangid = Convert.ToInt32(ddlLanguage.SelectedValue);
        try
        {
            if (MasterLangid != 0)
            {
                var data = db.Language_Master.Where(x => x.lng_ID == MasterLangid).FirstOrDefault();
                if (data != null)
                {
                    List<string> strList = GetFiles();
                    List<string> strAdminFile = GetFilesAdmin();
                    if (strList.Count > 0)
                    {
                        foreach (var i in strList)
                        {
                            string strFiles = i.Replace(".resx","");
                            string filename1 = Request.PhysicalApplicationPath + "\\Organisation\\App_LocalResources\\"+strFiles+".resx";
                            Stream stream = new FileStream(filename1, FileMode.Open, FileAccess.Read, FileShare.Read);
                            ResXResourceReader RrX = new ResXResourceReader(stream);
                            IDictionaryEnumerator RrEn = RrX.GetEnumerator();
                            List<ResourceView> slist = new List<ResourceView>();
                            while (RrEn.MoveNext())
                            {
                                if (!String.IsNullOrEmpty(Convert.ToString(RrEn.Value)))
                                {
                                    slist.Add(GetResourceDataView(RrEn.Key, null, RrEn.Value));
                                    // slist.Add(RrEn.Key, RrEn.Value);
                                }
                            }

                            RrX.Close();
                            stream.Dispose();
                            if (slist != null)
                            {
                                
                                string filename = Request.PhysicalApplicationPath + "\\Organisation\\App_LocalResources\\" + strFiles+data.lng_Extension+".resx";
                                if (!File.Exists(filename))
                                {
                                    ResXResourceWriter rw = new ResXResourceWriter(filename);
                                    foreach (var ii in slist)
                                    {
                                        rw.AddResource(Convert.ToString(ii.Key), Convert.ToString(ii.ValueEN));
                                    }
                                    rw.Close();
                                }
                            }
                        }
                        
                    }
                    if (strAdminFile.Count > 0)
                    {
                        foreach (var i in strAdminFile)
                        {
                            string strFiles = i.Replace(".resx", "");
                            string filename1 = Request.PhysicalApplicationPath + "\\webadmin\\App_LocalResources\\" + strFiles + ".resx";
                            Stream stream = new FileStream(filename1, FileMode.Open, FileAccess.Read, FileShare.Read);
                            ResXResourceReader RrX = new ResXResourceReader(stream);
                            IDictionaryEnumerator RrEn = RrX.GetEnumerator();
                            List<ResourceView> slist = new List<ResourceView>();
                            while (RrEn.MoveNext())
                            {
                                if (!String.IsNullOrEmpty(Convert.ToString(RrEn.Value)))
                                {
                                    slist.Add(GetResourceDataView(RrEn.Key, null, RrEn.Value));
                                    // slist.Add(RrEn.Key, RrEn.Value);
                                }
                            }

                            RrX.Close();
                            stream.Dispose();
                            if (slist != null)
                            {

                                string filename = Request.PhysicalApplicationPath + "\\webadmin\\App_LocalResources\\" + strFiles + data.lng_Extension + ".resx";
                                if (!File.Exists(filename))
                                {
                                    ResXResourceWriter rw = new ResXResourceWriter(filename);
                                    foreach (var ii in slist)
                                    {
                                        rw.AddResource(Convert.ToString(ii.Key), Convert.ToString(ii.ValueEN));
                                    }
                                    rw.Close();
                                }
                            }
                        }

                    }
                    ResourceLanguage rl = new ResourceLanguage();
                    rl.resRefLangId = data.lng_ID;
                    rl.resLanguage = data.lng_Name;
                    rl.resCulture = data.lng_Culture;
                    rl.resDateFormat = data.lng_DateTimeFormat;
                    rl.resExtension = data.lng_Extension;
                    rl.resIsActive = true;
                    db.ResourceLanguages.Add(rl);
                    db.SaveChanges();
                    gvLanguage.DataBind();
                    gvSystemLang.DataBind();
                }
                
            }

            lblMsg.ForeColor = System.Drawing.Color.Green;
            lblMsg.Text = "Record inserted successfully.";
        }
        catch
        {
            lblMsg.ForeColor = System.Drawing.Color.Red;
            lblMsg.Text = "Problem in inserting record.";
        }
        var SysLangData = db.ResourceLanguages.ToList();
        gvSystemLang.DataSource = SysLangData;
        gvSystemLang.DataBind();
        
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Dashboard.aspx");
    }
    public class ResourceView
    {
        public object Key { get; set; }

        public object ValueEN { get; set; }

        public object ValueDA { get; set; }


        public string Contact { get; set; }
    }
    private ResourceView GetResourceDataView(object key, object ValueDA, object ValueEN)
    {
        var view = new ResourceView();
        view.Key = key;
        view.ValueDA = ValueDA;
        view.ValueEN = ValueEN;
        // view.ValueLang = ValueLang;
        return view;
    }
    private List<string>  GetFiles()
    {

        string path = Server.MapPath("~/Organisation/App_LocalResources");
        DirectoryInfo d = new DirectoryInfo(path);//Assuming Test is your Folder
        var Files = d.GetFiles("*.resx").Where(x => x.Name.Contains(".aspx.resx") || x.Name.Contains(".master.resx")).Select(x => x.Name).ToList();

        return Files;
    }

    private List<string> GetFilesAdmin()
    {

        string path = Server.MapPath("~/webadmin/App_LocalResources");
        DirectoryInfo d = new DirectoryInfo(path);//Assuming Test is your Folder
        var Files = d.GetFiles("*.resx").Where(x => x.Name.Contains(".aspx.resx") || x.Name.Contains(".master.resx")).Select(x => x.Name).ToList();

        return Files;
    }

    protected void gvSystemLang_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        var db = new startetkuEntities1();
        int id =Convert.ToInt32( e.CommandArgument);
        if (e.CommandName == "active")
        {
            var data = db.ResourceLanguages.Find(id);
            data.resIsActive = true;
            db.SaveChanges();
            GridViewRow row = (e.CommandSource as LinkButton).NamingContainer as GridViewRow;
            LinkButton lblKey = row.FindControl("lnkbtnActive") as LinkButton;
            lblKey.Visible = false;
            LinkButton lblKey1 = row.FindControl("lnkbtnInActive") as LinkButton;
            lblKey1.Visible = true;
            var SysLangData = db.ResourceLanguages.ToList();
            gvSystemLang.DataSource = SysLangData;
            gvSystemLang.DataBind();
        }
        else if (e.CommandName == "inactive")
        {
            var data = db.ResourceLanguages.Find(id);
            data.resIsActive = false;
            db.SaveChanges();
            GridViewRow row = (e.CommandSource as LinkButton).NamingContainer as GridViewRow;
            LinkButton lblKey = row.FindControl("lnkbtnActive") as LinkButton;
            lblKey.Visible = true;
            LinkButton lblKey1 = row.FindControl("lnkbtnInActive") as LinkButton;
            lblKey1.Visible = false;

        }
        else if(e.CommandName=="Edit")
        {
            var SysLangData = db.ResourceLanguages.ToList();
            gvSystemLang.DataSource = SysLangData;
            gvSystemLang.DataBind();
        }
    }

    protected void gvSystemLang_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Label lblEng = e.Row.FindControl("lblName") as Label;
        Label lblstatus = e.Row.FindControl("lblStatus") as Label;
        

         if (lblstatus != null && lblEng!=null && lblEng.Text!="English")
         {
             LinkButton lblKey = e.Row.FindControl("lnkbtnActive") as LinkButton;
             LinkButton lblKey1 = e.Row.FindControl("lnkbtnInActive") as LinkButton;
             if (lblstatus.Text == "False")
                 lblKey.Visible = true;
             else
                 lblKey1.Visible = true;
         }
    }
    protected void gvSystemLang_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvSystemLang.EditIndex = e.NewEditIndex;
        gvSystemLang.DataBind();
    }
    protected void gvSystemLang_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var db = new startetkuEntities1();
        GridViewRow row = (GridViewRow)gvSystemLang.Rows[e.RowIndex];
        string ddlDate = (row.FindControl("ddlDateFormat") as DropDownList).SelectedValue;
        Label lblID = row.FindControl("lblResID") as Label;
       
        int id=Convert.ToInt32( lblID.Text);
        var data = db.ResourceLanguages.Find(id);
        data.resDateFormat = ddlDate;
        db.SaveChanges();
        gvSystemLang.EditIndex = -1;
        var SysLangData = db.ResourceLanguages.ToList();
        gvSystemLang.DataSource = SysLangData;
        gvSystemLang.DataBind();
    }
    protected void gvSystemLang_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var db = new startetkuEntities1();
        var SysLangData = db.ResourceLanguages.ToList();
        gvSystemLang.DataSource = SysLangData;
        gvSystemLang.DataBind();
        gvSystemLang.EditIndex = -1;
        gvSystemLang.DataBind();
    }
}