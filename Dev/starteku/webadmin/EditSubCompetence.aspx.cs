﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using startetku.Business.Logic;

public partial class webadmin_EditSubCompetence : System.Web.UI.Page
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["UserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            if (!String.IsNullOrEmpty(Request.QueryString["sub"]))
            {
                GetAllsubCompetenceMasterbyid();
            }
            else
            {
                divCompetence.Visible = false;
            }
        }
    }
    #endregion

    #region method
    protected void SetDefaultMessage()
    {
        string id = "?id=";
        string childurl = "child.aspx";
        if (Request.RawUrl.ToString().ToLower().Contains(childurl.ToLower()))
        {
            if (Request.RawUrl.ToString().ToLower().Contains(id.ToLower()))
            {
                lblDataDisplayTitle.Text = "Edit Children";

            }
            else
            {
                lblDataDisplayTitle.Text = "Add Children";
            }
        }
    }
    protected void InsertSubCompetence()
    {
        SubCompetenceBM obj = new SubCompetenceBM();
        obj.subCompetence = txtcountryName.Text;
        obj.subDescription = txtDescription.Text;
        obj.subUserId = Convert.ToInt32(Session["UserId"]);
        obj.subComId = Convert.ToInt32(Request.QueryString["id"]);
        obj.subIsActive = true;
        obj.subIsDeleted = false;
        obj.subCreatedDate = DateTime.Now;
        obj.subCreateBy = 0;
        obj.InsertSubCompetence();
        if (obj.ReturnBoolean == true)
        {
            Response.Redirect("SubCompetence.aspx?id="+Request.QueryString["id"]);
        }

        else
        {
            lblMsg.Text = CommonModule.msgProblemInsertRecord;
        }

    }
    protected void UpdateSubCompetence()
    {
        //CompetenceMasterBM obj = new CompetenceMasterBM();
        //obj.comId = Convert.ToInt32(Request.QueryString["id"]);
        //obj.comCompetence = txtcountryName.Text;
        //obj.comUpdatedDate = DateTime.Now;
        //obj.UpdateCompetenceMaster();
        SubCompetenceBM obj = new SubCompetenceBM();
        obj.subId = Convert.ToInt32(Request.QueryString["sub"]);
        obj.subCompetence = txtcountryName.Text;
        obj.subUpdatedDate = DateTime.Now;
        obj.subDescription = txtDescription.Text;
        obj.UpdateSubCompetence();
        if (obj.ReturnBoolean == true)
        {
            Response.Redirect("SubCompetence.aspx?id=" + Request.QueryString["id"]);
        }

        else
        {
            lblMsg.Text = CommonModule.msgProblemInsertRecord;
        }
    }
    protected void GetAllsubCompetenceMasterbyid()
    {
        SubCompetenceBM obj = new SubCompetenceBM();
        obj.subId = Convert.ToInt32(Request.QueryString["sub"]);
        obj.GetAllsubCompetenceMasterbyid();
        DataSet ds = obj.ds;
        if (ds != null)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["comCompetence"])))
                txtcountryName.Text = Convert.ToString(ds.Tables[0].Rows[0]["comCompetence"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["subDescription"])))
                txtDescription.Text = Convert.ToString(ds.Tables[0].Rows[0]["subDescription"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["comCompetence"])))
                lblCompetence.Text = Convert.ToString(ds.Tables[0].Rows[0]["comCompetence"]);
        }

    }

    #endregion

    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Request.QueryString["sub"]))
        {
            UpdateSubCompetence();
        }
        else
        {
            InsertSubCompetence();
        }
    }
    protected void btnCancel_click(object sender, EventArgs e)
    {
        Response.Redirect("SubCompetence.aspx?id=" + Request.QueryString["id"]);
    }
    #endregion
}