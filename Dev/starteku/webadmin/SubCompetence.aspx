﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webadmin/AdminMasterPage.master"
    AutoEventWireup="true" CodeFile="SubCompetence.aspx.cs" Inherits="webadmin_SubCompetence" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg"></asp:Label>
    <div class="row">
        <div class="col-md-12">
            <div class="widget box">
                <div class="widget-header">
                    <h4>
                        <i class="icon-reorder"></i>Sub Competence</h4>
                    <div class="toolbar no-padding">
                        <div class="btn-group">
                            <span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
                        </div>
                    </div>
                </div>
                <div class="widget-content no-padding">
                    <div class="btn-toolbar" style="text-align: right;">
                    </div>
                    <cc1:TabContainer ID="TabContainer1" runat="server" CssClass="fancy fancy-green">
                        <cc1:TabPanel ID="tbpnluser" runat="server">
                            <HeaderTemplate>
                                Sub Competence
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:GridView ID="gvGrid" runat="server" AutoGenerateColumns="False" CellPadding="0"
                                    Width="100%" GridLines="None" DataKeyNames="subId" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                                    EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>' OnRowCommand="gvGrid_RowCommand">
                                    <HeaderStyle CssClass="aa" />
                                    <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                    <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sr.No">
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Competence">
                                            <ItemTemplate>
                                                <asp:Label ID="lblcomCompetence" runat="server" Text="<%# bind('comCompetence') %>"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="sub Competence">
                                            <ItemTemplate>
                                                <asp:Label ID="lblsubCompetence" runat="server" Text="<%# bind('subCompetence') %>"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="50px">
                                            <ItemTemplate>
                                                <span class="btn-group">
                                                <a href='EditSubCompetence.aspx?sub=<%# Eval("subId")%>&id=<%# Eval("subComId")%>'
                                                    class="bs-tooltip" title="Edit" class="btn btn-xs"><i class="icon-pencil"></i></a>
                                                    <asp:LinkButton ID="lnkBtnName" runat="server" CommandName="archive" CommandArgument='<%# Eval("subId") %>'
                                                        CssClass="icon-trash" ToolTip="Archive" OnClientClick="return confirm('Are you sure you want to archive this record?');"></asp:LinkButton></span>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="10%" />
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>
                        </cc1:TabPanel>
                        <cc1:TabPanel ID="tbpnlusrdetails" runat="server">
                            <HeaderTemplate>
                                Archive
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:GridView ID="gvArchive" runat="server" AutoGenerateColumns="False" CellPadding="0"
                                    CellSpacing="0" Width="100%" GridLines="none" EmptyDataText='<%#CommonModule.msgGridRecordNotfound %>'
                                    DataKeyNames="subId" OnRowCommand="gvArchive_RowCommand" CssClass="table table-striped table-bordered table-hover table-checkable datatable">
                                    <HeaderStyle CssClass="aa" />
                                    <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                    <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sr.No" ItemStyle-Width="50px">
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Competence">
                                            <ItemTemplate>
                                                <asp:Label ID="lblcomCompetence" runat="server" Text="<%# bind('comCompetence') %>"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="sub Competence">
                                            <ItemTemplate>
                                                <asp:Label ID="lblsubCompetence" runat="server" Text="<%# bind('subCompetence') %>"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="50px">
                                            <ItemTemplate>
                                                <span class="btn-group">
                                                    <asp:LinkButton ID="lnkbtnrestore" runat="server" CommandName="archive" CommandArgument='<%# Eval("subId") %>'
                                                        ToolTip="Restore" OnClientClick="return confirm('Are you sure you want to restore this record?');"
                                                        Text="Restore"></asp:LinkButton>&nbsp;|&nbsp;
                                                    <asp:LinkButton ID="lnkBtnPermanentlydelete" runat="server" CommandName="permanentlydelete"
                                                        CommandArgument='<%# Eval("subId") %>' Text="Delete Permanently" ToolTip="Delete Permanently"
                                                        OnClientClick="return confirm('Are you sure you want to permanently delete this record?');"></asp:LinkButton>
                                                </span>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>
                        </cc1:TabPanel>
                    </cc1:TabContainer>
                </div>
            </div>
            <div class="btn-toolbar" style="text-align: right;">
                <div>
                    <asp:Button runat="server" ID="btnAdd" OnClick="btnsave_click"
                        Text="Add Sub Competence" CssClass=" btn btn-success" />
                    
                </div>
            </div>
        </div>
    </div>
    <!-- /no-padding -->
    <!--=== no-padding and table-tabletools ===-->
    <div class="row">
    </div>
    <!-- /no-padding and table-tabletools -->
    <!--=== no-padding and table-colvis ===-->
    <div class="row">
    </div>
    <!-- /no-padding and table-colvis -->
    <!--=== Horizontal Scrolling ===-->
    <div class="row">
    </div>
    <!-- /Normal -->
    <!-- /Page Content -->
    
</asp:Content>
