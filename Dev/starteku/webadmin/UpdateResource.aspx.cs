﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Globalization;
using System.IO;
using System.Resources;
using System.Xml;
using System.Xml.Linq;
using starteku_BusinessLogic;
using System.Reflection;

public partial class webadmin_UpdateResource : System.Web.UI.Page
{
    public int indexNum = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (String.IsNullOrEmpty(Convert.ToString(Session["UserId"])))
        //{
        //    Response.Redirect("login.aspx");
        //  }
        if (!IsPostBack)
        {
            LoadData();

            if (!String.IsNullOrEmpty(Request.QueryString["Bid"]))
            {
                webResources.SelectedIndex = Convert.ToInt32(Request.QueryString["Bid"]);
                webResources_load();
            }
            else if (!String.IsNullOrEmpty(Request.QueryString["Lid"]))
            {
                localResources.SelectedIndex = Convert.ToInt32(Request.QueryString["Bid"]);
                localResources_load();
            }


        }
    }

    //protected void cmbResources_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    if (cmbResources.SelectedIndex != 0)
    //    {
    //        string filename = Request.PhysicalApplicationPath + "App_GlobalResources\\" + cmbResources.SelectedItem.Text;
    //        Stream stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
    //        ResXResourceReader RrX = new ResXResourceReader(stream);
    //        IDictionaryEnumerator RrEn = RrX.GetEnumerator();
    //        SortedList slist = new SortedList();
    //        while (RrEn.MoveNext())
    //        {
    //            slist.Add(RrEn.Key, RrEn.Value);
    //        }
    //        RrX.Close();
    //        stream.Dispose();
    //        gridView1.DataSource = slist;
    //        gridView1.DataBind();
    //        DataGrid1.DataSource = null;
    //        DataGrid1.DataBind();
    //        DataGrid2.DataSource = null;
    //        DataGrid2.DataBind();
    //    }
    //}
    protected void localResources_SelectedIndexChanged(object sender, EventArgs e)
    {

        btnsubmit.Visible = true;
        pnlAddnew.Visible = true;
        txtEngValue.Text = "";
        txtDnValue.Text = "";
        txtKey.Text = "";
        webResources.SelectedIndex = 0;
        if (localResources.SelectedIndex != 0)
        {
            /*var file = localResources.SelectedItem.Text + @".aspx.resx";*/


            string file = string.Empty;
            if (localResources.SelectedItem.Text == "Employee")
            {
                file = localResources.SelectedItem.Text + @".master.resx";
            }
            else if (localResources.SelectedItem.Text.Contains("Master"))
            {
                file = localResources.SelectedItem.Text + @".master.resx"; ;
            }
            else
            {
                file = localResources.SelectedItem.Text + @".aspx.resx";
            }

            string filename = Request.PhysicalApplicationPath + "\\Organisation\\App_LocalResources\\" + file;
            Stream stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
            ResXResourceReader RrX = new ResXResourceReader(stream);
            IDictionaryEnumerator RrEn = RrX.GetEnumerator();
            List<ResourceView> slist = new List<ResourceView>();
            while (RrEn.MoveNext())
            {
                if (!String.IsNullOrEmpty(Convert.ToString(RrEn.Value)))
                {
                    slist.Add(GetResourceDataView(RrEn.Key, null, RrEn.Value));
                    // slist.Add(RrEn.Key, RrEn.Value);
               }
            }
            RrX.Close();
            stream.Dispose();

            //
            var s = "";
            if (localResources.SelectedItem.Text == "Employee")
            {
              //  s = file.Replace(".master", ".master.da");
                s = file.Replace(".master", ".master"+language.SelectedItem.Value);
            }
            else if (localResources.SelectedItem.Text.Contains("Master"))
            {

                //s = file.Replace(".master", ".master.da");
                s = file.Replace(".master", ".master"+language.SelectedItem.Value);
            }
            else
            {
                s = file.Replace(".aspx", ".aspx"+language.SelectedItem.Value);
            }

            string filename2 = Request.PhysicalApplicationPath + "\\Organisation\\App_LocalResources\\" + s;
            Stream stream2 = new FileStream(filename2, FileMode.Open, FileAccess.Read, FileShare.Read);
            ResXResourceReader RrX2 = new ResXResourceReader(stream2);
            IDictionaryEnumerator RrEn2 = RrX2.GetEnumerator();

            List<ResourceView> slist2 = new List<ResourceView>();
            while (RrEn2.MoveNext())
            {
               // if (!String.IsNullOrEmpty(Convert.ToString(RrEn2.Value)))
                //{
                    slist2.Add(GetResourceDataView(RrEn2.Key, RrEn2.Value, null));
                    // slist.Add(RrEn.Key, RrEn.Value);
                //}
            }
            //
            RrX2.Close();
            stream2.Dispose();
            //gridView1.DataSource = null;
            //gridView1.DataBind();

            var dem = from o in slist
                      join i in slist2 on o.Key equals i.Key
                      into res
                      from res1 in res.DefaultIfEmpty()
                      select new ResourceView
                      {
                          Key = o.Key,
                          ValueDA =res1==null?" ":res1.ValueDA,
                          ValueEN = o.ValueEN
                      };
            List<ResourceView> slist3 = new List<ResourceView>();
            /*foreach (var k in dem.ToList())
            {
                slist3.Add(GetResourceDataView(dem, RrEn2.Value, null));
            }*/
            DataGrid3.Columns[2].HeaderText = language.SelectedItem.Text;
            DataGrid3.DataSource = dem;
            DataGrid3.DataBind();
        }
        return;
        webResources.SelectedIndex = 0;
        if (localResources.SelectedIndex != 0)
        {
            string filename = Request.PhysicalApplicationPath + "\\Organisation\\App_LocalResources\\" + localResources.SelectedItem.Text;
            Stream stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
            ResXResourceReader RrX = new ResXResourceReader(stream);
            IDictionaryEnumerator RrEn = RrX.GetEnumerator();
            SortedList slist = new SortedList();
            while (RrEn.MoveNext())
            {
                if (!String.IsNullOrEmpty(Convert.ToString(RrEn.Value)))
                {
                    slist.Add(RrEn.Key, RrEn.Value);
                }
            }
            RrX.Close();
            stream.Dispose();
            //gridView1.DataSource = null;
            //gridView1.DataBind();
            DataGrid2.DataSource = null;
            DataGrid2.DataBind();
            DataGrid1.DataSource = slist;
            DataGrid1.DataBind();
        }
    }

    protected void webResources_SelectedIndexChanged(object sender, EventArgs e)
    {
        btnsubmit.Visible = true;
        pnlAddnew.Visible = true;
        localResources.SelectedIndex = 0;
        if (webResources.SelectedIndex != 0)
        {
            string file = string.Empty;
            if (webResources.SelectedItem.Text.Contains("Master"))
            {
                file = webResources.SelectedItem.Text + @".master.resx"; ;
            }
            else
            {
                file = webResources.SelectedItem.Text + @".aspx.resx";
            }


            string filename = Request.PhysicalApplicationPath + "\\webadmin\\App_LocalResources\\" + file;
            Stream stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
            ResXResourceReader RrX = new ResXResourceReader(stream);
            IDictionaryEnumerator RrEn = RrX.GetEnumerator();




            List<ResourceView> slist = new List<ResourceView>();
            while (RrEn.MoveNext())
            {
                if (!String.IsNullOrEmpty(Convert.ToString(RrEn.Value)))
                {
                    slist.Add(GetResourceDataView(RrEn.Key, null, RrEn.Value));
                    // slist.Add(RrEn.Key, RrEn.Value);
                }
            }
            RrX.Close();
            stream.Dispose();



            //
            //var s = file.Replace(".aspx", ".aspx.da");
            var s = file.Replace(".aspx", ".aspx"+language.SelectedItem.Value);
            if (webResources.SelectedItem.Text.Contains("Master"))
            {

                s = file.Replace(".master", ".master"+language.SelectedItem.Value);
            }
            else
            {
                s = file.Replace(".aspx", ".aspx"+language.SelectedItem.Value);
            }


            string filename2 = Request.PhysicalApplicationPath + "\\webadmin\\App_LocalResources\\" + s;
            Stream stream2 = new FileStream(filename2, FileMode.Open, FileAccess.Read, FileShare.Read);
            ResXResourceReader RrX2 = new ResXResourceReader(stream2);
            IDictionaryEnumerator RrEn2 = RrX2.GetEnumerator();

            List<ResourceView> slist2 = new List<ResourceView>();
            while (RrEn2.MoveNext())
            {
                if (!String.IsNullOrEmpty(Convert.ToString(RrEn2.Value)))
                {
                    slist2.Add(GetResourceDataView(RrEn2.Key, RrEn2.Value, null));
                    // slist.Add(RrEn.Key, RrEn.Value);
                }
            }
            //
            RrX2.Close();
            stream2.Dispose();
            //gridView1.DataSource = null;
            //gridView1.DataBind();

            var dem = from o in slist
                      join i in slist2 on o.Key equals i.Key
                      select new ResourceView
                      {
                          Key = o.Key,
                          ValueDA = i.ValueDA,
                          ValueEN = o.ValueEN
                      };
            List<ResourceView> slist3 = new List<ResourceView>();
            /*foreach (var k in dem.ToList())
            {
                slist3.Add(GetResourceDataView(dem, RrEn2.Value, null));
            }*/

            DataGrid3.DataSource = dem;
            DataGrid3.DataBind();
        }

        return;
        localResources.SelectedIndex = 0;
        if (webResources.SelectedIndex != 0)
        {
            string filename = Request.PhysicalApplicationPath + "\\webadmin\\App_LocalResources\\" + webResources.SelectedItem.Text;
            Stream stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
            ResXResourceReader RrX = new ResXResourceReader(stream);
            IDictionaryEnumerator RrEn = RrX.GetEnumerator();
            SortedList slist = new SortedList();
            while (RrEn.MoveNext())
            {
                if (!String.IsNullOrEmpty(Convert.ToString(RrEn.Value)))
                {
                    slist.Add(RrEn.Key, RrEn.Value);
                }
            }
            RrX.Close();
            stream.Dispose();
            //gridView1.DataSource = null;
            //gridView1.DataBind();
            DataGrid1.DataSource = null;
            DataGrid1.DataBind();
            DataGrid2.DataSource = slist;
            DataGrid2.DataBind();
        }
    }

    protected void language_SelectedIndexChanged(object sender, EventArgs e)
    {
        btnsubmit.Visible = true;
        pnlAddnew.Visible = true;
        txtEngValue.Text = "";
        //txtDnValue.Text = "";
        lblLanguageSelect.Text = language.SelectedItem.Text;
        lbllanguageSelected.Value = language.SelectedItem.Text;
        txtLanguage.Text = "";
        txtKey.Text = "";
        webResources.SelectedIndex = 0;
        if (localResources.SelectedIndex != 0)
        {
            /*var file = localResources.SelectedItem.Text + @".aspx.resx";*/


            string file = string.Empty;
            if (localResources.SelectedItem.Text == "Employee")
            {
                file = localResources.SelectedItem.Text + @".master.resx";
            }
            else if (localResources.SelectedItem.Text.Contains("Master"))
            {
                file = localResources.SelectedItem.Text + @".master.resx"; ;
            }
            else
            {
                file = localResources.SelectedItem.Text + @".aspx.resx";
            }

            string filename = Request.PhysicalApplicationPath + "\\Organisation\\App_LocalResources\\" + file;
            Stream stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
            ResXResourceReader RrX = new ResXResourceReader(stream);
            IDictionaryEnumerator RrEn = RrX.GetEnumerator();
            List<ResourceView> slist = new List<ResourceView>();
            while (RrEn.MoveNext())
            {
                if (!String.IsNullOrEmpty(Convert.ToString(RrEn.Value)))
                {
                    slist.Add(GetResourceDataView(RrEn.Key, null, RrEn.Value));
                    // slist.Add(RrEn.Key, RrEn.Value);
                }
            }
            RrX.Close();
            stream.Dispose();

            //
            var s = "";
            if (localResources.SelectedItem.Text == "Employee")
            {
                //s = file.Replace(".master", ".master.da");
                s = file.Replace(".master", ".master" + language.SelectedItem.Value);
            }
            else if (localResources.SelectedItem.Text.Contains("Master"))
            {

               // s = file.Replace(".master", ".master.da");
                s = file.Replace(".master", ".master"+language.SelectedItem.Value);
            }
            else
            {
               // s = file.Replace(".aspx", ".aspx.da");
                s = file.Replace(".aspx", ".aspx"+language.SelectedItem.Value);
            }

            string filename3 = Request.PhysicalApplicationPath + "\\Organisation\\App_LocalResources\\" + s;
            Stream stream3 = new FileStream(filename3, FileMode.Open, FileAccess.Read, FileShare.Read);
            ResXResourceReader RrX3 = new ResXResourceReader(stream3);
            IDictionaryEnumerator RrEn3 = RrX3.GetEnumerator();

            List<ResourceView> slist2 = new List<ResourceView>();
            while (RrEn3.MoveNext())
            {
                //if (!String.IsNullOrEmpty(Convert.ToString(RrEn2.Value)))
                //{
                slist2.Add(GetResourceDataView(RrEn3.Key, RrEn3.Value, null));
                    // slist.Add(RrEn.Key, RrEn.Value);
                //}
            }
            //
            RrX3.Close();
            stream3.Dispose();
            //gridView1.DataSource = null;
            //gridView1.DataBind();

            var dem = from o in slist
                      join i in slist2 on o.Key equals i.Key
                      into res from res1 in res.DefaultIfEmpty()
                      select new ResourceView
                      {
                          Key = o.Key,
                          ValueEN = o.ValueEN,
                          ValueDA = res1==null?" ":res1.ValueDA
                      };

            
          //  var dem=slist.GroupJoin(slist2,p=>p.Key,c=>c.Key,(p,c)=>new{p,c}).SelectMany(p=>p.p
            List<ResourceView> slist3 = new List<ResourceView>();
            /*foreach (var k in dem.ToList())
            {
                slist3.Add(GetResourceDataView(dem, RrEn2.Value, null));
            }*/
            DataGrid3.Columns[2].HeaderText = language.SelectedItem.Text;
            DataGrid3.DataSource = dem;
            DataGrid3.DataBind();

           
        }
        return;
        webResources.SelectedIndex = 0;
        if (localResources.SelectedIndex != 0)
        {
            string filename = Request.PhysicalApplicationPath + "\\Organisation\\App_LocalResources\\" + localResources.SelectedItem.Text;
            Stream stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
            ResXResourceReader RrX = new ResXResourceReader(stream);
            IDictionaryEnumerator RrEn = RrX.GetEnumerator();
            SortedList slist = new SortedList();
            while (RrEn.MoveNext())
            {
                if (!String.IsNullOrEmpty(Convert.ToString(RrEn.Value)))
                {
                    slist.Add(RrEn.Key, RrEn.Value);
                }
            }
            RrX.Close();
            stream.Dispose();
            //gridView1.DataSource = null;
            //gridView1.DataBind();
            DataGrid2.DataSource = null;
            DataGrid2.DataBind();
            DataGrid1.DataSource = slist;
            DataGrid1.DataBind();
        }
    }


    protected void ResultGridView_RowCommand(object sender, GridViewEditEventArgs e)
    {

        DataGrid3.EditIndex = e.NewEditIndex;

        LoadData();

    }

    protected void gvResourceEditor_RowUpdating(object sender, GridViewRowEventArgs gridViewRowEventArgs)
    {/*
        string theFileName = Server.MapPath("App_GlobalResources/CommonResource.da.resx");
        Label lblKey = DataGrid3.Rows[e.RowIndex].FindControl("lblKey") as Label;
        TextBox txtValue = DataGrid3.Rows[e.RowIndex].FindControl("txtValue") as TextBox;
        TextBox txtComment = DataGrid3.Rows[e.RowIndex].FindControl("txtComment") as TextBox;
        //Loading the Resource file as an XML.
        XDocument xDoc = XDocument.Load(theFileName);
        //Filtering out key which matches currently updated record.
        var elementToBeEdited = from xle in xDoc.Element("root").Elements("data") where xle.Attribute("name").Value == lblKey.Text select xle;

        if (elementToBeEdited != null && elementToBeEdited.Count() > 0)
        {
            //Updating the new value
            elementToBeEdited.First().SetElementValue("value", HttpUtility.HtmlDecode(txtValue.Text));

            //Checking if there is a comment node, if no then adding a new comment node.
            if (elementToBeEdited.First().Element("comment") == null && !string.IsNullOrEmpty(txtComment.Text))
            {
                elementToBeEdited.First().Add(new XElement("comment", HttpUtility.HtmlDecode(txtComment.Text)));
            }
            else if (elementToBeEdited.First().Element("comment") != null && !string.IsNullOrEmpty(txtComment.Text))
            {
                //If comment node is present then updating the comment node.
                elementToBeEdited.First().SetElementValue("comment", HttpUtility.HtmlDecode(txtComment.Text));
            }
            //Finally saving the Resource file.
            xDoc.Save(theFileName);
        }

        DataGrid3.EditIndex = -1;
        LoadData();*/
    }

    private ResourceView GetResourceDataView(object key, object ValueDA, object ValueEN)
    {
        var view = new ResourceView();
        view.Key = key;
        view.ValueDA = ValueDA;
        view.ValueEN = ValueEN;
       // view.ValueLang = ValueLang;
        return view;
    }

   
    protected void ResultGridView_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {


    }

    private void LoadData()
    {
        //string resourcespath = Request.PhysicalApplicationPath + "App_GlobalResources";
        //DirectoryInfo dirInfo = new DirectoryInfo(resourcespath);
        //foreach (FileInfo filInfo in dirInfo.GetFiles())
        //{
        //    string filename = filInfo.Name;
        //    cmbResources.Items.Add(filename);
        //}
        //cmbResources.Items.Insert(0, new ListItem("Select a Global Resource File"));

        string resourceslocalpath = Request.PhysicalApplicationPath + "\\Organisation\\App_LocalResources\\";
        DirectoryInfo dirlocalInfo = new DirectoryInfo(resourceslocalpath);
        foreach (FileInfo filInfo in dirlocalInfo.GetFiles())
        {
            string filename = filInfo.Name;
            int index = filename.IndexOf(".");
            if (index > 0)
                filename = filename.Substring(0, index);



            if (localResources.Items.Contains(localResources.Items.FindByText(filename)))
            {
                continue;
            }


            localResources.Items.Add(filename);
        }
        localResources.Items.Insert(0, new ListItem("Select a Organisation File"));



        string resourceswebpath = Request.PhysicalApplicationPath + "\\webadmin\\App_LocalResources\\";
        DirectoryInfo dirwebInfo = new DirectoryInfo(resourceswebpath);
        foreach (FileInfo filInfo in dirwebInfo.GetFiles())
        {
            string filename = filInfo.Name;
            int index = filename.IndexOf(".");
            if (index > 0)
                filename = filename.Substring(0, index);



            if (webResources.Items.Contains(webResources.Items.FindByText(filename)))
            {
                continue;
            }
            webResources.Items.Add(filename);
        }
        webResources.Items.Insert(0, new ListItem("Select a Super Admin File"));

        DataSet resds = new DataSet();
        ResourceLanguageBM obj = new ResourceLanguageBM();
        resds = obj.GetAllResourceLanguage();

        language.DataSource = resds;
        language.DataTextField = "resLanguage";
        language.DataValueField = "resExtension";
        language.DataBind();
     //   language.Items.FindByValue(".da").Selected = true;
        language.Items.Remove(language.Items.FindByText("English"));

    }
    protected void btnCancel_click(object sender, EventArgs e)
    {
        Response.Redirect("Dashboard.aspx");
    }
    protected void webResources_load()
    {
        return;
        if (webResources.SelectedIndex != 0)
        {
            string filename = Request.PhysicalApplicationPath + "\\webadmin\\App_LocalResources\\" + webResources.SelectedItem.Text;
            Stream stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
            ResXResourceReader RrX = new ResXResourceReader(stream);
            IDictionaryEnumerator RrEn = RrX.GetEnumerator();
            SortedList slist = new SortedList();
            while (RrEn.MoveNext())
            {
                if (!String.IsNullOrEmpty(Convert.ToString(RrEn.Value)))
                {
                    slist.Add(RrEn.Key, RrEn.Value);
                }
            }
            RrX.Close();
            stream.Dispose();
            //gridView1.DataSource = null;
            //gridView1.DataBind();
            DataGrid1.DataSource = null;
            DataGrid1.DataBind();
            DataGrid2.DataSource = slist;
            DataGrid2.DataBind();
        }
    }
    protected void localResources_load()
    {
        return;
        if (localResources.SelectedIndex != 0)
        {

            string filename = Request.PhysicalApplicationPath + "\\Organisation\\App_LocalResources\\" + localResources.SelectedItem.Text;
            Stream stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
            ResXResourceReader RrX = new ResXResourceReader(stream);
            IDictionaryEnumerator RrEn = RrX.GetEnumerator();
            SortedList slist = new SortedList();

            while (RrEn.MoveNext())
            {
                if (!String.IsNullOrEmpty(Convert.ToString(RrEn.Value)))
                {
                    slist.Add(RrEn.Key, RrEn.Value);
                }
            }
            RrX.Close();
            stream.Dispose();
            //gridView1.DataSource = null;
            //gridView1.DataBind();
            DataGrid2.DataSource = null;
            DataGrid2.DataBind();
            DataGrid1.DataSource = slist;
            DataGrid1.DataBind();
        }
    }
    public class ResourceView
    {
        public object Key { get; set; }

        public object ValueEN { get; set; }

        public object ValueDA { get; set; }
       

        public string Contact { get; set; }
    }

    protected void ResultGridView_RowCommands(object sender, GridViewCommandEventArgs e)
    {
        throw new NotImplementedException();
    }



    protected void gvGrid_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        if (e.CommandName == "save")
        {
        }
    }

    protected void btnsubmit_click(object sender, EventArgs e)
   {
        Boolean id = false;
        int i = 0;
        // int status= Convert.ToInt32(ddlsttus.SelectedValue); 
        for (i = 0; i < DataGrid3.Rows.Count; i++)
        {
            if (DataGrid3.Rows[i].RowType == DataControlRowType.DataRow)
            {
                Label key = (Label)DataGrid3.Rows[i].FindControl("lblKey");
                TextBox txtValueEN = (TextBox)DataGrid3.Rows[i].FindControl("txtValueEN");
                TextBox txtValueDA = (TextBox)DataGrid3.Rows[i].FindControl("txtValueDA");
                string filename = string.Empty;
                //for english

                if (localResources.SelectedIndex > 0)
                {
                    filename = localResources.SelectedItem.Text;
                    if (filename == "Employee")
                    {
                        filename = localResources.SelectedItem.Text + @".master.resx";
                    }
                    else if (filename.Contains("Master"))
                    {
                        filename = localResources.SelectedItem.Text + @".master.resx";
                    }
                    else
                    {
                        filename = localResources.SelectedItem.Text + @".aspx.resx";
                    }


                    filename = Request.PhysicalApplicationPath + "\\Organisation\\App_LocalResources\\" + filename;
                }
                else if (webResources.SelectedIndex > 0)
                {
                    filename = webResources.SelectedItem.Text;
                    if (filename.Contains("Master"))
                    {
                        filename = webResources.SelectedItem.Text + @".master.resx"; ;
                    }
                    else
                    {
                        filename = webResources.SelectedItem.Text + @".aspx.resx";
                    }

                    filename = Request.PhysicalApplicationPath + "\\webadmin\\App_LocalResources\\" + filename;
                }

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(filename);
                

                XDocument xDoc = XDocument.Load(filename);
                var elementToBeEdited = from xle in xDoc.Element("root").Elements("data") where xle.Attribute("name").Value == Convert.ToString(key.Text) select xle;
                if (elementToBeEdited != null && elementToBeEdited.Count() > 0)
                {
                    elementToBeEdited.First().SetElementValue("value", HttpUtility.HtmlDecode(txtValueEN.Text));
                    xDoc.Save(filename);
                }
                //for danish

                if (localResources.SelectedIndex > 0)
                {
                    filename = localResources.SelectedItem.Text;
                    if (filename == "Employee")
                    {
                        //filename = localResources.SelectedItem.Text + @".master.da.resx";
                        filename = localResources.SelectedItem.Text + @".master"+language.SelectedItem.Value+".resx";
                    }
                    else if (filename.Contains("Master"))
                    {
                        filename = localResources.SelectedItem.Text + @".master" + language.SelectedItem.Value + ".resx";
                    }
                    else
                    {
                      //  filename = localResources.SelectedItem.Text + @".aspx.da.resx";
                        filename = localResources.SelectedItem.Text + @".aspx" + language.SelectedItem.Value + ".resx";
                    }
                    filename = Request.PhysicalApplicationPath + "\\Organisation\\App_LocalResources\\" + filename;
                }
                else if (webResources.SelectedIndex > 0)
                {
                    filename = webResources.SelectedItem.Text;
                    if (filename.Contains("Master"))
                    {
                       // filename = webResources.SelectedItem.Text + @".master.da.resx"; ;
                        filename = webResources.SelectedItem.Text + @".master" + language.SelectedItem.Value + ".resx"; ;
                    }
                    else
                    {
                        filename = webResources.SelectedItem.Text + @".aspx" + language.SelectedItem.Value + ".resx";
                    }
                    filename = Request.PhysicalApplicationPath + "\\webadmin\\App_LocalResources\\" + filename;
                }
                
                XmlDocument xmlDoc2 = new XmlDocument();
                xmlDoc2.Load(filename);
                XDocument xDoc2 = XDocument.Load(filename);
                var elementToBeEdited2 = from xle in xDoc2.Element("root").Elements("data") where xle.Attribute("name").Value == Convert.ToString(key.Text) select xle;
                if (elementToBeEdited2 != null && elementToBeEdited.Count() > 0)
                {

                    try
                    {
                        elementToBeEdited2.First().SetElementValue("value", HttpUtility.HtmlDecode(txtValueDA.Text));
                        xDoc2.Save(filename);
                    }
                    catch
                    {
                        var element2 = new XElement("data",
                            new XAttribute("name", key.Text),
                            new XAttribute(XNamespace.Xml + "space", "preserve"),
                            new XElement("value", txtValueDA.Text));
                        xDoc2.Element("root").Add(element2);
                        xDoc2.Save(filename);
                    }
                    //elementToBeEdited2.First().SetElementValue("value", HttpUtility.HtmlDecode(txtValueDA.Text));
                    //xDoc2.Save(filename);
                }
            }
        }

        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('Update SuccessFully !');</script>", false);
    }




    protected void AddnewResource_Click(object sender, EventArgs e)
    {
        Boolean id = false;
        int i = 0;
        string filename = string.Empty;
        string filename2 = string.Empty;
        if (localResources.SelectedIndex > 0)
        {
            filename = localResources.SelectedItem.Text;
            filename2 = localResources.SelectedItem.Text;
            if (filename == "Employee")
            {
                filename = localResources.SelectedItem.Text + @".master.resx";
              //  filename2 = localResources.SelectedItem.Text + @".master.da.resx";
                filename2 = localResources.SelectedItem.Text + @".master" + language.SelectedItem.Value + ".resx";
            }
            else if (filename.Contains("Master"))
            {
                filename = localResources.SelectedItem.Text + @".master.resx";
                filename2 = localResources.SelectedItem.Text + @".master" + language.SelectedItem.Value + ".resx";
            }
            else
            {
                filename = localResources.SelectedItem.Text + @".aspx.resx";
                //filename2 = localResources.SelectedItem.Text + @".aspx.da.resx";
                filename2 = localResources.SelectedItem.Text + @".aspx" + language.SelectedItem.Value + ".resx";
            }


            filename = Request.PhysicalApplicationPath + "\\Organisation\\App_LocalResources\\" + filename;
            filename2 = Request.PhysicalApplicationPath + "\\Organisation\\App_LocalResources\\" + filename2;
        }
        else if (webResources.SelectedIndex > 0)
        {
            filename = webResources.SelectedItem.Text;
            if (filename.Contains("Master"))
            {
                filename = webResources.SelectedItem.Text + @".master.resx";
              //  filename2 = webResources.SelectedItem.Text + @".master.da.resx";
                filename2 = webResources.SelectedItem.Text + @".master" + language.SelectedItem.Value + ".resx";
            }
            else
            {
                filename = webResources.SelectedItem.Text + @".aspx.resx";
               // filename2 = webResources.SelectedItem.Text + @".aspx.da.resx";
                filename2 = webResources.SelectedItem.Text + @".aspx" + language.SelectedItem.Value + ".resx";
            }

            filename = Request.PhysicalApplicationPath + "\\webadmin\\App_LocalResources\\" + filename;
            filename2 = Request.PhysicalApplicationPath + "\\webadmin\\App_LocalResources\\" + filename2;
        }

        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.Load(filename);
        XDocument xDoc = XDocument.Load(filename);
        var elementToBeEdited = from xle in xDoc.Element("root").Elements("data") where xle.Attribute("name").Value.ToLower().Trim() == Convert.ToString(txtKey.Text).ToLower().Trim() select xle;

        XmlDocument xmlDoc2 = new XmlDocument();
        xmlDoc2.Load(filename2);
        XDocument xDoc2 = XDocument.Load(filename2);
        var elementToBeEdited2 = from xle1 in xDoc2.Element("root").Elements("data") where xle1.Attribute("name").Value.ToLower().Trim() == Convert.ToString(txtKey.Text).ToLower().Trim() select xle1;


        if (elementToBeEdited != null && elementToBeEdited.Count() > 0)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('Key Already Found!');</script>", false);
            return;

        }




        else if (elementToBeEdited2 == null && elementToBeEdited2.Count() <= 0)
        {



            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('Key Already Found!');</script>", false);
            return;

            
        }

        else
        {
            var element = new XElement("data",
                        new XAttribute("name", txtKey.Text),
                        new XAttribute(XNamespace.Xml + "space", "preserve"),
                        new XElement("value", txtEngValue.Text));
            xDoc.Element("root").Add(element);
            xDoc.Save(filename);

            try
            {
                elementToBeEdited2.First().SetElementValue("value", HttpUtility.HtmlDecode(txtDnValue.Text));
                xDoc2.Save(filename2);
            }
            catch
            {
                var element2 = new XElement("data",
                    new XAttribute("name", txtKey.Text),
                    new XAttribute(XNamespace.Xml + "space", "preserve"),
                    new XElement("value", txtDnValue.Text));
                xDoc2.Element("root").Add(element2);
                xDoc2.Save(filename2);
            }

           

        }

        localResources_SelectedIndexChanged(localResources, null);


        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('Resource Added Succeessfully!');</script>", false);
    }


    protected void btnAddNewFile_Click(object sender, EventArgs e)
    {
        string filename1 = Request.PhysicalApplicationPath + "\\Organisation\\App_LocalResources\\add_employee.aspx.resx";
        Stream stream = new FileStream(filename1, FileMode.Open, FileAccess.Read, FileShare.Read);
        ResXResourceReader RrX = new ResXResourceReader(stream);
        IDictionaryEnumerator RrEn = RrX.GetEnumerator();
        List<ResourceView> slist = new List<ResourceView>();
        while (RrEn.MoveNext())
        {
            if (!String.IsNullOrEmpty(Convert.ToString(RrEn.Value)))
            {
                slist.Add(GetResourceDataView(RrEn.Key, null, RrEn.Value));
                // slist.Add(RrEn.Key, RrEn.Value);
            }
        }
        RrX.Close();
        stream.Dispose();
        if (slist != null)
        {
            string filename = Request.PhysicalApplicationPath + "\\Organisation\\App_LocalResources\\add_employee.aspx.er.resx";
            if (!File.Exists(filename))
            {
                ResXResourceWriter rw = new ResXResourceWriter(filename);
                foreach (var i in slist)
                {
                    rw.AddResource(Convert.ToString(i.Key), Convert.ToString(i.ValueEN));
                }
                rw.Close();
            }
        }
       
        
        

       
    }


    protected void btnGetValues_Click(object sender, EventArgs e)
    {

        string[] str = Assembly.GetExecutingAssembly().GetManifestResourceNames();
        GetFiles();
        string filename = Request.PhysicalApplicationPath + "\\Organisation\\App_LocalResources\\add_employee.aspx.er.resx";
            Stream stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
            ResXResourceReader RrX = new ResXResourceReader(stream);
            IDictionaryEnumerator RrEn = RrX.GetEnumerator();
            List<ResourceView> slist = new List<ResourceView>();
            while (RrEn.MoveNext())
            {
                if (!String.IsNullOrEmpty(Convert.ToString(RrEn.Value)))
                {
                    slist.Add(GetResourceDataView(RrEn.Key, null, RrEn.Value));
                    // slist.Add(RrEn.Key, RrEn.Value);
                }
            }
            RrX.Close();
            stream.Dispose();

            //
            var s = "";


            string filename3 = Request.PhysicalApplicationPath + "\\Organisation\\App_LocalResources\\add_employee.aspx.resx";
            Stream stream3 = new FileStream(filename3, FileMode.Open, FileAccess.Read, FileShare.Read);
            ResXResourceReader RrX3 = new ResXResourceReader(stream3);
            IDictionaryEnumerator RrEn3 = RrX3.GetEnumerator();

            List<ResourceView> slist2 = new List<ResourceView>();
            while (RrEn3.MoveNext())
            {
                //if (!String.IsNullOrEmpty(Convert.ToString(RrEn2.Value)))
                //{
                slist2.Add(GetResourceDataView(RrEn3.Key, RrEn3.Value, null));
                    // slist.Add(RrEn.Key, RrEn.Value);
                //}
            }
            //
            RrX3.Close();
            stream3.Dispose();
            //gridView1.DataSource = null;
            //gridView1.DataBind();

            var dem = from o in slist
                      join i in slist2 on o.Key equals i.Key
                      select new ResourceView
                      {
                          Key = o.Key,
                          ValueEN = o.ValueEN,
                          ValueDA = i.ValueDA
                      };

            
          //  var dem=slist.GroupJoin(slist2,p=>p.Key,c=>c.Key,(p,c)=>new{p,c}).SelectMany(p=>p.p
            List<ResourceView> slist3 = new List<ResourceView>();
            /*foreach (var k in dem.ToList())
            {
                slist3.Add(GetResourceDataView(dem, RrEn2.Value, null));
            }*/
            DataGrid3.Columns[2].HeaderText = language.SelectedItem.Text;
            DataGrid3.DataSource = dem;
            DataGrid3.DataBind();

    }
    private void GetFiles()
    {

        string path = Server.MapPath("~/Organisation/App_LocalResources");
        DirectoryInfo d = new DirectoryInfo(path);//Assuming Test is your Folder
        var Files = d.GetFiles("*.resx").Where(x => x.Name.Contains(".aspx.resx") || x.Name.Contains(".master.resx")).Select(x => x.Name).ToList();
        
        
    }
}