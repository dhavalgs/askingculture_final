﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webadmin/AdminMasterPage.master" AutoEventWireup="true" CodeFile="StateList.aspx.cs" 
Inherits="webadmin_StateList" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" 
        meta:resourcekey="lblMsgResource1"></asp:Label>
    <div class="row">
        <div class="col-md-12">
            <div class="widget box">
                <div class="widget-header">
                    <h4>
                        <i class="icon-reorder"></i> <%-- <%= CommonMessages.State%>--%>
                        <asp:Literal ID="Literal9" runat="server" meta:resourcekey="State" enableviewstate="false"/></h4>
                    <div class="toolbar no-padding">
                        <div class="btn-group">
                            <span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
                        </div>
                    </div>
                </div>
                <div class="widget-content no-padding">
                    <div class="btn-toolbar" style="text-align: right;">
                    </div>
                    <cc1:TabContainer ID="TabContainer1" runat="server" 
                        CssClass="fancy fancy-green" ActiveTabIndex="0" 
                        meta:resourcekey="TabContainer1Resource1">
                        <cc1:TabPanel ID="tbpnluser" runat="server" 
                            meta:resourcekey="tbpnluserResource1">
                            <HeaderTemplate>
                               <%--  <%= CommonMessages.State%>--%>
                                 <asp:Literal ID="Literal1" runat="server" meta:resourcekey="State" enableviewstate="false"/>
                            </HeaderTemplate>
                            <ContentTemplate>                                
                                <asp:GridView ID="gvGrid" runat="server" AutoGenerateColumns="False" CellPadding="0"
                                    Width="100%" GridLines="None" DataKeyNames="staId" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                                    EmptyDataText="No Record Found"
                                    OnRowCommand="gvGrid_RowCommand" meta:resourcekey="gvGridResource1"
                                    >
                                    <HeaderStyle CssClass="aa" />
                                    <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                    <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sr.No" meta:resourcekey="TemplateFieldResource1">
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Country Name" 
                                            meta:resourcekey="TemplateFieldResource2">
                                            <ItemTemplate>
                                                <asp:Label ID="lblrNamer" runat="server" Text="<%# bind('couName') %>" 
                                                    meta:resourcekey="lblrNamerResource1"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="State Name" 
                                            meta:resourcekey="TemplateFieldResource3">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUserNamer" runat="server" Text="<%# bind('staName') %>" 
                                                    meta:resourcekey="lblUserNamerResource1"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        </asp:TemplateField>
                                        
                                        <asp:TemplateField HeaderText="Action" 
                                            meta:resourcekey="TemplateFieldResource4">
                                            <ItemTemplate>
                                                <span class="btn-group"><a href="<%# String.Format("State.aspx?id={0}", Eval("staId")) %>"
                                                    class="bs-tooltip" title="Edit" class="btn btn-xs"><i class="icon-pencil"></i></a>
                                                    <asp:LinkButton ID="lnkBtnName" runat="server" CommandName="archive" CommandArgument='<%# Eval("staId") %>'
                                                        CssClass="icon-trash" ToolTip="Archive" 
                                                    OnClientClick="return confirm('Are you sure you want to archive this record?');" 
                                                    meta:resourcekey="lnkBtnNameResource1"></asp:LinkButton></span>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="10%" />
                                        </asp:TemplateField>
                                        
                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>
                        </cc1:TabPanel>
                        <cc1:TabPanel ID="tbpnlusrdetails" runat="server" 
                            meta:resourcekey="tbpnlusrdetailsResource1">
                            <HeaderTemplate>
                                <%-- <%= CommonMessages.Archive%>--%>
                                 <asp:Literal ID="Literal2" runat="server" meta:resourcekey="Archive" enableviewstate="false"/>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:GridView ID="gvArchive" runat="server" AutoGenerateColumns="False" 
                                    CellPadding="0" Width="100%" GridLines="None" 
                                    EmptyDataText="No Record Found"
                                    DataKeyNames="staId" OnRowCommand="gvArchive_RowCommand"
                                     
                                    CssClass="table table-striped table-bordered table-hover table-checkable datatable" meta:resourcekey="gvArchiveResource1"
                                    >
                                    <HeaderStyle CssClass="aa" />
                                    <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                    <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sr.No" meta:resourcekey="TemplateFieldResource5">
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Country Name" 
                                            meta:resourcekey="TemplateFieldResource6">
                                            <ItemTemplate>
                                                <asp:Label ID="lblaNamer" runat="server" Text="<%# bind('couName') %>" 
                                                    meta:resourcekey="lblaNamerResource1"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="State Name" 
                                            meta:resourcekey="TemplateFieldResource7">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUserNamer" runat="server" Text="<%# bind('staName') %>" 
                                                    meta:resourcekey="lblUserNamerResource2"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        </asp:TemplateField>
                                        
                                        <asp:TemplateField HeaderText="Action" 
                                            meta:resourcekey="TemplateFieldResource8">
                                            <ItemTemplate>
                                                <span class="btn-group">
                                                    <asp:LinkButton ID="lnkbtnrestore" runat="server" CommandName="archive" 
                                                    CommandArgument='<%# Eval("staId") %>'
                                                        ToolTip="Restore" 
                                                        OnClientClick="return confirm('Are you sure you want to restore this record?');"
                                                        Text="Restore" meta:resourcekey="lnkbtnrestoreResource1"></asp:LinkButton>&nbsp;|&nbsp;
                                                    <asp:LinkButton ID="lnkBtnPermanentlydelete" runat="server" CommandName="permanentlydelete"
                                                        CommandArgument='<%# Eval("staId") %>' Text="Delete Permanently" ToolTip="Delete Permanently"
                                                        
                                                    OnClientClick="return confirm('Are you sure you want to permanently delete this record?');" 
                                                    meta:resourcekey="lnkBtnPermanentlydeleteResource1"></asp:LinkButton>
                                                </span>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="10%" />
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>
                        </cc1:TabPanel>
                    </cc1:TabContainer>
                </div>
            </div>
            <div class="btn-toolbar" style="text-align: right;">
                <div>
                <asp:Button runat="server" ID="btnAdd" PostBackUrl="State.aspx" Text="Add State"
                    CssClass=" btn btn-success" meta:resourcekey="btnAddResource1" />
            </div>
            </div>
        </div>
    </div>
    <!-- /no-padding -->
    <!--=== no-padding and table-tabletools ===-->
    <div class="row">
    </div>
    <!-- /no-padding and table-tabletools -->
    <!--=== no-padding and table-colvis ===-->
    <div class="row">
    </div>
    <!-- /no-padding and table-colvis -->
    <!--=== Horizontal Scrolling ===-->
    <div class="row">
    </div>
    <!-- /Normal -->
    <!-- /Page Content -->
</asp:Content>

