﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webadmin/AdminMasterPage.master" AutoEventWireup="true"
    CodeFile="Cms.aspx.cs" Inherits="webadmin_Cms" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="../Scripts/ckeditor/ckeditor.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" 
        meta:resourcekey="lblMsgResource1"></asp:Label>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-12">
                <div class="indicatesRequireFiled">
                    *<%--<%= CommonMessages.Indicatesrequiredfield%>--%>
                    <asp:Literal ID="Literal9" runat="server" meta:resourcekey="Indicatesrequiredfield" enableviewstate="false"/>
                </div>
                <div class="widget box">
                    <div class="widget-header">
                        <h4>
                            <i class="icon-reorder"></i>
                            <asp:Label runat="server" ID="lblDataDisplayTitle" Text="CMS" 
                                meta:resourcekey="lblDataDisplayTitleResource1"></asp:Label>
                        </h4>
                    </div>
                    <div class="widget-content">
                        <%--   <form class="form-horizontal row-border" action="#">--%>
                        <div class="form-horizontal row-border">
                            <div class="form-group">
                                <label class="col-md-2 control-label">
                                  <%--  <%= CommonMessages.Name%>--%>
                                     <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Name" enableviewstate="false"/>:<span class="starValidation">*</span></label>
                                <asp:HiddenField ID="hfTagId" Value="0" runat="server" />
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="txtName" onkeyup="run(this)" MaxLength="80"
                                        CssClass="form-control" meta:resourcekey="txtNameResource1" />
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                      ValidChars=" "  TargetControlID="txtName" Enabled="True">
                                    </cc1:FilteredTextBoxExtender>
                                    <br />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtName"
                                        ErrorMessage="Please enter Name." CssClass="commonerrormsg" Display="Dynamic"
                                        ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator5Resource1"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">
                                  <%--  <%= CommonMessages.Description%>--%>
                                     <asp:Literal ID="Literal2" runat="server" meta:resourcekey="Description" enableviewstate="false"/>:<span class="starValidation">*</span></label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="txtDescription" TextMode="MultiLine" Height="300px"
                                        MaxLength="500" CssClass="ckeditor" 
                                        meta:resourcekey="txtDescriptionResource1" />
                                      <asp:Label runat="server" ID="lblmsgDescription" CssClass="commonerrormsg" 
                                        meta:resourcekey="lblmsgDescriptionResource1"></asp:Label>
                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDescription"
                                        ErrorMessage="Please enter Description." CssClass="commonerrormsg" Display="Dynamic"
                                        ValidationGroup="chk"></asp:RequiredFieldValidator>--%>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">
                                  <%--  <%= CommonMessages.Description%>--%>
                                     <asp:Literal ID="Literal3" runat="server" meta:resourcekey="DescriptioninDanish" enableviewstate="false"/>:<span class="starValidation">*</span></label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="txtDescriptionDN" TextMode="MultiLine" Height="300px"
                                        MaxLength="500" CssClass="ckeditor" 
                                        meta:resourcekey="txtDescriptionDNResource1" />
                                      <asp:Label runat="server" ID="lblmsgDescriptionDN" CssClass="commonerrormsg" 
                                        meta:resourcekey="lblmsgDescriptionDNResource1"></asp:Label>
                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDescription"
                                        ErrorMessage="Please enter Description." CssClass="commonerrormsg" Display="Dynamic"
                                        ValidationGroup="chk"></asp:RequiredFieldValidator>--%>
                                </div>
                            </div>
                            <div class="form-horizontal row-border">
                                <div class="btn-toolbar">
                                    <asp:Button runat="server" ID="btnsubmit" Text="Save" CssClass="btn btn-success"
                                        ValidationGroup="chk" OnClick="btnsubmit_click" 
                                        meta:resourcekey="btnsubmitResource1" />
                                    <asp:Button runat="server" ID="btnCancel" Text="Cancel" CssClass="btn" ValidationGroup="chk"
                                        CausesValidation="False" OnClick="btnCancel_click" 
                                        meta:resourcekey="btnCancelResource1" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--=== no-padding ===-->
    <div class="row">
    </div>
    <div class="row">
    </div>
    <!-- /no-padding and table-tabletools -->
    <!--=== no-padding and table-colvis ===-->
    <div class="row">
    </div>
    <!-- /no-padding and table-colvis -->
    <!--=== Horizontal Scrolling ===-->
    <div class="row">
    </div>
    <!-- /Normal -->
    <!-- /Page Content -->
</asp:Content>
