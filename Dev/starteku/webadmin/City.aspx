﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webadmin/AdminMasterPage.master" AutoEventWireup="true" CodeFile="City.aspx.cs" Inherits="webadmin_City" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg"></asp:Label>
 <div class="row">
        <div class="col-md-12">
            <div class="col-md-12">
                <div class="indicatesRequireFiled">
                    * Indicates required field
                </div>
                <div class="widget box">
                    <div class="widget-header">
                        <h4>
                            <i class="icon-reorder"></i>
                            <asp:Label runat="server" ID="lblDataDisplayTitle" Text="State"></asp:Label>
                        </h4>
                    </div>
                    <div class="widget-content">                      
                        <div class="form-horizontal row-border">
                        <div class="form-group">
                                <label class="col-md-2 control-label">
                                    State Name:<span class="starValidation">*</span></label>
                                <div class="col-md-10">
                                   <asp:DropDownList ID="ddlstate" runat="server" Style="width: 100px; height: 32px;
                                            display: inline;">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="ddlstate"
                                            ErrorMessage="Please select State." InitialValue="0" CssClass="commonerrormsg"
                                            Display="Dynamic" ValidationGroup="chk"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">
                                    City Name:<span class="starValidation">*</span></label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" Text="" ID="txtcityName" MaxLength="50" CssClass="form-control" />                                   
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtcityName"
                                        ErrorMessage="Please enter City name." CssClass="commonerrormsg" Display="Dynamic"
                                        ValidationGroup="chk"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            
                            
                            
                            <div class="form-horizontal row-border">
                                <div class="btn-toolbar">                                    
                                    <asp:Button runat="server" ID="btnsubmit" Text="Save" CssClass="btn btn-success"
                                         ValidationGroup="chk" OnClick="btnsubmit_click"/>
                                    <asp:Button runat="server" ID="btnCancel" Text="Cancel" CssClass="btn" ValidationGroup="chk"
                                        CausesValidation="false" OnClick="btnCancel_click"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--=== no-padding ===-->
    <div class="row">
    </div>
    <div class="row">
    </div>
    <!-- /no-padding and table-tabletools -->
    <!--=== no-padding and table-colvis ===-->
    <div class="row">
    </div>
    <!-- /no-padding and table-colvis -->
    <!--=== Horizontal Scrolling ===-->
    <div class="row">
    </div>
    <!-- /Normal -->
    <!-- /Page Content -->
</asp:Content>

