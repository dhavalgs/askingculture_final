﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;

using starteku_BusinessLogic.Model;

public partial class webadmin_Login : System.Web.UI.Page
{
    #region Page Event
    startetkuEntities1 db = new startetkuEntities1();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            cookieExpires_type();
            checkRememberLogin();
        }

        LogoMaster logo = db.LogoMasters.FirstOrDefault();
        if (logo != null)
        {

            imgLoginLogo.ImageUrl = "../Log/upload/Userimage/" + logo.LoginLogo;
            ViewState["imgLoginLogo"] = logo.LoginLogo;
        }
    }
    #endregion

    #region method

    protected void checkRememberLogin()
    {
        if (Request.Cookies["startekuUserLogin"] != null)
        {
            if (Request.Cookies["startekuUserLogin"].Values["UserName"] != null)
            {
                txtUsername.Text = Convert.ToString(Request.Cookies["startekuUserLogin"].Values["UserName"]);
                txtPassword.Text = Convert.ToString(Request.Cookies["startekuUserLogin"].Values["Password"]);
                txtPassword.Attributes.Add("Value", Convert.ToString(Request.Cookies["startekuUserLogin"].Values["Password"]));
            }
        }
    }

    protected void setRememberMe()
    {
        if (cbRememberme.Checked)
        {
            HttpCookie cookie = new HttpCookie("startekuUserLogin");
            cookie.Values.Add("UserName", txtUsername.Text);
            cookie.Values.Add("Password", txtPassword.Text);

            cookie.Expires = DateTime.Now.AddDays(30);
            Response.Cookies.Add(cookie);
        }
    }

    //public void cookie_type()
    //{
    //    HttpCookie cookie = new HttpCookie("littleJackLoggedType");
    //    cookie.Values.Add("LoggedType", "admin");
    //    cookie.Expires = DateTime.Now.AddDays(30);
    //    Response.Cookies.Add(cookie);
    //}

    protected void cookieExpires_type()
    {
        HttpCookie cookie = new HttpCookie("startekuLoggedType");
        cookie.Expires = DateTime.Now.AddDays(-1);
        Response.Cookies.Add(cookie);
    }

    #endregion

    #region Button Event
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        UserBM obj = new UserBM();
        obj.userEmail = txtUsername.Text.Trim();
        obj.userPassword = CommonModule.encrypt(Convert.ToString(txtPassword.Text));
        obj.userType = 0;
        obj.userMasterLogin();
        DataSet dsObj = obj.ds;

        if (dsObj != null)
        {
            if (dsObj.Tables[0].Rows.Count > 0)
            {
                setRememberMe();
                if (Convert.ToInt32(dsObj.Tables[0].Rows[0]["userType"]) == 0)
                {
                   // Session.Timeout = 80;
                    Session["UserName"] = Convert.ToString(dsObj.Tables[0].Rows[0]["userFirstName"]);
                    Session["UserId"] = Convert.ToString(dsObj.Tables[0].Rows[0]["userId"]);
                    Session["userType"] = Convert.ToString(dsObj.Tables[0].Rows[0]["userType"]);
                    if (String.IsNullOrEmpty(Convert.ToString(dsObj.Tables[0].Rows[0]["languageName"])))
                    {
                        Session["Cultureadmin"] = "en-GB";
                    }
                    else
                    {
                        Session["Cultureadmin"] = Convert.ToString(dsObj.Tables[0].Rows[0]["languageName"]);
                    }
                    Response.Redirect("dashboard.aspx");
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgInvalidUserNameOrPassword;
            }
        }
        else
        {
            lblMsg.Text = CommonModule.msgInvalidUserNameOrPassword;
        }
    }
    #endregion
}