﻿<%@ Page Language="C#" ValidateRequest="false" %>

<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Resources" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="starteku_BusinessLogic" %>
<%@ Import Namespace="starteku_BusinessLogic.Model" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            
            SendAutoEmail();

        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "AutomaticServiceMail->Page_load");
        }
    }
    protected void SendAutoEmail()
    {
        try
        {
           
            var db = new startetkuEntities1();
           
            var data = db.GetEmailSendList().ToList();
            
            foreach (var d in data)
            {
                
                if (d.EmailType == "CompanyDir")
                {
                   
                    sendmailCompanyDir(d.userId, "CompanyDirectionPublish", d.userFirstName, d.userEmail, Convert.ToInt32(d.userCompanyId),d.CreateUserFirstName);
                }
                else if (d.EmailType == "Question")
                {
                    sendmailQuestion(d.userId, "QuestionPublish", d.userFirstName, d.userEmail, Convert.ToInt32(d.userCompanyId));
                }
                else if (d.EmailType == "ActivityRequest" || d.EmailType == "ActivityComplete" || d.EmailType == "ActivityApproved" || d.EmailType == "ActivityRejected" || d.EmailType == "ActivityReRequest" || d.EmailType =="ActivityCompleteApproval")
                {
                    sendmailActivity(d.userId, d.EmailType, Convert.ToInt32(d.userCompanyId),d.userFirstName,d.userEmail,Convert.ToInt32( d.userCreateBy),d.CreateUserFirstName);
                }
                else if (d.EmailType == "MeetDoc")
                {
                    sendmailCompanyDir(d.userId, "PDPRelatedDoc", d.userFirstName, d.userEmail, Convert.ToInt32(d.userCompanyId),d.CreateUserFirstName);
                }
                else if (d.EmailType == "PDPMeeting")
                {
                    sendmailCompanyDir(d.userId, "PDPMeeting", d.userFirstName, d.userEmail, Convert.ToInt32(d.userCompanyId),d.CreateUserFirstName);
                }
                else if (d.EmailType == "QuesDialog")
                {
                    sendmailCompanyDir(d.userId, "PDPQuesDialog", d.userFirstName, d.userEmail, Convert.ToInt32(d.userCompanyId), d.CreateUserFirstName,d.EmailContent);
                }
            }
            db.GetEmailSendList_Update();
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "SendAutoEmail");
        }
    }
    protected void sendmailActivity(Int32 touserid, string templateName = "ActivityRequest", int companyID = 0, string userFirstName = "", string userEmail = "", int userCreateBy = 0,string UserCreateFirstName="")
    {
        try
        {


            #region Send mail
            // String subject = "Set Competence for " + Convert.ToString(Session["OrgUserName"]);
            Template template = CommonModule.getTemplatebyname1(templateName, Convert.ToInt32(touserid));
            //String confirmMail = CommonModule.getTemplatebyname("Set Competence", touserid);
            String confirmMail = template.TemplateName;
            if (!String.IsNullOrEmpty(confirmMail))
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Convert.ToString(template.LanguageID));
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(Convert.ToString(template.LanguageID));
                
                var localResourceObject = HttpContext.GetLocalResourceObject("~/Organisation/ActivityLists.aspx", "Activity_Request_Email_Subject.Text"); //GetLocalResourceObject("Activity_Request_Email_Subject.Text");
                if (templateName == "ActivityComplete")
                {
                    localResourceObject = HttpContext.GetLocalResourceObject("~/Organisation/ActivityLists.aspx", "Activity_Complete_Email_Subject.Text");
                }
                if (templateName == "ActivityApproved")
                {
                    localResourceObject = HttpContext.GetLocalResourceObject("~/Organisation/ActivityRequestView.aspx", "Activity_Approved_Email_Subject.Text");
                }
                if (templateName == "ActivityRejected")
                {
                    localResourceObject = HttpContext.GetLocalResourceObject("~/Organisation/ActivityRequestView.aspx", "Activity_Reject_Email_Subject.Text");
                }
                if (templateName == "ActivityReRequest")
                {
                    localResourceObject = HttpContext.GetLocalResourceObject("~/Organisation/ActivityLists.aspx", "ActivityReRequest_Email_Subject.Text");
                }
                if (templateName == "ActivityCompleteApproval")
                {
                    localResourceObject = HttpContext.GetLocalResourceObject("~/Organisation/ActivityLists.aspx", "ActivityCompleteApproval_Email_Subject.Text");
                }
                if (localResourceObject != null)
                {
                    string subject = localResourceObject.ToString();

                    String name = Convert.ToString(userFirstName);
                    string fromid = Convert.ToString(ConfigurationManager.AppSettings["fromid"]);
                    String empname = userFirstName;
                    string tempString = confirmMail;
                    tempString = tempString.Replace("###name###", name);
                    tempString = tempString.Replace("###empname###", UserCreateFirstName);

                    CommonModule.SendMailToUser(Convert.ToString(userEmail), subject, tempString, fromid, Convert.ToString(companyID));
                }
            }

            #endregion
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "sendmailActivity");
        }
    }
    protected void sendmailCompanyDir(Int32 touserid, string templateName = "CompanyDirectionPublish", string userFirstName = "", string userEmail = "", int companyID = 0,string UserCreateFirstName="",string Dialogue="")
    {
        try
        {
         
            #region Send mail
            Template template = CommonModule.getTemplatebyname1(templateName, Convert.ToInt32(touserid));
           
            String confirmMail = template.TemplateName;
            if (!String.IsNullOrEmpty(confirmMail))
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Convert.ToString(template.LanguageID));
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(Convert.ToString(template.LanguageID));
                
                var localResourceObject = HttpContext.GetLocalResourceObject("~/Organisation/CompanyDirection.aspx", "CompanyDirectionPublish_Email_Subject.Text");//GetLocalResourceObject("CompanyDirectionPublish_Email_Subject.Text");
                if (templateName == "PDPRelatedDoc")
                {
                    localResourceObject = HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "PDPRelatedDoc_Email_Subject.Text");
                }
                else if (templateName == "PDPMeeting")
                {
                    localResourceObject = HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "PDPMeeting_Email_Subject.Text");
                }
                else if (templateName == "PDPQuesDialog")
                {
                    localResourceObject = HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "PDPQuesDialog_Email_Subject.Text");
                }
                if (localResourceObject != null)
                {
                    
                    string subject = localResourceObject.ToString();

                    //   var userDetail = UserBM.GetUserByIdLinq(touserid);

                    //if (userDetail != null)
                    {
                        String name = Convert.ToString(userFirstName);
                        string fromid = Convert.ToString(ConfigurationManager.AppSettings["fromid"]);
                        String empname = Convert.ToString(UserCreateFirstName);
                        string tempString = confirmMail;
                        tempString = tempString.Replace("###name###", name);
                        tempString = tempString.Replace("###empname###", empname);
                        tempString = tempString.Replace("###dialogue###", Dialogue);
                        tempString = tempString.Replace("###id###", Convert.ToString(touserid));

                        
                        CommonModule.SendMailToUser(Convert.ToString(userEmail), subject, tempString, fromid, Convert.ToString(companyID));
                        
                    }
                }
            }

            #endregion
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "sendmailCompanyDir");
        }
    }
    protected void sendmailQuestion(Int32 touserid, string templateName = "QuestionPublish", string userFirstName = "", string userEmail = "", int companyID = 0)
    {
        try
        {
            #region Send mail
            Template template = CommonModule.getTemplatebyname1(templateName, Convert.ToInt32(touserid));
            String confirmMail = template.TemplateName;
            if (!String.IsNullOrEmpty(confirmMail))
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Convert.ToString(template.LanguageID));
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(Convert.ToString(template.LanguageID));
                
                var localResourceObject = HttpContext.GetLocalResourceObject("~/Organisation/Question.aspx", "QuestionPublish_Email_Subject.Text");//GetLocalResourceObject("QuestionPublish_Email_Subject.Text");

                if (localResourceObject != null)
                {
                    string subject = localResourceObject.ToString();

                    //   var userDetail = UserBM.GetUserByIdLinq(touserid);

                    //if (userDetail != null)
                    {
                        String name = Convert.ToString(userFirstName);
                        string fromid = Convert.ToString(ConfigurationManager.AppSettings["fromid"]);
                        String empname = Convert.ToString(Session["OrgUserName"]);
                        string tempString = confirmMail;
                        tempString = tempString.Replace("###name###", name);
                        tempString = tempString.Replace("###id###", Convert.ToString(touserid));
                        CommonModule.SendMailToUser(Convert.ToString(userEmail), subject, tempString, fromid, Convert.ToString(companyID));
                    }
                }
            }

            #endregion
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "sendmailCompanyDir");
        }
    }
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
</head>
<body>
</body>
</html>
