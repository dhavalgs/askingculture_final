﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Organisation/OrganisationMaster.master"
    CodeFile="QuestionCategoryEdit.aspx.cs" Inherits="Organisation_QuestionEdit" culture="auto" meta:resourcekey="PageResource1" uiculture="auto"%>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .chat-widget-head h4 {
            float: none !important;
        }

        .treeNode {
        }

            .treeNode input {
                width: auto;
                margin: 5px;
                float: left !important;
            }

        .form-control {
            text-transform: none;
        }

        label {
            display: inline-block;
            font-size: 14px;
            font-weight: inherit;
            margin-bottom: 5px;
        }

        .inline-form input, .inline-form textarea {
            font-size: 15px;
        }
    </style>
    <style type="text/css">
        body {
            font-family: Geneva,Arial,Helvetica,sans-serif;
        }

        #ContentPlaceHolder1_chkList input {
            width: 33px;
            margin-bottom: 0px !important;
        }

        #ContentPlaceHolder1_chkList label {
            margin-top: 2px;
        }
    </style>
    <style type="text/css">
        .scroll_checkboxes {
            height: 120px;
            width: 270px;
            padding: 5px;
            overflow: auto;
            border: 1px solid #ccc;
        }

        .FormText {
            font-size: 11px;
            font-family: tahoma,sans-serif;
        }
    </style>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    
    <div class="col-md-6">
        <div class="heading-sec">
            <%--<h1>
                <asp:Label runat="server" ID="lblDataDisplayTitle" Text="Division"></asp:Label>
                <i>Welcome to Flat Lab </i></h1>--%>
            <h1>
                <asp:Label runat="server" ID="Label12" CssClass="lblModel" meta:resourcekey="Quecatedit" Font-Bold="True" ForeColor="white" Font-Size="22px"></asp:Label>
                   
            </h1>
        </div>
    </div>
    <br />
    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" Font-Size="Large" meta:resourcekey="lblMsgResource1"></asp:Label>
    <br />

    <div class="col-md-12">
        <div class="chat-widget widget-body" style="background: #fff;">
            <div class="chat-widget-head yellow yellow-radius">
                <h4>
                     <asp:Label runat="server" ID="Label1" CssClass="lblModel" meta:resourcekey="EditQuecat" Font-Bold="True" ForeColor="white" Font-Size="22px"></asp:Label>
                   
                </h4>
            </div>
            <div class="indicatesRequireFiled">
                <i><span>* </span>
                    <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Indicatesrequiredfield" enableviewstate="false"/> </i>
            </div>
          
                 <asp:HiddenField runat="server" ID="txtqcatID" />          
                      
            <div class="col-md-6" style="width:100%";>
                <div class="inline-form">
                    <label class="c-label">
                        <asp:Literal ID="Literal2" runat="server" Text="Question Category" meta:resourcekey="QuestionCategoryName" enableviewstate="false"/>:*</label>
                    <asp:TextBox runat="server" ID="txtqcatName" MaxLength="50" 
                        CssClass="form-control" placeholder="TITLE :" 
                        meta:resourcekey="txtqcatNameResource1"/>
                    <br/>

                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtqcatName"
                        ErrorMessage="Please enter Question Category name." CssClass="commonerrormsg" Display="Dynamic"
                        ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator5Resource1"></asp:RequiredFieldValidator>
                </div>
            </div>

            <div class="col-md-6" style="width:100%";>
                <div class="inline-form">
                    <label class="c-label">
                     <asp:Literal ID="Literal5" Text="Question Category(Danish)" runat="server" meta:resourcekey="QuestionCategoryNameDN" enableviewstate="false"/>:*</label>
                    <asp:TextBox runat="server" ID="txtqcatNameDN" MaxLength="50" 
                        CssClass="form-control" placeholder="TITLE :" 
                        meta:resourcekey="txtqcatNameDNResource1"/><br/>
                    <asp:RequiredFieldValidator ID="txtqcatNameDNValidator1" runat="server" ControlToValidate="txtqcatNameDN"
                        ErrorMessage="Please enter Question Category name." CssClass="commonerrormsg" Display="Dynamic"
                        ValidationGroup="chk" meta:resourcekey="txtqcatNameDNValidator1Resource1"></asp:RequiredFieldValidator>
                </div>
            </div>
           
            <div class="col-md-6" style="width:100%";>
                <div class="inline-form">
                    <label class="c-label">
                       <asp:Literal ID="Literal4" runat="server" Text="Description" meta:resourcekey="Description" enableviewstate="false"/>:</label>
                    <asp:TextBox runat="server" placeholder="DESCRIPTION :" ID="txtDESCRIPTION" MaxLength="500"
                        TextMode="MultiLine" Rows="5" CssClass="form-control" 
                        meta:resourcekey="txtDESCRIPTIONResource1"/><br/>
                </div>
            </div>
           
            <div class="modal-footer" style="border: 0px;">
                <asp:Button runat="server" ID="btnsubmit" class="btn btn-primary yellow" 
                    Text="Save" OnClick="btnsubmit_click"  ValidationGroup="chk" 
                    style="border-radius: 5px;" meta:resourcekey="btnsubmitResource1">
                </asp:Button>
                 <asp:Button runat="server" ID="btnCancel" class="btn btn-primary black" 
                    Text="Cancel" OnClick="btnCancel_click" meta:resourcekey="btnCancelResource1"></asp:Button>
            </div>

        </div>
    </div>
    
    
    <script>
        $(document).ready(function () {
            "use strict";

            App.init(); // Init layout and core plugins
            Plugins.init(); // Init all plugins
            FormComponents.init(); // Init all form-specific plugins
        });
    </script>
   
</asp:Content>

