﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;

public partial class Organisation_OrgStateList : System.Web.UI.Page
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            State.InnerHtml = "Welcome   " + Convert.ToString(Session["OrgUserName"]) + "!";
            GetAllCountry();
            StateSelectAll();
            StateSelectArchiveAll();
            SetDefaultMessage();
        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion


    #region method
    protected void SetDefaultMessage()
    {
        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["msg"])))
        {
            if (Convert.ToString(Request.QueryString["msg"]) == "del")
            {
                lblMsg.Text = CommonModule.msgRecordHasBeenDeletedSuccessfully;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "ins")
            {
                lblMsg.Text = CommonModule.msgRecordInsertedSuccessfully;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "upd")
            {
                lblMsg.Text = CommonModule.msgRecordUpdatedSuccss;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "arc")
            {
                lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
        }

    }
    protected void StateSelectAll()
    {
        StateBM obj = new StateBM();
        obj.staIsActive = true;
        obj.staIsDeleted = false;
        obj.staCompanyId = Convert.ToInt32(Session["OrgUserId"]);
        obj.GetAllState();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvGrid.DataSource = ds.Tables[0];
                gvGrid.DataBind();

                gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvGrid.DataSource = null;
                gvGrid.DataBind();
            }
        }
        else
        {
            gvGrid.DataSource = null;
            gvGrid.DataBind();
        }
    }
    protected void StateSelectArchiveAll()
    {
        StateBM obj = new StateBM();
        obj.staIsActive = false;
        obj.staIsDeleted = false;
        obj.staCompanyId = Convert.ToInt32(Session["OrgUserId"]);
        obj.GetAllState();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvArchive.DataSource = ds.Tables[0];
                gvArchive.DataBind();

                gvArchive.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvArchive.DataSource = null;
                gvArchive.DataBind();
            }
        }
        else
        {
            gvArchive.DataSource = null;
            gvArchive.DataBind();
        }
    }
    protected void GetAllCountry()
    {
        CountryBM obj = new CountryBM();
        obj.couIsActive = true;
        obj.couIsDeleted = false;
        obj.couCompanyId = Convert.ToInt32(Session["OrgUserId"]);
        obj.GetAllCountry();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlcountry.Items.Clear();

                ddlcountry.DataSource = ds.Tables[0];
                ddlcountry.DataTextField = "couName";
                ddlcountry.DataValueField = "couId";
                ddlcountry.DataBind();

                ddlcountry.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
            }
            else
            {
                ddlcountry.Items.Clear();
                ddlcountry.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
            }
        }
        else
        {
            ddlcountry.Items.Clear();
            ddlcountry.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
        }

    }
    #endregion


    #region grid Event
    protected void gvGrid_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "archive")
        {
            StateBM obj = new StateBM();
            obj.staId = Convert.ToInt32(e.CommandArgument);
            obj.staIsActive = false;
            obj.staIsDeleted = false;
            obj.stateStatusUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    StateSelectAll();
                    StateSelectArchiveAll();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }
    }

    protected void gvArchive_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "archive")
        {
            StateBM obj = new StateBM();
            obj.staId = Convert.ToInt32(e.CommandArgument);
            obj.staIsActive = true;
            obj.staIsDeleted = false;
            obj.stateStatusUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    StateSelectAll();
                    StateSelectArchiveAll();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }
        else if (e.CommandName == "permanentlydelete")
        {
            StateBM obj = new StateBM();
            obj.staId = Convert.ToInt32(e.CommandArgument);
            obj.staIsActive = false;
            obj.staIsDeleted = true;
            obj.stateStatusUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    StateSelectAll();
                    StateSelectArchiveAll();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }
    }

    #endregion

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        StateBM obj2 = new StateBM();
        obj2.stateCheckDuplication(txtstateName.Text, -1);
        String returnMsg = obj2.ReturnString;
        if (returnMsg == "")
        {
            StateBM obj = new StateBM();
            obj.staName = txtstateName.Text;
            obj.staCountryId = Convert.ToInt32(ddlcountry.SelectedValue);
            obj.staCompanyId = Convert.ToInt32(Session["OrgUserId"]);
            obj.staIsActive = true;
            obj.staIsDeleted = false;
            obj.staCreatedDate = DateTime.Now;
            obj.InsertState();
            //  DataSet ds = obj.ds;
            if (obj.ReturnBoolean == true)
            {
                Response.Redirect("OrgStateList.aspx?msg=ins");
            }
        }
        else
        {
            if (returnMsg == "stateName")
                lblMsg.Text = CommonModule.msgNameAlreadyExists;
            else
                lblMsg.Text = CommonModule.msgProblemInsertRecord;
        }
    }
}