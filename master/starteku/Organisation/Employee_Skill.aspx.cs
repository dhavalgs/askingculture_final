﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using startetku.Business.Logic;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.Services;
using System.Threading;
using System.Globalization;

public partial class Organisation_Employee_Skill : System.Web.UI.Page
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            Common.WriteLog("GetAllCompetencehelp");
            Skills.InnerHtml = CommonMessages.Welcome + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            GetAllCompetencehelp();
            Common.WriteLog("GetAllpoint");
            GetAllpoint();
            Common.WriteLog("CompetenceSelectAll");
            CompetenceSelectAll();
            Common.WriteLog("subCompetenceSelectAll");
            subCompetenceSelectAll();
            Common.WriteLog("end");
        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion

    #region method

    protected void CompetenceSelectAll()
    {
        try
        {
            CompetenceMasterBM obj = new CompetenceMasterBM();
            obj.comIsActive = true;
            obj.comIsDeleted = false;
            obj.comCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            obj.comJobtype = Convert.ToInt32(Session["userJobType"]);
            obj.GetAllCompetenceMasterEmpskill();
            DataSet ds = obj.ds;

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlcompetence.Items.Clear();

                    ddlcompetence.DataSource = ds.Tables[0];
                    ddlcompetence.DataTextField = "comCompetence";
                    ddlcompetence.DataValueField = "comId";
                    ddlcompetence.DataBind();

                    //ddlcompetence.Items.Insert(0, new ListItem(CommonModule.dropDownSelectDepartments, CommonModule.dropDownZeroValue));
                }
                else
                {
                    ddlcompetence.Items.Clear();
                    ddlcompetence.Items.Insert(0, new ListItem(CommonModule.dropDownSelectDepartments, CommonModule.dropDownZeroValue));
                }
            }
            else
            {
                ddlcompetence.Items.Clear();
                ddlcompetence.Items.Insert(0, new ListItem(CommonModule.dropDownSelectDepartments, CommonModule.dropDownZeroValue));
            }
        }
        catch (DataException ex)
        {
            Common.WriteLog("error in CompetenceSelectAll" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }
    protected void subCompetenceSelectAll()
    {
        try
        {
            SubCompetenceBM obj = new SubCompetenceBM();
            obj.subIsActive = true;
            obj.subIsDeleted = false;
            obj.subComId = Convert.ToInt32(ddlcompetence.SelectedValue);
            obj.GetAllSubCompetenceMaster();
            DataSet ds = obj.ds;
            ViewState["data"] = ds;

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    rep_competence.DataSource = ds;
                    rep_competence.DataBind();
                    rep_competencehigh.DataSource = ds;
                    rep_competencehigh.DataBind();
                    rep_tabmenu.DataSource = ds;
                    rep_tabmenu.DataBind();
                    rep_content.DataSource = ds;
                    rep_content.DataBind();
                }
                else
                {
                    rep_competence.DataSource = null;
                    rep_competence.DataBind();
                    rep_tabmenu.DataSource = null;
                    rep_tabmenu.DataBind();
                    rep_content.DataSource = null;
                    rep_content.DataBind();
                    rep_competencehigh.DataSource = null;
                    rep_competencehigh.DataBind();
                }
            }
            else
            {
                rep_competence.DataSource = null;
                rep_competence.DataBind();
                rep_tabmenu.DataSource = null;
                rep_tabmenu.DataBind();
                rep_content.DataSource = null;
                rep_content.DataBind();
                rep_competencehigh.DataSource = null;
                rep_competencehigh.DataBind();
            }
        }
        catch (Exception ex)
        {
            Common.WriteLog("error in subCompetenceSelectAll" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }
    protected void ddlcompetence_SelectedIndexChanged(object sender, EventArgs e)
    {
        // GetState(Convert.ToInt32(ddlcountry.SelectedValue));
        subCompetenceSelectAll();
    }
    protected void insert(Int32 subId)
    {
        String filePath = "";
        if (FileUpload1.HasFile)
        {
            filePath = System.DateTime.Now.ToString("MMddyyhhmmss") + "_" + FileUpload1.PostedFile.FileName;
            FileUpload1.SaveAs(Server.MapPath("../") + "Log/upload/Document/" + System.IO.Path.GetFileName(filePath.Replace(' ', '0')));

            EmployeeSkillBM obj = new EmployeeSkillBM();
            obj.skillSubId = subId;
            obj.skillComId = Convert.ToInt32(ddlcompetence.SelectedValue);
            obj.skillDoc = System.IO.Path.GetFileName(filePath.Replace(' ', '0'));
            obj.skillStatus = 3;
            obj.skillUserId = Convert.ToInt32(Session["OrgUserId"]);
            obj.skillIsActive = true;
            obj.skillIsDeleted = false;
            obj.skillCreatedDate = DateTime.Now;
            obj.skillCreateBy = 0;
            obj.skillPoint = 0;
            obj.skillCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            obj.InsertEmployeeSkill();
            if (obj.ReturnBoolean == true)
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('Save SussessFully !');</script>", false);
            }

        }
        else
        {
            //  ScriptManager.RegisterStartupScript(Page, this.GetType(), "alert", string.Format("alert('{1}', '{0}');", "fgh", Title), true);
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('Please Pdf Attachments.!');</script>", false);

        }

    }
    protected void inserthelp(Int32 subId)
    {
        Competence_HelpBM obj = new Competence_HelpBM();
        obj.helpUserIdHelper = 0;
        obj.helpUserIdHelped = Convert.ToInt32(Session["OrgUserId"]);
        obj.helpkillStatus = 0;
        obj.helpMessage = "";
        obj.comIsActive = true;
        obj.comIsDeleted = false;
        obj.comCreatedDate = DateTime.Now;
        obj.comCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.helpsubComId = subId;
        obj.helpstatus = true;
        obj.InsertCompetence_Help();
        GetAllCompetencehelp();
        subCompetenceSelectAll();
        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('Save SussessFully !');</script>", false);
    }
    protected void GetAllCompetencehelp()
    {
        try
        {
            Competence_HelpBM obj = new Competence_HelpBM();
            obj.comIsActive = true;
            obj.comIsDeleted = false;
            obj.comCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            obj.GetAllCompetencehelp();
            DataSet ds = obj.ds;
            ViewState["help"] = ds;
        }
        catch (Exception ex)
        {
            Common.WriteLog("error in GetAllCompetencehelp" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }
    protected void GetAllpoint()
    {
        try
        {
            PointBM obj = new PointBM();
            obj.PointIsStatus = true;
            obj.PointIsDeleted = false;
            obj.PointCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            obj.GetAllPoint();
            DataSet ds = obj.ds;
            ViewState["point"] = ds;
        }
        catch (Exception ex)
        {
            Common.WriteLog("error in GetAllpoint" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }
    #endregion

    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        Int32 subId = 0;
        foreach (RepeaterItem item in rep_competence.Items)
        {
            CheckBox cbInterest = (CheckBox)item.FindControl("checkbox1");
            bool isChecked = cbInterest.Checked;
            if (isChecked == true)
            {
                subId = Convert.ToInt32(((HiddenField)item.FindControl("subId")).Value);
                insert(subId);
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('Save SussessFully !');</script>", false);
            }

        }
        if (subId == 0)
        {
            foreach (RepeaterItem itemhigh in rep_competencehigh.Items)
            {
                CheckBox cbInterest = (CheckBox)itemhigh.FindControl("checkbox1");
                bool isChecked = cbInterest.Checked;
                if (isChecked == true)
                {
                    subId = Convert.ToInt32(((HiddenField)itemhigh.FindControl("subId")).Value);
                    insert(subId);
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('Save SussessFully !');</script>", false);
                }

            }
        }

    }
    protected void btnhelp_click(object sender, EventArgs e)
    {
        Button obj = (Button)sender;
        int sid = Convert.ToInt32(obj.CommandArgument);
        inserthelp(sid);
    }
    protected void btmAll_click(object sender, EventArgs e)
    {
        DataSet dtlocal = new DataSet();
        dtlocal = (DataSet)ViewState["point"];
        //DataView dv = new DataView();
        //dv = dtlocal.Tables[0].DefaultView;
        //dv.RowFilter = "userJobType = '" + Convert.ToInt32(Session["userJobType"]) + "'";
        //DataTable dtitm = dv.ToTable();
        //DataSet ds = new DataSet();
        // ds.Tables.Add(dtitm);
        rptlocal.DataSource = dtlocal;
        rptlocal.DataBind();
        mpe.Show();
    }
    protected void btnlocal_click(object sender, EventArgs e)
    {
        try
        {
            DataSet dtlocal = new DataSet();
            dtlocal = (DataSet)ViewState["point"];
            DataView dv = new DataView();
            dv = dtlocal.Tables[0].DefaultView;
            dv.RowFilter = "userJobType = '" + Convert.ToInt32(Session["userJobType"]) + "'";
            DataTable dtitm = dv.ToTable();
            DataSet ds = new DataSet();
            ds.Tables.Add(dtitm);
            rptlocal.DataSource = ds;
            rptlocal.DataBind();
            mpe.Show();
        }
        catch (Exception ex)
        {
        }
    }
    #endregion

    #region Repeater
    protected void R1_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (rpllocalhigh.Items.Count < 1)
            {
                if (e.Item.ItemType == ListItemType.Footer)
                {
                    HtmlTableRow tr = (HtmlTableRow)e.Item.FindControl("Emptyrow");
                    tr.Visible = true;
                }
            }
            DataSet ds1 = (DataSet)ViewState["help"];
            DataSet dsdata = (DataSet)ViewState["data"];
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField hbsubId = e.Item.FindControl("subId") as HiddenField;
                Label lbpoint = e.Item.FindControl("lbpoint") as Label;
                //Label lblsubcom = e.Item.FindControl("lblsubcom") as Label;
                DataView dv = new DataView();
                dv = ds1.Tables[0].DefaultView;
                dv.RowFilter = "helpUserIdHelped = '" + Convert.ToInt32(Session["OrgUserId"]) + "'";
                DataTable dtitm = dv.ToTable();
                DataSet ds = new DataSet();
                ds.Tables.Add(dtitm);
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            if (Convert.ToInt32(hbsubId.Value) == Convert.ToInt32(ds.Tables[0].Rows[i]["helpsubComId"]))
                            {
                                Button btnhelp = e.Item.FindControl("btnhelp") as Button;
                                btnhelp.Visible = false;
                                //lbpoint.Text = (Convert.ToString(ds.Tables[1].Rows[i]["skillPoint"]));
                                //lblsubcom.ForeColor = System.Drawing.Color.Green;
                                //((HtmlGenericControl)(e.Item.FindControl("myDiv"))).Attributes["class"] = "Completed_satus";
                                //((CheckBox)(e.Item.FindControl("checkbox"))).Visible = false;
                            }
                        }
                    }
                }

                DataView dvdata = new DataView();
                dvdata = dsdata.Tables[1].DefaultView;
                dvdata.RowFilter = "PointUserId = '" + Convert.ToInt32(Session["OrgUserId"]) + "'";
                DataTable dtitmdata = dvdata.ToTable();
                DataSet dsdatanew = new DataSet();
                dsdatanew.Tables.Add(dtitmdata);
                if (dsdatanew != null)
                {
                    if (dsdatanew.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < dsdatanew.Tables[0].Rows.Count; i++)
                        {
                            if (Convert.ToInt32(hbsubId.Value) == Convert.ToInt32(dsdatanew.Tables[0].Rows[i]["pintSubCompetance"]))
                            {
                                if (!String.IsNullOrEmpty(Convert.ToString(dsdatanew.Tables[0].Rows[i]["PointAnsCount"])))
                                {
                                    lbpoint.Text = (Convert.ToString(dsdatanew.Tables[0].Rows[i]["PointAnsCount"]));
                                }
                                else
                                {
                                    lbpoint.Text = "0";
                                }

                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Common.WriteLog("error in R1_ItemDataBound" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }
    protected void Repeater1_OnItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Local")
        {
            try
            {
                string col1 = ((Label)e.Item.FindControl("lbpoint")).Text;
                Int32 sid = Convert.ToInt32(e.CommandArgument);
                DataSet dtlocal = new DataSet();
                dtlocal = (DataSet)ViewState["point"];
                DataView dv = new DataView();
                dv = dtlocal.Tables[0].DefaultView;
                //dv.RowFilter = "userJobType = '" + Convert.ToInt32(Session["userJobType"]) + "'";
                dv.RowFilter = "userJobType = '" + Convert.ToInt32(Session["userJobType"]) + "' AND PointAnsCount < '" + Convert.ToInt32(col1) + "' AND pintSubCompetance = '" + Convert.ToInt32(sid) + "'";
                DataTable dtitm = dv.ToTable();
                DataSet ds = new DataSet();
                ds.Tables.Add(dtitm);
                //if (Common.IsDatasetvalid(ds))
                //{
                rptlocal.DataSource = ds;
                rptlocal.DataBind();
                mpe.Show();
                //}
                //else
                //{
                //string res = "<script>alert('" + CommonModule.msgRecordNotfound + "');</script>";

                //}
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in Repeater1_OnItemCommand" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            }
        }

        else if (e.CommandName == "All")
        {
            string col1 = ((Label)e.Item.FindControl("lbpoint")).Text;
            Int32 sid = Convert.ToInt32(e.CommandArgument);

            DataSet dtlocal = new DataSet();
            dtlocal = (DataSet)ViewState["point"];
            DataView dv = new DataView();
            dv = dtlocal.Tables[0].DefaultView;
            dv.RowFilter = "PointAnsCount < '" + Convert.ToInt32(col1) + "' AND pintSubCompetance = '" + Convert.ToInt32(sid) + "'";
            DataTable dtitm = dv.ToTable();
            DataSet ds = new DataSet();
            ds.Tables.Add(dtitm);
            //if (Common.IsDatasetvalid(ds))
            //{
            rptlocal.DataSource = ds;
            rptlocal.DataBind();
            mpe.Show();
            //}
            //else
            //{
            //   
            //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script>alert('" + CommonModule.msgRecordNotfound + "');</script>", false);

            //}
        }

    }
    protected void rep_competencehigh_OnItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Local")
        {
            try
            {
                string col1 = ((Label)e.Item.FindControl("lbpoint")).Text;
                Int32 sid = Convert.ToInt32(e.CommandArgument);
                DataSet dtlocal = new DataSet();
                dtlocal = (DataSet)ViewState["point"];
                DataView dv = new DataView();
                dv = dtlocal.Tables[0].DefaultView;
                //dv.RowFilter = "userJobType = '" + Convert.ToInt32(Session["userJobType"]) + "'";
                dv.RowFilter = "userJobType = '" + Convert.ToInt32(Session["userJobType"]) + "' AND PointAnsCount > '" + Convert.ToInt32(col1) + "' AND pintSubCompetance = '" + Convert.ToInt32(sid) + "'";
                DataTable dtitm = dv.ToTable();
                DataSet ds = new DataSet();
                ds.Tables.Add(dtitm);
                //if (Common.IsDatasetvalid(ds))
                //{
                rpllocalhigh.DataSource = ds;
                rpllocalhigh.DataBind();
                //mpe.Show();
                ModalPopupExtender1.Show();
                //}


            }
            catch (Exception ex)
            {
                Common.WriteLog("error in Repeater1_OnItemCommand" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            }
        }

        else if (e.CommandName == "All")
        {
            string col1 = ((Label)e.Item.FindControl("lbpoint")).Text;
            Int32 sid = Convert.ToInt32(e.CommandArgument);

            DataSet dtlocal = new DataSet();
            dtlocal = (DataSet)ViewState["point"];
            DataView dv = new DataView();
            dv = dtlocal.Tables[0].DefaultView;
            dv.RowFilter = "PointAnsCount > '" + Convert.ToInt32(col1) + "' AND pintSubCompetance = '" + Convert.ToInt32(sid) + "'";
            DataTable dtitm = dv.ToTable();
            DataSet ds = new DataSet();
            ds.Tables.Add(dtitm);
            //if (Common.IsDatasetvalid(ds))
            //{
            rpllocalhigh.DataSource = ds;
            rpllocalhigh.DataBind();
            ModalPopupExtender1.Show();
            //}

        }

    }
    protected void rep_competencehigh_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (rpllocalhigh.Items.Count < 1)
            {
                if (e.Item.ItemType == ListItemType.Footer)
                {
                    HtmlTableRow tr = (HtmlTableRow)e.Item.FindControl("Emptyrow");
                    tr.Visible = true;

                }
            }
            DataSet ds1 = (DataSet)ViewState["help"];
            DataSet dsdata = (DataSet)ViewState["data"];
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField hbsubId = e.Item.FindControl("subId") as HiddenField;
                Label lbpoint = e.Item.FindControl("lbpoint") as Label;
                //Label lblsubcom = e.Item.FindControl("lblsubcom") as Label;
                DataView dv = new DataView();
                dv = ds1.Tables[0].DefaultView;
                dv.RowFilter = "helpUserIdHelped = '" + Convert.ToInt32(Session["OrgUserId"]) + "'";
                DataTable dtitm = dv.ToTable();
                DataSet ds = new DataSet();
                ds.Tables.Add(dtitm);
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            if (Convert.ToInt32(hbsubId.Value) == Convert.ToInt32(ds.Tables[0].Rows[i]["helpsubComId"]))
                            {
                                Button btnhelp = e.Item.FindControl("btnhelp") as Button;
                                btnhelp.Visible = false;
                                //lbpoint.Text = (Convert.ToString(ds.Tables[1].Rows[i]["skillPoint"]));
                                //lblsubcom.ForeColor = System.Drawing.Color.Green;
                                //((HtmlGenericControl)(e.Item.FindControl("myDiv"))).Attributes["class"] = "Completed_satus";
                                //((CheckBox)(e.Item.FindControl("checkbox"))).Visible = false;
                            }
                        }
                    }
                }

                DataView dvdata = new DataView();
                dvdata = dsdata.Tables[1].DefaultView;
                dvdata.RowFilter = "PointUserId = '" + Convert.ToInt32(Session["OrgUserId"]) + "'";
                DataTable dtitmdata = dvdata.ToTable();
                DataSet dsdatanew = new DataSet();
                dsdatanew.Tables.Add(dtitmdata);
                if (dsdatanew != null)
                {
                    if (dsdatanew.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < dsdatanew.Tables[0].Rows.Count; i++)
                        {
                            if (Convert.ToInt32(hbsubId.Value) == Convert.ToInt32(dsdatanew.Tables[0].Rows[i]["pintSubCompetance"]))
                            {
                                if (!String.IsNullOrEmpty(Convert.ToString(dsdatanew.Tables[0].Rows[i]["PointAnsCount"])))
                                {
                                    lbpoint.Text = (Convert.ToString(dsdatanew.Tables[0].Rows[i]["PointAnsCount"]));
                                }
                                else
                                {
                                    lbpoint.Text = "0";
                                }

                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Common.WriteLog("error in R1_ItemDataBound" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }
    protected void rptlocal_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (rptlocal.Items.Count < 1)
            {
                if (e.Item.ItemType == ListItemType.Footer)
                {
                    HtmlTableRow tr = (HtmlTableRow)e.Item.FindControl("Emptyrow");
                    tr.Visible = true;

                }
            }
            //DataSet ds = (DataSet)ViewState["point"];
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField userId = e.Item.FindControl("userId") as HiddenField;
                HiddenField pintSubCompetance = e.Item.FindControl("pintSubCompetance") as HiddenField;
                HiddenField hdnhelpstatus = e.Item.FindControl("hdnhelpstatus") as HiddenField;
                if (!String.IsNullOrEmpty(hdnhelpstatus.Value))
                {
                    if (Convert.ToBoolean(hdnhelpstatus.Value) == true)
                    {
                        HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("tdControl");
                        td.Attributes.Add("style", "background-color:Greenyellow;");

                    }
                    else
                    {
                        HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("tdControl");
                        td.Attributes.Add("style", "background-color:Red;");
                    }
                }
                //else
                //{
                //    HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("tdControl");
                //    td.Attributes.Add("style", "background-color:Red;");

                //}
                //DataView dv = new DataView();
                //dv = ds1.Tables[0].DefaultView;
                //dv.RowFilter = "helpUserIdHelped = '" + Convert.ToInt32(Session["OrgUserId"]) + "'";
                //DataTable dtitm = dv.ToTable();
                //DataSet ds = new DataSet();
                //ds.Tables.Add(dtitm);
                //if (ds != null)
                //{
                //    if (ds.Tables[0].Rows.Count > 0)
                //    {
                //        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                //        {
                //            //if (Convert.ToInt32(userId.Value) == Convert.ToInt32(ds.Tables[0].Rows[i]["userId"]))
                //            if ((Convert.ToInt32(userId.Value) == Convert.ToInt32(ds.Tables[0].Rows[i]["userId"]))) //&& (Convert.ToInt32(pintSubCompetance.Value) == Convert.ToInt32(ds.Tables[1].Rows[i]["helpstatus"])))
                //            {
                //                if (ds.Tables[1].Rows.Count > 0)
                //                {
                //                    for (int j = 0; j < ds.Tables[1].Rows.Count; j++)
                //                    {
                //                        if ((Convert.ToInt32(userId.Value) == Convert.ToInt32(ds.Tables[1].Rows[j]["helpuserIdhelped"])) && (true == Convert.ToBoolean(ds.Tables[1].Rows[j]["helpstatus"])))
                //                        {
                //                            HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("tdControl");
                //                            td.Attributes.Add("style", "background-color:Greenyellow;");
                //                        }
                //                        else
                //                        {
                //                            HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("tdControl");
                //                            td.Attributes.Add("style", "background-color:Red;");
                //                        }
                //                    }
                //                }

                //            }
                //        }
                //    }
                //}

            }
        }
        catch (Exception ex)
        {
            Common.WriteLog("error in rptlocal_ItemDataBound" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }
    protected void rptlocal_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "popupcal")
        {
            try
            {
                HiddenField hdnhelpstatus = e.Item.FindControl("hdnhelpstatus") as HiddenField;
                if (!String.IsNullOrEmpty(hdnhelpstatus.Value))
                {
                    if (Convert.ToBoolean(hdnhelpstatus.Value) == true)
                    {
                        HiddenField pintSubCompetance = e.Item.FindControl("pintSubCompetance") as HiddenField;
                        HiddenField userFirstName = e.Item.FindControl("userFirstName") as HiddenField;
                        Int32 userId = Convert.ToInt32(e.CommandArgument);
                        Session["userId"] = Convert.ToInt32(userId);
                        Session["sid"] = pintSubCompetance.Value;
                        Session["chechtype"] = "0";
                        txtcname.Text = userFirstName.Value;
                        mpecal.Show();

                    }
                    else
                    {

                    }
                }
                else
                {
                    HiddenField pintSubCompetance = e.Item.FindControl("pintSubCompetance") as HiddenField;
                    HiddenField userFirstName = e.Item.FindControl("userFirstName") as HiddenField;
                    Int32 userId = Convert.ToInt32(e.CommandArgument);
                    Session["userId"] = Convert.ToInt32(userId);
                    Session["sid"] = pintSubCompetance.Value;
                    txtcname.Text = userFirstName.Value;
                    Session["chechtype"] = "0";
                    mpecal.Show();
                }
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in rptlocal_ItemCommand" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            }
        }
    }
    protected void rpllocalhigh_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (rpllocalhigh.Items.Count < 1)
            {
                if (e.Item.ItemType == ListItemType.Footer)
                {
                    HtmlTableRow tr = (HtmlTableRow)e.Item.FindControl("Emptyrow");
                    tr.Visible = true;
                }
            }

            //DataSet ds = (DataSet)ViewState["point"];
            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    HiddenField userId = e.Item.FindControl("userId") as HiddenField;
            //    HiddenField pintSubCompetance = e.Item.FindControl("pintSubCompetance") as HiddenField;
            //    HiddenField hdnhelpstatus = e.Item.FindControl("hdnhelpstatus") as HiddenField;
            //    if (!String.IsNullOrEmpty(hdnhelpstatus.Value))
            //    {
            //        if (Convert.ToBoolean(hdnhelpstatus.Value) == true)
            //        {
            //            HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("tdControl");
            //            td.Attributes.Add("style", "background-color:Greenyellow;");

            //        }
            //        else
            //        {
            //            HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("tdControl");
            //            td.Attributes.Add("style", "background-color:Red;");
            //        }
            //    }
            //}
        }
        catch (Exception ex)
        {
            Common.WriteLog("error in rptlocal_ItemDataBound" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }
    protected void rpllocalhigh_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "popupcal")
        {
            try
            {
                HiddenField hdnhelpstatus = e.Item.FindControl("hdnhelpstatus") as HiddenField;
                if (!String.IsNullOrEmpty(hdnhelpstatus.Value))
                {
                    if (Convert.ToBoolean(hdnhelpstatus.Value) == true)
                    {
                        HiddenField pintSubCompetance = e.Item.FindControl("pintSubCompetance") as HiddenField;
                        HiddenField userFirstName = e.Item.FindControl("userFirstName") as HiddenField;
                        Int32 userId = Convert.ToInt32(e.CommandArgument);
                        Session["userId"] = Convert.ToInt32(userId);
                        Session["sid"] = pintSubCompetance.Value;
                        Session["chechtype"] = "1";
                        txtcname.Text = userFirstName.Value;
                        mpecal.Show();
                    }
                    else
                    {

                    }
                }
                else
                {
                    HiddenField pintSubCompetance = e.Item.FindControl("pintSubCompetance") as HiddenField;
                    HiddenField userFirstName = e.Item.FindControl("userFirstName") as HiddenField;
                    Int32 userId = Convert.ToInt32(e.CommandArgument);
                    Session["userId"] = Convert.ToInt32(userId);
                    Session["sid"] = pintSubCompetance.Value;
                    Session["chechtype"] = "1";
                    txtcname.Text = userFirstName.Value;
                    mpecal.Show();
                }
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in rptlocal_ItemCommand" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            }
        }
    }
    #endregion

    #region WebMethod
    [WebMethod(EnableSession = true)]
    public static string UpdateData(string txtto, string txtsubject, string txtmessge)
    {
        string msg = string.Empty;

        PointBM objAtt = new PointBM();
        objAtt.PointuserIDhelper = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        objAtt.PointCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
        objAtt.PointDate = Convert.ToDateTime(CommonModule.FormatdtEnter(txtsubject));
        objAtt.Pointmessage = txtmessge;
        objAtt.PointuserIDhelped = Convert.ToInt32(HttpContext.Current.Session["userId"]);
        objAtt.PointComId = Convert.ToInt32(HttpContext.Current.Session["sid"]);
        string checktype = Convert.ToString(HttpContext.Current.Session["chechtype"]);
        objAtt.UpdatePoint(checktype);
        if (!objAtt.UpdatePoint(checktype))
        {
            msg = "false";

        }
        else
        {
            msg = "true";
            HttpContext.Current.Session["userId"] = "";
            HttpContext.Current.Session["sid"] = "";
        }


        return msg;
    }
    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        //string language = "French";
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Denish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion
}