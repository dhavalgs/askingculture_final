﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using System.Configuration;
using System.Drawing;
using starteku_BusinessLogic.Model;
using startetku.Business.Logic;
using System.Web.UI.HtmlControls;
using System.Threading;
using System.Globalization;

public partial class Organisation_ActivityRequestView : System.Web.UI.Page
{
    string temp = "0";
    static string temp1 = "0";

    /*
     * About Activity Request Stauts:  2017 04 01
     
     * We need to change status on an activity
    Available” ==1
            Activity can be requested
            Should be auto set when creating a public activity
    Requested   ==2
            Activity have been requested by user
            Should be auto set when user request a public activity
    Request approved ==3
            Manager have approved requested activity
            Should be auto set when manager approve users activity request
    Ongoing ==4
            Should be auto set when start date have been reached
    Completed ==5
            employee flag activity has been completed
            Should be set manuel by user (or manager)
    Completed approval ==6
            Manager have approved that activity have been completed by user
            Should be set manual by manager (or system owner)
     * 
     * REJECTED == 7
    */
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }

        if (!IsPostBack)
        {

            hdnLoginUserID.Value = Convert.ToString(Session["OrgUserId"]);
            hdnUserType.Value = Convert.ToString(Session["OrguserType"]);

            if (!String.IsNullOrEmpty(Request.QueryString["notif"]))
            {
               UpdateNotification();
            }
            ActivityStausDropDownList();


            int hdnAcrCreaterId1 = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
            hdnAcrCreaterId.Value = Convert.ToString(hdnAcrCreaterId1);
            var localResourceObject = GetLocalResourceObject("Welcome.Text");

            if (localResourceObject != null)
                Employee.InnerHtml = localResourceObject.ToString() + "  " + Convert.ToString(Session["OrgUserName"]) + "!";

            temp = Convert.ToString(Request.QueryString["id"]);
            temp1 = temp;
            GetAllJobTypeByCompanyId();
            GetAllDivisionByCompanyId();
            GetAllActivityCategory();
            CompetenceSelectAll();

            GetActivityData();

           


            if (!String.IsNullOrEmpty(Request.QueryString["Vw"]))
            {

                txtComment.Enabled = false;
                //ddlstastus.Enabled = false;
                ddlsttus.Attributes.Add("disabled", "disabled");

                btnsubmit.Visible = false;
                btnCancel.Visible = true;
            }

            txtComments.Attributes["placeholder"] = GetLocalResourceObject("TYPEYOURCOMMENTHERE.Text").ToString();
            

        }

    }

    private void UpdateNotification()
    {
        var db=new startetkuEntities1();
        try
        {
            if (!String.IsNullOrEmpty(Request.QueryString["notif"]))
            {
                var notId = Convert.ToInt32(Request.QueryString["notif"]);
                var notObj = db.Notifications.FirstOrDefault(o => o.notId == notId);
                if (notObj != null)
                {
                    notObj.notIsActive = false;
                    db.SaveChanges();
                }
            }

        }
        catch (Exception)
        {
                
            throw;
        }
    }

    private void ActivityStausDropDownList()
    {


        
        ddlsttus.Items.Add(new ListItem("APPROVE", "3"));
        ddlsttus.Items.Add(new ListItem("REJECT", "7"));
        ddlsttus.SelectedValue = "1";
    }

    protected void CompetenceSelectAll()
    {
        CompetenceMasterBM obj = new CompetenceMasterBM();
        obj.comIsActive = true;
        obj.comIsDeleted = false;
        obj.comCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllCompetenceMasterAdd();
        DataSet ds = obj.ds;
        if (Convert.ToString(Session["Culture"]) == "Danish")
        {
            ds.Tables[0].Columns["catName"].ColumnName = "abcd";
            ds.Tables[0].Columns["catNameDN"].ColumnName = "catName";

        }
        ViewState["ds"] = ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddCompetence.DataSource = ds.Tables[0];
                ddCompetence.DataTextField = "comCompetence";
                ddCompetence.DataValueField = "comId";
                ddCompetence.DataBind();
                ddCompetence.Items.Insert(0, "Select Competences");
                ddCompetence.SelectedValue = "0";


            }
            else
            {
                ddCompetence.DataSource = null;
                ddCompetence.DataBind();
            }
        }
        else
        {

        }
    }

    public void GetActivityData()
    {
        var actReqId = Request.QueryString["actReqId"];
        var actReqIdInt = 0;
        if (!string.IsNullOrWhiteSpace(actReqId))
        {
            actReqIdInt = Convert.ToInt32(actReqId);
        }





        var actObj = new GetActivityLists_Result();
        actObj.ActReqId = actReqIdInt;

        var getActData = ActivityMasterLogic.GetActivityList(actObj,1,1).FirstOrDefault(o => o.ActReqId == actReqIdInt);// db.ActivityMasters.FirstOrDefault(o => o.ActId == id);



        if (getActData != null)
        {
            hdnActReqUserId.Value = Convert.ToString(getActData.ActReqUserId);
            if (getActData.ActReqStatus == "3"|| getActData.ActReqStatus == "2")
            {
                //btnRequest.Visible = false;
            }

            try
            {
                spname.InnerHtml = UserBM.GetUserById(Convert.ToInt32(getActData.ActReqUserId))[0].UserFirstName;
            }
            catch (Exception)
            {

                spname.InnerHtml = "";
            }


            txtActName.Text = getActData.ActName;
            txtActCost.Text = Convert.ToString(getActData.ActCost);

            txtActEndDate.Text = getActData.ActEndDate.ToString("dd/MM/yyyy");
            txtActStartDate.Text = getActData.ActStartDate.ToString("dd/MM/yyyy");

            if (Convert.ToString(getActData.ActIsPublic) == "True")
            {
                rdoPublic.Items[0].Selected = true;
                rdoPublic.Items[1].Selected = false;
                // rdoGender.SelectedIndex = 0;
            }

            if (Convert.ToString(getActData.ActIsActive) == "True")
            {
                rdoEnabled.Items[0].Selected = true;
                rdoEnabled.Items[1].Selected = false;
                // rdoGender.SelectedIndex = 0;
            }

            if (Convert.ToString(getActData.ActIsComp) == "True")
            {
                chkIsCompDevDoc.Items[0].Selected = true;
                chkIsCompDevDoc.Items[1].Selected = false;
                // rdoGender.SelectedIndex = 0;
            }

            chkListDivision.SelectedValue = Convert.ToString(getActData.ActDivId);
            chkListJobtype.SelectedValue = Convert.ToString(getActData.ActJobId);
            ddCompetence.SelectedValue = Convert.ToString(getActData.ActCompId);
            ddActCate.SelectedValue = Convert.ToString(getActData.ActCatRefId);

            rdoLevel.SelectedValue = getActData.ActCompLevel;

            txtActTags.Text = getActData.ActTags;
            txtActRequirements.Text = getActData.ActRequirements;

            txtActDescription.Text = getActData.ActDescription;

            txtJustification.Text = getActData.ActReqJustification;
            txtExpctBehavChange.Text = getActData.ActReqExpJustBehav;

            hdnActReqIdInt.Value = actReqId;
            hdnActId.Value = Convert.ToString(getActData.ActId);

            //var ddlStatusSelectedValue = "1";
            //if (getActData.ActReqStatus == "ACCEPTED")
            //{
            //    ddlStatusSelectedValue = "1";
            //}
            //if (getActData.ActReqStatus == StringFor.COMPLETED)
            //{
            //    ddlStatusSelectedValue = "2";
            //}
            //if (getActData.ActReqStatus == StringFor.REJECTED)
            //{
            //    ddlStatusSelectedValue = "3";
            //}
            ddlsttus.SelectedValue = getActData.ActReqStatus;
            if (getActData.ActReqStatus == "5")
            {
                ddlsttus.Items.Add(new ListItem(StringFor.COMPLETED_APPROVE, "6"));
            }
            txtJustification.Enabled = false;
            txtExpctBehavChange.Enabled = false;
        }
        else
        {
            Response.Redirect("pending_helprequest.aspx?msg=err");
            lblMsg.Text = @"Oops! Activity is either removed or something wrong, please contact higher authority.";
            lblMsg.ForeColor = Color.White;
        }
    }

    protected void GetAllActivityCategory()
    {

        var db = new startetkuEntities1();
        var getAllActCat = db.ActivityCategoryMasters.ToList();

        if (getAllActCat.Any())
        {

            ddActCate.DataSource = getAllActCat;
            ddActCate.DataTextField = "ActCatName";
            ddActCate.DataValueField = "ActCatId";
            ddActCate.DataBind();
            ddActCate.Items.Insert(0, "All");


        }
    }
    protected void GetAllJobTypeByCompanyId()
    {
        JobTypeBM obj = new JobTypeBM();
        obj.jobIsActive = true;
        obj.jobIsDeleted = false;
        obj.jobCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);


        obj.GetAllJobTypeByCompanyId();


        DataSet ds = obj.ds;
        if (Convert.ToString(Session["Culture"]) == "Danish")
        {
            ds.Tables[0].Columns["jobName"].ColumnName = "abcd1";
            ds.Tables[0].Columns["jobNameDN"].ColumnName = "jobName";

        }
        ViewState["ds"] = ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                chkListJobtype.DataSource = ds.Tables[0];
                chkListJobtype.DataTextField = "jobName";
                chkListJobtype.DataValueField = "jobId";
                chkListJobtype.DataBind();
                chkListJobtype.Items.Insert(0, "All");
                chkListDivision.Items.Insert(0, "All");


            }

        }
    }

    protected void GetAllDivisionByCompanyId()
    {
        DivisionBM obj = new DivisionBM();
        obj.depIsActive = true;
        obj.depIsDeleted = false;
        obj.depCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);


        obj.GetAllDivisionsbyCompanyId();


        DataSet ds = obj.ds;
        if (Convert.ToString(Session["Culture"]) == "Danish")
        {
            ds.Tables[0].Columns["divName"].ColumnName = "abcd";
            ds.Tables[0].Columns["divNameDN"].ColumnName = "divName";

        }
        ViewState["ds"] = ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                chkListDivision.DataSource = ds.Tables[0];
                chkListDivision.DataTextField = "divName";
                chkListDivision.DataValueField = "divId";
                chkListDivision.DataBind();
                chkListDivision.Items.Insert(0, "Select Activity Type");



            }

        }
    }

    protected void btnCancel_click(object sender, EventArgs e)
    {
        Response.Redirect("pending_helprequest.aspx");
    }

    protected void btnsubmit_click(object sender, EventArgs e)
    {
        var db = new startetkuEntities1();
        try
        {
            int id = Convert.ToInt32(Request.QueryString["actReqId"]);
            int i = 0;
            int status = Convert.ToInt32(ddlsttus.SelectedValue);

            if (hdnStatus12.Value.ToString() == status.ToString())
            {

                Response.Redirect("pending_helprequest.aspx");
            }

            var getReqData = db.ActReqMasters.FirstOrDefault(o => o.ActReqId == id);

            if (getReqData != null)
            {
                getReqData.ActReqStatus = ddlsttus.SelectedItem.Value;
                db.SaveChanges();
                InsertNotification(getReqData.ActReqUserId);
                Response.Redirect("pending_helprequest.aspx?msg=upd");
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void btnback_click(object sender, EventArgs e)
    {
        throw new NotImplementedException();
    }

    protected void InsertNotification(int umId)
    {


        UserBM obj1 = new UserBM();
        obj1.SetUserId = Convert.ToInt32(umId);
        obj1.GetUserSettingById();
        DataSet ds1 = obj1.ds;
        if (ds1.Tables[0].Rows.Count > 0)
        {
            if (!String.IsNullOrEmpty(Convert.ToString(ds1.Tables[0].Rows[0]["SetInvitationNotification"])))
            {
                if (Convert.ToBoolean(ds1.Tables[0].Rows[0]["SetNewFileUploadwarning"]))
                {
                    NotificationBM obj = new NotificationBM();
                    string EmailType = "ActivityApproved";
                    obj.nottype = "actRes";

                    if (ddlsttus.SelectedItem.Value == "7")
                    {
                        EmailType = "ActivityRejected";
                        obj.nottype = "actRej";
                    }
                    else if (ddlsttus.SelectedItem.Value == "6")
                    {
                        EmailType = "ActivityCompleteApproval";
                        obj.nottype = "actCom";
                    }

                   
                    obj.notsubject = "Has " + ddlsttus.SelectedItem.Text + "ED" + " Activity Request.";
                    obj.notUserId = Convert.ToInt32(Session["OrgUserId"]);
                    obj.notIsActive = true;
                    obj.notIsDeleted = false;
                    obj.notCreatedDate = DateTime.Now;
                    obj.notCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
                    obj.notpage = "ActivityLists.aspx?id=" + Convert.ToString(Session["OrgUserId"]);

                    

                   
                    obj.notToUserId = Convert.ToInt32(umId);
                    obj.InsertNotification();

                   
                    sendActivityEmail(obj.notToUserId, EmailType);
                }

            }
        }
    }
    protected void sendActivityEmail(Int32 touserid, string templateName = "ActivityApproved")
    {
        #region Send mail
        // String subject = "Set Competence for " + Convert.ToString(Session["OrgUserName"]);
        Template template = CommonModule.getTemplatebyname1(templateName, Convert.ToInt32(Session["OrgCreatedBy"]));
        //String confirmMail = CommonModule.getTemplatebyname("Set Competence", touserid);
        String confirmMail = template.TemplateName;
        if (!String.IsNullOrEmpty(confirmMail))
        {
            var localResourceObject = GetLocalResourceObject("Activity_Approved_Email_Subject.Text");
            if (templateName == "ActivityRejected")
            {
                localResourceObject = GetLocalResourceObject("Activity_Reject_Email_Subject.Text");
            }
            if (templateName == "ActivityCompleteApproval")
            {
                localResourceObject = GetLocalResourceObject("ActivityCompleteApproval_Email_Subject.Text");
            }
            if (localResourceObject != null)
            {
                string subject = localResourceObject.ToString();

                var userDetail = UserBM.GetUserByIdLinq(touserid);

                if (userDetail != null)
                {
                  
                        String name = Convert.ToString(userDetail.userFirstName);
                        string fromid = Convert.ToString(ConfigurationManager.AppSettings["fromid"]);
                        String empname = Convert.ToString(Session["OrgUserName"]);
                        string tempString = confirmMail;
                        tempString = tempString.Replace("###name###", name);
                        tempString = tempString.Replace("###empname###", empname);
                        CommonModule.SendMailToUser(userDetail.userEmail, subject, tempString, fromid, Convert.ToString(HttpContext.Current.Session["OrgCompanyId"]));

                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'> generate('information', '<b>'" + RequestSentSuccessful.Value + "'</b><br> ', 'bottomCenter');</script>", false);
                    
                }
            }
        }

        #endregion
    }
    protected void sendmail(Int32 touserid)
    {
        #region Send mail
        // String subject = "Status Competence for " + Convert.ToString(Session["OrgUserName"]);

        Template template = CommonModule.getTemplatebyname1("Status Competence", touserid);
        string confirmMail = template.TemplateName;
        if (!String.IsNullOrEmpty(confirmMail))
        {

            string subject = GetLocalResourceObject("StatusActivityRequest_Email_Subject.Text").ToString();


            UserBM Cust = new UserBM();
            Cust.userId = Convert.ToInt32(touserid);
            // Cust.SelectmanagerByUserId();
            Cust.SelectPasswordByUserId();
            DataSet ds = Cust.ds;
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    UserBM objAtt = new UserBM();
                    objAtt.userIsActive = true;
                    objAtt.userIsDeleted = false;
                    objAtt.userId = Convert.ToInt32(touserid);
                    objAtt.userCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
                    objAtt.GetEmployeeDetailbyPdf();
                    DataSet ds1 = objAtt.ds;
                    #region Competence
                    String Competence = "";
                    

                    #endregion
                    String name = Convert.ToString(ds.Tables[0].Rows[0]["userFirstName"]);
                    string fromid = Convert.ToString(ConfigurationManager.AppSettings["fromid"]);
                    String empname = Convert.ToString(Session["OrgUserName"]);
                    String status = ddlsttus.SelectedValue;
                    if (ddlsttus.SelectedValue == "1")
                    {
                        status = "Accept";
                    }
                    else if (ddlsttus.SelectedValue == "2")
                    {
                        status = "Cancel";
                    }
                    else if (ddlsttus.SelectedValue == "3")
                    {
                        status = "Pending";
                    }
                    string tempString = confirmMail;
                    tempString = tempString.Replace("###name###", name);
                    tempString = tempString.Replace("###empname###", empname);
                    tempString = tempString.Replace("###status###", status);
                    tempString = tempString.Replace("###Table###", Competence);
                    CommonModule.SendMailToUser(Convert.ToString(ds.Tables[0].Rows[0]["userEmail"]), subject, tempString, fromid, Convert.ToString(HttpContext.Current.Session["OrgCompanyId"]));
                }
            }
        }




        #endregion
    }


    #region
    [WebMethod(EnableSession = true)]
    public static GetActivityCommentById_Result[] GetActivityCommentById(string AcrCreaterId, int ActID, int LoginUserID, int UserType)
    {

        int reqID = 0;
        if (String.IsNullOrEmpty(AcrCreaterId))
        {
            reqID = 0;
        }
        else
        {
            reqID = Convert.ToInt32(AcrCreaterId);
        }
        //var actReqId = Request.QueryString["actReqId"];
        var actReqList = ActReqComment.GetActivityCommentById(reqID, ActID, LoginUserID, UserType);

        return actReqList.ToArray();
    }

    [WebMethod(EnableSession = true)]
    public static string insertmessage(string message, string ActReqId, string ActReqUserId, string ActID)
    {



        string msg = string.Empty;
        CompetenceBM obj = new CompetenceBM();


        var db = new startetkuEntities1();


        var actCat = new ActvityReqComment();
        actCat.AcrCompanyID = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
        actCat.AcrCompComm = message;

        actCat.AcrCreaterId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        actCat.AcrComToUserID = Convert.ToInt32(ActReqUserId);
        actCat.ActComActReqId = Convert.ToInt32(ActReqId);
        if (String.IsNullOrEmpty(ActID))
        {
            actCat.ActID = 0;
        }
        else
        {
            actCat.ActID = Convert.ToInt32(ActID);  //swati on 24/01/2018
        }

        actCat.AcrIsActive = true;
        actCat.AcrIsdeleted = false;
        actCat.AcrComCreateDate = CommonUtilities.GetCurrentDateTime();
        actCat.AcrComUpdateDate = CommonUtilities.GetCurrentDateTime();

        actCat.ActUserTypeId = Convert.ToInt32(HttpContext.Current.Session["OrguserType"]);
        actCat.ActUserCreatedById = Convert.ToInt32(HttpContext.Current.Session["OrgCreatedBy"]);

        db.ActvityReqComments.Add(actCat);
        db.SaveChanges();
        msg = "true";


        return msg;
    }


    public class comment
    {
        public string compId { get; set; }

        public string name { get; set; }
        public string comuserid { get; set; }
        public string Image { get; set; }
        public string compcomment { get; set; }
        public string comcreatedate { get; set; }

    }
    #endregion



    #region Culture
    protected override void InitializeCulture()
    {
        // string language = "French";
        string language = Convert.ToString(Session["Culture"]);

        string languageId = "";

        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }

        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion


    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
}