﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using startetku.Business.Logic;
using System.Web.UI.HtmlControls;
using System.Threading;
using System.Globalization;

public partial class Organisation_Chatroom : System.Web.UI.Page
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            //Conatct.InnerHtml = CommonMessages.Welcome + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            Conatct.InnerHtml = GetLocalResourceObject("Welcome.Text").ToString() + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            GetAllEmployeeList1();
            if (!String.IsNullOrEmpty(Request.QueryString["mes"]))
            {
                GetAllmessagebyId(Convert.ToInt32(Request.QueryString["mes"]));
            }
            
        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion

    #region method

    //protected void updatenotification()
    //{
    //    NotificationBM obj = new NotificationBM();
    //    obj.notIsActive = false;
    //    obj.notUpdatedDate = DateTime.Now;
    //    obj.notUserId = Convert.ToInt32(Request.QueryString["id"]);
    //    obj.nottype = "UserAcceptCompetence-Notification";
    //    obj.UpdateNotification();
    //}
    protected void OnTick_RefreshChatList(object sender, EventArgs e)
    {

        if (string.IsNullOrWhiteSpace(Convert.ToString(Session["touser"])))
        {
            return;
        }
        GetAllmessagebyId(Convert.ToInt32(Session["touser"]));
    }
    protected void GetAllEmployeeList1()
    {
        UserBM obj = new UserBM();
        obj.userIsActive = true;
        obj.userIsDeleted = false;
        obj.userType = 2;
        obj.userCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllEmployeebymessge();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                lst_contacts.DataSource = ds.Tables[0];
                lst_contacts.DataBind();

                if (!String.IsNullOrEmpty((Convert.ToString(ds.Tables[0].Rows[0]["userId"]))))
                {
                   // lbllocal.Text = (Convert.ToString(ds.Tables[0].Rows[0]["skillLocal"]));
                    GetAllmessagebyId(Convert.ToInt32(ds.Tables[0].Rows[0]["userId"]));
                    chat.InnerHtml = Convert.ToString(ds.Tables[0].Rows[0]["name"]);
                }
            }
            else
            {
                lst_contacts.DataSource = null;
                lst_contacts.DataBind();
            }
        }
        else
        {
            lst_contacts.DataSource = null;
            lst_contacts.DataBind();
        }
        //Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "alert('');", true);
       
    }

    protected void GetAllEmployeeList()
    {
        UserBM obj = new UserBM();
        obj.userIsActive = true;
        obj.userIsDeleted = false;
        obj.userType = 2;
        obj.userCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllEmployeebymessge();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                lst_contacts.DataSource = ds.Tables[0];
                lst_contacts.DataBind();
            }
            else
            {
                lst_contacts.DataSource = null;
                lst_contacts.DataBind();
            }
        }
        else
        {
            lst_contacts.DataSource = null;
            lst_contacts.DataBind();
        }
    }

    protected void GetAllmessagebyId(Int32 id)
    {
        NotificationBM obj = new NotificationBM();
        obj.mesfromUserId = Convert.ToInt32(Session["OrgUserId"]);
        obj.masToUserId = id;
        obj.GetAllmessagebyId();
        DataSet ds = obj.ds;
        ViewState["chat"] = ds;
        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                rpt_chat.DataSource = ds.Tables[0];
                rpt_chat.DataBind();
            }
            else
            {
                rpt_chat.DataSource = null;
                rpt_chat.DataBind();
            }
        }
        else
        {
            rpt_chat.DataSource = null;
            rpt_chat.DataBind();
        }
       // ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('Save SussessFully !');</script>", false);
    }

    protected void InsertMessage()
    {
        NotificationBM obj = new NotificationBM();
        obj.mesfromUserId = Convert.ToInt32(Session["OrgUserId"]);
        obj.masToUserId = Convert.ToInt32(Session["touser"]);
        obj.messubject = txtmessage.Text;
        obj.masIsActive = true;
        obj.masIsDeleted = false;
        obj.masCreatedDate = DateTime.Now;
        obj.masCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.mastype = "";
        if (obj.InsertMessage())
        {
            txtmessage.Text = "";
        }
        // insertChild(id, level);
        //if (id > 0)
        //{
        //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('Save SussessFully !');</script>", false);
        //}

        NotificationBM obj1 = new NotificationBM();
        obj1.notsubject = "Click here to check your message.";
        obj1.notUserId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        obj1.notIsActive = true;
        obj1.notIsDeleted = false;
        obj1.notCreatedDate = DateTime.Now;
        obj1.notCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
        obj1.notpage = "chart.aspx?mes=" + Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        obj1.nottype = "Message";
        obj1.notToUserId = Convert.ToInt32(HttpContext.Current.Session["masToUserId"]);
        obj1.InsertNotification();
    }
    #endregion

    #region Repeater
    protected void lst_contacts_OnItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Message")
        {
            try
            {
                if (!String.IsNullOrEmpty(Convert.ToString(e.CommandArgument)))
                {
                    LinkButton lb_contact = e.Item.FindControl("lb_contact") as LinkButton;
                    chat.InnerHtml = lb_contact.Text;
                    
                    Session["touser"] = e.CommandArgument;
                    GetAllmessagebyId(Convert.ToInt32(e.CommandArgument));
                    
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>setTimeout(function () { HighlighThis(); }, 500);</script>", false);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>Please select User Name;</script>", false);
                }
                //DataSet ds = (DataSet)ViewState["chat"];
                //if (ds != null)
                //{
                //    if (ds.Tables[0].Rows.Count > 0)
                //    {
                //        rpt_chat.DataSource = ds;
                //        rpt_chat.DataBind();
                //        //dvsubretr.Visible = true;
                //    }
                //    else
                //    {
                //        // dvsubretr.Visible = false;
                //    }
                //}
                //else
                //{
                //    // dvsubretr.Visible = false;
                //}
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in total" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            }

        }
    }
    protected void rpt_chat_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataSet ds = (DataSet)ViewState["chat"];
                HiddenField mesfromUserId = e.Item.FindControl("mesfromUserId") as HiddenField;
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            if (Convert.ToInt32(mesfromUserId.Value) == Convert.ToInt32(Session["OrgUserId"]))
                            {
                                HtmlGenericControl myLi = (HtmlGenericControl)e.Item.FindControl("listItem");
                                myLi.Attributes.Add("class", "reply1");
                            }
                        }

                    }
                }


                //Label lbpoint = e.Item.FindControl("lbpoint") as Label;
                //Label lbllocal = e.Item.FindControl("lbllocal") as Label;
                //Label lblAchive = e.Item.FindControl("lblAchive") as Label;
                //Label lbltarget = e.Item.FindControl("lbltarget") as Label;
                //Label lblset = e.Item.FindControl("lblset") as Label;
                //DataSet ds = (DataSet)ViewState["data"];
                //if (ds != null)
                //{
                //if (ds.Tables[0].Rows.Count > 0)
                //{
                //    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                //    {                            //comId
                //        if (Convert.ToInt32(comId.Value) == Convert.ToInt32(ds.Tables[0].Rows[i]["skillComId"]))
                //        {
                //            if (!String.IsNullOrEmpty((Convert.ToString(ds.Tables[0].Rows[i]["skillLocal"]))))
                //            {
                //                lbllocal.Text = (Convert.ToString(ds.Tables[0].Rows[i]["skillLocal"]));
                //            }
                //            if (!String.IsNullOrEmpty((Convert.ToString(ds.Tables[0].Rows[i]["skillAchive"]))))
                //            {
                //                lblAchive.Text = (Convert.ToString(ds.Tables[0].Rows[i]["skillAchive"]));
                //            }
                //            if (!String.IsNullOrEmpty((Convert.ToString(ds.Tables[0].Rows[i]["skilltarget"]))))
                //            {
                //                lbltarget.Text = (Convert.ToString(ds.Tables[0].Rows[i]["skilltarget"]));
                //            }
                //            if (!String.IsNullOrEmpty((Convert.ToString(ds.Tables[0].Rows[i]["skillSet"]))))
                //            {
                //                //lblset.Text = (Convert.ToString(ds.Tables[0].Rows[i]["skillSet"]));
                //                lblset.Text = "Yes";
                //            }
                //        }
                //    }
                //}

                //
                // }
            }

        }
        catch (Exception ex)
        {
            Common.WriteLog("error in rpt_chat_ItemDataBound" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }
    #endregion

    #region Button
    protected void Button1_Click(object sender, EventArgs e)
    {
                
        InsertMessage();
        GetAllmessagebyId(Convert.ToInt32(Session["touser"]));
      //  ScriptManager.RegisterStartupScript(this, GetType(), "myFunction", "myFunction();", true);
        
    }
    #endregion


    #region Culture
    protected override void InitializeCulture()
    {
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Danish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion
}