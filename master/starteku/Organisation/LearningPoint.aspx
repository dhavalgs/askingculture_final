﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LearningPoint.aspx.cs" MasterPageFile="~/Organisation/OrganisationMaster.master"
    Inherits="Organisation_LearningPoint" EnableEventValidation="false" Culture="auto" meta:resourcekey="PageResource1"
    UICulture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--Chart--%>
    <%--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script src="../Scripts/Scripts/exporting.js"></script>
    <script src="../Scripts/highcharts.js"></script>--%>
    <style type="text/css">
        .report_table td {
            text-align: left;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script>


        function ChangeColor(a) {
            // alert(a);
            $("#" + a).parent().css('background', 'ActiveBorder');
        }
    </script>
    <%-- <asp:UpdatePanel runat="server" ID="up_report">
       <ContentTemplate>--%>

    <asp:HiddenField runat="server" ID="hdnAlpPlpChart" meta:resourcekey="resourceAlpPlarChart" Value="Active learningspoints And Passive learning points Chart"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdnPdpChart" meta:resourcekey="resourcePdpChart" Value="Personal Development Points Chart"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdnCompCatNaME" meta:resourcekey="resourceCompCatNaME" Value="Competence Category Name"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdnBasic" meta:resourcekey="resourceBasic" Value="Basic"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdnActual" meta:resourcekey="resourceActual" Value="Actual"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdnTarget" meta:resourcekey="resourceTarget" Value="Target"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdnreportApproveBy" meta:resourcekey="resourcereportApproveBy" Value="This report was approved by"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdnReportGraph" meta:resourcekey="resourceReportGraph" Value=""></asp:HiddenField>

    <asp:HiddenField runat="server" ID="hdnDocumentShared" meta:resourcekey="resourceDocumentShared" Value="Document Shared"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdndocType" meta:resourcekey="resourcedocType" Value="Document Type"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdndocSharedName" meta:resourcekey="resourcedocSharedName" Value="Name"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdndocName" meta:resourcekey="resourcedocName" Value="Document Name"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdndocDate" meta:resourcekey="resourcedocDate" Value="Date"></asp:HiddenField>

    <asp:HiddenField runat="server" ID="GraphLevel" meta:resourcekey="GraphLevel"></asp:HiddenField>
    <div class="">
        <div class="container" id="div1" runat="server">
            <div class="col-md-8">
                <div class="heading-sec">
                    <h1>
                        <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Report" EnableViewState="false" />
                        <i><span runat="server" id="Settings"></span></i>
                    </h1>
                </div>
            </div>

            <div class="col-md-4">
                <asp:HiddenField runat="server" ID="GraphSubTitle" meta:resourcekey="ResourceGraphSubTitle"></asp:HiddenField>
                <asp:HiddenField runat="server" ID="GraphAxisLevel" meta:resourcekey="ResourceGraphAxisLevel"></asp:HiddenField>
                <asp:HiddenField runat="server" ID="GraphToolTip" meta:resourcekey="ResourceGraphToolTip"></asp:HiddenField>
                <div class="dropdown-example">
                    <ul class="nav nav-pills">
                        <li class="dropdown" style="float: left!important; width: 100%;">
                            <a class="skill_dropdown" id="drop7" role="button" data-toggle="dropdown" href="#"><span
                                id="ddLabel"><%--Original Competence Level--%><asp:Label ID="Label2" runat="server" meta:resourcekey="ResourceGraphLabelLearningPoint"></asp:Label></span><b class="skill_caret"></b> </a>
                            <ul id="ddCompetencesTable" class="dropdown-menu" role="menu" aria-labelledby="drop7"
                                style="width: 100%;">
                                <li role="presentation"><a id="A1" role="menuitem" runat="server" tabindex="-1" href="Report.aspx?dd=o">
                                    <%--  Original Competence Level--%><asp:Label ID="GraphLabelOriginal" runat="server" meta:resourcekey="ResourceGraphLabelOriginal"></asp:Label></a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="Report.aspx?dd=n">
                                    <%--New Competence Level--%><asp:Label ID="GraphLabelNew" runat="server" meta:resourcekey="ResourceGraphLabelNew"></asp:Label></a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="Report.aspx?dd=d">
                                    <%--Development Points--%><asp:Label ID="GraphLabelDevelopment" runat="server" meta:resourcekey="ResourceGraphLabelDevelopment"></asp:Label></a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="Report.aspx?dd=k">
                                    <%--Knowledge Share--%><asp:Label ID="GraphLabelKnowledge" runat="server" meta:resourcekey="ResourceGraphLabelKnowledge"></asp:Label></a></li>
                                <%-- <li role="presentation"><a role="menuitem" tabindex="-1" href="#" onclick="showThisGraph('original')">
                                    Potential for development</a></li>--%>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="LearningPoint.aspx">
                                    <%--Knowledge Share--%><asp:Label ID="GraphLabelLearningPoint" runat="server" meta:resourcekey="ResourceGraphLabelLearningPoint"></asp:Label></a></li>
                                <li role="presentation" class="divider"></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="ReportAll.aspx">
                                    <asp:Label ID="Label3" runat="server" meta:resourcekey="ResourceGraphLabelAll"></asp:Label></a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-12">
                <div id="graph-wrapper">
                    <div id="Original">
                        <h3 class="custom-heading blue" style="margin-bottom: 0px;">
                            <asp:Label ID="LearningPointLevel" runat="server" meta:resourcekey="LearningPointLevel"
                                EnableViewState="false" />

                            <asp:Button ID="btnPDF" runat="server" CommandArgument="Originalcompetencelevel"
                                CommandName="Original" Text="Export to PDF" CssClass="btns  yellow  col-md-11 libray_btn"
                                Style="float: right; width: 130px; font-size: 17px; display: none;" OnClick="btnPDF_Click" />
                        </h3>
                        <div class="col-md-12 comp_table" style="background: #fff; overflow-x: auto;">
                            <asp:Repeater runat="server" ID="rowRepeater" OnItemDataBound="rowRepeater_ItemBound">
                                <HeaderTemplate>
                                    <table width="100%" border="1" class="report_table report_table1" style="background-color: #E8E8E8; font-size: 12px; margin-top: 0;">
                                        <tr style="height: 35px;">
                                            <th>
                                                <asp:Label runat="server" ID="lblrName" Text="Name" meta:resourcekey="lblrNameResource1" />
                                            </th>
                                            <th>
                                                <asp:Label runat="server" ID="lblAlpHdr" Text="ALP" meta:resourcekey="lblAlpHdrResource" />
                                            </th>
                                            <th>
                                                <asp:Label runat="server" ID="lblplpHdr" Text="PLP" meta:resourcekey="lblplpHdrResource" />
                                            </th>
                                            <th>
                                                <asp:Label runat="server" ID="lblTotalHdr" Text="Total" meta:resourcekey="lblTotalHdrResource" />
                                            </th>

                                        </tr>
                                       
                                </HeaderTemplate>
                                <ItemTemplate>
                                    
                                    <tr style="height: 35px;">
                                        <td>
                                             <asp:HiddenField ID="userId" runat="server" Value='<%# Eval("userid") %>' />
                                            <asp:Label ID="lblhead1" Text='<%# Eval("username") %>' runat="server" meta:resourcekey="lblhead1Resource1"></asp:Label>

                                        </td>
                                        <td>

                                            <asp:Label ID="lblContentALP" Text='<%# Eval("ALP") %>' runat="server" meta:resourcekey="lblhead1Resource1"></asp:Label>

                                        </td>
                                        <td>

                                            <asp:Label ID="lblContentPLP" Text='<%# Eval("PLP") %>' runat="server" meta:resourcekey="lblhead1Resource1"></asp:Label>

                                        </td>
                                        <td>

                                            <asp:Label ID="lblContentTOTAL" Text='<%# Eval("TOTAL") %>' runat="server" meta:resourcekey="lblhead1Resource1"></asp:Label>

                                        </td>
                                    </tr>
                                   
                                </ItemTemplate>
                                <FooterTemplate>
                                    
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>

                        <div class="home_grap col-md-12 ">
                            <div id="containerOriginal" style="min-width: 310px; height: 400px; margin: 0 auto">
                                <div id="wait" style="margin: 190px 612px">
                                    <img src="../images/wait.gif" />
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div style="clear: both">
                </div>
                <!-- TIME LINE -->
            </div>
        </div>
        <!-- Chat Widget -->
        <!-- Recent Post -->
        <div class="col-md-3">
        </div>
        <!-- Twitter Widget -->
        <!-- Weather Widget -->
    </div>
   
    <script type="text/javascript" src="js/chart-line-and-graph.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script src="../Scripts/Scripts/exporting.js" type="text/javascript"></script>
    <script src="../Scripts/highcharts.js" type="text/javascript"></script>
    <script src="../Scripts/jsPDF-min.js" type="text/javascript"></script>
    <%-- Use for Chart--%>
    <%-- <script src="../Scripts/chartCat.js" type="text/javascript"></script>--%>
    <script src="../Scripts/flotJs.js" type="text/javascript"></script>
    <script src="../Scripts/chartCat.js" type="text/javascript"></script>
    <%-- <script src="http://mrrio.github.io/jsPDF/dist/jspdf.min.js" type="text/javascript"></script>--%>
    <script src=" https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"
        type="text/javascript"></script>
    <script type="text/javascript">

        function DropDownLableChange() {
            $("#ddCompetencesTable li a").click(function () {
                $("#ddLabel").text($(this).text());
                var text = $('#<%=Label2.ClientID %>').text();
                var subtext = $('#<%=GraphSubTitle.ClientID %>').val();
                var AxisText = $('#<%=GraphAxisLevel.ClientID %>').val();
                ToolTipText = $('#<%=GraphToolTip.ClientID %>').val();

                if (subtext == "") {
                    subtext = "Click and drag mouse arrow to zoom chart";
                }
                if (text == "") {
                    text = $("#ddLabel").text();
                    if (text == "") {
                        text = "Original Competence Level";
                    }
                }
                if (AxisText == "") {

                    text = "Level";
                }
                if (ToolTipText == "") {

                    ToolTipText = "Level";
                }

                $(".highcharts-title").text(text);
                $(".highcharts-subtitle").text(subtext);
                $(".highcharts-yaxis-title").text(AxisText);
                $(".spanText").text(ToolTipText);
            });

            $("#ddCompetencesTable li a").click(function () { $(".highcharts-title").text($(this).text()); });


        }

        function showDivByQueryString() {

            var url = window.location.href;
            var dd = GetQueryParameterByName("dd", url);

            if (dd == "o" || dd == "o#") {
                dd = "Original";
                $('#<%=Label2.ClientID %>').text($("#ddCompetencesTable li a:eq(0)").text().trim());
                $("#ddLabel").text($("#ddCompetencesTable li a:eq(0)").text());
            } else if (dd == "d" || dd == "d#") {
                dd = "Development";
                $('#<%=Label2.ClientID %>').text($("#ddCompetencesTable li a:eq(2)").text().trim());
                $("#ddLabel").text($("#ddCompetencesTable li a:eq(2)").text());
            } else if (dd == "n" || dd == "n#") {
                dd = "New";
                $('#<%=Label2.ClientID %>').text($("#ddCompetencesTable li a:eq(1)").text().trim());
                $("#ddLabel").text($("#ddCompetencesTable li a:eq(1)").text());
            } else if (dd == "k" || dd == "k#") {
                dd = "Knowledge";
                $('#<%=Label2.ClientID %>').text($("#ddCompetencesTable li a:eq(3)").text().trim());
                $("#ddLabel").text($("#ddCompetencesTable li a:eq(3)").text());
            }

    showThisGraph(dd);
}

function showThisGraph(id) {
    if (id == "Original") {

        $("#Original").fadeIn();
        $("#New").fadeOut();
        $("#Development").fadeOut();
        $("#Knowledge").fadeOut();
    }
    else if (id == "New") {

        $("#Original").fadeOut();
        $("#New").fadeIn();
        $("#Development").fadeOut();
        $("#Knowledge").fadeOut();
        drwaChartNewCompLevel();
    }
    else if (id == "Development") {

        $("#Original").fadeOut();
        $("#New").fadeOut();
        $("#Development").fadeIn();
        $("#Knowledge").fadeOut();
        drwaChartDevelopmentCompLevel();
    }
    else if (id == "Knowledge") {

        $("#Original").fadeOut();
        $("#New").fadeOut();
        $("#Development").fadeOut();
        $("#Knowledge").fadeIn();
        drwaKnowledgeDevelopment();
    }
}

var icons = new Skycons();
icons.set("rain", Skycons.RAIN);
icons.play();
var ToolTipText;

$(document).ready(function () {
    SetExpandCollapse();
    // ExampanCollapsMenu();
    showDivByQueryString();
    DropDownLableChange();
    setTimeout(function () {
        var text = $('#<%=LearningPointLevel.ClientID %>').text();
        var subtext = $('#<%=GraphSubTitle.ClientID %>').val();
        var AxisText = $('#<%=GraphAxisLevel.ClientID %>').val();
        ToolTipText = $('#<%=GraphToolTip.ClientID %>').val();

        if (subtext == "") {
            subtext = "Click and drag mouse arrow to zoom chart";
        }
        if (text == "") {
            text = $("#ddLabel").text();
            if (text == "") {
                text = "Original Competence Level";
            }
        }
        if (AxisText == "") {

            text = "Level";
        }
        if (ToolTipText == "") {

            ToolTipText = "Level";
        }

        $(".highcharts-title").text(text);
        $(".highcharts-subtitle").text(subtext);
        $(".highcharts-yaxis-title").text(AxisText);
        $(".spanText").text(ToolTipText);
    }, 3000);
});


var doc = new jsPDF();
var specialElementHandlers = {
    '#editor': function (element, renderer) {
        return true;
    }
};

$('#cmd').click(function () {
    doc.fromHTML($('#ContentPlaceHolder1_div1').html(), 15, 15, {
        'width': 170,
        'elementHandlers': specialElementHandlers
    });
    doc.save('sample-file.pdf');
});
    </script>
 

    <script>
        var totalLevel;
        function SubmitBtn1() {
            GetPageName();
            var ReturnData;
            $.ajax({
                type: "POST",
                cache: false,
                async: true,
                url: "LearningPoint.aspx/BtnSubmitAJAX",
                data: '{txtName:"' + '' + '",txtDesc:"' + '' + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    dataDev = response.d;

                    Developmentcomnames1();
                    setTimeout(function () {

                        drwaGraphDevelopment1();
                        $("#wait").fadeOut();

                    }, 500);


                },
                failure: function (msg) {

                    alert(msg);
                    ReturnData = msg;
                }
            });

        }

        function Developmentcomnames1() {


            var ReturnData;
            $.ajax({
                type: "POST",
                cache: false,
                async: true,
                url: "LearningPoint.aspx/Developmentcomnames",
                data: '{txtName:"' + '' + '",txtDesc:"' + '' + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                   
                    totalLevel = response.d;
                    return totalLevel;

                },
                failure: function (msg) {

                    alert(msg);
                    ReturnData = msg;
                }
            });

        }


        function drwaGraphDevelopment1() {
            console.log(totalLevel);
            console.log(dataDev);
            $(function () {
                $('#containerOriginal').highcharts({
                    chart: {
                        type: 'column',
                        zoomType: 'x'
                    },
                    title: {
                        text: 'Graph'
                    },
                    subtitle: {
                        text: 'Click and drag mouse arrow to zoom chart'
                    },

                    xAxis: {
                        categories: totalLevel

                    },
                    yAxis: {

                        min: 0,
                        title: {
                            text: 'Level'
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                            //'<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                            '<td style="padding:0"><b>{point.y:.1f} <span class="spanText">Level</span></b></td></tr>',
                        footerFormat: '</table>',
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series: dataDev
                });
            });
        }





    </script>

</asp:Content>
