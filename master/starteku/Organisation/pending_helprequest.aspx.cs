﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using System.Configuration;
using starteku_BusinessLogic.Model;
using starteku_BusinessLogic.View;
using startetku.Business.Logic;
using System.Web.UI.HtmlControls;
using System.Threading;
using System.Globalization;

public partial class Organisation_pending_helprequest : System.Web.UI.Page
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {


           // Common.WriteLog("pending_helprequest 1");
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "my", " SetExpandCollapse();", true);
           // Common.WriteLog("pending_helprequest 2");
            if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
            {
                Response.Redirect("login.aspx");
            }
         //   Common.WriteLog("pending_helprequest 3");
            if (!IsPostBack)
            {
              //  Common.WriteLog("pending_helprequest 4");
                Employee.InnerHtml = GetLocalResourceObject("Welcome.Text").ToString() + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
              //  Common.WriteLog("pending_helprequest 5");
                if (Convert.ToString(Session["OrguserType"]) == "1")
                {
                    //sp1.InnerHtml = " Competence ";
                    //sp1.InnerHtml = CommonMessages.Competence;
                    sp1.InnerHtml = hdnCompetence.Value;// "Competence";
                }
                else
                {
                    sp1.InnerHtml = hdnPendingRequest.Value;//"Pending Request ";
                    //sp1.InnerHtml = CommonMessages.PendingRequest;
                }
               // Common.WriteLog("pending_helprequest 6");
                GetAllEmployeeList();
             //   Common.WriteLog("pending_helprequest 7");
                //EmployeeListArchiveAll();
                SetDefaultMessage();
             //   Common.WriteLog("pending_helprequest 8");

                var userId = Convert.ToInt32(Session["OrgUserId"]);
                var userCompanyID = Convert.ToInt32(Session["OrgCompanyId"]);
                var db = new startetkuEntities1();
                var actEnaData = db.ActicityEnableMasters.FirstOrDefault(o => o.Aemcompid == userId || o.Aemcompid == userCompanyID);
                if (actEnaData != null)
                {
                    Session["Aemcompid"] = actEnaData.Aemcompid;
                }
                else
                {
                    Session["Aemcompid"] = null;
                }
            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "pending_helprequest->Page_Load");
        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion

    #region method
    protected void SetDefaultMessage()
    {
        // lblMsg.Text = GetLocalResourceObject("lblHelloWorld.Text").ToString();

        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["msg"])))
        {
            if (Convert.ToString(Request.QueryString["msg"]) == "del")
            {
                //lblMsg.Text = CommonModule.msgRecordHasBeenDeletedSuccessfully;
                //lblMsg.Text = CommonMessages.msgRecordHasBeenDeletedSuccessfully;
                lblMsg.Text = GetLocalResourceObject("msgRecordHasBeenDeletedSuccessfully.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "ins")
            {
                //lblMsg.Text = CommonMessages.msgRecordInsertedSuccessfully;
                lblMsg.Text = GetLocalResourceObject("msgRecordInsertedSuccessfully.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "upd")
            {
                //lblMsg.Text = CommonMessages.msgRecordUpdatedSuccss;
                lblMsg.Text = GetLocalResourceObject("msgRecordUpdatedSuccss.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "arc")
            {
                // lblMsg.Text = CommonMessages.msgRecordArchiveSucces;
                lblMsg.Text = GetLocalResourceObject("CommonMessages.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "err")
            {
                // lblMsg.Text = CommonMessages.msgRecordArchiveSucces;
                lblMsg.Text = @"Something is wrong, please contact higher authority";
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
        }

    }
    public static DataView GetView(DataSet ds, string filter, string sort)
    {
        try
        {
            DataView dv = new DataView(ds.Tables[0]);
            dv.Sort = sort;
            dv.RowFilter = filter;
            return dv;
        }
        catch (Exception ex)
        {

            Common.WriteLog("GetView===============Error============" + ex.StackTrace);
            return null;
        }

    }
    protected void GetAllEmployeeList()
    {
        EmployeeSkillBM obj = new EmployeeSkillBM();
        obj.skillIsActive = true;
        obj.skillIsDeleted = false;
        if (Convert.ToString(Session["OrguserType"]) == "1")
        {
            obj.skillStatus = 0;
        }
        else
        {
            obj.skillStatus = 0;
        }
        obj.skillCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);

        obj.skillCreateBy = Convert.ToInt32(Session["OrgUserId"]);

        var CreatedBy = Convert.ToString(Session["OrgUserId"]);



        // obj.GetAllSkillPendingRequest();

        var db = new startetkuEntities1();



        //string comall=userCo

        //var dbData = (from o in db.SkillMasters
        //              join q in db.usermasters
        //              on o.skillUserId equals q.userId
        //              where q.userIsActive == true && q.userCreateBy == CreatedBy && o.skillCompanyId == obj.skillCompanyId/* &&
        //            (o.skillStatus == obj.skillStatus || o.skillIsApproved == false)*/

        //            && o.skillIsActive == true && o.skillIsDeleted == false && q.userIsActive == true
        //              select new { o, q }).ToList();


        var dbData = db.spComp_PendingRequestList(Convert.ToInt32(CreatedBy), Convert.ToInt32(obj.skillCompanyId)).ToList();


        var skillMaster = new List<SkillMasterView>();


        foreach (var sm in dbData)
        {

            var notsaveData = dbData.Where(x => x.skillIsSaved == false && x.skillUserId == sm.skillUserId).ToList();
            if (notsaveData.Count > 0)
            {
                var chkDup = (from o in skillMaster where o.skillUserId == sm.skillUserId select o).FirstOrDefault();

                if (chkDup == null)
                {
                    skillMaster.Add(GetSkillMasterView(sm, "false"));
                }

            }

            else
            {
                var chkDup = (from o in skillMaster where o.skillUserId == sm.skillUserId select o).FirstOrDefault();

                if (chkDup != null)
                {
                    if (chkDup.skillStatus != sm.skillStatus)
                    {
                        if (chkDup.skillStatus == 1)
                        {
                            skillMaster.Remove(chkDup);
                        }
                        else
                        {
                            continue;
                        }
                    }
                    else
                    {
                        continue;
                    }
                }

                // Comment by Swati on 16/01/18 due to request came Approved though was Pending
                //Common.WriteLog("chkDupCount 1: " + Convert.ToString(sm.skillStatus));
                var chkDupCount = (from o in db.SkillMasters where o.skillUserId == sm.skillUserId where o.skillLocal > 0 select o).OrderByDescending(o => o.skillStatus).FirstOrDefault();
                if (chkDupCount != null)
                {
                    sm.skillStatus = chkDupCount.skillStatus;
                    if (Convert.ToDateTime(chkDupCount.skillCreatedDate).Date > Convert.ToDateTime(chkDupCount.skillUpdatedDate).Date)
                    {
                        sm.skillCreatedDate = chkDupCount.skillCreatedDate;
                    }
                    else
                    {
                        sm.skillCreatedDate = chkDupCount.skillUpdatedDate;
                    }
                //  Common.WriteLog("chkDupCount 2: " + Convert.ToString(sm.skillStatus));
                    skillMaster.Add(GetSkillMasterView(sm, ""));
                }
                //skillMaster.Add(GetSkillMasterView(sm, ""));
            }

        }

        var userId = Convert.ToInt32(Session["OrgUserId"]);
        if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["Aemcompid"])))
        {
            try
            {
                db.ActReqMaster_Update_ReqStatus();
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex, 0, "ActReqMaster_Update_ReqStatus");
            }
           
            var actReq = ActivityRequestLogic.GetActivityRequestList(userId);

            foreach (var i in actReq)
            {
                skillMaster.Add(GetActReqInSkillMasterView(i));
            }

        }

        //
        if (skillMaster != null && skillMaster.Any())
        {
            gvGrid.DataSource = skillMaster.OrderBy(o => o.skillCreatedDate);
            gvGrid.DataBind();
            gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        else
        {
            gvGrid.DataSource = null;
            gvGrid.DataBind();
        }
    }





    private SkillMasterView GetActReqInSkillMasterView(GetActivityRequestLists_Result sm)
    {


        var v = new SkillMasterView();
        v.name = sm.userFirstName;
        v.skillAcceptDate = sm.ActReqUpdateDate;
        v.skillAchive = 0;
        v.skillComId = 0;
        v.skillComment = "";
        v.skillCompanyId = sm.ActReqOrgId;
        v.skillCreatedDate = sm.ActReqCreateDate;
        v.skillId = sm.ActReqId;
        v.skillIsActive = sm.ActIsActive;
        v.skillIsApproved = sm.skillIsApproved;
        v.skillUserId = sm.ActReqUserId;

      //  Common.WriteLog("GetActReqInSkillMasterView ->sm.userFirstName-> actReqStatus :" + sm.userFirstName +" "+ Convert.ToString(sm.ActReqStatus));
        v.status = GetActivityStatusByStatusId(sm.ActReqStatus, false);


        v.SkillType = "Activity Requests";


        return v;
    }
    private SkillMasterView GetSkillMasterView(spComp_PendingRequestList_Result sm, string sts)
    {


        var v = new SkillMasterView();
        v.name = sm.userFirstName;
        v.skillAcceptDate = sm.skillAcceptDate;
        v.skillAchive = sm.skillAchive;
        v.skillComId = sm.skillComId;
        v.skillComment = sm.skillComment;
        v.skillCompanyId = sm.skillCompanyId;
        v.skillCreatedDate = sm.skillCreatedDate;
        v.skillId = sm.skillId;
        v.skillIsActive = sm.skillIsActive;
        v.skillIsApproved = sm.skillIsApproved;
        v.skillUserId = sm.skillUserId;
        v.skillStatus = sm.skillStatus;
        v.SkillType = "Competence Setting";
        if (!string.IsNullOrEmpty(sts))
        {

            v.skillIsSaved = false;
            sm.skillIsSaved = false;
        }
        else
        {
            v.skillIsSaved = sm.skillIsSaved;
        }
        if (sm.skillIsSaved == false)
        {
            v.status = "Not Saved";
        }

        else
        {
         //   Common.WriteLog("sm.skillStatus sm.userFirstName sm.skillCreatedDate " + sm.userFirstName + " " + Convert.ToString(sm.skillStatus) + Convert.ToString(sm.skillCreatedDate.ToString()));
            if (sm.skillStatus == 1)
            {
                v.status = hdnAccept.Value;
            }
            else if (sm.skillStatus == 2)
            {
                v.status = hdnCancel.Value;

            }
            else
            {
               
                v.status = hdnPending.Value;

            }
        }

        return v;
    }

    //private SkillMasterView GetSkillMasterView(SkillMaster sm, usermaster usermaster,string sts)
    //{


    //    var v = new SkillMasterView();
    //    v.name = usermaster.userFirstName;
    //    v.skillAcceptDate = sm.skillAcceptDate;
    //    v.skillAchive = sm.skillAchive;
    //    v.skillComId = sm.skillComId;
    //    v.skillComment = sm.skillComment;
    //    v.skillCompanyId = sm.skillCompanyId;
    //    v.skillCreatedDate = sm.skillCreatedDate;
    //    v.skillId = sm.skillId;
    //    v.skillIsActive = sm.skillIsActive;
    //    v.skillIsApproved = sm.skillIsApproved;
    //    v.skillUserId = sm.skillUserId;
    //    v.skillStatus = sm.skillStatus;
    //    if (!string.IsNullOrEmpty(sts))
    //    {

    //        v.skillIsSaved =false;
    //        sm.skillIsSaved = false;
    //    }
    //    else
    //    {
    //        v.skillIsSaved = sm.skillIsSaved;
    //    }
    //    if (sm.skillIsSaved == false)
    //    {
    //        v.status = "Not Saved";
    //    }

    //    else
    //    {
    //        if (sm.skillStatus == 1)
    //        {
    //            v.status = hdnAccept.Value;
    //        }
    //        else if (sm.skillStatus == 2)
    //        {
    //            v.status = hdnCancel.Value;

    //        }
    //        else
    //        {
    //            v.status = hdnPending.Value;

    //        }
    //    }

    //    return v;
    //}


    #endregion

    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {

    }

    #endregion

    #region grid Event
    protected void gvGrid_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "archive")
        {
            EmployeeSkillBM obj = new EmployeeSkillBM();
            obj.skillId = Convert.ToInt32(e.CommandArgument);
            obj.skillIsActive = false;
            obj.skillIsDeleted = false;
            obj.SkillRequestStatusUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    GetAllEmployeeList();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }

    }
    protected void gvGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (Convert.ToString(Session["OrguserType"]) == "1")
            {
                HtmlGenericControl Edit = (HtmlGenericControl)e.Row.FindControl("Edit");
                HtmlGenericControl Delete = (HtmlGenericControl)e.Row.FindControl("Delete");
                Edit.Visible = true;
                Delete.Visible = true;

            }

            try
            {

                Label test = (Label)e.Row.FindControl("lblSkillType");

                if ((test.Text == "Competence Setting"))
                {
                    ((Label)e.Row.FindControl("lblSkillType")).Text = GetLocalResourceObject("lblSkillType.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }

                if ((test.Text == "Activity Requests"))
                {
                    ((Label)e.Row.FindControl("lblSkillType")).Text = GetLocalResourceObject("ActivityRequests.Text").ToString();//GetLocalResourceObject("lblDays.Text").ToString();
                }
            }
            catch (Exception ex)
            {
                if ((((Label)(e.Row.FindControl("lblSkillType"))).Text == "Competence Setting"))
                {
                    ((Label)e.Row.FindControl("lblSkillType")).Text = GetLocalResourceObject("lblSkillType.Text").ToString();
                }
            }

        }



    }

    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Denish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion


    public string GetActivityStatusByStatusId(string actReqStatus, bool isPrivate)
    {
       
        /*
     * About Activity Request Stauts:  2017 04 01
     
     * We need to change status on an activity
    Available” ==1
            Activity can be requested
            Should be auto set when creating a public activity
    Requested   ==2
            Activity have been requested by user
            Should be auto set when user request a public activity
    Request approved ==3
            Manager have approved requested activity
            Should be auto set when manager approve users activity request
    Ongoing ==4
            Should be auto set when start date have been reached
    Completed ==5
            employee flag activity has been completed
            Should be set manuel by user (or manager)
    Completed approval ==6
            Manager have approved that activity have been completed by user
            Should be set manual by manager (or system owner)
     * 
     * REJECTED == 7
    */
        //var status = isPrivate ? StringFor.CREATED : StringFor.Available;

        GetResource();
        var status = false ? StringFor.CREATED : StringFor.Available;

        switch (actReqStatus)
        {
            case "2":
                {
                    status = REQUESTED;//getResource.GetResource("REQUESTED.Text");//StringFor.REQUESTED;
                    break;
                }
            case "3":
                {
                    status = APPROVED;// getResource.GetResource("REQUESTED.Text"); //StringFor.APPROVED;
                    break;
                }
            case "4":
                {
                    status = ONGOING;// getResource.GetResource("REQUESTED.Text"); //StringFor.ONGOING;
                    break;
                }
            case "5":
                {
                    status = COMPLETED;// getResource.GetResource("REQUESTED.Text"); //StringFor.COMPLETED;
                    break;
                }
            case "6":
                {
                    status = COMPLETED_APPROVE;//getResource.GetResource("REQUESTED.Text"); //StringFor.COMPLETED_APPROVE;
                    break;
                }
            case "7":
                {
                    status = REJECTED;//getResource.GetResource("REQUESTED.Text"); //StringFor.REJECTED;
                    break;
                }


        }
        return status;
    }

    public string REQUESTED { get; set; }
    public string APPROVED { get; set; }
    public string ONGOING { get; set; }

    public string COMPLETED { get; set; }
    public string COMPLETED_APPROVE { get; set; }
    public string REJECTED { get; set; }

    private void GetResource()
    {
        REQUESTED = GetLocalResourceObject("REQUESTED.Text").ToString();
        APPROVED = GetLocalResourceObject("APPROVED.Text").ToString();
        ONGOING = GetLocalResourceObject("ONGOING.Text").ToString();

        COMPLETED = GetLocalResourceObject("COMPLETED.Text").ToString();
        COMPLETED_APPROVE = GetLocalResourceObject("COMPLETED_APPROVE.Text").ToString();
        REJECTED = GetLocalResourceObject("REJECTED.Text").ToString();
    }

}
