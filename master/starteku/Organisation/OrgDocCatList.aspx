﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master" AutoEventWireup="true" CodeFile="OrgDocCatList.aspx.cs" Inherits="Organisation_OrgDocCatList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
       <link href="css/ajaxtab.css" rel="stylesheet" />
    <style type="text/css">
        .yellow
        {
            border-radius: 0px;
        }
        .treeNode input
        {
            width: auto;
            margin: 5px;
            float: left !important;
        }
    </style>
    <style type="text/css">
        .yellow
        {
            border-radius: 0px;
        }
        body
        {
            font-family: Geneva,Arial,Helvetica,sans-serif;
        }
        #ContentPlaceHolder1_chkList input
        {
            width: 33px;
            margin-bottom: 0px !important;
        }
        #ContentPlaceHolder1_chkList label
        {
            margin-top: 2px;
        }
        label
        {
            display: inline-block;
            font-size: 14px;
            font-weight: inherit;
            margin-bottom: 5px;
        }
        .table > thead > tr > th
        {
            vertical-align: middle;
        }
    </style>
    <script src="../Scripts/sMain.js?version=1.0" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <style type="text/css">
        .scroll_checkboxes
        {
            height: 120px;
            padding: 5px;
            overflow: auto;
            border: 1px solid #ccc;
        }
        
        .FormText
        {
            font-size: 11px;
            font-family: tahoma,sans-serif;
        }
    </style>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            setTimeout(function () {
                ExampanCollapsMenu();
                ExampanCollapssubMenu();
                SetExpandCollapse();
                // tabMenuClick();
                $(".OrgDocCatList").addClass("active_page");
            }, 500);
        });
    </script>
    <script language="javascript">

        var color = 'White';

        function changeColor(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=chkList.ClientID%>");
            if (color == '') {
                color = getRowColor();
            }
            if (obj.checked) {
                rowObject.style.backgroundColor = '#A3B1D8';
            }
            else {
                rowObject.style.backgroundColor = color;
                color = 'White';
            }

            // private method
            function getRowColor() {
                if (rowObject.style.backgroundColor == 'White') return parentTable.style.backgroundColor;
                else return rowObject.style.backgroundColor;
            }

        }

        // This method returns the parent row of the object

        function getParentRow(obj) {
            do {
                obj = obj.parentElement;
            }
            while (obj.tagName != "TR")
            return obj;
        }


        function TurnCheckBoixGridView(id) {
            var frm = document.forms[0];

            for (i = 0; i < frm.elements.length; i++) {
                if (frm.elements[i].type == "checkbox" && frm.elements[i].id.indexOf("<%= chkList.ClientID %>") == 0) {
                    frm.elements[i].checked = document.getElementById(id).checked;
                }
            }
        }

        function SelectAll(id) {

            var parentTable = document.getElementById("<%=chkList.ClientID%>");
            var color

            if (document.getElementById(id).checked) {
                color = '#A3B1D8'
            }
            else {
                color = 'White'
            }

            for (i = 0; i < parentTable.rows.length; i++) {
                parentTable.rows[i].style.backgroundColor = color;
            }
            TurnCheckBoixGridView(id);

        }



    </script>
    <div class="col-md-6">
        <div class="heading-sec">
            <h1 style="margin-left: 15px;">
                <asp:Label runat="server" meta:resourcekey="doccatres"></asp:Label><i><span runat="server" id="Division"></span></i>
            </h1>
        </div>
    </div>
    <%-- <div class="col-md-3">
        <div class="dropdown-example">
            <ul class="nav nav-pills">
                <li class="dropdown" style="float: left!important; width: 100%;">
                    <a class="skill_dropdown" id="drop7" role="button" data-toggle="dropdown" href="#">Account Department<b class="skill_caret"></b></a>
                    <ul id="menu7" class="dropdown-menu" role="menu" aria-labelledby="drop7" style="width: 100%;">
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Account Department</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Sales Department</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Human Resources Department </a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Software Department</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Network Department</a></li>
                        <li role="presentation" class="divider"></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="report_all.html">All</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-md-3">
        <div class="dropdown-example">
            <ul class="nav nav-pills">
                <li class="dropdown" style="float: left!important; width: 100%;">
                    <a class="skill_dropdown" id="A1" role="button" data-toggle="dropdown" href="report_all.html">All<b class="skill_caret"></b></a>
                    <ul id="Ul1" class="dropdown-menu" role="menu" aria-labelledby="drop7" style="width: 100%;">
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="report.html">Original competence level</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">New competence level</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Development points</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Knowledge share</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Potential for development</a></li>
                        <li role="presentation" class="divider"></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">All</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>--%>
   
    <div class="col-md-12" style="margin-top: 20px;">
        <div id="graph-wrapper">
           
            <div class="col-md-12">
                <a href="#add-post-title" data-toggle="modal" title="" style="margin-bottom: 15px;">
                    <button style="border: 0px;" class="btn btn-primary yellow lrg-btn flat-btn add_user" type="button">
                        <%--<%= CommonMessages.AddNewDivision%>--%>
                        <asp:Literal ID="Literal1" runat="server" meta:resourcekey="AddNewDivision" EnableViewState="false" /></button>
                </a>
                  
                <div aria-hidden="true" role="dialog" tabindex="-1" class="modal fade" id="add-post-title"
                    style="display: none;">
                    <asp:UpdatePanel runat="server" ID="addnewdivisionUpdatePanel">
        <ContentTemplate>
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header blue yellow-radius">
                                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                                    ×
                                </button>
                                <h4 class="modal-title">                                   
                                    <asp:Literal ID="Literal2" runat="server" meta:resourcekey="AddNewDivision" EnableViewState="false" /></h4>
                            </div>
                            <div class="modal-body">
                                <asp:TreeView ID="TreeView1" runat="server" ShowCheckBoxes="All" onclick="client_OnTreeNodeChecked(event)"
                                    OnTreeNodePopulate="TreeView_TreeNodePopulate" Style="cursor: pointer" ShowLines="True"
                                    NodeStyle-CssClass="treeNode" Visible="False" meta:resourcekey="TreeView1Resource1" />
                                <br />
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                                    <ContentTemplate>
                                        <asp:TextBox runat="server" placeholder="Title :" ID="txtDepartmentName" MaxLength="50"
                                            meta:resourcekey="txtDepartmentNameResource1" AutoPostBack="true" OnTextChanged="txtDepartmentName_TextChanged" />
                                        <asp:Label runat="server" ID="lblDepartment" CssClass="commonerrormsg" style="float:left;"></asp:Label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtDepartmentName"
                                    ErrorMessage="Please enter division Name." CssClass="commonerrormsg" Display="Dynamic"
                                    ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator5Resource1" style="float:left;"></asp:RequiredFieldValidator>

                                         <asp:TextBox runat="server" placeholder="Division Name in Danish" ID="txtDepartmentNameDN" MaxLength="50"
                                            meta:resourcekey="txtDepartmentNameDNResource1" AutoPostBack="true"/>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorDN" runat="server" ControlToValidate="txtDepartmentNameDN"
                                    ErrorMessage="Please enter division Name in danish." CssClass="commonerrormsg" Display="Dynamic"
                                    ValidationGroup="chk" meta:resourcekey="RequiredFieldValidatorDNResource1" style="float:left;"></asp:RequiredFieldValidator>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <br />
                                <div style="clear:both"></div>
                                <div class="scroll_checkboxes">
                                    <asp:Label runat="server" meta:resourcekey="categoryres"></asp:Label><br />
                                    <asp:CheckBoxList Width="100%" ID="chkList" runat="server" CssClass="FormText" RepeatDirection="Vertical"
                                        RepeatColumns="1" BorderWidth="0" Datafield="description" DataValueField="value"
                                        Style="font-weight: inherit; font-size: 14px;">
                                    </asp:CheckBoxList>
                                   
                                    <br />
                                </div>
                               <%--  <asp:CustomValidator ID="CustomValidator1" ErrorMessage="Please select at least one item."
                                        ForeColor="Red" ClientValidationFunction="ValidateModuleList" runat="server" Font-Size="13px"
                                        ValidationGroup="chk" />--%>
                               
                                <br />
                                <asp:TextBox runat="server" placeholder="Description :" ID="txtDESCRIPTION" MaxLength="500"
                                    TextMode="MultiLine" Rows="5" meta:resourcekey="txtDESCRIPTIONResource1" />
                                <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDESCRIPTION"
                                    ErrorMessage="Please Enter DESCRIPTION." CssClass="commonerrormsg" Display="Dynamic"
                                    ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator1Resource1"></asp:RequiredFieldValidator>--%>
                            </div>
                            <div class="modal-footer">
                                <asp:Button runat="server" ID="btnsubmit" Text="Submit" CssClass="btn btn-primary yellow"
                                    OnClientClick="return CheckValidations('chk');generate('warning','Please wait','bottomCenter');" ValidationGroup="chk" OnClick="btnsubmit_click"
                                    Style="border-radius: 5px;" meta:resourcekey="btnsubmitResource1" />
                                <button data-dismiss="modal" class="btn btn-default black" type="button">
                                    <%-- <%= CommonMessages.Close%> --%>
                                    <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Close" EnableViewState="false" />
                                </button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                     </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnsubmit" runat="server" EventName="click"/>
                        </Triggers>

    </asp:UpdatePanel>
            
                </div>
                      <div style="clear:both;"></div>
            <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" meta:resourcekey="lblMsgResource1"></asp:Label>
                <br />
                <br />
                <div class="chart-tab manager_table">
                    <%-- <div id="tabs-container">--%>
                    <%--<cc1:TabContainer ID="TabContainer1" runat="server" CssClass="Tab">
                            <cc1:TabPanel ID="tbpnluser" runat="server">
                                <HeaderTemplate>
                                    Division
                                    <a></a>
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <div class="invoice" style="background-color: white; margin-top: 0px;">--%>
                    <asp:GridView ID="gvGrid" runat="server" AutoGenerateColumns="False" CellPadding="0"
                        Width="100%" GridLines="None" DataKeyNames="dcCatId" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                        EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>' OnRowCommand="gvGrid_RowCommand"
                        OnRowDataBound="gvGrid_RowDataBound" meta:resourcekey="gvGridResource1">
                        <HeaderStyle CssClass="aa" />
                        <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:TemplateField HeaderText="ID" meta:resourcekey="TemplateFieldResource1">
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1 %>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" Font-Size="14px" />
                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                    Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CATEGORY" meta:resourcekey="TemplateFieldResource2">
                                <ItemTemplate>
                                    <%if(Convert.ToString(Session["Culture"]) == "Danish"){%>
                                    <asp:Label ID="lblrNamer" runat="server" Text='<%# Eval("dcNameDN") %>' meta:resourcekey="lblrNamerResource1"></asp:Label>
                                    <%} %>
                                    <% else{%>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("dcName") %>' meta:resourcekey="lblrNamerResource1"></asp:Label>
                                    <%} %>
                                    <asp:HiddenField ID="divCompanyId" runat="server" Value='<%# Eval("dcCompanyId") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Font-Size="14px" />
                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                    Width="50%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                            </asp:TemplateField>
                            <%-- <asp:TemplateField HeaderText="Division Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUserNamer" runat="server" Text="<%# bind('divName') %>"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        </asp:TemplateField>--%>
                            <asp:TemplateField HeaderText="" ItemStyle-Width="50px" meta:resourcekey="TemplateFieldResource3">
                                <ItemTemplate>
                                    <div class="vat" style="width: 70px;" id="divEdit" runat="server">
                                        <p>
                                            <i class="fa fa-pencil"></i><a href="<%# String.Format("OrgAddDocCate.aspx?id={0}", Eval("dccatId")) %>"
                                                title="Edit"><asp:Label runat="server" meta:resourcekey="btnEdit"></asp:Label></a>
                                        </p>
                                    </div>
                                    <div class="total" style="width: 70px;" id="divDeletet" runat="server">
                                        <p>
                                            <i class="fa fa-trash-o"></i>
                                            <asp:LinkButton ID="lnkBtnName" runat="server" CommandName="archive" CommandArgument='<%# Eval("dccatId") %>'
                                                ToolTip="Delete " OnClientClick="return confirm('Are you sure you want to Delete  this record?');"
                                                meta:resourcekey="lnkBtnNameResource1">Delete </asp:LinkButton>
                                        </p>
                                    </div>
                                    <div class="total" style="width: 200px;" id="div1" runat="server">
                                        <p>
                                            <i>
                                                <img src="images/block.jpg" alt="" height="15px;" width="15px;" /></i>
                                            <asp:Literal ID="Literal3" runat="server" meta:resourcekey="NotEditable" EnableViewState="false" /><%--<%= CommonMessages.NotEditable%>--%>
                                        </p>
                                    </div>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" Font-Size="14px" />
                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                    Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <%-- </div>
                                </ContentTemplate>
                            </cc1:TabPanel>
                            <cc1:TabPanel ID="tbpnlusrdetails" runat="server">
                                <HeaderTemplate>
                                    Archive
                                    <a></a>
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <div class="invoice" style="background-color: white; margin-top: 0px;">--%>
                    
                    <%--</div>
                                </ContentTemplate>
                            </cc1:TabPanel>
                        </cc1:TabContainer>--%>
                </div>
            </div>
  
        </div>
    </div>
            
    <%--</div>--%>
    <!-- /no-padding -->
    <!--=== no-padding and table-tabletools ===-->
    <div class="row">
    </div>
    <!-- /no-padding and table-tabletools -->
    <!--=== no-padding and table-colvis ===-->
    <div class="row">
    </div>
    <!-- /no-padding and table-colvis -->
    <!--=== Horizontal Scrolling ===-->
    <div class="row">
    </div>
    <!-- /Normal -->
    <!-- /Page Content -->
    <script type="text/javascript">
        function ValidateModuleList(source, args) {

            var chkListModules = document.getElementById('<%= chkList.ClientID %>');
            var chkListinputs = chkListModules.getElementsByTagName("input");
            for (var i = 0; i < chkListinputs.length; i++) {
                if (chkListinputs[i].checked) {
                    args.IsValid = true;
                    return;
                }
            }

            args.IsValid = false;
        }</script>

</asp:Content>

