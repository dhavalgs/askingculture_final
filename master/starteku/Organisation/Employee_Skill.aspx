﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true" CodeFile="Employee_Skill.aspx.cs" Inherits="Organisation_Employee_Skill" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .chat-widget-head
        {
            float: left;
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <div class="col-md-6">
            <div class="heading-sec">
                <h1>
                    <%= CommonMessages.EmployeeSkills%> <i><span runat="server" id="Skills"></span></i>
                </h1>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-md-6">
            <%-- <div class="col-md-9" style="padding-bottom: 22px;">--%>
            <div class="col-md-11" style="padding-bottom: 22px;">
                <div class="dropdown-example">
                    <ul class="nav nav-pills">
                        <li class="dropdown" style="float: left!important; width: 100%;">
                            <%-- <asp:UpdatePanel runat="server" ID="UpdatePanedropdown" UpdateMode="Conditional">
                                <ContentTemplate>--%>
                            <asp:DropDownList ID="ddlcompetence" runat="server" Class="skill_dropdown" Style="float: left!important;
                                width: 100%; background-color: #02406d; color: White; -webkit-appearance: none;"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlcompetence_SelectedIndexChanged">
                            </asp:DropDownList>
                            <%--    </ContentTemplate>--%>
                            <%--<Triggers>
                        <asp:AsyncPostBackTrigger ControlID="txtsearch" />
                    </Triggers>--%>
                            <%-- </asp:UpdatePanel>--%>
                        </li>
                        <%--<li class="dropdown" style="float: left!important; width: 100%;"><a class="skill_dropdown"
                            id="drop7" role="button" data-toggle="dropdown" href="#">Category X<b class="skill_caret"></b></a>
                            <ul id="menu7" class="dropdown-menu" role="menu" aria-labelledby="drop7" style="width: 100%;">
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Category y </a>
                                </li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Category z</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Category X </a>
                                </li>
                                <li role="presentation" class="divider"></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Other </a></li>
                            </ul>
                        </li>--%>
                    </ul>
                </div>
            </div>
            <div class="col-md-11">
                <div class="chat-widget widget-body" style="border-radius: 5px;">
                    <div class="chat-widget-head yellow">
                        <h4>
                            Sub <%= CommonMessages.Competence%></h4>
                    </div>
                    <%--   <asp:UpdatePanel runat="server" ID="UpdatePanelUploaed" UpdateMode="Conditional">
                        <ContentTemplate>--%>
                    <div class="col-md-12" style="padding: 0!important;">
                        <%--<table width="100%" border="1" bordercolor="#dfdfdf" class="skill_table">
                            <tr>
                                <td width="49%">
                                    <a href="#">Sub Competencie 1</a>
                                </td>
                                <td width="26%">
                                    5
                                </td>
                                <td width="13%">
                                    <form name="form1" method="post" action="">
                                    <label>
                                        <input type="checkbox" name="checkbox" value="checkbox">
                                    </label>
                                    </form>
                                </td>
                                <td width="12%">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="#">Sub Competencie 2 </a>
                                </td>
                                <td>
                                    3
                                </td>
                                <td>
                                    <form name="form2" method="post" action="">
                                    <label>
                                        <input type="checkbox" name="checkbox2" value="checkbox">
                                    </label>
                                    </form>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Sub Competencie 3
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <form name="form3" method="post" action="">
                                    <label>
                                        <input type="checkbox" name="checkbox3" value="checkbox">
                                    </label>
                                    </form>
                                </td>
                                <td>
                                    <div class="active_satus">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Sub Competencie 4
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <form name="form4" method="post" action="">
                                    <label>
                                        <input type="checkbox" name="checkbox4" value="checkbox">
                                    </label>
                                    </form>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Sub Competencie 5
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <form name="form5" method="post" action="">
                                    <label>
                                        <input type="checkbox" name="checkbox5" value="checkbox">
                                    </label>
                                    </form>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>--%>
                        <asp:UpdatePanel runat="server" ID="UpdatePanedrop" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Repeater ID="rep_competence" runat="server" OnItemDataBound="R1_ItemDataBound"
                                    OnItemCommand="Repeater1_OnItemCommand">
                                    <HeaderTemplate>
                                        <table width="100%" border="1" bordercolor="#dfdfdf" class="skill_table" id="tblContacts">
                                            <thead>
                                                <th style="text-align: center">
                                                    <%= CommonMessages.Competence%>
                                                </th>
                                                <th style="text-align: center">
                                                    <%= CommonMessages.value%>
                                                </th>
                                                <th style="text-align: center">
                                                    <%= CommonMessages.check%>
                                                </th>
                                                <th style="text-align: center">
                                                     <%= CommonMessages.Local%>
                                                </th>
                                                <th style="text-align: center">
                                                     <%= CommonMessages.All%>
                                                </th>
                                                <th style="text-align: center">
                                                    <%= CommonMessages.Help%>
                                                </th>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td width="49%">
                                                <%--  <a href="">--%>
                                                <asp:Label ID="lblsubcom" runat="server" Text='<%# Eval("subCompetence")%>'></asp:Label>
                                                <%-- </a>--%>
                                                <asp:HiddenField ID="subId" runat="server" Value="<%# bind('subId') %>" />
                                            </td>
                                            <td width="13%">
                                                <asp:Label ID="lbpoint" runat="server" Text="0"></asp:Label>
                                            </td>
                                            <td width="13%">
                                                <%--<form name="form1" method="post" action="">--%>
                                                <label>
                                                    <%-- <input type="checkbox" name="checkbox" value="checkbox" onclick="CheckBoxCheck(this);" id="checkbox1"/>--%>
                                                    <asp:CheckBox ID="checkbox1" runat="server" onclick="CheckBoxCheck(this);" />
                                                </label>
                                                <%--</form>--%>
                                            </td>
                                            <td width="13%">
                                                <asp:Button runat="server" ID="btnlocal" Text="L" CssClass="yellow" Style="border-radius: 100%;
                                                    height: 25px; width: 25px; color: white;" CommandName="Local" CommandArgument='<%# Eval("subId") %>' />
                                                <%-- OnClick="btnlocal_click"--%>
                                            </td>
                                            <td width="15%">
                                                <asp:Button runat="server" ID="btmAll" Text="A" CssClass="yellow" Style="border-radius: 100%;
                                                    height: 25px; width: 25px; color: white;" CommandName="All" CommandArgument='<%# Eval("subId") %>' /><%-- OnClick="btmAll_click""--%>
                                            </td>
                                            <td width="12%">
                                                <asp:Button runat="server" ID="btnhelp" Text="H" CssClass="yellow" Style="border-radius: 100%;
                                                    height: 25px; width: 25px; color: white;" OnClick="btnhelp_click" CommandName="help"
                                                    CommandArgument='<%# Bind("subId") %>' Visible="false" />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <tr id="Emptyrow" runat="server" visible="false">
                                            <td>
                                                <asp:Label ID="lblEmptyData" Text="No Data To Display" runat="server">
                                                </asp:Label>
                                            </td>
                                        </tr>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <%--<asp:Panel ID="pneedhelp" runat="server">
                                 <span>Need Help</span>
                                 </asp:Panel>
                                <asp:Panel ID="Panel1" runat="server">--%>
                                <asp:Repeater ID="rep_competencehigh" runat="server" OnItemDataBound="rep_competencehigh_ItemDataBound"
                                    OnItemCommand="rep_competencehigh_OnItemCommand">
                                    <HeaderTemplate>
                                        <table width="100%" border="1" bordercolor="#dfdfdf" class="skill_table" id="tblContacts1">
                                            <thead>
                                                <th style="text-align: left" colspan="6">
                                                    <%= CommonMessages.NeedHelp%>
                                                </th>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td width="42%">
                                                <%--  <a href="">--%>
                                                <asp:Label ID="lblsubcom" runat="server" Text='<%# Eval("subCompetence")%>'></asp:Label>
                                                <%-- </a>--%>
                                                <asp:HiddenField ID="subId" runat="server" Value="<%# bind('subId') %>" />
                                            </td>
                                            <td width="13%">
                                                <asp:Label ID="lbpoint" runat="server" Text="0"></asp:Label>
                                            </td>
                                            <td width="13%">
                                                <%--<form name="form1" method="post" action="">--%>
                                                <label>
                                                    <%-- <input type="checkbox" name="checkbox" value="checkbox" onclick="CheckBoxCheck(this);" id="checkbox1"/>--%>
                                                    <asp:CheckBox ID="checkbox1" runat="server" onclick="CheckBoxCheck(this);" />
                                                </label>
                                                <%--</form>--%>
                                            </td>
                                            <td width="12%">
                                                <asp:Button runat="server" ID="btnlocal" Text="L" CssClass="yellow" Style="border-radius: 100%;
                                                    height: 25px; width: 25px; color: white;" CommandName="Local" CommandArgument='<%# Eval("subId") %>'
                                                    OnClientClick="ExpandCollapse()" />
                                                <%-- OnClick="btnlocal_click"--%>
                                            </td>
                                            <td width="11%">
                                                <asp:Button runat="server" ID="btmAll" Text="A" CssClass="yellow" Style="border-radius: 100%;
                                                    height: 25px; width: 25px; color: white;" CommandName="All" CommandArgument='<%# Eval("subId") %>' /><%-- OnClick="btmAll_click""--%>
                                            </td>
                                            <td>
                                                <asp:Button runat="server" ID="btnhelp" Text="H" CssClass="yellow" Style="border-radius: 100%;
                                                    height: 25px; width: 25px; color: white;" OnClick="btnhelp_click" CommandName="help"
                                                    CommandArgument='<%# Bind("subId") %>' />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <tr id="Emptyrow" runat="server" visible="false">
                                            <td>
                                                <asp:Label ID="lblEmptyData" Text="No Data To Display" runat="server">
                                                </asp:Label>
                                            </td>
                                        </tr>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <%-- </asp:Panel>--%>
                                <%--<cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server" CollapseControlID="pneedhelp"
                                                    Collapsed="true" ExpandControlID="pneedhelp" TextLabelID="lblMessage" CollapsedText="Show"
                                                    ExpandedText="Hide" ImageControlID="imgArrows" ExpandDirection="Vertical" TargetControlID="Panel1"
                                                    ScrollContents="false">
                                                </cc1:CollapsiblePanelExtender>--%>
                            </ContentTemplate>
                            <%--<Triggers>
                        <asp:AsyncPostBackTrigger ControlID="txtsearch" />
                    </Triggers>--%>
                        </asp:UpdatePanel>
                        <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="Please select at least one record."
                            ClientValidationFunction="Validate" ForeColor="Red" ValidationGroup="chk"></asp:CustomValidator>
                        <div style="clear: both">
                        </div>
                        <div class="col-xs-12 profile_bottom" style="width: 96%; display: none;">
                            <table width="100%" border="0px" style="border: 0px;">
                                <tr>
                                    <td width="142">
                                        Completed
                                    </td>
                                    <td width="915">
                                        <div class="Completed_satus">
                                        </div>
                                    </td>
                                    <td width="160">
                                        Active
                                    </td>
                                    <td width="34">
                                        <div class="active_satus">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="col-xs-12 profile_bottom" style="width: 96%;">
                        <label for="exampleInputFile">
                            <i class="fa fa-paperclip attch_file"></i>Attachments...</label>
                        <asp:FileUpload ID="FileUpload1" runat="server" />
                        <asp:RegularExpressionValidator ID="reFile1" runat="server" ControlToValidate="FileUpload1"
                            CssClass="legend" Display="Dynamic" ErrorMessage="Please upload only .pdf File."
                            ValidationExpression="^.*\.(PDF|pdf)$" ForeColor="Red" ValidationGroup="chk"></asp:RegularExpressionValidator>
                    </div>
                    <div class="modal-footer">
                        <asp:Button runat="server" ID="Button1" Text="Save Changes" CssClass="btn btn-primary yellow"
                            ValidationGroup="chk" OnClick="btnsubmit_click" />
                    </div>
                    <%--</ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlcompetence" />
                        </Triggers>
                    </asp:UpdatePanel>--%>
                </div>
            </div>
        </div>
        <!-- Resualt-->
        <div class="col-md-6" style="margin-top: 30px; background: #f4f4f4; float: left;
            border-radius: 5px;">
            <div id="tabmenu">
                <%--   <asp:UpdatePanel runat="server" ID="UpdatePaneltabmenu" UpdateMode="Conditional">
                    <ContentTemplate>--%>
                <ul id="nav1">
                    <%--<li><a href="#" class="active">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">6</a></li>
                    <li><a href="#">7</a></li>
                    <li><a href="#">8</a></li>
                    <li><a href="#">9</a></li>--%>
                    <asp:Repeater ID="rep_tabmenu" runat="server">
                        <ItemTemplate>
                            <li><a href="#">
                                <%# Eval("no")%></a></li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
                <%-- </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlcompetence" />
                    </Triggers>
                </asp:UpdatePanel>--%>
                <div id="tab-content">
                    <%--<div id="tab1">
                        <h2 class="step_titel">
                            Sub Competencie 1</h2>
                        <form class="sub_competencie">
                        <label>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id neque quam. Aliquam
                            sollicitudin venenatis ipsum ac feugiat. Lorem ipsum dolor sit amet, consectetur
                            adipiscing elit. Morbi id neque quam. Aliquam sollicitu sollicitudin ve din venenatis
                            ipsum action.
                        </label>
                        </form>
                    </div>
                    <div id="tab2">
                        <h2 class="step_titel">
                            Sub Competencie 2</h2>
                        <form class="sub_competencie">
                        <label>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id neque quam. Aliquam
                            sollicitudin venenatis ipsum ac feugiat. Lorem ipsum dolor sit amet, consectetur
                            adipiscing elit. Morbi id neque quam. Aliquam sollicitu sollicitudin ve din venenatis
                            ipsum action. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id
                            neque quam. Aliquam sollicitudin venenatis ipsum ac feugiat. Lorem ipsum dolor sit
                            amet, consectetur adipiscing elit. Morbi id neque quam. Aliquam sollicitu sollicitudin
                            ve din venenatis ipsum action.</label>
                        </form>
                    </div>
                    <div id="tab3">
                        <h2 class="step_titel">
                            Sub Competencie 3</h2>
                        <form class="sub_competencie">
                        <label>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id neque quam. Aliquam
                            sollicitudin venenatis ipsum ac feugiat. Lorem ipsum dolor sit amet, consectetur
                            adipiscing elit. Morbi id neque quam. Aliquam sollicitu sollicitudin ve din venenatis
                            ipsum action.
                        </label>
                        </form>
                    </div>
                    <div id="tab4">
                        <h2 class="step_titel">
                            Sub Competencie 4</h2>
                        <form class="sub_competencie">
                        <label>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id neque quam. Aliquam
                            sollicitudin venenatis ipsum ac feugiat. Lorem ipsum dolor sit amet, consectetur
                            adipiscing elit. Morbi id neque quam. Aliquam sollicitu sollicitudin ve din venenatis
                            ipsum action.
                        </label>
                        </form>
                    </div>
                    <div id="tab5">
                        <h2 class="step_titel">
                            Sub Competencie 5</h2>
                        <form class="sub_competencie">
                        <label>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id neque quam. Aliquam
                            sollicitudin venenatis ipsum ac feugiat. Lorem ipsum dolor sit amet, consectetur
                            adipiscing elit. Morbi id neque quam. Aliquam sollicitu sollicitudin ve din venenatis
                            ipsum action.
                        </label>
                        </form>
                    </div>
                    <div id="tab6">
                        <h2 class="step_titel">
                            Sub Competencie 6</h2>
                        <form class="sub_competencie">
                        <label>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id neque quam. Aliquam
                            sollicitudin venenatis ipsum ac feugiat. Lorem ipsum dolor sit amet, consectetur
                            adipiscing elit. Morbi id neque quam. Aliquam sollicitu sollicitudin ve din venenatis
                            ipsum action. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id
                            neque quam. Aliquam sollicitudin venenatis ipsum ac feugiat. Lorem ipsum dolor sit
                            amet, consectetur adipiscing elit. Morbi id neque quam. Aliquam sollicitu sollicitudin
                            ve din venenatis ipsum action.</label>
                        </form>
                    </div>
                    <div id="tab7">
                        <h2 class="step_titel">
                            Sub Competencie 7</h2>
                        <form class="sub_competencie">
                        <label>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id neque quam. Aliquam
                            sollicitudin venenatis ipsum ac feugiat. Lorem ipsum dolor sit amet, consectetur
                            adipiscing elit. Morbi id neque quam. Aliquam sollicitu sollicitudin ve din venenatis
                            ipsum action.
                        </label>
                        </form>
                    </div>
                    <div id="tab8">
                        <h2 class="step_titel">
                            Sub Competencie 8</h2>
                        <form class="sub_competencie">
                        <label>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id neque quam. Aliquam
                            sollicitudin venenatis ipsum ac feugiat. Lorem ipsum dolor sit amet, consectetur
                            adipiscing elit. Morbi id neque quam. Aliquam sollicitu sollicitudin ve din venenatis
                            ipsum action.
                        </label>
                        </form>
                    </div>
                    <div id="tab9">
                        <h2 class="step_titel">
                            Sub Competencie 9</h2>
                        <form class="sub_competencie">
                        <label>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id neque quam. Aliquam
                            sollicitudin venenatis ipsum ac feugiat. Lorem ipsum dolor sit amet, consectetur
                            adipiscing elit. Morbi id neque quam. Aliquam sollicitu sollicitudin ve din venenatis
                            ipsum action.
                        </label>
                        </form>
                    </div>--%>
                    <asp:Repeater ID="rep_content" runat="server">
                        <ItemTemplate>
                            <div id="tab1">
                                <h2 class="step_titel">
                                    <%# Eval("subCompetence")%></h2>
                                <form class="sub_competencie">
                                <label style="width: 100%;">
                                    <%# Eval("subDescription")%>
                                </label>
                                </form>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
            <!-- TIME LINE -->
            <!-- Recent Post -->
        </div>
        <div class="col-md-6" style="margin-top: 20px; border-radius: 5px; background: #fff;">
            <div class="home_grap" style="border-radius: 5px;">
                <div id="graph-wrapper">
                    <h3 class="custom-heading" style="color: #000;">
                         <%= CommonMessages.EmployeeSkills%></h3>
                    <div class="graph-info">
                        <a href="#" id="bars"><span><i class="fa fa-bar-chart-o"></i></span></a><a href="#"
                            id="lines" class="active"><span><i class="fa fa-code-fork"></i></span></a>
                    </div>
                    <div class="graph-container">
                        <div id="graph-lines">
                        </div>
                        <div id="graph-bars">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%--<div class="modal-footer" style="border: 0px; width: 50%;">
            <%-- <button class="btn btn-primary yellow" type="button">
                Save Changes</button>--%>
        <%--<asp:Button runat="server" ID="btnsubmit" Text="Submit" CssClass="btn btn-primary yellow"
                OnClick="btnsubmit_click" />
        </div>--%>
        <<%--div class="modal-footer" style="border: 0px; margin-left: -54%; margin-top: -27%;">
          
            <asp:Button runat="server" ID="Button1" Text="Save Changes" CssClass="btn btn-primary yellow"
                ValidationGroup="chk" OnClick="btnsubmit_click" />
        </div>--%>
        <!-- Container -->
    </div>
    <!-- Wrapper -->
    <!-- RAIn ANIMATED ICON-->
    <asp:UpdatePanel runat="server" ID="updlocal">
        <ContentTemplate>
            <asp:LinkButton Text="" ID="lnkFake" runat="server" />
            <cc1:ModalPopupExtender ID="mpe" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkFake"
                CancelControlID="btnClose" BackgroundCssClass="modalBackground">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="margin-left: 170px !important;
                width: 17% !important; position: absolute !important; z-index: 100001 !important;
                top: 117px !important;">
                <div class="modal-header blue" style="color: #ffffff;">
                    <h4 class="modal-title">
                         <%= CommonMessages.Competence%></h4>
                    <div style="float: right; margin-top: -26px;">
                        <asp:Button ID="btnClose" runat="server" Text="×" CssClass="close" />
                    </div>
                </div>
                <div class="modal-body">
                    <%--<table border="0" cellpadding="3" cellspacing="0">
                <tr>
                    <td >--%>
                    <asp:Repeater ID="rptlocal" runat="server" OnItemDataBound="rptlocal_ItemDataBound"
                        OnItemCommand="rptlocal_ItemCommand">
                        <HeaderTemplate>
                            <table width="100%" border="1" bordercolor="#dfdfdf" class="skill_table" id="tblContacts">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td width="49%">
                                    <asp:Label ID="lblsubcom" runat="server" Text='<%# Eval("userFirstName")%>'></asp:Label>
                                    <asp:HiddenField ID="userId" runat="server" Value="<%# bind('userId') %>" />
                                    <asp:HiddenField ID="pintSubCompetance" runat="server" Value="<%# bind('pintSubCompetance') %>" />
                                    <asp:HiddenField ID="hdnhelpstatus" runat="server" Value="<%# bind('helpstatus') %>" />
                                    <asp:HiddenField ID="userFirstName" runat="server" Value="<%# bind('userFirstName') %>" />
                                </td>
                                <td width="13%" id="tdControl" runat="server">
                                    <asp:LinkButton ID="lnkpoint" runat="server" Text='<%# Eval("pointanscount")%>' CommandArgument='<%# Eval("userId") %>'
                                        ForeColor="Black" CommandName="popupcal"></asp:LinkButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            <tr id="Emptyrow" runat="server" visible="false">
                                <td>
                                    <asp:Label ID="lblEmptyData" Text="No Data To Display" runat="server">
                                    </asp:Label>
                                </td>
                            </tr>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <%--</td>
                </tr>
                
            </table>--%>
                </div>
            </asp:Panel>
            <asp:LinkButton Text="" ID="LinkButton1" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" PopupControlID="Panel1"
                TargetControlID="LinkButton1" CancelControlID="btnClose" BackgroundCssClass="modalBackground">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" Style="margin-left: 170px !important;
                width: 17% !important; position: absolute !important; z-index: 100001 !important;
                top: 117px !important;">
                <div class="modal-header blue" style="color: #ffffff;">
                    <h4 class="modal-title">
                         <%= CommonMessages.Competence%></h4>
                    <div style="float: right; margin-top: -26px;">
                        <asp:Button ID="Button2" runat="server" Text="×" CssClass="close" />
                    </div>
                </div>
                <div class="modal-body">
                    <%--<table border="0" cellpadding="3" cellspacing="0">
                <tr>
                    <td >--%>
                    <asp:Repeater ID="rpllocalhigh" runat="server" OnItemCommand="rpllocalhigh_ItemCommand"
                        OnItemDataBound="rpllocalhigh_ItemDataBound">
                        <HeaderTemplate>
                            <table width="100%" border="1" bordercolor="#dfdfdf" class="skill_table" id="tblContacts">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td width="49%">
                                    <asp:Label ID="lblsubcom" runat="server" Text='<%# Eval("userFirstName")%>'></asp:Label>
                                    <asp:HiddenField ID="userId" runat="server" Value="<%# bind('userId') %>" />
                                    <asp:HiddenField ID="pintSubCompetance" runat="server" Value="<%# bind('pintSubCompetance') %>" />
                                    <asp:HiddenField ID="hdnhelpstatus" runat="server" Value="<%# bind('helpstatus') %>" />
                                    <asp:HiddenField ID="userFirstName" runat="server" Value="<%# bind('userFirstName') %>" />
                                </td>
                                <td width="13%" id="tdControl" runat="server">
                                    <asp:LinkButton ID="lnkpoint" runat="server" Text='<%# Eval("pointanscount")%>' CommandArgument='<%# Eval("userId") %>'
                                        ForeColor="Black" CommandName="popupcal"></asp:LinkButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            <tr id="Emptyrow" runat="server" visible="false">
                                <td>
                                    <asp:Label ID="lblEmptyData" Text="No Data To Display" runat="server">
                                    </asp:Label>
                                </td>
                            </tr>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <%--</td>
                </tr>
                
            </table>--%>
                </div>
            </asp:Panel>
            <asp:LinkButton Text="" ID="lnkcalpopup" runat="server" />
            <cc1:ModalPopupExtender ID="mpecal" runat="server" PopupControlID="pnlPopupcal" TargetControlID="lnkcalpopup"
                CancelControlID="btnClosecal" BackgroundCssClass="modalBackground">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlPopupcal" runat="server" CssClass="modalPopup" Style="z-index: 100001 !important;
                top: 117px !important;">
                <div class="modal-header blue" style="color: #ffffff;">
                    <h4 class="modal-title">
                         <%= CommonMessages.Competence%></h4>
                    <div style="float: right; margin-top: -26px;">
                        <asp:Button ID="btnClosecal" runat="server" Text="×" CssClass="close" />
                    </div>
                </div>
                <div class="s-body">
                    <table border="0" cellpadding="3" cellspacing="0" width="100%">
                        <tr>
                            <%--<td style="width: 80px; padding-bottom: 25px;">
                        <b>Name: </b>
                    </td>--%>
                            <td style="padding-left: 25px;">
                                <asp:TextBox runat="server" placeholder="Name" ID="txtcname" MaxLength="50" Style="border: 1px solid #FFF;
                                    float: left; font-family: open sans; font-size: 15px; margin-bottom: 20px; padding: 10px 13px;
                                    width: 100%; transition: all 0.4s ease 0s;" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtcname"
                                    ErrorMessage="Please enter Name." CssClass="commonerrormsg" Display="Dynamic"
                                    ValidationGroup="chhk"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left: 25px;">
                                <asp:TextBox runat="server" Text="" ID="txtfromdate" MaxLength="10" placeholder="Date"
                                    Style="border: 1px solid #FFF; float: left; font-family: open sans; font-size: 15px;
                                    margin-bottom: 20px; padding: 10px 13px; width: 100%; transition: all 0.4s ease 0s;" />
                                <cc1:CalendarExtender ID="CalendarExtender2" PopupPosition="TopLeft" runat="server"
                                    TargetControlID="txtfromdate" Format="dd/MM/yyyy">
                                </cc1:CalendarExtender>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtfromdate"
                                    ErrorMessage="Please select Job Type." InitialValue="0" CssClass="commonerrormsg"
                                    Display="Dynamic" ValidationGroup="chhk"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left: 25px;">
                                <asp:TextBox runat="server" placeholder="Message" ID="txtmsg" TextMode="MultiLine"
                                    Style="border: 1px solid #FFF; float: left; font-family: open sans; font-size: 15px;
                                    margin-bottom: 20px; padding: 10px 13px; width: 100%; transition: all 0.4s ease 0s;"
                                    Width="100%" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtmsg"
                                    ErrorMessage="Please enter Message." CssClass="commonerrormsg" Display="Dynamic"
                                    ValidationGroup="chhk"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left: 25px;">
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <input type="button" onclick="myFunction()" id="btnSend" value="Save" class="btn btn-primary yellow" />
                    <button data-dismiss="modal" class="btn btn-default black" type="button">
                        <%= CommonMessages.Close%>
                    </button>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 10pt;
        }
        .modalBackground
        {
            background-color: Black;
            filter: alpha(opacity=40);
            opacity: 0.4;
        }
        .modalPopup1
        {
            background-color: #FFFFFF;
            width: 300px;
            border: 3px solid #0DA9D0;
        }
        .modalPopup
        {
            background: none repeat scroll 0 0 #ffffff;
            display: table;
            float: none;
            margin: 96px auto;
            width: 606px;
            top: 60px !important;
        }
        .modalPopup .header
        {
            background-color: #2FBDF1;
            height: 30px;
            color: White;
            line-height: 30px;
            text-align: center;
            font-weight: bold;
        }
        .modalPopup .body
        {
            min-height: 50px;
            line-height: 30px;
            text-align: center;
            padding: 5px;
        }
        .modalPopup .footer
        {
            padding: 3px;
        }
        .modalPopup .button
        {
            height: 23px;
            color: White;
            line-height: 23px;
            text-align: center;
            font-weight: bold;
            cursor: pointer;
            background-color: #9F9F9F;
            border: 1px solid #5C5C5C;
        }
        .modalPopup td
        {
            text-align: left;
        }
        
        .button
        {
            background-color: transparent;
        }
        .yellow
        {
            border-radius: 0px;
        }
    </style>
    <script>
        var icons = new Skycons();
        icons.set("rain", Skycons.RAIN);
        icons.play();
    </script>
    <script type="text/javascript">
        function Validate(sender, args) {
            debugger
            var gridView = document.getElementById("ContentPlaceHolder1_UpdatePanedrop");
            var checkBoxes = gridView.getElementsByTagName("input");
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {
                    args.IsValid = true;
                    return;
                }
            }
            args.IsValid = false;

        }
    </script>
    <script>
        function myFunction() {
            //            debugger;
            var txtto = $('#<%=txtcname.ClientID %>').val();
            var txtsubject = $('#<%=txtfromdate.ClientID %>').val();
            var txtmessge = $('#<%=txtmsg.ClientID %>').val();

            if (txtto != '' && txtsubject != '' && txtmessge != '') {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Employee_Skill.aspx/UpdateData",
                    data: "{'txtto':'" + txtto + "','txtsubject':'" + txtsubject + "','txtmessge':'" + txtmessge + "'}",
                    dataType: "json",
                    success: function (data) {
                        var obj = data.d;
                        if (obj == 'true') {
                            $('#<%=txtcname.ClientID %>').val('');
                            $('#<%=txtfromdate.ClientID %>').val('');
                            $('#<%=txtmsg.ClientID %>').val('');
                            alert('Save SuccessFully')
                            location.reload();
                            // mpe.Show();
                            self.location.assign(location)
                            //document.getElementById('btnClosecal').click();
                        }
                    },
                    error: function (result) {
                        alert("Error");
                    }
                });
            }
            else {
                alert('Please enter all the fields')
                return false;
            }

        }


    </script>
    <script type="text/javascript">
        function CheckBoxCheck(rb) {
            //debugger;
            var gv = document.getElementById("<%=UpdatePanedrop.ClientID%>");
            // var row = rb.parentNode.parentNode;
            var rbs = gv.getElementsByTagName("input");
            for (var i = 0; i < rbs.length; i++) {
                if (rbs[i].type == "checkbox") {
                    if (rbs[i].checked && rbs[i] != rb) {
                        rbs[i].checked = false;
                        break;
                    }
                }
            }
        }

    </script>
    <%--<script type="text/javascript">

        function ExpandCollapse() {
            alert("asd");
            //debugger;
            //            var collPanel = document.getElementById("<%=CollapsiblePanelExtender1.ClientID%>");
            var collPanel = document.getElementById("<%=UpdatePanedrop.ClientID%>");
            if (collPanel.get_Collapsed())
                collPanel.set_Collapsed(false);
            else
                collPanel.set_Collapsed(true);
        }      
 
    </script> --%>
</asp:Content>
