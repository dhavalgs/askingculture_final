﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using System.Threading;
using System.Globalization;
using startetku.Business.Logic;
using System.Collections;


public partial class Organisation_OrgJobType : System.Web.UI.Page
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        //txtJobTypeNameDn.Attributes["placeholder"] = GetLocalResourceObject("txtJobTypeNameDnResource1.Text").ToString();
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            JobType.InnerHtml = GetLocalResourceObject("Welcome.Text").ToString() + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                GetAllcompetenceCategory();
                GetAllJobtypebyid();
            }
        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion

    #region method
    protected void SetDefaultMessage()
    {

        string id = "?id=";
        string childurl = "child.aspx";
        if (Request.RawUrl.ToString().ToLower().Contains(childurl.ToLower()))
        {
            if (Request.RawUrl.ToString().ToLower().Contains(id.ToLower()))
            {
                // lblDataDisplayTitle.Text = "Edit Children";

            }
            else
            {
                // lblDataDisplayTitle.Text = "Add Children";
            }
        }
    }
    protected void InsertJobtype()
    {
        JobTypeBM obj2 = new JobTypeBM();
        obj2.JobTypeCheckDuplication(txtJobTypeName.Text, -1, -1);
        String returnMsg = obj2.ReturnString;
        if (returnMsg == "")
        {
            JobTypeBM obj = new JobTypeBM();
            obj.jobName = txtJobTypeName.Text;
            obj.jobNameDN = txtJobTypeNameDn.Text;
            obj.jobCompanyId = Convert.ToInt32(Session["OrgUserId"]);
            obj.jobIsActive = true;
            obj.jobIsDeleted = false;
            obj.jobCreatedDate = DateTime.Now;

            //
            string str_clk_list_QualityArea = string.Empty;
            for (int i = 0; i < chkList.Items.Count; i++)
            {
                if (chkList.Items[i].Selected)
                {
                    if (string.IsNullOrEmpty(str_clk_list_QualityArea))
                    {
                        str_clk_list_QualityArea = chkList.Items[i].Value;
                    }
                    else
                    {
                        str_clk_list_QualityArea = str_clk_list_QualityArea + "," + chkList.Items[i].Value;
                    }
                }
            }
            //
            obj.jobcatid = str_clk_list_QualityArea;
            obj.InsertJobType();
            DataSet ds = obj.ds;

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    TargetMaxLongBM obj1 = new TargetMaxLongBM();
                    obj1.CompanyID = Convert.ToInt32(Session["OrgCompanyId"]);
                    obj1.UserID = Convert.ToInt32(Session["OrgUserId"]);
                    obj1.tarType = "JobType";
                    obj1.RefID = Convert.ToInt32(ds.Tables[0].Rows[0]["JobID"]);
                    obj1.ComptenceCatIDs = obj.jobcatid;

                    obj1.InsertUpdateTargetMaxLong();
                }
            }
            if (obj.ReturnBoolean == true)
            {
                Response.Redirect("OrgJobTypeList.aspx?msg=ins");
            }
        }
        else
        {
            if (returnMsg == "jobName")
                lblMsg.Text = CommonModule.msgNameAlreadyExists;
            else
                lblMsg.Text = CommonModule.msgProblemInsertRecord;

        }

    }
    protected void updateJobtype()
    {
        JobTypeBM obj2 = new JobTypeBM();
        obj2.JobTypeCheckDuplication(txtJobTypeName.Text, Convert.ToInt32(Request.QueryString["id"]), Convert.ToInt32(Session["OrgCompanyId"]));
        String returnMsg = obj2.ReturnString;
        if (returnMsg == "")
        {
            JobTypeBM obj = new JobTypeBM();
            obj.jobId = Convert.ToInt32(Request.QueryString["id"]);
            obj.jobName = txtJobTypeName.Text;
            obj.jobNameDN = txtJobTypeNameDn.Text;
            obj.jobUpdatedDate = DateTime.Now;
            obj.jobDescription = txtDESCRIPTION.Text;
            obj.jobDepId = Convert.ToInt32(ddlDepartment.SelectedValue);
            //
            string str_clk_list_QualityArea = string.Empty;
            for (int i = 0; i < chkList.Items.Count; i++)
            {
                if (chkList.Items[i].Selected)
                {
                    if (string.IsNullOrEmpty(str_clk_list_QualityArea))
                    {
                        str_clk_list_QualityArea = chkList.Items[i].Value;
                    }
                    else
                    {
                        str_clk_list_QualityArea = str_clk_list_QualityArea + "," + chkList.Items[i].Value;
                    }
                }
            }
            //
            obj.jobcatid = str_clk_list_QualityArea;
            obj.UpdateJobType();

            #region TargetSet
                TargetMaxLongBM obj1 = new TargetMaxLongBM();
                obj1.CompanyID = Convert.ToInt32(Session["OrgCompanyId"]);
                obj1.UserID = Convert.ToInt32(Session["OrgUserId"]);
                obj1.tarType = "JobType";
                obj1.RefID = Convert.ToInt32(Request.QueryString["id"]);
                obj1.ComptenceCatIDs = obj.jobcatid;

                obj1.InsertUpdateTargetMaxLong();
            #endregion

            if (obj.ReturnBoolean == true)
            {
                Response.Redirect("OrgJobTypeList.aspx?msg=upd");
            }
        }
        else
        {
            if (returnMsg == "jobName")
                lblMsg.Text = CommonModule.msgNameAlreadyExists;
            else
                lblMsg.Text = CommonModule.msgProblemInsertRecord;
        }
    }
    protected void GetAllJobtypebyid()
    {
        JobTypeBM obj = new JobTypeBM();
        obj.jobId = Convert.ToInt32(Request.QueryString["id"]);
        obj.GetAllJobTypebyid();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["jobName"])))
                txtJobTypeName.Text = Convert.ToString(ds.Tables[0].Rows[0]["jobName"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["jobNameDN"])))
                txtJobTypeNameDn.Text = Convert.ToString(ds.Tables[0].Rows[0]["jobNameDN"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["jobDepId"])))
                ddlDepartment.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["jobDepId"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["jobDescription"])))
                txtDESCRIPTION.Text = Convert.ToString(ds.Tables[0].Rows[0]["jobDescription"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["jobcatId"])))
            //var x= "1";
            {
                var catIds = Convert.ToString(ds.Tables[0].Rows[0]["jobcatId"]);
                string[] list = catIds.Split(Convert.ToChar(","));
                //list.AddRange(values.Split(new char[] { ',' }));
                for (var j = 0; j <= chkList.Items.Count - 1; j++)
                {
                    foreach (var i in list)
                    {
                        if (chkList.Items[j].Value.Contains(i))
                        {
                            chkList.Items[j].Selected = true;
                            //  break;
                        }
                    }
                }
            }

        }

    }
    //protected void GetAllDepartments()
    //{
    //    DepartmentsBM obj = new DepartmentsBM();
    //    obj.depIsActive = true;
    //    obj.depIsDeleted = false;
    //    obj.depCompanyId = 0;
    //    obj.GetAllDepartments();
    //    DataSet ds = obj.ds;

    //    if (ds != null)
    //    {
    //        if (ds.Tables[0].Rows.Count > 0)
    //        {
    //            ddlDepartment.Items.Clear();

    //            ddlDepartment.DataSource = ds.Tables[0];
    //            ddlDepartment.DataTextField = "depName";
    //            ddlDepartment.DataValueField = "depId";
    //            ddlDepartment.DataBind();

    //            ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownSelectDepartments, CommonModule.dropDownZeroValue));
    //        }
    //        else
    //        {
    //            ddlDepartment.Items.Clear();
    //            ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownSelectDepartments, CommonModule.dropDownZeroValue));
    //        }
    //    }
    //    else
    //    {
    //        ddlDepartment.Items.Clear();
    //        ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownSelectDepartments, CommonModule.dropDownZeroValue));
    //    }

    //}
    protected void GetAllcompetenceCategory()
    {
        CompetenceMasterBM obj = new CompetenceMasterBM();
        obj.catIsActive = true;
        obj.catIsDeleted = false;
        obj.catCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllcompetenceCategory();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {

                if (Convert.ToString(Session["Culture"]) == "Danish")
                {
                    ds.Tables[0].Columns["catName"].ColumnName = "abcd";
                    ds.Tables[0].Columns["catNameDN"].ColumnName = "catName";

                }


                ddlDepartment.Items.Clear();

                ddlDepartment.DataSource = ds.Tables[0];
                ddlDepartment.DataTextField = "catName";
                ddlDepartment.DataValueField = "catId";
                ddlDepartment.DataBind();

                chkList.DataSource = ds.Tables[0];
                chkList.DataTextField = "catName";
                chkList.DataValueField = "catId";
                chkList.DataBind();


                //chkList.Items.Insert(0, new ListItem(CommonModule.dropDownCategory, CommonModule.dropDownZeroValue));

                ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownCategory, CommonModule.dropDownZeroValue));
            }
            else
            {
                ddlDepartment.Items.Clear();
                ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownCategory, CommonModule.dropDownZeroValue));

                chkList.Items.Clear();
                //ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownCategory, CommonModule.dropDownZeroValue));
            }


        }
        else
        {
            chkList.Items.Clear();
            ddlDepartment.Items.Clear();
            ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownCategory, CommonModule.dropDownZeroValue));
        }

    }
    #endregion


    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Request.QueryString["id"]))
        {
            updateJobtype();
        }
        else
        {
            InsertJobtype();
        }
    }
    protected void btnCancel_click(object sender, EventArgs e)
    {
        Response.Redirect("OrgJobTypeList.aspx");
    }
    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        //string language = "en-GB";
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Danish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion
}