﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Organisation/OrganisationMaster.master"
    CodeFile="OrgTeamList.aspx.cs" Inherits="Organisation_OrgTeamList" 
    Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" 
    %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/ajaxtab.css" rel="stylesheet" />
    <style type="text/css">
        .yellow
        {
            border-radius: 0px;
        }
        body
        {
            font-family: Geneva,Arial,Helvetica,sans-serif;
        }
        #ContentPlaceHolder1_chkList input
        {
            width: 33px;
            margin-bottom: 0px !important;
        }
        #ContentPlaceHolder1_chkList label
        {
            margin-top: 2px;
        }
        label
        {
            display: inline-block;
            font-size: 14px;
            font-weight: inherit;
            margin-bottom: 5px;
        }
        .table > thead > tr > th
        {
            vertical-align: middle;
        }
    </style>
    <script src="../Scripts/sMain.js?version=1.0" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        .scroll_checkboxes
        {
            height: 120px;
            width: 100%;
            padding: 5px;
            overflow: auto;
            border: 1px solid #ccc;
        }
        
        .FormText
        {
            font-size: 11px;
            font-family: tahoma,sans-serif;
        }
    </style>
      <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            //  alert();
            setTimeout(function () {
                //ExampanCollapsMenu();
                //ExampanCollapssubMenu();
                SetExpandCollapse();
                // tabMenuClick();
                $(".OrgTeamList").addClass("active_page");
            }, 500);
        });
    </script>
    <script language="javascript">

        var color = 'White';

        function changeColor(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=chkList.ClientID%>");
            if (color == '') {
                color = getRowColor();
            }
            if (obj.checked) {
                rowObject.style.backgroundColor = '#A3B1D8';
            }
            else {
                rowObject.style.backgroundColor = color;
                color = 'White';
            }

            // private method
            function getRowColor() {
                if (rowObject.style.backgroundColor == 'White') return parentTable.style.backgroundColor;
                else return rowObject.style.backgroundColor;
            }

        }

        // This method returns the parent row of the object

        function getParentRow(obj) {
            do {
                obj = obj.parentElement;
            }
            while (obj.tagName != "TR")
            return obj;
        }


        function TurnCheckBoixGridView(id) {
            var frm = document.forms[0];

            for (i = 0; i < frm.elements.length; i++) {
                if (frm.elements[i].type == "checkbox" && frm.elements[i].id.indexOf("<%= chkList.ClientID %>") == 0) {
                    frm.elements[i].checked = document.getElementById(id).checked;
                }
            }
        }

        function SelectAll(id) {

            var parentTable = document.getElementById("<%=chkList.ClientID%>");
            var color

            if (document.getElementById(id).checked) {
                color = '#A3B1D8'
            }
            else {
                color = 'White'
            }

            for (i = 0; i < parentTable.rows.length; i++) {
                parentTable.rows[i].style.backgroundColor = color;
            }
            TurnCheckBoixGridView(id);

        }

    </script>
    <div class="col-md-6">
        <div class="heading-sec">
            <h1 style="margin-left: 15px;">
               <asp:Label ID="Literal4" runat="server" meta:resourcekey="Team" enableviewstate="false"/>  <i><span runat="server" id="Team"></span></i>
            </h1>
        </div>
    </div>
  
    <div class="col-md-12" style="margin-top: 20px;">
        <div id="graph-wrapper">
            <div class="col-md-12">
                <a href="#add-post-title" data-toggle="modal" title="" style="margin-bottom: 15px;">
                    <button style="border: 0px;" class="btn btn-primary yellow lrg-btn flat-btn add_user" type="button">
                         <asp:Literal ID="Literal1" runat="server" meta:resourcekey="AddNewTeam" enableviewstate="false"/>
                      <%--  <%= CommonMessages.AddNew%>--%>
                    </button>
                </a>


                <div aria-hidden="true" role="dialog" tabindex="-1" class="modal fade" id="add-post-title"
                    style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header blue yellow-radius">
                                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                                    ×
                                </button>
                                <h4 class="modal-title">
                                  <asp:Literal ID="Literal2" runat="server" meta:resourcekey="AddNewTeam" EnableViewState="false" />
                                </h4>
                            </div>
                            <div class="modal-body">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                                    <ContentTemplate>
                                        <asp:TextBox runat="server"  ID="txtTeamName" MaxLength="50"
                                            meta:resourcekey="txtTeamNamePlaceHolder" AutoPostBack="true" OnTextChanged="txtTeamName_TextChanged" />
                                        <asp:Label runat="server" ID="lblJobTypeName" CssClass="commonerrormsg"></asp:Label>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtTeamName"
                                    ErrorMessage="Please enter Team Name." CssClass="commonerrormsg" Display="Dynamic"
                                    ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator5Resource1"></asp:RequiredFieldValidator>
                                        <br>
                                         <asp:TextBox runat="server" ID="txtTeamNameDN" MaxLength="50"
                                            meta:resourcekey="txtTeamNameDnResource1" AutoPostBack="true"  />

                                        <asp:Label runat="server" ID="Label1" CssClass="commonerrormsg"></asp:Label>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtTeamNameDN"
                                     CssClass="commonerrormsg" Display="Dynamic"
                                    ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator2Resource1"></asp:RequiredFieldValidator>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <br />
                                <div class="widget-body custom-form" style="margin-bottom: 10px; text-transform: uppercase;
                                    display: none;">
                                    <div class="sec">
                                        <asp:DropDownList ID="ddlDepartment" runat="server" Style="width: 100%; height: 32px;
                                            display: inline;" CssClass="form-control" meta:resourcekey="ddlDepartmentResource1">
                                        </asp:DropDownList>
                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="ddlDepartment"
                                            ErrorMessage="Please select Department." InitialValue="0" CssClass="commonerrormsg"
                                            Display="Dynamic" ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator14Resource1"></asp:RequiredFieldValidator>--%>
                                    </div>
                                </div>
                                <div class="scroll_checkboxes">
                                  <asp:Label ID="Label3" runat="server" meta:resourcekey="Categoty" enableviewstate="false"/>   :<br />
                                    <asp:CheckBoxList Width="100%" ID="chkList" runat="server" CssClass="FormText" RepeatDirection="Vertical"
                                        RepeatColumns="1" BorderWidth="0" Datafield="description" DataValueField="value">
                                    </asp:CheckBoxList>
                                   
                                    
                                </div><br/> <%--<asp:CustomValidator ID="CustomValidator1" ErrorMessage="Please select at least one item."
                                        ForeColor="Red" ClientValidationFunction="ValidateCheckBoxList" runat="server"  Font-Size="13px"
                                        ValidationGroup="chk" />--%>
                                <br />
                                <br />
                                <div class="Description">
                                    <asp:TextBox runat="server"  ID="txtDESCRIPTION" MaxLength="500"
                                    TextMode="MultiLine" Rows="5" meta:resourcekey="DESCRIPTIONPlaceHolder" />
                                </div>
                                
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDESCRIPTION"
                                    ErrorMessage="Please Enter DESCRIPTION." CssClass="commonerrormsg" Display="Dynamic"
                                    ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator1Resource1"></asp:RequiredFieldValidator>--%>
                            </div>
                            <div class="modal-footer">
                                <asp:Button runat="server" ID="btnsubmit" Text="Submit" CssClass="btn btn-primary yellow"
                                    OnClientClick="CheckValidations('chk');generate('warning','Please wait','bottomCenter');" ValidationGroup="chk" OnClick="btnsubmit_Click"
                                    Style="border-radius: 5px;" meta:resourcekey="btnsubmitResource1" />
                                <button data-dismiss="modal" class="btn btn-default black" type="button">
                                    <%-- <%= CommonMessages.Close%>--%><asp:Literal ID="Literal3" runat="server" meta:resourcekey="Close"
                                        EnableViewState="false" />
                                </button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                </div> <br />
                <div style="clear:both;"></div>
                <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" meta:resourcekey="lblMsgResource1"></asp:Label>
                <br />
                <br />
                <div class="chart-tab manager_table">
                    <%-- <div id="tabs-container">--%>
                    <%--<cc1:TabContainer ID="TabContainer1" runat="server" CssClass="Tab">
                            <cc1:TabPanel ID="tbpnluser" runat="server">
                                <HeaderTemplate>
                                    Job Type
                                    <a></a>
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <div class="invoice" style="background-color: white; margin-top: 0px;">--%>
                    <asp:GridView ID="gvGrid" runat="server" AutoGenerateColumns="False" CellPadding="0"
                        Width="100%" GridLines="None" DataKeyNames="TeamID" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                        EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>' OnRowCommand="gvGrid_RowCommand"
                        OnRowDataBound="gvGrid_RowDataBound" meta:resourcekey="gvGridResource1">
                        <HeaderStyle CssClass="aa" />
                        <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:TemplateField HeaderText="ID" meta:resourcekey="TemplateFieldResource1">
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1 %>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" Font-Size="14px" />
                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                    Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                            </asp:TemplateField>
                            <%-- <asp:TemplateField HeaderText="JOB NAME">
                                <ItemTemplate>
                                    <asp:Label ID="lblUserNamer" runat="server" Text="<%# bind('jobName') %>"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                    Width="50%" ForeColor="White" />
                            </asp:TemplateField>--%>
                            <%--<asp:TemplateField HeaderText="CATEGORY" meta:resourcekey="TemplateFieldResource2">
                                <ItemTemplate>
                                    <asp:Label ID="lblUserNamer" runat="server" Text="<%# bind('catName') %>" meta:resourcekey="lblUserNamerResource1"></asp:Label>
                                    <asp:HiddenField ID="jobCompanyId" runat="server" Value="<%# bind('jobCompanyId') %>" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Font-Size="14px" />
                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                    Width="50%" ForeColor="White" Font-Size="16px" BorderWidth="0px" />
                            </asp:TemplateField>--%>
                            <asp:TemplateField HeaderText="Team Name" meta:resourcekey="TeamName">
                                <ItemTemplate>
                                     <% if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["Danish"])))
                                        { %>

                                    <asp:Label ID="lblTeamName" runat="server" Text='<%# Eval("TeamName") %>'></asp:Label>
                                    
                                     <% }
                                        else
                                        {
                                     %>
                                     <asp:Label ID="Label2" runat="server" Text='<%# Eval("TeamNameDN") %>'></asp:Label>
                                    <% } %>
                                    

                                    <asp:HiddenField ID="TeamCompanyID" runat="server" Value='<%# Eval("TeamCompanyID") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Font-Size="14px" />
                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                    Width="50%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" ItemStyle-Width="50px" meta:resourcekey="Action">
                                <ItemTemplate>
                                    <div class="vat" style="width: 70px;" id="divEdit" runat="server">
                                        <p>
                                            <i class="fa fa-pencil"></i><a href="<%# String.Format("OrgTeam.aspx?id={0}", Eval("TeamID")) %>"
                                                title="Edit"><asp:Label runat="server" meta:resourcekey="Edit"></asp:Label></a>
                                        </p>
                                    </div>
                                    <div class="total" style="width: 70px;" id="divDeletet" runat="server">
                                        <p>
                                            <i class="fa fa-trash-o"></i>
                                            <asp:LinkButton ID="lnkBtnName" runat="server" CommandName="archive" CommandArgument='<%# Eval("TeamID") %>'
                                                ToolTip="Delete" OnClientClick="return confirm('Are you sure you want to Delete this record?');"
                                                meta:resourcekey="lnkBtnNameResource1"></asp:LinkButton>
                                        </p>
                                    </div>
                                   
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" Font-Size="14px" />
                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                    Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px"/>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <%-- </div>
                                </ContentTemplate>
                            </cc1:TabPanel>
                            <cc1:TabPanel ID="tbpnlusrdetails" runat="server">
                                <HeaderTemplate>
                                    Archive
                                    <a></a>
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <div class="invoice" style="background-color: white; margin-top: 0px;">--%>
                  
                    <%-- </div>
                                </ContentTemplate>
                            </cc1:TabPanel>
                        </cc1:TabContainer>--%>
                </div>
                  <a href="#" onclick="javascript:history.back(); return false;" class="btn black pull-right" style="border-radius: 5px;margin-left:0px;margin-top:5px;float:right;display: none">
                     <asp:Label runat="server" ID="Label4" CssClass="commonerrormsg" meta:resourcekey="Back"></asp:Label>  
                  </a>
            </div>
        </div>
    </div>
    </div>
    <script type="text/javascript">
        function ValidateCheckBoxList(sender, args) {
            var checkBoxList = document.getElementById("<%=chkList.ClientID %>");
            var checkboxes = checkBoxList.getElementsByTagName("input");
            var isValid = false;
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].checked) {
                    isValid = true;
                    break;
                }
            }
            args.IsValid = isValid;
            //alert("Please select at least one job type.");
        }

        function GetTeamlang(catId, catName, t) {

            debugger;
            var dataText = $(t).text();

            $("#ContentPlaceHolder1_lblTeamName").text(dataText);

            $.ajax({
                type: "POST",
                cache: false,
                async: true,
                url: "OrgTeamList.aspx/GetTeamlang",
                data: '{catId:"' + catId + '",catName:"' + catName + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(response) {
                    alert("success");
                    if ($("#ContentPlaceHolder1_Hdn_Lang_id").val() == "English") {
                        var ttt = response.d[0].catName;
                        ttt = ttt.replace(/(<p[^>]+?>|<p>|<\/p>)/img, "");
                    }
                    if ($("#ContentPlaceHolder1_Hdn_Lang_id").val() == "Danish") {
                        var ttt = response.d[0].catNameDN;
                        ttt = ttt.replace(/(<p[^>]+?>|<p>|<\/p>)/img, "");
                    }


                },

                failure: function(msg) {

                    alert(msg);
                }
            });
        }
    </script>
</asp:Content>
