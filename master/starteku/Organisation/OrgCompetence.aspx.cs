﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using System.Web.UI.HtmlControls;
using startetku.Business.Logic;
using System.Threading;
using System.Globalization;

public partial class Organisation_OrgCompetence : System.Web.UI.Page
{
    string id = "";
    string id1 = "";

    public static String GroupName
    {
        get
        {
            return "Test";
        }

    }

    #region Page Event
    DataTable dt = new DataTable();
    int queid = 0;
    DataSet ds;
    public static int ans = 0;

    protected void Page_Load(object sender, EventArgs e)
    {

        Page.ClientScript.RegisterStartupScript(Page.GetType(), "my", " SetExpandCollapse();", true);

        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        //setDefault();
        if (!IsPostBack)
        {
            firstimeload();
            Competence.InnerHtml = GetLocalResourceObject("Welcome.Text").ToString() + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            //Competence.InnerHtml = "Welcome   " + Convert.ToString(Session["OrgUserName"]) + "!";           
            GetAllcompetenceCategory();
        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion

    #region method
    protected void firstimeload()
    {
        if (!string.IsNullOrEmpty(Request.QueryString["id"]))
        {
            GetAllCompetenceAddbyid();
            //int ans = ds.Tables[1].Rows.Count ;
            //for (int i = 0; i < ans; i++)
            //{
            //    ds.Tables[1].Rows.RemoveAt(ds.Tables[1].Rows.Count - 1);
            //}
            //if (ds.Tables[1].Rows.Count > Convert.ToInt32(5))
            //{
            //    int ans = ds.Tables[1].Rows.Count - Convert.ToInt32(5);
            //    for (int i = 0; i < ans; i++)
            //    {
            //        ds.Tables[1].Rows.RemoveAt(ds.Tables[1].Rows.Count - 1);
            //    }

            //}
            //else
            //{
            //    int ans = Convert.ToInt32(5) - ds.Tables[1].Rows.Count;
            //    for (int i = 0; i < ans; i++)
            //    {
            //        DataRow dr = ds.Tables[1].NewRow();
            //        dr["answer"] = " ";
            //        ds.Tables[1].Rows.Add(dr);
            //    }
            //}
            //Repeater1.DataSource = ds.Tables[1];
            //Repeater1.DataBind();
        }
        else
        {
            if (Repeater1.Controls.Count <= 0)
            {
                int val = Convert.ToInt32(5);
                dt.Columns.Add("comchildlevel", typeof(String));
                dt.Columns.Add("comchildlevelDN", typeof(String));

                for (int i = 0; i < val; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr["comchildlevel"] = "";
                    dr["comchildlevelDN"] = "";
                    dt.Rows.Add(dr);
                }

                Repeater1.DataSource = dt;
                Repeater1.DataBind();
            }
            //else
            //{
            //    if ((Repeater1.Controls.Count) < Convert.ToInt32(5))
            //    {
            //        dt.Columns.Add("answer", typeof(String));
            //        for (int i = 0; i < Repeater1.Controls.Count; i++)
            //        {
            //            TextBox box = (TextBox)Repeater1.Controls[i].FindControl("TextBox1");
            //            RadioButton rbMyRadio = (RadioButton)Repeater1.Controls[i].FindControl("RadioButton1");

            //            DataRow dr = dt.NewRow();
            //            dr["answer"] = box.Text;
            //            dt.Rows.Add(dr);
            //        }
            //        int add_new = Convert.ToInt32(5) - (Repeater1.Controls.Count);
            //        for (int j = 0; j < add_new; j++)
            //        {
            //            DataRow dr = dt.NewRow();
            //            dr["answer"] = "";
            //            dt.Rows.Add(dr);
            //        }
            //    }
            //    else
            //    {
            //        dt.Columns.Add("answer", typeof(String));
            //        for (int i = 0; i < Repeater1.Controls.Count; i++)
            //        {
            //            TextBox box = (TextBox)Repeater1.Controls[i].FindControl("TextBox1");
            //            RadioButton rbMyRadio = (RadioButton)Repeater1.Controls[i].FindControl("RadioButton1");

            //            DataRow dr = dt.NewRow();
            //            dr["answer"] = box.Text;
            //            dt.Rows.Add(dr);
            //        }
            //        int remove_new = (Repeater1.Controls.Count) - Convert.ToInt32(5);
            //        for (int j = 0; j < remove_new; j++)
            //        {
            //            dt.Rows.RemoveAt(dt.Rows.Count - 1);
            //        }
            //    }
            //    Repeater1.DataSource = dt;
            //    Repeater1.DataBind();
            //}
        }
    }
    protected void SetDefaultMessage()
    {

        string id = "?id=";
        string childurl = "child.aspx";
        if (Request.RawUrl.ToString().ToLower().Contains(childurl.ToLower()))
        {
            if (Request.RawUrl.ToString().ToLower().Contains(id.ToLower()))
            {
                lblDataDisplayTitle.Text = "Edit Children";

            }
            else
            {
                lblDataDisplayTitle.Text = "Add Children";
            }
        }
    }
    //protected void setDefault()
    //{
    //    if (!String.IsNullOrEmpty(Request.QueryString["id"]))
    //    {
    //        //GetAllCompetencebyid();
    //    }
    //    if (!String.IsNullOrEmpty(Request.QueryString["Add"]))
    //    {
    //        //first.Visible = false;
    //        //second.Visible = true;
    //        //btnAdd.Visible = true;
    //        //btnAddque.Visible = false;
    //        //btnsubmit.Visible = false;
    //        //  Addmore.Visible = true;
    //    }
    //    else
    //    {
    //        //first.Visible = true;
    //        //second.Visible = false;
    //        //btnAdd.Visible = false;
    //        // Addmore.Visible = false;
    //    }
    //    if (!String.IsNullOrEmpty(Request.QueryString["cid"]))
    //    {

    //        GetAllQuestionbyid();
    //        //set_data();
    //       // first.Visible = false;
    //        //second.Visible = true;
    //        //btnAdd.Visible = true;
    //        //btnAddque.Visible = false;
    //        btnsubmit.Visible = false;
    //        // Addmore.Visible = true;
    //    }

    //}
    //protected void GetAllJobtype()
    //{
    //    JobTypeBM obj = new JobTypeBM();
    //    obj.jobIsActive = true;
    //    obj.jobIsDeleted = false;
    //    obj.jobCompanyId = Convert.ToInt32(Session["OrgUserId"]);
    //    obj.GetAllJobType();
    //    DataSet ds = obj.ds;

    //    if (ds != null)
    //    {
    //        if (ds.Tables[0].Rows.Count > 0)
    //        {
    //            ddljobtype.Items.Clear();

    //            ddljobtype.DataSource = ds.Tables[0];
    //            ddljobtype.DataTextField = "jobName";
    //            ddljobtype.DataValueField = "jobId";
    //            ddljobtype.DataBind();

    //            ddljobtype.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
    //        }
    //        else
    //        {
    //            ddljobtype.Items.Clear();
    //            ddljobtype.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
    //        }
    //    }
    //    else
    //    {
    //        ddljobtype.Items.Clear();
    //        ddljobtype.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
    //    }

    //}
    protected void InsertCompetence()
    {
        CompetenceMasterBM obj = new CompetenceMasterBM();
        obj.comCompetence = txtCompetence.Text;
        obj.comCompetenceDN = txtCompetenceDN.Text;
        obj.comLevel = "";
        obj.comUserId = Convert.ToInt32(Session["OrgUserId"]);
        obj.comCompanyId = Convert.ToInt32(Session["OrgUserId"]);
        obj.comIsActive = true;
        obj.comIsDeleted = false;
        obj.comCreatedDate = DateTime.Now;
        obj.comCompanyId = Convert.ToInt32(Session["OrgUserId"]);
        obj.comcategoryId = Convert.ToInt32(ddlcategory.SelectedValue);
        //obj.comDepartmentId = GETDepartment(TreeView1.Nodes);
       // obj.comIndId = GETDivision(TreeView2.Nodes);
        //
        //queid = obj.InsertCompetence();
        queid = obj.InsertCompetenceMasteAdd();
        insertCompetencechild(queid);
        ViewState["queid"] = queid;
        // save_Que_and_ans();
        if (queid > 0)
        {
           // Response.Redirect("OrgCompetenceList.aspx?msg=ins");
            Response.Redirect("AddCompetence.aspx?msg=ins");
        }


        //CompetenceBM obj = new CompetenceBM();
        //obj.comCompetence = txtCompetence.Text;
        //obj.comLevel = Convert.ToInt32(txtlevel.Text);
        //obj.comJobId = Convert.ToInt32(ddljobtype.SelectedValue);
        //obj.comCompanyId = Convert.ToInt32(Session["OrgUserId"]);
        //obj.comIsActive = true;
        //obj.comIsDeleted = false;
        //obj.comCreatedDate = DateTime.Now;
        //obj.comQuestion = txt_Question.Text;
        //obj.comQuestionNo = Convert.ToInt32(ddlNumber.SelectedValue);
        //obj.comDepartmentId = GETDepartment(TreeView1.Nodes);
        //obj.comIndId = GETDivision(TreeView2.Nodes);
        ////
        //queid = obj.InsertCompetence();
        //ViewState["queid"] = queid;
        //// save_Que_and_ans();
        //if (queid > 0)
        //{
        //    Response.Redirect("OrgCompetenceList.aspx?msg=ins");
        //}

    }
    protected void insertCompetencechild(Int32 id)
    {
        foreach (RepeaterItem item in Repeater1.Items)
        {
            TextBox box = (TextBox)item.FindControl("TextBox1");
            TextBox box1 = (TextBox)item.FindControl("TextBoxDN");
            CompetenceMasterBM obj = new CompetenceMasterBM();
            obj.comchildcomID = id;
            obj.comchildlevel = box.Text;
            obj.comchildlevelDN = box1.Text;
            obj.comIsActive = true;
            obj.comIsDeleted = false;
            obj.comCreatedDate = DateTime.Now;
            obj.InsertCompetencechild();
        }
    }
    protected void updateCompetence()
    {
        CompetenceMasterBM obj = new CompetenceMasterBM();
        obj.comId = Convert.ToInt32(Request.QueryString["id"]);
        obj.comCompetence = txtCompetence.Text;
        obj.comCompetenceDN = txtCompetenceDN.Text;
        obj.comcategoryId = Convert.ToInt32(ddlcategory.SelectedValue);
        obj.comUpdatedDate = DateTime.Now;
        obj.UpdateCompetenceAdd();
        DeleteAnswearsbyId(Convert.ToInt32(Request.QueryString["id"]));
        insertCompetencechild(Convert.ToInt32(Request.QueryString["id"]));
        if (obj.ReturnBoolean == true)
        {
            Response.Redirect("AddCompetence.aspx?msg=upd");
        }
    }
    protected void InsertCompetencewithquestion()
    {
        //CompetenceBM obj = new CompetenceBM();
        //obj.comCompetence = txtCompetence.Text;
        //obj.comLevel = Convert.ToInt32(txtlevel.Text);
        //obj.comJobId = Convert.ToInt32(ddljobtype.SelectedValue);
        //obj.comCompanyId = Convert.ToInt32(Session["OrgUserId"]);
        //obj.comIsActive = true;
        //obj.comIsDeleted = false;
        //obj.comCreatedDate = DateTime.Now;
        //obj.comQuestion = txt_Question.Text;
        //obj.comQuestionNo = Convert.ToInt32(ddlNumber.SelectedValue);
        //obj.comDepartmentId = GETDepartment(TreeView1.Nodes);
        //obj.comIndId = GETDivision(TreeView2.Nodes);
        //queid = obj.InsertCompetence();
        //ViewState["comid"] = queid;
        //// save_Que();
        //if (queid > 0)
        //{
        //    Response.Redirect("OrgCompetence.aspx?Add=" + ViewState["comid"]);
        //}

    }
    protected void updateCompetencewithquestion()
    {
        //CompetenceBM obj = new CompetenceBM();
        //obj.comId = Convert.ToInt32(Request.QueryString["id"]);
        //obj.comCompetence = txtCompetence.Text;
        //obj.comLevel = Convert.ToInt32(txtlevel.Text);
        //obj.comJobId = Convert.ToInt32(ddljobtype.SelectedValue);
        //obj.comUpdatedDate = DateTime.Now;
        //obj.comQuestion = txt_Question.Text;
        //obj.comQuestionNo = Convert.ToInt32(0);
        //obj.comDepartmentId = GETDepartment(TreeView1.Nodes);
        //obj.comIndId = GETDivision(TreeView2.Nodes);
        //obj.UpdateCompetence();
        ////DeleteAnswearsbyId(Convert.ToInt32(Request.QueryString["id"]));

        ////save_Que_and_ans();
        //if (obj.ReturnBoolean == true)
        //{
        //    Response.Redirect("OrgCompetence.aspx?Add=" + Request.QueryString["id"]);
        //}
    }

    protected void DeleteAnswearsbyId(int comId)
    {
        CompetenceMasterBM obj = new CompetenceMasterBM();
        obj.comId = comId;
        obj.DeleteCompetencechildbyId();
    }
    protected void GetAllCompetenceAddbyid()
    {
        CompetenceMasterBM obj = new CompetenceMasterBM();
        obj.comId = Convert.ToInt32(Request.QueryString["id"]);
        obj.GetAllCompetenceAddbyid();
        ds = obj.ds;

        if (ds != null)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["comCompetence"])))
                txtCompetence.Text = Convert.ToString(ds.Tables[0].Rows[0]["comCompetence"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["comCompetenceDN"])))
                txtCompetenceDN.Text = Convert.ToString(ds.Tables[0].Rows[0]["comCompetenceDN"]);


            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["comcategoryId"])))
                ddlcategory.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["comcategoryId"]);

            Repeater1.DataSource = ds.Tables[1];
            Repeater1.DataBind();

        }
       




    }
    //protected void GetAllCompetencebyid()
    //{
    //    ViewState["DepartmentId"] = "0";
    //    ViewState["comIndId"] = "0";
    //    CompetenceBM obj = new CompetenceBM();
    //    obj.comId = Convert.ToInt32(Request.QueryString["id"]);
    //    obj.GetAllCompetencebyid();
    //    ds = obj.ds;

    //    if (ds != null)
    //    {
    //        if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["comCompetence"])))
    //            txtCompetence.Text = Convert.ToString(ds.Tables[0].Rows[0]["comCompetence"]);

    //        if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["comLevel"])))
    //            //txtlevel.Text = Convert.ToString(ds.Tables[0].Rows[0]["comLevel"]);

    //        if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["comJobId"])))
    //            //ddljobtype.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["comJobId"]);

    //        if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["comDepartmentId"])))
    //            ViewState["DepartmentId"] = Convert.ToString(ds.Tables[0].Rows[0]["comDepartmentId"]);

    //        if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["comIndId"])))
    //            ViewState["comIndId"] = Convert.ToString(ds.Tables[0].Rows[0]["comIndId"]);



    //    }

    //}

    //protected void GetAllQuestionbyid()
    //{
    //    ViewState["DepartmentId"] = "0";
    //    ViewState["comIndId"] = "0";
    //    QuestionBM obj = new QuestionBM();
    //    obj.queId = Convert.ToInt32(Request.QueryString["cid"]);
    //    obj.GetAllQuestionbyid();
    //    ds = obj.ds;

    //    if (ds != null)
    //    {
    //        //    if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["queQuestion"])))
    //        //        txtCompetence.Text = Convert.ToString(ds.Tables[0].Rows[0]["queQuestion"]);

    //        //    if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["comLevel"])))
    //        //        txtlevel.Text = Convert.ToString(ds.Tables[0].Rows[0]["comLevel"]);

    //        //    if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["comJobId"])))
    //        //        ddljobtype.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["comJobId"]);

    //        //if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["comDepartmentId"])))
    //        //    ViewState["DepartmentId"] = Convert.ToString(ds.Tables[0].Rows[0]["comDepartmentId"]);

    //        //if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["comIndId"])))
    //        //    ViewState["comIndId"] = Convert.ToString(ds.Tables[0].Rows[0]["comIndId"]);

    //    }

    //}

    //protected void set_data()
    //{
    //    if (ds != null)
    //    {
    //        ViewState["queComId"] = (ds.Tables[0].Rows[0]["queComId"]).ToString();
    //       // txt_Question.Text = (ds.Tables[0].Rows[0]["queQuestion"]).ToString();
    //        ddlNumber.SelectedValue = (ds.Tables[0].Rows[0]["queQuestionNo"]).ToString();

    //        if (ds.Tables[1].Rows.Count > 1)
    //        {
    //            Repeater1.DataSource = ds.Tables[1];
    //            dt = ds.Tables[1];
    //            Repeater1.DataBind();
    //        }

    //        int index = 0;
    //        foreach (RepeaterItem item in Repeater1.Items)
    //        {
    //            TextBox TB = (TextBox)item.FindControl("TextBox1");
    //            if (Convert.ToBoolean(ds.Tables[1].Rows[index]["answerSelect"]) == true)
    //            {
    //                ImageButton IB = (ImageButton)item.FindControl("Button1");
    //                IB.ImageUrl = "../Organisation/img/true_check.png";
    //            }
    //            else
    //            {
    //                ImageButton IB = (ImageButton)item.FindControl("Button1");
    //                IB.ImageUrl = "../Organisation/img/false_check.png";
    //            }
    //            index++;
    //        }


    //    }
    //}
    //protected void save_Que(Boolean add)
    //{
    //    QuestionBM obj = new QuestionBM();
    //    //obj.queComId = Convert.ToInt32(ViewState["comid"]);
    //    obj.queComId = Convert.ToInt32(Request.QueryString["Add"]);
    //    obj.queCompanyId = Convert.ToInt32(Session["OrgUserId"]);
    //    obj.queIsActive = true;
    //    obj.queIsDeleted = false;
    //    obj.queCreatedDate = DateTime.Now;
    //    //obj.queQuestionNo = Convert.ToInt32(ddlNumber.SelectedValue);
    //    //obj.queQuestion = txt_Question.Text;
    //    queid = obj.InsertQuestion();
    //    ViewState["queid"] = queid;
    //    save_Que_and_ans();
    //    if (queid > 0)
    //    {
    //        if (add == true)
    //        {
    //            ViewState["comid"] = Request.QueryString["Add"];
    //            Response.Redirect("OrgCompetence.aspx?Add=" + ViewState["comid"]);
    //        }
    //        else
    //        {
    //            Response.Redirect("OrgCompetenceList.aspx?msg=ins");
    //        }

    //    }
    //}
    //protected void update_Que()
    //{
    //    QuestionBM obj = new QuestionBM();
    //    //obj.queComId = Convert.ToInt32(ViewState["comid"]);
    //    obj.queId = Convert.ToInt32(Request.QueryString["cid"]);
    //    obj.queUpdatedDate = DateTime.Now;
    //    //obj.queQuestionNo = Convert.ToInt32(ddlNumber.SelectedValue);
    //    //obj.queQuestion = txt_Question.Text;
    //    obj.UpdateQuestion();
    //    DeleteAnswearsbyId(Convert.ToInt32(Request.QueryString["cid"]));
    //    ViewState["queid"] = Request.QueryString["cid"];
    //    save_Que_and_ans();
    //    Response.Redirect("OrgQuestionList.aspx?id=" + ViewState["queComId"]);
    //}
    //protected void save_Que_and_ans()
    //{
    //    Boolean done = false;
    //    if (string.IsNullOrEmpty(Request.QueryString["id"]))
    //    {
    //        if (ViewState["queid"] != null)
    //            queid = Convert.ToInt32(ViewState["queid"]);
    //        else
    //            queid = 0;
    //    }
    //    else
    //    {
    //        queid = Convert.ToInt32(Request.QueryString["id"]);
    //    }

    //    foreach (RepeaterItem item in Repeater1.Items)
    //    {
    //        TextBox box = (TextBox)item.FindControl("TextBox1");
    //        ImageButton btn = (ImageButton)item.FindControl("Button1");
            
    //        AnsBM obj = new AnsBM();
    //        obj.aNo = queid;
    //        obj.ans = box.Text;
    //        if (default_index == Convert.ToInt32(btn.CommandName))
    //        {
    //            obj.answerSelect = true;
    //        }
    //        else
    //        {
    //            obj.answerSelect = false;
    //        }
    //        done = obj.insert_ans();
            
    //    }
    //}
    //protected void loadfirst()
    //{
    //    if (!string.IsNullOrEmpty(Request.QueryString["id"]))
    //    {
    //        GetAllCompetencebyid();
    //        if (ds.Tables[1].Rows.Count > Convert.ToInt32(ddlNumber.SelectedValue))
    //        {
    //            int ans = ds.Tables[1].Rows.Count - Convert.ToInt32(ddlNumber.SelectedValue);
    //            for (int i = 0; i < ans; i++)
    //            {
    //                ds.Tables[1].Rows.RemoveAt(ds.Tables[1].Rows.Count - 1);
    //            }

    //        }
    //        else
    //        {
    //            int ans = Convert.ToInt32(ddlNumber.SelectedValue) - ds.Tables[1].Rows.Count;
    //            for (int i = 0; i < ans; i++)
    //            {
    //                DataRow dr = ds.Tables[1].NewRow();
    //                dr["answer"] = " ";
    //                ds.Tables[1].Rows.Add(dr);
    //            }
    //        }
    //        Repeater1.DataSource = ds.Tables[1];
    //        Repeater1.DataBind();
    //    }
    //    else
    //    {
    //        if (Repeater1.Controls.Count <= 0)
    //        {
    //            int val = Convert.ToInt32(ddlNumber.SelectedValue);
    //            dt.Columns.Add("answer", typeof(String));

    //            for (int i = 0; i < val; i++)
    //            {
    //                DataRow dr = dt.NewRow();
    //                dr["answer"] = "";
    //                dt.Rows.Add(dr);
    //            }

    //            Repeater1.DataSource = dt;
    //            Repeater1.DataBind();
    //        }
    //        else
    //        {
    //            if ((Repeater1.Controls.Count) < Convert.ToInt32(ddlNumber.SelectedValue))
    //            {
    //                dt.Columns.Add("answer", typeof(String));
    //                for (int i = 0; i < Repeater1.Controls.Count; i++)
    //                {
    //                    TextBox box = (TextBox)Repeater1.Controls[i].FindControl("TextBox1");
    //                    RadioButton rbMyRadio = (RadioButton)Repeater1.Controls[i].FindControl("RadioButton1");

    //                    DataRow dr = dt.NewRow();
    //                    dr["answer"] = box.Text;
    //                    dt.Rows.Add(dr);
    //                }
    //                int add_new = Convert.ToInt32(ddlNumber.SelectedValue) - (Repeater1.Controls.Count);
    //                for (int j = 0; j < add_new; j++)
    //                {
    //                    DataRow dr = dt.NewRow();
    //                    dr["answer"] = "";
    //                    dt.Rows.Add(dr);
    //                }
    //            }
    //            else
    //            {
    //                dt.Columns.Add("answer", typeof(String));
    //                for (int i = 0; i < Repeater1.Controls.Count; i++)
    //                {
    //                    TextBox box = (TextBox)Repeater1.Controls[i].FindControl("TextBox1");
    //                    RadioButton rbMyRadio = (RadioButton)Repeater1.Controls[i].FindControl("RadioButton1");

    //                    DataRow dr = dt.NewRow();
    //                    dr["answer"] = box.Text;
    //                    dt.Rows.Add(dr);
    //                }
    //                int remove_new = (Repeater1.Controls.Count) - Convert.ToInt32(ddlNumber.SelectedValue);
    //                for (int j = 0; j < remove_new; j++)
    //                {
    //                    dt.Rows.RemoveAt(dt.Rows.Count - 1);
    //                }
    //            }
    //            Repeater1.DataSource = dt;
    //            Repeater1.DataBind();
    //        }
    //    }
    //}

    protected void GetAllcompetenceCategory()
    {
        CompetenceMasterBM obj = new CompetenceMasterBM();
        obj.catIsActive = true;
        obj.catIsDeleted = false;
        obj.catCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllcompetenceCategory();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (Convert.ToString(Session["Culture"]) == "Danish")
                {
                    ds.Tables[0].Columns["catName"].ColumnName = "abcd";
                    ds.Tables[0].Columns["catNameDN"].ColumnName = "catName";

                }

                ddlcategory.DataSource = ds.Tables[0];
                ddlcategory.DataTextField = "catName";
                ddlcategory.DataValueField = "catId";
                ddlcategory.DataBind();
            }
            else
            {
                ddlcategory.Items.Clear();
                ddlcategory.Items.Insert(0, new ListItem(CommonModule.dropDownCategory, CommonModule.dropDownZeroValue));
            }

        }
        else
        {
            ddlcategory.Items.Clear();
            ddlcategory.Items.Insert(0, new ListItem(CommonModule.dropDownCategory, CommonModule.dropDownZeroValue));
        }

    }
    #endregion

    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Request.QueryString["id"]))
        {
            updateCompetence();
        }
        else
        {
            InsertCompetence();

        }

    }
    //protected void btnAddque_click(object sender, EventArgs e)
    //{

    //    if (!String.IsNullOrEmpty(Request.QueryString["id"]))
    //    {
    //        updateCompetencewithquestion();
    //    }
    //    else
    //    {
    //        InsertCompetencewithquestion();
    //    }

    //}
    //protected void btnAdd_click(object sender, EventArgs e)
    //{
    //    if (!String.IsNullOrEmpty(Request.QueryString["cid"]))
    //    {
    //        update_Que();
    //    }
    //    else
    //    {
    //        save_Que(false);
    //    }

    //}
    //protected void Addmore_click(object sender, EventArgs e)
    //{
    //    //if (!String.IsNullOrEmpty(Request.QueryString["id"]))
    //    //{
    //    //    updateCompetencewithquestion();
    //    //}
    //    //else
    //    //{
    //    save_Que(true);
    //    //}

    //}
    protected void btnCancel_click(object sender, EventArgs e)
    {
        Response.Redirect("AddCompetence.aspx");
    }
    //protected void txt_number_TextChanged(object sender, EventArgs e)
    //{
    //    if (!string.IsNullOrEmpty(Request.QueryString["id"]))
    //    {
    //        GetAllCompetencebyid();
    //        if (ds.Tables[1].Rows.Count > Convert.ToInt32(txt_Number.Text))
    //        {
    //            int ans = ds.Tables[1].Rows.Count - Convert.ToInt32(txt_Number.Text);
    //            for (int i = 0; i < ans; i++)
    //            {
    //                ds.Tables[1].Rows.RemoveAt(ds.Tables[1].Rows.Count - 1);
    //            }

    //        }
    //        else
    //        {
    //            int ans = Convert.ToInt32(txt_Number.Text) - ds.Tables[1].Rows.Count;
    //            for (int i = 0; i < ans; i++)
    //            {
    //                DataRow dr = ds.Tables[1].NewRow();
    //                dr["answer"] = " ";
    //                ds.Tables[1].Rows.Add(dr);
    //            }
    //        }
    //        Repeater1.DataSource = ds.Tables[1];
    //        Repeater1.DataBind();
    //    }
    //    else
    //    {
    //        if (Repeater1.Controls.Count <= 0)
    //        {
    //            int val = Convert.ToInt32(txt_Number.Text);
    //            dt.Columns.Add("answer", typeof(String));

    //            for (int i = 0; i < val; i++)
    //            {
    //                DataRow dr = dt.NewRow();
    //                dr["answer"] = "";
    //                dt.Rows.Add(dr);
    //            }

    //            Repeater1.DataSource = dt;
    //            Repeater1.DataBind();
    //        }
    //        else
    //        {
    //            if ((Repeater1.Controls.Count) < Convert.ToInt32(txt_Number.Text))
    //            {
    //                dt.Columns.Add("answer", typeof(String));
    //                for (int i = 0; i < Repeater1.Controls.Count; i++)
    //                {
    //                    TextBox box = (TextBox)Repeater1.Controls[i].FindControl("TextBox1");
    //                    RadioButton rbMyRadio = (RadioButton)Repeater1.Controls[i].FindControl("RadioButton1");

    //                    DataRow dr = dt.NewRow();
    //                    dr["answer"] = box.Text;
    //                    dt.Rows.Add(dr);
    //                }
    //                int add_new = Convert.ToInt32(txt_Number.Text) - (Repeater1.Controls.Count);
    //                for (int j = 0; j < add_new; j++)
    //                {
    //                    DataRow dr = dt.NewRow();
    //                    dr["answer"] = "";
    //                    dt.Rows.Add(dr);
    //                }
    //            }
    //            else
    //            {
    //                dt.Columns.Add("answer", typeof(String));
    //                for (int i = 0; i < Repeater1.Controls.Count; i++)
    //                {
    //                    TextBox box = (TextBox)Repeater1.Controls[i].FindControl("TextBox1");
    //                    RadioButton rbMyRadio = (RadioButton)Repeater1.Controls[i].FindControl("RadioButton1");

    //                    DataRow dr = dt.NewRow();
    //                    dr["answer"] = box.Text;
    //                    dt.Rows.Add(dr);
    //                }
    //                int remove_new = (Repeater1.Controls.Count) - Convert.ToInt32(txt_Number.Text);
    //                for (int j = 0; j < remove_new; j++)
    //                {
    //                    dt.Rows.RemoveAt(dt.Rows.Count - 1);
    //                }
    //            }
    //            Repeater1.DataSource = dt;
    //            Repeater1.DataBind();
    //        }
    //    }
    //}
    //protected void ddlNumber_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    if (!string.IsNullOrEmpty(Request.QueryString["id"]))
    //    {
    //        GetAllCompetencebyid();
    //        if (ds.Tables[1].Rows.Count > Convert.ToInt32(ddlNumber.SelectedValue))
    //        {
    //            int ans = ds.Tables[1].Rows.Count - Convert.ToInt32(ddlNumber.SelectedValue);
    //            for (int i = 0; i < ans; i++)
    //            {
    //                ds.Tables[1].Rows.RemoveAt(ds.Tables[1].Rows.Count - 1);
    //            }

    //        }
    //        else
    //        {
    //            int ans = Convert.ToInt32(ddlNumber.SelectedValue) - ds.Tables[1].Rows.Count;
    //            for (int i = 0; i < ans; i++)
    //            {
    //                DataRow dr = ds.Tables[1].NewRow();
    //                dr["answer"] = " ";
    //                ds.Tables[1].Rows.Add(dr);
    //            }
    //        }
    //        Repeater1.DataSource = ds.Tables[1];
    //        Repeater1.DataBind();
    //    }
    //    else
    //    {
    //        if (Repeater1.Controls.Count <= 0)
    //        {
    //            int val = Convert.ToInt32(ddlNumber.SelectedValue);
    //            dt.Columns.Add("answer", typeof(String));

    //            for (int i = 0; i < val; i++)
    //            {
    //                DataRow dr = dt.NewRow();
    //                dr["answer"] = "";
    //                dt.Rows.Add(dr);
    //            }

    //            Repeater1.DataSource = dt;
    //            Repeater1.DataBind();
    //        }
    //        else
    //        {
    //            if ((Repeater1.Controls.Count) < Convert.ToInt32(ddlNumber.SelectedValue))
    //            {
    //                dt.Columns.Add("answer", typeof(String));
    //                for (int i = 0; i < Repeater1.Controls.Count; i++)
    //                {
    //                    TextBox box = (TextBox)Repeater1.Controls[i].FindControl("TextBox1");
    //                    RadioButton rbMyRadio = (RadioButton)Repeater1.Controls[i].FindControl("RadioButton1");

    //                    DataRow dr = dt.NewRow();
    //                    dr["answer"] = box.Text;
    //                    dt.Rows.Add(dr);
    //                }
    //                int add_new = Convert.ToInt32(ddlNumber.SelectedValue) - (Repeater1.Controls.Count);
    //                for (int j = 0; j < add_new; j++)
    //                {
    //                    DataRow dr = dt.NewRow();
    //                    dr["answer"] = "";
    //                    dt.Rows.Add(dr);
    //                }
    //            }
    //            else
    //            {
    //                dt.Columns.Add("answer", typeof(String));
    //                for (int i = 0; i < Repeater1.Controls.Count; i++)
    //                {
    //                    TextBox box = (TextBox)Repeater1.Controls[i].FindControl("TextBox1");
    //                    RadioButton rbMyRadio = (RadioButton)Repeater1.Controls[i].FindControl("RadioButton1");

    //                    DataRow dr = dt.NewRow();
    //                    dr["answer"] = box.Text;
    //                    dt.Rows.Add(dr);
    //                }
    //                int remove_new = (Repeater1.Controls.Count) - Convert.ToInt32(ddlNumber.SelectedValue);
    //                for (int j = 0; j < remove_new; j++)
    //                {
    //                    dt.Rows.RemoveAt(dt.Rows.Count - 1);
    //                }
    //            }
    //            Repeater1.DataSource = dt;
    //            Repeater1.DataBind();
    //        }
    //    }
    //}
    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        // string language = "French";
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    //if (language.EndsWith("French")) languageId = "da-DK";
        //    if (language.EndsWith("Danish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion

    

    

   // protected static int default_index = 0;

    //protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    //{
    //    foreach (RepeaterItem item in Repeater1.Items)
    //    {
    //        ImageButton rb = (ImageButton)item.FindControl("Button1");
    //        rb.ImageUrl = "../Organisation/img/false_check.png";
    //        string val = rb.CommandName;
    //    }
    //    ImageButton rb2 = (ImageButton)e.Item.FindControl("Button1");
    //    rb2.ImageUrl = "../Organisation/img/true_check.png";
    //    default_index = Convert.ToInt32(rb2.CommandName);

    //}
}