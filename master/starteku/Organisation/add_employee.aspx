﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true" CodeFile="add_employee.aspx.cs" Inherits="Organisation_add_employee"
    Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/ajaxtab.css" rel="stylesheet" />
    <script src="../assets/assets/js/plugins.js" type="text/javascript"></script>
    <script src="../Scripts/sMain.js?version=1.0" type="text/javascript"></script>
    <%--<link rel="stylesheet" href="/../code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" />
  <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.js"></script>--%>
    <%--  <script type="text/javascript" src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>--%>
    <link href="../Styles/UpdateCustom.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        .treeNode {
        }

            .treeNode input {
                width: auto;
                margin: 5px;
                float: left !important;
            }
    </style>
    <style type="text/css">
        body {
            font-family: Geneva,Arial,Helvetica,sans-serif;
        }

        .table > thead > tr > th {
            vertical-align: middle;
        }
    </style>
    <script>
        $(document).ready(function () {

            setTimeout(function () {
                //ExampanCollapsMenu();

                $(".add_employee").addClass("active_page");
            }, 500);
        });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        .ascroll_checkboxes {
            height: 120px; /* width: 270px;*/
            padding: 5px;
            overflow: auto;
            border: 1px solid #ccc;
        }

        .FormText {
            font-size: 11px;
            font-family: tahoma,sans-serif;
        }
    </style>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {

            "use strict";
            App.init(); // Init layout and core plugins
            Plugins.init(); // Init all plugins
            FormComponents.init(); // Init all form-specific plugins*/
            accordian();
        });

        var color = 'White';


        function changeColor(obj) {
            var rowObject = getParentRow(obj);

            if (color == '') {
                color = getRowColor();
            }
            if (obj.checked) {
                rowObject.style.backgroundColor = '#A3B1D8';
            }
            else {
                rowObject.style.backgroundColor = color;
                color = 'White';
            }
            // private method/**/
            function getRowColor() {
                if (rowObject.style.backgroundColor == 'White') return parentTable.style.backgroundColor;
                else return rowObject.style.backgroundColor;
            }

        }

        // This method returns the parent row of the object

        function getParentRow(obj) {
            do {
                obj = obj.parentElement;
            }
            while (obj.tagName != "TR")
            return obj;
        }



    </script>
    <div class="col-md-6">

          <asp:HiddenField ID="hdnCurrentLanguage" runat="server" Value="en-GB"  />
        <asp:HiddenField ID="hdnSelectDivisions" runat="server" Value="Select Division" meta:resourcekey="SelectDivisionsResource" />
        <asp:Label ID="hdnConfirmArchive" Style="display: none;" CssClass="abcde" meta:resourcekey="ConfirmArchive" runat="server" />
        <asp:HiddenField ID="hdnSelectJob" runat="server" Value="Select Jobtype" meta:resourcekey="SelectJobResource" />
        <asp:HiddenField ID="hdnSelectTeam" runat="server" Value="Select Team" meta:resourcekey="SelectTeamResource" />
        <asp:HiddenField ID="hdnSelectGender" runat="server" Value="Select Gender" meta:resourcekey="SelectGenderResource" />
        <asp:HiddenField ID="hdnSelectLanguage" runat="server" Value="Select Language" meta:resourcekey="SelectLanguageResource" />
        <asp:HiddenField ID="hdnSelectAll" runat="server" Value="Select All" meta:resourcekey="SelectAll" />
        <asp:HiddenField ID="hdnUnSelectAll" runat="server" Value="UnSelect All" meta:resourcekey="UnSelectAll" />
        <div class="heading-sec">
            <h1>
                <%-- <%= CommonMessages.Employee%>--%>
                <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Employee" EnableViewState="false" />
                <i><span>
                    <asp:Literal ID="Literal10" runat="server" meta:resourcekey="Welcome" EnableViewState="false" /></span><span runat="server" id="Employee"></span></i>
            </h1>
        </div>
    </div>
    <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="lll">
        <ContentTemplate>
            <div class="col-md-12" style="margin-top: 20px;">
                <div id="graph-wrapper">
                    <div class="col-md-12">
                        <asp:Button runat="server" ID="Button1" PostBackUrl="Registration.aspx?id=1" Text="Add employee"
                            CssClass="btn btn-primary yellow" Visible="False" meta:resourcekey="Button1Resource1" />

                        <a href="#add-post-title" data-toggle="modal" title="" onclick="javascript:showpopup1();">
                            <button type="button" class="btns  yellow  lrg-btn flat-btn add_user " style="border: 0px;">
                                <%--  <%= CommonMessages.AddNewEmployee%>--%>
                                <asp:Literal ID="Literal2" runat="server" meta:resourcekey="AddNewEmployee" EnableViewState="false" />
                            </button>
                        </a>

                        <a href="TeamTargetView.aspx" title="" class="btns  yellow  lrg-btn flat-btn add_user" style="border: 0px; margin-left: 5px; border-radius: 7%;"><asp:Label runat="server" meta:resourcekey="teamtargetsres"></asp:Label>
                            <%-- <button type="button" class="btns  yellow  lrg-btn flat-btn add_user" style="border: 0px;margin-left:5px;">--%>
                            <asp:Literal ID="Literal22" runat="server" EnableViewState="false" />
                            <%-- </button>--%>
                        </a>

                        <asp:Panel runat="server" ID="pointPanel">
                            <a href="ViewPoint.aspx" title="" class="btns  yellow  lrg-btn flat-btn add_user" style="border: 0px; margin-left: 5px; border-radius: 7%;">
                                <asp:Literal ID="Literal9" runat="server" meta:resourcekey="ViewPoint" EnableViewState="false" />
                            </a>
                        </asp:Panel>
                        <asp:Panel runat="server" ID="btnActOvp">
                            <a href="ActOvP.aspx" title="" class="btns  yellow  lrg-btn flat-btn add_user" style="border: 0px; margin-left: 5px; border-radius: 7%;">
                                <%-- <button type="button" class="btns  yellow  lrg-btn flat-btn add_user" style="border: 0px;margin-left:5px;">--%>
                                <asp:Literal ID="Literal12" runat="server" meta:resourcekey="Activities" EnableViewState="false" />
                                <%-- </button>--%>
                            </a>
                        </asp:Panel>
                        <br />
                        <br />
                        <br />
                        <div class="chart-tab manager_table">
                            <div id="tabs-container">
                                <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" meta:resourcekey="lblMsgResource1"></asp:Label>
                                <%-- <cc1:TabContainer ID="TabContainer1" runat="server" CssClass="Tab">
                                    <cc1:TabPanel ID="tbpnluser" runat="server">
                                    <HeaderTemplate>
                                        Employee<a></a>
                                    </HeaderTemplate>--%>
                                <%--  <div class="invoice" style="background-color: white; margin-top: 0px;">--%>
                                <span runat="server" id="login" />
                                <asp:GridView ID="gvGrid" runat="server" AutoGenerateColumns="False" CellPadding="0" AllowSorting="true"
                                    Width="100%" GridLines="None" DataKeyNames="userId" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                                    EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>'
                                    OnRowCommand="gvGrid_RowCommand"
                                    AllowPaging="True" PageSize="15"
                                    OnPageIndexChanging="gvGrid_PageIndexChanging" OnSorting="gvGrid_Sorting"
                                    BackColor="White" meta:resourcekey="gvGridResource1">
                                    <HeaderStyle CssClass="aa" />
                                    <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                    <AlternatingRowStyle BackColor="White" />

                                    <Columns>
                                        <asp:TemplateField HeaderText="ID" meta:resourcekey="TemplateFieldResource1">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="aa" Style="max-width: 15px"> <%# Container.DataItemIndex + 1 %></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                Width="3%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <a href="PersonalDevelopmentPlan.aspx?umId=<%# Eval("userId") %>" id="A2">
                                                    <input type="button" value="PDP" class="btn-danger"
                                                        style="padding: 0px 0px !important;" />
                                                    </button>
                                                </a>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                Width="3%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="EMPLOYEE NAME" meta:resourcekey="TemplateFieldResource2" SortExpression="name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUNamer" runat="server" Text="<%# bind('name') %>" meta:resourcekey="lblUNamerResource1"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="200px" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow" Font-Underline="true"
                                                Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="JOB TYPE" meta:resourcekey="TemplateFieldResource3" SortExpression="jobName">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUserNamer" runat="server" Text="<%# bind('jobName') %>" meta:resourcekey="lblUserNamerResource1"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="200px" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow" Font-Underline="true"
                                                Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="DIVISION" meta:resourcekey="TemplateFieldResource4" SortExpression="divName">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUserNamer1" runat="server" Text="<%# bind('divName') %>" meta:resourcekey="lblUserNamer1Resource1"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="200px" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow" Font-Underline="true"
                                                Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Team" SortExpression="teamName" meta:resourcekey="teamnameres">
                                            <ItemTemplate>
                                                <%if(Convert.ToString(Session["Culture"]) == "Danish"){%>
                                                <asp:Label ID="lblUserNamer1111" runat="server" Text='<%#Eval("TeamNameDN") %>'></asp:Label>
                                                <%} %>
                                                <%else{ %>
                                                <asp:Label ID="Label22222" runat="server" Text='<%# Eval("TeamName") %>'></asp:Label>
                                                <%} %>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="200px" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow" Font-Underline="true"
                                                Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ALP" SortExpression="pointALP">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUserNamer122" runat="server" Text="<%# bind('Apl') %>"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="200px" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow" Font-Underline="true"
                                                Width="7%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="PLP" SortExpression="pointPLP">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUserNamer123" runat="server" Text="<%# bind('Plp') %>"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="200px" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow" Font-Underline="true"
                                                Width="7%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="PDP" SortExpression="pointPDP">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUserNamer124" runat="server" Text="<%# bind('pdp') %>"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="200px" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow" Font-Underline="true"
                                                Width="7%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Competence" meta:resourcekey="comres">
                                            <ItemTemplate>
                                                <div class="vat" style="width: 100px;">
                                                    <p>
                                                        <a href="<%# String.Format("View_requestComp.aspx?rid={0}", Eval("userId")) %>" style="font-size: 13px"><asp:Label runat="server" meta:resourcekey="INDIVIDUALTEAMTARGET"></asp:Label>
                                                        </a>
                                                    </p>
                                                </div>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="200px" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                        </asp:TemplateField>


                                        <%--Show Password----------------------------------%>
                                        <asp:TemplateField HeaderText="Password" Visible="False">
                                            <ItemTemplate>
                                                <div class="vat" style="width: 50px;">
                                                    <p>
                                                        <asp:LinkButton ID="LinkButton12" runat="server" CommandName="Password" CommandArgument='<%# Eval("userId") %>'
                                                            ToolTip="View">View</asp:LinkButton>
                                                    </p>
                                                </div>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="70px" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                Width="3%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="" meta:resourcekey="TemplateFieldResource5">
                                            <ItemTemplate>
                                                <div class="vat" style="width: 30%;">
                                                    <p>
                                                        <i class="fa fa-info-circle"></i>
                                                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="View" CommandArgument='<%# Eval("userId") %>'
                                                            ToolTip="View" meta:resourcekey="LinkButton1Resource1">Details</asp:LinkButton>
                                                    </p>
                                                </div>
                                                <div class="vat" style="width: 32%;">
                                                    <p>
                                                        <i class="fa fa-pencil"></i><a href="<%# String.Format("Eemployee.aspx?id={0}", Eval("userId")) %>"
                                                            title="Edit">
                                                            <asp:Label runat="server" ID="lblEdit" meta:resourcekey="EditResource"></asp:Label></a>
                                                    </p>
                                                </div>
                                                <div class="total" style="width: 32%;">
                                                    <p>
                                                        <i class="fa fa-trash-o"></i>

                                                        <asp:LinkButton ID="lnkBtnName" CssClass="def" runat="server" CommandName="archive" OnClientClick="return xyz();" CommandArgument='<%# Eval("userId") %>'
                                                            ToolTip="Delete" meta:resourcekey="lnkBtnNameResource1">Delete</asp:LinkButton>
                                                    </p>
                                                </div>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="70px" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                Width="24%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                </span>
                            </div>
                            <%-- </cc1:TabPanel>--%>
                            <%-- <cc1:TabPanel ID="tbpnlusrdetails" runat="server">
                                <HeaderTemplate>
                                    Archive<a></a>
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <div class="invoice" style="background-color: white; margin-top: 0px;">
                                        <asp:GridView ID="gvArchive" runat="server" AutoGenerateColumns="False" CellPadding="0" Visible="false"
                                            CellSpacing="0" Width="100%" GridLines="none" EmptyDataText='<%#CommonModule.msgGridRecordNotfound %>'
                                            DataKeyNames="userId" OnRowCommand="gvArchive_RowCommand" CssClass="table table-striped table-bordered table-hover table-checkable datatable">
                                            <HeaderStyle CssClass="aa" />
                                            <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                            <AlternatingRowStyle BackColor="White" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Sr.No">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                        Width="10%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Employee Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUNamer" runat="server" Text="<%# bind('name') %>"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                        Width="10%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Job Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUserNamer" runat="server" Text="<%# bind('jobName') %>"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                        Width="10%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Department">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUserNamer1" runat="server" Text="<%# bind('depName') %>"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                        Width="10%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Action" ItemStyle-Width="30px">
                                                    <ItemTemplate>
                                                        <span class="btn-group">
                                                            <asp:LinkButton ID="lnkbtnrestore" runat="server" CommandName="archive" CommandArgument='<%# Eval("userId") %>'
                                                                ToolTip="Restore" OnClientClick="return confirm('Are you sure you want to restore this record?');"
                                                                Text="Restore"></asp:LinkButton>&nbsp;|&nbsp;
                                                            <asp:LinkButton ID="lnkBtnPermanentlydelete" runat="server" CommandName="permanentlydelete"
                                                                CommandArgument='<%# Eval("userId") %>' Text="Delete Permanently" ToolTip="Delete Permanently"
                                                                OnClientClick="return confirm('Are you sure you want to permanently delete this record?');"></asp:LinkButton>
                                                        </span>
                                                    </ItemTemplate>
                                                     <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow" Width="10%" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </ContentTemplate>
                            </cc1:TabPanel>--%>
                            <%--  </cc1:TabContainer>--%>
                            <%--</div>--%>
                        </div>
                    </div>
                </div>
            </div>
            <asp:LinkButton ID="lnkFake" runat="server" meta:resourcekey="lnkFakeResource1" />
            <cc1:ModalPopupExtender ID="mpe" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkFake"
                CancelControlID="btnClose" BackgroundCssClass="modalBackground" DynamicServicePath=""
                Enabled="True">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup modal-dialog" meta:resourcekey="pnlPopupResource1">
                <div class="modal-header blue yellow-radius" style="color: #ffffff;">
                    <h4 class="modal-title">
                        <asp:Literal ID="Literal21" runat="server" meta:resourcekey="Details" EnableViewState="false" />
                    </h4>
                    <div style="float: right; margin-top: -20px;">
                        <asp:Button ID="btnClose" runat="server" Text="×" CssClass="close" meta:resourcekey="btnCloseResource1" />
                    </div>
                </div>
                <div class="modal-body">
                    <table border="0" cellpadding="3" cellspacing="0">
                        <tr>
                            <td style="width: 135px">
                                <b>
                                    <%-- <%= CommonMessages.Name%>--%>
                                    <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Name" EnableViewState="false" />:
                                </b>
                            </td>
                            <td>
                                <asp:Label ID="lblName" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 135px">
                                <b>
                                    <%-- <%= CommonMessages.Name%>--%>
                                    <asp:Literal ID="Literal11" runat="server" meta:resourcekey="LastName" EnableViewState="false" />:
                                </b>
                            </td>
                            <td>
                                <asp:Label ID="lblLastName" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    <%-- <%= CommonMessages.Address%>--%>
                                    <asp:Literal ID="Literal4" runat="server" meta:resourcekey="Address" EnableViewState="false" />:
                                </b>
                            </td>
                            <td>
                                <asp:Label ID="lblAddress" runat="server" meta:resourcekey="lblAddressResource1" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    <asp:Literal ID="Literal13" runat="server" meta:resourcekey="Email" EnableViewState="false" />: </b>
                            </td>
                            <td>
                                <asp:Label ID="lblEmail" runat="server" meta:resourcekey="lblEmailResource1" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    <%--<%= CommonMessages.PhoneNo%>--%>
                                    <asp:Literal ID="Literal5" runat="server" meta:resourcekey="PhoneNo" EnableViewState="false" />:
                                </b>
                            </td>
                            <td>
                                <asp:Label ID="lblPhoneNo" runat="server" meta:resourcekey="lblPhoneNoResource1" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    <asp:Literal ID="Literal14" runat="server" meta:resourcekey="Division" EnableViewState="false" />: </b>
                            </td>
                            <td>
                                <asp:Label ID="lblDepartments" runat="server" meta:resourcekey="lblDepartmentsResource1" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    <asp:Literal ID="Literal15" runat="server" meta:resourcekey="Job_Type" EnableViewState="false" />: </b>
                            </td>
                            <td>
                                <asp:Label ID="lblJob" runat="server" meta:resourcekey="lblJobResource1" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <asp:LinkButton ID="LinkButton2" runat="server" />
            <cc1:ModalPopupExtender ID="Modal1" runat="server" PopupControlID="Panel1" TargetControlID="LinkButton2"
                CancelControlID="Button3" BackgroundCssClass="modalBackground" DynamicServicePath=""
                Enabled="True">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="Panel1" runat="server" CssClass="modalPopup modal-dialog">
                <div class="modal-header blue yellow-radius" style="color: #ffffff;">
                    <h4 class="modal-title">
                        <asp:Literal ID="Literal16" runat="server" meta:resourcekey="Password" EnableViewState="false" /></h4>
                    <div style="float: right; margin-top: -20px;">
                        <asp:Button ID="Button3" runat="server" Text="×" CssClass="close" />
                    </div>
                </div>
                <div class="modal-body">
                    <table border="0" cellpadding="3" cellspacing="0">
                        <tr>
                            <td style="width: 135px">
                                <b>
                                    <asp:Literal ID="Literal17" runat="server" meta:resourcekey="Password" EnableViewState="false" />: </b>
                            </td>
                            <td>
                                <asp:Label ID="lblpassword" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>

            </asp:Panel>
        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>
    <asp:Button runat="server" ID="btnBack" Text="Back" CssClass="btn btn-default black pull-right"
        OnClick="btnBack_Click" Style="margin-top: 10px; margin-right: 27px;" meta:resourcekey="BackButtonResource" />
    <%--<a href="#"  onclick="javascript:history.back(); return false;"  class="btn black pull-right" style=" margin-right: 26px;border-radius: 5px;margin-left:0px;margin-top:5px;float:right;">Back</a>--%>
    <%----------------------------Add new employee popup-----------------------------------------------------------------------------%>
    <div aria-hidden="true" role="dialog" tabindex="-1" class="modal fade" id="add-post-title"
        style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header blue yellow-radius">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button" style="display: none;">
                        ×
                    </button>
                    <h4 class="modal-title">
                        <asp:Literal ID="Literal6" runat="server" meta:resourcekey="AddNewEmployee" EnableViewState="false" />
                        <%-- <%= CommonMessages.AddNewEmployee%>--%>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="otherDiv">
                        <asp:TextBox runat="server" ID="txtName" MaxLength="50" meta:resourcekey="txtNameResource1" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName"
                            ErrorMessage="Please enter Name." CssClass="commonerrormsg" Display="Dynamic"
                            ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator5Resource1"></asp:RequiredFieldValidator>
                    </div>
                    <div class="otherDiv">
                        <asp:TextBox runat="server" ID="txtLastName" MaxLength="50" meta:resourcekey="txtNameResource2" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtName"
                            ErrorMessage="Please enter Name." CssClass="commonerrormsg" Display="Dynamic"
                            ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator5Resource1"></asp:RequiredFieldValidator>
                    </div>
                    <div class="otherDiv">
                        <asp:TextBox runat="server" ID="txtaddress" Rows="2" TextMode="MultiLine"
                            meta:resourcekey="txtaddressResource1" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtaddress"
                            ErrorMessage="Please enter Address." CssClass="commonerrormsg" Display="Dynamic"
                            ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator2Resource1"></asp:RequiredFieldValidator>
                    </div>
                    <div class="otherDiv">
                        <asp:UpdatePanel runat="server" ID="UpdatePanelEmail" UpdateMode="Conditional">
                            <ContentTemplate>
                                <%--<textarea placeholder="Address" rows="3"></textarea>--%>
                                <asp:TextBox runat="server" ID="txtEmail" MaxLength="50" meta:resourcekey="txtEmailResource1"
                                    OnTextChanged="txtEmail_TextChanged" AutoPostBack="true" />
                                <asp:Label runat="server" ID="lblemailvalid" CssClass="commonerrormsg"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:RequiredFieldValidator ID="rfvEmail" runat="server" CssClass="commonerrormsg"
                            ControlToValidate="txtEmail" Display="Dynamic" ValidationGroup="chk" ErrorMessage="Please enter email."
                            meta:resourcekey="rfvEmailResource1"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revEmail" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                            ControlToValidate="txtEmail" CssClass="commonerrormsg" Display="Dynamic" ValidationGroup="chk"
                            ErrorMessage="Please enter valid email." meta:resourcekey="revEmailResource1"></asp:RegularExpressionValidator>
                    </div>
                    <div class="otherDiv">
                        <asp:TextBox runat="server" ID="txtMobile" MaxLength="50"
                            meta:resourcekey="txtMobileResource1" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtMobile"
                            ErrorMessage="Minimum mobile length is 10." Display="Dynamic" CssClass="commonerrormsg"
                            ValidationGroup="chk" ValidationExpression=".{10}.*" meta:resourcekey="RegularExpressionValidator1Resource1" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtMobile"
                            ErrorMessage="Please enter mobile." CssClass="commonerrormsg" Display="Dynamic"
                            ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator8Resource1"></asp:RequiredFieldValidator>
                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Custom, Numbers"
                            ValidChars=" ,+,(,),-" TargetControlID="txtMobile" Enabled="True">
                        </cc1:FilteredTextBoxExtender>
                        <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtMobile"
                            ErrorMessage="Minimum mobile length is 9." Display="Dynamic" CssClass="commonerrormsg"
                            ValidationGroup="chk" ValidationExpression=".{9}.*" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtMobile"
                            ErrorMessage="Please enter Phone No." CssClass="commonerrormsg" Display="Dynamic"
                            ValidationGroup="chk"></asp:RequiredFieldValidator>
                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers"
                            TargetControlID="txtMobile" Enabled="True">
                        </cc1:FilteredTextBoxExtender>--%>
                    </div>
                    <div class="otherDiv">
                        <asp:DropDownList ID="ddlgender" runat="server" CssClass="form-control" Style="width: 100%; height: 32px; display: inline; margin-bottom: 20px;"
                            meta:resourcekey="ddlgenderResource1">
                            <asp:ListItem Value="Male" Text="Male" meta:resourcekey="ListItemResource1"></asp:ListItem>
                            <asp:ListItem Value="Female" Text="Female" meta:resourcekey="ListItemResource2"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorddlgender" runat="server" ControlToValidate="ddlgender"
                            ErrorMessage="Please select gender." InitialValue="0" CssClass="commonerrormsg"
                            Display="Dynamic" ValidationGroup="chk" meta:resourcekey="selectgenderres"></asp:RequiredFieldValidator>
                    </div>
                    <div class="otherDiv">
                        <asp:DropDownList ID="ddlLanguage" runat="server" Style="width: 100%; height: 32px; display: inline; margin-bottom: 20px;"
                            CssClass="form-control">
                            <%--<asp:ListItem Value="English" Text="English" meta:resourcekey="EnglishResource"></asp:ListItem>
                            <asp:ListItem Value="Danish" Text="Danish" meta:resourcekey="Denishresource"></asp:ListItem>--%>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorddlLanguage" runat="server" ControlToValidate="ddlLanguage"
                            ErrorMessage="Please select Language." InitialValue="0" CssClass="commonerrormsg"
                            Display="Dynamic" ValidationGroup="chk" meta:resourcekey="RequiredFieldValidatorddlLanguageResource1"> </asp:RequiredFieldValidator>

                    </div>

                    <asp:UpdatePanel runat="server" ID="UpdatePanedrop" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="otherDiv">
                                <asp:DropDownList ID="ddlDepartment" runat="server" Style="width: 100%; height: 32px; display: inline; margin-bottom: 20px;"
                                    CssClass="form-control" meta:resourcekey="ddlDepartmentResource1"
                                    OnSelectedIndexChanged="ddlcompetence_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorddlDepartment" runat="server" ControlToValidate="ddlDepartment"
                                    ErrorMessage="Please select Division." InitialValue="0" CssClass="commonerrormsg"
                                    Display="Dynamic" ValidationGroup="chk" meta:resourcekey="RequiredFieldValidatorddlDepartmentResource1"></asp:RequiredFieldValidator>
                            </div>
                            <div class="otherDiv">
                                <asp:DropDownList ID="ddljobtype" runat="server" Style="width: 100%; height: 32px; display: inline; margin-bottom: 20px;"
                                    CssClass="form-control" meta:resourcekey="ddljobtypeResource1"
                                    OnSelectedIndexChanged="ddljobtype_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorddljobtype" runat="server" ControlToValidate="ddljobtype"
                                    ErrorMessage="Please select jobtype." InitialValue="0" CssClass="commonerrormsg"
                                    Display="Dynamic" ValidationGroup="chk" meta:resourcekey="RequiredFieldValidatorddljobtypeResource1"></asp:RequiredFieldValidator>

                            </div>

                            <div class="otherDiv">
                                <asp:DropDownList ID="ddlTeam" runat="server" Style="width: 100%; height: 32px; display: inline; margin-bottom: 20px;"
                                    CssClass="form-control" meta:resourcekey="ddlTeamResource1">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorddlTeam" runat="server" ControlToValidate="ddlTeam"
                                    ErrorMessage="Please select Team." InitialValue="0" CssClass="commonerrormsg"
                                    Display="Dynamic" ValidationGroup="chk" meta:resourcekey="RequiredFieldValidatorddlteamResource1"></asp:RequiredFieldValidator>
                            </div>

                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddljobtype"
                                    ErrorMessage="Please select job type." InitialValue="0" CssClass="commonerrormsg"
                                    Display="Dynamic" ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator1Resource1"></asp:RequiredFieldValidator>--%>
                            </div>
                            <%-------------------------------Checkboxlist--------------------------------------%>
                            <div class="scroll_checkboxes" style="display: none; height: 400px; overflow: auto;"
                                id="CompetenceCheckboxes">
                                <div id="Div2" runat="server">
                                    <asp:HyperLink ID="HyperLink1" runat="server" onclick="fnSelectAll(this);"  meta:resourcekey="lnkSelectAll">
                                        
                                    </asp:HyperLink>
                                    <%-- <a href="#" runat="server" meta:resourcekey="SelectAll" onclick="fnSelectAll();" title="Select All">Select All</a>--%>
                                </div>
                                <label class="c-label" style="width: 100%;">
                                    <asp:Literal ID="Literal20" runat="server" meta:resourcekey="CATEGORY" EnableViewState="false" /></label><br />

                                <div id="accordion">
                                    <div id="Div1">
                                        <div id="NoRecords" runat="server" visible="false">
                                            <asp:Literal ID="Literal18" runat="server" meta:resourcekey="None" EnableViewState="false" />
                                        </div>
                                        <asp:Repeater ID="repCompCat" runat="server" OnItemDataBound="Repeater1_ItemDataBound">
                                            <HeaderTemplate>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <div style="margin-bottom: 2%;">

                                                    <asp:Label runat="server" ID="lblDivIdName" Visible="false"><%# DataBinder.Eval(Container.DataItem, "catName")%> </asp:Label>
                                                     <asp:Label runat="server" ID="lblDivIdName1" Visible="false"><%# DataBinder.Eval(Container.DataItem, "catNameDN")%> </asp:Label>
                                                     <asp:Label runat="server" ID="lblCompCategoryName"> </asp:Label>
                                                </div>
                                                <div>
                                                    <asp:Repeater ID="repCompAdd" runat="server" OnItemDataBound="Repeater2_ItemDataBound">
                                                        <HeaderTemplate>
                                                            <table style="margin-top: -2%;">
                                                                <%-- <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                            </tr>
                                        </thead>--%>
                                                                <tbody>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td>
                                                                    <asp:CheckBoxList Width="250px" ID="chkList" runat="server" CssClass="chkliststyle"
                                                                        RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description"
                                                                        DataValueField="value" Style="width: 100%; overflow: auto;">
                                                                    </asp:CheckBoxList>
                                                                    <asp:Label runat="server" Font-Bold="true" ID="hdnChkboxValue" Text='<%# DataBinder.Eval(Container.DataItem, "comId")%>'
                                                                        Style="display: none"></asp:Label>
                                                                </td>
                                                                <%--<asp:CustomValidator ID="CustomValidator1" ErrorMessage="Please select at least one item."
                                    ForeColor="Red" runat="server"
                                    ValidationGroup="chk" />
                                                                --%>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </tbody> </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </div>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                    <%-- <div id="accordion">
                           <h3><asp:Label runat="server" ID="lblDivIdName"></asp:Label></h3>
                               <div class="contentss">
                                   
                                          <asp:CheckBoxList Width="100%" ID="chkList" runat="server" CssClass="FormText scroll_checkboxes"
                                    RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description"
                                    DataValueField="value" Style="width: 100%; overflow: auto; border: 1px solid #d4d4d4;">
                                </asp:CheckBoxList>
                                <asp:CustomValidator ID="CustomValidator1" ErrorMessage="Please select at least one item."
                                    ForeColor="Red" runat="server"
                                    ValidationGroup="chk" />
                              </div>
                           
                            <h3><asp:Label runat="server" ID="lblJobtype"></asp:Label></h3>
                               
                                  <div  class="contentss">
                                             <asp:CheckBoxList Width="100%" ID="CheckBoxList1" runat="server" CssClass="FormText scroll_checkboxes"
                                        RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description"
                                        DataValueField="value" Style="width: 100%; overflow: auto; border: 1px solid #d4d4d4;">
                                    </asp:CheckBoxList>
                                    <asp:CustomValidator ID="CustomValidator2" ErrorMessage="Please select at least one item."
                                        ForeColor="Red" runat="server"
                                        ValidationGroup="chk" />
                                   </div>
                               
                       
                        </div>--%>
                                    <div>
                                    </div>
                                </div>
                                <%-- <div class="scroll_checkboxes" style="display: none;height:400px; overflow: auto;" id="CompetenceCheckboxes" >
                                Competence:<br />
                                <asp:CheckBoxList Width="100%" ID="chkList" runat="server" CssClass="FormText" RepeatDirection="Vertical"
                                    RepeatColumns="1" BorderWidth="0" Datafield="description" DataValueField="value">
                                </asp:CheckBoxList>
                                <asp:CustomValidator ID="CustomValidator1" ErrorMessage="Please select at least one item."
                                    ForeColor="Red" runat="server"
                                    ValidationGroup="chk" />
                                <br />
                            </div>--%>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <%--NEXT Button--%>
                <div class="modal-footer" id="nextButtons">
                    <%-- <asp:Button runat="server" ID="btnNext" Text="Next" CssClass="btn btn-primary yellow"
                        ValidationGroup="chk" OnClientClick="ShowCompotenceCategory(true);" Style="border-radius: 5px;" />--%>
                    <%-- <input id="" type="button" onclick="CallCompetence();" value="Next" class="btn btn-primary yellow"
                        style="border-radius: 5px;" validationgroup="chk" />--%>

                    <button class="btn btn-primary yellow" type="button" validationgroup="chk" onclick="CallCompetence();">
                        <asp:Literal ID="Literal19" runat="server" meta:resourcekey="Next" EnableViewState="false" />
                    </button>
                    <button data-dismiss="modal" class="btn btn-primary black" type="button" onclick="ShowCompotenceCategory(false)">
                        <asp:Literal ID="Literal7" runat="server" meta:resourcekey="Close" EnableViewState="false" />
                        <%--<%= CommonMessages.Close%>--%>
                    </button>
                </div>
                <%--Save Button--%>
                <div class="modal-footer" id="submitButtons" style="display: none">

                    <asp:Button runat="server" ID="Button4" meta:resourcekey="Back" CssClass="btn btn-primary yellow"
                        OnClientClick="ShowCompotenceCategory(false);return false;" Style="border-radius: 5px;" />
                    <%--  <input id="Button2" type="button" onclick="ShowCompotenceCategory(false);" value="Back"
                        class="btn btn-primary yellow" style="border-radius: 5px;" validationgroup="chk" />--%>



                    <asp:Button runat="server" ID="btnsubmit" Text="Submit" CssClass="btn btn-primary yellow"
                        ValidationGroup="chk" OnClick="btnsubmit_click" Style="border-radius: 5px;" meta:resourcekey="btnsubmitResource1" />
                    <%--<asp:Button runat="server" ID="btnclose1" Text="Close" CssClass="btn btn-default black"
                         OnClick="btnclose_click1" class="btn btn-default black"/>--%>
                    <button data-dismiss="modal" class="btn btn-primary black" type="button" onclick="ShowCompotenceCategory(false)">
                        <%--  <%= CommonMessages.Close%>--%><asp:Literal ID="Literal8" runat="server" meta:resourcekey="Close"
                            EnableViewState="false" />
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>

    </div>
    <style type="text/css">
        body {
            font-family: Arial;
            font-size: 11pt;
        }

        .modalBackground {
            background-color: Black;
            filter: alpha(opacity=40);
            opacity: 0.4;
        }

        .modalPopup1 {
            background-color: #FFFFFF;
            width: 300px;
            border: 3px solid #0DA9D0;
        }

        .modalPopup {
            background: none repeat scroll 0 0 #ffffff;
            display: table;
            float: none;
            margin: 96px auto;
            width: 450px;
            top: 60px !important;
        }

            .modalPopup .header {
                background-color: #2FBDF1;
                height: 30px;
                color: White;
                line-height: 30px;
                text-align: center;
                font-weight: bold;
            }

            .modalPopup .body {
                min-height: 50px;
                line-height: 30px;
                text-align: center;
                padding: 5px;
            }

            .modalPopup .footer {
                padding: 3px;
            }

            .modalPopup .button {
                height: 23px;
                color: White;
                line-height: 23px;
                text-align: center;
                font-weight: bold;
                cursor: pointer;
                background-color: #9F9F9F;
                border: 1px solid #5C5C5C;
            }

            .modalPopup td {
                text-align: left;
            }

        .button {
            background-color: transparent;
        }

        .yellow {
            border-radius: 0px;
        }
    </style>
    <script type="text/javascript" src="https://code.jquery.com/jquery.min.js"></script>
    <script type="text/javascript">
        function fnSelectAll(a) {
            if ($("#accordion :input[type='checkbox']").prop('checked') == true) {
                $(a).text($('#ContentPlaceHolder1_hdnSelectAll').val());
                $("#accordion :input[type='checkbox']").prop('checked', false);
            }
            else {

                $(a).text($('#ContentPlaceHolder1_hdnUnSelectAll').val());
                $("#accordion :input[type='checkbox']").prop('checked', true);
            }

        }
    </script>
    <script type="text/javascript">
        function showpopup1() {

            document.getElementById("ContentPlaceHolder1_txtName").value = "";
            document.getElementById("ContentPlaceHolder1_txtaddress").value = "";
            document.getElementById("ContentPlaceHolder1_txtEmail").value = "";
            document.getElementById("ContentPlaceHolder1_txtMobile").value = "";
            document.getElementById("ContentPlaceHolder1_txtLastName").value = "";
            document.getElementById("ContentPlaceHolder1_ddlDepartment").value = "0";
            document.getElementById("ContentPlaceHolder1_ddljobtype").value = "0";
            document.getElementById("ContentPlaceHolder1_ddlTeam").value = "0";
            document.getElementById("ContentPlaceHolder1_ddlgender").value = "0";
            $(".otherDiv").show();
            $("#CompetenceCheckboxes").hide();
            $("#submitButtons").hide();
            $("#nextButtons").show();
            $("#<%=ddlLanguage.ClientID%>").val($("#ContentPlaceHolder1_hdnCurrentLanguage").val());

            $("#ContentPlaceHolder1_RequiredFieldValidator1").hide();
            $("#ContentPlaceHolder1_RequiredFieldValidator2").hide();
            $("#ContentPlaceHolder1_RequiredFieldValidator3").hide();
            $("#ContentPlaceHolder1_revEmail").hide();
            $("#ContentPlaceHolder1_RequiredFieldValidator4").hide();
            $("#ContentPlaceHolder1_RegularExpressionValidator1").hide();

            $("#ContentPlaceHolder1_RequiredFieldValidatorddlLanguage").hide();
            $("#ContentPlaceHolder1_RequiredFieldValidatorddljobtype").hide();
            $("#ContentPlaceHolder1_RequiredFieldValidatorddlDepartment").hide();
            $("#ContentPlaceHolder1_RequiredFieldValidatorddlTeam").hide();
            $("#ContentPlaceHolder1_RequiredFieldValidatorddlgender").hide();

        }

        function CallCompetence() {

            if ($('#ContentPlaceHolder1_txtName').val() == '' || $('#ContentPlaceHolder1_txtlastName').val() == ''
                || $('#ContentPlaceHolder1_txtaddress').val() == '' || $('#ContentPlaceHolder1_txtEmail').val() == ''
                || $('#ContentPlaceHolder1_txtMobile').val() == '' || $('#ContentPlaceHolder1_ddlgender').val() == '0'
                || $('#ContentPlaceHolder1_ddlLanguage').val() == '0' || $('#ContentPlaceHolder1_jobtype').val() == '0' || $('#ContentPlaceHolder1_ddlDepartment').val() == '0'
                || $('#ContentPlaceHolder1_ddlTeam').val() == '0') {
                // $('#RequiredFieldValidator1').attr('ErrorMessage')
                ValidatorEnable($("[id$='RequiredFieldValidator1']")[0], true);
                ValidatorEnable($("[id$='RequiredFieldValidator2']")[0], true);

                ValidatorEnable($("[id$='RequiredFieldValidator3']")[0], true);
                ValidatorEnable($("[id$='RequiredFieldValidator4']")[0], true);
                ValidatorEnable($("[id$='RequiredFieldValidatorddlLanguage']")[0], true);
                ValidatorEnable($("[id$='RequiredFieldValidatorddljobtype']")[0], true);
                ValidatorEnable($("[id$='RequiredFieldValidatorddlDepartment']")[0], true);
                ValidatorEnable($("[id$='RequiredFieldValidatorddlTeam']")[0], true);
                ValidatorEnable($("[id$='RequiredFieldValidatorddlgender']")[0], true);
                ValidatorEnable($("[id$='revEmail']")[0], true);

            }
            else {
                ShowCompotenceCategory(true);
            }
        }
        $(document).ready(function () {
            //xyz();
        });

        function xyz() {
            return confirm($(".abcde").text());
            //$('.def').attr("onclick", "return confirm('" + $(".abcde").text() + "')");
        }
    </script>
</asp:Content>
