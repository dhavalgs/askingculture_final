﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using startetku.Business.Logic;
using System.Web.UI.HtmlControls;
using System.Threading;
using System.Globalization;
using System.Web.Services;

public partial class Organisation_chart : System.Web.UI.Page
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            var mes = 0;
            if (!string.IsNullOrWhiteSpace(Request.QueryString["mes"]))
            {
                mes = Convert.ToInt32(Request.QueryString["mes"]);
            }



            Page.ClientScript.RegisterStartupScript(Page.GetType(), "my", " SetExpandCollapse(); Default_Userdetails(); GetmessgeById(" + mes + ");", true);
            //Conatct.InnerHtml = GetLocalResourceObject("Welcome.Text").ToString() + "  " + Convert.ToString(Session["OrgUserName"]) + "!";  
            Conatct.InnerHtml = " " + Convert.ToString(Session["OrgUserName"]) + "!";


            var notId = Request.QueryString["notIf"];
            if (!string.IsNullOrWhiteSpace(notId))
            {
                NotificationBM obj = new NotificationBM();
                obj.notId = Convert.ToInt32(notId);
                obj.notPopUpStatus = false;
                obj.UpdateNotificationbyid();
            }
            GetResource();

        }

    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion


    #region WebMethod

    [WebMethod(EnableSession = true)]
    public static User[] Userdetails()
    {
        DataTable dt = new DataTable();
        List<User> Userdetails = new List<User>();

        UserBM obj = new UserBM();
        obj.userIsActive = true;
        obj.userIsDeleted = false;
        obj.userType = 2;
        obj.userCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
        obj.userId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        if (obj.userId == 0)
        {

            return null;

        }
        else
        {

            obj.GetAllEmployeebymessge();
            DataSet ds = obj.ds;

            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dtrow in ds.Tables[0].Rows)
                {
                    //var logginUserId = obj.userId;
                    try
                    {
                        //logged in user should not visible in Chat box
                        if (obj.userId == Convert.ToInt32(dtrow["userId"]))
                        {
                            continue;
                        }
                    }
                    catch (Exception)
                    {
                        continue;
                    }

                    User User = new User();
                    User.name = dtrow["name"].ToString();
                    User.userId = dtrow["userId"].ToString();
                    User.Image = dtrow["userImage"].ToString();
                    User.notification = dtrow["notification"].ToString();
                    User.sessionimage = Convert.ToString(HttpContext.Current.Session["OrgUserImage"]);
                    Userdetails.Add(User);
                }
            }
            return Userdetails.OrderBy(o => o.masCreatedDate).ToArray();
        }

    }

    public static string days { get; set; }
    public static string hours { get; set; }
    public static string and { get; set; }
    public static string minutes { get; set; }
    public static string ago { get; set; }

    private void GetResource()
    {
        days = GetLocalResourceObject("days.Text").ToString();
        hours = GetLocalResourceObject("hours.Text").ToString();
        and = GetLocalResourceObject("and.Text").ToString();

        minutes = GetLocalResourceObject("minutes.Text").ToString();
        ago = GetLocalResourceObject("ago.Text").ToString();
        //REJECTED = GetLocalResourceObject("REJECTED.Text").ToString();
    }
    [WebMethod(EnableSession = true)]
    public static User[] GetmessgeById(string userId)
    {
        

        DataTable dt = new DataTable();
        List<User> messge = new List<User>();

        if (string.IsNullOrWhiteSpace(userId))
        {
            return messge.ToArray();
        }
        NotificationBM obj = new NotificationBM();
        obj.mesfromUserId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        obj.masToUserId = Convert.ToInt32(userId);
        obj.GetAllmessagebyId();
        DataSet ds = obj.ds;



        NotificationBM obj1 = new NotificationBM();
        obj1.notUserId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        obj1.masStatus = false;
        obj1.UpdatemessagebyId_stauts();

        if (ds.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow dtrow in ds.Tables[0].Rows)
            {
                User User = new User();
                User.messubject = dtrow["messubject"].ToString();
                User.mesfromUserId = dtrow["mesfromUserId"].ToString();
                User.Image = dtrow["img1"].ToString();
                DateTime date = Convert.ToDateTime(dtrow["masCreatedDate"].ToString());
                //User.masCreatedDate = Convert.ToString(dtrow["masCreatedDate"]);
                User.masCreatedDate = Convert.ToString(dtrow["ago"]);
                User.masCreatedDate = User.masCreatedDate.Replace("days", days);
                User.masCreatedDate = User.masCreatedDate.Replace("ago", ago);
                User.masCreatedDate = User.masCreatedDate.Replace("minutes", minutes);
                User.masCreatedDate = User.masCreatedDate.Replace("hours", hours);
                User.masCreatedDate = User.masCreatedDate.Replace("and", and);


                //ResourceLanguageBM objLang = new ResourceLanguageBM();
                //if (!string.IsNullOrEmpty(Convert.ToString(HttpContext.Current.Session["Language"])))
                //{
                //    DataSet resds = objLang.GetResourceLanguage(Convert.ToString(HttpContext.Current.Session["Language"]));
                //    User.masCreatedDate = string.Format("{0:" + resds.Tables[0].Rows[0]["resDateFormat"] + " HH:mm:ss}", date);
                //}
                //ResourceLanguageBM res = new ResourceLanguageBM();
                //DataSet resdes = res.GetAllResourceLanguage();

                //for (int i = 0; i < resdes.Tables[0].Rows.Count; i++)
                //{
                //    if (Convert.ToString( resdes.Tables[0].Rows[i]["resCulture"]) == Convert.ToString(HttpContext.Current.Session["Language"]))
                //    {
                //       // dt = string.Format("{0:" + resdes.Tables[0].Rows[i]["resDateFormat"] + " HH:mm:ss}", DateTime.Now);
                //        User.masCreatedDate = string.Format("{0:" + resdes.Tables[0].Rows[i]["resDateFormat"] + " HH:mm:ss}", date);
                //    }
                //}
                //if (Convert.ToString(HttpContext.Current.Session["Language"]) == "da-DK")
                //{
                //    User.masCreatedDate = string.Format("{0:dd/MM/yyyy HH:mm:ss}", date);
                //}
                //else
                //{
                //    User.masCreatedDate = string.Format("{0:MM/dd/yyyy HH:mm:ss}", date);
                //}

                User.sessionUserId = Convert.ToString(HttpContext.Current.Session["OrgUserId"]);
                User.name = Convert.ToString(dtrow["name"].ToString());
                messge.Add(User);
            }
        }
        return messge.ToArray();
    }
    [WebMethod(EnableSession = true)]
    // public static string insertmessage(string message, string touserid, string currentdate,string currenttime)
    public static string insertmessage(string message, string touserid)
    {
        string msg = string.Empty;
        NotificationBM obj = new NotificationBM();
        obj.mesfromUserId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        obj.masToUserId = Convert.ToInt32(touserid);
        obj.messubject = message;
        obj.masIsActive = true;
        obj.masIsDeleted = false;
        //currenttime=currenttime.Replace("-", ":");
        //currentdate += " " + currenttime;
        //obj.masCreatedDate = Convert.ToDateTime(currentdate);//Convert.ToDateTime(currentdate+" "+currenttime);//DateTime.UtcNow; 
        obj.masCreatedDate = DateTime.Now;
        obj.masCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
        obj.mastype = "";
        string dt = Convert.ToString(DateTime.Now);

        //ResourceLanguageBM res = new ResourceLanguageBM();
        //DataSet resdes = res.GetAllResourceLanguage();

        //for (int i = 0; i < resdes.Tables[0].Rows.Count; i++)
        //{
        //    if(Convert.ToString (resdes.Tables[0].Rows[i]["resCulture"] )== Convert.ToString(HttpContext.Current.Session["Language"]))
        //    {
        //        dt = string.Format("{0:" + resdes.Tables[0].Rows[i]["resDateFormat"] + " HH:mm:ss}", DateTime.Now);
        //    }
        //}
        ResourceLanguageBM objLang = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(Convert.ToString(HttpContext.Current.Session["Language"])))
        {
            DataSet resds = objLang.GetResourceLanguage(Convert.ToString(HttpContext.Current.Session["Language"]));
            dt = string.Format("{0:" + resds.Tables[0].Rows[0]["resDateFormat"] + " HH:mm:ss}", DateTime.Now);
        }

        //if (Convert.ToString(HttpContext.Current.Session["Language"]) == "da-DK")
        //{
        //    dt = string.Format("{0:dd/MM/yyyy HH:mm:ss}", DateTime.Now);
        //}
        //else
        //{
        //    dt = string.Format("{0:MM/dd/yyyy HH:mm:ss}", DateTime.Now);
        //}

        if (obj.InsertMessage())
        {
            msg = "true," + dt;
        }
        else
        {
            msg = "false," + dt;
        }
        NotificationBM aobj = new NotificationBM();
        aobj.notsubject = message;//"Click here to check your message.";
        aobj.notUserId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        aobj.notIsActive = true;
        aobj.notIsDeleted = false;
        aobj.notCreatedDate = DateTime.Now;
        aobj.notCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
        aobj.notpage = "chart.aspx?mes=" + Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        aobj.nottype = "Message";
        aobj.notToUserId = Convert.ToInt32(touserid);
        aobj.InsertNotification();

        return msg;
    }
    [WebMethod(EnableSession = true)]
    public static User[] GetAllmessagebyId_stauts(string userId)
    {
        DataTable dt = new DataTable();
        List<User> messge = new List<User>();

        if (string.IsNullOrWhiteSpace(userId))
        {
            return messge.ToArray();
        }
        NotificationBM obj = new NotificationBM();
        obj.mesfromUserId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        obj.masToUserId = Convert.ToInt32(userId);
        obj.masStatus = true;
        obj.GetAllmessagebyId_stauts();
        DataSet ds = obj.ds;

        NotificationBM obj1 = new NotificationBM();
        obj1.notUserId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        obj1.masStatus = false;
        obj1.UpdatemessagebyId_stauts();
        CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
        if (ds.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow dtrow in ds.Tables[0].Rows)
            {
                if (dtrow["mesfromUserId"].ToString() != Convert.ToString(HttpContext.Current.Session["OrgUserId"]))
                {
                    User User = new User();
                    User.messubject = dtrow["messubject"].ToString();
                    User.mesfromUserId = dtrow["mesfromUserId"].ToString();
                    User.Image = dtrow["img1"].ToString();

                    User.masCreatedDate = dtrow["masCreatedDate"].ToString();
                    User.sessionUserId = Convert.ToString(HttpContext.Current.Session["OrgUserId"]);
                    User.name = Convert.ToString(dtrow["username"].ToString());
                    messge.Add(User);
                }
            }
        }
        return messge.ToArray();
    }
    public class User
    {
        public string name { get; set; }
        public string userId { get; set; }
        public string Image { get; set; }
        public string messubject { get; set; }
        public string masCreatedDate { get; set; }
        public string mesfromUserId { get; set; }
        public string sessionUserId { get; set; }
        public string notification { get; set; }
        public string sessionimage { get; set; }
    }
    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        //string language = "French";
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Danish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }

        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }

    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion
}