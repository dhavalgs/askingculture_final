﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using startetku.Business.Logic;
using System.Web.Services;
using System.Web.Script.Services;
using System.Threading;
using System.Globalization;
using starteku_BusinessLogic.Model;

public partial class Organisation_Employee : System.Web.UI.MasterPage
{
    string country = "";
    string state = "";
    startetkuEntities1 db = new startetkuEntities1();
    protected void Page_Load(object sender, EventArgs e)
    {

        LogoMaster logo = db.LogoMasters.FirstOrDefault();
        var userId = Convert.ToInt32(Session["OrgUserId"]);
        hdnLoginUserIDType.Value = Convert.ToString(Session["OrguserType"]);
        Int32 userCompanyID = Convert.ToInt32(Session["OrgCompanyId"]);
        var EnaData = db.GetItemsEnableList(userId, userCompanyID).FirstOrDefault();

        Session["Aemcompid"] = EnaData.Aemcompid;
        Session["Cemcompid"] = EnaData.comdirCompID;
        Session["Compcompid"] = EnaData.compCompID;
        Session["Pointcompid"] = EnaData.pointCompID;
        Session["Quescompid"] = EnaData.quesemID;

        if (logo != null)
        {

            imgPageLogo.ImageUrl = "../Log/upload/Userimage/" + logo.PageLogo;
            ViewState["imgLoginLogo"] = logo.PageLogo;
        }
        if (string.IsNullOrWhiteSpace(Convert.ToString(Session["Aemcompid"])))
        {
            activityPanel.Visible = false;
        }

        if (string.IsNullOrWhiteSpace(Convert.ToString(Session["Compcompid"])))
        {
            competencePanel.Visible = false;
        }
        if (!IsPostBack)
        {
            if (!String.IsNullOrEmpty(Convert.ToString(Session["OrgUserName"])))
            {
                Setlanguage();
                detdata();
                //rep_notificationAll();
                GetUserpoints();
                Getpdprpoints();
            }
        }
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrgUserName"])))
        {
            span2.InnerHtml = Convert.ToString(Session["OrgUserName"]);
            span3.InnerHtml = state + "," + country;
            span4.InnerHtml = state + "," + country;
            //login.InnerHtml = "Last Login was         " + Convert.ToString(Session["userDateTime"]);
            login.InnerHtml = Convert.ToString(GetLocalResourceObject("LastLoginResource.Text"));
            login1.InnerHtml = Convert.ToString(GetLocalResourceObject("LastLoginResource.Text"))+" : " + Convert.ToString(Session["userDateTime"]);
        }
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrgUserImage"])))
        {
            img.ImageUrl = "../Log/upload/Userimage/" + Convert.ToString(Session["OrgUserImage"]) + "";
            img1.ImageUrl = "../Log/upload/Userimage/" + Convert.ToString(Session["OrgUserImage"]) + "";
        }
    }

    protected void detdata()
    {
        UserBM obj = new UserBM();
        obj.userId = Convert.ToInt32(Session["OrgUserId"]);
        obj.GetEmployeedetailsbyid();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["couName"])))
                    country = Convert.ToString(ds.Tables[0].Rows[0]["couName"]);

                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["staName"])))
                    state = Convert.ToString(ds.Tables[0].Rows[0]["staName"]);
            }
        }
    }

    //protected void rep_notificationAll()
    //{
    //    try
    //    {
    //        NotificationBM obj = new NotificationBM();
    //        obj.notIsActive = true;
    //        obj.notIsDeleted = false;
    //        obj.userCreateBy = Convert.ToString(Session["OrgUserId"]);
    //        obj.GetAllNotification();
    //        DataSet ds = obj.ds;
    //        ViewState["data"] = ds;

    //        if (ds != null)
    //        {
    //            if (ds.Tables[0].Rows.Count > 0)
    //            {
    //                DataView dv = new DataView();
    //                dv = ds.Tables[0].DefaultView;
    //                String messge = "Message";
    //                dv.RowFilter = ("nottype <>  '" + messge + "'");
    //                DataTable dtitm = dv.ToTable();
    //                DataSet ds1 = new DataSet();
    //                ds1.Tables.Add(dtitm);

    //                DataView dv1 = new DataView();
    //                dv1 = ds.Tables[0].DefaultView;
    //                dv1.RowFilter = ("nottype =  '" + messge + "'");
    //                DataTable dtitm1 = dv1.ToTable();
    //                DataSet ds2 = new DataSet();
    //                ds2.Tables.Add(dtitm1);

    //                //rep_notification.DataSource = ds1;
    //                //rep_notification.DataBind();

    //                //rpt_messge1.DataSource = ds2;
    //                //rpt_messge1.DataBind();
    //            }
    //            else
    //            {
    //                //rep_notification.DataSource = null;
    //                //rep_notification.DataBind();

    //                //rpt_messge1.DataSource = null;
    //                //rpt_messge1.DataBind();
    //            }
    //            if (ds.Tables[1].Rows.Count > 0)
    //            {
    //                lblcountnotification.Text = Convert.ToString(ds.Tables[1].Rows[0]["count1"]);
    //                lblshownotification.Text = Convert.ToString(ds.Tables[1].Rows[0]["count1"]);

    //                lblmessge.Text = Convert.ToString(ds.Tables[2].Rows[0]["count1"]);
    //                lblshowmessge.Text = Convert.ToString(ds.Tables[2].Rows[0]["count1"]);
    //            }
    //            else
    //            {
    //               lblcountnotification.Text = "0";
    //               lblmessge.Text = "0";
    //            }
    //        }
    //        else
    //        {
    //            //rep_notification.DataSource = null;
    //            //rep_notification.DataBind();

    //            //rpt_messge1.DataSource = null;
    //            //rpt_messge1.DataBind();
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        Common.WriteLog("error in rep_notificationAll" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
    //        throw ex;
    //    }
    //}

    private void GetUserpoints()
    {
        try
        {
            PointBM obj = new PointBM();
            obj.GetUserPoint(Convert.ToInt32(Session["OrgUserId"]), true);
            DataSet ds = obj.ds;
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["pointPLP"])))
                {
                    lblPLP.Text = Convert.ToString(ds.Tables[0].Rows[0]["pointPLP"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["pointALP"])))
                {
                    lblALP.Text = Convert.ToString(ds.Tables[0].Rows[0]["pointALP"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["pointTotal"])))
                {
                    lblTotal.Text = Convert.ToString(ds.Tables[0].Rows[0]["pointTotal"]);
                }
               
            }

        }
        catch (Exception ex)
        {

        }
    }

    private void Getpdprpoints()
    {
        try
        {
            SubCompetenceBM obj = new SubCompetenceBM();
            obj.subIsActive = true;
            obj.subIsDeleted = false;
            obj.GetPDPpoint(Convert.ToInt32(Session["OrgUserId"]));
            DataSet ds = obj.ds;
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["PDPTotal"])))
                    {
                        lblpdp.Text = Convert.ToString(ds.Tables[0].Rows[0]["PDPTotal"]);
                    }
                }

            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void Setlanguage()
    {
        try
        {
            string language = Convert.ToString(Session["Culture"]);
            //string language = "Denish";
            string languageId = "";
            //if (!string.IsNullOrEmpty(language))
            //{
            //    if (language.EndsWith("Denish")) languageId = "da-DK";
            //    else languageId = "en-GB";
            //    SetCulture(languageId);
            //}
            //ResourceLanguageBM obj = new ResourceLanguageBM();
            //DataSet resds = obj.GetAllResourceLanguage();
            //if (!string.IsNullOrEmpty(language))
            //{
            //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
            //    {
            //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
            //        {
            //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
            //        }
            //    }

            //    SetCulture(languageId);
            //}
            ResourceLanguageBM obj = new ResourceLanguageBM();
            if (!string.IsNullOrEmpty(language))
            {
                DataSet resds = obj.GetResourceLanguage(language);
                languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

                SetCulture(languageId);
            }

            if (Session["Language"] != null)
            {
                if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
            }
        }
        catch { }
    }
    protected void SetCulture(string languageId)
    {
        try
        {
            Session["Language"] = languageId;
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
        }
        catch { }
    }
    protected string GetToolTip(string tag)
    {
        var localResourceObject = "";
        try
        {
            localResourceObject = Convert.ToString(GetLocalResourceObject(tag));
        }
        catch (Exception)
        {
            localResourceObject = "";

        }
        return localResourceObject;
    }
}
