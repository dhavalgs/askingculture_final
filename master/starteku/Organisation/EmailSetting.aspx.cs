﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using System.Threading;
using System.Globalization;
using startetku.Business.Logic;

public partial class Organisation_EmailSetting : System.Web.UI.Page
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("Login.aspx");
        }
        if (!IsPostBack)
        {
            Setting.InnerHtml = GetLocalResourceObject("Welcome.Text").ToString() + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            Getmailsettingbyid();
        }
    }
    #endregion

    #region Button Click
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        Updatemailsetting(Convert.ToInt32(Session["OrgCompanyId"]));
    }
    protected void btnCancel_click(object sender, EventArgs e)
    {
        Response.Redirect("Organisation-dashboard.aspx");

    }
    #endregion

    #region method
    protected void Updatemailsetting(int OrgCompanyId)
    {
        EmailSettingBM mail = new EmailSettingBM();
        mail.EmailUserId = Convert.ToInt32(Session["OrgUserId"]);
        mail.EmailCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        mail.EmailCreatedDate = DateTime.Now;
        mail.EmailIsActive = true;
        mail.EmailIsDeleted = false;
        mail.EmailImage = "";
        mail.EmailHeader = "";
        mail.EmailFooter = "";
        mail.EmailSender = txtsEmail.Text;
        mail.Email = "";
        mail.password = CommonModule.encrypt(txtpassword.Text.Trim());
        mail.EmailHost = txthost.Text;
        mail.EmailPort = txtport.Text;
        mail.EmailDisplayName = txtdisplayname.Text;
        mail.InsertEmailSetting();
        if (mail.ReturnBoolean == true)
        {
            lblMsg.Text = CommonModule.msgRecordUpdatedSuccss;
            lblMsg.ForeColor = System.Drawing.Color.White;
        }
        else
        {
            lblMsg.Text = CommonModule.msgSomeProblemOccure;
            lblMsg.ForeColor = System.Drawing.Color.White;
        }
    }
    protected void SetDefaultMessage()
    {
        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["msg"])))
        {
            if (Convert.ToString(Request.QueryString["msg"]) == "del")
            {
                lblMsg.Text = CommonModule.msgRecordHasBeenDeletedSuccessfully;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "ins")
            {
                lblMsg.Text = CommonModule.msgRecordInsertedSuccessfully;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "upd")
            {
                lblMsg.Text = CommonModule.msgRecordUpdatedSuccss;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "arc")
            {
                lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
        }

    }
    protected void Getmailsettingbyid()
    {
        EmailSettingBM mail = new EmailSettingBM();
        mail.EmailIsActive = true;
        mail.EmailIsDeleted = false;
        mail.EmailCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        mail.GetAllEmailSetting();
        DataSet ds = mail.ds;
        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["EmailSender"])))
                    txtsEmail.Text = Convert.ToString(ds.Tables[0].Rows[0]["EmailSender"]);


                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["password"])))
                {
                    // txtpassword.Text = Convert.ToString(ds.Tables[0].Rows[0]["password"]);
                    string Password = CommonModule.decrypt(Convert.ToString(ds.Tables[0].Rows[0]["password"]));
                    txtpassword.Attributes.Add("value", Password);
                    txtConfirmPassword.Attributes.Add("value", Password);
                }

                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["EmailHost"])))
                    txthost.Text = Convert.ToString(ds.Tables[0].Rows[0]["EmailHost"]);


                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["EmailPort"])))
                    txtport.Text = Convert.ToString(ds.Tables[0].Rows[0]["EmailPort"]);

                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["EmailDisplayName"])))
                    txtdisplayname.Text = Convert.ToString(ds.Tables[0].Rows[0]["EmailDisplayName"]);


                //if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userImage"])))
                //    ViewState["userImage"] = Convert.ToString(ds.Tables[0].Rows[0]["userImage"]);
                //img_profile.ImageUrl = "../Log/upload/Userimage/" + Convert.ToString(ds.Tables[0].Rows[0]["userImage"]) + "";
                //Image1.ImageUrl = "../Log/upload/Userimage/" + Convert.ToString(ds.Tables[0].Rows[0]["userImage"]) + "";

                //if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["languageName"])))
                //    ddllanguages.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["languageName"]);
                //lblLanguage.Text = Convert.ToString(ds.Tables[0].Rows[0]["languageName"]);
            }

        }

    }
    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        //string language = "French";
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Denish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion
}