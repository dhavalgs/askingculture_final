﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="forget_password.aspx.cs" Inherits="Organisation_forget_password" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Log in </title>


    <%--<link href='fonts.googleapis.com/css_ac5e1c55.css' rel='stylesheet' type='text/css' />
    <link href='fonts.googleapis.com/css_3e3d9b9e.css' rel='stylesheet' type='text/css' />
    <link href='fonts.googleapis.com/css_98ee0297.css' rel='stylesheet' type='text/css' />
    <link href='fonts.googleapis.com/css_7f7d8532.css' rel='stylesheet' type='text/css' />
    <link href='fonts.googleapis.com/css_52a9019e.css' rel='stylesheet' type='text/css' />--%>

    <!-- Styles -->
    <link rel="stylesheet" href="font-awesome-4.0.3/css/font-awesome.css" type="text/css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <!-- Style -->
    <link rel="stylesheet" href="css/responsive.css" type="text/css" />
    <!-- Responsive -->
    
  

    <style type="text/css">
        .sign-in-form form input[type=submit] {
            box-shadow: 0px 2px 0px 0px #3E4C5C inset;
            background: -moz-linear-gradient(center top, #3C4752 5%, #2F3640 100%) repeat scroll 0% 0% #3C4752;
            border-radius: 4px;
            border: 1px solid #323E4F;
            display: inline-block;
            color: #FFF;
            font-family: Arial;
            font-size: 15px;
            font-weight: bold;
            font-style: normal;
            height: 35px;
            line-height: 20px;
            width: 150px;
            text-decoration: none;
            text-align: center;
            margin-left: 20px;
        }

        .sign-in-form form input[type="text"], input[type="password"] {
            background: none repeat scroll 0px 0px #D9DDE9;
            border: medium none;
            border-radius: 0px 3px 3px 0px;
            color: #666;
            float: left;          
            font-size: 15px;
            height: 45px;
            letter-spacing: 0.3px;
            padding-left: 20px;
            width: 300px;
            margin:20px;
        }
        #osx-modal-content, #osx-modal-data {display:none;}

/* Overlay */
#osx-overlay {background-color:#000;}

/* Container */
#osx-container {background-color:#eee; color:#000; font: 16px/24px "Lucida Grande",Arial,sans-serif; padding-bottom:4px; width:600px; -moz-border-radius-bottomleft:6px; -webkit-border-bottom-left-radius:6px; -moz-border-radius-bottomright:6px; -webkit-border-bottom-right-radius:6px; border-radius:0 0 6px 6px; -moz-box-shadow:0 0 64px #000; -webkit-box-shadow:0 0 64px #000; box-shadow:0 0 64px #000;}
#osx-container a {color:#ddd;}
#osx-container #osx-modal-title {color:#000; background-image: url("images/yellow.jpg");
    background-size: 100% 100%; border-bottom:1px solid #ccc; font-weight:bold; padding:6px 8px; text-shadow:0 1px 0 #f4f4f4;}
#osx-container .close {display:none; position:absolute; right:0; top:0;}
#osx-container .close a {display:block; color:#777; font-weight:bold; padding:6px 12px 0; text-decoration:none; text-shadow:0 1px 0 #f4f4f4;}
#osx-container .close a:hover {color:#000;}
#osx-container #osx-modal-data {font-size:12px; padding:6px 12px;}
#osx-container h2 {margin:10px 0 6px;}
#osx-container p {margin-bottom:10px;}
#osx-container span {color:#777;}
    </style>
</head>


<body class="sign-in-bg" style="background: #0b5d99;">
    <div class="sign-in">
        <div class="sign-in-head yellow" style="border: none;">
            <div class="sign-in-details" style="width: 300px;">
                <h1>Forgot Password <i class="fa fa-lock"></i></h1>
            </div>
            <p></p>
        </div>
        <div class="sign-in-form">
            <form id="Form1" runat="server">

                <asp:TextBox ID="txtemail" runat="server" CssClass="form-control" placeholder="Enter Email Address"
                    MaxLength="200"></asp:TextBox>
                <div style="margin-left: 22px;"><asp:RequiredFieldValidator ID="rfvEmail" runat="server" CssClass="commonerrormsg"
                    ControlToValidate="txtemail" Display="Dynamic" ValidationGroup="chk" ErrorMessage="Email is required."></asp:RequiredFieldValidator>
                    </div>
                <asp:RegularExpressionValidator ID="revEmail" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                    ControlToValidate="txtemail" CssClass="commonerrormsg" ValidationGroup="chk"></asp:RegularExpressionValidator>
                <asp:Button runat="server" ID="btnsubmit" Text="Submit" CssClass="submit btn btn-default btn-block "
                    OnClick="btnForgot_ServerClick" ValidationGroup="chk" />
                <asp:Button runat="server" ID="Button1" Text=" Back " CssClass="submit btn btn-default btn-block"
                    CausesValidation="false" PostBackUrl="Login.aspx" />
                <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" style="margin:22px"></asp:Label>
               <input type='button' name='osx' value='Demo' style="display: none" class='osx demo' id="popupConfirmation"/> 
            </form>
        </div>
<%----%>
        <div id="osx-modal-content">
			<div id="osx-modal-title">Reset password maill sent successfully!</div>
			<div class="close"><a href="#" class="simplemodal-close">x</a></div>
			<div id="osx-modal-data">
				<h2>Hello! </h2>
				<h2>An e mail has been send to you in order to reset your password</h2>
<%--danish--%>            <%--    <p>“En e-mail er sendt til dig så du kan nulstille dit password"</p>--%>

				
				<p><button class="simplemodal-close" onclick="window.location='login.aspx'">Login Page</button> <span>(or press ESC or click the overlay to update email address and resend mail)</span></p>
			</div>
            
		</div>
        <ul>
            <li><a href="#" title=""><i class="fa fa-facebook"></i></a></li>
            <li><a href="#" title=""><i class="fa fa-twitter"></i></a></li>
            <li><a href="#" title=""><i class="fa fa-google-plus"></i></a></li>
        </ul>
    </div>
    
    <script src="../Scripts/modelPopup/jquery.js"></script>
    <script src="../Scripts/modelPopup/jquery.simplemodal.js"></script>
    <script src="../Scripts/modelPopup/osx.js"></script>
      <script src="../assets/js/jquery.noty.packaged.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            "use strict";
            Login.init(); // Init login JavaScript
        });

        function generate(type, text, layout) {
            var n = noty({
                text: text,
                type: type,
                dismissQueue: true,
                timeout: 10000,
                closeWith: ['click'],
                layout: layout,
                theme: 'defaultTheme',
                maxVisible: 10
            });
            console.log('html: ' + n.options.id);
        }

        function generate1(type, text) {
            var n = noty({
                text: text,
                type: type,
                dismissQueue: true,
                timeout: 10000,
                closeWith: ['click'],
                layout: "topCenter",
                theme: 'defaultTheme',
                maxVisible: 10
            });
            console.log('html: ' + n.options.id);
        }

        function generateAll() {
            generate('alert', 'alert');
            generate('information', 'information');
            generate('error', 'e');
            generate('warning', 'w');
            generate('notification', 'n');
            generate('success', 's');
        }
    </script>
    
</body>
</html>
