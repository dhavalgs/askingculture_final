﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;


public partial class Organisation_OrgCmsList : System.Web.UI.Page
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            CMS.InnerHtml = "Welcome   " + Convert.ToString(Session["OrgUserName"]) + "!";
            CMSSelectAll();
            CMSntrySelectArchiveAll();
            SetDefaultMessage();
        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion

    #region method
    protected void SetDefaultMessage()
    {
        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["msg"])))
        {
            if (Convert.ToString(Request.QueryString["msg"]) == "del")
            {
                lblMsg.Text = CommonModule.msgRecordHasBeenDeletedSuccessfully;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "ins")
            {
                lblMsg.Text = CommonModule.msgRecordInsertedSuccessfully;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "upd")
            {
                lblMsg.Text = CommonModule.msgRecordUpdatedSuccss;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "arc")
            {
                lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
        }

    }
    protected void CMSSelectAll()
    {
        try
        {
            CmsBM obj = new CmsBM();
            obj.cmsIsActive = true;
            obj.cmsIsDeleted = false;
            obj.cmsCompanyId = Convert.ToInt32(Session["OrgUserId"]);
            obj.GetAllCms();
            DataSet ds = obj.ds;

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvGrid.DataSource = ds.Tables[0];
                    gvGrid.DataBind();

                    gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                else
                {
                    gvGrid.DataSource = null;
                    gvGrid.DataBind();
                }
            }
            else
            {
                gvGrid.DataSource = null;
                gvGrid.DataBind();
            }
        }
        catch (DataException ex)
        {
            throw ex;
        }
    }
    protected void CMSntrySelectArchiveAll()
    {
        try
        {
            CmsBM obj = new CmsBM();
            obj.cmsIsActive = false;
            obj.cmsIsDeleted = false;
            obj.cmsCompanyId = Convert.ToInt32(Session["OrgUserId"]);
            obj.GetAllCms();
            DataSet ds = obj.ds;

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvArchive.DataSource = ds.Tables[0];
                    gvArchive.DataBind();
                    gvArchive.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                else
                {
                    gvArchive.DataSource = null;
                    gvArchive.DataBind();
                }
            }
            else
            {
                gvArchive.DataSource = null;
                gvArchive.DataBind();
            }
        }
        catch (DataException ex)
        {
            throw ex;
        }
    }
    #endregion

    #region grid Event
    protected void gvGrid_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "archive")
        {
            CmsBM obj = new CmsBM();
            obj.cmsId = Convert.ToInt32(e.CommandArgument);
            obj.cmsIsActive = false;
            obj.cmsIsDeleted = false;
            obj.cmsStatusUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    CMSSelectAll();
                    CMSntrySelectArchiveAll();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }
    }

    protected void gvArchive_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "archive")
        {
            CmsBM obj = new CmsBM();
            obj.cmsId = Convert.ToInt32(e.CommandArgument);
            obj.cmsIsActive = true;
            obj.cmsIsDeleted = false;
            obj.cmsStatusUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    CMSSelectAll();
                    CMSntrySelectArchiveAll();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }
        else if (e.CommandName == "permanentlydelete")
        {
            CmsBM obj = new CmsBM();
            obj.cmsId = Convert.ToInt32(e.CommandArgument);
            obj.cmsIsActive = false;
            obj.cmsIsDeleted = true;
            obj.cmsStatusUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    CMSSelectAll();
                    CMSntrySelectArchiveAll();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }
    }

    #endregion

    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        try
        {
            if (String.IsNullOrEmpty(txtDescription.Text))
            {
                lblmsgDescription.Text = "Please enter Description.";
            }
            else
            {
                CmsBM obj = new CmsBM();
                obj.cmsName = txtName.Text;
                obj.cmsDescription = txtDescription.Text;
                obj.cmsCompanyId = Convert.ToInt32(Session["OrgUserId"]);
                obj.cmsIsActive = true;
                obj.cmsIsDeleted = false;
                obj.cmsCreatedDate = DateTime.Now;
                obj.InsertCms();
                if (obj.ReturnBoolean == true)
                {
                    Response.Redirect("OrgCmsList.aspx");
                }
            }
        }
        catch (DataException ex)
        {
            throw ex;
        }
    }
    #endregion
}