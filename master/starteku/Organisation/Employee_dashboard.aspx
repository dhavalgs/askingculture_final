﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/Employee.master" AutoEventWireup="true"
    CodeFile="Employee_dashboard.aspx.cs" Inherits="Organisation_Employee_dashboard"
    Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .HiddenText label {
            display: none;
        }
    </style>
    <style type="text/css">
        #lines span, #toggle span i {
            color: #333333;
            float: left;
            font-size: 13px;
            line-height: 22px;
            padding: 3px 9px;
        }

        #lines, #toggle {
            border-bottom: 2px solid #F65B5B;
            cursor: pointer;
            float: right;
            height: 32px;
            margin-left: 10px;
            margin-right: 0;
            padding: 0;
            width: 34px;
            background-color: white;
            border: 1px solid #EBEBEB;
            z-index: 100;
        }

        .yellow {
            border-radius: 0px;
        }

        .treeNode {
        }

            .treeNode input {
                width: auto;
                margin: 5px;
                float: left !important;
            }

        .chat-widget-head {
            float: left;
            width: 100%;
        }
    </style>
    <%--joyride---------------------------------------------%>
    <%--<script src="../Scripts/jquery321.js" type="text/javascript"></script>--%>
    <link href="../plugins/joyride/joyride-2.1.css" rel="stylesheet" type="text/css" />
    <%--<script src="../plugins/joyride/jquery-1.10.1.js" type="text/javascript"></script>--%>
    <script src="../plugins/joyride/jquery.cookie.js" type="text/javascript"></script>
    <script src="../plugins/joyride/jquery.joyride-2.1.js" type="text/javascript"></script>
    <script src="../plugins/joyride/modernizr.mq.js" type="text/javascript"></script>

    <%-- <script src="../plugins/highchart/highcharts.js" type="text/javascript"></script>--%>
    <script type="text/javascript" src="../plugins/highchart/highstock.js"></script>
    <%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js" type="text/javascript"></script>--%>

    <script src="../plugins/highchart/highcharts-more.js" type="text/javascript"></script>
    <script src="../Scripts/plugins/exporting.js" type="text/javascript"></script>
    <link href="../assets/css/multiselect.css" rel="stylesheet" />
    <script src="../assets/js/multiselect.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(window).load(function () {
            $("#bars").click();
        });
    </script>





    <%--joyride---------------------------------------------%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:HiddenField ID="hdnLastlogin" runat="server" Value="Last Login was on : " meta:resourcekey="resourceLastLogin"></asp:HiddenField>
    <%--Hiddne Labels--%>

    <asp:HiddenField ID="hdnSystemUpdated" runat="server" meta:resourcekey="SystemUpdate" />
    <asp:HiddenField ID="Literal7" runat="server" meta:resourcekey="Pending" />
    <asp:HiddenField ID="hdnAccpeted" runat="server" meta:resourcekey="Accpeted" EnableViewState="false" />
    <asp:HiddenField ID="hdnPending" runat="server" meta:resourcekey="Pending" EnableViewState="false" />
    <asp:HiddenField ID="hdnCancel" runat="server" meta:resourcekey="Cancel" EnableViewState="false" />
    <asp:HiddenField ID="hdnMinute" runat="server" meta:resourcekey="Minute" EnableViewState="false" />
    <asp:HiddenField ID="hdnHour" runat="server" meta:resourcekey="Hour" EnableViewState="false" />
    <asp:HiddenField ID="hdnDays" runat="server" meta:resourcekey="Days" EnableViewState="false" />
    <asp:HiddenField ID="hdnSecond" runat="server" meta:resourcekey="Seconds" EnableViewState="false" />

    <asp:HiddenField ID="hdnChartLevel" runat="server" Value="" meta:resourcekey="ChartLevelRes" />
    <asp:HiddenField ID="hdnChartTarget" runat="server" Value="" meta:resourcekey="TargetRes" />
    <asp:HiddenField ID="hdnChartActual" runat="server" Value="" meta:resourcekey="ActualRes" />

    <asp:HiddenField ID="hdnChartMax" runat="server" Value="" meta:resourcekey="MaxRes" />

    <asp:HiddenField ID="hdnNoOption" runat="server" Value="" meta:resourcekey="NoOptionRes" />

    <asp:HiddenField ID="hdnPrintChart" runat="server" Value="" meta:resourcekey="PrintChartRes" />
    <asp:HiddenField ID="hdnDownloadPNG" runat="server" Value="" meta:resourcekey="DownloadPNGRes" />
    <asp:HiddenField ID="hdnDownloadJPEG" runat="server" Value="" meta:resourcekey="DownloadJPEGRes" />
    <asp:HiddenField ID="hdnDownloadPDF" runat="server" Value="" meta:resourcekey="DownloadPDFRes" />
    <asp:HiddenField ID="hdnDownloadSVG" runat="server" Value="" meta:resourcekey="DownloadSVGRes" />

       <asp:HiddenField ID="hdnGraphUserSelectedID" runat="server" Value=""/>

    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg"></asp:Label>
    <div class="col-md-6">
        <%--<div class="heading-sec">
            <h1>Dashboard  <i>Welcome To Bette Bayer!</i></h1>
        </div>--%>
        <div class="heading-sec">
            <h1>
                <%--   <%= CommonMessages.Dashboard%>      --%>

                <i><span>
                    <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Welcome" EnableViewState="false" /></span><span runat="server" id="Dashboard"></span></i>
            </h1>
        </div>
    </div>
    <div class="col-md-6">
        <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; display: none; padding: 5px 10px; border: 1px solid #ccc">
            <i class="fa fa-calendar icon-calendar icon-large"></i><span>August 5, 2014 - September
                3, 2014</span> <b class="caret"></b>
        </div>
    </div>
    <!--<li class="range">
<a href="#">
<i class="icon-calendar"></i>
<span>August 5, 2014 - September 3, 2014</span>
<i class="icon-angle-down"></i>
</a>
</li>-->
    <div class="col-md-12">
        <div id="graph-wrapper">

            <h3 class="custom-heading darkblue" style="height: 45px">
                <%-- <%= CommonMessages.EmployeeSkills%>--%>
                <input type="hidden" id="hdnChartType" name="hdnChartType" value="bars" />
                <asp:Literal ID="Literal5" runat="server" meta:resourcekey="EmployeeSkills" EnableViewState="false" />

                <%--                   <a href="#" id="bars" onclick="return SetChartType('bars');"><span><i class="fa fa-bar-chart-o"></i></span></a>
                    <a href="#" id="lines" class="active" onclick="return SetChartType('lines');"><span><i class="fa fa-bullseye" style="font-size:15px;"></i></span></a>
                    <a href="#" id="toggle" onclick="return SetChartType('toggle');"><span><i class="fa fa-bar-chart-o"></i></span></a>--%>
                <a href="#" id="bars" onclick="return SetChartType('bars');"><span>

                    <img src="img/vertical-bar-chart-loss-icon.jpg" style="height: 25px; width: 25px; margin-left: 3px; margin-top: 2px;" />
                </span></a>
                <a href="#" id="lines" class="active" onclick="return SetChartType('lines');">
                    <i class="fa fa-bullseye" style="font-size: 15px; margin-left: 10px; margin-top: 8px; color: black"></i></a>
                <a href="#" id="toggle" onclick="return SetChartType('bars');"><span><i class="fa fa-bar-chart-o"></i>
                </span></a>

            </h3>

            <div class="home_grap">
                <%--<span id="lblUserName" style="margin-left: 50%"><asp:Label runat="server" meta:resourcekey="AllEmployeeResource" ID="lblAllEmployee"/></span>--%>

                <span id="lblUserName" style="margin-left: 50%; display: none;">
                    <asp:Label runat="server" meta:resourcekey="AllEmployeeResource" ID="lblAllEmployee" /></span>

                <div>
                    <input type="hidden" value="0" id="ddlCompetence" name="ddlCompetence" />
                    <asp:DropDownList ID="lstFruits" runat="server" multiple="multiple">
                    </asp:DropDownList>
                </div>
                <%-- <div class="graph-container" style="margin-top:50px">--%>


                <div id="container" style="width: 100%; height: 400px;">
                </div>

                <div id="graph-barsEmployee" style="width: 100%; height: 400px; margin-top: 10px;">
                </div>


                <div id="graph-barEmp" style="width: 100%; height: 400px;">
                </div>

                <select id="chartType" name="Chart Type" style="float: right; margin-top: -430px; margin-right: 20%; position: relative;">
                    <option value="0">
                        <asp:Label runat="server" meta:resourcekey="sortbyres"></asp:Label></option>
                    <option value="1">
                        <asp:Label runat="server" meta:resourcekey="hightolowres"></asp:Label></option>
                    <option value="2">
                        <asp:Label runat="server" meta:resourcekey="lowtohighres"></asp:Label></option>
                </select>

                <select id="ddlUserTarget" name="Chart Type" style="float: right; margin-top: -430px; margin-right: 35px; position: relative;">
                    <option value="1" selected="selected">
                        <asp:Literal ID="Literal9" runat="server" meta:resourcekey="shorttermRes" Text="Show short term target" EnableViewState="false" /></option>
                    <option value="2">
                        <asp:Literal ID="Literal10" runat="server" meta:resourcekey="longtermRes" Text="Show long term target" EnableViewState="false" /></option>
                    <option value="3">
                        <asp:Literal ID="Literal21" runat="server" meta:resourcekey="targetJobType" Text="Show company target for JobType" EnableViewState="false" /></option>
                    <option value="4">
                        <asp:Literal ID="Literal22" runat="server" meta:resourcekey="targetDivision" Text="Show company target for Division" EnableViewState="false" /></option>
                    <%-- <option value="3"><asp:Literal ID="Literal11" runat="server" meta:resourcekey="longtermRes" Text="Show company target for Team" EnableViewState="false" /></option>--%>
                </select>
                <%-- </div>--%>
            </div>
        </div>
        <!-- Widget Visitor -->
        <!-- ORDR REVIEWS -->
    </div>
    <div class="col-md-6">
        <div class="timeline-sec widget-body" style="margin-top: 28px;">
            <div class="timeline-head yellow yellow-radius">
                <span style="line-height: 25px;"><%--Request(s) For Help--%>
                    <asp:Literal ID="Literal2" runat="server" meta:resourcekey="Request_Help" EnableViewState="false" />
                    <i></i></span>
                <div class="add-btn">
                    <a href="OrgInvitation_request.aspx" title="" style="border-radius: 5px"><i class="fa fa-plus blue"></i><%--VIEW ALL  --%>
                        <asp:Literal ID="Literal4" runat="server" meta:resourcekey="VIEW_ALL" EnableViewState="false" /></a>
                </div>
            </div>
            <div id="scrollbox3" class="timeline">
                <ul>
                    <asp:Repeater ID="Rptequeste" runat="server">
                        <ItemTemplate>
                            <li>
                                <div class="timeline-title gray">
                                    <h6>
                                        <asp:Label ID="lblto" runat="server" meta:resourcekey="LabelTo" Text=""></asp:Label>
                                        <%# Eval("toname")%>
                                    </h6>
                                    <i>
                                        <%# Eval("MinAgo")%>
                                                             
                                    </i>
                                    <i>
                                        <%# Eval("status")%>
                                    </i>
                                    <a href="OrgInvitation.aspx?View=<%# Eval("invId")%>&helptrackid=<%# Eval("invHelpTrackId")%>" title=""><i class="fa fa-comment-o"></i>
                                        <asp:Literal ID="Literal6" runat="server" meta:resourcekey="Open" EnableViewState="false" />
                                    </a>
                                </div>
                                <div class="timeline-content">
                                    <p>
                                        <%# Eval("invsubject")%>
                                    </p>
                                </div>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </div>
        </div>
    </div>
    <!-- TIME LINE -->
    <div class="col-md-6">
        <div class="timeline-sec widget-body" style="margin-top: 28px;">
            <div class="timeline-head yellow yellow-radius">
                <span style="line-height: 25px;"><%--Invitation(s) For Help--%>
                    <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Invitation_Help" EnableViewState="false" />
                    <i></i></span>
                <div class="add-btn">
                    <a href="OrgInvitation_request.aspx" title="" style="border-radius: 5px"><i class="fa fa-plus blue"></i><%--VIEW ALL --%>
                        <asp:Literal ID="Literal6" runat="server" meta:resourcekey="VIEW_ALL" EnableViewState="false" /></a>
                </div>

            </div>
            <div id="scrollbox3_new" class="timeline">
                <ul>
                    <asp:Repeater ID="RptINVITATION" runat="server">
                        <ItemTemplate>
                            <li>
                                <div class="timeline-title gray">
                                    <h6>
                                        <%--<asp:Label ID="lblto" runat="server"  meta:resourcekey="LabelTo" Text=""></asp:Label> <%# Eval("userFirstName")%>--%>
                                        <asp:Label ID="lblfrom" runat="server" meta:resourcekey="LabelFrom" Text=""></asp:Label>
                                        <%# Eval("userFirstName")%>
                                    </h6>
                                    <i>
                                        <%# Eval("MinAgo")%>
                                    </i><a href="<%# String.Format("OrgInvitation.aspx?id={0}", Eval("invId")) %>" title="">
                                        <i class="fa fa-comment-o"></i>
                                        <asp:Label ID="Label1" runat="server" meta:resourcekey="ProvideHelp"></asp:Label></a>
                                    <i>
                                        <%# Eval("status")%>
                                    </i>
                                </div>
                                <div class="timeline-content">
                                    <p>
                                        <%# Eval("invsubject")%>
                                    </p>
                                </div>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </div>
        </div>
    </div>

    <input type="hidden" id="txtvalue" value="<%=Session["Culture"] %>" />

    <asp:HiddenField ID="columnchartname" runat="server" Value="Column" meta:resourcekey="Column" />
    <asp:HiddenField ID="barchartname" runat="server" Value="BarChart" meta:resourcekey="BarChart" />
    <asp:HiddenField ID="spiderchartname" runat="server" Value="SpiderChart" meta:resourcekey="SpiderChart" />

    <%-- <div class="col-md-6">
        <div class="tdl-holder ">
            <div class="yellow" style="width: 100%; float: left;">
                <div class="" style="float: left; width: 36%;">
                    <h2 class="yellow" style="border: 0px;">
                        TO DO LIST
                    </h2>
                </div>
                <div style="float: right;">
                    <div id="reportrange1" class="pull-right" style="background: #fff; cursor: pointer;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           padding: 5px 10px; border: 1px solid #ccc">
                        <i class="fa fa-calendar icon-calendar icon-large"></i><span>August 5, 2014 - September
                            3, 2014</span> <b class="caret"></b>
                    </div>
                </div>
            </div>
            <asp:UpdatePanel runat="server" ID="UpdatePanellist" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="tdl-content">
                        <asp:LinkButton ID="LinkButton1" runat="server" class="" Style="height: 0px; width: 0px;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            float: right; text-align: center; line-height: 0px;" OnClick="LinkButton1_click">
                        </asp:LinkButton>
                        <asp:HiddenField ID="HiddenField1" runat="server" Value="0" />
                        <asp:Repeater ID="Repeaterlist" runat="server">
                            <HeaderTemplate>
                                <ul>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li>
                                    <div class="todo_checkbox">
                                       
    <asp:CheckBox ID="chklist" runat="server" AutoPostBack="True" TextAlign="Right" OnCheckedChanged="Check_Clicked"
        onclick="javascript:return CheckedChange(this)" Text='<%#Eval("listId")%>' CssClass="HiddenText" />
    </div>
    <div style="float: left;">
        <label>
            <i class="todo_i">
                <%# Eval("no")%></i><span><%# Eval("listsubject")%></span></label></div>
    </li> </ItemTemplate>
    <footertemplate>
                                </ul>
                            </footertemplate>
    </asp:Repeater> </div>
    <asp:TextBox ID="TextBox1" class="tdl-new" runat="server" placeholder="Write new item and hit 'Enter'..."
        AutoPostBack="true" OnTextChanged="TextBox1_TextChanged"></asp:TextBox>
    </ContentTemplate> </asp:UpdatePanel> </div> </div> --%>
    <!-- Recent Post -->
    <div class="col-md-3">
    </div>
    <!-- Twitter Widget -->
    <!-- Weather Widget -->
    <!---------------------------------------JoyRide >> Tip Content--------------------------------------------------------- -->
    <ol id="joyRideTipContent">
        <%--<li data-class="menu-profile" data-text="Next" class="custom" data-button="End Tour" data-options="tipLocation:right-middle">
        <h5>Welcome! Lets have a tour on Starteku.</h5>
        <p>You can control all the details for you tour stop.</p>
      </li>--%>
        <%-- <li data-class="heading-sec" data-button="stop" data-button="Already visited!" data-options="tipLocation:top;tipAnimation:fade">
        <h2>Stop #2</h2>
        <p>Get the details right by styling Joyride with a custom stylesheet!</p>
      </li>
      <li data-id="Head1" data-button="Next" data-text="Already visited!" data-options="tipLocation:right">
        <h2>Stop #3</h2>
        <p>It works right aligned.</p>
      </li>--%>
        <li data-button="Close" data-button="previous">
            <div runat="server" id="welcomeMsg"></div>
            <div class="tourTickbtn">
                <input id="chkTurnOffTour" onclick="joyrideEnd()" type="checkbox" title="" />
                <p style="margin-left: 15px; margin-top: -16px">
                    <asp:Literal ID="Literal8" runat="server" meta:resourcekey="TurnOffJoyRide" />
                </p>
            </div>
            <br />
            <br />
        </li>
        <%--<li data-button="Close">
            <h4>
                Congratulations <span class="WelComeUsername"></span>!
            </h4>
            <br />
            <p>
                You have now received :
            </p>
            <p>
                PLP points : <span class="plpSpan">0 </span>
            </p>
            <p>
                ALP points : <span class="alpSpan">0 </span>
            </p>
            <p>
                PDP points : <span class="pdpSpan">0 </span>
            </p>
            <p>
                Since your <span class="lastLogin">00:00 </span>!</p>
        </li>--%>
    </ol>
    <%--point welcom MESSAGE--%>
    <%--<ol id="welcomeWithPoint" style="display: none">
        <li data-button="Close" data-button="previous">
            <h4>
                Congratulations <span class="WelComeUsername"></span>!
            </h4>
            <br />
            <p>
                You have now received :
            </p>
            <p>
                PLP points : <span class="plpSpan">0 </span>
            </p>
            <p>
                ALP points : <span class="alpSpan">0 </span>
            </p>
            <p>
                PDP points : <span class="pdpSpan">0 </span>
            </p>
            <p>
                Since your <span class="lastLogin">00:00 </span>!</p>
            <div class="tourTickbtn">
            </div>
            <br />
            <br />
        </li>
       
    </ol>--%>
    <style>
        .tourTickbtn {
            margin-left: 78px;
            margin-top: 43px;
            position: absolute;
        }
    </style>

    <script src="../Scripts/flotJs.js" type="text/javascript"></script>
    <script src="../Scripts/chartCat.js" type="text/javascript"></script>
    <script src="js/chart-line-and-graph.js?v=2.5" type="text/javascript"></script>


    <script type="text/javascript">
        setTimeout(function () {
            $('.highcharts-contextbutton').on('click', function () {
                setTimeout(function () {
                    $(".highcharts-menu-item").each(function (a) {
                        if ($(this).html() == "Print chart") {
                            $(this).html($(this).html().replace('Print chart', $('#ContentPlaceHolder1_hdnPrintChart').val()));
                        } else if ($(this).html() == "Download PNG image") {
                            $(this).html($(this).html().replace('Download PNG image', $('#ContentPlaceHolder1_hdnDownloadPNG').val()));
                        }
                        else if ($(this).html() == "Download JPEG image") {
                            $(this).html($(this).html().replace('Download JPEG image', $('#ContentPlaceHolder1_hdnDownloadJPEG').val()));
                        }
                        else if ($(this).html() == "Download PDF document") {
                            $(this).html($(this).html().replace('Download PDF document', $('#ContentPlaceHolder1_hdnDownloadPDF').val()));
                        }
                        else if ($(this).html() == "Download SVG vector image") {
                            $(this).html($(this).html().replace('Download SVG vector image', $('#ContentPlaceHolder1_hdnDownloadSVG').val()));
                        }
                    });

                }, 500);
            });
        }, 2000);
    </script>

    <script type="text/javascript">

        function joyrideEnd() {


            var checkedValue = $('#chkTurnOffTour:checked').val();

            $.ajax({
                type: "POST",
                url: 'WebService1.asmx/JoyRideTourShowHide',
                data: "{'checkedValue':'" + checkedValue + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#divResult").html(msg.d);
                },
                error: function (e) {
                    $("#divResult").html("WebSerivce unreachable");
                }
            });

        }

        function JoyrideStart() {
            setUsername();
            $('#joyRideTipContent').joyride({
                autoStart: true,
                postStepCallback: function (index, tip) {
                    if (index == 2) {
                        $(this).joyride('set_li', false, 1);
                    }
                }, exposed: [],
                modal: true,
                expose: true,
                next_button: true,      // true or false to control whether a next button is used
                prev_button: true,
            });


        }

        function WelComeWithPoint() {

            setTimeout(function () {
                setUsername();
                $('#welcomeWithPoint').joyride({
                    autoStart: true,
                    postStepCallback: function (index, tip) {
                        if (index == 2) {
                            $(this).joyride('set_li', false, 1);
                        }
                    }, exposed: [],
                    modal: true,
                    expose: true,
                    next_button: true,      // true or false to control whether a next button is used
                    prev_button: true,
                });
            }, 1000);
        }

        function SubmitFrm(cn) {
            window.location = cn;
        }

        function CheckedChange(objCheckBox) {
            if (confirm('Are you sure you want to delete?')) {
                __doPostBack("'" + objCheckBox.id + "'", '');
                return true;
            }
            else {
                objCheckBox.checked = false;
                return false;
            }
        }

        function setUsername() {
            $(".WelComeUsername").text($("#span2").text());
            $(".plpSpan").text($("#lblPLP").text());
            $(".alpSpan").text($("#lblALP").text());
            $(".pdpSpan").text($("#lblpdp").text());
            $(".lastLogin").text($("#login").text());
        }
    </script>
    <script type="text/javascript">
        function SetChartType(a) {

            $('#hdnChartType').val(a);


        }
    </script>

    <script type="text/javascript">
        $(document).ready(function () {

            $('#ContentPlaceHolder1_lstFruits').multiselect({
                buttonText: function (options) {

                    if (options.length === 0) {
                        return $('#ContentPlaceHolder1_hdnNoOption').val();
                    }
                    var labels = [];
                    var num = [];

                    options.each(function () {
                        if ($(this).attr('value') !== undefined) {
                            labels.push($(this).text());
                            num.push($(this).val());

                        }
                    });
                    $('#ddlCompetence').val(num.join(','));

                    return labels.join(', ');
                },
                onDropdownHide: function (event) {
                    GetData("");


                },
                //enableFiltering: true,
                includeSelectAllOption: true,
                //buttonWidth: '400px',
                selectAllText: 'All Competence',
                //  //nonSelectedText: 'None Location selected',
                allSelectedText: 'All Competence',
                //  nSelectedText: 'selected',
                //  numberDisplayed: 2,


            });
            setTimeout(function () {
                $(".highcharts-menu-item").each(function (a) {

                    if ($(this).html() == "Print chart") {
                        $(this).html($(this).html().replace('Print chart', $('#ContentPlaceHolder1_hdnPrintChart').val()));
                    } else if ($(this).html() == "Download PNG image") {
                        $(this).html($(this).html().replace('Download PNG image', $('#ContentPlaceHolder1_hdnDownloadPNG').val()));
                    }
                    else if ($(this).html() == "Download JPEG image") {
                        $(this).html($(this).html().replace('Download JPEG image', $('#ContentPlaceHolder1_hdnDownloadJPEG').val()));
                    }
                    else if ($(this).html() == "Download PDF document") {
                        $(this).html($(this).html().replace('Download PDF document', $('#ContentPlaceHolder1_hdnDownloadPDF').val()));
                    }
                    else if ($(this).html() == "Download SVG vector image") {
                        $(this).html($(this).html().replace('Download SVG vector image', $('#ContentPlaceHolder1_hdnDownloadSVG').val()));
                    }
                });

            }, 1500);
        });
    </script>

</asp:Content>
