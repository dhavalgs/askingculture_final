﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using System.Configuration;
using System.Threading;
using System.Globalization;

public partial class Organisation_DocumentList : System.Web.UI.Page
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            Documents.InnerHtml = GetLocalResourceObject("Welcome.Text").ToString() + "    " + Convert.ToString(Session["OrgUserName"]) + " !";
            GetAllDocuments();
            GetAllDivisionByCompanyId();
            GetAllJobTypeByCompanyId();
        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion

    #region Methods
    /*----------------------GET Division BY Id ----------------------------------------------------------------------------------------------*/
    protected void GetAllDivisionByCompanyId()
    {
        DivisionBM obj = new DivisionBM();
        obj.depIsActive = true;
        obj.depIsDeleted = false;
        obj.depCompanyId = Convert.ToInt32(Session["OrgUserId"]);


        obj.GetAllDivisionsbyCompanyId();


        DataSet ds = obj.ds;
        ViewState["ds"] = ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                chkListDivision.DataSource = ds.Tables[0];
                chkListDivision.DataTextField = "divName";
                chkListDivision.DataValueField = "divId";
                chkListDivision.DataBind();

            }

        }
    }

    /*----------------------GET Jobtype BY Id ----------------------------------------------------------------------------------------------*/
    protected void GetAllJobTypeByCompanyId()
    {
        JobTypeBM obj = new JobTypeBM();
        obj.jobIsActive = true;
        obj.jobIsDeleted = false;
        obj.jobCompanyId = Convert.ToInt32(Session["OrgUserId"]);


        obj.GetAllJobTypeByCompanyId();


        DataSet ds = obj.ds;
        ViewState["ds"] = ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                chkListJobtype.DataSource = ds.Tables[0];
                chkListJobtype.DataTextField = "jobName";
                chkListJobtype.DataValueField = "jobId";
                chkListJobtype.DataBind();

            }

        }
    }

    public void GetAllDocuments()
    {
        try
        {
            DocumentBM obj = new DocumentBM();
            obj.docIsActive = true;
            obj.GetAllDocument();
            DataSet ds = obj.ds;

            DataView dv = new DataView();
            dv = ds.Tables[0].DefaultView;

            // dv.RowFilter = ("docUserId =  '" + Convert.ToInt32(Session["OrgUserId"]) + "'");

            dv.RowFilter = "(docUserId = '" + Convert.ToInt32(Session["OrgUserId"]) + "' or docIsPublic = 'True'  ) ";


            //dv.RowFilter = "(docUserId = '" + Convert.ToInt32(Session["OrgUserId"]) + "' or docIsPublic = 1  ) ";

            dv.RowFilter += "and (docFileName_Friendly LIKE '%" + Convert.ToString(txtsearch.Text.Trim()) + "%' OR docKeywords like '%" + Convert.ToString(txtsearch.Text.Trim()) + "%') ";
            // dv.RowFilter = string.Concat("userFullname LIKE '%", Convert.ToString(txtserch.Value), "%'");

            DataTable dtitm = dv.ToTable();
            DataSet ds1 = new DataSet();
            ds1.Tables.Add(dtitm);

            if (ds1.Tables[0].Rows.Count > 0)
            {
                rep_document.DataSource = ds1;
                rep_document.DataBind();
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void txtsearch_TextChanged(object sender, EventArgs e)
    {
        GetAllDocuments();
        //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>AnotherFunction();</script>", false);

    }
    #endregion
    private static string CleanFileName(string fileName)
    {


        var s =
            fileName.Replace(' ', '_')
                    .Replace('^', '_')
                    .Replace('%', '_')
                    .Replace('*', '_')
                    .Replace('(', '_')
                    .Replace(')', '_')
                    .Replace('{', '_')
                    .Replace('}', '_')
                    .Replace('@', '_')
                    .Replace('$', '_')
                    .Replace('+', '_')
                    .Replace('-', '_')
                    .Replace('`', '_')
                    .Replace('?', '_')
                    .Replace('&', '_')
                    .Replace('#', '_');

        return s;
    }
    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        String filePath = "";
        DocumentBM obj = new DocumentBM();

        var docFileNameFriendly = txtFileName.Text;
        var docAttachmentName = string.Empty;

        obj.docDescription = txtKeywords.Text;
       
        if (FileUpload1.HasFile)
        {
            filePath = System.DateTime.Now.ToString("MMddyyhhmmss") + "_" + FileUpload1.PostedFile.FileName;
            FileUpload1.SaveAs(Server.MapPath("../") + "Log/upload/Document/" + System.IO.Path.GetFileName(CleanFileName(filePath)));
            docAttachmentName = System.IO.Path.GetFileName(CleanFileName(filePath));

        }
        var docFileNameInSystem = CleanFileName(docAttachmentName);
        docAttachmentName = CleanFileName(docAttachmentName);
        const string docTypes = "pdf";
        var userId = Convert.ToInt32(Session["OrgUserId"]);
        
        var docCreatedDt = DateTime.Now;
        var docCompnyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.docDepId = 0;
        obj.docApprovedStatus = false;
        obj.doccompetence = txtKeywords.Text;
        
       
        DateTime docUpdatedDate = new DateTime();
        
        string docCreatedBy=string.Empty;
        int docDepIds = 0;
        string docTitle=txtFileName.Text;
        bool docApprovedStatus = false;

        //
        var aa = new List<string>();
        foreach (ListItem item in chkListDivision.Items)
        {
            if (item.Selected)
            {
                aa.Add(item.Value);
                //string selectedValue = item.Value;
            }
        }
        string divisionIds = string.Join(",", aa.ToArray());
        aa.Clear();
        //
        foreach (ListItem item in chkListJobtype.Items)
        {
            if (item.Selected)
            {
                aa.Add(item.Value);
                //string selectedValue = item.Value;
            }
        }
        string jobTypeIds = string.Join(",", aa.ToArray());


        var isPublic = rdoPrivatePublic.SelectedValue=="Public";

        var keywords = txtKeywords.Text;

        var repository = ddRepository.SelectedValue;

        var txtDescriptions = txtDescription.Text;

        obj.InsertDocument(docFileNameFriendly, docFileNameInSystem, docAttachmentName, docTypes, userId, docCreatedDt, docUpdatedDate, false, true, docCompnyId, docCreatedBy, docDepIds, docTitle, string.Empty, docApprovedStatus, divisionIds, jobTypeIds, isPublic, keywords, repository, txtDescriptions,0,0,"");
          
        if (obj.ReturnBoolean == true)
        {
            // Response.Redirect("OrgDepartmentsList.aspx?msg=ins");
        }
        GetAllDocuments();
        
        ClearField();
        InsertUpdateLog();
    }
    public  void InsertUpdateLog()
    {
        LogMasterLogic obj = new LogMasterLogic();
      obj.LogDescription = string.Format("{0} has uploaded {1} document ", Convert.ToString(Session["OrgUserName"]), rdoPrivatePublic.SelectedValue);   // User log description..

      var userID = Convert.ToInt32(Session["OrgUserId"]);

      var isPointAllow= rdoPrivatePublic.SelectedValue=="Public";

      var point = LogMasterLogic.InsertUpdateLogParam(userID, Common.PLP(), Common.PublicDocUpload(), obj.LogDescription, isPointAllow,0,0);

      if (isPointAllow)
          PointBM.InsertUpdatePoint(userID, point, 0);

    }
    private void ClearField()
    {
        txtFileName.Text = string.Empty;
        txtKeywords.Text = string.Empty;
        foreach (ListItem item in chkListDivision.Items)
        {
            item.Selected = false;
        }
        foreach (ListItem item in chkListJobtype.Items)
        {
            item.Selected = false;
        }
        ddRepository.SelectedIndex = 0;
        txtDescription.Text = string.Empty;
    }

    #endregion


    protected void btn_delete_Click(object sender, EventArgs e)
    {
        LinkButton lb = (LinkButton)sender;
        int id = Convert.ToInt32(lb.CommandName);
        DocumentBM obj = new DocumentBM();
        obj.docId = id;
        obj.DeleteDocumentById();
        GetAllDocuments();
    }

    #region Culture
    protected override void InitializeCulture()
    {
        //string language = "French";
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Denish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion

}