﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class Organisation_OrgQuestion : System.Web.UI.Page
{
    string id = "";
    string id1 = "";

    public static String GroupName
    {
        get
        {
            return "Test";
        }

    }

    #region Page Event
    DataTable dt = new DataTable();
    int queid = 0;
    DataSet ds;
    public static int ans = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }

        if (!String.IsNullOrEmpty(Request.QueryString["id"]))
        {
            GetAllCompetencebyid();
        }

        if (!IsPostBack)
        {
            Competence.InnerHtml = "Welcome   " + Convert.ToString(Session["OrgUserName"]) + "!";
            ViewState["catParentId"] = "0";
            ViewState["divParentId"] = "0";
            PopulateRootLevelDepartment();
            PopulateRootLevelDivision();

            set_data();
            GetAllJobtype();
            loadfirst();
        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion

    #region method
    protected void SetDefaultMessage()
    {

        string id = "?id=";
        string childurl = "child.aspx";
        if (Request.RawUrl.ToString().ToLower().Contains(childurl.ToLower()))
        {
            if (Request.RawUrl.ToString().ToLower().Contains(id.ToLower()))
            {
                lblDataDisplayTitle.Text = "Edit Children";

            }
            else
            {
                lblDataDisplayTitle.Text = "Add Children";
            }
        }
    }
    protected void GetAllJobtype()
    {
        JobTypeBM obj = new JobTypeBM();
        obj.jobIsActive = true;
        obj.jobIsDeleted = false;
        obj.jobCompanyId = Convert.ToInt32(Session["OrgUserId"]);
        obj.GetAllJobType();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddljobtype.Items.Clear();

                ddljobtype.DataSource = ds.Tables[0];
                ddljobtype.DataTextField = "jobName";
                ddljobtype.DataValueField = "jobId";
                ddljobtype.DataBind();

                ddljobtype.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
            }
            else
            {
                ddljobtype.Items.Clear();
                ddljobtype.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
            }
        }
        else
        {
            ddljobtype.Items.Clear();
            ddljobtype.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
        }

    }
    protected void InsertCompetence()
    {
        CompetenceBM obj = new CompetenceBM();
        obj.comCompetence = txtCompetence.Text;
        obj.comLevel = Convert.ToInt32(txtlevel.Text);
        obj.comJobId = Convert.ToInt32(ddljobtype.SelectedValue);
        obj.comCompanyId = Convert.ToInt32(Session["OrgUserId"]);
        obj.comIsActive = true;
        obj.comIsDeleted = false;
        obj.comCreatedDate = DateTime.Now;
        obj.comQuestion = txt_Question.Text;
        obj.comQuestionNo = Convert.ToInt32(ddlNumber.SelectedValue);
        obj.comDepartmentId = GETDepartment(TreeView1.Nodes);
        obj.comIndId = GETDivision(TreeView2.Nodes);
        //
        queid = obj.InsertCompetence();
        ViewState["queid"] = queid;
        save_Que_and_ans();
        if (queid > 0)
        {
            Response.Redirect("OrgCompetenceList.aspx?msg=ins");
        }

    }
    protected void updateCompetence()
    {
        CompetenceBM obj = new CompetenceBM();
        obj.comId = Convert.ToInt32(Request.QueryString["id"]);
        obj.comCompetence = txtCompetence.Text;
        obj.comLevel = Convert.ToInt32(txtlevel.Text);
        obj.comJobId = Convert.ToInt32(ddljobtype.SelectedValue);
        obj.comUpdatedDate = DateTime.Now;
        obj.comQuestion = txt_Question.Text;
        obj.comQuestionNo = Convert.ToInt32(ddlNumber.SelectedValue);
        obj.comDepartmentId = GETDepartment(TreeView1.Nodes);
        obj.comIndId = GETDivision(TreeView2.Nodes);
        obj.UpdateCompetence();
        DeleteAnswearsbyId(Convert.ToInt32(Request.QueryString["id"]));

        save_Que_and_ans();
        if (obj.ReturnBoolean == true)
        {
            Response.Redirect("OrgCompetenceList.aspx?msg=upd");
        }
    }

    protected void DeleteAnswearsbyId(int comId)
    {
        AnsBM obj = new AnsBM();
        obj.comId = comId;
        obj.DeleteAnswearsbyId();
    }

    protected void GetAllCompetencebyid()
    {
        ViewState["DepartmentId"] = "0";
        ViewState["comIndId"] = "0";
        CompetenceBM obj = new CompetenceBM();
        obj.comId = Convert.ToInt32(Request.QueryString["id"]);
        obj.GetAllCompetencebyid();
        ds = obj.ds;

        if (ds != null)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["comCompetence"])))
                txtCompetence.Text = Convert.ToString(ds.Tables[0].Rows[0]["comCompetence"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["comLevel"])))
                txtlevel.Text = Convert.ToString(ds.Tables[0].Rows[0]["comLevel"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["comJobId"])))
                ddljobtype.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["comJobId"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["comDepartmentId"])))
                ViewState["DepartmentId"] = Convert.ToString(ds.Tables[0].Rows[0]["comDepartmentId"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["comIndId"])))
                ViewState["comIndId"] = Convert.ToString(ds.Tables[0].Rows[0]["comIndId"]);



        }

    }

    protected void set_data()
    {
        if (ds != null)
        {
            txt_Question.Text = (ds.Tables[0].Rows[0]["comQuestion"]).ToString();
            ddlNumber.SelectedValue = (ds.Tables[0].Rows[0]["comQuestionNo"]).ToString();

            if (ds.Tables[1].Rows.Count > 1)
            {
                Repeater1.DataSource = ds.Tables[1];
                dt = ds.Tables[1];
                Repeater1.DataBind();
            }

            int index = 0;
            foreach (RepeaterItem item in Repeater1.Items)
            {
                TextBox TB = (TextBox)item.FindControl("TextBox1");
                if (Convert.ToBoolean(ds.Tables[1].Rows[index]["answerSelect"]) == true)
                {
                    ImageButton IB = (ImageButton)item.FindControl("Button1");
                    IB.ImageUrl = "../Organisation/img/true_check.png";
                }
                else
                {
                    ImageButton IB = (ImageButton)item.FindControl("Button1");
                    IB.ImageUrl = "../Organisation/img/false_check.png";
                }
                index++;
            }


        }
    }

    protected void save_Que_and_ans()
    {
        Boolean done = false;
        if (string.IsNullOrEmpty(Request.QueryString["id"]))
        {
            if (ViewState["queid"] != null)
                queid = Convert.ToInt32(ViewState["queid"]);
            else
                queid = 0;
        }
        else
        {
            queid = Convert.ToInt32(Request.QueryString["id"]);
        }


        foreach (RepeaterItem item in Repeater1.Items)
        {
            TextBox box = (TextBox)item.FindControl("TextBox1");
            ImageButton btn = (ImageButton)item.FindControl("Button1");

            //if (!string.IsNullOrEmpty(box.Text))
            //{
            AnsBM obj = new AnsBM();
            obj.aNo = queid;
            obj.ans = box.Text;
            if (default_index == Convert.ToInt32(btn.CommandName))
            {
                obj.answerSelect = true;
            }
            else
            {
                obj.answerSelect = false;
            }
            done = obj.insert_ans();
            //}
        }
    }
    protected void loadfirst()
    {
        if (!string.IsNullOrEmpty(Request.QueryString["id"]))
        {
            GetAllCompetencebyid();
            if (ds.Tables[1].Rows.Count > Convert.ToInt32(ddlNumber.SelectedValue))
            {
                int ans = ds.Tables[1].Rows.Count - Convert.ToInt32(ddlNumber.SelectedValue);
                for (int i = 0; i < ans; i++)
                {
                    ds.Tables[1].Rows.RemoveAt(ds.Tables[1].Rows.Count - 1);
                }

            }
            else
            {
                int ans = Convert.ToInt32(ddlNumber.SelectedValue) - ds.Tables[1].Rows.Count;
                for (int i = 0; i < ans; i++)
                {
                    DataRow dr = ds.Tables[1].NewRow();
                    dr["answer"] = " ";
                    ds.Tables[1].Rows.Add(dr);
                }
            }
            Repeater1.DataSource = ds.Tables[1];
            Repeater1.DataBind();
        }
        else
        {
            if (Repeater1.Controls.Count <= 0)
            {
                int val = Convert.ToInt32(ddlNumber.SelectedValue);
                dt.Columns.Add("answer", typeof(String));

                for (int i = 0; i < val; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr["answer"] = "";
                    dt.Rows.Add(dr);
                }

                Repeater1.DataSource = dt;
                Repeater1.DataBind();
            }
            else
            {
                if ((Repeater1.Controls.Count) < Convert.ToInt32(ddlNumber.SelectedValue))
                {
                    dt.Columns.Add("answer", typeof(String));
                    for (int i = 0; i < Repeater1.Controls.Count; i++)
                    {
                        TextBox box = (TextBox)Repeater1.Controls[i].FindControl("TextBox1");
                        RadioButton rbMyRadio = (RadioButton)Repeater1.Controls[i].FindControl("RadioButton1");

                        DataRow dr = dt.NewRow();
                        dr["answer"] = box.Text;
                        dt.Rows.Add(dr);
                    }
                    int add_new = Convert.ToInt32(ddlNumber.SelectedValue) - (Repeater1.Controls.Count);
                    for (int j = 0; j < add_new; j++)
                    {
                        DataRow dr = dt.NewRow();
                        dr["answer"] = "";
                        dt.Rows.Add(dr);
                    }
                }
                else
                {
                    dt.Columns.Add("answer", typeof(String));
                    for (int i = 0; i < Repeater1.Controls.Count; i++)
                    {
                        TextBox box = (TextBox)Repeater1.Controls[i].FindControl("TextBox1");
                        RadioButton rbMyRadio = (RadioButton)Repeater1.Controls[i].FindControl("RadioButton1");

                        DataRow dr = dt.NewRow();
                        dr["answer"] = box.Text;
                        dt.Rows.Add(dr);
                    }
                    int remove_new = (Repeater1.Controls.Count) - Convert.ToInt32(ddlNumber.SelectedValue);
                    for (int j = 0; j < remove_new; j++)
                    {
                        dt.Rows.RemoveAt(dt.Rows.Count - 1);
                    }
                }
                Repeater1.DataSource = dt;
                Repeater1.DataBind();
            }
        }
    }
    #endregion

    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Request.QueryString["id"]))
        {
            updateCompetence();
        }
        else
        {
            InsertCompetence();

        }

    }
    protected void btnCancel_click(object sender, EventArgs e)
    {
        Response.Redirect("OrgCompetenceList.aspx");
    }
    protected void txt_number_TextChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["id"]))
        {
            GetAllCompetencebyid();
            if (ds.Tables[1].Rows.Count > Convert.ToInt32(txt_Number.Text))
            {
                int ans = ds.Tables[1].Rows.Count - Convert.ToInt32(txt_Number.Text);
                for (int i = 0; i < ans; i++)
                {
                    ds.Tables[1].Rows.RemoveAt(ds.Tables[1].Rows.Count - 1);
                }

            }
            else
            {
                int ans = Convert.ToInt32(txt_Number.Text) - ds.Tables[1].Rows.Count;
                for (int i = 0; i < ans; i++)
                {
                    DataRow dr = ds.Tables[1].NewRow();
                    dr["answer"] = " ";
                    ds.Tables[1].Rows.Add(dr);
                }
            }
            Repeater1.DataSource = ds.Tables[1];
            Repeater1.DataBind();
        }
        else
        {
            if (Repeater1.Controls.Count <= 0)
            {
                int val = Convert.ToInt32(txt_Number.Text);
                dt.Columns.Add("answer", typeof(String));

                for (int i = 0; i < val; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr["answer"] = "";
                    dt.Rows.Add(dr);
                }

                Repeater1.DataSource = dt;
                Repeater1.DataBind();
            }
            else
            {
                if ((Repeater1.Controls.Count) < Convert.ToInt32(txt_Number.Text))
                {
                    dt.Columns.Add("answer", typeof(String));
                    for (int i = 0; i < Repeater1.Controls.Count; i++)
                    {
                        TextBox box = (TextBox)Repeater1.Controls[i].FindControl("TextBox1");
                        RadioButton rbMyRadio = (RadioButton)Repeater1.Controls[i].FindControl("RadioButton1");

                        DataRow dr = dt.NewRow();
                        dr["answer"] = box.Text;
                        dt.Rows.Add(dr);
                    }
                    int add_new = Convert.ToInt32(txt_Number.Text) - (Repeater1.Controls.Count);
                    for (int j = 0; j < add_new; j++)
                    {
                        DataRow dr = dt.NewRow();
                        dr["answer"] = "";
                        dt.Rows.Add(dr);
                    }
                }
                else
                {
                    dt.Columns.Add("answer", typeof(String));
                    for (int i = 0; i < Repeater1.Controls.Count; i++)
                    {
                        TextBox box = (TextBox)Repeater1.Controls[i].FindControl("TextBox1");
                        RadioButton rbMyRadio = (RadioButton)Repeater1.Controls[i].FindControl("RadioButton1");

                        DataRow dr = dt.NewRow();
                        dr["answer"] = box.Text;
                        dt.Rows.Add(dr);
                    }
                    int remove_new = (Repeater1.Controls.Count) - Convert.ToInt32(txt_Number.Text);
                    for (int j = 0; j < remove_new; j++)
                    {
                        dt.Rows.RemoveAt(dt.Rows.Count - 1);
                    }
                }
                Repeater1.DataSource = dt;
                Repeater1.DataBind();
            }
        }
    }
    protected void ddlNumber_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["id"]))
        {
            GetAllCompetencebyid();
            if (ds.Tables[1].Rows.Count > Convert.ToInt32(ddlNumber.SelectedValue))
            {
                int ans = ds.Tables[1].Rows.Count - Convert.ToInt32(ddlNumber.SelectedValue);
                for (int i = 0; i < ans; i++)
                {
                    ds.Tables[1].Rows.RemoveAt(ds.Tables[1].Rows.Count - 1);
                }

            }
            else
            {
                int ans = Convert.ToInt32(ddlNumber.SelectedValue) - ds.Tables[1].Rows.Count;
                for (int i = 0; i < ans; i++)
                {
                    DataRow dr = ds.Tables[1].NewRow();
                    dr["answer"] = " ";
                    ds.Tables[1].Rows.Add(dr);
                }
            }
            Repeater1.DataSource = ds.Tables[1];
            Repeater1.DataBind();
        }
        else
        {
            if (Repeater1.Controls.Count <= 0)
            {
                int val = Convert.ToInt32(ddlNumber.SelectedValue);
                dt.Columns.Add("answer", typeof(String));

                for (int i = 0; i < val; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr["answer"] = "";
                    dt.Rows.Add(dr);
                }

                Repeater1.DataSource = dt;
                Repeater1.DataBind();
            }
            else
            {
                if ((Repeater1.Controls.Count) < Convert.ToInt32(ddlNumber.SelectedValue))
                {
                    dt.Columns.Add("answer", typeof(String));
                    for (int i = 0; i < Repeater1.Controls.Count; i++)
                    {
                        TextBox box = (TextBox)Repeater1.Controls[i].FindControl("TextBox1");
                        RadioButton rbMyRadio = (RadioButton)Repeater1.Controls[i].FindControl("RadioButton1");

                        DataRow dr = dt.NewRow();
                        dr["answer"] = box.Text;
                        dt.Rows.Add(dr);
                    }
                    int add_new = Convert.ToInt32(ddlNumber.SelectedValue) - (Repeater1.Controls.Count);
                    for (int j = 0; j < add_new; j++)
                    {
                        DataRow dr = dt.NewRow();
                        dr["answer"] = "";
                        dt.Rows.Add(dr);
                    }
                }
                else
                {
                    dt.Columns.Add("answer", typeof(String));
                    for (int i = 0; i < Repeater1.Controls.Count; i++)
                    {
                        TextBox box = (TextBox)Repeater1.Controls[i].FindControl("TextBox1");
                        RadioButton rbMyRadio = (RadioButton)Repeater1.Controls[i].FindControl("RadioButton1");

                        DataRow dr = dt.NewRow();
                        dr["answer"] = box.Text;
                        dt.Rows.Add(dr);
                    }
                    int remove_new = (Repeater1.Controls.Count) - Convert.ToInt32(ddlNumber.SelectedValue);
                    for (int j = 0; j < remove_new; j++)
                    {
                        dt.Rows.RemoveAt(dt.Rows.Count - 1);
                    }
                }
                Repeater1.DataSource = dt;
                Repeater1.DataBind();
            }
        }
    }
    #endregion

 

    #region TreeView
    private void FillDepartmentChecklist(TreeNodeCollection nodes, int id)
    {

        if (String.IsNullOrEmpty(Request.QueryString["id"]))
        {
            foreach (TreeNode child in nodes)
            {

                if (Convert.ToString(id) == Convert.ToString(child.Value))
                {
                    child.Checked = true;

                }

                FillDepartmentChecklist(child.ChildNodes, id);
            }
        }
        else
        {
            String DepartmentId = Convert.ToString(ViewState["DepartmentId"]);
            string[] splittedString = DepartmentId.Split(',');
            foreach (TreeNode child in nodes)
            {
                int x = nodes.Count;
                foreach (string sp in splittedString)
                {
                    if (Convert.ToString(sp) == Convert.ToString(child.Value))
                    {
                        child.Checked = true;
                    }
                    FillDepartmentChecklist(child.ChildNodes, 0);
                }
            }
        }

    }
    protected void TreeView_TreeNodePopulateDepartment(object sender, TreeNodeEventArgs e)
    {
        int ParentCatagoryID = Int32.Parse(e.Node.Value);
        PopulateSubLevelDepartment(ParentCatagoryID, e.Node);
        
        FillDepartmentChecklist(TreeView1.Nodes, Convert.ToInt32(ViewState["catParentId"]));

    }
    private void PopulateSubLevelDepartment(int ParentCatagoryID, TreeNode parentNode)
    {
       

        DepartmentsBM obj = new DepartmentsBM();
        obj.depPerentId = ParentCatagoryID;
        obj.depCompanyId = Convert.ToInt32(Session["OrgUserId"]);
        obj.GetDepartmentsFromParentId();
        DataSet ds = obj.ds;
        PopulateNodesDepartment(ds.Tables[0], parentNode.ChildNodes);

    }
    private void PopulateNodesDepartment(DataTable dt, TreeNodeCollection nodes)
    {
        foreach (DataRow dr in dt.Rows)
        {
            TreeNode tn = new TreeNode();
            tn.Value = dr["depId"].ToString();
            tn.Text = dr["depName"].ToString();
            nodes.Add(tn);
            //If node has child nodes, then enable on-demand populating
            tn.PopulateOnDemand = ((int)(dr["catChildCount"]) > 0);


        }
    }
    private void PopulateRootLevelDepartment()
    {

        DepartmentsBM obj = new DepartmentsBM();
        obj.depPerentId = 0;
        obj.depCompanyId = Convert.ToInt32(Session["OrgUserId"]);
        obj.GetDepartmentsFromParentId();
        DataSet ds = obj.ds;
        PopulateNodesDepartment(ds.Tables[0], TreeView1.Nodes);

    }
    private String GETDepartment(TreeNodeCollection treeNode)
    {
        foreach (TreeNode child in treeNode)
        {
            if (child.Checked == true)
            {
                String val = Convert.ToString(child.Value);
                if (!string.IsNullOrEmpty(id))
                {
                    id = id + Convert.ToString(child.Value) + ",";
                }
                else
                {
                    id = "," + Convert.ToString(child.Value) + ",";
                }

            }
            GETDepartment(child.ChildNodes);
        }
        return id;

    }



    private void FillDivisionChecklist(TreeNodeCollection nodes, int id)
    {
        if (String.IsNullOrEmpty(Request.QueryString["id"]))
        {
            foreach (TreeNode child in nodes)
            {
                if (Convert.ToString(id) == Convert.ToString(child.Value))
                {
                    child.Checked = true;

                }
                FillDivisionChecklist(child.ChildNodes, id);
            }
        }
        else
        {
            String IndId = Convert.ToString(ViewState["comIndId"]);
            string[] splittedString = IndId.Split(',');
            foreach (TreeNode child in nodes)
            {
                int x = nodes.Count;
                foreach (string sp in splittedString)
                {
                    if (Convert.ToString(sp) == Convert.ToString(child.Value))
                    {
                        child.Checked = true;
                    }
                    FillDivisionChecklist(child.ChildNodes, 0);
                }
            }
        }

    }
    protected void TreeView_TreeNodePopulateDivision(object sender, TreeNodeEventArgs e)
    {
        int ParentCatagoryID = Int32.Parse(e.Node.Value);
        PopulateSubLevelDivision(ParentCatagoryID, e.Node);
        FillDivisionChecklist(TreeView2.Nodes, Convert.ToInt32(ViewState["divParentId"]));

        
    }
    private void PopulateSubLevelDivision(int ParentCatagoryID, TreeNode parentNode)
    {
        //Your sublevel Datatable ie. dtSub
        DivisionBM obj = new DivisionBM();
        obj.depPerentId = ParentCatagoryID;
        obj.depCompanyId = Convert.ToInt32(Session["OrgUserId"]);
        obj.GetDivisionsFromParentId();
        DataSet ds = obj.ds;
        PopulateNodesDivision(ds.Tables[0], parentNode.ChildNodes);


    }
    private void PopulateNodesDivision(DataTable dt, TreeNodeCollection nodes)
    {
        foreach (DataRow dr in dt.Rows)
        {
            TreeNode tn = new TreeNode();
            tn.Value = dr["divId"].ToString();
            tn.Text = dr["divName"].ToString();
            nodes.Add(tn);
            tn.PopulateOnDemand = ((int)(dr["catChildCount"]) > 0);


        }
    }
    private void PopulateRootLevelDivision()
    {
        DivisionBM obj = new DivisionBM();
        obj.depPerentId = 0;
        obj.depCompanyId = Convert.ToInt32(Session["OrgUserId"]);
        obj.GetDivisionsFromParentId();
        DataSet ds = obj.ds;
        PopulateNodesDivision(ds.Tables[0], TreeView2.Nodes);

    }
    private String GETDivision(TreeNodeCollection treeNode)
    {
        foreach (TreeNode child in treeNode)
        {
            if (child.Checked == true)
            {
                String val = Convert.ToString(child.Value);
                if (!string.IsNullOrEmpty(id1))
                {
                    id1 = id1 + Convert.ToString(child.Value) + ",";
                }
                else
                {
                    id1 = "," + Convert.ToString(child.Value) + ",";
                }

            }
            GETDivision(child.ChildNodes);
        }
        return id1;

    }
    #endregion

    protected static int default_index = 0;

    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        foreach (RepeaterItem item in Repeater1.Items)
        {
            ImageButton rb = (ImageButton)item.FindControl("Button1");
            rb.ImageUrl = "../Organisation/img/false_check.png";
            string val = rb.CommandName;
        }
        ImageButton rb2 = (ImageButton)e.Item.FindControl("Button1");
        rb2.ImageUrl = "../Organisation/img/true_check.png";
        default_index = Convert.ToInt32(rb2.CommandName);

    }
}