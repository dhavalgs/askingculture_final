﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Organisation_Login" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Log in </title>
    <%--<link href='../../fonts.googleapis.com/css_ac5e1c55.css' rel='stylesheet' type='text/css' />
    <link href='../../fonts.googleapis.com/css_3e3d9b9e.css' rel='stylesheet' type='text/css' />
    <link href='../../fonts.googleapis.com/css_98ee0297.css' rel='stylesheet' type='text/css' />
    <link href='../../fonts.googleapis.com/css_7f7d8532.css' rel='stylesheet' type='text/css' />
    <link href='../../fonts.googleapis.com/css_52a9019e.css' rel='stylesheet' type='text/css' />--%>
    <link rel="stylesheet" href="font-awesome-4.0.3/css/font-awesome.css" type="text/css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <!-- Style -->
    <link rel="stylesheet" href="css/responsive.css" type="text/css" />
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <script src="../bootstrap/js/bootstrap.js" type="text/javascript"></script>
    <!-- Responsive -->
    <%--<script type="text/javascript">
        $(document).ready(function () {
            "use strict";
            Login.init(); // Init login JavaScript
        });
        
    </script>--%>
    <style type="text/css">
        .sign-in-form form input[type=submit]
        {
            /* background: -moz-linear-gradient(center top, #F3AF32 5%, #F3AF32 100%) repeat scroll 0% 0% #F3AF32;
           -webkit-background: -moz-linear-gradient(center top, #F3AF32 5%, #F3AF32 100%) repeat scroll 0% 0% #F3AF32;*/
            background: -moz-linear-gradient(center top, #F3AF32 5%, #F3AF32 100%) repeat scroll 0% 0% #F3AF32;
           
           -webkit-background-color: #F3AF32;
           -moz-background-color: #F3AF32;
           -ms-background-color: #F3AF32;
            -o-background-color: #F3AF32;           
            background-color: #F3AF32;
          
          
          
            border-radius: 4px;
            text-indent: 0px;
            border: 1px solid #F3AF32;
            display: inline-block;
            color: #FFF;
            font-family: Arial;
            font-size: 15px;
            font-weight: bold;
            font-style: normal;
            height: 45px;
            line-height: 40px;
            width: 250px;
            text-decoration: none;
            text-align: center;
            text-shadow: 0px 0px 0px #001A33;
            margin-left: 45px;
        }
        .sign-in-form form input[type="text"], input[type="password"]
        {
            background: none repeat scroll 0px 0px #D9DDE9;
            border: medium none;
            border-radius: 0px 3px 3px 0px;
            color: #666;
            float: left; /*font-family: pt sans;*/
            font-size: 15px;
            height: 48px;
            letter-spacing: 0.3px;
            padding-left: 20px;
            width: 237px;
        }


        .LogoProfile {
        
        }
    </style>
    <script src="../Scripts/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
    
    <script src="../assets/assets/js/libs/jquery-1.10.2.min.js"></script>
    <script type="text/javascript">
        var iCount = 20;
        function incrementProgressbar() {
            if ($('form')[0].checkValidity()) {
                if ($("#txtUsername").val() != "" && $("#txtPassword").val() != "") {
                    //$(".progress1").show();
                    $(".progress").fadeIn();
                    setInterval(function() {
                        iCount = iCount + 4;
                        $(".progress-bar").css("width", iCount + "%");
                        if (iCount > 100) {
                            iCount = 0;
                        }
                    }, 800);
                }
            }
        }
    </script>
</head>
<body class="sign-in-bg" style="background: #0b5d99;">
    <div class="sign-in">
        <div class="sign-in-head yellow" style="border: none; height: 120px; border-radius: 5px 5px 0 0;">
            <div class="sign-in-details">
                <%--<h1 style="line-height: 70px;">
                  
                    <asp:Literal ID="Literal9" runat="server" meta:resourcekey="LogIn" enableviewstate="false"/>
                    <i class="fa fa-lock"></i>
                </h1>--%>
               <%-- <span>or <%--Sign up 
                  <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Signup" enableviewstate="false"/></span>--%>
            </div>
            <%--<div class="log-in-thumb" style="margin-left: 26%;float: unset;">--%>
            <div class="log-in-thumb" style="float: unset; margin-left: 20%; height: 80%; width: 70%;">
                    <asp:Image runat="server" ID="imgLoginLogo" AlternateText="logo" style="position: relative; text-align: center; z-index: 1; float: left; width: 215px; height: 80px;"></asp:Image>
                <%--<img src="images/strateku_centreret_white.png" alt="" style="width:150px;height:85px;"/><br />--%>
            </div>
        </div>
        <div class="sign-in-form">
            <h5>
            </h5>
            <form id="Form1" runat="server">
            <div style="float: left; margin-bottom: 20px; width: 100%;">
                <i class="fa fa-user"></i>
                <asp:TextBox ID="txtUsername" type="text" placeholder="USER NAME" runat="server"
                    MaxLength="100" TabIndex="1" meta:resourcekey="txtUsernameResource1" />
                <asp:RequiredFieldValidator ID="rfvUsername" runat="server" CssClass="commonerrormsg"
                    Style="margin-left: 20px;" ControlToValidate="txtUsername" Display="Dynamic"
                    ValidationGroup="chk" ErrorMessage="Please enter your username." 
                    meta:resourcekey="rfvUsernameResource1"></asp:RequiredFieldValidator>
            </div>
            <div style="float: left; margin-bottom: -10px; width: 100%;">
                <i class="fa fa-lock"></i>
                <asp:TextBox ID="txtPassword" type="text" runat="server" MaxLength="20" TextMode="Password"
                    placeholder="PASSWORD" TabIndex="2" 
                    meta:resourcekey="txtPasswordResource1" /><br />
                <asp:RequiredFieldValidator ID="rfvPassword" runat="server" CssClass="commonerrormsg"
                    Style="margin-left: 20px;" ControlToValidate="txtPassword" ErrorMessage="Please enter your password."
                    Display="Dynamic" ValidationGroup="chk" 
                    meta:resourcekey="rfvPasswordResource1"></asp:RequiredFieldValidator><br />
                <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" Style="margin-left: 20px;" meta:resourcekey="lblMsgResource1"></asp:Label>
            </div>
            <div style="clear: both">
            </div>
                 <div class="progress progress-striped active process" style="display: none;margin-top: 15px">
                        <div class="progress-bar progress-bar-warning" style="width: 10%">Please wait...</div>
                    </div>
            <div class="checkbox pull-left">
                <%--<asp:CheckBox runat="server" ID="cbRememberme" CssClass="uniform" Text="Remember me" Width="50px"/>--%>
                <label class="checkbox pull-left" style="font-family: Arial;color:Gray;font-size:15px;margin-top:15px;">
                    <asp:CheckBox runat="server" ID="cbRememberme" CssClass="uniform" 
                    Style="margin-left: 18px" meta:resourcekey="cbRemembermeResource1" />
                    <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Rememberme" enableviewstate="false"/></label>
            </div>
            <h5 style="font-family: Arial;color:Gray;">
                <a href="forget_password.aspx" title=""><%--Forgot your password--%> <asp:Literal ID="Literal2" runat="server" meta:resourcekey="Forgotyourpassword" enableviewstate="false"/>? </a>
            </h5>
            <div>
                <asp:Button runat="server" ID="btnLogin" Text=" LOG IN " CssClass="btn_submit" Style="background-color: #F3AF32 !important;
                    font-size: 18px; border: 0px;" OnClick="btnLogin_Click" ValidationGroup="chk" OnClientClick="incrementProgressbar();"
                    TabIndex="3" meta:resourcekey="btnLoginResource1" />
                <%--<asp:LinkButton runat="server" ID="btnLogin" Text=" LOG IN" CssClass="black"
                    OnClick="btnLogin_Click" ValidationGroup="chk" >
                    <div style="width:300px;height:30px;">
                    </div>
                    </asp:LinkButton>--%>
                <%--<a href="" title="" class="black" OnClick="btnLogin_Click">LOG IN </a> --%>
            </div>
                 <h5 style="font-family: Arial;color:#d0d0d0">
                <a href="#" title="" style="color:#d0d0d0">Version 15.11.04.00 </a>
            </h5>
            </form>
        </div>
        <ul>
            <li><a href="#" title=""><i class="fa fa-facebook"></i></a></li>
            <li><a href="#" title=""><i class="fa fa-twitter"></i></a></li>
            <li><a href="#" title=""><i class="fa fa-google-plus"></i></a></li>
        </ul>
    </div>
</body>

    <script>
        $(document).ready(function () {
        
            eraseCookie("CollapseClick");
            eraseCookie("CollapseClickSubmenu");
        });
        function eraseCookie(name) {
            createCookie(name, "", -1);
        }

        function createCookie(name, value, days) {
            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                var expires = "; expires=" + date.toGMTString();
            }
            else var expires = "";

            document.cookie = name + "=" + value + expires + "; path=/";
        }

        function readCookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        }


        </script>
</html>
