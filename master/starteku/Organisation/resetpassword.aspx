﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="resetpassword.aspx.cs" Inherits="Organisation_resetpassword" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Log in </title>


    <link href='../../fonts.googleapis.com/css_ac5e1c55.css' rel='stylesheet' type='text/css' />
    <link href='../../fonts.googleapis.com/css_3e3d9b9e.css' rel='stylesheet' type='text/css' />
    <link href='../../fonts.googleapis.com/css_98ee0297.css' rel='stylesheet' type='text/css' />
    <link href='../../fonts.googleapis.com/css_7f7d8532.css' rel='stylesheet' type='text/css' />
    <link href='../../fonts.googleapis.com/css_52a9019e.css' rel='stylesheet' type='text/css' />

    <!-- Styles -->
    <link rel="stylesheet" href="font-awesome-4.0.3/css/font-awesome.css" type="text/css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <!-- Style -->
    <link rel="stylesheet" href="css/responsive.css" type="text/css" />
    <!-- Responsive -->
    <script type="text/javascript">
        $(document).ready(function () {
            "use strict";
            Login.init(); // Init login JavaScript
        });
    </script>
    <style type="text/css">
        .sign-in-form form input[type=submit] {
            box-shadow: 0px 2px 0px 0px #3E4C5C inset;
            background: -moz-linear-gradient(center top, #3C4752 5%, #2F3640 100%) repeat scroll 0% 0% #3C4752;
            border-radius: 4px;
            border: 1px solid #323E4F;
            display: inline-block;
            color: #FFF;
            font-family: Arial;
            font-size: 15px;
            font-weight: bold;
            font-style: normal;
            height: 45px;
            line-height: 20px;
            width: 320px;
            text-decoration: none;
            text-align: center;
            margin-left: 20px;
            margin-top:10px;
        }

        .sign-in-form form input[type="text"], input[type="password"] {
            background: none repeat scroll 0px 0px #D9DDE9;
            border: medium none;
            border-radius: 0px 3px 3px 0px;
            color: #666;
            float: left;
            font-family: pt sans;
            font-size: 15px;
            height: 45px;
            letter-spacing: 0.3px;
            padding-left: 20px;
            width: 300px;
            margin: 20px;
            margin-top:10px;
        }
    </style>
</head>


<body class="sign-in-bg" style="background: #0b5d99;">
    <div class="sign-in">
        <div class="sign-in-head yellow" style="border: none;">
            <div class="sign-in-details" style="width: 300px;">
                <h1>Reset Password?<i class="fa fa-lock"></i></h1>
            </div>
            <p></p>
        </div>
        <div class="sign-in-form">
            <form id="Form2" runat="server">
                <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" placeholder="Enter Password"
                    MaxLength="200"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvPassword" runat="server" CssClass="commonerrormsg"
                    ControlToValidate="txtPassword" Display="Dynamic" ValidationGroup="chk" ErrorMessage="Password is required."></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revPassword" runat="server" ControlToValidate="txtpassword"
                    ValidationExpression="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,8}$" CssClass="commonerrormsg"
                    Display="Dynamic" ValidationGroup="chk" ErrorMessage="
Password must be at least 4 characters, no more than 8 characters, and must include at least one upper case letter, one lower case letter, and one numeric digit" />

                <%--<---------->--%>
                <asp:TextBox ID="txtConfirmPassword" runat="server" CssClass="form-control" placeholder="Enter ConfirmPassword"
                    MaxLength="200"></asp:TextBox>
                <asp:CompareValidator ID="cvPasswordNotMatch" ControlToValidate="txtConfirmPassword"
                    ControlToCompare="txtPassword" runat="server" CssClass="commonerrormsg" Display="Dynamic"
                    ValidationGroup="chk" ErrorMessage="Password and confirm password does not match."></asp:CompareValidator>
                <asp:RequiredFieldValidator ID="rfvConfirmPassword" runat="server" CssClass="commonerrormsg"
                    ControlToValidate="txtConfirmPassword" Display="Dynamic" ErrorMessage="Confirm password is required."
                    ValidationGroup="chk"></asp:RequiredFieldValidator>
                <asp:Button runat="server" ID="btnsubmit" Text="Submit" CssClass="submit btn btn-default btn-block"
                    ValidationGroup="chk" OnClick="btnForgot_ServerClick" />
                <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg"></asp:Label>
            </form>
        </div>
        <ul>
            <li><a href="#" title=""><i class="fa fa-facebook"></i></a></li>
            <li><a href="#" title=""><i class="fa fa-twitter"></i></a></li>
            <li><a href="#" title=""><i class="fa fa-google-plus"></i></a></li>
        </ul>
    </div>
</body>
</html>
