﻿<%@ Page Title="AskingCulture" Language="C#" MasterPageFile="~/Organisation/Employee.master" AutoEventWireup="true" CodeFile="ActivityRequestView.aspx.cs" Inherits="Organisation_ActivityRequestView" %>


<%--Desing and maintain by JAINAM SHAH 20-3-2017--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <style type="text/css">
        .table > thead > tr > th {
            vertical-align: middle;
        }
    </style>
    <style type="text/css">
        body {
            font-family: Arial;
            font-size: 11pt;
        }

        .modalBackground {
            background-color: Black;
            filter: alpha(opacity=40);
            opacity: 0.4;
        }

        .modalPopup1 {
            background-color: #FFFFFF;
            width: 300px;
            border: 3px solid #0DA9D0;
        }

        .modalPopup {
            background: none repeat scroll 0 0 #ffffff;
            display: table;
            float: none;
            margin: 96px auto;
            width: 600px !important;
            top: 10px !important;
        }

            .modalPopup .header {
                background-color: #2FBDF1;
                height: 30px;
                color: White;
                line-height: 30px;
                text-align: center;
                font-weight: bold;
            }

            .modalPopup .body {
                min-height: 50px;
                line-height: 30px;
                text-align: center;
                padding: 5px;
            }

            .modalPopup .footer {
                padding: 3px;
            }

            .modalPopup .button {
                height: 23px;
                color: White;
                line-height: 23px;
                text-align: center;
                font-weight: bold;
                cursor: pointer;
                background-color: #9F9F9F;
                border: 1px solid #5C5C5C;
            }

            .modalPopup td {
                text-align: left;
            }

        .button {
            background-color: transparent;
        }

        .yellow {
            border-radius: 0px;
        }

        .reply {
            clear: both;
        }

        #scrollbox6 {
            overflow-x: hidden;
            overflow-y: auto;
            background: #f4f4f4;
        }

        #txtCmt {
            overflow-x: hidden;
            overflow-y: auto;
            background: #f4f4f4;
            height: 315px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

     <asp:HiddenField runat="server" ID="hdnLoginUserID" Value="0" />
       <asp:HiddenField runat="server" ID="hdnUserType" Value="0" />
      <asp:HiddenField ID="RequestSentSuccessful" meta:resourcekey="RequestSentSuccess" runat="server" />
    <div class="col-md-6">
        <div class="heading-sec">
            <h1 style="margin-left: 15px;">
                <%-- <%= CommonMessages.Viewrequest%>--%>
                <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Viewrequest" EnableViewState="false" />
                <i><span runat="server" id="Employee"></span>
                </i>
            </h1>
        </div>
    </div>
    <div class="chat-widget widget-body" style="background: #fff;">
    </div>

    <div class="col-md-12" style="margin-top: -20px; width: 100%;">
        <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg"
            ></asp:Label>
    </div>
    <div class="col-md-12" style="margin-top: 20px; width: 100%;">
        <%--<div id="graph-wrapper">--%>
        <div class="chat-widget-head yellow" style="width: 97.5%; margin-left: 15px; margin-bottom: -16px;">
            <h4 style="margin: -6px 0px 0px;">
                <span runat="server" id="spname"></span>'s  <%--<%= CommonMessages.Competencelevel%>--%><asp:Literal ID="Literal2" runat="server" meta:resourcekey="Activity_Requests" EnableViewState="false" />
            </h4>
        </div>
        <div class="col-md-12">

            <div class="chat-widget widget-body" style="margin-top: 10px;">
                <div class="" style="padding: 3px;">
                    <div class="col-md-12" style="clear: both">
                        <div class="form-group" style="clear: both">
                            <div id="errorDiv" runat="server" style="clear: both" visible="false">
                                <div class="col-md-3" style="padding-top: 10px;">
                                    <asp:Label runat="server" ID="Label17" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                        <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Error" EnableViewState="false" />
                                    </asp:Label>
                                </div>
                                <div class="col-md-9">

                                    <asp:Label runat="server" ID="txtError" ForeColor="red" />
                                    <%--meta:resourcekey="txttitleResource1"--%>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate=""
                                        ErrorMessage="Please enter activity name." CssClass="commonerrormsg" Display="Dynamic"
                                        ValidationGroup="chkdoc"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                        <br />

                        <div class="col-md-12">

                            <div class="col-md-4">


                                <div class="row">
                                    <div class="col-md-5">
                                        <asp:HiddenField runat="server" ID="hdnActId" />
                                        <asp:HiddenField ID="hdnAcrCreaterId" runat="server" />
                                        <asp:Label runat="server" ID="Label5" CssClass="" Style="color: black; font-weight: bold;">
                                            <asp:Literal ID="Literal4" runat="server" meta:resourcekey="Activity_Type" EnableViewState="false" />

                                        </asp:Label>
                                    </div>
                                    <div class="col-md-7">
                                        <asp:DropDownList ID="ddActCate" runat="server" CssClass="form-control"
                                            RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description"
                                            DataValueField="value" Enabled="False" Width="100%">
                                        </asp:DropDownList>
                                    </div>

                                </div>


                                <br />

                                <div class="row">
                                    <div class="col-md-5">
                                        <asp:Label runat="server" ID="Label1" CssClass="" Style="color: black; font-weight: bold; margin-bottom: 4px;">
                                            <asp:Literal ID="Literal5" runat="server" meta:resourcekey="Activity_Name" EnableViewState="false" />

                                        </asp:Label>
                                    </div>
                                    <div class="col-md-7">
                                        <asp:TextBox runat="server" ID="txtActName"  CssClass="form-control" MaxLength="50" ReadOnly="True" Width="100%" Enabled="False" class="form-control" />
                                        <%--meta:resourcekey="txttitleResource1"--%>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtActName"
                                            ErrorMessage="Please enter activity name." CssClass="commonerrormsg" Display="Dynamic"
                                            ValidationGroup="chkdoc"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <br />


                                <div class="row">
                                    <div class="col-md-5">
                                        <asp:Label runat="server" ID="Label2" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                            <asp:Literal ID="Literal6" runat="server" meta:resourcekey="Description" EnableViewState="false" />
                                        </asp:Label>
                                    </div>
                                    <div class="col-md-7">
                                        <asp:TextBox runat="server" ID="txtActDescription"  CssClass="form-control" TextMode="MultiLine" MaxLength="50" ReadOnly="True" Enabled="False" />
                                        <%--meta:resourcekey="txttitleResource1"--%>
                                    </div>
                                </div>

                                <br />
                                <div class="row">
                                    <div class="col-md-5">

                                        <asp:Label runat="server" ID="Label6" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                            <asp:Literal ID="Literal7" runat="server" meta:resourcekey="Requirement" EnableViewState="false" />
                                        </asp:Label>
                                    </div>
                                    <div class="col-md-7">
                                        <asp:TextBox runat="server"  CssClass="form-control" ID="txtActRequirements" MaxLength="50" Width="100%" ReadOnly="True" Enabled="False" />
                                        <%--meta:resourcekey="txttitleResource1"--%>
                                    </div>
                                </div>



                                <div class="row">
                                    <div class="col-md-5">

                                        <asp:Label runat="server" ID="Label7" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                            <asp:Literal ID="Literal8" runat="server" meta:resourcekey="Tags" EnableViewState="false" Visible="False" />
                                        </asp:Label>
                                    </div>
                                    <div class="col-md-7">
                                        <asp:TextBox  CssClass="form-control" runat="server" ID="txtActTags" MaxLength="50" Width="100%" ReadOnly="True" Enabled="False" Visible="False" />
                                        <%--meta:resourcekey="txttitleResource1"--%>
                                    </div>
                                </div>


                                <br />



                                <div class="row">
                                    <div class="col-md-5">
                                        <asp:Label runat="server" ID="Label8" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                            <asp:Literal ID="Literal9" runat="server" meta:resourcekey="Start_Date" EnableViewState="false" />
                                        </asp:Label>
                                    </div>
                                    <div class="col-md-7">
                                        <asp:TextBox runat="server" ID="txtActStartDate"
                                             CssClass="form-control" Width="100%"  MaxLength="50" ReadOnly="True" Enabled="False" />
                                        <%--meta:resourcekey="txttitleResource1"--%>
                                    </div>
                                </div>

                                <br />



                                <div class="row">
                                    <div class="col-md-5">
                                        <asp:Label runat="server" ID="Label9" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                            <asp:Literal ID="Literal10" runat="server" meta:resourcekey="End_Date" EnableViewState="false" />
                                        </asp:Label>
                                    </div>
                                    <div class="col-md-7">
                                        <asp:TextBox runat="server" ID="txtActEndDate"
                                             Width="100%" CssClass="txtActEndDate form-control" MaxLength="50" ReadOnly="True" Enabled="False" />
                                        <%--meta:resourcekey="txttitleResource1"--%>
                                    </div>
                                </div>

                                <br />


                                <div class="row">
                                    <div class="col-md-5">
                                        <asp:Label runat="server" ID="Label10" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                            <asp:Literal ID="Literal11" runat="server" meta:resourcekey="Cost" EnableViewState="false" />
                                        </asp:Label>
                                    </div>
                                    <div class="col-md-7">
                                        <asp:TextBox runat="server" ID="txtActCost" MaxLength="50" Width="100%" type="number"
                                            CssClass="form-control"
                                             onkeypress="return isNumber(event,this);" ReadOnly="True" Enabled="False" />
                                        <%--meta:resourcekey="txttitleResource1"--%>
                                    </div>
                                </div>


                            </div>

                            <div class="col-md-4">
                                <div class="row" style="display: none">
                                    <div class="col-md-6">
                                        <asp:Label runat="server" ID="Label12" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                             <asp:Literal ID="Literal18" runat="server" meta:resourcekey="Enabled" EnableViewState="false" />
                                        </asp:Label>
                                    </div>
                                    <div class="col-md-6">
                                        <asp:RadioButtonList runat="server" ID="rdoEnabled" RepeatDirection="Horizontal" CssClass="inline-rb" TextAlign="Right" Enabled="False">
                                            <asp:ListItem Text="Yes" Value="true" />
                                            <asp:ListItem Text="No" Value="false" Selected="True" />
                                        </asp:RadioButtonList>
                                        <%--meta:resourcekey="txttitleResource1"--%>
                                    </div>
                                </div>



                                <div class="row" style="display: none">
                                    <div class="col-md-6">
                                        <asp:Label runat="server" ID="Label13" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                             <asp:Literal ID="Literal19" runat="server" meta:resourcekey="Public" EnableViewState="false" />
                                            
                                        </asp:Label>
                                    </div>
                                    <div class="col-md-6">
                                        <div>
                                            <asp:RadioButtonList runat="server" ID="rdoPublic" RepeatDirection="Horizontal" CssClass="inline-rb" TextAlign="Right" Enabled="False">
                                                <asp:ListItem Text="Yes" Value="true" />
                                                <asp:ListItem Text="No" Value="false" Selected="True" />
                                            </asp:RadioButtonList>
                                        </div>
                                        <div class="ddpDivision col-md- " style="display: none">
                                            <asp:DropDownList Width="250px" ID="chkListJobtype" runat="server" CssClass="chkliststyle form-control"
                                                RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description"
                                                DataValueField="value" Style="width: 100%; overflow: auto;" Enabled="False">
                                            </asp:DropDownList>

                                        </div>
                                        <div class="ddpDivision col-md- " style="display: none">
                                            <asp:DropDownList Width="250px" ID="chkListDivision" runat="server" CssClass="chkliststyle form-control"
                                                RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description"
                                                DataValueField="value" Style="width: 100%; overflow: auto;" Enabled="False">
                                            </asp:DropDownList>
                                        </div>
                                        <%--meta:resourcekey="txttitleResource1"--%>
                                    </div>
                                </div>




                                <div class="row">
                                    <div class="col-md-6">
                                        <asp:Label runat="server" ID="Label14" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                             <asp:Literal ID="Literal20" runat="server" meta:resourcekey="Comp_Development" EnableViewState="false" />
                                            
                                        </asp:Label>
                                    </div>
                                    <div class="col-md-6">

                                        <asp:RadioButtonList Width="100%" runat="server" ID="chkIsCompDevDoc" RepeatDirection="Horizontal" CssClass="inline-rb" TextAlign="Right" Enabled="False">
                                            <asp:ListItem Text="Yes" Value="true" />
                                            <asp:ListItem Text="No" Value="false" Selected="True" />
                                        </asp:RadioButtonList>
                                    </div>
                                </div>

                                <br />

                                <div class="row">
                                    <div class="col-md-4">
                                        <asp:Label runat="server" ID="Label11" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                             <asp:Literal ID="Literal21" runat="server" meta:resourcekey="Competence" EnableViewState="false" />
                                            
                                        </asp:Label>
                                    </div>
                                    <div class="col-md-8">

                                        <asp:DropDownList Width="250px" ID="ddCompetence" runat="server" CssClass="chkliststyle form-control"
                                            RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description"
                                            DataValueField="value" Style="width: 100%; overflow: auto;" Enabled="False">
                                        </asp:DropDownList>

                                    </div>
                                </div>

                                <br />


                                <div class="row">
                                    <div class="col-md-2">
                                        <asp:Label runat="server" ID="Label15" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                             <asp:Literal ID="Literal22" runat="server" meta:resourcekey="Level" EnableViewState="false" />
                                            
                                        </asp:Label>
                                    </div>
                                    <div class="col-md-10">

                                        <asp:RadioButtonList runat="server" ID="rdoLevel" RepeatDirection="Horizontal" RepeatLayout="Table" CssClass="inline-rb form-control" TextAlign="Right" Enabled="False" Width="100%">
                                            <asp:ListItem Text="1 to 2" Value="1 to 2" meta:resourcekey="OneToTwoResource" Selected="True">1 to 2</asp:ListItem>
                                            <asp:ListItem Text="2 to 3" Value="2 to 3" meta:resourcekey="TwoToThreeResource">2 to 3</asp:ListItem>
                                            <asp:ListItem Text="3 to 4" Value="3 to 4" meta:resourcekey="ThreeToFourResource">3 to 4</asp:ListItem>
                                            <asp:ListItem Text="4 to 5" Value="4 to 5" meta:resourcekey="FourToFiveResource">4 to 5</asp:ListItem>
                                        </asp:RadioButtonList><br />
                                    </div>
                                </div>





                                <div class="row">
                                    <div class="col-md-6">
                                        <asp:Label runat="server" ID="Label16" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                            <asp:Literal ID="Literal12" runat="server" Text="Justification" meta:resourcekey="Justification" EnableViewState="false" />
                                        </asp:Label>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-12">
                                        <asp:TextBox runat="server" ID="txtJustification" CssClass="form-control" MaxLength="450" Width="100%" />
                                        <%--meta:resourcekey="txttitleResource1"--%>
                                    </div>
                                </div>


                                <br />


                                <div class="row">
                                    <div class="col-md-12">
                                        <asp:Label runat="server" ID="Label18" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                            <asp:Literal ID="Literal13" runat="server" meta:resourcekey="Expected_Behaviour_Change" EnableViewState="false" />
                                        </asp:Label>
                                    </div>
                                </div>
                                <br />




                                <div class="row">
                                    <div class="col-md-12">
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txtExpctBehavChange" TextMode="MultiLine" ReadOnly="False" Enabled="True" Width="100%" />
                                        <%--meta:resourcekey="txttitleResource1"--%>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-4">

                                <div class="row">
                                    <div class="col-md-12">
                                        <asp:Label runat="server" ID="Label4" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                            <asp:Literal ID="Literal14" runat="server" Text="Comments:" EnableViewState="false" meta:resourcekey="Comments"/>
                                        </asp:Label>
                                    </div>
                                </div>
                                <br />

                                <div class="row">

                                    <div id="txtCmtDiv">
                                        <ul id="txtCmt">
                                            <asp:Literal ID="Literal15" runat="server" Text="Record not found" EnableViewState="false" meta:resourcekey="RecordNotfoundResource" />
                                            <li class="reply"> </li>
                                        </ul>
                                        <div class="reply-sec">
                                        <div class="reply-sec-MESSAGE" style="margin-left: 20px;">

                                          <%--  <input type="text" placeholder="TYPE YOUR COMMENT HERE" id="Text2" class="txtcomment" style="padding: 10px;" />--%>


                                              <asp:TextBox ID="txtComments" align="right" runat="server" CssClass="txtcomment" AutoPostBack="false" placeholder="TYPE YOUR COMMENT HERE" Text=""
                                        MaxLength="1" TextMode="MultiLine" Style="float: left;  width: 295px; height: 48px;"
                                        Enabled="true"></asp:TextBox>
                                            

                                            <a href="#" title="" class="black" onclick="insertcomment(true)"><i class="fa fa-comments-o"></i></a>
                                        </div>
                                    </div>
                                    </div>
                                    <asp:TextBox ID="txtComment" align="right" runat="server" AutoPostBack="false" Text=""
                                        MaxLength="1" TextMode="MultiLine" Style="float: left; margin-left: 50px; width: 295px; height: 196px;display: none"
                                        Enabled="false"></asp:TextBox><br />

                                    <a href="#" id='btnChat' onclick="return GetActivityCommentById()" data-index="txtComment" class="black" style="display: none; float: left; margin-left: 310px; margin-top: 8px; padding-left: 10px; padding-top: 10px; height: 35px; width: 35px;"><i class="fa fa-comments-o"></i></a>
                                    <%----%>
                                    <asp:HiddenField ID="hdnActReqIdInt" runat="Server" Value="This is the Value of Hidden field" />
                                    <asp:HiddenField ID="hdnActReqUserId" runat="Server" Value="This is the Value of Hidden field" />


                                </div>






                                <%--meta:resourcekey="txttitleResource1"--%>
                            </div>

                            <asp:LinkButton ID="lnkFake" runat="server" />
                            <cc1:ModalPopupExtender ID="mpe" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkFake"
                                CancelControlID="btnClose" BackgroundCssClass="modalBackground" DynamicServicePath=""
                                Enabled="True">
                            </cc1:ModalPopupExtender>
                            <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup modal-dialog" Style="top: 0px !important;">
                                <div class="modal-header blue yellow-radius" style="color: #ffffff;">
                                    <asp:Label runat="server" ID="Label3" CssClass="modal-title" Style="font-size: 135%;" meta:resourcekey="Comments"></asp:Label><%--<h4 class="modal-title">Comments</h4>--%><div style="float: right; margin-top: -10px;">
                                        <asp:Button ID="btnClose" runat="server" Text="×" CssClass="close" />
                                    </div>
                                </div>
                                <div class="modal-body">
                                    <ul id="scrollbox6">
                                        <li class="reply">
                                             <asp:Literal ID="Literal16" runat="server" Text="Record not found" EnableViewState="false" meta:resourcekey="RecordNotfoundResource" />
                                        </li>
                                    </ul>
                                    <div class="reply-sec">
                                        <div class="reply-sec-MESSAGE" style="margin-left: 20px;">

                                            <input type="text" placeholder="TYPE YOUR COMMENT HERE" id="Text1" class="txtcomment" style="padding: 10px;" />



                                            <a href="#" title="" class="black" onclick="insertcomment()"><i class="fa fa-comments-o"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
















                        <br />
                        <br />
                        <div class="chat-widget widget-body" style="background: #fff;">
                            <%--<div class="col-md-6" style="width: 100%;float:right;">
                            <div class="inline-form" style="width: 100%;">
                                 <label class="c-label" >
                                    Comment:</label>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" ID="txtComment"></asp:TextBox>
                               
                               
                            </div>
                        </div>--%>

                            <asp:HiddenField runat="server" ID="hdnStatus12" />
                            <div class="col-md-6" style="width: 100%; float: right;">
                                <div class="inline-form" style="width: 100%; float: right;">
                                    <asp:DropDownList ID="ddlsttus" runat="server" Style="width: 142px; height: 32px; display: inline; float: right;"
                                        meta:resourcekey="ddlsttusResource1">
                                       
                                    </asp:DropDownList><label class="c-label" style="width: 7%; float: right;">
                                         <asp:Literal ID="Literal17" runat="server" Text="Status" EnableViewState="false" meta:resourcekey="Status" />:
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12" id="submit" runat="server" style="margin: 10px; padding: 10px; width: 98%;">
                    <asp:Button runat="server" ID="btnCancel" Text="Cancel" CssClass="btn black pull-right"
                        ValidationGroup="chk" CausesValidation="False" OnClick="btnCancel_click"
                        Style="color: white;" meta:resourcekey="btnCancel" />
                    <asp:Button runat="server" ID="btnsubmit" Text="Save" CssClass="btn blue pull-right"
                        ValidationGroup="chk" OnClick="btnsubmit_click" Style="color: white;"
                        meta:resourcekey="btnsubmitResource1" />
                    <asp:Button runat="server" ID="btnback" Text="Back" CssClass="btn black pull-right"
                        CausesValidation="False" OnClick="btnback_click" Visible="False"
                        Style="color: white;" meta:resourcekey="btnback" />
                </div>
            </div>
        </div>


    </div>
    <script src="../assets/assets/js/libs/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            GetActivityCommentById(true);
        });
        function insertcomment(isFirstTime) {

            var message = $(".txtcomment").val();
            var ActReqId = $('#ContentPlaceHolder1_hdnActReqIdInt').val();
            var ActID = $('#ContentPlaceHolder1_hdnActId').val();
            var ActReqUserId = $('#ContentPlaceHolder1_hdnActReqUserId').val();

            if (ActReqId != '0') {
                if (message != '') {

                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",

                        //url: "View_request.aspx/insertmessage",

                        url: "ActivityRequestView.aspx/insertmessage",
                        data: "{'message':'" + message + "','ActReqId':'" + ActReqId + "','ActReqUserId':'" + ActReqUserId + "','ActID':'" + ActID + "'}",
                        //data: "{'message':'" + message + "','ActReqId':'" + ActReqId + ",'ActReqUserId':'" + ActReqUserId + "'}",
                        dataType: "json",
                        success: function (data) {
                            var obj = data.d;
                            if (obj == 'true') {

                                $(".txtcomment").val("");
                                // document.getElementById("txtcomment").value = '';
                                // GetmessgeById(touserid);
                                //GetmessgeById_afterinsert(touserid)
                                GetActivityCommentById(isFirstTime);

                                setTimeout(function () {
                                    $("#scrollbox6").scrollTop(99999999999999999999);
                                }, 122);
                                $("#" + aId).prev().prev().text(message); // copy text and paste on parent page's comment box
                            }
                        },
                        error: function (result) {
                            // alert("Error");
                        }
                    });
                }
                else {
                    generate("warning", "Oops! You have missed text to send a massage.", "bottomCenter");
                    return false;
                }
            }
            else {
                generate("warning", "Please Select Contact", "bottomCenter");

                document.getElementById("message").value = '';
                return false;
            }
        }


        var aId;
        var html = "";
        function GetActivityCommentById(isFirstTime) {
            var umId = 0;
            var urlParms = new URLSearchParams(window.location.search); //specfic text
            umId = urlParms.get('actReqId');
            //javascript e hdn file value lavani
            var AcrCreaterId = $('#ContentPlaceHolder1_hdnActReqIdInt').val();
            var ActID = $('#ContentPlaceHolder1_hdnActId').val();
            var LoginUserID = $('#ContentPlaceHolder1_hdnLoginUserID').val();
            var UserType = $('#ContentPlaceHolder1_hdnUserType').val();
            debugger;
            $("#scrollbox6").scrollTop(99999999999999999999);
            $("#txtCmt").scrollTop(99999999999999999999);
            //var comid = document.getElementById("hdncom").value;
            html = "";
            $("#scrollbox6").html('');
            $("#txtCmt").html('');

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "ActivityRequestView.aspx/GetActivityCommentById",
                data: "{'AcrCreaterId':'" + AcrCreaterId + "','ActID':'" + ActID + "','LoginUserID':'" + LoginUserID + "','UserType':'" + UserType + "'}",
                dataType: "json",
                success: function (data) {

                    //$("#hdncom").val(a);

                    cat = data.d;

                    for (var i = 0; i < data.d.length; i++) {
                        debugger;
                        var cls = "reply";

                        html += "<li class='" + cls + "'><div class='chat-thumb'><img src='../Log/upload/Userimage/" + data.d[i].userImage + "' alt=''/></div>";
                        html += "<div class='chat-desc'><p>" + data.d[i].AcrCompComm + "</p><i class='chat-time'>" + parseJsonDate(data.d[i].AcrComCreateDate) + "</i>";
                        html += "</div></li>";
                    }

                    if (data.d.length == 0) {
                        html += "No records found.";
                    }

                    
                    $("#scrollbox6").append(html);
                    $("#txtCmt").append(html);
                    var modalPopup = $find('<%=mpe.ClientID %>');
                    debugger;
                    if (modalPopup != null) {
                        if (isFirstTime==true) {
                            
                        } else {
                            modalPopup.show();
                        }

                    }
                    $("#scrollbox6").scrollTop(99999999999999999999);
                    $("#txtCmt").scrollTop(99999999999999999999);
                },



                error: function (result) {
                    //alert("Error");

                }
            });
        }

        function parseJsonDate(jsonDateString) {


            //return new Date(parseInt(jsonDateString.replace('/Date(', '')));

            var formattedDate = new Date(parseInt(jsonDateString.substr(6)));
            var d = formattedDate.getDate();
            var m = formattedDate.getMonth();
            m += 1; // JavaScript months are 0-11
            var y = formattedDate.getFullYear();
            var min = formattedDate.getMinutes();
            var hour = formattedDate.getHours();
            var sec = formattedDate.getSeconds();

            //$("#txtDate").val(d + "." + m + "." + y);
            return d + "/" + m + "/" + y + " " + hour + ":" + min + ":" + sec;
        }

        function formatDate(d) {
            if (hasTime(d)) {
                var s = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
                s += ' ' + d.getHours() + ':' + zeroFill(d.getMinutes()) + ':' + zeroFill(d.getSeconds());
            } else {
                var s = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
            }

            return s;
        }
    </script>
</asp:Content>
