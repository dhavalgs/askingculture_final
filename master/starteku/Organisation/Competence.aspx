﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true" CodeFile="Competence.aspx.cs" Inherits="Organisation_Competence" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" 
        meta:resourcekey="lblMsgResource1"></asp:Label>
    <div class="col-md-6">
        <div class="heading-sec">
            <h1>
                <%= CommonMessages.Competence%><i><span runat="server" id="Competence"></span></i>
            </h1>
        </div>
    </div>
    <div class="col-md-12" style="margin-top: 20px;">
        <div id="graph-wrapper">
            <div class="col-md-12">
                <asp:Button runat="server" ID="Button1" PostBackUrl="Registration.aspx?id=1" Text="Add employee"
                    CssClass="btn btn-primary yellow" Visible="False" 
                    meta:resourcekey="Button1Resource1" />
                <a href="#add-post-title" data-toggle="modal" title="">
                    <button type="button" class="btns  yellow  lrg-btn flat-btn add_user" style="border: 0px;">
                        <%= CommonMessages.AddNew%> <%= CommonMessages.Competence%></button></a>
                <br />
                <br />
                <br />
                <div class="chart-tab">
                    <div id="tabs-container">
                        <asp:GridView ID="gvGrid" runat="server" AutoGenerateColumns="False" CellPadding="0"
                            Width="100%" GridLines="None" DataKeyNames="comId" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                            EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>' OnRowCommand="gvGrid_RowCommand"
                            BackColor="White" OnRowDataBound="gvGrid_RowDataBound" 
                            meta:resourcekey="gvGridResource1">
                            <HeaderStyle CssClass="aa" />
                            <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:TemplateField HeaderText="ID" meta:resourcekey="TemplateFieldResource1">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" BorderWidth="0px" Font-Size="16px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="COMPETENCE" 
                                    meta:resourcekey="TemplateFieldResource2">
                                    <ItemTemplate>
                                        <asp:Label ID="lblcomCompetence" runat="server" 
                                            Text="<%# bind('comCompetence') %>" 
                                            meta:resourcekey="lblcomCompetenceResource1"></asp:Label>
                                        <asp:HiddenField ID="comId" runat="server" Value="<%# bind('comCompanyId') %>" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" BorderWidth="0px" Font-Size="16px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="" ItemStyle-Width="50px" 
                                    meta:resourcekey="TemplateFieldResource3">
                                    <ItemTemplate>
                                        <div class="vat" style="width: 50%;" id="divsub" runat="server">
                                            <p>
                                                <a href="<%# String.Format("SubCompetence.aspx?id={0}", Eval("comId")) %>" title="Edit">
                                                   <%= CommonMessages.AddSubCompetence%></a>
                                            </p>
                                        </div>
                                        <div class="vat" style="width: 20%;" id="divEdit" runat="server">
                                            <p>
                                                <i class="fa fa-pencil"></i>
                                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="View" CommandArgument='<%# Eval("comId") %>'
                                                    ToolTip="Edit" meta:resourcekey="LinkButton2Resource1">Edit</asp:LinkButton>
                                            </p>
                                        </div>
                                        <div class="total" style="width: 20%;" id="divDeletet" runat="server">
                                            <p>
                                                <i class="fa fa-trash-o"></i>
                                                 <asp:Label ID="hdnConfirmArchive" style="display:none;"   CssClass="abcde" meta:resourcekey="ConfirmArchive" runat="server" />
                                                <asp:LinkButton ID="lnkBtnName" CssClass="def" runat="server" CommandName="archive" CommandArgument='<%# Eval("comId") %>'
                                                    ToolTip="Delete"        meta:resourcekey="lnkBtnNameResource1">Delete</asp:LinkButton>
                                            </p>
                                        </div>
                                        <div class="total" style="width: 100%;" id="div1" runat="server">
                                            <p>
                                                <i>
                                                    <img src="images/block.jpg" alt="" height="15px;" width="15px;" /></i>  <%= CommonMessages.NotEditable%>
                                            </p>
                                        </div>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" BorderWidth="0px" Font-Size="16px" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div aria-hidden="true" role="dialog" tabindex="-1" class="modal fade" id="add-post-title"
        style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header blue" style="border-radius: 0px;">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                        ×
                    </button>
                    <h4 class="modal-title">
                         <%= CommonMessages.AddNew%> <%= CommonMessages.Competence%></h4>
                </div>
                <div class="modal-body">
                    <div>
                        <asp:TextBox runat="server" placeholder="Name" ID="txtName" MaxLength="50" 
                            meta:resourcekey="txtNameResource1" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtName"
                            ErrorMessage="Please enter Name." CssClass="commonerrormsg" Display="Dynamic"
                            ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator5Resource1"></asp:RequiredFieldValidator>
                    </div>

                    <div>
                        <asp:TextBox runat="server" placeholder="Name" ID="txtNameDN" MaxLength="50" 
                            meta:resourcekey="txtNameDNResource1" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtNameDN"
                            ErrorMessage="Please enter Name." CssClass="commonerrormsg" Display="Dynamic"
                            ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator3Resource1"></asp:RequiredFieldValidator>
                    </div>
                    <div>
                        <asp:DropDownList ID="ddljobtype" runat="server" Style="width: 100%; height: 32px;
                            display: inline;" CssClass="form-control" 
                            meta:resourcekey="ddljobtypeResource1">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="ddljobtype"
                            ErrorMessage="Please select Job Type." InitialValue="0" CssClass="commonerrormsg"
                            Display="Dynamic" ValidationGroup="chk" 
                            meta:resourcekey="RequiredFieldValidator14Resource1"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:Button runat="server" ID="btnsubmit" Text="Submit" CssClass="btn btn-primary yellow"
                        ValidationGroup="chk" OnClick="btnsubmit_click" 
                        Style="border-radius: 5px;" meta:resourcekey="btnsubmitResource1" />
                    <button data-dismiss="modal" class="btn btn-default black" type="button">
                       <%= CommonMessages.Close%>
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    <asp:LinkButton ID="lnkFake" runat="server" 
        meta:resourcekey="lnkFakeResource1" />
    <cc1:ModalPopupExtender ID="mpe" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkFake"
        CancelControlID="btnClose" BackgroundCssClass="modalBackground" 
        DynamicServicePath="" Enabled="True">
    </cc1:ModalPopupExtender>
    <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" 
        meta:resourcekey="pnlPopupResource1">
        <div class="modal-header blue" style="color: #ffffff;">
            <h4 class="modal-title">
                Edit  <%= CommonMessages.Competence%></h4>
            <div style="float: right; margin-top: -26px;">
                <asp:Button ID="btnClose" runat="server" Text="×" CssClass="close" 
                    meta:resourcekey="btnCloseResource1" />
            </div>
        </div>
        <div class="modal-body">
            <table border="0" cellpadding="3" cellspacing="0">
                <tr>
                    <td style="width: 80px; padding-bottom: 25px;">
                        <b> <%= CommonMessages.Name%>: </b>
                    </td>
                    <td>
                        <asp:TextBox runat="server" placeholder="Name" ID="txtcname" MaxLength="50" 
                            meta:resourcekey="txtcnameResource1" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtcname"
                            ErrorMessage="Please enter Name." CssClass="commonerrormsg" Display="Dynamic"
                            ValidationGroup="chhk" meta:resourcekey="RequiredFieldValidator1Resource1"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="width: 80px; padding-bottom: 25px;">
                        <b>job Type: </b>
                    </td>
                    <td style="width: 380px;">
                        <asp:DropDownList ID="ddljob" runat="server" Style="width: 100%; height: 32px; display: inline;
                            margin-top: -16px;" CssClass="form-control" 
                            meta:resourcekey="ddljobResource1">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddljob"
                            ErrorMessage="Please select Job Type." InitialValue="0" CssClass="commonerrormsg"
                            Display="Dynamic" ValidationGroup="chhk" 
                            meta:resourcekey="RequiredFieldValidator2Resource1"></asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>
        </div>
        <div class="footer" align="right">
            <asp:Button runat="server" ID="btnupdate" Text="Submit" CssClass="btn btn-primary yellow"
                ValidationGroup="chhk" OnClick="btnupdate_click" 
                Style="border-radius: 5px;" meta:resourcekey="btnupdateResource1" />
            <asp:Button runat="server" ID="Button3" Text="Close" 
                CssClass="btn btn-primary black" meta:resourcekey="Button3Resource1" />
        </div>
    </asp:Panel>
    <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 10pt;
        }
        .modalBackground
        {
            background-color: Black;
            filter: alpha(opacity=40);
            opacity: 0.4;
        }
        .modalPopup1
        {
            background-color: #FFFFFF;
            width: 300px;
            border: 3px solid #0DA9D0;
        }
        .modalPopup
        {
            background: none repeat scroll 0 0 #ffffff;
            display: table;
            float: none;
            margin: 96px auto;
            width: 606px;
            top: 60px !important;
        }
        .modalPopup .header
        {
            background-color: #2FBDF1;
            height: 30px;
            color: White;
            line-height: 30px;
            text-align: center;
            font-weight: bold;
        }
        .modalPopup .body
        {
            min-height: 50px;
            line-height: 30px;
            text-align: center;
            padding: 5px;
        }
        .modalPopup .footer
        {
            padding: 3px;
        }
        .modalPopup .button
        {
            height: 23px;
            color: White;
            line-height: 23px;
            text-align: center;
            font-weight: bold;
            cursor: pointer;
            background-color: #9F9F9F;
            border: 1px solid #5C5C5C;
        }
        .modalPopup td
        {
            text-align: left;
        }
        
        .button
        {
            background-color: transparent;
        }
        .yellow
        {
            border-radius: 0px;
        }
    </style>
      <script type="text/javascript" src="https://code.jquery.com/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            xyz();
        });

        function xyz() {

            $('.def').attr("onclick", "return confirm('" + $(".abcde").text() + "')");
        }



    </script>
</asp:Content>
