﻿<%@ Page Title="AskingCulture" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master" AutoEventWireup="true" meta:resourcekey="PageResource1"
    CodeFile="Question.aspx.cs" Inherits="Organisation_Question" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .tabs-menu li {
            width: 33.33%;
        }

        .tab {
            padding-top: 0px !important;
        }

        .tab-content {
            padding: 0px !important;
        }

        .ItemStyle {
            padding-left: 2%;
        }

        .dropdown-menu > li > a {
            display: block;
            padding: 8px 65px !important;
            padding-bottom: 8px;
            clear: both;
            font-weight: normal;
            line-height: 1.428571429;
            color: #333;
            white-space: nowrap;
        }

        .radio input[type="radio"], .radio-inline input[type="radio"], .checkbox input[type="checkbox"], .checkbox-inline input[type="checkbox"] {
            float: left;
            margin-left: -80px !important;
        }

        .radiobuttonlist {
            font: 12px Verdana, sans-serif;
            color: #000; /* non selected color */
        }

        .inline-rb input[type="radio"] {
            width: auto;
            margin-right: 10px;
            margin-left: 20px;
        }

        .inline-rb label {
            display: inline;
        }

        .publishButtonStyle {
            margin-bottom: 2%;
            margin-top: 2%;
        }
    </style>
    <style type="text/css">
        .radio input[type="radio"], .radio-inline input[type="radio"], .checkbox input[type="checkbox"], .checkbox-inline input[type="checkbox"] {
            margin-left: -20px !important;
            width: auto;
        }

        .dropdown-menu > li > a {
            padding: 8px 20px !important;
        }

        .multiselect.dropdown-toggle.btn.btn-default {
            max-width: 560px;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
        }
    </style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="../Scripts/pickadate.js-3.5.6/lib/themes/classic.css" rel="stylesheet" />
    <link href="../Scripts/fullcalendor/pickADate/classic.date.css" rel="stylesheet" />


    <%--joyride---------------------------------------------%>
    <link href="../plugins/joyride/joyride-2.1.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/multiselect.css" rel="stylesheet" />
    <link href="../Styles/UpdateCustom.css" rel="stylesheet" type="text/css" />
    <script src="../plugins/joyride/jquery.cookie.js" type="text/javascript"></script>
    <script src="../plugins/joyride/jquery.joyride-2.1.js" type="text/javascript"></script>
    <script src="../plugins/joyride/modernizr.mq.js" type="text/javascript"></script>

    <script src="../plugins/highchart/highcharts.js" type="text/javascript"></script>


    <script src="../plugins/highchart/highcharts-more.js" type="text/javascript"></script>
    <script src="../Scripts/plugins/exporting.js" type="text/javascript"></script>
    <script src="../Scripts/jquery321.js" type="text/javascript"></script>
    <%--    <script src="../Scripts/jquery_1_8_3.min.js" type="text/javascript"></script>--%>

    <script src="../assets/js/multiselect.js" type="text/javascript"></script>

    <div class="col-md-6">
        <div class="heading-sec">
            <h1 style="margin-left: 15px;">
                <asp:Literal ID="Literal32" runat="server" meta:resourcekey="QuestionTitleRes" EnableViewState="false" />
                <i>
                    <span>
                        <asp:Literal ID="Literal33" runat="server" meta:resourcekey="Welcome" EnableViewState="false" /></span>
                </i>
            </h1>
        </div>
    </div>
    <div class="col-md-12" style="margin-top: 20px;">
        <asp:HiddenField ID="hdnSelectQuesCat" runat="server" Value="Select Question Category" meta:resourcekey="SelectQuesCatResource" />
        <asp:HiddenField ID="hdnSelectQues" runat="server" Value="Select Question" meta:resourcekey="SelectQuesResource" />
        <asp:HiddenField ID="hdnSelectQuesTemplate" runat="server" Value="Select Template" meta:resourcekey="SelectQuesTemplateResource" />

        <asp:HiddenField ID="hdnSelectQuesTemplate1" runat="server" Value="Select Template" meta:resourcekey="SelectQuesTemplateResource1" />

        <asp:HiddenField ID="hdnSelectTeam" runat="server" Value="All Team" meta:resourcekey="SelectAllTeamResource" />
        <asp:HiddenField ID="hdnSelectUser" runat="server" Value="Select User" meta:resourcekey="SelectUserResource" />
        <asp:HiddenField ID="hdnAll" runat="server" Value="All" meta:resourcekey="AllRes" />

        <asp:HiddenField ID="hdnSelectedTemplateIDs" runat="server" Value="0" />
        <asp:HiddenField ID="hdnSelectedQuestionIDs" runat="server" Value="0" />

        <asp:HiddenField ID="hdnSelectedTeamIDs" runat="server" Value="0" />
        <asp:HiddenField ID="hdnSelectedUserIDs" runat="server" Value="0" />

        <asp:HiddenField ID="hdnAllTeamIDs" runat="server" Value="0" />

        <asp:HiddenField ID="hdnPublishTeamUserID" runat="server" Value="0" />

        <asp:Label runat="server" ID="Label26" CssClass="commonerrormsg" meta:resourcekey="lblMsgResource1"></asp:Label>
        <asp:Label ID="hdnConfirmArchive" Style="display: none;" CssClass="hdnConfirmArchive" meta:resourcekey="ConfirmArchive" runat="server" />
         

        <asp:HiddenField ID="hdnQuesSelectedIDs" runat="server" Value="0" />
        <asp:HiddenField ID="hdnTempSelectedIDs" runat="server" Value="0" />
        <asp:HiddenField ID="hdnTeamSelectedIDs" runat="server" Value="0" />
        <asp:HiddenField ID="hdnUserSelectedIDs" runat="server" Value="0" />

        <asp:HiddenField ID="hdnNoOption" runat="server" Value="" meta:resourcekey="NoOptionRes" />

        <div id="Div1">
            <div class="col-md-12">
                <div class="add-btn1" style="float: right;">
                    <a href="#popupAddNewQues" data-toggle="modal" title="" style="display: none" onclick="return clearData();">
                        <asp:Label runat="server" ID="lblQuestion" CssClass="lblModel" meta:resourcekey="AddQuestion" Style="font-size: 19px; margin-top: 4px;"></asp:Label>
                    </a>
                    <asp:Button runat="server" ID="btnAddQues" Text="Add Question" CssClass="btn btn-default yellow "
                        meta:resourcekey="btnAddQuestion" type="button" OnClick="OpenCreateQuestion" Style="border-radius: 5px; height: 38px; color: white;" />

                    <a href="#popupAddNewQuesCat" data-toggle="modal" title="" style="display: none" onclick="return clearData();">
                        <asp:Label runat="server" ID="lblQuestionCat" CssClass="lblModel" meta:resourcekey="AddQuestionCat" Style="font-size: 19px; margin-top: 4px;"></asp:Label>
                    </a>
                    <asp:Button runat="server" ID="btnAddQuesCat" Text="Add Question Category" CssClass="btn btn-default yellow " meta:resourcekey="AddQuestionCatres"
                        type="button" Style="border-radius: 5px; height: 38px; color: white;" OnClick="OpenCreateQuestionCat" />

                    <a href="#popupAddNewQuesTemplate" data-toggle="modal" title="" style="display: none" onclick="return clearData();">
                        <asp:Label runat="server" ID="lblQuestionTemp" CssClass="lblModel" meta:resourcekey="AddQuestionTemplate" Style="font-size: 19px; margin-top: 4px;"></asp:Label>
                    </a>
                    <asp:Button runat="server" ID="btnAddTemplate" Text="Add Question Template" CssClass="btn btn-default yellow " meta:resourcekey="AddQuestionTemp"
                        type="button" Style="border-radius: 5px; height: 38px; color: white;" OnClick="OpenCreateQuestionTemplate" />
                </div>

                <div style="clear: both;"></div>
            </div>
        </div>
        <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg"
            meta:resourcekey="lblMsgResource1"></asp:Label>
    </div>
    <div>
        <%--Question--%>
        <div>
            <div aria-hidden="true" role="dialog" tabindex="-1" class="modal fade col-md-12" id="popupAddNewQues" style="display: none;">
                <div class="modal-dialog" style="width: 55%">
                    <div class="modal-content">
                        <div class="modal-header blue yellow-radius">
                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                                ×
                            </button>
                            <h4 class="modal-title">
                                <asp:Label runat="server" ID="Label4" CssClass="" Style="color: white; font-size: 19px; margin-top: 4px;" meta:resourcekey="resaddquestion">
                                    <asp:Literal ID="lblModelHeader" runat="server" Text="Add Question"
                                        EnableViewState="true" />
                                </asp:Label></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <div id="errorDiv" runat="server" style="clear: both" visible="false">
                                    <div class="col-md-3" style="padding-top: 10px;">
                                        <asp:Label runat="server" ID="Label17" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                            <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Error" EnableViewState="false" />
                                        </asp:Label>
                                    </div>
                                    <div class="col-md-9">
                                        <asp:Label runat="server" ID="txtError" ForeColor="red" />
                                    </div>
                                </div>
                            </div>
                            <br />

                            <div class="form-group">
                                <div class="col-md-3" style="padding-top: 10px;">
                                    <asp:HiddenField runat="server" ID="hdnUmId" />
                                    <asp:HiddenField runat="server" ID="hdnQuesID" />
                                    <asp:HiddenField ID="hdnQuesCreaterId" runat="server" />
                                    <asp:Label runat="server" ID="Label5" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Question Category" meta:resourcekey="Question_Category" EnableViewState="false" />
                                    </asp:Label>
                                </div>
                                <div class="col-md-9">
                                    <div class="ddpDivision ">

                                        <asp:DropDownList Width="250px"
                                            ID="ddlQuesCat" runat="server" CssClass="chkliststyle form-control" AppendDataBoundItems="False"
                                            RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description"
                                            DataValueField="value" AutoPostBack="false"
                                            Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; width: 100%; color: white; overflow: auto;">
                                        </asp:DropDownList>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlQuesCat"
                                            ErrorMessage="Please select question category." InitialValue="0" CssClass="commonerrormsg"
                                            Display="Dynamic" ValidationGroup="chkdoc" meta:resourcekey="selectquecat"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-3" style="padding-top: 10px;">
                                    <asp:Label runat="server" ID="Label1" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                        <asp:Literal ID="Literal4" runat="server" Text="Question" meta:resourcekey="Question_Name" EnableViewState="false" />
                                    </asp:Label>
                                </div>
                                <div class="col-md-9" style="margin-top: 10px;">
                                    <asp:TextBox runat="server" ID="txtQuestion" placeholder="Enter Question" MaxLength="180" meta:resourcekey="enterque" />
                                    <%--meta:resourcekey="txttitleResource1"--%>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtQuestion"
                                        ErrorMessage="Please enter Question." CssClass="commonerrormsg" Display="Dynamic"
                                        ValidationGroup="chkdoc" meta:resourcekey="plsenterqueres"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-3" style="padding-top: 10px;">
                                    <asp:Label runat="server" ID="Label2" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Description" EnableViewState="false" meta:resourcekey="descriptionres" />
                                    </asp:Label>
                                </div>
                                <div class="col-md-9">
                                    <asp:TextBox runat="server" ID="txtquesDescription" placeholder="Enter Description for the question" TextMode="MultiLine" MaxLength="200" meta:resourcekey="enterdescription" />

                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="col-md-3" style="padding-top: 10px;">
                                            <asp:Label runat="server" ID="Label10" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                                <asp:Literal ID="Literal12" runat="server" Text="Seq. No" meta:resourcekey="SeqNolt" EnableViewState="false" />
                                            </asp:Label>
                                        </div>
                                        <div class="col-md-5">
                                            <asp:TextBox runat="server" ID="txtSeqNo" MaxLength="3" min="0" type="number" max="999" placeholder="" meta:resourcekey="SeqNoRes" onkeypress="return isNumber(event,this);" />
                                            <%--meta:resourcekey="txttitleResource1"--%>
                                        </div>
                                        <div class="col-md-3">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="divAssignTo" runat="server">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="col-md-3" style="padding-top: 10px;">
                                            <asp:Label runat="server" ID="Label20" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                                <asp:Literal ID="Literal26" runat="server" Text="Assign To" meta:resourcekey="AssignTo" EnableViewState="false" />
                                            </asp:Label>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="ddpDivision col-md-4" style="margin-left: -15px">
                                                <%-- <asp:DropDownList Width="250px" ID="ddtTemplate" runat="server" CssClass="chkliststyle form-control"
                                        RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description"
                                        DataValueField="value" Style="width: 100%; overflow: auto;">
                                    </asp:DropDownList>--%>
                                                <asp:HiddenField runat="server" ID="ddtTemplateVal" Value="0" meta:resourcekey="nooptionselected" />
                                                <asp:DropDownList ID="ddtTemplate" runat="server" multiple="multiple">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="col-md-3" style="padding-top: 10px;">
                                            <asp:Label runat="server" ID="Label32" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                                <asp:Literal ID="lblCreateName" runat="server" Text="Creator Name" EnableViewState="false" Visible="false" meta:resourcekey="lblCreateNameres" />
                                            </asp:Label>
                                        </div>
                                        <div class="col-md-5">
                                            <asp:TextBox runat="server" ID="txtCreateName" ReadOnly="true" Visible="false" />
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="col-md-3" style="padding-top: 10px;">
                                            <asp:Label runat="server" ID="Label33" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                                <asp:Literal ID="lblCreateDate" runat="server" Text="Creator Date" EnableViewState="false" Visible="false" meta:resourcekey="lblCreateDateRes" />
                                            </asp:Label>
                                        </div>
                                        <div class="col-md-5">
                                            <asp:TextBox runat="server" ID="txtCreateDate" ReadOnly="true" Visible="false" />
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">

                            <asp:Button runat="server" ID="btnCreateQuestion"
                                Text="Create" CssClass="btn btn-primary yellow"
                                type="button" ValidationGroup="chkdoc" OnClick="CreateQuestion" OnClientClick="CheckQuesTempValue();CheckValidations('chkdoc');closeModelCancel('popupAddNewQues');"
                                Style="border-radius: 5px; width: 100px; height: 38px; margin-left: 359px;" meta:resourcekey="btncreate" />

                            <asp:Button runat="server" ID="btnUpdateQuestion"
                                Text="Update" CssClass="btn btn-primary yellow"
                                type="button" ValidationGroup="chkdoc" OnClick="UpdateQuestion" OnClientClick="CheckQuesTempValue();CheckValidations('chkdoc');closeModelCancel('popupAddNewQues');"
                                Style="border-radius: 5px; width: 100px; height: 38px; margin-left: 359px;" meta:resourcekey="btnupdate" />

                            <asp:Button runat="server" ID="Button3"
                                Text="Close" CssClass="btn btn-default black"
                                type="button" ValidationGroup="chkdoc" OnClientClick="closeModelCancel('popupAddNewQues');"
                                Style="border-radius: 5px; width: 100px; height: 38px;" meta:resourcekey="btnclose" />


                        </div>
                    </div>
                </div>
            </div>
        </div>

        <%--Category--%>
        <div>
            <div aria-hidden="true" role="dialog" tabindex="-1" class="modal fade col-md-12" id="popupAddNewQuesCat" style="display: none;">
                <div class="modal-dialog" style="width: 55%">
                    <div class="modal-content">
                        <div class="modal-header blue yellow-radius">
                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                                ×</button><h4 class="modal-title">
                                    <asp:Label runat="server" ID="Label3" CssClass="" Style="color: white; font-size: 19px; margin-top: 4px;" meta:resourcekey="addquecatres">
                                        <asp:Literal ID="lblQuesCatModalHeader" runat="server" Text="Add Question Category"
                                            EnableViewState="true" />
                                    </asp:Label></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <div id="Div3" runat="server" style="clear: both" visible="false">
                                    <div class="col-md-3" style="padding-top: 10px;">
                                        <asp:Label runat="server" ID="Label6" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                            <asp:Literal ID="Literal6" runat="server" meta:resourcekey="Errorres" EnableViewState="false" />
                                        </asp:Label>
                                    </div>
                                    <div class="col-md-9">
                                        <asp:Label runat="server" ID="Label7" ForeColor="red" />
                                    </div>
                                </div>
                            </div>
                            <br />

                            <div class="form-group">
                                <div class="col-md-3" style="padding-top: 10px;">
                                    <asp:Label runat="server" ID="Label9" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                        <asp:Literal ID="Literal11" runat="server" Text="Question Category Name" meta:resourcekey="QuestionCat_Name" EnableViewState="false" />
                                    </asp:Label>
                                </div>
                                <div class="col-md-9" style="margin-top: 10px;">
                                    <asp:TextBox runat="server" ID="txtqcatName" placeholder="Enter Question Category" MaxLength="50" meta:resourcekey="enterquecatres" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtqcatName"
                                        ErrorMessage="Please enter Question Category." CssClass="commonerrormsg" Display="Dynamic"
                                        ValidationGroup="chkdoc1" meta:resourcekey="plsenterquerescat"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-3" style="padding-top: 10px;">
                                    <asp:Label runat="server" ID="Label8" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                        <asp:Literal ID="Literal10" runat="server" Text="Question Category Name (Danish)" meta:resourcekey="QuestionCatnamedn" EnableViewState="false" />
                                    </asp:Label>
                                </div>
                                <div class="col-md-9" style="margin-top: 10px;">
                                    <asp:TextBox runat="server" ID="txtqcatNameDN" placeholder="Enter Question Category" MaxLength="50" meta:resourcekey="enterquecatdn" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtqcatNameDN"
                                        ErrorMessage="Please enter Question Category." CssClass="commonerrormsg" Display="Dynamic"
                                        ValidationGroup="chkdoc1" meta:resourcekey="plsenterquecardn"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-3" style="padding-top: 10px;">
                                    <asp:Label runat="server" ID="Label11" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                        <asp:Literal ID="Literal13" runat="server" Text="Description" EnableViewState="false" meta:resourcekey="descrires" />
                                    </asp:Label>
                                </div>
                                <div class="col-md-9">
                                    <asp:TextBox runat="server" ID="txtqcatDescription" placeholder="Enter Description for the category" TextMode="MultiLine" MaxLength="200" meta:resourcekey="enterdescres" />

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="col-md-3" style="padding-top: 10px;">
                                            <asp:Label runat="server" ID="Label34" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                                <asp:Literal ID="lblCatCreateName" runat="server" Text="Creator Name" EnableViewState="false" Visible="false" />
                                            </asp:Label>
                                        </div>
                                        <div class="col-md-5">
                                            <asp:TextBox runat="server" ID="txtCatCreateName" ReadOnly="true" Visible="false" />
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="col-md-3" style="padding-top: 10px;">
                                            <asp:Label runat="server" ID="Label35" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                                <asp:Literal ID="lblCatCreateDate" runat="server" Text="Creator Date" EnableViewState="false" Visible="false" />
                                            </asp:Label>
                                        </div>
                                        <div class="col-md-5">
                                            <asp:TextBox runat="server" ID="txtCatCreateDate" ReadOnly="true" Visible="false" />
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">

                            <asp:Button runat="server" ID="btnCreateCat"
                                Text="Create" CssClass="btn btn-primary yellow"
                                type="button" OnClick="CreateQuestionCategory" OnClientClick="CheckValidations('chkdoc1');closeModelCancel('popupAddNewQuesCat');"
                                Style="border-radius: 5px; width: 100px; height: 38px; margin-left: 359px;" meta:resourcekey="createres" />
                            <%--  <asp:Button runat="server" 
                           CssClass="btn btn-primary yellow"
                             OnClick="btnCreateCat_click" Text="Create Category"
                            Style="border-radius: 5px; width: 150px; height: 38px; margin-left: 359px;" />--%>
                            <asp:Button runat="server" ID="Button2"
                                Text="Close" CssClass="btn btn-default black"
                                type="button" ValidationGroup="chkdoc1" OnClientClick="closeModelCancel('popupAddNewQuesCat');"
                                Style="border-radius: 5px; width: 100px; height: 38px;" meta:resourcekey="closeres" />


                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%--Template--%>
        <div>
            <div aria-hidden="true" role="dialog" tabindex="-1" class="modal fade col-md-12" id="popupAddNewQuesTemplate" style="display: none;">
                <div class="modal-dialog" style="width: 55%">
                    <div class="modal-content">
                        <div class="modal-header blue yellow-radius">
                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                                ×</button><h4 class="modal-title">
                                    <asp:Label runat="server" ID="Label12" CssClass="" Style="color: white; font-size: 19px; margin-top: 4px;" meta:resourcekey="addquetempres">
                                        <asp:Literal ID="Literal14" runat="server" Text="Add Question Template"
                                            EnableViewState="true" />
                                    </asp:Label></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <div id="Div4" runat="server" style="clear: both" visible="false">
                                    <div class="col-md-3" style="padding-top: 10px;">
                                        <asp:Label runat="server" ID="Label13" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                            <asp:Literal ID="Literal15" runat="server" meta:resourcekey="resError" EnableViewState="false" />
                                        </asp:Label>
                                    </div>
                                    <div class="col-md-9">
                                        <asp:Label runat="server" ID="Label14" ForeColor="red" />
                                    </div>
                                </div>
                            </div>
                            <br />

                            <div class="form-group">
                                <div class="col-md-3" style="padding-top: 10px;">
                                    <asp:Label runat="server" ID="Label15" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                        <asp:Literal ID="Literal16" runat="server" Text="Question Template Name" meta:resourcekey="QuestionTemp_Name" EnableViewState="false" />
                                    </asp:Label>
                                </div>
                                <div class="col-md-9" style="margin-top: 10px;">
                                    <asp:TextBox runat="server" ID="txttmpName" placeholder="Enter Question Template Name" MaxLength="50" meta:resourcekey="enterquetemp" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txttmpName"
                                        ErrorMessage="Please enter Template Name." CssClass="commonerrormsg" Display="Dynamic"
                                        ValidationGroup="chkdoc3" meta:resourcekey="plsenterquetempname"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-3" style="padding-top: 10px;">
                                    <asp:Label runat="server" ID="Label16" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                        <asp:Literal ID="Literal18" runat="server" Text="Question Template Name (Danish)" meta:resourcekey="QuestionTempNamedn" EnableViewState="false" />
                                    </asp:Label>
                                </div>
                                <div class="col-md-9" style="margin-top: 10px;">
                                    <asp:TextBox runat="server" ID="txttmpNameDN" placeholder="Enter Question Template Name" MaxLength="50" meta:resourcekey="enterquetemname" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txttmpNameDN"
                                        ErrorMessage="Please enter Template Name." CssClass="commonerrormsg" Display="Dynamic"
                                        ValidationGroup="chkdoc3" meta:resourcekey="plsentertemnamedn"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-3" style="padding-top: 10px;">
                                    <asp:Label runat="server" ID="Label18" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                        <asp:Literal ID="Literal19" runat="server" Text="Description" EnableViewState="false" meta:resourcekey="descriptionresource" />
                                    </asp:Label>
                                </div>
                                <div class="col-md-9">
                                    <asp:TextBox runat="server" ID="txttmpDescription" placeholder="Enter Description" TextMode="MultiLine" MaxLength="200" meta:resourcekey="enterdescriptionres" />

                                </div>
                            </div>

                            <div class="form-group" runat="server" id="enableDiv">
                                <div class="col-md-3" style="padding-top: 10px;">
                                    <asp:Label runat="server" ID="Label19" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                        <asp:Literal ID="Literal20" runat="server" Text="Active" meta:resourcekey="Enabled" EnableViewState="false" /></asp:Label>
                                </div>
                                <div class="col-md-9" style="margin-top: 10px">
                                    <asp:RadioButtonList runat="server" ID="rdoIsActive" RepeatDirection="Horizontal" CssClass="inline-rb" TextAlign="Right">
                                        <asp:ListItem Text="Yes" meta:resourcekey="Yesres" Value="true" Selected="True" />
                                        <asp:ListItem Text="No" meta:resourcekey="Nores" Value="false" />
                                    </asp:RadioButtonList>
                                    <%--meta:resourcekey="txttitleResource1"--%>
                                </div>
                            </div>

                            <div class="form-group" runat="server" id="publicDiv">
                                <div class="col-md-3" style="padding-top: 10px;">
                                    <asp:Label runat="server" ID="Label21" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                        <asp:Literal ID="Literal21" runat="server" Text="Public" meta:resourcekey="Public" EnableViewState="false" /></asp:Label>
                                </div>
                                <div class="col-md-9" style="margin-top: 10px">
                                    <asp:RadioButtonList runat="server" ID="roIsPublic" RepeatDirection="Horizontal" CssClass="inline-rb" TextAlign="Right">
                                        <asp:ListItem Text="Yes" meta:resourcekey="lblYesresource" Value="true" Selected="True" />
                                        <asp:ListItem Text="No" meta:resourcekey="lblNores" Value="false" />
                                    </asp:RadioButtonList>
                                    <%--meta:resourcekey="txttitleResource1"--%>
                                </div>
                            </div>

                            <div class="form-group" runat="server" id="mandatoryDiv">
                                <div class="col-md-3" style="padding-top: 10px;">
                                    <asp:Label runat="server" ID="Label22" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                        <asp:Literal ID="Literal22" runat="server" Text="Mandatory" meta:resourcekey="Mandatorylt" EnableViewState="false" /></asp:Label>
                                </div>
                                <div class="col-md-9" style="margin-top: 10px">
                                    <asp:RadioButtonList runat="server" ID="roIsMandatory" RepeatDirection="Horizontal" CssClass="inline-rb" TextAlign="Right">
                                        <asp:ListItem Text="Yes" meta:resourcekey="Yesresource1" Value="true" Selected="True" />
                                        <asp:ListItem Text="No" meta:resourcekey="Noresource1" Value="false" />
                                    </asp:RadioButtonList>

                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">

                            <asp:Button runat="server" ID="btnCreateTemplate"
                                Text="Create" CssClass="btn btn-primary yellow"
                                type="button" OnClick="CreateQuestionTemplate" OnClientClick="CheckValidations('chkdoc3');closeModelCancel('popupAddNewQuesTemplate');"
                                Style="border-radius: 5px; width: 100px; height: 38px; margin-left: 359px;" meta:resourcekey="createresource" />

                            <asp:Button runat="server" ID="Button4"
                                Text="Close" CssClass="btn btn-default black"
                                type="button" ValidationGroup="chkdoc3" OnClientClick="closeModelCancel('popupAddNewQuesTemplate');"
                                Style="border-radius: 5px; width: 100px; height: 38px;" meta:resourcekey="closeresource" />


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="col-md-12" style="margin-top: 20px;" id="htmlData">
        <div id="graph-wrapper">
            <div class="col-md-12">
                <ul class="tabs-menu maintab">
                    <li class="current" id="t1"><a href="javascript:void(0)" onclick="DisplayTab(1);">
                        <asp:Literal ID="Literal7" runat="server" Text="Manage Question" meta:resourcekey="ManageQuesResource"
                            EnableViewState="false" /></a></li>
                    <li id="t2"><a href="javascript:void(0)" onclick="DisplayTab(2);">
                        <asp:Literal ID="Literal8" runat="server" Text="Assign Question" meta:resourcekey="AssignQues" EnableViewState="false" /></a></li>
                    <li id="t3"><a href="javascript:void(0)" onclick="DisplayTab(3);">
                        <asp:Literal ID="Literal9" runat="server" Text="Publish Question" meta:resourcekey="PublishQues" EnableViewState="false" />
                    </a></li>
                </ul>
                <div class="chart-tab">
                    <div id="tabs-container">
                        <div class="loaderDiv" style="display: none">
                            <div class="progress small-progress">
                                <div style="width: 40%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40"
                                    role="progressbar" class="progress-bar blue">
                                </div>
                            </div>

                            <div class="home_grap">
                                <div id="ContainerDevelopment" style="min-width: 310px; height: 400px; margin: 0 auto">
                                    <div id="waitDevelopment" style="margin: 190px 602px">
                                        <img src="../images/wait.gif" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab">

                            <div id="tab-1" class="tab-content">
                                <%--   <asp:UpdatePanel ID="UpdatePanel33" runat="server">
                                    <ContentTemplate>--%>
                                <div class="form-group col-md-12" style="margin-top: 2%;">
                                    <div class="col-md-1">
                                        <asp:Label runat="server" ID="Label27" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                            <asp:Literal ID="Literal5" runat="server" Text="Filter on Template" meta:resourcekey="FilterTemplate" EnableViewState="false" />
                                        </asp:Label>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="ddpDivision ">
                                            <asp:DropDownList Width="250px"
                                                ID="ddlFilterTemplate" runat="server" CssClass="chkliststyle form-control" AppendDataBoundItems="False"
                                                RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description"
                                                DataValueField="value" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterTemplate_OnSelectedIndexChanged"
                                                Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; width: 100%; color: white; overflow: auto;">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-md-1">
                                        <asp:Label runat="server" ID="Label30" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                            <asp:Literal ID="Literal27" runat="server" Text="Filter on Category" meta:resourcekey="FilterCategory" EnableViewState="false" />
                                        </asp:Label>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="ddpDivision ">
                                            <asp:DropDownList Width="250px"
                                                ID="ddlFilterCategory" runat="server" CssClass="chkliststyle form-control" AppendDataBoundItems="False"
                                                RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description"
                                                DataValueField="value" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterTemplate_OnSelectedIndexChanged"
                                                Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; width: 100%; color: white; overflow: auto;">
                                            </asp:DropDownList>
                                        </div>
                                    </div>


                                    <div class="col-md-6">
                                        <div class="col-md-4">
                                            <asp:TextBox runat="server" CssClass="library_search datepicker1 col-md-2"
                                                ID="txtSearchStartDate" placeholder="Start Date" meta:resourcekey="StartDateRes"
                                                data-mask="99/99/2099" AutoPostBack="False" Style="height: 42px;"
                                                onmouseover="SetDatePicker();" OnTextChanged="ddlFilterTemplate_OnSelectedIndexChanged"></asp:TextBox>
                                        </div>


                                        <div class="col-md-4">
                                            <asp:TextBox runat="server" CssClass="library_search datepicker1 col-md-2"
                                                ID="txtSearchEndDate" placeholder="End Date" meta:resourcekey="enddtres"
                                                data-mask="99/99/2099" AutoPostBack="False" Style="height: 42px;"
                                                onmouseover="SetDatePicker();" OnTextChanged="ddlFilterTemplate_OnSelectedIndexChanged"></asp:TextBox>
                                        </div>
                                    </div>

                                </div>

                                <div class="chart-tab manager_table">
                                    <div id="tabs-container manager_table">
                                        <asp:GridView ID="gvGrid_Question" runat="server" AutoGenerateColumns="False" CellPadding="0"
                                            Width="100%" GridLines="None" DataKeyNames="quesID"
                                            CssClass="table1 table-striped table-bordered table-hover table-checkable datatable"
                                            EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>'
                                            BackColor="White"
                                            meta:resourcekey="GridRecordNotfound"
                                            OnRowDataBound="gvGrid_Question_RowDataBound"
                                            OnRowCommand="gvGrid_Question_RowCommand">
                                            <%-- OnRowDataBound="gvGrid_Question_RowDataBound"
                                            OnRowCreated="gvGrid_RowCreated"
                                            OnPreRender="gvGrid_PreRender">--%>


                                            <HeaderStyle CssClass="aa" />
                                            <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                            <AlternatingRowStyle BackColor="White" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="SrNo" meta:resourcekey="SrNo">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs ItemStyle" Width="50px" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                        Width="3%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />


                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Question" meta:resourcekey="Question">
                                                    <ItemTemplate>
                                                        <%#Eval("Question") %>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs ItemStyle" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                        Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                    <FooterTemplate>
                                                        <asp:Label runat="server" ID="asdf">
                                                            <asp:Literal ID="Literal17" runat="server" meta:resourcekey="TotalCount" EnableViewState="false" /></asp:Label>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Question Catetgory" meta:resourcekey="Questioncat">
                                                    <ItemTemplate>
                                                        <%--  <%#Eval("qcatName") %>--%>
                                                        <%if (Convert.ToString(Session["Culture"]) == "Danish")
                                                          {%>
                                                        <%#Eval("qcatNameDN") %>
                                                        <%} %>
                                                        <%else
                                                          { %>
                                                        <%#Eval("qcatName") %>
                                                        <%} %>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs ItemStyle" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                        Width="15%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                </asp:TemplateField>




                                                <asp:TemplateField HeaderText="Seq. No" meta:resourcekey="Seqno">
                                                    <ItemTemplate>
                                                        <%#Eval("SeqNo") %>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" CssClass="hidden-xs" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                        Width="5%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="" ItemStyle-Width="50px"
                                                    meta:resourcekey="TemplateFieldResource3">
                                                    <ItemTemplate>
                                                        <asp:HiddenField runat="server" ID="hdnActUmId" Value='<%# Eval("CreatedBy") %>' />
                                                        <asp:Panel runat="server" ID="viewPanel">
                                                            <i class="fa fa-pencil"></i>
                                                            <asp:LinkButton ID="lnkEdit" CssClass="def" runat="server" CommandName="EditRow" CommandArgument='<%# Eval("quesID") %>'
                                                                ToolTip="Edit"><asp:Label runat="server" meta:resourcekey="btnEdit"></asp:Label></asp:LinkButton>&nbsp;&nbsp; <i class="fa fa-trash-o"></i>
                                                            <asp:LinkButton ID="lnkBtnName" CssClass="def" runat="server" CommandName="archive" CommandArgument='<%# Eval("quesID") %>'
                                                                ToolTip="Archive" meta:resourcekey="btnDelete" OnClientClick="var ConfirmArchive = function() { return confirm($('.hdnConfirmArchive').text())};return ConfirmArchive();"></asp:LinkButton>&nbsp;&nbsp; <i class="fa fa-info-circle"></i>
                                                            <asp:LinkButton ID="lnkView" CssClass="def" runat="server" CommandName="ViewRow" CommandArgument='<%# Eval("quesID") %>'
                                                                ToolTip="View">
                                                                <asp:Label ID="Label37" runat="server" meta:resourcekey="btnViewRes"></asp:Label>
                                                            </asp:LinkButton>
                                                        </asp:Panel>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                        Width="15%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkQues" runat="server" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" CssClass="hidden-xs" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                        Width="5%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                </asp:TemplateField>

                                            </Columns>
                                            <FooterStyle BackColor="#cccccc" Font-Bold="True" ForeColor="Black" HorizontalAlign="Left" />
                                        </asp:GridView>
                                    </div>
                                </div>
                                <%-- </ContentTemplate>

                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="gvGrid_Question" />
                                    </Triggers>
                                </asp:UpdatePanel>--%>
                                <div class="form-group col-md-12" style="margin-left: 50%; margin-top: 2%;">
                                    <div class="col-md-2" style="text-align: right;">

                                        <asp:Label runat="server" ID="Label36" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;" Text="Add to Template" meta:resourcekey="Addtotemplate1">
                                            
                                        </asp:Label>
                                    </div>
                                    <div class="col-md-2">
                                        <asp:DropDownList Width="50px"
                                            ID="ddlAddTemplateQues" runat="server" CssClass="chkliststyle form-control" AppendDataBoundItems="False"
                                            RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description" meta:resourcekey="ddlAddTemplateQues"
                                            DataValueField="value" AutoPostBack="false"
                                            Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; width: 100%; color: white; overflow: auto;">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-md-2">
                                        <asp:Button runat="server" ID="btnSaveQuesToTemplate" meta:resourcekey="btnSave" Text="Save"
                                            CssClass="btn btn-default black col-md-12" OnClick="btnSaveQuesToTemplate_Click"
                                            CausesValidation="False"></asp:Button>
                                    </div>

                                </div>

                            </div>
                            <div id="tab-2" class="tab-content">
                                <div class="col-md-12">
                                    <div class="col-md-6" style="margin-top: 2%;">
                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div style="height: 100px;">
                                                    <div class="form-group col-md-12">
                                                        <div class="col-md-2">
                                                            <asp:Label runat="server" ID="Label28" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                                                <asp:Literal ID="Literal29" runat="server" Text="Choose Template" meta:resourcekey="Question_Template" EnableViewState="false" />
                                                            </asp:Label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="ddpDivision ">
                                                                <asp:DropDownList Width="250px"
                                                                    ID="ddlAssignQuesTemplate" runat="server" CssClass="chkliststyle form-control" AppendDataBoundItems="False"
                                                                    RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description"
                                                                    DataValueField="value" AutoPostBack="true" OnSelectedIndexChanged="ddlAssignQuesTemplate_OnSelectedIndexChanged"
                                                                    Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; width: 100%; color: white; overflow: auto;">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <asp:Button runat="server" ID="btnAssignSelectQuesTemplate" meta:resourcekey="btnSelectresource" Text="Select"
                                                                CssClass="btn black" OnClick="btnAssignSelectQuesTemplate_Click"
                                                                CausesValidation="False"></asp:Button>

                                                            <asp:Button runat="server" ID="btnAssignResetQuesTemplate" Text="Reset" meta:resourcekey="btnReset"
                                                                CssClass="btn black" OnClick="btnAssignResetQuesTemplate_Click"
                                                                CausesValidation="False"></asp:Button>
                                                        </div>
                                                    </div>
                                                    <div class="form-group  col-md-12">
                                                        <div class="col-md-2">
                                                            <asp:Label runat="server" ID="Label29" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                                                <asp:Literal ID="Literal30" runat="server" Text="Choose Question" meta:resourcekey="Question_Category" EnableViewState="false" />
                                                            </asp:Label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="ddpDivision ">

                                                                <asp:DropDownList Width="250px"
                                                                    ID="ddlAssignQuesByTemp" runat="server" CssClass="chkliststyle form-control" AppendDataBoundItems="False"
                                                                    RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description"
                                                                    DataValueField="value" AutoPostBack="false"
                                                                    Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; width: 100%; color: white; overflow: auto;">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <asp:Button runat="server" ID="btnAssignSelectQues" meta:resourcekey="btnSelectres" Text="Select"
                                                                CssClass="btn btn-default black col-md-12" OnClick="btnAssignSelectQues_Click"
                                                                CausesValidation="False"></asp:Button>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div style="height: 400px; overflow-y: scroll; margin-top: 2%; width: 100%;">
                                                    <asp:GridView ID="gvQuesTemplate" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                                                        DataKeyNames="qtmpID"
                                                         OnRowDataBound="gvQuesTemplate_OnRowDataBound" Width="100%"
                                                        EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>'
                                                        meta:resourcekey="NoRecordFound">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <img alt="PLUS" style="cursor: pointer" class="plussign plus" src="../images/plus.png" onclick="plussign(this);" />
                                                                    <asp:Panel ID="pnlOrders" runat="server" Style="display: none">
                                                                        <asp:HiddenField runat="server" ID="hdnqtmpID" Value='<%#Eval("qtmpID") %>' />
                                                                        <asp:HiddenField runat="server" ID="hdnIsTmpMandatory" Value='<%#Eval("IsMandatory") %>' />
                                                                        <asp:Label ID="lblNoRecordFoundQues" Visible="false" ForeColor="Red" runat="server" Text="No record found" meta:resourcekey="RecordNotfoundResource" EnableViewState="false" />
                                                                        <asp:GridView ID="gvQues" runat="server" AutoGenerateColumns="false" 
                                                                            EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>'
                                                                             OnRowDataBound="gvQues_OnRowDataBound"
                                                                            CssClass="table table-striped table-bordered table-hover table-checkable datatable">
                                                                            <Columns>
                                                                                <asp:TemplateField meta:resourcekey="QuestionTemplateres" HeaderText="Question">
                                                                                    <ItemTemplate>
                                                                                         <asp:HiddenField runat="server" ID="hdnquesIsMandatory_gvQues" Value='<%#Eval("quesIsMandatory") %>' />
                                                                                        <%#Eval("Question") %>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                        Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                                    <%-- <FooterTemplate>
                                                                        <asp:Label runat="server" ID="asdf"> Requested   </asp:Label>
                                                                    </FooterTemplate>--%>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField meta:resourcekey="Mandatory" HeaderText="Mandatory">
                                                                                    <ItemTemplate>
                                                                                      <%--  <%# (Boolean.Parse(Eval("quesIsMandatory").ToString())) ? "Yes" : "No" %>--%> 
                                                                                          <asp:Label runat="server" ID="lblmandatory_gvQues"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                        Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                        <%--   <input type="checkbox" name="chekQues" id='<%# String.Format("QuesID_{0}_{1}_status", Eval("TmpID").ToString(),Eval("quesID").ToString()) %>'
                                                                                onchange="SelectQuesTemplate(this, '<%# Eval("TmpID") %>    ','<%# Eval("quesID") %>    ');" />--%>

                                                                                        <input type="checkbox" onclick="SelectQuesTemplate(this,'<%# Eval("TmpID") %>    ','<%# Eval("quesID") %>    ')"
                                                                                            class='<%# String.Format("Ques{0}", Eval("TmpID").ToString()) %>' checked="checked"
                                                                                            id='<%# String.Format("Ques{0}{1}", Eval("TmpID").ToString(),Eval("quesID").ToString()) %>' />

                                                                                        <%-- <input type="checkbox" runat="server" onclick="SelectQuesTemplate(this,'<%#DataBinder.Eval(Container.DataItem,"TmpID")%>    ','<%#DataBinder.Eval(Container.DataItem,"quesID")%>    ')"
                                                                                class='<%# String.Format("Ques{0}", DataBinder.Eval(Container.DataItem,"TmpID")) %>' 
                                                                                id='Checkbox1' 
                                                                                checked="checked" />--%>
                                                                                        <asp:HiddenField runat="server" ID="hdnChkQuesTemp" Value="1" EnableViewState="true" />
                                                                                        <asp:HiddenField runat="server" ID="hdnID" Value='<%#Eval("quesID") %>' />
                                                                                        <asp:HiddenField runat="server" ID="txthdnTmpID" Value='<%#Eval("TmpID") %>' />
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                        Width="5%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                                </asp:TemplateField>

                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </asp:Panel>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" CssClass="hidden-xs" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="5%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                            </asp:TemplateField>


                                                            <asp:TemplateField meta:resourcekey="TemplateName" HeaderText="Template">
                                                                <ItemTemplate>

                                                                    <%if (Convert.ToString(Session["Culture"]) == "Danish")
                                                                      {%>
                                                                    <%#Eval("tmpNameDN") %>
                                                                    <%} %>
                                                                    <%else
                                                                      { %>
                                                                    <%#Eval("tmpName") %>
                                                                    <%} %>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="80%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                <%-- <FooterTemplate>
                                                    <asp:Label runat="server" ID="lblActivity"> Total   </asp:Label>
                                                </FooterTemplate>--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField meta:resourcekey="Mandatoryres" HeaderText="Mandatory">
                                                                <ItemTemplate>
                                                                    <%--<%# (Boolean.Parse(Eval("IsMandatory").ToString())) ? "Yes" : "No" %>--%>
                                                                         <asp:Label runat="server" ID="lblmandatory_gvQuesTemplate"></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <%-- <asp:CheckBox Width="250px" ID="chkList" runat="server" CssClass="chkliststyle"
                                                        BorderWidth="0" Style="width: 100%;"></asp:CheckBox>--%>
                                                                    <input type="checkbox" name="chekQues" checked="checked"
                                                                        class='<%# String.Format("MainQues{0}", Eval("qtmpID").ToString()) %>'
                                                                        id='<%# String.Format("MainQues{0}", Eval("qtmpID").ToString()) %>'
                                                                        onclick="SelectMainQuesTemplate(this, '<%# Eval("qtmpID") %>    ');" />

                                                                    <asp:HiddenField runat="server" ID="hdnChkMainQuesTemp" Value="1" EnableViewState="true" />
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                            </asp:TemplateField>
                                                            <%-- <asp:BoundField ItemStyle-Width="150px" DataField="" HeaderText="City" />--%>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="gvQuesTemplate" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div class="col-md-6" style="margin-top: 2%;">
                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div style="height: 100px;">
                                                    <div class="form-group col-md-12">
                                                        <div class="col-md-2">
                                                            <asp:Label runat="server" ID="Label23" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                                                <asp:Literal ID="Literal23" runat="server" Text="Choose Team" meta:resourcekey="Team" EnableViewState="false" />
                                                            </asp:Label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="ddpDivision ">

                                                                <asp:DropDownList Width="250px"
                                                                    ID="ddlAssignTeam" runat="server" CssClass="chkliststyle form-control" AppendDataBoundItems="False"
                                                                    RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description"
                                                                    DataValueField="value" AutoPostBack="true" OnSelectedIndexChanged="ddlAssignTeam_OnSelectedIndexChanged"
                                                                    Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; width: 100%; color: white; overflow: auto;">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <asp:Button runat="server" ID="btnAssignSelectTeam" meta:resourcekey="Filterbtnres" Text="Select"
                                                                CssClass="btn black" OnClick="btnAssignSelectTeam_Click"
                                                                CausesValidation="False"></asp:Button>

                                                            <asp:Button runat="server" ID="btnAssignResetTeam" Text="Reset" meta:resourcekey="Resetbtn"
                                                                CssClass="btn black" OnClick="btnAssignResetTeam_Click"
                                                                CausesValidation="False"></asp:Button>
                                                        </div>
                                                    </div>
                                                    <div class="form-group  col-md-12">
                                                        <div class="col-md-2">
                                                            <asp:Label runat="server" ID="Label25" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                                                <asp:Literal ID="Literal25" runat="server" Text="Choose User" meta:resourcekey="User" EnableViewState="false" />
                                                            </asp:Label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="ddpDivision ">

                                                                <asp:DropDownList Width="250px"
                                                                    ID="ddlAssignUser" runat="server" CssClass="chkliststyle form-control" AppendDataBoundItems="False"
                                                                    RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description"
                                                                    DataValueField="value" AutoPostBack="false"
                                                                    Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; width: 100%; color: white; overflow: auto;">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <asp:Button runat="server" ID="btnAssignSelectTeamUser" meta:resourcekey="btnFilterres" Text="Select"
                                                                CssClass="btn btn-default black col-md-12" OnClick="btnAssignSelectTeamUser_Click"
                                                                CausesValidation="False"></asp:Button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div style="height: 400px; overflow-y: scroll; margin-top: 2%;width:100%;">
                                                    <asp:GridView ID="gridTeamList" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                                                        DataKeyNames="TeamID" OnRowDataBound="gridTeamList_OnRowDataBoundTeam" EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>'
                                                        meta:resourcekey="NoRecordFound">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <img alt="PLUS" style="cursor: pointer" class="teamplussign teamplus" src="../images/plus.png" onclick="teamplussign(this);" />
                                                                    <asp:Panel ID="pnlTeamUser" runat="server" Style="display: none">
                                                                        <asp:HiddenField runat="server" ID="hdnTeamID" Value='<%#Eval("TeamID") %>' />
                                                                        <asp:Label ID="lblNoRecordFound" ForeColor="Red" Visible="false" runat="server" Text="No record found" meta:resourcekey="RecordNotfoundResource" EnableViewState="false" />
                                                                        <asp:GridView ID="gvTeamUser" runat="server" AutoGenerateColumns="false" EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>'
                                                                            CssClass="table table-striped table-bordered table-hover table-checkable datatable">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="User Name">
                                                                                    <ItemTemplate>
                                                                                        <%#Eval("userFirstName") %>  <%#Eval("userLastName") %>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                        Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />

                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>

                                                                                        <input type="checkbox"
                                                                                            onclick="SelectUserTemplate(this,'<%# Eval("TeamID") %>    ','<%# Eval("userId") %>    ')"
                                                                                            class='<%# String.Format("User{0}", Eval("TeamID").ToString()) %>' checked="checked"
                                                                                            id='<%# String.Format("User{0}{1}", Eval("TeamID").ToString(),Eval("userId").ToString()) %>' />
                                                                                        <asp:HiddenField runat="server" ID="hdnChkTeamUser" Value="1" EnableViewState="true" />

                                                                                        <asp:HiddenField runat="server" ID="hdnUsernoID" Value='<%#Eval("userId") %>' />
                                                                                        <asp:HiddenField runat="server" ID="txthdnTeamNoID" Value='<%#Eval("TeamID") %>' />

                                                                                    </ItemTemplate>
                                                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                        Width="5%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />

                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </asp:Panel>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" CssClass="hidden-xs" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="5%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                            </asp:TemplateField>


                                                            <asp:TemplateField meta:resourcekey="TeamName" HeaderText="Target">
                                                                <ItemTemplate>
                                                                    <%if (Convert.ToString(Session["Culture"]) == "Danish")
                                                                      {%>
                                                                    <%#Eval("TeamNameDN") %>
                                                                    <%} %>
                                                                    <%else
                                                                      { %>
                                                                    <%#Eval("TeamName") %>
                                                                    <%} %>
                                                                    <%-- <%#Eval("TeamName") %>--%>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="80%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField meta:resourcekey="TeamName">
                                                                <ItemTemplate>
                                                                    <input type="checkbox" name="chekQues"
                                                                        class='<%# String.Format("TeamUser{0}", Eval("TeamID").ToString()) %>'
                                                                        id='<%# String.Format("TeamUser{0}", Eval("TeamID").ToString()) %>' checked="checked"
                                                                        onclick="SelectTeamUserTemplate(this, '<%# Eval("TeamID") %>    ');" />

                                                                    <asp:HiddenField runat="server" ID="hdnChkMainTeam" Value="1" EnableViewState="true" />
                                                                    <%-- <input type="checkbox" name="chekQues" class='<%# String.Format("Team{0}", Eval("TeamID").ToString()) %>'
                                                            onclick="SelectTeamTemplate(this, '<%# Eval("TeamID") %>    ');" />--%>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="5%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="gridTeamList" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>


                                <div class="col-md-12" style="margin-top: 5%;">
                                    <div class="form-group col-md-12">
                                        <div class="col-md-3" style="padding-top: 10px; text-align: right;">
                                            <asp:Label runat="server" ID="Label24" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                                <asp:Literal ID="Literal24" runat="server" Text="Assign Template to Team" EnableViewState="false" meta:resourcekey="AssignTotemp" />
                                            </asp:Label>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="chat-widget widget-body col-md-12 adncPrivateDiv" style="display: block; -ms-border-radius: 0px; border-radius: 0px">
                                                <div class="col-md-3 ">
                                                    <asp:TextBox runat="server" CssClass="library_search datepicker1 col-md-2"
                                                        ID="txtSearchStartDate_Public"
                                                        placeholder="Start Date" meta:resourcekey="startdateresource"
                                                        data-mask="99/99/2099" AutoPostBack="False" Style="height: 42px;"
                                                        onmouseover="SetDatePicker();"></asp:TextBox>
                                                </div>
                                                <div class="col-md-3 ">
                                                    <asp:TextBox runat="server" CssClass="library_search datepicker1 col-md-2"
                                                        placeholder="End Date" meta:resourcekey="enddateresource"
                                                        ID="txtSearchEndDate_Public" data-mask="99/99/2099"
                                                        AutoPostBack="False"
                                                        onmouseover="SetDatePicker();"
                                                        Style="height: 42px;"></asp:TextBox>
                                                </div>
                                                <div class="col-md-2 " style="margin-top: 5px">
                                                    <asp:Button runat="server" ID="btnAssign" meta:resourcekey="btnAssign" Text="Assign" CssClass="btn btn-default black col-md-12"
                                                        CausesValidation="False" OnClick="btnAssign_click" OnClientClick="return GetAllValues();"></asp:Button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <asp:GridView ID="grid_QuestionAssign" runat="server" AutoGenerateColumns="False" CellPadding="0"
                                        Width="100%" GridLines="None" DataKeyNames="assignID" CssClass="table1 table-striped table-bordered table-hover table-checkable datatable"
                                        EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>'
                                        BackColor="White"
                                        meta:resourcekey="GridRecordNotfound"
                                        OnRowDataBound="grid_QuestionAssign_RowDataBound"
                                        OnRowCommand="grid_QuestionAssign_RowCommand">

                                        <HeaderStyle CssClass="aa" />
                                        <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                        <AlternatingRowStyle BackColor="White" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <img alt="PLUS" style="cursor: pointer" class="assignplussign assignplus" src="../images/plus.png" onclick="assignplussign(this);" />
                                                    <asp:Panel ID="pnlOrders" runat="server" Style="display: none">
                                                        <asp:HiddenField runat="server" ID="hdngridAssignID" Value='<%#Eval("assignID") %>' />
                                                        <asp:HiddenField runat="server" ID="hdngridAssignTempID" Value='<%#Eval("TemplateID") %>' />
                                                        <asp:HiddenField runat="server" ID="hdngridAssignTeamID" Value='<%#Eval("TeamID") %>' />

                                                        <asp:HiddenField runat="server" ID="hdngridAssignQuesIDs" Value='<%#Eval("QuesIDs") %>' />
                                                        <asp:HiddenField runat="server" ID="hdngridAssignUserIDs" Value='<%#Eval("UserIDs") %>' />
                                                        <asp:HiddenField runat="server" ID="hdnIsmandatory" Value='<%# Eval("IsMandatory") %>' />

                                                        <asp:Label ID="lblNoRecordFoundQues" Visible="false" ForeColor="Red" runat="server" Text="No record found" meta:resourcekey="RecordNofound" EnableViewState="false" />
                                                        <div class="col-md-12">
                                                            <div class="col-md-6">
                                                                <asp:GridView ID="gvQues_AssignList" runat="server" AutoGenerateColumns="false" EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>'
                                                                    CssClass="table table-striped table-bordered table-hover table-checkable datatable">
                                                                    <Columns>
                                                                        <asp:TemplateField meta:resourcekey="Question" HeaderText="Question">
                                                                            <ItemTemplate>
                                                                                <%#Eval("Question") %>
                                                                            </ItemTemplate>
                                                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />

                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <asp:GridView ID="gvTeamUser_AssignList" runat="server" EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>'
                                                                    AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover table-checkable datatable">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="User Name" meta:resourcekey="usernameresource">
                                                                            <ItemTemplate>
                                                                                <%#Eval("userFirstName") %>  <%#Eval("userLastName") %>
                                                                            </ItemTemplate>
                                                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />

                                                                        </asp:TemplateField>

                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" CssClass="hidden-xs" />
                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                    Width="5%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="SrNo" meta:resourcekey="SrNoresource1">
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs ItemStyle" Width="50px" />
                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                    Width="2%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Template" meta:resourcekey="Tempres">
                                                <ItemTemplate>
                                                    <%if (Convert.ToString(Session["Culture"]) == "Danish")
                                                      {%>
                                                    <%#Eval("tmpNameDN") %>
                                                    <%} %>
                                                    <%else
                                                      { %>
                                                    <%#Eval("tmpName") %>
                                                    <%} %>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs ItemStyle" />
                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                    Width="15%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                <FooterTemplate>
                                                    <asp:Label runat="server" ID="asdf">
                                                        <asp:Literal ID="Literal17" runat="server" meta:resourcekey="TotalCount" EnableViewState="false" /></asp:Label>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="User" meta:resourcekey="UserRes">
                                                <ItemTemplate>
                                                    <%if (Convert.ToString(Session["Culture"]) == "Danish")
                                                      {%>
                                                    <%#Eval("TeamNameDN") %>
                                                    <%} %>
                                                    <%else
                                                      { %>
                                                    <%#Eval("TeamName") %>
                                                    <%} %>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs ItemStyle" />
                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                    Width="15%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Start Date-End Date" meta:resourcekey="starttoenddate">
                                                <ItemTemplate>
                                                    <%#Eval("StartDate") %> - <%#Eval("EndDate") %>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" CssClass="hidden-xs" />
                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                    Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Mandatory" meta:resourcekey="IsMandatoryres">
                                                <ItemTemplate>
                                                    <%--  <%# (Boolean.Parse(Eval("IsMandatory").ToString())) ? "Yes" : "No" %>--%>
                                                    <asp:Label runat="server" ID="lblmandatory"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" CssClass="hidden-xs" />
                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                    Width="5%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="" ItemStyle-Width="50px"
                                                meta:resourcekey="TemplateFieldResource3">
                                                <ItemTemplate>
                                                    <asp:HiddenField runat="server" ID="hdnassignID" Value='<%# Eval("assignID") %>' />
                                                    <asp:Panel runat="server" ID="viewPanel">
                                                        <i class="fa fa-trash-o"></i>
                                                        <asp:LinkButton ID="lnkBtnName" CssClass="def" runat="server" CommandName="archive" CommandArgument='<%# Eval("assignID") %>'
                                                            ToolTip="Archive"  meta:resourcekey="IsArchive" OnClientClick="var ConfirmArchive = function() { return confirm($('.hdnConfirmArchive').text())};return ConfirmArchive();"></asp:LinkButton>
                                                    </asp:Panel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                    Width="5%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="#cccccc" Font-Bold="True" ForeColor="Black" HorizontalAlign="Left" />
                                    </asp:GridView>
                                </div>

                            </div>

                            <div id="tab-3" class="tab-content">
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                    <ContentTemplate>
                                        <div style="height: 50%">
                                            <asp:GridView ID="gridPublishQues" runat="server" AutoGenerateColumns="False" CellPadding="0"
                                                Width="100%" GridLines="None" DataKeyNames="assignID" CssClass="table1 table-striped table-bordered table-hover table-checkable datatable"
                                                EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>'
                                                BackColor="White"
                                                meta:resourcekey="GridRecordNotfound"
                                                OnRowDataBound="gridPublishQues_RowDataBound"
                                                OnRowCommand="gridPublishQues_RowCommand">

                                                <HeaderStyle CssClass="aa" />
                                                <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                                <AlternatingRowStyle BackColor="White" />
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <img alt="PLUS" style="cursor: pointer" class="assignplussign assignplus" src="../images/plus.png" onclick="assignplussign(this);" />
                                                            <asp:Panel ID="pnlOrders" runat="server" Style="display: none">
                                                                <asp:HiddenField runat="server" ID="hdngridPublishAssignID" Value='<%#Eval("assignID") %>' />

                                                                <asp:HiddenField runat="server" ID="hdngridPublishTempID" Value='<%#Eval("TemplateID") %>' />
                                                                <asp:HiddenField runat="server" ID="hdngridPublishTeamID" Value='<%#Eval("TeamID") %>' />

                                                                <asp:HiddenField runat="server" ID="hdngridPublishQuesIDs" Value='<%#Eval("QuesIDs") %>' />
                                                                <asp:HiddenField runat="server" ID="hdngridPublishUserIDs" Value='<%#Eval("UserIDs") %>' />
                                                                <asp:HiddenField runat="server" ID="hdnIsmandatoryPublish" Value='<%# Eval("IsMandatory") %>' />
                                                                <asp:HiddenField runat="server" ID="hdnStatusPublish" Value='<%# Eval("Status") %>' />

                                                                <asp:Label ID="lblNoRecordFoundQues" Visible="false" ForeColor="Red" runat="server" Text="No record found" meta:resourcekey="RecordNotfoundRes" EnableViewState="false" />
                                                                <div class="col-md-12">
                                                                    <div class="col-md-6">
                                                                        <asp:GridView ID="gvPublishQues_AssignList" runat="server" AutoGenerateColumns="false" EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>'
                                                                            CssClass="table table-striped table-bordered table-hover table-checkable datatable">
                                                                            <Columns>
                                                                                <asp:TemplateField meta:resourcekey="Quesres" HeaderText="Question">
                                                                                    <ItemTemplate>
                                                                                        <%#Eval("Question") %>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                        Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />

                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>

                                                                    <div class="col-md-6">
                                                                        <asp:GridView ID="gvPublishTeamUser_AssignList" runat="server" EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>'
                                                                            AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover table-checkable datatable">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="User Name" meta:resourcekey="UserNameres">
                                                                                    <ItemTemplate>
                                                                                        <%#Eval("userFirstName") %>  <%#Eval("userLastName") %>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                        Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />

                                                                                </asp:TemplateField>

                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </asp:Panel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" CssClass="hidden-xs" />
                                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                            Width="5%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="SrNo" meta:resourcekey="SrNoresource">
                                                        <ItemTemplate>
                                                            <%# Container.DataItemIndex + 1 %>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="left" CssClass="hidden-xs ItemStyle" Width="50px" />
                                                        <HeaderStyle HorizontalAlign="left" CssClass="invoice-head chat-widget-head yellow"
                                                            Width="1%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Template" meta:resourcekey="TemplateRes">
                                                        <ItemTemplate>
                                                            <%if (Convert.ToString(Session["Culture"]) == "Danish")
                                                              {%>
                                                            <%#Eval("tmpNameDN") %>
                                                            <%} %>
                                                            <%else
                                                              { %>
                                                            <%#Eval("tmpName") %>
                                                            <%} %>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs ItemStyle" />
                                                        <HeaderStyle HorizontalAlign="left" CssClass="invoice-head chat-widget-head yellow"
                                                            ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" Width="20%" />
                                                        <FooterTemplate>
                                                            <asp:Label runat="server" ID="asdf">
                                                                <asp:Literal ID="Literal17" runat="server" meta:resourcekey="TotalCount" EnableViewState="false" /></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="User" meta:resourcekey="userres">
                                                        <ItemTemplate>
                                                            <%if (Convert.ToString(Session["Culture"]) == "Danish")
                                                              {%>
                                                            <%#Eval("TeamNameDN") %>
                                                            <%} %>
                                                            <%else
                                                              { %>
                                                            <%#Eval("TeamName") %>
                                                            <%} %>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs ItemStyle" />
                                                        <HeaderStyle HorizontalAlign="left" CssClass="invoice-head chat-widget-head yellow"
                                                            ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" Width="10%" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Start Date-End Date" meta:resourcekey="startdtenddt">
                                                        <ItemTemplate>
                                                            <%#Eval("StartDate") %> - <%#Eval("EndDate") %>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="center" CssClass="hidden-xs" />
                                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                            ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" Width="18%" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Mandatory" meta:resourcekey="IsMandatoryresource">
                                                        <ItemTemplate>
                                                            <%-- <%# (Boolean.Parse(Eval("IsMandatory").ToString())) ? "Yes" : "No" %>--%>
                                                            <asp:Label runat="server" ID="lblmandatoryPublish"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="center" CssClass="hidden-xs" />
                                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                            ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" Width="10%" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Status" meta:resourcekey="statusres">
                                                        <ItemTemplate>
                                                            <%--  <%#Eval("Status") %>--%>
                                                            <asp:Label runat="server" ID="lblStatusPublish"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="center" CssClass="hidden-xs" />
                                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                            ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" Width="8%" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="" ItemStyle-Width="50px"
                                                        meta:resourcekey="TemplateFieldResource3">
                                                        <ItemTemplate>

                                                            <asp:Panel runat="server" ID="viewPanel1">
                                                                <asp:LinkButton ID="lbkPublishQues" CssClass="btn btn-default blue publishButtonStyle" runat="server" Width="85px"
                                                                    CommandName='<%# Eval("Status").ToString() == "Publish" ? "UnPublish" : "Publish" %>' CommandArgument='<%# Eval("assignID") %>'
                                                                    ToolTip='<%# Eval("Status").ToString() == "Publish" ? "UnPublish" : "Publish" %>'>
                                                                    
                                                                    <%# Eval("Status").ToString() == "Publish" ? "UnPublish" : "Publish" %>

                                                                </asp:LinkButton>
                                                            </asp:Panel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                            Width="5%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="" ItemStyle-Width="50px"
                                                        meta:resourcekey="TemplateFieldResource3">
                                                        <ItemTemplate>
                                                            <asp:HiddenField runat="server" ID="hdnassignID" Value='<%# Eval("assignID") %>' />
                                                            <asp:Panel runat="server" ID="viewPanel">
                                                                <i class="fa fa-trash-o"></i>
                                                                <asp:LinkButton ID="lnkBtnName" CssClass="def" runat="server" CommandName="archive" CommandArgument='<%# Eval("assignID") %>'
                                                                    ToolTip="Archive" meta:resourcekey="Archiveres" OnClientClick="var ConfirmArchive = function() { return confirm($('.hdnConfirmArchive').text())};return ConfirmArchive();"></asp:LinkButton>
                                                            </asp:Panel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                            Width="5%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <FooterStyle BackColor="#cccccc" Font-Bold="True" ForeColor="Black" HorizontalAlign="Left" />
                                            </asp:GridView>
                                        </div>
                                        <div style="height: 50%">
                                            <div class="form-group col-md-12" style="margin-top: 2%;">
                                                <div class="col-md-8">
                                                    <asp:Label runat="server" ID="Label31" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px; float: right;">
                                                        <asp:Literal ID="Literal28" runat="server" Text="Display assigned or published question" meta:resourcekey="FilterAssignPublishQuestion" EnableViewState="false" />
                                                    </asp:Label>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="ddpDivision ">

                                                        <asp:DropDownList Width="250px"
                                                            ID="ddlFilterAssignPublishQuestion" runat="server" CssClass="chkliststyle form-control" AppendDataBoundItems="False"
                                                            RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description"
                                                            DataValueField="value" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterAssignPublishQuestion_OnSelectedIndexChanged"
                                                            Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; width: 100%; color: white; overflow: auto;">
                                                            <asp:ListItem Value="All" meta:resourcekey="allresource"></asp:ListItem>
                                                            <asp:ListItem Value="Assign" meta:resourcekey="assignres"></asp:ListItem>
                                                            <asp:ListItem Value="Publish" meta:resourcekey="publishres"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                </div>

                                            </div>
                                            <div class="col-md-12">
                                                <div class="col-md-4">
                                                    <asp:GridView ID="gvTeamUser_PublishList" runat="server" AutoGenerateColumns="false" 
                                                        EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>'
                                                        CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                                                        OnRowCommand="gvTeamUser_PublishList_RowCommand"
                                                        >
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Employees" meta:resourcekey="employee">
                                                                <ItemTemplate>
                                                                    <%#Eval("userFirstName") %>  <%#Eval("userLastName") %>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />

                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Department" meta:resourcekey="Department">
                                                                <ItemTemplate>
                                                                    <%if (Convert.ToString(Session["Culture"]) == "Danish")
                                                                      {%>
                                                                    <%#Eval("TeamNameDN") %>
                                                                    <%} %>
                                                                    <%else
                                                                      { %>
                                                                    <%#Eval("TeamName") %>
                                                                    <%} %>
                                                                    <%-- <%#Eval("TeamName") %>--%>
                                                                    <%-- <asp:HiddenField runat="server" ID="hdnTeamNamePublishUser" Value='<%#Eval("TeamName") %>' />
                                                                    <asp:HiddenField runat="server" ID="hdnTeamNamePublishUserDN" Value='<%#Eval("TeamNameDN") %>' />
                                                                     <asp:Label runat="server" ID="lblTeamNamePublishUser" ></asp:Label>--%>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />

                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="" ItemStyle-Width="50px"
                                                                meta:resourcekey="TemplateFieldResource3">
                                                                <ItemTemplate>

                                                                    <asp:Panel runat="server" ID="viewPanel">
                                                                        <asp:LinkButton ID="lnkView12" CssClass="def" runat="server" CommandName="ViewRow" CommandArgument='<%# Eval("userId") %>'
                                                                            ToolTip="View"><asp:Label runat="server" meta:resourcekey="btnViewresource"></asp:Label></asp:LinkButton>
                                                                    </asp:Panel>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="15%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                            </asp:TemplateField>

                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                                <div class="col-md-8">
                                                    <asp:GridView ID="gvTeamUser_PublishQuesList" runat="server" EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>'
                                                        AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                                                        OnRowDataBound="gvTeamUser_PublishQuesList_RowDataBound">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Question" meta:resourcekey="resQuestion">
                                                                <ItemTemplate>
                                                                    <%#Eval("Question") %>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="SeqNo" meta:resourcekey="resSeqno">
                                                                <ItemTemplate>
                                                                    <%#Eval("SeqNo") %>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Template" meta:resourcekey="resTemplate">
                                                                <ItemTemplate>
                                                                    <asp:HiddenField runat="server" ID="hdntmpNamePublishUser" Value='<%#Eval("tmpName") %>' />
                                                                    <asp:HiddenField runat="server" ID="hdntmpNamePublishUserDN" Value='<%#Eval("tmpNameDN") %>' />
                                                                    <asp:Label runat="server" ID="lbltmpNamePublishUser"></asp:Label>
                                                                    <%--  <%#Eval("tmpName") %>--%>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Mandatory" meta:resourcekey="resIsMandatory">
                                                                <ItemTemplate>
                                                                    <asp:HiddenField runat="server" ID="hdnmandatoryPublishUser" Value='<%#Eval("IsMandatory") %>' />
                                                                    <%-- <%# (Boolean.Parse(Eval("IsMandatory").ToString())) ? "Yes" : "No" %>--%>
                                                                    <asp:Label runat="server" ID="lblmandatoryPublishUser"></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="../Scripts/pickadate.js-3.5.6/lib/legacy.js" type="text/javascript"></script>
    <script src="../Scripts/pickadate.js-3.5.6/lib/picker.js" type="text/javascript"></script>
    <script src="../Scripts/pickadate.js-3.5.6/lib/picker.date.js" type="text/javascript"></script>
    <script src="../Scripts/pickadate.js-3.5.6/lib/picker.time.js" type="text/javascript"></script>
    <script src="../assets/js/libs/tagsInput.js" type="text/javascript"></script>
    <script type="text/javascript">
        function setSession()
        {
            sessionStorage.setItem("test","2");
        }
        //$('.plussign').on("click", function () {
        function plussign(a)
        {
            if ($(a).hasClass('plus')) {
                $(a).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(a).next().html() + "</td></tr>");
                $(a).attr("src", "../images/minus.png");
                $(a).removeClass('plus');
                $(a).addClass('minus');
            }
            else {
                $(a).attr("src", "../images/plus.png");
                $(a).closest("tr").next().remove();
                $(a).removeClass('minus');
                $(a).addClass('plus');
            }

            //});
        }


        // $('.teamplussign').on("click", function () {
        function teamplussign(a){

            if ($(a).hasClass('teamplus')) {
                $(a).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(a).next().html() + "</td></tr>");
                $(a).attr("src", "../images/minus.png");
                $(a).removeClass('teamplus');
                $(a).addClass('teamminus');
            }
            else {
                $(a).attr("src", "../images/plus.png");
                $(a).closest("tr").next().remove();
                $(a).removeClass('teamminus');
                $(a).addClass('teamplus');
            }

            //  });
        }

        // $('.assignplussign').on("click", function () {
        function assignplussign(a)
        {
         
            if ($(a).hasClass('assignplus')) {
                $(a).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(a).next().html() + "</td></tr>");
                $(a).attr("src", "../images/minus.png");
                $(a).removeClass('assignplus');
                $(a).addClass('assignminus');
            }
            else {
                $(a).attr("src", "../images/plus.png");
                $(a).closest("tr").next().remove();
                $(a).removeClass('assignminus');
                $(a).addClass('assignplus');
            }
             
            //  });
        }
        
    </script>
    <script type="text/javascript">
        function clearData()
        {
           
            //$('#ContentPlaceHolder1_txtQuestion').val('');
            //$('#ContentPlaceHolder1_txtquesDescription').val('');
            //$('#ContentPlaceHolder1_txtSeqNo').val('');
            //$('#ContentPlaceHolder1_txtqcatName').val('');
            //$('#ContentPlaceHolder1_txtqcatNameDN').val('');
            //$('#ContentPlaceHolder1_txtqcatDescription').val('');
            //$('#ContentPlaceHolder1_txttmpName').val('');
            //$('#ContentPlaceHolder1_txttmpNameDN').val('');
            //$('#ContentPlaceHolder1_txttmpDescription').val('');
            //$("#ContentPlaceHolder1_ddtTemplate").val('');
            //$("#ContentPlaceHolder1_ddtTemplate").multiselect("refresh");
            
        }
    </script>
    <script type="text/javascript">
        function closeModel(a) {

            if (CheckValidations('chkdoc')) { $('#' + a).modal('hide'); }

        }


        function closeModelCancel(a) { $('#' + a).modal('hide'); }

        function CheckQuesTempValue()
        {
            if($('.multiselect-selected-text').text()=='No option selected')
            {
                //  alert('empty');
                $('#ContentPlaceHolder1_ddtTemplateVal').val('0');
            }
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            var curTab=sessionStorage.getItem("test");
            if(curTab!=null && curTab!='' && curTab=="2")
            {
                sessionStorage.setItem("test","0");
                DisplayTab(curTab);
            }
            $('#ContentPlaceHolder1_ddtTemplate').multiselect({

                //enableFiltering: true,
                includeSelectAllOption: true,
                //buttonWidth: '400px',
                selectAllText: 'All Template',
                //  //nonSelectedText: 'None Location selected',
                allSelectedText: 'All Template',
                //  nSelectedText: 'selected',
                //  numberDisplayed: 2,


                buttonText: function (options) {
                  
                    if (options.length === 0) {
                        return $('#ContentPlaceHolder1_hdnNoOption').val();
                    }
                    var labels = [];
                    var num = [];

                    options.each(function () {
                      
                        if ($(this).attr('value') !== undefined) {
                            labels.push($(this).text());
                            num.push($(this).val());

                        }
                    });
                    
                    $('#ContentPlaceHolder1_ddtTemplateVal').val(num.join(','));
                    return labels.join(',');
                }
                //}).on('hide.bs.dropdown', function () {
                //    alert('drop down hide');
                //});
                //}).on('blur', function () {
                //    alert('drop down hide');
            });


            var dataarray = $('#ContentPlaceHolder1_ddtTemplateVal').val().split(",");
            $("#ContentPlaceHolder1_ddtTemplate").val(dataarray);
            $("#ContentPlaceHolder1_ddtTemplate").multiselect("refresh");

        });

        function DisplayTab(a) {
           
            if (a == 1) {
                $("#tab-1").show();
                $(".maintab li").removeClass("current");
                $("#t1").addClass("current");
                $("#tab-2").hide();
                $("#tab-3").hide();
            }
            else if (a == 2) {
                $("#tab-1").hide();
                $("#tab-2").show();
                $(".maintab li").removeClass("current");
                $("#t2").addClass("current");
                $("#tab-3").hide();
                //sessionStorage.setItem("test","2");
                var curTab=sessionStorage.getItem("test");
                if(curTab!=null && curTab!='' && curTab=="2")
                {
                    window.open('Question.aspx',"_self");
                }
            }
            else if (a == 3) {
                $("#tab-1").hide();
                $("#tab-2").hide();
                $("#tab-3").show();
                $(".maintab li").removeClass("current");
                $("#t3").addClass("current");
            }
        }
    </script>



    <script type="text/javascript">
      
        function SelectQuesTemplate(a,b,c) {
            //   alert('SelectQuesTemplate');
            c=c.trim();
            b=b.trim();
            if(a.checked)
            {
                debugger;
                $('#Ques'+b+c).next().val('1');
                if($('#ContentPlaceHolder1_hdnSelectedQuestionIDs').val()!='' && $('#ContentPlaceHolder1_hdnSelectedQuestionIDs').val()!=null && $('#ContentPlaceHolder1_hdnSelectedQuestionIDs').val()!="0")
                {
                  
                    var tmp=$('#ContentPlaceHolder1_hdnSelectedQuestionIDs').val();
                    var array = tmp.split(',');

                    if(tmp.indexOf(c)<0)
                    {
                        array.push(c);
                        $('#ContentPlaceHolder1_hdnSelectedQuestionIDs').val(array.join(','));
                    }
                }
                else{
                    $('#ContentPlaceHolder1_hdnSelectedQuestionIDs').val(c);
                }
            }
            else{
                debugger;
                $('#Ques'+b+c).next().val('0');
                if($('#ContentPlaceHolder1_hdnSelectedQuestionIDs').val()!='' && $('#ContentPlaceHolder1_hdnSelectedQuestionIDs').val()!=null && $('#ContentPlaceHolder1_hdnSelectedQuestionIDs').val()!="0")
                {
                    var tmp=$('#ContentPlaceHolder1_hdnSelectedQuestionIDs').val();
                    var array = tmp.split(',');
                    if(tmp.indexOf(c)>-1)
                    {
                        index=(array.indexOf(c)) ;
                        array.splice(index, 1);
                        $('#ContentPlaceHolder1_hdnSelectedQuestionIDs').val(array.join(','));
                    }
                }
                $('.MainQues'+b).prop("checked",false);
            }
            //alert($('#Ques'+b+c).next().val());
        }
        function SelectMainQuesTemplate(a,b) {
            // alert('SelectMainQuesTemplate');
            debugger;
            b=b.trim();
            if($('.MainQues'+b).prop("checked")==false)
            {
                $('#MainQues'+b).next().val('0');
                if($('#ContentPlaceHolder1_hdnSelectedTemplateIDs').val()!='' && $('#ContentPlaceHolder1_hdnSelectedTemplateIDs').val()!=null && $('#ContentPlaceHolder1_hdnSelectedTemplateIDs').val()!="0")
                {
                    var tmp=$('#ContentPlaceHolder1_hdnSelectedTemplateIDs').val();
                    var array = tmp.split(',');
                    if(tmp.indexOf(b)>-1)
                    {
                        index=(array.indexOf(b)) ;
                        array.splice(index, 1);
                        $('#ContentPlaceHolder1_hdnSelectedTemplateIDs').val(array.join(','));
                    }
                }
                $('.Ques'+b).prop("checked", false);
                $('.Ques'+b).each(function() {
                    $(this).next().val('0');
                });
            }
            else
            {
                $('#MainQues'+b).next().val('1');
                if($('#ContentPlaceHolder1_hdnSelectedTemplateIDs').val()!='' && $('#ContentPlaceHolder1_hdnSelectedTemplateIDs').val()!=null && $('#ContentPlaceHolder1_hdnSelectedTemplateIDs').val()!="0")
                {
                    var tmp=$('#ContentPlaceHolder1_hdnSelectedTemplateIDs').val();
                    var array = tmp.split(',');
                    if(tmp.indexOf(b)<0)
                    {
                        array.push(b);
                        $('#ContentPlaceHolder1_hdnSelectedTemplateIDs').val(array.join(','));
                    }
                }
                else{
                    $('#ContentPlaceHolder1_hdnSelectedTemplateIDs').val(b);
                }

                $('.Ques'+b).prop("checked", true);
                
                $('.Ques'+b).each(function() {
                    $(this).next().val('1');
                });
            }
        }

        function SelectUserTemplate(a,b,c) {
            // alert('SelectUserTemplate');
            c=c.trim();
            b=b.trim();

            if(a.checked)
            {
                $('#User'+b+c).next().val('1');
                if($('#ContentPlaceHolder1_hdnSelectedUserIDs').val()!='' && $('#ContentPlaceHolder1_hdnSelectedUserIDs').val()!=null && $('#ContentPlaceHolder1_hdnSelectedUserIDs').val()!="0")
                {
                  
                    var tmp=$('#ContentPlaceHolder1_hdnSelectedUserIDs').val();
                    var array = tmp.split(',');

                    if(tmp.indexOf(c)<0)
                    {
                        array.push(c);
                        $('#ContentPlaceHolder1_hdnSelectedUserIDs').val(array.join(','));
                    }
                }
                else{
                    $('#ContentPlaceHolder1_hdnSelectedUserIDs').val(c);
                }
            }
            else{
                $('#User'+b+c).next().val('0');
                if($('#ContentPlaceHolder1_hdnSelectedUserIDs').val()!='' && $('#ContentPlaceHolder1_hdnSelectedUserIDs').val()!=null && $('#ContentPlaceHolder1_hdnSelectedUserIDs').val()!="0")
                {
                    var tmp=$('#ContentPlaceHolder1_hdnSelectedUserIDs').val();
                    var array = tmp.split(',');
                    if(tmp.indexOf(c)>-1)
                    {
                        index=(array.indexOf(c)) ;
                        array.splice(index, 1);
                        $('#ContentPlaceHolder1_hdnSelectedUserIDs').val(array.join(','));
                    }
                }
                $('.TeamUser'+b).prop("checked",false);
            }
            //alert($('#ContentPlaceHolder1_hdnSelectedUserIDs').val());
        }
        function SelectTeamUserTemplate(a,b) {
            // alert('SelectTeamUserTemplate');
            debugger;
            b=b.trim();
            if($('.TeamUser'+b).prop("checked")==false)
            {
                if($('#ContentPlaceHolder1_hdnSelectedTeamIDs').val()!='' && $('#ContentPlaceHolder1_hdnSelectedTeamIDs').val()!=null && $('#ContentPlaceHolder1_hdnSelectedTeamIDs').val()!="0")
                {
                    var tmp=$('#ContentPlaceHolder1_hdnSelectedTeamIDs').val();
                    var array = tmp.split(',');
                    if(tmp.indexOf(b)>-1)
                    {
                        index=(array.indexOf(b)) ;
                        array.splice(index, 1);
                        $('#ContentPlaceHolder1_hdnSelectedTeamIDs').val(array.join(','));
                    }
                }
                $('.User'+b).prop("checked", false);

                $('.User'+b).each(function() {
                    $(this).next().val('0');
                });
            }
            else
            {
                if($('#ContentPlaceHolder1_hdnSelectedTeamIDs').val()!='' && $('#ContentPlaceHolder1_hdnSelectedTeamIDs').val()!=null && $('#ContentPlaceHolder1_hdnSelectedTeamIDs').val()!="0")
                {
                    var tmp=$('#ContentPlaceHolder1_hdnSelectedTeamIDs').val();
                    var array = tmp.split(',');
                    if(tmp.indexOf(b)<0)
                    {
                        array.push(b);
                        $('#ContentPlaceHolder1_hdnSelectedTeamIDs').val(array.join(','));
                    }
                }
                else{
                    $('#ContentPlaceHolder1_hdnSelectedTeamIDs').val(b);
                }

                $('.User'+b).prop("checked", true);
                $('.User'+b).each(function() {
                    $(this).next().val('1');
                });
            }
            //alert($('#ContentPlaceHolder1_hdnSelectedTeamIDs').val());
        }
        //function SelectTeamUserTemplate(a,b) {
           
        //    if($('.TeamUser'+b).prop("checked")==false)
        //    {
              
        //        $('.Team'+b).prop("checked", false);
        //    }
        //    else
        //    {
              
        //        $('.TeamUser'+b).prop("checked", true);
        //    }
        //}
        //function SelectTeamTemplate(a,b) {
           
        //    if($('.Team'+b).prop("checked")==false)
        //    {
        //        $('.TeamUser'+b).prop("checked", false);
        //    }
        //    else
        //    {
        //        $('.TeamUser'+b).prop("checked", true);
        //    }
        //}
       
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            setTimeout(function () {
                SetDatePicker();
            }, 1500);

            var yesterday = new Date((new Date()).valueOf() - 1000 * 60 * 60 * 24);

            $(".txtActStartDate").pickadate({
                format: datePickerFormate, disable: [
                    { from: [0, 0, 0], to: yesterday }
                ]
            });


            $(".txtActEndDate").pickadate({
                format: datePickerFormate, disable: [
                    { from: [0, 0, 0], to: yesterday }
                ]
            });


            $(".datepicker1").pickadate({
                format: datePickerFormate,
                hide: function() {
                    $("select#ContentPlaceHolder1_ddlFilterTemplate").change();
                }
            });

        });
        var datePickerFormate = 'dd/mm/yyyy';
        function SetDatePicker() {

            var yesterday = new Date((new Date()).valueOf() - 1000 * 60 * 60 * 24);

            $(".txtActStartDate").pickadate({
                format: datePickerFormate, disable: [
                    { from: [0, 0, 0], to: yesterday }
                ]
            });


            $(".txtActEndDate").pickadate({
                format: datePickerFormate, disable: [
                    { from: [0, 0, 0], to: yesterday }
                ]
            });


            $(".datepicker1").pickadate({
                format: datePickerFormate,
                onClose: function() {
                   
                    $("select#ContentPlaceHolder1_ddlFilterTemplate").change();
                   
                }
            });

            ///


            $('.txtActStartDate').on('change', function () {
                $('.txtActEndDate').pickadate('picker').set('min', $(this).val());
            });
        }

        function parseJsonDate(jsonDateString) {


            //return new Date(parseInt(jsonDateString.replace('/Date(', '')));

            var formattedDate = new Date(parseInt(jsonDateString.substr(6)));
            var d = formattedDate.getDate();
            var m = formattedDate.getMonth();
            m += 1; // JavaScript months are 0-11
            var y = formattedDate.getFullYear();
            var min = formattedDate.getMinutes();
            var hour = formattedDate.getHours();
            var sec = formattedDate.getSeconds();

            //$("#txtDate").val(d + "." + m + "." + y);
            return d + "/" + m + "/" + y + " " + hour + ":" + min + ":" + sec;
        }

        function formatDate(d) {
            if (hasTime(d)) {
                var s = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
                s += ' ' + d.getHours() + ':' + zeroFill(d.getMinutes()) + ':' + zeroFill(d.getSeconds());
            } else {
                var s = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
            }

            return s;
        }
    </script>
    <%-- <script type="text/javascript">
        function OnChangeCheckbox (checkbox) {
            if (checkbox.checked) {
                alert ("The check box is checked.");
            }
            else {
                alert ("The check box is not checked.");
            }
        }
    </script>--%>
</asp:Content>
