﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true" CodeFile="myCompetence.aspx.cs" Inherits="Organisation_myCompetence"
    Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .HiddenText label {
            display: none;
        }
    </style>
    <style type="text/css">
        .treeNode {
        }

            .treeNode input {
                width: auto;
                margin: 5px;
                float: left !important;
            }

        .chat-widget-head {
            float: left;
            width: 100%;
        }
    </style>
    <style type="text/css">
        .LabelEllipsisStyle {
            text-overflow: ellipsis;
            white-space: nowrap;
            display: block;
            overflow: hidden;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style="display: none">
        <asp:Literal ID="View" runat="server" meta:resourcekey="View" EnableViewState="false" />
        <asp:Literal ID="RequestHelp" runat="server" meta:resourcekey="RequestHelp" EnableViewState="false" />
    </div>
    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" meta:resourcekey="lblMsgResource1"></asp:Label>
    <div class="col-md-6">
        <%--<div class="heading-sec">
            <h1>Dashboard  <i>Welcome To Bette Bayer!</i></h1>
        </div>--%>
        <div class="heading-sec">
            <h1>
                <%--  <%= CommonMessages.CompetencesSkill%>--%>
                <asp:Literal ID="Literal1" runat="server" meta:resourcekey="CompetencesSkill" EnableViewState="false" />
                <i><span runat="server" id="Skills"></span></i>
            </h1>
        </div>
    </div>
    <!--<li class="range">
<a href="#">
<i class="icon-calendar"></i>
<span>August 5, 2014 - September 3, 2014</span>
<i class="icon-angle-down"></i>
</a>
</li>-->
    <div class="col-md-6 com_top_btn">
        <%--<div class="heading-sec">
            <h1>Dashboard  <i>Welcome To Bette Bayer!</i></h1>
        </div>--%>
        <div class="heading-seca" style="float: right">
            
             <asp:Button runat="server" ID="Button2" meta:resourcekey="PDP" CssClass="yellow"
                Style="border-radius: 5px; height: 32px; color: white;display:none;"
                OnClick="RedirectToPDP"
               />
            

            <asp:Button runat="server" ID="btnlocal" Text='Set Competencies' CssClass="yellow"
                Style="border-radius: 5px; height: 32px; color: white;" PostBackUrl='CompetenceSkill.aspx'
                meta:resourcekey="btnSetCompetencies" />
            <asp:Button runat="server" ID="Button1" Text='Notification Center' PostBackUrl="OrgInvitation_request.aspx"
                CssClass="yellow" Style="border-radius: 5px; height: 32px; color: white;display:none;" meta:resourcekey="btnNotification" />
            
            <asp:Button runat="server" ID="btnActivity" Text='Notification Center' PostBackUrl="ActivityLists.aspx"
                CssClass="yellow" Style="border-radius: 5px; height: 32px; color: white;display:none;" meta:resourcekey="Activities" />
            
           
        </div>
    </div>

    <div class="col-md-12">
        <div id="graph-wrapper">
            <h3 class="custom-heading darkblue">
                <%--Lorem Ipsum is simply dummy text--%>
                <%--<%= CommonMessages.CompetencesSkill%>--%>
                <asp:Literal ID="Literal2" runat="server" meta:resourcekey="CompetencesSkillResource1" EnableViewState="false" />
            </h3>
        </div>
        <!-- Widget Visitor -->
        <!-- ORDR REVIEWS -->
    </div>
    <div style="clear: both"></div>
    <div class="col-md-12" style="width: 100%;">
        <div class="col-md-12 comp_table yellow1 " style="float: left; margin-left: 0px; margin-right: 10px;">
            <div class=" ">
                <div class=" ">
                    <asp:UpdatePanel runat="server" ID="UpdatePanedrop" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class=" ">
                                <div class="chat-widget-head yellow1 yellow-radius">
                                    <h4>
                                        <%--  <%= CommonMessages.YourTop3Competence%> --%>
                                        <asp:Literal ID="Literal3" runat="server" meta:resourcekey="YourTop3Competence" EnableViewState="false" /></h4>
                                    &nbsp;
                                </div>
                                <asp:Repeater ID="rep_competence" runat="server" OnItemDataBound="R1_ItemDataBound"
                                    OnItemCommand="Repeater1_OnItemCommand">
                                    <HeaderTemplate>

                                        <table width="100%" border="1" bordercolor="#dfdfdf" class=" skill_table" id="tblContacts"
                                            style="background-color: #fff; text-align: left;">

                                            <thead>

                                                <th style="text-align: left;" width="20%"></th>
                                                <th style="text-align: left;" width="10%">
                                                    <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Local" EnableViewState="false" />
                                                    <%--                                               <%= CommonMessages.Local%>  --%>
                                                </th>
                                                <th style="text-align: left" width="10%">
                                                    <%-- <%= CommonMessages.Achieve%>  --%>
                                                    <asp:Literal ID="Literal4" runat="server" meta:resourcekey="Achieve" EnableViewState="false" />
                                                </th>
                                                <th style="text-align: left" width="10%">
                                                    <asp:Literal ID="Literal14" runat="server" meta:resourcekey="Target" EnableViewState="false" />

                                                </th>

                                                <th style="text-align: left" width="10%">
                                                    <asp:Literal ID="Literal5" runat="server" meta:resourcekey="Comparison" EnableViewState="false" />
                                                    <%--  <%= CommonMessages.Comparison%>  --%>
                                                </th>

                                                <th style="text-align: left" width="10%">
                                                    <%-- <%= CommonMessages.NeedHelp%> --%>
                                                    <asp:Literal ID="Literal6" runat="server" meta:resourcekey="NeedHelp" EnableViewState="false" />?
                                                </th>

                                                <th style="text-align: left">
                                                    <asp:Literal ID="Literal13" runat="server" Text="Comment" meta:resourcekey="Comment" EnableViewState="false" />

                                                </th>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:LinkButton ID="lnkCatName" runat="server" Text='<%# Eval("comCompetence" ) %>'
                                                    CommandName="catNameClick" CommandArgument='<%# Eval("skillComId") %>' Visible="False"
                                                    meta:resourcekey="lnkCatNameResource1"></asp:LinkButton>
                                                <asp:Label ID="lblCatName" runat="server" Text='<%# Eval("comCompetence" ) %>'
                                                    ToolTip='<%# Eval("comCompetence" ) %>' CssClass="LabelEllipsisStyle"></asp:Label>

                                                <asp:HiddenField ID="comId" runat="server" Value='<%# Eval("skillComId") %>' />
                                                <asp:HiddenField ID="comCompetence" runat="server" Value='<%# Eval("comCompetence") %>' />
                                            </td>
                                            <td style="display: none;">
                                                <asp:Label ID="lbpoint" runat="server" Text="0" meta:resourcekey="lbpointResource1"></asp:Label>
                                            </td>
                                            <td style="display: none;">
                                                <label>
                                                    <asp:CheckBox ID="checkbox1" runat="server" onclick="CheckBoxCheck(this);" meta:resourcekey="checkbox1Resource1" />
                                                </label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lbllocal" runat="server" Text='<%# Eval("skillLocal") %>' meta:resourcekey="lbllocalResource1"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblAchive" runat="server" Text='<%# Eval("skillAchive") %>' meta:resourcekey="lblAchiveResource1"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lbltarget" runat="server" Text='<%# Eval("skilltarget") %>' meta:resourcekey="lbltargetResource1"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblLtotal" runat="server" Text="N/A" Visible="False" meta:resourcekey="lblLtotalResource1"></asp:Label>
                                                <asp:Button runat="server" ID="btnlocal" Text='<%# Eval("Ltotal") %>' CssClass="yellow"
                                                    Style="border-radius: 5px; height: 32px; color: white;" PostBackUrl='<%# String.Format("HelpCompetence.aspx?Lid={0}&Gid={1}", Eval("skillComId"), Eval("skillAchive")) %>'
                                                    meta:resourcekey="btnlocalResource1" />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblGtotal" runat="server" Text="N/A" Visible="False" meta:resourcekey="lblGtotalResource1"></asp:Label>
                                                <asp:Button runat="server" ID="btnNeedhelp" Text='<%# Eval("Gtotal" ) %>' CssClass="yellow"
                                                    Style="border-radius: 5px; height: 32px; color: white;" PostBackUrl='<%# String.Format("HelpCompetence.aspx?Hid={0}&Nid={1}", Eval("skillComId"), Eval("skillAchive")) %>'
                                                    meta:resourcekey="btnNeedhelpResource1" />
                                            </td>

                                            <td>
                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("skillComment") %>'></asp:Label>

                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <tr id="Emptyrow" runat="server" visible="False">
                                            <td runat="server">
                                                <asp:Label ID="lblEmptyData" Text="No Data To Display" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
        <div class="col-md-12 comp_table yellow1" style="float: left; margin-left: 0px;">
            <div class=" " style="border-radius: 5px;">
                <div class=" ">
                    <div class=" ">
                        <div class="chat-widget-head yellow1 yellow-radius">
                            <h4>
                                <asp:Literal ID="Literal6" runat="server" meta:resourcekey="YourBottom3Competence"
                                    EnableViewState="false" />
                                <%--<%= CommonMessages.YourBottom3Competence%> --%>
                            </h4>
                        </div>
                        <asp:Repeater ID="rep_competencehigh" runat="server" OnItemDataBound="rep_competencehigh_ItemDataBound"
                            OnItemCommand="rep_competencehigh_OnItemCommand">
                            <HeaderTemplate>
                                <table width="100%" border="1" bordercolor="#dfdfdf" class="skill_table" id="tblContacts1"
                                    style="background-color: #fff; text-align: left;">
                                    <thead>
                                        <th style="text-align: left" width="20%"></th>
                                        <th style="text-align: left;" width="10%">
                                            <%-- <%= CommonMessages.Local%> --%>
                                            <asp:Literal ID="Literal6" runat="server" meta:resourcekey="Local" EnableViewState="false" />
                                        </th>
                                        <th style="text-align: left" width="10%">
                                            <%-- <%= CommonMessages.Achieve%>  --%>
                                            <asp:Literal ID="Literal7" runat="server" meta:resourcekey="Achieve" EnableViewState="false" />
                                        </th>
                                        <th style="text-align: left" width="10%">
                                            <%--  Target--%>
                                            <asp:Literal ID="Literal15" runat="server" meta:resourcekey="Target" EnableViewState="false" />

                                        </th>
                                        <th style="text-align: left" width="10%">
                                            <asp:Literal ID="Literal8" runat="server" meta:resourcekey="Comparison" EnableViewState="false" />
                                            <%-- <%= CommonMessages.Comparison%> --%>
                                        </th>
                                        <th style="text-align: left" width="10%">
                                            <%--    <%= CommonMessages.NeedHelp%>--%>
                                            <asp:Literal ID="Literal9" runat="server" meta:resourcekey="NeedHelp" EnableViewState="false" />
                                            ?
                                        </th>
                                        <th style="text-align: left">
                                            <asp:Literal ID="Literal13" runat="server" Text="Comment" meta:resourcekey="Comment" EnableViewState="false" />
                                            <%--  <%= CommonMessages.Comparison%>  --%>
                                        </th>
                                    </thead>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkCatName" runat="server" Text='<%# Eval("comCompetence" ) %>'
                                            CommandName="catNameClick" CommandArgument='<%# Eval("skillComId") %>' Visible="False"
                                            meta:resourcekey="lnkCatNameResource2"></asp:LinkButton>
                                        <asp:Label ID="lblCatName" runat="server" Text='<%# Eval("comCompetence" ) %>'
                                            ToolTip='<%# Eval("comCompetence" ) %>' CssClass="LabelEllipsisStyle"></asp:Label>
                                        <asp:HiddenField ID="comId" runat="server" Value='<%# Eval("skillComId") %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbllocalhigh" runat="server" Text='<%# Eval("skillLocal") %>' meta:resourcekey="lbllocalhighResource1"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblAchivehigh" runat="server" Text='<%# Eval("skillAchive") %>' meta:resourcekey="lblAchivehighResource1"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbltargethigh" runat="server" Text='<%# Eval("skilltarget") %>' meta:resourcekey="lbltargethighResource1"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblLtotal" runat="server" Text="N/A" Visible="False" meta:resourcekey="lblLtotalResource2"></asp:Label>
                                        <asp:Button runat="server" ID="btnlocal" Text='<%# Eval("Ltotal") %>' CssClass="yellow"
                                            Style="border-radius: 5px; height: 32px; color: white;" PostBackUrl='<%# String.Format("HelpCompetence.aspx?Lid={0}&Gid={1}", Eval("skillComId"), Eval("skillAchive")) %>'
                                            meta:resourcekey="btnlocalResource2" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblGtotal" runat="server" Text="N/A" Visible="False" meta:resourcekey="lblGtotalResource2"></asp:Label>
                                        <asp:Button runat="server" ID="btnNeedhelp" Text='<%# Eval("Gtotal") %>' CssClass="yellow"
                                            Style="border-radius: 5px; height: 32px; color: white;" PostBackUrl='<%# String.Format("HelpCompetence.aspx?Hid={0}&Nid={1}", Eval("skillComId"), Eval("skillAchive")) %>'
                                            meta:resourcekey="btnNeedhelpResource2" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label2" runat="server" Text='<%# Eval("skillComment") %>'></asp:Label>

                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                <tr id="Emptyrow" runat="server" visible="False">
                                    <td runat="server">
                                        <asp:Label ID="lblEmptyData" Text="No Data To Display" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container comptable_last">
        <div class="col-md-12 comp_table yellow1" style="padding: 0!important; margin-left: 0px; margin-top: 10px; width: 100%;">
            <div class="chat-widget widget-body" style="border-radius: 5px;">
                <div class="chat-widget-head yellow1 yellow-radius">
                    <h4>

                        <asp:Literal ID="Literal9" runat="server" meta:resourcekey="Competence" EnableViewState="false" />
                        <%--  <%= CommonMessages.Competence%>--%></h4>
                </div>
            </div>
            <div style="background: #fff;">
                <%--<asp:Panel ID="pnlShowCountry" runat="server">
            <cc1:Accordion ID="Accordion1" runat="server" TransitionDuration="100" SelectedIndex="-1"
                FramesPerSecond="200" FadeTransitions="true" RequireOpenedPane="false" OnItemDataBound="Accordion1_ItemDataBound"
                ContentCssClass="accordion-content" HeaderCssClass="accordion-header" HeaderSelectedCssClass="accordion-selected"
                Width="100%" CssClass="Accordion1">
                <HeaderTemplate>
                    <%#DataBinder.Eval(Container.DataItem, "catName")%>
                </HeaderTemplate>
                <ContentTemplate>
                    <asp:HiddenField ID="txt_categoryID" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"catId") %>' />--%>
                <asp:Repeater ID="rptAll" runat="server" OnItemDataBound="rptAll_ItemDataBound" OnItemCommand="rptAll_OnItemCommand">
                    <HeaderTemplate>
                        <table width="100%" border="1" bordercolor="#dfdfdf" style="background: #fff; text-align: left;"
                            class="skill_table" id="tblContacts">
                            <thead>
                                <th style="text-align: left" width="20%"></th>
                                <th style="text-align: left;" width="10%">
                                    <%--    <%= CommonMessages.Local%>  --%>
                                    <asp:Literal ID="Literal9" runat="server" meta:resourcekey="Local" EnableViewState="false" />
                                </th>
                                <th style="text-align: left" width="10%">
                                    <asp:Literal ID="Literal10" runat="server" meta:resourcekey="Achieve" EnableViewState="false" />
                                    <%-- <%= CommonMessages.Achieve%> --%>
                                </th>
                                <th style="text-align: left" width="10%">
                                    <%--Target--%>
                                    <asp:Literal ID="Literal16" runat="server" meta:resourcekey="Target" EnableViewState="false" />
                                </th>
                                <th style="text-align: left" width="10%">
                                    <asp:Literal ID="Literal11" runat="server" meta:resourcekey="Comparison" EnableViewState="false" />
                                    <%-- <%= CommonMessages.Comparison%>  --%>
                                </th>
                                <th style="text-align: left" width="10%">
                                    <%-- <%= CommonMessages.NeedHelp%> --%>
                                    <asp:Literal ID="Literal12" runat="server" meta:resourcekey="NeedHelp" EnableViewState="false" />?
                                </th>
                                <th style="text-align: left">
                                    <asp:Literal ID="Literal13" runat="server" Text="Comment" meta:resourcekey="Comment" EnableViewState="false" />
                                    <%--  <%= CommonMessages.Comparison%>  --%>
                                </th>
                            </thead>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:LinkButton ID="lnkCatName" runat="server" Text='<%# Eval("comCompetence" ) %>'
                                    CommandName="catNameClick" CommandArgument='<%# Eval("skillComId") %>' Visible="False"
                                    meta:resourcekey="lnkCatNameResource3"></asp:LinkButton>
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("comCompetence" ) %>'
                                    ToolTip='<%# Eval("comCompetence" ) %>' CssClass="LabelEllipsisStyle"></asp:Label>
                                <asp:HiddenField ID="comId" runat="server" Value='<%# Eval("skillComId") %>' />
                                <asp:HiddenField ID="comCompetence" runat="server" Value='<%# Eval("comCompetence") %>' />
                            </td>
                            <td>
                                <asp:Label ID="lbllocalhigh" runat="server" Text='<%# Eval("skillLocal") %>' meta:resourcekey="lbllocalhighResource2"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblAchive" runat="server" Text='<%# Eval("skillAchive") %>' meta:resourcekey="lblAchiveResource2"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lbltarget" runat="server" Text='<%# Eval("skilltarget") %>' meta:resourcekey="lbltargetResource2"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblLtotal" runat="server" Text="N/A" Visible="False" meta:resourcekey="lblLtotalResource3"></asp:Label>
                                <asp:Button runat="server" ID="btnlocal" Text='<%# Eval("Ltotal") %>' CssClass="yellow"
                                    Style="border-radius: 5px; height: 32px; color: white;" PostBackUrl='<%# String.Format("HelpCompetence.aspx?Lid={0}&Gid={1}", Eval("skillComId"), Eval("skillAchive")) %>'
                                    meta:resourcekey="btnlocalResource3" />
                            </td>
                            <td>
                                <asp:Label ID="lblGtotal" runat="server" Text="N/A" Visible="False" meta:resourcekey="lblGtotalResource3"></asp:Label>
                                <asp:Button runat="server" ID="btnNeedhelp" Text='<%# Eval("Gtotal") %>' CssClass="yellow"
                                    Style="border-radius: 5px; height: 32px; color: white;" PostBackUrl='<%# String.Format("HelpCompetence.aspx?Hid={0}&Nid={1}", Eval("skillComId"), Eval("skillAchive")) %>'
                                    meta:resourcekey="btnNeedhelpResource3" />
                            </td>
                            <td>
                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("skillComment") %>'></asp:Label>

                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <%--</ContentTemplate>
            </cc1:Accordion>
        </asp:Panel>--%>
            </div>
        </div>
    </div>
    <asp:UpdatePanel runat="server" ID="updlocal">
        <ContentTemplate>
            <asp:LinkButton ID="lnkFake" runat="server" meta:resourcekey="lnkFakeResource1" />
            <cc1:ModalPopupExtender ID="mpe" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkFake"
                CancelControlID="btnClose" BackgroundCssClass="modalBackground" DynamicServicePath=""
                Enabled="True">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="margin-left: 170px !important; width: 17% !important; position: absolute !important; z-index: 100001 !important; top: 117px !important; display: none;"
                meta:resourcekey="pnlPopupResource1">
                <div class="modal-header blue" style="color: #ffffff;">
                    <h4 class="modal-title">
                        <%-- <%= CommonMessages.Competence%>--%>
                        <asp:Literal ID="Literal12" runat="server" meta:resourcekey="Competence" EnableViewState="false" />
                    </h4>
                    <div style="float: right; margin-top: -26px;">
                        <asp:Button ID="btnClose" runat="server" Text="×" CssClass="close" meta:resourcekey="btnCloseResource1" />
                    </div>
                </div>
                <div class="modal-body">
                    <asp:Repeater ID="rptlocal" runat="server" OnItemDataBound="rptlocal_ItemDataBound"
                        OnItemCommand="rptlocal_ItemCommand">
                        <HeaderTemplate>
                            <table width="100%" border="1" bordercolor="#dfdfdf" class="skill_table" id="tblContacts" style="text-align: left;">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td width="49%">
                                    <asp:Label ID="lblsubcom" runat="server" Text='<%# Eval("skillComId") %>' meta:resourcekey="lblsubcomResource1"></asp:Label>
                                    <asp:HiddenField ID="userId" runat="server" Value='<%# Eval("skillUserId") %>' />
                                    <asp:HiddenField ID="userFirstName" runat="server" Value='<%# Eval("userFirstName") %>' />
                                </td>
                                <td width="13%" id="tdControl" runat="server"></td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!-- TIME LINE -->
    <!-- Recent Post -->
    <div class="col-md-3">
    </div>
    <div class="modal-footer" style="border-top: 0px !important; margin-top: -50px; margin-left: 7px;">
        <asp:Button runat="server" ID="btnBack" meta:resourcekey="Back"  CssClass="btn black pull-right"
            OnClick="btnBack_click" Style="margin: 4px; display: none" />
        <%--  <a href="#" onclick="javascript:history.back(); return false;" class="btn black pull-right" style="border-radius: 5px;margin-left:0px;margin-top:5px;float:right;">Back</a>--%>
    </div>
    <!-- Twitter Widget -->
    <!-- Weather Widget -->


</asp:Content>
