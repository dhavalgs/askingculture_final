﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/ManagerMaster.master"
    AutoEventWireup="true" CodeFile="Manager-dashboard.aspx.cs" Inherits="Organisation_Manager_dashboard"
    Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <%--joyride---------------------------------------------%>
    <link href="../plugins/joyride/joyride-2.1.css" rel="stylesheet" type="text/css" />
    <%--<script src="../plugins/joyride/jquery-1.10.1.js" type="text/javascript"></script>--%>
    <script src="../plugins/joyride/jquery.cookie.js" type="text/javascript"></script>
    <script src="../plugins/joyride/jquery.joyride-2.1.js" type="text/javascript"></script>
    <script src="../plugins/joyride/modernizr.mq.js" type="text/javascript"></script>

    <%--<script src="../plugins/highchart/highcharts.js" type="text/javascript"></script>--%>
    <script type="text/javascript" src="../plugins/highchart/highstock.js"></script>
    <%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js" type="text/javascript"></script>--%>

    <script src="../plugins/highchart/highcharts-more.js" type="text/javascript"></script>
    <script src="../Scripts/plugins/exporting.js" type="text/javascript"></script>
    <script src="../Scripts/jquery321.js"></script>

    <script src="../assets/js/multiselect.js" type="text/javascript"></script>
    <link href="../assets/css/multiselect.css" rel="stylesheet" />

    <style type="text/css">
        #lines span, #toggle span i {
            color: #333333;
            float: left;
            font-size: 13px;
            line-height: 22px;
            padding: 3px 9px;
        }

        #lines, #toggle {
            border-bottom: 2px solid #F65B5B;
            cursor: pointer;
            float: right;
            height: 32px;
            margin-left: 10px;
            margin-right: 0;
            padding: 0;
            width: 34px;
            background-color: white;
            border: 1px solid #EBEBEB;
            z-index: 100;
        }

        #ContentPlaceHolder1_chkList input {
            width: auto;
            margin-bottom: 0px !important;
        }

        #ContentPlaceHolder1_chkList label {
            margin-top: 2px;
        }

        .scroll_checkboxes {
            height: 120px;
            width: 100%;
            padding: 5px;
            overflow: auto;
            border: 1px solid #ccc;
        }

        .HiddenText label {
            display: none;
        }

        .cke_skin_kama .cke_editor {
            display: table !important;
            width: 100%;
        }
    </style>
    <link href="../Styles/UpdateCustom.css" rel="stylesheet" type="text/css" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:HiddenField ID="columnchartname" runat="server" Value="Column" meta:resourcekey="Column" />
    <asp:HiddenField ID="barchartname" runat="server" Value="BarChart" meta:resourcekey="BarChart" />
    <asp:HiddenField ID="spiderchartname" runat="server" Value="SpiderChart" meta:resourcekey="SpiderChart" />

    <asp:HiddenField ID="hdnLastlogin" runat="server" Value="Last Login was on : " meta:resourcekey="resourceLastLogin"></asp:HiddenField>
    <asp:HiddenField ID="hdnSelectDivisions" runat="server" Value="Select Division" meta:resourcekey="SelectDivisionsResource" />
    <asp:HiddenField ID="hdnSelectJob" runat="server" Value="Select Jobtype" meta:resourcekey="SelectJobResource" />

    <asp:HiddenField ID="hdnSelectTeam" runat="server" Value="Select Team" meta:resourcekey="SelectTeamResource" />
    <asp:HiddenField ID="hdnSelectTeamFilter" runat="server" Value="Select Team" meta:resourcekey="SelectTeamFilterResource" />
    <asp:HiddenField ID="hdnSelectGender" runat="server" Value="Select Gender" meta:resourcekey="SelectGenderResource" />
    <asp:HiddenField ID="hdnSelectAll" runat="server" Value="Select All" meta:resourcekey="SelectAll" />
    <asp:HiddenField ID="hdnUnSelectAll" runat="server" Value="UnSelect All" meta:resourcekey="UnSelectAll" />


    <asp:HiddenField ID="hdnChartLevel" runat="server" Value="" meta:resourcekey="ChartLevelRes" />
    <asp:HiddenField ID="hdnChartTarget" runat="server" Value="" meta:resourcekey="TargetRes" />
    <asp:HiddenField ID="hdnChartActual" runat="server" Value="" meta:resourcekey="ActualRes" />

      <asp:HiddenField ID="hdnChartMax" runat="server" Value="" meta:resourcekey="MaxRes" />

    <asp:HiddenField ID="hdnNoOption" runat="server" Value="" meta:resourcekey="NoOptionRes" />

    <asp:HiddenField ID="hdnPrintChart" runat="server" Value="" meta:resourcekey="PrintChartRes" />
    <asp:HiddenField ID="hdnDownloadPNG" runat="server" Value="" meta:resourcekey="DownloadPNGRes" />
    <asp:HiddenField ID="hdnDownloadJPEG" runat="server" Value="" meta:resourcekey="DownloadJPEGRes" />
    <asp:HiddenField ID="hdnDownloadPDF" runat="server" Value="" meta:resourcekey="DownloadPDFRes" />
    <asp:HiddenField ID="hdnDownloadSVG" runat="server" Value="" meta:resourcekey="DownloadSVGRes" />

    <%--   <asp:HiddenField ID="lblAllManager1" runat="server" Value="all manager" meta:resourcekey="AllManagerResource1" />--%>
     
       <asp:HiddenField ID="hdnGraphUserSelectedID" runat="server" Value=""/>

    <div style="display: none">
        <asp:Literal ID="hdnSystemUpdated" runat="server" meta:resourcekey="SystemUpdate" />
    </div>

    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" meta:resourcekey="lblMsgResource1"></asp:Label>
    <div class="col-md-6">
        <div class="heading-sec">
            <h1>
                <%-- <%= CommonMessages.Dashboard%> --%>
                <%--  <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Dashboard" EnableViewState="false" />--%>
                <i><span>
                    <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Welcome" EnableViewState="false" /></span><span runat="server" id="Dashboard"></span></i>
            </h1>
        </div>
    </div>
    <div class="col-md-6" style="display: none">
        <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
            <i class="fa fa-calendar icon-calendar icon-large"></i><span>August 5, 2014 - September
                3, 2014</span> <b class="caret"></b>
        </div>
    </div>
    <div class="col-md-12">
        <div id="graph-wrapper">




            <h3 class="custom-heading darkblue" style="height: 45px">
                <%--<%= CommonMessages.EmployeeSkills%>--%>

                <asp:DropDownList ID="ddlTeamFilter" runat="server"
                    Style="width: 25%; height: 32px; float: right; display: inline; margin-bottom: 20px; margin-right: 15px; margin-left: 2%;"
                    CssClass="form-control" AutoPostBack="False">
                </asp:DropDownList>

                <input type="hidden" id="hdnChartType" name="hdnChartType" value="bars" />
                <asp:Literal ID="Literal2" runat="server" meta:resourcekey="EmployeeSkills" EnableViewState="false" />
                <%-- <a href="#" id="bars" onclick="return SetChartType('bars');"><span><i class="fa fa-bar-chart-o"></i></span></a>
                    <a href="#" id="lines" class="active" onclick="return SetChartType('lines');"><span><i class="fa fa-bullseye" style="font-size:15px;"></i></span></a>
                    <a href="#" id="toggle" onclick="return SetChartType('toggle');"><span><i class="fa fa-bar-chart-o"></i></span></a>--%>
                <a href="#" id="bars" onclick="return SetChartType('bars');"><span>

                    <img src="img/vertical-bar-chart-loss-icon.jpg" style="height: 25px; width: 25px; margin-left: 3px; margin-top: 2px;" />
                </span></a>
                <a href="#" id="lines" class="active" onclick="return SetChartType('lines');">
                    <i class="fa fa-bullseye" style="font-size: 15px; margin-left: 10px; margin-top: 8px; color: black"></i></a>
                <a href="#" id="toggle" onclick="return SetChartType('bars');"><span><i class="fa fa-bar-chart-o"></i>
                </span></a>

            </h3>
            <div class="home_grap">
                <span id="lblUserName" style="margin-left: 50%; display: none;">
                    <asp:Label runat="server" meta:resourcekey="AllEmployeeResource" ID="lblAllEmployee" /></span>

                <asp:HiddenField ID="lblAllEmployee1" runat="server" Value="All Employee " meta:resourcekey="AllEmployeeResource1"></asp:HiddenField>

                <asp:HiddenField ID="lblAllManager1" runat="server" Value="All Employee " meta:resourcekey="AllEmployeeResource1"></asp:HiddenField>

                <div>
                    <input type="hidden" value="0" id="ddlCompetence" name="ddlCompetence" />
                    <asp:DropDownList ID="managercat" runat="server" multiple="multiple">
                    </asp:DropDownList>

                </div>
                <%--  <div class="graph-container" style="margin-top:50px">--%>


                <div id="graph-barsEmployee" style="width: 100%; height: 400px; margin-top: 10px;">
                </div>
                <div id="container" style="width: 100%; height: 400px;">
                </div>
                <div id="graph-barEmp" style="width: 100%; height: 400px;">
                </div>
                <select id="chartType" name="Chart Type" style="float: right; margin-top: -430px; margin-right: 24%; position: relative;">
                    <option value="0">
                        <asp:Label ID="Label1" runat="server" meta:resourcekey="sortbyres"></asp:Label></option>
                    <option value="1">
                        <asp:Label ID="Label2" runat="server" meta:resourcekey="hightolowres"></asp:Label></option>
                    <option value="2">
                        <asp:Label ID="Label3" runat="server" meta:resourcekey="lowtohighres"></asp:Label></option>
                </select>

                <select id="ddlUserTarget" name="Chart Type" style="float: right; margin-top: -430px; margin-right: 35px; position: relative;">
                    <option value="1" selected="selected">
                        <asp:Literal ID="Literal16" runat="server" meta:resourcekey="shorttermRes" Text="Show short term target" EnableViewState="false" /></option>
                    <option value="2">
                        <asp:Literal ID="Literal17" runat="server" meta:resourcekey="longtermRes" Text="Show long term target" EnableViewState="false" /></option>
                     <option value="3">
                        <asp:Literal ID="Literal21" runat="server" meta:resourcekey="targetJobType" Text="Show company target for JobType" EnableViewState="false" /></option>
                     <option value="4">
                        <asp:Literal ID="Literal22" runat="server" meta:resourcekey="targetDivision" Text="Show company target for Division" EnableViewState="false" /></option>
                    <%-- <option value="3"><asp:Literal ID="Literal19" runat="server" meta:resourcekey="longtermRes" Text="Show company target for Team" EnableViewState="false" /></option>--%>
                </select>

                <%-- </div>--%>
            </div>
            <div class="col-md-12" style="background: #f4f4f4; padding-left: 0px !important; float: left">
                <div class="col-md-1 profile-details" style="padding-left: 0px !important;">
                    <%-- <a class="black" style="float: left" onclick="GetGraphById('','')">--%>
                    <%-- <a class="black" style="float: left" onclick="GetGraphById('','')"><i class="fa fa-users"></i>
                        <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Employee" EnableViewState="false" /></a>--%>


                    <a class="black" style="float: left" onclick="GetGraphByManagerId('','')"><i class="fa fa-users"></i>
                        <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Manager" EnableViewState="false" /></a>

                </div>
                <div class="col-md-9">
                    <div class="total-members">
                        <ul id="userList">
                        </ul>
                        <%--<div class="total-members">
                        <ul>
                            <li><a href="#" title="">
                                <img src="images/colleagus/2.jpg" alt="" /></a>
                                <div class="member-hover">
                                    <a href="#" title="">
                                        <img src="images/colleagus/2.jpg" alt="" /></a> <span>JOHN DOE </span>
                                </div>
                            </li>
                            <li><a href="#" title="">
                                <img src="images/colleagus/3.jpg" alt="" /></a>
                                <div class="member-hover">
                                    <a href="#" title="">
                                        <img src="images/colleagus/3.jpg" alt="" /></a> <span>LARA SMITH </span>
                                </div>
                            </li>
                            <li><a href="#" title="">
                                <img src="images/colleagus/2.jpg" alt="" /></a>
                                <div class="member-hover">
                                    <a href="#" title="">
                                        <img src="images/colleagus/2.jpg" alt="" /></a> <span>YAMAZ DAN </span>
                                </div>
                            </li>
                            <li><a href="#" title="">
                                <img src="images/colleagus/9.jpg" alt="" /></a>
                                <div class="member-hover">
                                    <a href="#" title="">
                                        <img src="images/colleagus/9.jpg" alt="" /></a> <span>DAREN JANCE </span>
                                </div>
                            </li>
                            <li><a href="#" title="">
                                <img src="images/colleagus/10.jpg" alt="" /></a>
                                <div class="member-hover">
                                    <a href="#" title="">
                                        <img src="images/colleagus/10.jpg" alt="" /></a> <span>HELOA HAZ </span>
                                </div>
                            </li>
                            <li><a href="#" title="">
                                <img src="images/colleagus/2.jpg" alt="" /></a>
                                <div class="member-hover">
                                    <a href="#" title="">
                                        <img src="images/colleagus/2.jpg" alt="" /></a> <span>JOHN DOE </span>
                                </div>
                            </li>
                            <li><a href="#" title="">
                                <img src="images/colleagus/3.jpg" alt="" /></a>
                                <div class="member-hover">
                                    <a href="#" title="">
                                        <img src="images/colleagus/3.jpg" alt="" /></a> <span>LARA SMITH </span>
                                </div>
                            </li>
                            <li><a href="#" title="">
                                <img src="images/colleagus/2.jpg" alt="" /></a>
                                <div class="member-hover">
                                    <a href="#" title="">
                                        <img src="images/colleagus/2.jpg" alt="" /></a> <span>YAMAZ DAN </span>
                                </div>
                            </li>
                            <li><a href="#" title="">
                                <img src="images/colleagus/9.jpg" alt="" /></a>
                                <div class="member-hover">
                                    <a href="#" title="">
                                        <img src="images/colleagus/9.jpg" alt="" /></a> <span>DAREN JANCE </span>
                                </div>
                            </li>
                        </ul>
                    </div>--%>

                        <input type="hidden" id="txtvalue" value="<%=Session["Culture"] %>" />
                        <div style="display: none">
                            <%-- <asp:Repeater ID="Repeateremp" runat="server">
                            <HeaderTemplate>
                                <ul>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li><a href="#" title="" onclick="GetGraphById(<%# Eval("userId") %>,'<%# Eval("name")%>')">
                                    <img src='../Log/upload/Userimage/<%# Eval("userImage") %>' alt="" /></a>
                                    <div class="member-hover" style="width: 130px;">
                                        <a href="#" title="">
                                            <img src='../Log/upload/Userimage/<%# Eval("userImage") %>' alt="" /></a> <span>
                                                <%# Eval("name")%></span>
                                    </div>
                                </li>
                            </ItemTemplate>
                            <FooterTemplate>
                                </ul>
                            </FooterTemplate>
                        </asp:Repeater>--%>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="total-memeber-head">
                        <div class="add-btn">
                            <a href="#add-post-title" data-toggle="modal" title="" onclick="javascript:showpopup1();">
                                <i class="fa fa-plus pink"></i>
                                <asp:Literal ID="Literal4" runat="server" meta:resourcekey="AddNew" EnableViewState="false" />
                                <asp:Literal ID="Literal5" runat="server" meta:resourcekey="Employee" EnableViewState="false" />

                                <%--
                                <%= CommonMessages.AddNew%>
                                <%= CommonMessages.Employee%>--%>
                            </a>

                        </div>


                    </div>
                    <%----------------------------Add new employee popup-----------------------------------------------------------------------------%>
                    <asp:UpdatePanel runat="server" ID="UpdatePanel1" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div aria-hidden="true" role="dialog" tabindex="-1" class="modal fade" id="add-post-title"
                                style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header blue" style="border-radius: 0px;">
                                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button" style="display: none;">
                                                ×
                                            </button>
                                            <h4 class="modal-title">
                                                <asp:Literal ID="Literal6" runat="server" meta:resourcekey="AddNew" EnableViewState="false" />
                                                <asp:Literal ID="Literal7" runat="server" meta:resourcekey="Employee" EnableViewState="false" />
                                                <%-- <%= CommonMessages.AddNew%>
                                        <%= CommonMessages.Employee%>--%>
                                            </h4>
                                        </div>
                                        <div class="modal-body">

                                            <div class="otherDiv">
                                                <asp:TextBox runat="server" placeholder="Name" ID="txtName" MaxLength="50" meta:resourcekey="txtNameResource1" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName"
                                                    ErrorMessage="Please enter Name." CssClass="commonerrormsg" Display="Dynamic"
                                                    ValidationGroup="chkNewEmp" meta:resourcekey="RequiredFieldValidator5Resource1"></asp:RequiredFieldValidator>
                                            </div>
                                            <div class="otherDiv">
                                                <asp:TextBox runat="server" placeholder="Last Name" ID="txtLastName" MaxLength="50" meta:resourcekey="txtLastNameResource1" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtName"
                                                    ErrorMessage="Please enter last name." CssClass="commonerrormsg" Display="Dynamic"
                                                    ValidationGroup="chkNewEmp" meta:resourcekey="RequiredFieldValidator5Resource1"></asp:RequiredFieldValidator>
                                            </div>
                                            <div class="otherDiv">
                                                <asp:TextBox runat="server" placeholder="Address" ID="txtaddress" Rows="2" TextMode="MultiLine"
                                                    meta:resourcekey="txtaddressResource1" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtaddress"
                                                    ErrorMessage="Please enter Address." CssClass="commonerrormsg" Display="Dynamic"
                                                    ValidationGroup="chkNewEmp" meta:resourcekey="RequiredFieldValidator2Resource1"></asp:RequiredFieldValidator>
                                            </div>
                                            <div class="otherDiv">
                                                <%--<textarea placeholder="Address" rows="3"></textarea>--%>
                                                <asp:TextBox runat="server" placeholder="Email" ID="txtEmail" MaxLength="50" meta:resourcekey="txtEmailResource1"
                                                    onchange="checkDuplicateEmail();" />
                                                <asp:Label runat="server" ID="lblEmail" CssClass="commonerrormsg"
                                                    meta:resourcekey="lblEmailResource1"></asp:Label>
                                                <asp:RequiredFieldValidator ID="rfvEmail" runat="server" CssClass="commonerrormsg"
                                                    ControlToValidate="txtEmail" Display="Dynamic" ValidationGroup="chkNewEmp" ErrorMessage="Please enter email."
                                                    meta:resourcekey="rfvEmailResource1"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revEmail" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                    ControlToValidate="txtEmail" CssClass="commonerrormsg" Display="Dynamic" ValidationGroup="chkNewEmp"
                                                    ErrorMessage="Please enter valid email." meta:resourcekey="revEmailResource1"></asp:RegularExpressionValidator>
                                            </div>
                                            <div class="otherDiv">
                                                <asp:TextBox runat="server" placeholder="Phone No" ID="txtMobile" MaxLength="15"
                                                    meta:resourcekey="txtMobileResource1" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtMobile"
                                                    ErrorMessage="Minimum mobile length is 10." Display="Dynamic" CssClass="commonerrormsg"
                                                    ValidationGroup="chkNewEmp" ValidationExpression=".{10}.*" meta:resourcekey="Minimum_mobile_length_is_10" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtMobile"
                                                    ErrorMessage="Please enter mobile." CssClass="commonerrormsg" Display="Dynamic"
                                                    ValidationGroup="chkNewEmp" meta:resourcekey="RequiredFieldValidator6Resource1"></asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Custom, Numbers"
                                                    ValidChars=" ,+,(,),-" TargetControlID="txtMobile" Enabled="True">
                                                </cc1:FilteredTextBoxExtender>
                                                <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtMobile"
                                            ErrorMessage="Minimum mobile length is 9." Display="Dynamic" CssClass="commonerrormsg"
                                            ValidationGroup="chk" ValidationExpression=".{9}.*" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtMobile"
                                            ErrorMessage="Please Enter Phone No." CssClass="commonerrormsg" Display="Dynamic"
                                            ValidationGroup="chk"></asp:RequiredFieldValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers"
                                            TargetControlID="txtMobile" Enabled="True">
                                        </cc1:FilteredTextBoxExtender>--%>
                                            </div>
                                            <div class="otherDiv">
                                                <asp:DropDownList ID="ddlgender" runat="server" CssClass="form-control" Style="width: 100%; height: 32px; display: inline; margin-bottom: 20px;"
                                                    meta:resourcekey="ddlgenderResource1">
                                                    <asp:ListItem Value="Male" Text="Male" meta:resourcekey="ListItemResource1"></asp:ListItem>
                                                    <asp:ListItem Value="Female" Text="Female" meta:resourcekey="ListItemResource2"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorddlgender" runat="server" ControlToValidate="ddlgender"
                                                    ErrorMessage="Please select gender." InitialValue="0" CssClass="commonerrormsg"
                                                    Display="Dynamic" ValidationGroup="chk"></asp:RequiredFieldValidator>
                                            </div>

                                            <div class="otherDiv">
                                                <asp:DropDownList ID="ddlLanguage" runat="server" Style="width: 100%; height: 32px; display: inline; margin-bottom: 20px;"
                                                    CssClass="form-control">
                                                    <asp:ListItem Value="English" Text="English" meta:resourcekey="EnglishResource"></asp:ListItem>
                                                    <asp:ListItem Value="Danish" Text="Danish" meta:resourcekey="Denishresource"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorddlLanguage" runat="server" ControlToValidate="ddlLanguage"
                                                    ErrorMessage="Please select Language." InitialValue="0" CssClass="commonerrormsg"
                                                    Display="Dynamic" ValidationGroup="chk" meta:resourcekey="RequiredFieldValidatorddlLanguageResource1">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                            <asp:UpdatePanel runat="server" ID="UpdatePanedrop" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <div class="otherDiv">
                                                        <asp:DropDownList ID="ddlDepartment" runat="server" Style="width: 100%; height: 32px; display: inline; margin-bottom: 20px;"
                                                            CssClass="form-control" OnSelectedIndexChanged="ddlcompetence_SelectedIndexChanged"
                                                            AutoPostBack="True" meta:resourcekey="ddlDepartmentResource1">
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorddlDepartment" runat="server" ControlToValidate="ddlDepartment"
                                                            ErrorMessage="Please select Division." InitialValue="0" CssClass="commonerrormsg"
                                                            Display="Dynamic" ValidationGroup="chk" meta:resourcekey="RequiredFieldValidatorddlDepartmentResource1"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="otherDiv">
                                                        <asp:DropDownList ID="ddljobtype" runat="server" Style="width: 100%; height: 32px; display: inline; margin-bottom: 20px;"
                                                            CssClass="form-control" OnSelectedIndexChanged="ddljobtype_SelectedIndexChanged"
                                                            AutoPostBack="True" meta:resourcekey="ddljobtypeResource1">
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorddljobtype" runat="server" ControlToValidate="ddljobtype"
                                                            ErrorMessage="Please select jobtype." InitialValue="0" CssClass="commonerrormsg"
                                                            Display="Dynamic" ValidationGroup="chk" meta:resourcekey="RequiredFieldValidatorddljobtypeResource1"></asp:RequiredFieldValidator>
                                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddljobtype"
                                                            ErrorMessage="Please select job type." InitialValue="0" CssClass="commonerrormsg"
                                                            Display="Dynamic" ValidationGroup="chkNewEmp" meta:resourcekey="RequiredFieldValidator1Resource1"></asp:RequiredFieldValidator>--%>
                                                    </div>

                                                    <div class="otherDiv">
                                                        <asp:DropDownList ID="ddlTeam" runat="server" Style="width: 100%; height: 32px; display: inline; margin-bottom: 20px;"
                                                            CssClass="form-control"
                                                            AutoPostBack="false" meta:resourcekey="ddlTeamResource1">
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorddlTeam" runat="server" ControlToValidate="ddlTeam"
                                                            ErrorMessage="Please select Team." InitialValue="0" CssClass="commonerrormsg"
                                                            Display="Dynamic" ValidationGroup="chk" meta:resourcekey="RequiredFieldValidatorddljobtypeResource1"></asp:RequiredFieldValidator>
                                                    </div>

                                                    <div class="col-md-6 scroll_checkboxes" style="display: none; height: 400px; overflow: auto;"
                                                        id="CompetenceCheckboxes">
                                                        <div id="Div2" runat="server">
                                                            <asp:HyperLink ID="HyperLink1" runat="server" onclick="fnSelectAll(this);" meta:resourcekey="lnkSelectAll" />
                                                            <%-- <a href="#" runat="server" meta:resourcekey="SelectAll" onclick="fnSelectAll();" title="Select All">Select All</a>--%>
                                                        </div>
                                                        <label class="c-label" style="width: 100%;">
                                                            <asp:Literal ID="Literal8" runat="server" meta:resourcekey="CATEGORY" EnableViewState="false" />
                                                            <%--  <%= CommonMessages.CATEGORY%>--%>:</label><br />

                                                        <div id="accordion-resizer" class="ui-widget-content" style="border: none;">
                                                            <div id="accordion">
                                                                <div id="NoRecords" runat="server" visible="false">
                                                                    <asp:Literal ID="Literal18" runat="server" meta:resourcekey="None" EnableViewState="false" />
                                                                </div>
                                                                <asp:Repeater ID="repCompCat" runat="server" OnItemDataBound="Repeater1_ItemDataBound">
                                                                    <HeaderTemplate>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <div style="margin-bottom: 2%;">
                                                                            <asp:Label runat="server" ID="lblDivIdName" Visible="false"><%# DataBinder.Eval(Container.DataItem, "catName")%> </asp:Label>
                                                                            <asp:Label runat="server" ID="lblDivIdName1" Visible="false"><%# DataBinder.Eval(Container.DataItem, "catNameDN")%> </asp:Label>
                                                                            <asp:Label runat="server" ID="lblCompCategoryName"> </asp:Label>
                                                                        </div>
                                                                        <div>
                                                                            <asp:Repeater ID="repCompAdd" runat="server" OnItemDataBound="Repeater2_ItemDataBound">
                                                                                <HeaderTemplate>
                                                                                    <table style="margin-top: -2%;">
                                                                                        <tbody>
                                                                                </HeaderTemplate>
                                                                                <ItemTemplate>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:CheckBoxList Width="250px" ID="chkList" runat="server" CssClass="chkliststyle chkcomname"
                                                                                                RepeatColumns="1" BorderWidth="0px" Datafield="description" DataValueField="value"
                                                                                                Style="width: 100%; overflow: auto;" meta:resourcekey="chkListResource1">
                                                                                            </asp:CheckBoxList>
                                                                                            <asp:Label runat="server" ID="hdnChkboxValue" Text='<%# DataBinder.Eval(Container.DataItem, "comId") %>'
                                                                                                Style="display: none" meta:resourcekey="hdnChkboxValueResource1"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                </ItemTemplate>
                                                                                <FooterTemplate>
                                                                                    </tbody> </table>
                                                                                </FooterTemplate>
                                                                            </asp:Repeater>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                    </FooterTemplate>
                                                                </asp:Repeater>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                        <%--NEXT Button--%>
                                        <div class="modal-footer" id="nextButtons">
                                            <%-- <asp:Button runat="server" ID="btnNext" Text="Next" CssClass="btn btn-primary yellow"
                        ValidationGroup="chk" OnClientClick="ShowCompotenceCategory(true);" Style="border-radius: 5px;" />--%>
                                            <%--<input id="" type="button" onclick="GoNext()" value="Next" class="btn btn-primary yellow"
                                                style="border-radius: 5px;" validationgroup="chkNewEmp" />--%>

                                            <button class="btn btn-primary yellow" type="button" validationgroup="chkNewEmp" onclick="GoNext()">
                                                <%-- <%= CommonMessages.Close%>--%>
                                                <asp:Literal ID="Literal15" runat="server" meta:resourcekey="Next" EnableViewState="false" />
                                            </button>


                                            <button data-dismiss="modal" class="btn btn-primary black" type="button" onclick="ShowCompotenceCategory(false)">
                                                <asp:Literal ID="Literal9" runat="server" meta:resourcekey="Close" EnableViewState="false" />
                                                <%-- <%= CommonMessages.Close%>--%>
                                            </button>
                                        </div>
                                        <%--Save Button--%>
                                        <div class="modal-footer" id="submitButtons" style="display: none">
                                            <asp:Button runat="server" ID="Button3" meta:resourcekey="Back" CssClass="btn btn-primary yellow"
                                                OnClientClick="ShowCompotenceCategory(false);return false;" Style="border-radius: 5px;" />


                                            <%--  <input id="Button2" type="button" onclick="ShowCompotenceCategory(false);" value="Back"
                                                class="btn btn-primary yellow" style="border-radius: 5px;" validationgroup="chkNewEmp" />
                                          <asp:Button runat="server" ID="Button1" Text="Submit" CssClass="btn btn-primary yellow"
                                                ValidationGroup="chkNewEmp" OnClick="btnsubmit_click" Style="border-radius: 5px;"
                                                meta:resourcekey="Submit" />--%>
                                            <asp:Button runat="server" ID="Button1" Text="Submit" CssClass="btn btn-primary yellow"
                                                ValidationGroup="chkNewEmp" OnClientClick="AddNewUser();return false;" Style="border-radius: 5px;"
                                                meta:resourcekey="Submit" />
                                            <%--<asp:Button runat="server" ID="btnclose1" Text="Close" CssClass="btn btn-default black"
                         OnClick="btnclose_click1" class="btn btn-default black"/>--%>
                                            <button data-dismiss="modal" class="btn btn-primary black" type="button" onclick="ShowCompotenceCategory(false)">
                                                <%-- <%= CommonMessages.Close%>--%>
                                                <asp:Literal ID="Literal10" runat="server" meta:resourcekey="Close" EnableViewState="false" />
                                            </button>
                                        </div>
                                        <%--<asp:Button runat="server" ID="btnsubmit" Text="Submit" CssClass="btn btn-primary yellow"
                                        ValidationGroup="chkNewEmp" OnClick="btnsubmit_click" />
                                    <button data-dismiss="modal" class="btn btn-default black" type="button">
                                        Close
                                    </button>--%>
                                        <%--  </div>--%>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <!-- Widget Visitor -->
    <!-- ORDR REVIEWS -->
    <div style="clear: both">
    </div>
    <%--ALPPLP-------------------------------%>
    <div class="col-md-12">
        <div id="">
            <h3 class="custom-heading darkblue">
                <asp:Literal ID="Literal11" runat="server" meta:resourcekey="GraphForPoints" EnableViewState="false" />
                <%--Graph For Points (Average ALP & Average PLP By Date)--%>
            </h3>
        </div>
        <%-- <asp:UpdatePanel runat="server" ID="UpdatePanel1" UpdateMode="Conditional">
                <ContentTemplate>--%>
        <div class="home_grap">
            <div class="graph-info">
                <a href="#" id="AlpBars"><span><i class="fa fa-bar-chart-o"></i></span></a>
                <a href="#" id="Alplines" class="active"><span><i class="fa fa-code-fork"></i></span></a>

            </div>
            <div class="graph-container">
                <div id="graph-lines-alpplp">
                </div>
                <div id="graph-bars-alpplp">
                </div>
            </div>
        </div>
        <%-- </ContentTemplate>
                $1$ <Triggers>
                    <asp:AsyncPostBackTrigger  EventName="OnCheckedChanged"/>
                </Triggers>#1#
            </asp:UpdatePanel>--%>
    </div>
    <%--PDP-------------------------------%>
    <div class="col-md-12" id="pdp">
        <div id="Div1">
            <h3 class="custom-heading darkblue">
                <asp:Literal ID="Literal13" runat="server" meta:resourcekey="GraphForPDPPoints" EnableViewState="false" />

            </h3>
        </div>
        <%-- <asp:UpdatePanel runat="server" ID="UpdatePanel1" UpdateMode="Conditional">
                <ContentTemplate>--%>
        <div class="home_grap">
            <div class="graph-info">
                <a href="#" id="PDPBars"><span><i class="fa fa-bar-chart-o"></i></span></a><a href="#"
                    id="PDPlines" class="active"><span><i class="fa fa-code-fork"></i></span></a>
            </div>
            <div class="graph-container">
                <div id="graph-lines-pdp">
                </div>
                <div id="graph-bars-pdp">
                </div>
            </div>
        </div>
        <%-- </ContentTemplate>
                $1$ <Triggers>
                    <asp:AsyncPostBackTrigger  EventName="OnCheckedChanged"/>
                </Triggers>#1#
            </asp:UpdatePanel>--%>
    </div>
    <div class="tdl-holder " style="display: none">
        <div class="yellow" style="width: 100%; float: left;">
            <div class="" style="float: left; width: 36%;">
                <h2 class="yellow" style="border: 0px;">TO DO LIST
                </h2>
            </div>
            <div style="float: right;">
                <div id="reportrange1" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                    <i class="fa fa-calendar icon-calendar icon-large"></i>
                    <asp:Label runat="server" ID="lblcalendar" Text="August 5, 2014 - September 3, 2014"
                        meta:resourcekey="lblcalendarResource1"></asp:Label><b class="caret"></b>
                    <%-- <span>August 5, 2014 - September 3, 2014</span> <b class="caret"></b>--%>
                    <%--<span ID="lblcalendar1" runat="server" va></span><b class="caret"></b>--%>
                </div>
            </div>
        </div>
        <asp:UpdatePanel runat="server" ID="UpdatePanellist" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="tdl-content">
                    <asp:LinkButton ID="LinkButton1" runat="server" class="" Style="height: 0px; width: 0px; float: right; text-align: center; line-height: 0px;"
                        OnClick="LinkButton1_click"
                        meta:resourcekey="LinkButton1Resource1"></asp:LinkButton>
                    <asp:HiddenField ID="HiddenField1" runat="server" Value="0" />
                    <asp:Repeater ID="Repeaterlist" runat="server">
                        <HeaderTemplate>
                            <ul>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <li>
                                <div class="todo_checkbox">
                                    <asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="True" OnCheckedChanged="Check_Clicked"
                                        Text='<%# Eval("listId") %>' CssClass="HiddenText" meta:resourcekey="CheckBox1Resource1" />
                                </div>
                                <div style="float: left;">
                                    <label>
                                        <i class="todo_i">
                                            <%# Eval("no")%></i><span><%# Eval("listsubject")%></span></label>
                                </div>
                            </li>
                        </ItemTemplate>
                        <FooterTemplate>
                            </ul>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                <asp:TextBox ID="TextBox1" class="tdl-new" runat="server" placeholder="Write new item and hit 'Enter'..."
                    AutoPostBack="True" OnTextChanged="TextBox1_TextChanged" meta:resourcekey="TextBox1Resource1"></asp:TextBox>
            </ContentTemplate>
            <%-- <Triggers>
                    <asp:AsyncPostBackTrigger  EventName="OnCheckedChanged"/>
                </Triggers>--%>
        </asp:UpdatePanel>
    </div>
    </div>
    <!-- TIME LINE -->
    <div class="col-md-6" style="margin-top: 30px; margin-bottom: 20px; display: none">
        <asp:UpdatePanel runat="server" ID="UpdatePanel11" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="contact-list widget-body">
                    <div class="timeline-head yellow">
                        <span>PDP Graph </span>
                    </div>
                    <div class="col-md-12">
                        <div class="home_grap">
                            <input id="Text2" type="text" />
                            <div class="graph-info">
                                <a href="#" id="A3"><span><i class="fa fa-bar-chart-o"></i></span></a><a href="#"
                                    id="A4" class="active"><span><i class="fa fa-code-fork"></i></span></a>
                            </div>
                            <div class="graph-container">
                                <div id="Div3">
                                </div>
                                <div id="Div4">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%----------------------------------------------------------------------%>
                <div class="contact-list widget-body" style="display: none">
                    <div class="timeline-head yellow">
                        <span>My Contact List </span>
                        <div class="add-btn">
                            <%--  <a href="#" title=""><i class="fa fa-plus blue"></i>Replay to all</a>--%>
                            <a href="#mail" data-toggle="modal" title="" onclick="mailall()"><i class="fa fa-plus blue"></i>Replay to all</a>
                            <asp:Label ID="lblmailall" runat="server" Text="" Style="display: none"></asp:Label>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="search-head" style="width: 100%;">
                            <input type="text" placeholder="SEARCH" id="txtserch" runat="server" style="padding: 9px; width: 90%; border: 0px;" />
                            <%-- <a href="#" title="" class="blue" style="width:35px; height:35px;"><i class="fa fa-search" ></i></a>--%>
                            <asp:LinkButton ID="lnkBtnName" runat="server" class="fa fa-search blue" Style="height: 35px; width: 35px; float: right; text-align: center; line-height: 29px;"
                                OnClick="btnserch_click"
                                meta:resourcekey="lnkBtnNameResource1"></asp:LinkButton>
                        </div>
                    </div>
                    <ul id="scrollbox3">
                        <asp:Repeater ID="Repeatercontct" runat="server" OnItemDataBound="Repeatercontct_ItemDataBound">
                            <ItemTemplate>
                                <li>
                                    <div class="todo_checkbox1">
                                        <input name="" class="" type="checkbox" value="">
                                    </div>
                                    <img src='../Log/upload/Userimage/<%# Eval("userImage") %>' alt="" /><a href="#"
                                        title=""><%# Eval("userFullname")%></a>
                                    <p>
                                        <i class="fa fa-map-marker"></i>
                                        <%# Eval("couName")%>
                                        <asp:HiddenField ID="userEmail" runat="server" Value="<%#bind('userEmail') %>" />
                                    </p>
                                    <div class="choose-contact">
                                        <a href="#mail" data-toggle="modal" title=""></a><a href="#mail" data-toggle="modal"
                                            title="" onclick="mail('<%# Eval("userEmail") %>')"><i class="fa fa-envelope"></i>
                                        </a>
                                    </div>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                        <script type="text/javascript">
                            function mail(a) { $('#ContentPlaceHolder1_lblmail').text(a); }
                            function mailall() {
                                var lbltext = document.getElementById('ContentPlaceHolder1_lblmailall').innerHTML;
                                $('#ContentPlaceHolder1_lblmail').text(lbltext);
                            }
                        </script>
                    </ul>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div aria-hidden="true" role="dialog" tabindex="-1" class="modal fade" id="mail"
        style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header blue yellow-radius">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                        ×
                    </button>
                    <h4 class="modal-title">Send Mail
                    </h4>
                </div>
                <div class="modal-body">
                    <div>
                        <asp:Label ID="lblmail" runat="server" Text="" Width="50%" Style="display: none"></asp:Label>
                    </div>
                    <br />
                    <div>
                        <asp:TextBox runat="server" ID="txt_Subjects" placeholder="Subject :"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txt_Subjects"
                            ErrorMessage="Please enter Subjects." CssClass="commonerrormsg" Display="Dynamic"
                            ValidationGroup="chkMail"></asp:RequiredFieldValidator>
                    </div>
                    <br />
                    <div>
                        <asp:TextBox runat="server" ID="txtReply" TextMode="MultiLine" MaxLength="500" CssClass="ckeditor"
                            Height="300px" Width="90%" placeholder="Message :" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" CssClass="commonerrormsg"
                            ControlToValidate="txtReply" Display="Dynamic" ValidationGroup="chkMail" ErrorMessage="Please Enter Message."></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" onclick="myFunction()" id="btnSend" value="Send" class="btn btn-primary yellow" />
                    <button data-dismiss="modal" class="btn btn-default black" type="button" id="btnClose">
                        <%-- <%= CommonMessages.Close%>--%>
                        <asp:Literal ID="Literal12" runat="server" meta:resourcekey="Close" EnableViewState="false" />
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- Chat Widget -->
    <!-- Recent Post -->
    <div class="col-md-3">
    </div>
    </div>
 <!---------------------------------------JoyRide >> Tip Content--------------------------------------------------------- -->
    <ol id="joyRideTipContent">
        <%--<li data-class="menu-profile" data-text="Next" class="custom" data-button="End Tour" data-options="tipLocation:right-middle">
        <h5>Welcome! Lets have a tour on Starteku.</h5>
        <p>You can control all the details for you tour stop.</p>
      </li>--%>
        <%-- <li data-class="heading-sec" data-button="stop" data-button="Already visited!" data-options="tipLocation:top;tipAnimation:fade">
        <h2>Stop #2</h2>
        <p>Get the details right by styling Joyride with a custom stylesheet!</p>
      </li>
      <li data-id="Head1" data-button="Next" data-text="Already visited!" data-options="tipLocation:right">
        <h2>Stop #3</h2>
        <p>It works right aligned.</p>
      </li>--%>
        <li data-button="Close" data-button="previous">
            <div runat="server" id="welcomeMsg"></div>
            <div class="tourTickbtn">
                <input id="chkTurnOffTour" onclick="joyrideEnd()" type="checkbox" title="" />
                <p style="margin-left: 15px; margin-top: -16px">
                    <asp:Literal ID="Literal14" runat="server" meta:resourcekey="TurnOffJoyRide" />
                </p>
            </div>
            <br />
            <br />
        </li>
        <%--<li data-button="Close">
            <h4>
                Congratulations <span class="WelComeUsername"></span>!
            </h4>
            <br />
            <p>
                You have now received :
            </p>
            <p>
                PLP points : <span class="plpSpan">0 </span>
            </p>
            <p>
                ALP points : <span class="alpSpan">0 </span>
            </p>
            <p>
                PDP points : <span class="pdpSpan">0 </span>
            </p>
            <p>
                Since your <span class="lastLogin">00:00 </span>!</p>
        </li>--%>
    </ol>
    <%--point welcom MESSAGE--%>
    <%--<ol id="welcomeWithPoint" style="display: none">
        <li data-button="Close" data-button="previous">
            <h4>
                Congratulations <span class="WelComeUsername"></span>!
            </h4>
            <br />
            <p>
                You have now received :
            </p>
            <p>
                PLP points : <span class="plpSpan">0 </span>
            </p>
            <p>
                ALP points : <span class="alpSpan">0 </span>
            </p>
            <p>
                PDP points : <span class="pdpSpan">0 </span>
            </p>
            <p>
                Since your <span class="lastLogin">00:00 </span>!</p>
            <div class="tourTickbtn">
            </div>
            <br />
            <br />
        </li>
       
    </ol>--%>



    <script src="../Scripts/chartCat.js" type="text/javascript"></script>
    <script src="../Scripts/flotJs.js" type="text/javascript"></script>
    <script src="../Scripts/chartCat.js" type="text/javascript"></script>
    <%-- End--%>


    <script type="text/javascript" src="../Scripts/ckeditor/ckeditor.js"> </script>
    <script type="text/javascript" src="js/chart-line-and-graph.js?v=2.7"></script>

    <script type="text/javascript">
        function fnSelectAll(a) {
            if ($("#accordion-resizer :input[type='checkbox']").prop('checked') == true) {
                $(a).text($('#ContentPlaceHolder1_hdnSelectAll').val());
                $("#accordion-resizer :input[type='checkbox']").prop('checked', false);
            }
            else {

                $(a).text($('#ContentPlaceHolder1_hdnUnSelectAll').val());
                $("#accordion-resizer :input[type='checkbox']").prop('checked', true);
            }

        }
    </script>
    <script type="text/javascript">
        function SetChartType(a) {

            $('#hdnChartType').val(a);

        }
    </script>
    <script type="text/javascript">



        function SetExpandCollapse1() {

            if (readCookie("CollapseClick") === null || readCookie("CollapseClick") === "") {
                createCookie("CollapseClick", "Expand", 1);
                ExampanCollapsMenu();
            }
            else {
                SetExpandCollapse();
            }
        }

        function joyrideEnd() {


            var checkedValue = $('#chkTurnOffTour:checked').val();

            $.ajax({
                type: "POST",
                url: 'WebService1.asmx/JoyRideTourShowHide',
                data: "{'checkedValue':'" + checkedValue + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#divResult").html(msg.d);
                },
                error: function (e) {
                    $("#divResult").html("WebSerivce unreachable");
                }
            });

        }

        function JoyrideStart() {
            setUsername();
            $('#joyRideTipContent').joyride({
                autoStart: true,
                postStepCallback: function (index, tip) {
                    if (index == 2) {
                        $(this).joyride('set_li', false, 1);
                    }
                }, exposed: [],
                modal: true,
                expose: true,
                next_button: true,      // true or false to control whether a next button is used
                prev_button: true,
            });
            $(".joyride-content-wrapper").mouseleave(function () { OnLoadCall(); });

        }
        function setUsername() {
            $(".WelComeUsername").text($("#span2").text());
            $(".plpSpan").text($("#lblPLP").text());
            $(".alpSpan").text($("#lblALP").text());
            $(".pdpSpan").text($("#lblpdp").text());
            $(".lastLogin").text($("#login").text());
        }

        function Signup() {
            // alert("sdf");
            PageMethods.RegisterUser();
            // alert("sdf");
        }


        function CheckedChange(objCheckBox) {
            if (confirm('Are you sure you want to delete?')) {
                //                __doPostBack("'" + objCheckBox.id + "'", '');
                return true;
            }
            else {
                objCheckBox.checked = false;
                return false;
            }
        }


        function myFunction(value) {
            //script.js
            var txtto = value;
            document.getElementById('<%= HiddenField1.ClientID %>').value = txtto;
            var btn = document.getElementById("ContentPlaceHolder1_LinkButton1");
            if (btn != null) {
                btn.click();
            }

        }

    </script>
    <script type="text/javascript" src="js/script.js"></script>

    <script type="text/javascript">
        setTimeout(function () {
            $('.highcharts-contextbutton').on('click', function () {
                setTimeout(function () {
                    $(".highcharts-menu-item").each(function (a) {
                        if ($(this).html() == "Print chart") {
                            $(this).html($(this).html().replace('Print chart', $('#ContentPlaceHolder1_hdnPrintChart').val()));
                        } else if ($(this).html() == "Download PNG image") {
                            $(this).html($(this).html().replace('Download PNG image', $('#ContentPlaceHolder1_hdnDownloadPNG').val()));
                        }
                        else if ($(this).html() == "Download JPEG image") {
                            $(this).html($(this).html().replace('Download JPEG image', $('#ContentPlaceHolder1_hdnDownloadJPEG').val()));
                        }
                        else if ($(this).html() == "Download PDF document") {
                            $(this).html($(this).html().replace('Download PDF document', $('#ContentPlaceHolder1_hdnDownloadPDF').val()));
                        }
                        else if ($(this).html() == "Download SVG vector image") {
                            $(this).html($(this).html().replace('Download SVG vector image', $('#ContentPlaceHolder1_hdnDownloadSVG').val()));
                        }
                    });

                }, 500);
            });
        }, 2000);
    </script>

    <script type="text/javascript">

        function showpopup1() {
            $("#nextButtons").show();
            $("#submitButtons").hide();
            $(".otherDiv").show();
            $("#CompetenceCheckboxes").hide();
            document.getElementById("ContentPlaceHolder1_txtName").value = "";
            document.getElementById("ContentPlaceHolder1_txtLastName").value = "";
            document.getElementById("ContentPlaceHolder1_txtaddress").value = "";
            document.getElementById("ContentPlaceHolder1_txtEmail").value = "";
            document.getElementById("ContentPlaceHolder1_txtMobile").value = "";

            document.getElementById("ContentPlaceHolder1_ddlDepartment").value = "0";
            document.getElementById("ContentPlaceHolder1_ddljobtype").value = "0";
            document.getElementById("ContentPlaceHolder1_ddlTeam").value = "0";
            document.getElementById("ContentPlaceHolder1_ddlgender").value = "0";

            <%-- $("#<%=ddlLanguage.ClientID%>").val("English");--%>
            $("#<%=ddlLanguage.ClientID%>").val("0");
            $("#<%=ddlDepartment.ClientID%>").val("0");
            $("#<%=ddljobtype.ClientID%>").val("0");
            $("#<%=ddlTeam.ClientID%>").val("0");

            $("#<%=RequiredFieldValidator5.ClientID%>").hide();
            $("#<%=RequiredFieldValidator4.ClientID%>").hide();
            $("#<%=RegularExpressionValidator6.ClientID%>").hide();
            $("#<%=rfvEmail.ClientID%>").hide();
            $("#<%=revEmail.ClientID%>").hide();
            $("#<%=RequiredFieldValidator2.ClientID%>").hide();
            $("#<%=RequiredFieldValidator3.ClientID%>").hide();
            $("#<%=RequiredFieldValidator1.ClientID%>").hide();

            $("#ContentPlaceHolder1_RequiredFieldValidatorddlLanguage").hide();
            $("#ContentPlaceHolder1_RequiredFieldValidatorddljobtype").hide();
            $("#ContentPlaceHolder1_RequiredFieldValidatorddlDepartment").hide();
            $("#ContentPlaceHolder1_RequiredFieldValidatorddlTeam").hide();
            $("#ContentPlaceHolder1_RequiredFieldValidatorddlgender").hide();
        }
    </script>
    <%-- <script type="text/javascript">
       
        CKEDITOR.config.toolbar = [
   ['Font', 'FontSize', 'Bold', 'Italic', 'Underline', 'StrikeThrough', '-', 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'Find', 'Replace', 'Print'],
   '/', ['NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'], ['Link', 'Smiley', 'TextColor', 'BGColor']];

    
       
    </script>--%>
    <script type="text/javascript">
        var userList;

        $(document).ready(function () {
            OnLoadCall();

        });

        function OnLoadCall() {
            var ddGraphTeamVal = $("#<%=ddlTeamFilter.ClientID%>").val();
            GetAllUserByType(3);
            notificationShowHide();
            GetPageName();
            //GetData("");
            ForMainGraph();
            // $("#toggle").click();
            $("#bars").click();
            $("#PDPBars").click();
            setTimeout(function () {
                // $("#toggle").click();

                GetData_OLD("", ddGraphTeamVal);
                //  GetData("");
                $("#bars").click();
                $("#PDPBars").click();
            }, 1500);
            setTimeout(function () {

                GetData_OLD("", ddGraphTeamVal);
                // $("#toggle").click();
                //  GetData("");
                $("#bars").click();
                $("#PDPBars").click();
            }, 4500);
            // Tooltip #################################################



            GetPDPData('', ddGraphTeamVal);

            ddpGraphUpdate();
        }

        function ddpGraphUpdate() {

            $("#<%=ddlTeamFilter.ClientID%>").change(function () {

                console.log("changed");
                ddGraphTeamVal = $("#<%=ddlTeamFilter.ClientID%>").val();
                // alert(selectedText);
                console.log("selected text= " + ddGraphTeamVal);
                //alert(ddGraphJobTypeVal);
                GetGraphByManagerId('', '');
                GetDataByTeameID(3, ddGraphTeamVal);

                GetPDPData('', ddGraphTeamVal);

                GetData_OLD("", ddGraphTeamVal);
            });
        }

        function GetDataByTeameID(userType, TeamID) {

            $.ajax({
                type: "POST",
                async: true,
                url: "webservice1.asmx/GetAllUserByType_Team",
                data: "{'userTypeId':'" + userType + "','TeamID':'" + TeamID + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (e) {
                    $("#divResult").html(e.d);
                    var msg = e.d;

                    userList = msg;
                    if (userList != "error") {
                        RanderUser(userList);
                    } else {
                        //alert("");
                    }
                },
                error: function (e) {
                    $("#divResult").html("WebSerivce unreachable");
                }
            });
        }
        function myFunction() {
            var txtmessge = CKEDITOR.instances.ContentPlaceHolder1_txtReply.getData();
            var txtsubject = $('#<%=txt_Subjects.ClientID %>').val();
            var to = document.getElementById("ContentPlaceHolder1_lblmail").innerHTML;
            document.getElementById('btnClose').click();
            if (txtsubject != '' && txtsubject != '' && to != '') {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Manager-dashboard.aspx/Sendmail",
                    data: "{'txtsubject':'" + txtsubject + "','txtmessge':'" + txtmessge + "','to':'" + to + "'}",
                    dataType: "json",
                    success: function (data) {
                        var obj = data.d;
                        if (obj == 'true') {
                            $('#<%=txt_Subjects.ClientID %>').val('');
                            $('#<%=txtReply.ClientID %>').val('');
                            //                            CKEDITOR.instances.ContentPlaceHolder1_txtReply = '';
                            alert('Send SuccessFully');

                        }
                    },
                    error: function (result) {
                        alert("Error");
                    }
                });
            }
            else {
                alert('Please Enter All The Fields');
                return false;
            }
        }

        function checkDuplicateEmail() {

            var email = $("#<%=txtEmail.ClientID%>").val();
            if (email.length > 4) {
                console.log(email);
                $.ajax({
                    type: "POST",
                    async: true,
                    url: "webservice1.asmx/UserEmailCheckDuplication",
                    data: "{'email':'" + email + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (e) {
                        $("#divResult").html(e.d);
                        var msg = e.d;
                        if (msg == "Success") {
                            $("#<%=lblEmail.ClientID%>").text("");
                        } else {
                            setTimeout(function () {
                                $("#<%=lblEmail.ClientID%>").text(msg);
                                }, 500);
                            }


                        //return msg.d;

                    },
                    error: function (e) {
                        $("#divResult").html("WebSerivce unreachable");

                    }
                });
                }
            }



            function AddNewUser() {
                if (!CheckValidations("chkNewEmp")) {
                    return;
                }
                $(".close").click();
                //  $("#add-post-title").modal("toggle");
                var Name = $("#<%=txtName.ClientID%>").val();
            var lastName = $("#<%=txtLastName.ClientID%>").val();
            var email = $("#<%=txtEmail.ClientID%>").val();
            var address = $("#<%=txtaddress.ClientID%>").val();
            var mobile = $("#<%=txtMobile.ClientID%>").val();
            var userdepId = document.getElementById("ContentPlaceHolder1_ddlDepartment").value;
            var jobtype = document.getElementById("ContentPlaceHolder1_ddljobtype").value;
            // var Language = $("#<%=ddlLanguage.ClientID%>").val();
            var Language = $("#<%=ddlLanguage.ClientID%>").find("option:selected").text();
            var gender = $("#<%=ddlgender.ClientID%>").find("option:selected").val();
            var TeamID = document.getElementById("ContentPlaceHolder1_ddlTeam").value;

            var selectedValues = [];
            $("input:checked").each(function () {
                selectedValues.push($(this).val());
            });
            if (selectedValues.length > 0) {
                // alert("Selected Value(s): " + selectedValues);
            } else {
                // alert("No item has been selected.");
            }
            generate('information', 'Please wait, we are processing to add new user.', 'bottomCenter');
            $.ajax({
                type: "POST",
                async: true,
                url: "Manager-dashboard.aspx/AddNewEmp",
                data: "{'email':'" + email + "','address':'" + address + "','mobile':'" + mobile + "','userdepId':'" + userdepId + "','jobtype':'" + jobtype + "','selectedValues':'" + selectedValues + "','Name':'" + Name + "','lastName':'" + lastName + "','Language':'" + Language + "','gender':'" + gender + "','TeamID':'" + TeamID + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (e) {
                    //                    $("#divResult").html(e.d);
                    var msg = e.d;

                    if (msg == "success") {
                        window.location.reload();
                        $("#add-post-title").modal("hide");
                        //alert();
                        $(".close").click();
                        $("#nextButtons").show();
                        $("#submitButtons").hide();
                        $(".otherDiv").show();
                        $("#CompetenceCheckboxes").hide();
                        modelPopupDilouge("Add New User", "New Employee added successfully.", true);
                        // GetAllUserByType(3);

                    } else {
                        //modelPopupDilouge("Add New User", msg.d, false);
                        modelPopupDilouge("Add New User", msg.d, false);
                    }
                },
                error: function (e) {
                    $("#divResult").html("WebSerivce unreachable");
                },
                complete: function () {
                    window.location.reload();
                }
            });
        }


        function GetAllUserByType(userType) {
            $.ajax({
                type: "POST",
                async: true,
                url: "webservice1.asmx/GetAllUserByType",
                data: "{'userTypeId':'" + userType + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (e) {
                    $("#divResult").html(e.d);
                    var msg = e.d;

                    userList = msg;
                    if (userList != "error") {
                        RanderUser(userList);
                    } else {
                        //alert("");
                    }
                },
                error: function (e) {
                    $("#divResult").html("WebSerivce unreachable");
                }
            });
        }

        function ShowCompotenceCategory(toggle) {

            accordian();

            if (toggle) {

                if (!CheckValidations("chkNewEmp")) {

                    //Ahh... User not validate all fields... Let him inform


                } else {
                    toggleSubmitButtons(true);
                    $("#CompetenceCheckboxes").show();
                    $(".otherDiv").hide();
                }

            } else {

                //this functions will make user on top main div
                toggleSubmitButtons(false);
                $("#CompetenceCheckboxes").hide();
                $(".otherDiv").show();
            }

        }
        function RanderUser(userList) {
            var html = "";
            $("#userList").html('');
            var dataUser = JSON.parse(userList);
            $.each(dataUser.Table, function (i, item) {

                html += '<li><a href="#" title="" onclick ="GetGraphById(' + item.userId + ',\'' + item.name + '\')' + '">';
                html += '<img src="../Log/upload/Userimage/' + item.userImage + '" alt="" /></a>';
                html += ' <div class="member-hover" style="width: 130px;"><a href="#" title=""><img src="../Log/upload/Userimage/' + item.userImage + '" alt="" /></a> ';
                html += '<span>' + item.name + '</span></div></li>';

            });
            $("#userList").append(html);

        }

        function CallCompetence() {

            if ($('#ContentPlaceHolder1_txtName').val() == '' || $('#ContentPlaceHolder1_txtlastName').val() == ''
                || $('#ContentPlaceHolder1_txtaddress').val() == '' || $('#ContentPlaceHolder1_txtEmail').val() == ''
                || $('#ContentPlaceHolder1_txtMobile').val() == '' || $('#ContentPlaceHolder1_ddlgender').val() == '0'
            || $('#ContentPlaceHolder1_ddlLanguage').val() == '0' || $('#ContentPlaceHolder1_jobtype').val() == '0' || $('#ContentPlaceHolder1_ddlDepartment').val() == '0'
                || $('#ContentPlaceHolder1_ddlTeam').val() == '0') {

                ValidatorEnable($("[id$='RequiredFieldValidator1']")[0], true);
                ValidatorEnable($("[id$='RequiredFieldValidator2']")[0], true);
                ValidatorEnable($("[id$='RequiredFieldValidator3']")[0], true);
                ValidatorEnable($("[id$='RequiredFieldValidator4']")[0], true);

                ValidatorEnable($("[id$='RequiredFieldValidatorddlLanguage']")[0], true);

                ValidatorEnable($("[id$='RequiredFieldValidatorddljobtype']")[0], true);
                ValidatorEnable($("[id$='RequiredFieldValidatorddlDepartment']")[0], true);
                ValidatorEnable($("[id$='RequiredFieldValidatorddlTeam']")[0], true);
                ValidatorEnable($("[id$='RequiredFieldValidatorddlgender']")[0], true);
                ValidatorEnable($("[id$='revEmail']")[0], true);
            }
            else {

                ShowCompotenceCategory(true);
            }
        }
        function GoNext() {

            if ($("#<%=lblEmail.ClientID%>").text() != "") {
                alert("Email already exists.");
                return;
            }
            else {

                CallCompetence();
            }
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('#ContentPlaceHolder1_managercat').multiselect({

                //enableFiltering: true,
                includeSelectAllOption: true,
                //buttonWidth: '400px',
                selectAllText: 'All Competence',
                //  //nonSelectedText: 'None Location selected',
                allSelectedText: 'All Competence',
                //  nSelectedText: 'selected',
                //  numberDisplayed: 2,


                buttonText: function (options) {

                    if (options.length === 0) {

                        return $('#ContentPlaceHolder1_hdnNoOption').val();

                    }
                    var labels = [];
                    var num = [];

                    options.each(function () {
                        if ($(this).attr('value') !== undefined) {
                            labels.push($(this).text());
                            num.push($(this).val());

                        }
                    });
                    $('#ddlCompetence').val(num.join(','));

                    return labels.join(', ');
                },
                onChange: function (event) {

                    GetData("");


                },


            });

        });
    </script>

</asp:Content>
