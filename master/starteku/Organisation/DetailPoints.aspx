﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true" CodeFile="DetailPoints.aspx.cs" Inherits="Organisation_DetailPoints" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" 
        meta:resourcekey="lblMsgResource1"></asp:Label>
    <div class="col-md-6">
        <div class="heading-sec">
            <h1>
             <%-- <%= CommonMessages.PointSummary%>--%>  <asp:Literal ID="Literal1" runat="server" meta:resourcekey="PointSummary" enableviewstate="false"/> <i><span runat="server" id="Employee"></span> </i>
            </h1>
        </div>
    </div>
    <div class="col-md-12" style="margin-top: 20px;">
        <div id="graph-wrapper">
            <div class="col-md-12">
                <div class="chart-tab">
                    <div id="tabs-container">
                        <div class="invoice ALL" style="margin-top: 20px;">
                            <h2 class="pointtable_h2" style="margin-bottom: -11px;width:81%;">
                               
                                <span runat="server" id="Points"></span>
                                 <asp:Label ID="lblFor" meta:resourcekey="ForResource" runat="server"/>
                                <asp:Label ID="lblUserName" runat="server"></asp:Label>
                                <asp:HiddenField runat="server" ID="lblAcceptProvidedHelp" meta:resourcekey="lblAcceptProvidedHelpResource"></asp:HiddenField>
                                <asp:HiddenField runat="server" ID="lblRequestSession" meta:resourcekey="lblRequestSessionResource"></asp:HiddenField>
                                <asp:HiddenField runat="server" ID="lblSuggestSession" meta:resourcekey="lblSuggestSessionResource"></asp:HiddenField>
                                <asp:HiddenField runat="server" ID="lblReadDoc" meta:resourcekey="lblReadDocResource"></asp:HiddenField>
                                <asp:HiddenField runat="server" ID="lblReadDocByOther" meta:resourcekey="ReadDocByOtherResource"></asp:HiddenField>
                                <asp:HiddenField runat="server" ID="lblUploadPublicDoc" meta:resourcekey="lblUploadPublicDocResource"></asp:HiddenField>
                                <asp:HiddenField runat="server" ID="lblFinalFillCompetences" meta:resourcekey="lblFinalFillCompetencesResource"></asp:HiddenField>
                                <asp:HiddenField runat="server" ID="lblOpenCompetencesScreen" meta:resourcekey="lblOpenCompetencesScreenResource"></asp:HiddenField>
                                <asp:HiddenField runat="server" ID="lblSignIn" meta:resourcekey="lblSignInResource"></asp:HiddenField>
                                
                                 <span id="AAlp" runat="server" style="float:right;"></span><span id="Span1" runat="server" style="float:right;"><asp:Label ID="lbltotal" runat="server" meta:resourcekey="Resourcelbltotal"></asp:Label>: </span>
                            </h2>
                            <asp:GridView ID="gvGrid" runat="server" AutoGenerateColumns="False" CellPadding="0"
                                Width="100%" GridLines="None" DataKeyNames="logUserId" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                                EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>' 
                                BackColor="White" meta:resourcekey="gvGridResource1">
                                <HeaderStyle CssClass="aa" />
                                <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                <AlternatingRowStyle BackColor="White" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Session" 
                                        meta:resourcekey="TemplateFieldResource1">
                                        <ItemTemplate>
                                            <asp:Label ID="lblUNamer" CssClass="pointinfo" runat="server" Text='<%# Eval("logPointInfo") %>' 
                                                meta:resourcekey="lblUNamerResource1"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                            Width="20%" ForeColor="White" BorderWidth="0px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description" 
                                        meta:resourcekey="TemplateFieldResource4">
                                        <ItemTemplate>
                                           
                                             <%
                                                   
                                                if(Convert.ToString(ViewState["logpointinfo"])=="AcceptProvidedHelp")
                                                {%>
                                                <asp:Label ID="lblFrmoName" runat="server" Text='<%# Eval("Fromname") %>'></asp:Label>
                                                 <asp:Label ID="Label1" runat="server" Text="Accept help from" meta:resourcekey="lblAcceptHelpresource"></asp:Label>
                                              <asp:Label ID="lbltoname" runat="server" Text='<%# Eval("Toname") %>'></asp:Label>
                                                <% }
                                                   
                                                   else  if(Convert.ToString(ViewState["logpointinfo"])=="RequestSession")
                                                {%>
                                                <asp:Label ID="lblFrmoName1" runat="server" Text='<%# Eval("Fromname") %>'></asp:Label>
                                                 <asp:Label ID="lbltakehelp" runat="server" Text="Take help from" meta:resourcekey="lbltakeHelpresource"></asp:Label>
                                              <asp:Label ID="lbltoname1" runat="server" Text='<%# Eval("Toname") %>'></asp:Label>
                                                <% }
                                                   
                                                      else  if(Convert.ToString(ViewState["logpointinfo"])=="ReadDocByOther")
                                                {%>
                                                <asp:Label ID="lblFrmoName2" runat="server" Text='<%# Eval("Fromname") %>'></asp:Label>
                                                 <asp:Label ID="lblProvidehelp" runat="server" Text="provided help on request of" meta:resourcekey="lblprovideHelpresource"></asp:Label>
                                              <asp:Label ID="lbltoname2" runat="server" Text='<%# Eval("Toname") %>'></asp:Label>
                                                <% }
                                                   
                                                      else  if(Convert.ToString(ViewState["logpointinfo"])=="SuggestSession")
                                                {%>
                                                <asp:Label ID="lblFrmoName3" runat="server" Text='<%# Eval("Fromname") %>'></asp:Label>
                                                 <asp:Label ID="lblOfferhelp" runat="server" Text="Offer help to" meta:resourcekey="lblofferHelpresource"></asp:Label>
                                              <asp:Label ID="lbltoname3" runat="server" Text='<%# Eval("Toname") %>'></asp:Label>
                                                <% }
                                                   
                                                     else  if(Convert.ToString(ViewState["logpointinfo"])=="ReadDoc")
                                                {%>
                                                <asp:Label ID="lblFrmoName4" runat="server" Text='<%# Eval("Fromname") %>'></asp:Label>
                                                 <asp:Label ID="lblReadDoc" runat="server" Text="read/download document" meta:resourcekey="lblReadDocresource1"></asp:Label>
                                              <asp:Label ID="lbltoname4" runat="server" Text='<%# Eval("rdDoc") %>'></asp:Label>
                                                <% }
                                                   
                                                   else  if(Convert.ToString(ViewState["logpointinfo"])=="UploadPublicDoc")
                                                {%>
                                                <asp:Label ID="lblFrmoName5" runat="server" Text='<%# Eval("Fromname") %>'></asp:Label>
                                                 <asp:Label ID="lblUploadDoc" runat="server" Text="has uploaded document" meta:resourcekey="lblUploadDocresource1"></asp:Label>
                                              <asp:Label ID="lbltoname5" runat="server" Text='<%# Eval("docFileName_Friendly") %>'></asp:Label>
                                                <% }
                                                      else  if(Convert.ToString(ViewState["logpointinfo"])=="OpenCompetencesScreen")
                                                {%>
                                                <asp:Label ID="lblFrmoName6" runat="server" Text='<%# Eval("Fromname") %>'></asp:Label>
                                                 <asp:Label ID="lblopenCompetence" runat="server" Text="open competence screen" meta:resourcekey="lblopenCompetenceresource1"></asp:Label>
                                             
                                                <% }
                                                   
                                                   else  if(Convert.ToString(ViewState["logpointinfo"])=="SignIn")
                                                {%>
                                                <asp:Label ID="lblFrmoName7" runat="server" Text='<%# Eval("Fromname") %>'></asp:Label>
                                                 <asp:Label ID="lblSignin" runat="server" Text="sign in" meta:resourcekey="lblSigninresource1"></asp:Label>
                                             
                                                <% }
                                                     else  if(Convert.ToString(ViewState["logpointinfo"])=="FinalFillCompetences")
                                                {%>
                                                <asp:Label ID="lblFrmoName8" runat="server" Text='<%# Eval("Fromname") %>'></asp:Label>
                                                 <asp:Label ID="lblFillComp" runat="server" Text="fill final competence" meta:resourcekey="llblFillCompresource1"></asp:Label>
                                             
                                                <% }
                                             else 
                                                {
                                                %>
                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("logDescription") %>'></asp:Label>
                                                <% } %>
                                           
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs"  />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                            Width="50%" ForeColor="White" BorderWidth="0px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date" meta:resourcekey="TemplateFieldResource2">
                                        <ItemTemplate>
                                            <asp:Label ID="lblUNamer1" runat="server" Text='<%# Eval("logCreateDate") %>'
                                                meta:resourcekey="lblUNamer1Resource1"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                            Width="20%" ForeColor="White" BorderWidth="0px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Point" meta:resourcekey="TemplateFieldResource3">
                                        <ItemTemplate>
                                            <asp:Label ID="lblUserNamer1" runat="server" Text='<%# Eval("TotalPoint") %>' 
                                                meta:resourcekey="lblUserNamer1Resource1"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                            Width="10%" ForeColor="White" BorderWidth="0px" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer" style="border: 0px none; width: 99%; padding-top: 0px; margin-top: 0px;">
        
        <asp:Button runat="server" ID="Button2" class="btn btn-default black" Text="Back"
           OnClick="btnback_Click" meta:resourcekey="Button2Resource1" ></asp:Button>
    </div>
    <style type="text/css">
        .yellow
        {
            border-radius: 0px;
        }
    </style>
</asp:Content>
