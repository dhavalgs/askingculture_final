﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master" AutoEventWireup="true" CodeFile="ForumNewTopic.aspx.cs" Inherits="Organisation_ForumNewTopic" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="../Scripts/ckeditor/ckeditor.js"></script>
     <link href="../assets/css/jquery.tagsinput.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:HiddenField ID="hdnAll" runat="server" meta:resourcekey="drpAllResourse"></asp:HiddenField>
    <asp:HiddenField ID="hdnSelectCategoty" runat="server" meta:resourcekey="drpSelectCategoty"></asp:HiddenField>
    <asp:HiddenField ID="hdnSelectCompetence" runat="server" meta:resourcekey="drpSelectCompetence"></asp:HiddenField>
   
     <asp:HiddenField ID="hdnSelectPerson" runat="server" meta:resourcekey="drpSelectPerson"></asp:HiddenField>
    <asp:HiddenField ID="hdnSelectDivision" runat="server" meta:resourcekey="drpSelectDivision"></asp:HiddenField>
    <asp:HiddenField ID="hdnSelectJob" runat="server" meta:resourcekey="drpSelectJob"></asp:HiddenField>

    <div class="col-md-6">
        <div class="heading-sec">
            <h1><asp:Literal ID="Literal11" runat="server" meta:resourcekey="lblAddNewForumResourseTop" EnableViewState="false" /><i><span runat="server" id="Conatct"></span></i>
            </h1>
        </div>
    </div>
    <div style="height: 47px">
    </div>

    <div class="col-md-12">
        <div class="chat-widget widget-body" style="background: #fff;">
            <div class="chat-widget-head yellow yellow-radius">
                <h4 style="margin: -5px 0px 0px;">
                    <asp:Literal ID="lblAddNewForum" runat="server" meta:resourcekey="lblAddNewForumResourse" EnableViewState="false" />
                </h4>
            </div>
            <div class="indicatesRequireFiled">
                <i>*  
                    <asp:Literal ID="Literal5" runat="server" meta:resourcekey="Indicatesrequiredfield" EnableViewState="false" />
                    <%-- <%= CommonMessages.Indicatesrequiredfield %>--%></i>
            </div>




            <div class="col-md-6" style="width: 100%;">
                <div class="inline-form">
                    <label class="c-label">
                        <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Name" EnableViewState="false" /><%-- <%= CommonMessages.Name%>--%>:<span class="starValidation">*</span></label>
                    <asp:HiddenField ID="hfTagId" Value="0" runat="server" />

                    <asp:TextBox runat="server" ID="txtName"
                        placeholder="What's your question? Be specific."
                        meta:resourcekey="txtNameResource1" />

                    <br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5"
                        runat="server" ControlToValidate="txtName"
                        ErrorMessage="Please enter question."
                        CssClass="commonerrormsg" Display="Dynamic"
                        ValidationGroup="chk"
                        meta:resourcekey="RequiredFieldValidator5Resource1"></asp:RequiredFieldValidator>
                </div>
            </div>


                 <div class="col-md-6" style="width: 100%;">
                <div class="inline-form">
                    <label class="c-label">
                      <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Tags" EnableViewState="false" />   <span class="starValidation"></span></label>
                    <asp:HiddenField ID="HiddenField2" Value="0" runat="server" />


                    <asp:TextBox runat="server" ID="tag1" MaxLength="80" Width="100%"
                        CssClass="form-control tagsinput"
                        placeholder="Please enter tag. (Helpfull for search)"
                        meta:resourcekey="tag1Resource1" />


                    <br />


                </div>


            </div>

             <div class="form-group" style="margin-top: 188px; display:block;">
                                <div class="col-md-6">
                                    <div class="">
                                        <label>
                                            <asp:Literal ID="Literal4" runat="server" meta:resourcekey="JobTitleScopToTarget" EnableViewState="false" /> 
                                        </label>
                                    </div>
                                    <div class="ddpDivision modal-body">
                                        <asp:DropDownList Width="250px" ID="chkListJobtype" runat="server" CssClass="chkliststyle form-control"
                                            RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="1" Datafield="description"
                                            DataValueField="value" Style="width: 100%; overflow: auto;">
                                        </asp:DropDownList>
                                        
                                        <%--    <asp:CheckBoxList Width="250px" ID="chkListJobtype" runat="server" CssClass="chkliststyle"
                                            RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description"
                                            DataValueField="value" Style="width: 100%; overflow: auto;">
                                        </asp:CheckBoxList>--%>
                                        <%--<asp:TextBox runat="server" placeholder="DESCRIPTION :" ID="txtDESCRIPTION" MaxLength="500"
                            TextMode="MultiLine" meta:resourcekey="txtDESCRIPTIONResource1" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDESCRIPTION"
                            ErrorMessage="Please enter DESCRIPTION." CssClass="commonerrormsg" Display="Dynamic"
                            ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator1Resource1"></asp:RequiredFieldValidator>
                        <br />--%>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>
                                       <asp:Literal ID="Literal6" runat="server" meta:resourcekey="OrgUnitToTarget" EnableViewState="false" /> 
                                    </label>
                                    <div class="ddpDivision modal-body">
                                        <asp:DropDownList Width="250px" ID="chkListDivision" runat="server" CssClass="chkliststyle form-control"
                                            RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="1" Datafield="description"
                                            DataValueField="value" Style="width: 100%; overflow: auto;">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                            </div>
            <br/><br/>
              <div class="form-group" style="margin-top: 160px">
                                <div class="col-md-3"><label><asp:Literal ID="Literal7" runat="server" meta:resourcekey="CompDevDoc" EnableViewState="false" /> </label> </div>
                                <div class="col-md-9">
                                    <asp:RadioButtonList runat="server" ID="chkIsCompDevDoc" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="Yes" Value="Competence Development Document"  meta:resourcekey="rbnchkIsCompDevDocResourse" />
                                        <asp:ListItem Text="No" Value="General" Selected="True" meta:resourcekey="rbnchkIsCompDevDocResourse1"/>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                            
                             
                            <br/><br/>
                            <div class="form-group" style="display: none;" id="doccompetence">
                                <div class="col-md-3" style="padding-top: 7px;"><label><asp:Literal ID="Literal8" runat="server" meta:resourcekey="Competence" EnableViewState="false" />  </label></div>
                                <div class="col-md-9">
                                    <asp:DropDownList runat="server" ID="ddlCompetences"  CssClass="form-control">
                                      
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group" style="clear: both;">
                                <div class="col-md-3" style="padding-top: 15px"><label><asp:Literal ID="Literal9" runat="server" meta:resourcekey="Level" EnableViewState="false" />  </label></div>
                                <div class="col-md-9">
                                    <asp:RadioButtonList runat="server" ID="rdoLevel"  RepeatDirection="Horizontal" RepeatLayout="Table" CellPadding="15" CellSpacing="10">
                                        <%--<asp:ListItem Text="1 to 2" Selected="True" />
                                         <asp:ListItem Text="2 to 3" />
                                         <asp:ListItem Text="3 to 4" />
                                         <asp:ListItem Text="4 to 5" />--%>
                                        <asp:ListItem Text="1 to 2" Value="1 to 2" meta:resourcekey="OneToTwoResource" Selected="True">1 to 2</asp:ListItem>
                                        <asp:ListItem Text="2 to 3" Value="2 to 3" meta:resourcekey="TwoToThreeResource">2 to 3</asp:ListItem>
                                        <asp:ListItem Text="3 to 4" Value="3 to 4" meta:resourcekey="ThreeToFourResource">3 to 4</asp:ListItem>
                                        <asp:ListItem Text="4 to 5" Value="4 to 5" meta:resourcekey="FourToFiveResource">4 to 5</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                            </div>
                            <br/><br/>
            <div class="form-group" style="" id="docCategory">
                                <div class="col-md-3"><label><asp:Literal ID="Literal10" runat="server" meta:resourcekey="CompetenceCategory" EnableViewState="false" /></label></div>
                                <div class="col-md-9">
                                    <asp:DropDownList runat="server" ID="ddlDocumentCategory"  CssClass="form-control">
                                      
                                    </asp:DropDownList>
                                </div>
                            </div>
            <br/><br/>
                             

            <div class="col-md-6" style="width: 100%;display: none">
                <div class="inline-form">
                    <label class="c-label">
                        Category:</label>
                    <asp:DropDownList ID="ddForumCat" runat="server"
                        meta:resourcekey="ddForumCatResource1">
                    </asp:DropDownList>

                </div>
            </div>

            <div class="col-md-6" style="width: 100%;">
                <div class="inline-form">
                    <label class="c-label">
                        <asp:Literal ID="Literal2" runat="server" meta:resourcekey="Description" EnableViewState="false" /><%--   <%= CommonMessages.Description%>--%>:<span class="starValidation">*</span></label>
                    <div style="margin-top: 31px;">

                        <asp:TextBox runat="server" ID="txtDescription" TextMode="MultiLine" Height="300px"
                            MaxLength="500" CssClass="ckeditor" Width="90%"
                            meta:resourcekey="txtDescriptionResource1" />
                        <asp:Label runat="server" ID="lblmsgDescription" CssClass="commonerrormsg"
                            meta:resourcekey="lblmsgDescriptionResource1"></asp:Label>


                    </div>
                </div>
            </div>


       

            <div class="modal-footer" style="border: 0px;">
                <asp:Button runat="server" ID="btnsubmit" class="btn btn-primary yellow"
                    Text="Post Your Question" OnClick="btnsubmit_click" ValidationGroup="chk"
                    Style="border-radius: 5px;" meta:resourcekey="btnsubmitResource1"></asp:Button>
                <asp:Button runat="server" ID="btnCancel" class="btn btn-primary black"
                    Text="Cancel" PostBackUrl="ForumAll.aspx"
                    meta:resourcekey="btnCancelResource1"></asp:Button>
                <%-- <button data-dismiss="modal" class="btn btn-default black" type="button">Cancel  </button>--%>
            </div>



        </div>
        <asp:HiddenField runat="server" meta:resourcekey="AddATag" ID="hdnAddATag" />
    </div>


    <%--Use for tag jquery plugin....... SAURIN :' 2015 01 27--
    
    Source : http://xoxco.com/projects/code/tagsinput/
    

    --%>
    <link rel="stylesheet" type="text/css" href="http://xoxco.com/projects/code/tagsinput/jquery.tagsinput.css" />
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
    <script type="text/javascript" src="https://xoxco.com/projects/code/tagsinput/jquery.tagsinput.js"></script>
    <!-- To test using the original jQuery.autocomplete, uncomment the following -->
    <!--
	<script type='text/javascript' src='http://xoxco.com/x/tagsinput/jquery-autocomplete/jquery.autocomplete.min.js'></script>
	<link rel="stylesheet" type="text/css" href="http://xoxco.com/x/tagsinput/jquery-autocomplete/jquery.autocomplete.css" />
	-->
    <script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/jquery-ui.min.js'></script>
    <link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/themes/start/jquery-ui.css" />
     <script src="../assets/js/libs/tagsInput.js" type="text/javascript"></script>
    <script type="text/javascript">
  function UpdateResourceOfTag() {
      $("#ContentPlaceHolder1_tag1_tagsinput_tag").val($("#ContentPlaceHolder1_hdnAddATag").val());
            }
        function activateTagsInput() {
          
            setTimeout(function () {
                $('#ContentPlaceHolder1_tag1_tagsinput').tagsInput({ onAddTag: UpdateResourceOfTag });
                //$('#ContentPlaceHolder1_tag1_tagsinput').tagsInput({ onAddTag: onAddTag });
                $("#ContentPlaceHolder1_tag1_tagsinput_tag").val($("#ContentPlaceHolder1_hdnAddATag").val());
                $("#ContentPlaceHolder1_tag1_tagsinput_tagsinput").css("width", "100%");
                $("#ContentPlaceHolder1_tag1_tagsinput_tag").css("width", "120px");
            }, 500);
        }
        $(document).ready(function () {
            SetExpandCollapse();
            $("#ContentPlaceHolder1_tag1_tag").removeAttr("data-default");
            $("#ContentPlaceHolder1_chkIsCompDevDoc_0").click(function () {
                document.getElementById("<%= ddlDocumentCategory.ClientID %>").selectedIndex = 0;
                
                $("#docCategory").hide();
                $("#doccompetence").show();
            });
            $("#ContentPlaceHolder1_chkIsCompDevDoc_1").click(function () {
                document.getElementById("<%= ddlCompetences.ClientID %>").selectedIndex=0;
                
                $("#docCategory").show();
                $("#doccompetence").hide();
            });
            setTimeout(function () {
                activateTagsInput();
               // ExampanCollapsMenu();
                // tabMenuClick(); 
                
                $(".ForumAll").addClass("active_page");
            }, 500);
            GetTagsForAutoComplete();
            var x;
            setTimeout(function () {
                var x = document.getElementById("<%= tag1.ClientID %>");
                // alert(x);
                $("<%= tag1.ClientID %>").tagsInput({
                    // my parameters here
                });
            }, 1500);
            setTimeout(function () {

                // tabMenuClick();
                $(".ForumAll").addClass("active_page");
            }, 500);
            $('#ContentPlaceHolder1_tag1').tagsInput({
                'width': '100%',
                autocomplete_url: data,
                autocomplete: { selectFirst: true, width: '100px', autoFill: true }
                // my parameters here
            });
        });
        var data;
        function GetTagsForAutoComplete() {

            $.ajax({
                type: "POST",
                async: false,
                url: "ForumNewTopic.aspx/GetForumTagsByforumCompanyId",

                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#divResult").html(msg.d);
                    data = msg.d;
                    // alert(data);
                    setTimeout(function () {

                    }, 500);

                    //return msg.d;

                },
                error: function (e) {
                    $("#divResult").html("WebSerivce unreachable");
                }
            });
        }
    </script>

</asp:Content>

