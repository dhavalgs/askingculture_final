﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true" CodeFile="TeamTargetView.aspx.cs" Inherits="Organisation_TeamTargetView" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <style type="text/css">
        .table > thead > tr > th {
            vertical-align: middle;
        }
    </style>
    <style type="text/css">
        body {
            font-family: Arial;
            font-size: 11pt;
        }

        .modalBackground {
            background-color: Black;
            filter: alpha(opacity=40);
            opacity: 0.4;
        }

        .modalPopup1 {
            background-color: #FFFFFF;
            width: 300px;
            border: 3px solid #0DA9D0;
        }

        .modalPopup {
            background: none repeat scroll 0 0 #ffffff;
            display: table;
            float: none;
            margin: 96px auto;
            width: 600px !important;
            top: 10px !important;
        }

            .modalPopup .header {
                background-color: #2FBDF1;
                height: 30px;
                color: White;
                line-height: 30px;
                text-align: center;
                font-weight: bold;
            }

            .modalPopup .body {
                min-height: 50px;
                line-height: 30px;
                text-align: center;
                padding: 5px;
            }

            .modalPopup .footer {
                padding: 3px;
            }

            .modalPopup .button {
                height: 23px;
                color: White;
                line-height: 23px;
                text-align: center;
                font-weight: bold;
                cursor: pointer;
                background-color: #9F9F9F;
                border: 1px solid #5C5C5C;
            }

            .modalPopup td {
                text-align: left;
            }

        .button {
            background-color: transparent;
        }

        .yellow {
            border-radius: 0px;
        }

        .reply {
            clear: both;
        }

        #scrollbox6 {
            overflow-x: hidden;
            overflow-y: auto;
            background: #f4f4f4;
        }

        .labelBorder {
            border-width: 1px;
            border-style: solid;
            border-color: lightgray;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg"
        meta:resourcekey="lblMsgResource1"></asp:Label>
    <div class="col-md-6">
        <div class="heading-sec">
            <h1 style="margin-left: 15px;">
                <%-- <%= CommonMessages.Viewrequest%>--%>
                <asp:Literal ID="Literal1" runat="server" meta:resourcekey="TeamTarget" EnableViewState="false" />
                <i><span runat="server" id="Employee"></span>
                </i>
            </h1>
        </div>
    </div>
    <div class="chat-widget widget-body" style="background: #fff;">
    </div>
    <div class="col-md-12" style="margin-top: 20px; width: 100%;">
        <%--<div id="graph-wrapper">--%>
        <%-- <div class="chat-widget-head yellow" style="width: 97.5%; margin-left: 15px; margin-bottom: -16px;">
            <h4 style="margin: -6px 0px 0px;">
                <span runat="server" id="spname"></span>'s  
                <asp:Literal ID="Literal2" runat="server" meta:resourcekey="Competencelevel" EnableViewState="false" />
            </h4>
        </div>--%>

        <div class="col-md-12">
            <asp:DropDownList ID="ddlTeam" runat="server" Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; color: white; overflow: auto; width: 25%; height: 32px; float: right; display: inline; margin-bottom: 20px; margin-Top: 6px; margin-right: 20px;"
                CssClass="chkliststyle form-control"
                AutoPostBack="True"
                OnSelectedIndexChanged="ddActCate_OnSelectedIndexChanged">
            </asp:DropDownList>
        </div>
        <div class="col-md-12">
            <div class="chart-tab">
                <div id="tabs-container">

                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>

                            <asp:GridView ID="gridTotalPoint" runat="server" AutoGenerateColumns="False" CellPadding="0"
                                Width="100%" GridLines="None" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                                EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>'
                                BackColor="White" OnRowDataBound="gridTotalPoint_RowDataBound"
                                meta:resourcekey="gvGridResource1">
                                <HeaderStyle CssClass="aa" />
                                <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                <AlternatingRowStyle BackColor="White" />
                                <Columns>
                                    <asp:TemplateField HeaderText="ID" meta:resourcekey="TemplateFieldResource1">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="10px" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                            Width="2%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Points"
                                        meta:resourcekey="TemplateFieldResource22">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPoints" runat="server" Text='Point'
                                                meta:resourcekey="lblActualResource1"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                            Width="15%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="" meta:resourcekey="TemplateFieldResource111">
                                        <ItemTemplate>
                                            <asp:Label ID="lblActual" runat="server" Text='Team Actual'
                                                meta:resourcekey="lblActualResource2"></asp:Label><br />
                                            <asp:Label ID="lblTarget" runat="server" Text='Team Target'
                                                meta:resourcekey="lblTargetResource2"></asp:Label><br />
                                            <asp:HiddenField ID="skillUserID" runat="server" Value='<%# Eval("UserID") %>' />
                                            <asp:HiddenField ID="hdnteamActID" runat="server" Value='<%# Eval("teamActID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="10px" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                            Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="PLP" meta:resourcekey="PLPResource1">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtPLPActual" runat="server" Text="<%# bind('ActualPLP') %>" onkeyup="checkNumber(this)"
                                                Width="50px" meta:resourcekey="txtPLPActualResource1" ReadOnly="true"
                                                class='<%# String.Format("PointActual{0}", Eval("teamActID").ToString()) %>'
                                                ></asp:TextBox>

                                            <br />
                                            <asp:TextBox ID="txtPLPTarget" runat="server" Text="<%# bind('TargetPLP') %>" onkeyup="checkNumber(this)"
                                                Width="50px" meta:resourcekey="txtPLPTargetResource1" 
                                                class='<%# String.Format("PointTarget{0}", Eval("teamActID").ToString()) %>'
                                                 onchange="calcPointTotal(this);"
                                                ></asp:TextBox>

                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                            Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ALP" meta:resourcekey="ALPResource1">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtALPActual" runat="server" Text="<%# bind('ActualALP') %>" onkeyup="checkNumber(this)"
                                                Width="50px" meta:resourcekey="txtALPActualResource1" ReadOnly="true"
                                                class='<%# String.Format("PointActual{0}", Eval("teamActID").ToString()) %>'
                                                ></asp:TextBox>

                                            <br />
                                            <asp:TextBox ID="txtALPTarget" runat="server" Text="<%# bind('TargetALP') %>" onkeyup="checkNumber(this)"
                                                Width="50px" meta:resourcekey="txtALPTargetResource1" 
                                                 class='<%# String.Format("PointTarget{0}", Eval("teamActID").ToString()) %>'
                                                 onchange="calcPointTotal(this);"
                                                ></asp:TextBox>

                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                            Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Total" meta:resourcekey="TotalResource1">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtPointTotalActual" runat="server" Text="<%# bind('ActualTotal') %>" onkeyup="checkNumber(this)"
                                                Width="50px" meta:resourcekey="txtPointTotalActualResource1" ReadOnly="true"
                                                class='<%# String.Format("PointActual{0}", Eval("teamActID").ToString()) %>'
                                                ></asp:TextBox>

                                            <br />
                                            <asp:TextBox ID="txtPointTotalTarget" runat="server" Text="<%# bind('TargetTotal') %>" onkeyup="checkNumber(this)"
                                                Width="50px" meta:resourcekey="txtPointTotalTargetResource1" 
                                                 class='<%# String.Format("PointTarget{0}", Eval("teamActID").ToString()) %>'
                                                 onchange="calcPointTotal(this);"
                                                ></asp:TextBox>

                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                            Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="PDP" meta:resourcekey="PDPResource1">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtPDPActual" runat="server" Text="<%# bind('PDPActual') %>" onkeyup="checkNumber(this)"
                                                Width="50px" meta:resourcekey="txtPDPActualResource1" ReadOnly="true"
                                                class='<%# String.Format("PointActual{0}", Eval("teamActID").ToString()) %>'
                                                ></asp:TextBox>

                                            <br />
                                            <asp:TextBox ID="txtPDPTarget" runat="server" Text="<%# bind('PDPTarget') %>" onkeyup="checkNumber(this)"
                                                Width="50px" meta:resourcekey="txtPDPTargetResource1"
                                                 class='<%# String.Format("PointTarget{0}", Eval("teamActID").ToString()) %>'
                                                 onchange="calcPointTotal(this);"
                                                ></asp:TextBox>

                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                            Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>


                            <asp:GridView ID="gvGrid" runat="server" AutoGenerateColumns="False" CellPadding="0"
                                Width="100%" GridLines="None" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                                EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>'
                                BackColor="White" OnRowDataBound="gvGrid_RowDataBound"
                                meta:resourcekey="gvGridResource1">
                                <HeaderStyle CssClass="aa" />
                                <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                <AlternatingRowStyle BackColor="White" />
                                <Columns>
                                    <asp:TemplateField HeaderText="ID" meta:resourcekey="TemplateFieldResource1">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="10px" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                            Width="2%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="COMPETENCE"
                                        meta:resourcekey="TemplateFieldResource2">
                                        <ItemTemplate>
                                            <%if(Convert.ToString(Session["Culture"]) == "Danish"){%>
                                            <asp:Label ID="lblUNamer" runat="server" Text='<%# Eval("comCompetenceDN") %>'
                                                meta:resourcekey="lblUNamerResource1"></asp:Label>
                                            <%} %>
                                            <%else{ %>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("comCompetence") %>'
                                                meta:resourcekey="lblUNamerResource1"></asp:Label>
                                            <%} %>
                                            <asp:HiddenField ID="skillComId" runat="server" Value="<%# bind('skillComId') %>" />
                                            <asp:HiddenField ID="tID" runat="server" Value='<%# Eval("tID") %>' />

                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                            Width="15%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="" meta:resourcekey="TemplateFieldResource111">
                                        <ItemTemplate>
                                            <asp:Label ID="lblActual" runat="server" Text='Team Actual'
                                                meta:resourcekey="lblActualResource11"></asp:Label><br />
                                            <asp:Label ID="lblTarget" runat="server" Text='Team Target'
                                                meta:resourcekey="lblTargetResource11"></asp:Label><br />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="10px" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                            Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Level1" meta:resourcekey="Level1Resource1">
                                        <ItemTemplate>
                                            <asp:Label ID="txtActualOne" runat="server" Text="<%# bind('ActualOne') %>" onkeyup="checkNumber(this)"
                                                Width="50px" meta:resourcekey="txtActualOneResource1" ReadOnly="true"
                                                CssClass='<%# String.Format("labelBorder Actual{0}", Eval("tID").ToString()) %>'></asp:Label>
                                            <%-- <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"
                                                TargetControlID="txtActualOne" Enabled="True">
                                            </cc1:FilteredTextBoxExtender>--%>
                                            <br />
                                            <asp:TextBox ID="txtTargetOne" runat="server" Text='<%# Eval("TargetOne") %>' onkeyup="checkNumber(this)"
                                                Width="50px" meta:resourcekey="txtTargetOneResource1"
                                                class='<%# String.Format("Target{0}", Eval("tID").ToString()) %>'
                                                onchange="calcTotal(this);"></asp:TextBox>

                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers"
                                                TargetControlID="txtTargetOne" Enabled="True">
                                            </cc1:FilteredTextBoxExtender>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                            Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Level2" meta:resourcekey="Level2Resource1">
                                        <ItemTemplate>
                                            <asp:Label ID="txtActualTwo" runat="server" Text="<%# bind('ActualTwo') %>" onkeyup="checkNumber(this)"
                                                Width="50px" meta:resourcekey="txtActualTwoResource1" ReadOnly="true"
                                                CssClass='<%# String.Format("labelBorder Actual{0}", Eval("tID").ToString()) %>'></asp:Label>
                                            <%-- <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers"
                                                TargetControlID="txtActualTwo" Enabled="True">
                                            </cc1:FilteredTextBoxExtender>--%>
                                            <br />
                                            <asp:TextBox ID="txtTargetTwo" runat="server" Text='<%# Eval("TargetTwo") %>' onkeyup="checkNumber(this)"
                                                Width="50px" meta:resourcekey="txtTargetTwoResource1"
                                                class='<%# String.Format("Target{0}", Eval("tID").ToString()) %>'
                                                onchange="calcTotal(this);"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers"
                                                TargetControlID="txtTargetTwo" Enabled="True">
                                            </cc1:FilteredTextBoxExtender>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                            Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Level3" meta:resourcekey="Level3Resource1">
                                        <ItemTemplate>
                                            <asp:Label ID="txtActualThree" runat="server" Text="<%# bind('ActualThree') %>" onkeyup="checkNumber(this)"
                                                Width="50px" meta:resourcekey="txtActualThreeResource1" ReadOnly="true"
                                                CssClass='<%# String.Format("labelBorder Actual{0}", Eval("tID").ToString()) %>'></asp:Label>
                                            <%--  <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Numbers"
                                                TargetControlID="txtActualThree" Enabled="True">
                                            </cc1:FilteredTextBoxExtender>--%>
                                            <br />
                                            <asp:TextBox ID="txtTargetThree" runat="server" Text="<%# bind('TargetThree') %>" onkeyup="checkNumber(this)"
                                                Width="50px" meta:resourcekey="txtTargetThreeResource1"
                                                class='<%# String.Format("Target{0}", Eval("tID").ToString()) %>'
                                                onchange="calcTotal(this);"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Numbers"
                                                TargetControlID="txtTargetThree" Enabled="True">
                                            </cc1:FilteredTextBoxExtender>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                            Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Level4" meta:resourcekey="Level4Resource1">
                                        <ItemTemplate>
                                            <asp:Label ID="txtActualFour" runat="server" Text="<%# bind('ActualFour') %>" onkeyup="checkNumber(this)"
                                                Width="50px" meta:resourcekey="txtActualFourResource1" ReadOnly="true"
                                                CssClass='<%# String.Format("labelBorder Actual{0}", Eval("tID").ToString()) %>'></asp:Label>
                                            <%-- <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Numbers"
                                                TargetControlID="txtActualFour" Enabled="True">
                                            </cc1:FilteredTextBoxExtender>--%>
                                            <br />
                                            <asp:TextBox ID="txtTargetFour" runat="server" Text="<%# bind('TargetFour') %>" onkeyup="checkNumber(this)"
                                                Width="50px" meta:resourcekey="txtTargetFourResource1"
                                                class='<%# String.Format("Target{0}", Eval("tID").ToString()) %>'
                                                onchange="calcTotal(this);"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterType="Numbers"
                                                TargetControlID="txtTargetFour" Enabled="True">
                                            </cc1:FilteredTextBoxExtender>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                            Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Level5" meta:resourcekey="Level5Resource1">
                                        <ItemTemplate>
                                            <asp:Label ID="txtActualFive" runat="server" Text="<%# bind('ActualFive') %>" onkeyup="checkNumber(this)"
                                                Width="50px" meta:resourcekey="txtActualFiveResource1" ReadOnly="true"
                                                CssClass='<%# String.Format("labelBorder Actual{0}", Eval("tID").ToString()) %>'></asp:Label>
                                            <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterType="Numbers"
                                                TargetControlID="txtActualFive" Enabled="True">
                                            </cc1:FilteredTextBoxExtender>--%>
                                            <br />
                                            <asp:TextBox ID="txtTargetFive" runat="server" Text="<%# bind('TargetFive') %>" onkeyup="checkNumber(this)"
                                                Width="50px" meta:resourcekey="txtTargetFiveResource1"
                                                class='<%# String.Format("Target{0}", Eval("tID").ToString()) %>'
                                                onchange="return calcTotal(this);"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterType="Numbers"
                                                TargetControlID="txtTargetFive" Enabled="True">
                                            </cc1:FilteredTextBoxExtender>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                            Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Total" meta:resourcekey="Total1Resource1">
                                        <ItemTemplate>
                                            <asp:Label ID="txtTotalActual" runat="server" Text="<%# bind('TotalActual') %>" onkeyup="checkNumber(this)"
                                                Width="50px" meta:resourcekey="txtActualFiveResource1" ReadOnly="true" CssClass="labelBorder"></asp:Label>
                                            <%-- <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterType="Numbers"
                                                TargetControlID="txtActualFive" Enabled="True">
                                            </cc1:FilteredTextBoxExtender>--%>
                                            <br />
                                            <asp:Label ID="txtTotalTarget" runat="server" Text="<%# bind('TotalTarget') %>" onkeyup="checkNumber(this)"
                                                Width="50px" meta:resourcekey="txtTargetFiveResource1" ReadOnly="true"
                                                CssClass='<%# String.Format("labelBorder Total{0}", Eval("tID").ToString()) %>'></asp:Label>
                                            <%-- <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" FilterType="Numbers"
                                                TargetControlID="txtTargetFive" Enabled="True">
                                            </cc1:FilteredTextBoxExtender>--%>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                            Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Average" meta:resourcekey="AverageResource1">
                                        <ItemTemplate>
                                            <asp:Label ID="txtActualAverage" runat="server" Text="<%# bind('ActualAverage') %>" onkeyup="checkNumber(this)"
                                                Width="50px" meta:resourcekey="txtActualFiveResource1" ReadOnly="true" CssClass="labelBorder"></asp:Label>
                                            <%-- <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" FilterType="Numbers"
                                                TargetControlID="txtActualFive" Enabled="True">
                                            </cc1:FilteredTextBoxExtender>--%>
                                            <br />
                                            <asp:Label ID="txtTargetAverage" runat="server" Text="<%# bind('TargetAverage') %>" onkeyup="checkNumber(this)"
                                                Width="50px" meta:resourcekey="txtTargetFiveResource1" ReadOnly="true"
                                                CssClass='<%# String.Format("labelBorder Avg{0}", Eval("tID").ToString()) %>'></asp:Label>
                                            <%-- <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" FilterType="Numbers"
                                                TargetControlID="txtTargetFive" Enabled="True">
                                            </cc1:FilteredTextBoxExtender>--%>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                            Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>



                        </ContentTemplate>
                        <%--<Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtTarget" EventName="TextChanged" />
                                    </Triggers>--%>
                    </asp:UpdatePanel>

                </div>

            </div>
        </div>
        <div class="col-xs-12" id="submit" runat="server" style="margin: 10px; padding: 10px; width: 98%;">
            <asp:Button runat="server" ID="btnCancel" Text="Cancel" CssClass="btn black pull-right"
                ValidationGroup="chk" CausesValidation="False" OnClick="btnCancel_click"
                Style="color: white;" meta:resourcekey="btnCancelResource1" />
            <asp:Button runat="server" ID="btnsubmit" Text="Save" CssClass="btn blue pull-right"
                ValidationGroup="chk" OnClick="btnsubmit_click" Style="color: white;"
                meta:resourcekey="btnsubmitResource1" />
            <asp:Button runat="server" ID="btnback" Text="Back" CssClass="btn black pull-right"
                CausesValidation="False" OnClick="btnback_click" Visible="False"
                Style="color: white;" meta:resourcekey="btnbackResource1" />
        </div>
        <%--</div>--%>
    </div>
    <div style="display: none">
        <asp:Label runat="server" ID="lblNoRecord" CssClass="lblNoRecords" meta:resourcekey="NoRecordsFound"></asp:Label>
        <asp:Label runat="server" ID="lblTypeYourComments" CssClass="lblTypeYourComments" meta:resourcekey="PlsEnterComments"></asp:Label>
    </div>
    <style type="text/css">
        .yellow {
            border-radius: 0px;
        }
    </style>
    <script src="../assets/assets/js/libs/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script>
        function calcPointTotal(a) {
            debugger;
            var classname = $(a).attr('class');
            var ActualClass = 'PointActual' + classname.replace('PointTarget', '');
            var TargetClass = classname;
            var j = 0;
            $('.' + classname).each(function () {
               
                if (parseInt($('.' + ActualClass).eq(j).val()) >= parseInt($('.' + TargetClass).eq(j).val())) {
                    $('.' + ActualClass).eq(j).css("background-color", "#EBEBE0");
                }
                else {
                    $('.' + ActualClass).eq(j).css("background-color", "#FFE6E6");
                }
                j = parseInt(j) + 1;
            });
        }
    </script>
    <script type="text/javascript">
        function calcTotal(a) {
            var classname = $(a).attr('class');
            var ActualClass = 'Actual' + classname.replace('Target', '');
            var TargetClass = classname;
            var Totalclassname = 'Total' + classname.replace('Target', '');
            var Avgclassname = 'Avg' + classname.replace('Target', '');

            var tot = 0;
            var avger = 0.0;
            var avgCal = 0;
            var cnt = 1;
            var j = 0;
            //  alert(TargetClass);
            $('.' + classname).each(function () {
                // alert('actual :' + $('.' + ActualClass).eq(j).text() + ' target :' + $('.' + TargetClass).eq(j).val());
                if (parseInt($('.' + ActualClass).eq(j).text()) >= parseInt($('.' + TargetClass).eq(j).val())) {
                    $('.' + ActualClass).eq(j).css("background-color", "#EBEBE0");
                }
                else {
                    $('.' + ActualClass).eq(j).css("background-color", "#FFE6E6");
                }

                avgCal = parseInt(avgCal) + (parseInt(cnt) * parseInt($(this).val()));

                tot = parseInt(tot) + parseInt($(this).val());

                cnt = parseInt(cnt) + 1;
                j = parseInt(j) + 1;
            });
            debugger;
            avger = Math.round((parseFloat(avgCal) / tot) * 100) / 100;
            $('.' + Totalclassname).text(tot);
            $('.' + Avgclassname).text(avger);
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            setTimeout(function () {
                $(".active").click();
                onLoad();

                // tabMenuClick();
                // $(".myCompetence").addClass("active_page");
            }, 500);
            $(".pending_helprequest").addClass("active_page");
        });


        $(document).keypress(function (e) {
            if (e.keyCode === 13) {
                insertcomment();
                e.preventDefault();
                return false;
            }
        });
        function insertcomment() {

            var message = document.getElementById("txtcomment").value;
            var comid = $("#hdncom").val();

            if (comid != '0') {
                if (message != '') {

                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "View_request.aspx/insertmessage",
                        data: "{'message':'" + message + "','comid':'" + comid + "'}",
                        dataType: "json",
                        success: function (data) {
                            var obj = data.d;
                            if (obj == 'true') {
                                document.getElementById("txtcomment").value = '';
                                // GetmessgeById(touserid);
                                //GetmessgeById_afterinsert(touserid)
                                GetcommentById(comid, aId);
                                $("#" + aId).prev().prev().text(message); // copy text and paste on parent page's comment box
                                $("#scrollbox6").scrollTop(99999999999999999999);
                            }
                        },
                        error: function (result) {
                            // alert("Error");
                        }
                    });
                }
                else {
                    generate("warning", "Oops! You have missed text to send a massage.", "bottomCenter");
                    return false;
                }
            }
            else {
                generate("warning", "Please Select Contact", "bottomCenter");

                document.getElementById("message").value = '';
                return false;
            }
        }

        var aId;

        function checkNumber(ele) {
            var key = window.event ? event.keyCode : event.which;
            var keychar = String.fromCharCode(key);
            var el = ele.value;
            var input = el + "" + keychar;

            //your code to valide it.........

        }
        function onLoad() {

            SetExpandCollapse();
            setTimeout(function () {
                //ExampanCollapsMenu();

                // tabMenuClick();
                $(".pending_helprequest").addClass("active_page");
            }, 500);
        }
    </script>
</asp:Content>
