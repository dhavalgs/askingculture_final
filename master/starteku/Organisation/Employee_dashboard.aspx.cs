﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using startetku.Business.Logic;
using System.Data;
using System.Threading;
using System.Globalization;
using starteku_BusinessLogic;
using System.Web.UI.HtmlControls;
using System.Configuration;
using starteku_BusinessLogic.Model;


public partial class Organisation_Employee_dashboard : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       

        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            //Dashboard.InnerHtml = CommonMessages.Welcome + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            Dashboard.InnerHtml = "  " + Convert.ToString(Session["OrgUserName"]) + "!";

            lblAllEmployee.Text = Convert.ToString(Session["OrgUserName"]);

            StartJoyRideTour();
            GetAllInvitionRequest();
            GetHelpRequestedByUserId();
            // GetTO_DO_LISTbyid();

            // get_ds_to_do_list();
            if (Request.QueryString["user"] == "saveSuccess")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Redirect", "generate('information','" + hdnSystemUpdated.Value + "','bottomCenter')", true);
                
            }
            if (Session["isLastLoginSeen"] == null)
            {
                Session["isLastLoginSeen"] = "true";

                Page.ClientScript.RegisterStartupScript(this.GetType(), "",
                                                        "setTimeout(function(){generate('warning','" + hdnLastlogin.Value +
                                                        Convert.ToString(Session["userDateTime"]) +
                                                        ".','bottomRight');},990);", true);
            }
           
        }
        // Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "WelComeWithPoint();", true);
      if (!IsPostBack)
        {
            EmployeeSkillBM obj = new EmployeeSkillBM();
          
            var userid = 0;
            var usertype = 3;
            var userCompanyID = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
            userid = Convert.ToInt32(Session["OrgUserId"]);
            usertype = Convert.ToInt32(Session["OrguserType"]);
          
            obj.skillIsActive = true;
            obj.skillIsDeleted = false;
            obj.skillUserId = userid;
            obj.skillCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);

            obj.temp = 0;
            obj.mulsel="0";

            obj.mulselrecord();

            DataSet resds = obj.ds;
            var db = new startetkuEntities1();
            var actEnaData = db.ActicityEnableMasters.FirstOrDefault(o => o.Aemcompid == userid || o.Aemcompid == userCompanyID);
            if (actEnaData != null)
            {
                Session["Aemcompid"] = actEnaData.Aemcompid;
            }
            else
            {
                Session["Aemcompid"] = null;
            }


            lstFruits.DataSource = resds;
            if (Convert.ToString(Session["Culture"]) == "Danish")
            {
                lstFruits.DataTextField = "comCompetenceDN";
                //competences = Convert.ToString(ds.Tables[0].Rows[i]["comCompetenceDN"]);
            }
            else
            {
                lstFruits.DataTextField = "comCompetence";
                //competences = Convert.ToString(ds.Tables[0].Rows[i]["comCompetence"]);
            }

            
            lstFruits.DataValueField = "skillComId";
            lstFruits.DataBind();
        }
       
    }


    #region method

    private void StartJoyRideTour()
    {
        var obj = new JoyrideTourBM();
        try
        {

            var uid = Convert.ToInt32(Session["OrgUserId"]);
            if (uid <= 0) return;

            DataSet ds = obj.GetJoyrideTourByUserId(uid);


            if (ds == null) return;

            var dataRowCollection = ds.Tables[0].Rows;
            if (dataRowCollection != null)
            {
                var isAlreadyTakenTour = dataRowCollection[0]["isTourOff"];
                if (isAlreadyTakenTour != null)
                {
                    var x = Convert.ToInt32(dataRowCollection[0]["isTourOff"]);
                    if (x > 0) return;
                }
            }

            //DateTime date=DateTime.Now.AddDays(-2);
            //if (Session["userDateTime"] != null)
            //{
            //     date = Convert.ToDateTime(Session["userDateTime"]).Date;
            //}

            //var todaysDate = DateTime.Now.Date;

            //if (date == todaysDate)
            //{
            //    return;
            //}
            if (Session["isJoyRideTaken"] == null)
            {
                CmsBM objMail = new CmsBM();
                objMail.cmsName = "WelcomeMessage";

                objMail.SelectMailTemplateByName();
                DataSet dsMail = objMail.ds;
                
                if (dsMail.Tables[0].Rows.Count > 0)
                {
                    string confirmMail = "";
                    if (Convert.ToString(Session["Culture"]) == "English")
                    {
                        confirmMail = dsMail.Tables[0].Rows[0]["cmsDescription"].ToString();

                    }
                    else
                    {
                        confirmMail = dsMail.Tables[0].Rows[0]["cmsDescriptionDN"].ToString();

                    }
                    welcomeMsg.InnerHtml = confirmMail;
                }
                Session["isJoyRideTaken"] = "true";

                Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "JoyrideStart();", true);
            }
            //  ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "JoyrideStart();", false);
        }
        catch (Exception)
        {


        }
    }
    //protected void get_ds_to_do_list()
    //{
    //    TO_DO_LISTBM obj = new TO_DO_LISTBM();
    //    obj.listIsActive = true;
    //    obj.listIsDeleted = false;
    //    obj.listUserId = Convert.ToInt32(Session["OrgUserId"]);
    //    obj.GetTO_DO_LISTbyid();
    //    DataSet ds = obj.ds;
    //    ViewState["dolist"] = ds;
    //   // GetTO_DO_LISTbyid();
    //}
    //protected void GetTO_DO_LISTbyidcopy()
    //{
    //    TO_DO_LISTBM obj = new TO_DO_LISTBM();
    //    obj.listIsActive = true;
    //    obj.listIsDeleted = false;
    //    obj.listUserId = Convert.ToInt32(Session["OrgUserId"]);
    //    obj.GetTO_DO_LISTbyid();
    //    DataSet ds = obj.ds;
    //    if (ds != null)
    //    {
    //        if (ds.Tables[0].Rows.Count > 0)
    //        {
    //            Repeaterlist.DataSource = ds.Tables[0];
    //            Repeaterlist.DataBind();
    //        }
    //        else
    //        {
    //            Repeaterlist.DataSource = null;
    //            Repeaterlist.DataBind();
    //        }
    //    }
    //    else
    //    {
    //        Repeaterlist.DataSource = null;
    //        Repeaterlist.DataBind();
    //    }

    //}
    //protected void GetTO_DO_LISTbyid()
    //{
    //    DateTime startdate;
    //    DateTime Enddate;
    //    String s = HiddenField1.Value;
    //    if (s != "0")
    //    {
    //        string[] str = s.Split('-');

    //        startdate = Convert.ToDateTime(str[0]);
    //        Enddate = Convert.ToDateTime(str[1]);
    //    }
    //    else
    //    {
    //        startdate = Convert.ToDateTime(DateTime.Now).AddDays(-30);
    //        Enddate = Convert.ToDateTime(DateTime.Now);
    //    }

    //    DataSet ds = (DataSet)ViewState["dolist"];
    //    DataView dv = new DataView();
    //    dv = ds.Tables[0].DefaultView;
    //    dv.RowFilter = "listCreatedDate >= '" + startdate.ToString("MM-dd-yyyy") + "' AND listCreatedDate <= '" + Enddate.ToString("MM-dd-yyyy") + "'";
    //    //dv.RowFilter = "listCreatedDate BETWEEN '" + startdate + "' AND '" + Enddate + "'";

    //    DataTable dtitm = dv.ToTable();
    //    DataSet ds1 = new DataSet();
    //    ds1.Tables.Add(dtitm);

    //    if (ds1 != null)
    //    {
    //        if (ds1.Tables[0].Rows.Count > 0)
    //        {
    //            Repeaterlist.DataSource = ds1.Tables[0];
    //            Repeaterlist.DataBind();

    //        }
    //        else
    //        {
    //            Repeaterlist.DataSource = null;
    //            Repeaterlist.DataBind();
    //        }
    //    }
    //    else
    //    {
    //        Repeaterlist.DataSource = null;
    //        Repeaterlist.DataBind();
    //    }

    //}
    //protected void insert_to_do_list()
    //{
    //    DateTime stdate;
    //    DateTime enddate;

    //    string s = HiddenField1.Value;
    //    if (s != "0")
    //    {
    //        string[] str = s.Split('-');
    //        stdate = Convert.ToDateTime(str[0]);
    //        enddate = Convert.ToDateTime(str[1]);
    //    }
    //    else
    //    {
    //        stdate = Convert.ToDateTime(DateTime.Now).AddDays(-30);
    //        enddate = Convert.ToDateTime(DateTime.Now);
    //    }

    //    TO_DO_LISTBM obj = new TO_DO_LISTBM();
    //    obj.listUserId = Convert.ToInt32(Session["OrgUserId"]);
    //    obj.listsubject = TextBox1.Text;
    //    obj.listCreateBy = 0;
    //    obj.listCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
    //    obj.listIsActive = true;
    //    obj.listIsDeleted = false;
    //    obj.listCreatedDate = enddate;
    //   // obj.InsertTO_DO_LIST();
    //    if (obj.ReturnBoolean == true)
    //    {
    //       // GetTO_DO_LISTbyid();
    //        get_ds_to_do_list();
    //        TextBox1.Text = "";
    //    }
    //    else
    //    {
    //        lblMsg.Text = CommonModule.msgProblemInsertRecord;
    //    }
    //}

    //protected void delete(Int32 id)
    //{
    //    TO_DO_LISTBM obj = new TO_DO_LISTBM();
    //    obj.listId = Convert.ToInt32(id);
    //    obj.listIsActive = false;
    //    obj.listIsDeleted = false;
    //    obj.GetTO_DO_LISTUpdatestatus();

    //    DataSet ds = obj.ds;
    //    if (ds != null)
    //    {
    //        string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
    //        if (returnMsg == "success")
    //        {
    //           // GetTO_DO_LISTbyid();
    //            get_ds_to_do_list();
    //            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('" + CommonModule.msgRecordDeletedSuccss + "');</script>", false);
    //        }
    //        else
    //        {
    //            //lblMsg.Text = CommonModule.msgSomeProblemOccure;
    //            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('" + CommonModule.msgSomeProblemOccure + "');</script>", false);
    //        }
    //    }
    //    else
    //    {
    //        //lblMsg.Text = CommonModule.msgSomeProblemOccure;
    //        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('" + CommonModule.msgSomeProblemOccure + "');</script>", false);
    //    }
    //}
    protected void GetAllInvitionRequest()
    {
        InvitationBM obj = new InvitationBM();
        obj.invIsActive = true;
        obj.invIsDeleted = false;
        obj.invToUserId = Convert.ToInt32(Session["OrgUserId"]);
        obj.invCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllInvitionRequest();

        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {

                for (var i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToString(ds.Tables[0].Rows[i]["status"]).ToString() == "Pending")
                    {
                        ds.Tables[0].Rows[i]["status"] = hdnPending.Value;
                    }
                    if (Convert.ToString(ds.Tables[0].Rows[i]["status"]).ToString() == "Accepted")
                    {
                        ds.Tables[0].Rows[i]["status"] = hdnAccpeted.Value;
                    }
                    if (Convert.ToString(ds.Tables[0].Rows[i]["status"]).ToString() == "Cancel")
                    {
                        ds.Tables[0].Rows[i]["status"] = hdnCancel.Value;
                    }
                    if (Convert.ToString(ds.Tables[0].Rows[i]["MinAgo"]).ToString() == "Pending")
                    {
                        ds.Tables[0].Rows[i]["MinAgo"] = hdnCancel.Value;
                    }
                    if (Convert.ToString(ds.Tables[0].Rows[i]["MinAgo"]).Contains("Minutes"))
                    {

                        var val = ds.Tables[0].Rows[i]["MinAgo"].ToString().Replace("Minutes", hdnMinute.Value);
                        ds.Tables[0].Rows[i]["MinAgo"] = val;
                    }

                    if (Convert.ToString(ds.Tables[0].Rows[i]["MinAgo"]).Contains("Hours"))
                    {
                        var val = ds.Tables[0].Rows[i]["MinAgo"].ToString().Replace("Hours", hdnHour.Value);
                        //ds.Tables[0].Rows[i]["MinAgo"] = hdnHour.Value;
                        ds.Tables[0].Rows[i]["MinAgo"] = val;
                        // ds.Tables[0].Rows[i]["MinAgo"]=hdnHour.Text;
                    }
                    if (Convert.ToString(ds.Tables[0].Rows[i]["MinAgo"]).Contains("Days"))
                    {
                        var val = ds.Tables[0].Rows[i]["MinAgo"].ToString().Replace("Days", hdnDays.Value);
                        //ds.Tables[0].Rows[i]["MinAgo"] = hdnDays.Value;
                        ds.Tables[0].Rows[i]["MinAgo"] = val;
                    }
                    if (Convert.ToString(ds.Tables[0].Rows[i]["MinAgo"]).Contains("Second"))
                    {
                        var val = ds.Tables[0].Rows[i]["MinAgo"].ToString().Replace("Second", hdnSecond.Value);
                        //ds.Tables[0].Rows[i]["MinAgo"] = hdnSecond.Value;
                        ds.Tables[0].Rows[i]["MinAgo"] = val;
                    }
                }

                RptINVITATION.DataSource = ds.Tables[0];
                RptINVITATION.DataBind();

            }
            else
            {
                RptINVITATION.DataSource = null;
                RptINVITATION.DataBind();
            }
        }
        else
        {
            RptINVITATION.DataSource = null;
            RptINVITATION.DataBind();
        }
    }
    protected void GetHelpRequestedByUserId()
    {
        DataSet ds = null;
        var cacheKey = "GetHelpRequestedByUserId-" + Convert.ToString(Session["OrgUserId"]);
        if (CacheHelper.Exists(""))
        {
            DataSet s;
            CacheHelper.Get(cacheKey, out ds);

        }
        else
        {
            InvitationBM obj = new InvitationBM();
            obj.invIsActive = true;
            obj.invIsDeleted = false;
            obj.invFromUserId = Convert.ToInt32(Session["OrgUserId"]);
            obj.invCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            obj.GetHelpRequestedByUserId();



            ds = obj.ds;

            CacheHelper.Add(ds, cacheKey, CacheHelper.CacheExpireTime());
        }
        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (var i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToString(ds.Tables[0].Rows[i]["status"]).ToString() == "Pending")
                    {
                        ds.Tables[0].Rows[i]["status"] = hdnPending.Value;
                    }
                    if (Convert.ToString(ds.Tables[0].Rows[i]["status"]).ToString() == "Accepted")
                    {
                        ds.Tables[0].Rows[i]["status"] = hdnAccpeted.Value;
                    }
                    if (Convert.ToString(ds.Tables[0].Rows[i]["status"]).ToString() == "Cancel")
                    {
                        ds.Tables[0].Rows[i]["status"] = hdnCancel.Value;
                    }
                    if (Convert.ToString(ds.Tables[0].Rows[i]["MinAgo"]).ToString() == "Pending")
                    {
                        ds.Tables[0].Rows[i]["MinAgo"] = hdnCancel.Value;
                    }
                    if (Convert.ToString(ds.Tables[0].Rows[i]["MinAgo"]).Contains("Minutes"))
                    {

                        var val = ds.Tables[0].Rows[i]["MinAgo"].ToString().Replace("Minutes", hdnMinute.Value);
                        ds.Tables[0].Rows[i]["MinAgo"] = val;
                    }

                    if (Convert.ToString(ds.Tables[0].Rows[i]["MinAgo"]).Contains("Hours"))
                    {
                        var val = ds.Tables[0].Rows[i]["MinAgo"].ToString().Replace("Hours", hdnHour.Value);
                        //ds.Tables[0].Rows[i]["MinAgo"] = hdnHour.Value;
                        ds.Tables[0].Rows[i]["MinAgo"] = val;
                        // ds.Tables[0].Rows[i]["MinAgo"]=hdnHour.Text;
                    }
                    if (Convert.ToString(ds.Tables[0].Rows[i]["MinAgo"]).Contains("Days"))
                    {
                        var val = ds.Tables[0].Rows[i]["MinAgo"].ToString().Replace("Days", hdnDays.Value);
                        //ds.Tables[0].Rows[i]["MinAgo"] = hdnDays.Value;
                        ds.Tables[0].Rows[i]["MinAgo"] = val;
                    }
                    if (Convert.ToString(ds.Tables[0].Rows[i]["MinAgo"]).Contains("Second"))
                    {
                        var val = ds.Tables[0].Rows[i]["MinAgo"].ToString().Replace("Second", hdnSecond.Value);
                        //ds.Tables[0].Rows[i]["MinAgo"] = hdnSecond.Value;
                        ds.Tables[0].Rows[i]["MinAgo"] = val;
                    }
                }
                Rptequeste.DataSource = ds.Tables[0];
                Rptequeste.DataBind();

            }
            else
            {
                Rptequeste.DataSource = null;
                Rptequeste.DataBind();
            }
        }
        else
        {
            Rptequeste.DataSource = null;
            Rptequeste.DataBind();
        }

    }
    #endregion

    #region Button Event
    //protected void TextBox1_TextChanged(object sender, EventArgs e)
    //{
    //    insert_to_do_list();
    //}
    //protected void Check_Clicked(Object sender, EventArgs e)
    //{
    //    string id = "0";
    //    CheckBox chk = (CheckBox)sender;
    //    id = chk.Text;
    //    delete(Convert.ToInt32(id));
    //}
    //protected void LinkButton1_click(object sender, EventArgs e)
    //{
    //    GetTO_DO_LISTbyid();
    //}
    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        //Common.WriteLog("Culture");
        //string language = "French";
        string language = Convert.ToString(Session["Culture"]);
        // Common.WriteLog("language" + language);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    // Common.WriteLog("languagein");
        //    if (language.EndsWith("Denish"))
        //    {
        //        languageId = "da-DK";
        //    }
        //    else
        //    {
        //        // Common.WriteLog("languageelse");
        //        languageId = "en-GB";
        //    }
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion



}