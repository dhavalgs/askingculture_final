﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/ManagerMaster.master"
    AutoEventWireup="true" CodeFile="pending_request.aspx.cs" Inherits="Organisation_pending_request" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>
<%@ Reference Page="~/Organisation/ActivityLists.aspx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .table > thead > tr > th {
            vertical-align: middle;
        }


    </style>

  <%--  <script src="../assets/assets/js/libs/jquery-1.10.2.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/themes/start/jquery-ui.css" />
    <script src="../plugins/bootstrap-inputmask/jquery.inputmask.min.js" type="text/javascript"></script>--%>

    

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
      <asp:HiddenField ID="hdnShow" runat="server" Value="Show" meta:resourcekey="ShowRes"></asp:HiddenField>
    <asp:HiddenField ID="hdnentries" runat="server" Value="entries" meta:resourcekey="entriesRes"></asp:HiddenField>
    <asp:HiddenField ID="hdnSearch" runat="server" Value="Search" meta:resourcekey="SearchRes"></asp:HiddenField>

    <asp:HiddenField ID="hdnShowing" runat="server" Value="Showing" meta:resourcekey="ShowingRes"></asp:HiddenField>
    <asp:HiddenField ID="hdnto" runat="server" Value="to" meta:resourcekey="toRes"></asp:HiddenField>
    <asp:HiddenField ID="hdnof" runat="server" Value="of" meta:resourcekey="ofRes"></asp:HiddenField>
    <asp:HiddenField ID="hdnentrie" runat="server" Value="entries" meta:resourcekey="entrieRes"></asp:HiddenField>

    <asp:HiddenField ID="hdnPrevious" runat="server" Value="of" meta:resourcekey="PreviousRes"></asp:HiddenField>
    <asp:HiddenField ID="hdnNext" runat="server" Value="entries" meta:resourcekey="NextRes"></asp:HiddenField>
    <div class="col-md-6">
        <div class="heading-sec">
            <h1 style="margin-left: 15px;">
                <span runat="server" id="sp1"></span><i><span runat="server" id="Employee"></span>
                </i>
            </h1>
        </div>
    </div>
    <div class="col-md-12" style="margin-top: 20px;">
        <div id="graph-wrapper">
            <div class="col-md-12">
                <div class="chart-tab manager_table">
                    <div id="tabs-container">
                        <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" meta:resourcekey="lblMsgResource1"></asp:Label>
                        
                        <asp:HiddenField ID="hdnPending" runat="server" Value="Pending" meta:resourcekey="PendingResource"></asp:HiddenField>
                        <asp:HiddenField ID="hdnAccept" runat="server" Value="Accept" meta:resourcekey="AcceptResource"></asp:HiddenField>
                        <asp:HiddenField ID="hdnCancel" runat="server" Value="Cancel" meta:resourcekey="CancelResource"></asp:HiddenField>
                       
                         <asp:GridView ID="gvGrid" runat="server" AutoGenerateColumns="False" CellPadding="0"
                            Width="100%" GridLines="None" DataKeyNames="skillUserId" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                            EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>' OnRowCommand="gvGrid_RowCommand"
                            OnRowDataBound="gvGrid_RowDataBound" BackColor="White"
                            meta:resourcekey="gvGridResource1">
                            <HeaderStyle CssClass="aa" />
                            <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:TemplateField HeaderText="Id" meta:resourcekey="TemplateFieldResource1">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Employee Name"
                                    meta:resourcekey="TemplateFieldResource2">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUNamer" runat="server" Text='<%# Eval("name") %>'
                                            meta:resourcekey="lblUNamerResource1"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                </asp:TemplateField>
                                
                                   <asp:TemplateField HeaderText="Request Type" meta:resourcekey="TemplateFieldResource2">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSkillType" runat="server" Text='<%# Eval("SkillType") %>' meta:resourcekey="lblUNamerResource1"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Date" meta:resourcekey="TemplateFieldResource3">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserNamer1" runat="server"
                                            Text='<%# Eval("skillCreatedDate") %>'
                                            meta:resourcekey="lblUserNamer1Resource1"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Status">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnIsSaved" Value='<%#Eval("skillIsSaved")%>' runat="server" ></asp:HiddenField>
                                       <asp:Label ID="lblUserStatus" runat="server" Text='<%# Eval("status") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                </asp:TemplateField>
                                <%--<asp:TemplateField HeaderText="Comment" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblComment" runat="server" Text="<%# bind("skillComment") %>" ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs"  />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                </asp:TemplateField>--%>
                                <asp:TemplateField HeaderText="" meta:resourcekey="TemplateFieldResource4">
                                    <ItemTemplate>
                                       <div class="vat" style="width: 70px;">
                                            <p>
                                                <%--<i class=""></i><a href="<%# String.Format("request.aspx?View={0}", Eval("skillId")) %>"
                                                    title="View"><%= CommonMessages.View%></a>--%>
                                                <i class=""></i>
                                                <a  href="<%# String.Format("{0}", (string) Eval("SkillType")=="Activity Requests"?"ActivityRequestView.aspx?Vw=true&actReqId="+Eval("skillId"):"View_request.aspx?Vw="+Eval("skillUserId")) %>"
                                                    title="View">
                                                    <asp:Literal ID="Literal1" runat="server" meta:resourcekey="View" EnableViewState="false" /></a>
                                            </p>
                                        </div>
                                       <div class="vat" style="width: 100px;" id="Edit" runat="server">
                                            

                                            <p class='<%# Eval("status").ToString() == "Not Saved!" ? "HideDiv" : "DisplayDiv" %>'>
                                                <%--<i class="fa fa-pencil"></i><a href="<%# String.Format("request.aspx?id={0}", Eval("skillId")) %>"
                                                    title="Edit">Edit</a>--%>
                                                <i class="fa fa-pencil"></i><a 
                                                    href="<%# String.Format("{0}", (string) Eval("SkillType")=="Activity Requests"?"ActivityRequestView.aspx?actReqId="+Eval("skillId"):"View_request.aspx?rid="+Eval("skillUserId")) %>"
                                                    title="Edit">
                                                    <asp:Literal ID="Literal2" runat="server" meta:resourcekey="Edit" EnableViewState="false" />

                                                </a>
                                            </p>
                                        </div>
                                         <div class="total" style="width: 70px; display: none;" id="Delete" runat="server">
                                            <p>
                                                <i class="fa fa-trash-o"></i>
                                                <asp:LinkButton ID="lnkBtnName" runat="server" CommandName="archive" CommandArgument='<%# Eval("skillUserId") %>'
                                                    ToolTip="Delete" OnClientClick="return confirm('Are you sure you want to archive this record?');"
                                                    meta:resourcekey="lnkBtnNameResource1">Delete</asp:LinkButton>
                                            </p>
                                        </div>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:Button runat="server" ID="btnBack" Text="Back" CssClass="btn black pull-right"
                            OnClick="btnBack_Click" Style="margin: 4px;" />
                        <%--<a href="#" onclick="javascript:history.back(); return false;" class="btn black pull-right" style="border-radius: 5px;margin-left:0px;margin-top:5px;float:right;">Back</a>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style type="text/css">
        .yellow {
            border-radius: 0px;
        }

        
        .HideDiv {
            display:none;
        }

        .DisplayDiv {
               display:block;
        }
    </style>
   <%-- <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.js"></script>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            SetExpandCollapse();
            setTimeout(function () {
                //ExampanCollapsMenu();
                // tabMenuClick();
                //  $(".OrgJobTypeList").addClass("active_page");
            }, 500);

            //$('.dataTables_length').find('label').html($('.dataTables_length').find('label').html().replace('Show', $('#ContentPlaceHolder1_hdnShow').val()));
            //$('.dataTables_filter').find('label').html($('.dataTables_filter').find('label').html().replace('entries', $('#ContentPlaceHolder1_hdnSearch').val()));
            //$('.dataTables_length').find('label').html($('.dataTables_length').find('label').html().replace('Search', $('#ContentPlaceHolder1_hdnentries').val()));

            //$('.dataTables_info').html($('.dataTables_info').html().replace('Showing', $('#ContentPlaceHolder1_hdnentries').val()));
            //$('.dataTables_info').html($('.dataTables_info').html().replace('to', $('#ContentPlaceHolder1_hdnto').val()));
            //$('.dataTables_info').html($('.dataTables_info').html().replace('of', $('#ContentPlaceHolder1_hdnof').val()));
            //$('.dataTables_info').html($('.dataTables_info').html().replace('entries', $('#ContentPlaceHolder1_hdnentrie').val()));


            //$('.paginate_button.previous').html($('.paginate_button.previous').html().replace('Previous', $('#ContentPlaceHolder1_hdnPrevious').val()));
            //$('.paginate_button.next').html($('.paginate_button.next').html().replace('Next', $('#ContentPlaceHolder1_hdnNext').val()));
        });
    </script>
</asp:Content>
