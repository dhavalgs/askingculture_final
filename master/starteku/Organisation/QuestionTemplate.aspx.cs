﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using starteku_BusinessLogic.Model;
using startetku.Business.Logic;
using System.Threading;
using System.Web.Services;

public partial class Organisation_QuestionTemplate : System.Web.UI.Page
{
    public void SetPublicVaribale()
    {
        CompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        LoggedUserId = Convert.ToInt32(Session["OrgUserId"]);
        OrguserType = Convert.ToInt32(Session["OrguserType"]);

    }

    public static int LoggedUserId { get; set; }
    public static int OrguserType { get; set; }
    public static int ResLangId = 0;
    private static int CompanyId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        ResLangId = Convert.ToInt32(Session["resLangID"]);
        GetAllQuestionTemplateList();
    }
    #region Culture
    protected override void InitializeCulture()
    {
        // string language = "French";
        string language = Convert.ToString(Session["Culture"]);

        string languageId = "";

        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!String.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }

        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion

    #region Method
    protected void OpenCreateQuestionTemplate(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lblQuestionTemp').click();", true);
        ClearData();
    }
    protected void ClearData()
    {
        txttmpName.Text = "";
        txttmpNameDN.Text = "";
        txttmpDescription.Text = "";

    }
    protected void GetAllQuestionTemplateList()
    {
        //ddlQuesCat
        QuestionBM obj = new QuestionBM();
        obj.quesTmpIDs = "-99";
        obj.queIsActive = true;
        obj.queIsDeleted = false;
        obj.queCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.CreatedByID = Convert.ToInt32(Session["OrgUserId"]);

        obj.GetAllQuestionTemplateList();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvGrid_QuestionTemp.DataSource = ds.Tables[0];
                gvGrid_QuestionTemp.DataBind();

                gvGrid_QuestionTemp.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvGrid_QuestionTemp.DataSource = null;
                gvGrid_QuestionTemp.DataBind();
            }
        }
        else
        {
            gvGrid_QuestionTemp.DataSource = null;
            gvGrid_QuestionTemp.DataBind();
        }

    }
    #endregion



    #region Modal Popup
    protected void CreateQuestionTemplate(object sender, EventArgs e)
    {
        CreateQuestionTemplateMethod();
        GetAllQuestionTemplateList();
    }
    protected void CreateQuestionTemplateMethod()
    {
        try
        {
            // check Duplicate
            var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
            var userId = Convert.ToInt32(Session["OrgUserId"]);
            var logginUserType = Convert.ToInt32(Session["OrguserType"]);
            QuestionBM obj = new QuestionBM();
            obj.quesTmpID = 0;
            obj.quesTemplateName = txttmpName.Text;
            obj.quesTemplateNameDN = txttmpNameDN.Text;
            obj.quesDescription = txttmpDescription.Text;
            obj.CreatedByID = userId;
            obj.queCompanyId = companyId;

            obj.queIsActive = Convert.ToBoolean(rdoIsActive.SelectedItem.Value);
            obj.quesIsPublic = Convert.ToBoolean(roIsPublic.SelectedItem.Value);
            obj.quesIsMandatory = Convert.ToBoolean(roIsMandatory.SelectedItem.Value);

            obj.queIsDeleted = false;
            obj.queCreatedDate = DateTime.Now;
            obj.InsertQuestionTemplateData();
            DataSet ds = obj.ds;

        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex);
        }
    }
    #endregion


    #region Grid Method
    protected void gvGrid_QuestionTemp_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hdnIsActive = (HiddenField)e.Row.FindControl("hdnIsActive");
                HiddenField hdnIspublic = (HiddenField)e.Row.FindControl("hdnIspublic");
                HiddenField hdnIsmandatory = (HiddenField)e.Row.FindControl("hdnIsmandatory");
                Label lblIsActive = (Label)e.Row.FindControl("lblactive");
                Label lblpublic = (Label)e.Row.FindControl("lblpublic");
                Label lblmandatory = (Label)e.Row.FindControl("lblmandatory");
                if (Convert.ToBoolean(hdnIsActive.Value) == true)
                {
                    lblIsActive.Text = GetLocalResourceObject("TRUE.Text").ToString();
                }
                else
                {
                    lblIsActive.Text = GetLocalResourceObject("FALSE.Text").ToString();
                }
                if (Convert.ToBoolean(hdnIspublic.Value) == true)
                {
                    lblpublic.Text = GetLocalResourceObject("TRUE.Text").ToString();
                }
                else
                {
                    lblpublic.Text = GetLocalResourceObject("FALSE.Text").ToString();
                }
                if (Convert.ToBoolean(hdnIsmandatory.Value) == true)
                {
                    lblmandatory.Text = GetLocalResourceObject("TRUE.Text").ToString();
                }
                else
                {
                    lblmandatory.Text = GetLocalResourceObject("FALSE.Text").ToString();
                }
            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "gvGrid_QuestionTemp_RowDataBound");
        }
    }

    protected void gvGrid_QuestionTemp_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "archive")
        {
            QuestionBM obj = new QuestionBM();
            obj.quesTmpID = Convert.ToInt32(e.CommandArgument);
            obj.queIsActive = false;
            obj.queIsDeleted = true;
            obj.QuestionTemplateStatusUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    GetAllQuestionTemplateList();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }
    }
    #endregion

}