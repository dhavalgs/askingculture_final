﻿using starteku_BusinessLogic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Globalization;

public partial class Organisation_Point : System.Web.UI.Page
{
    #region Page Event
    DataTable dt = new DataTable();
    int queid = 0;
    DataSet ds;
    public static int ans = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            //Settings.InnerHtml = "Welcome   " + Convert.ToString(Session["OrgUserName"]) + "!";
            Settings.InnerHtml = GetLocalResourceObject("Welcome.Text").ToString() + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            GetUserSettingById();
        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion

    #region Methods
    private void GetUserSettingById()
    {
        try
        {
            PointBM obj = new PointBM();
            obj.GetUserPointAllocation(Convert.ToInt32(Session["OrgCompanyId"]));
            DataSet ds = obj.ds;
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToString(ds.Tables[0].Rows[i]["paPointName"]) == "SignIn")
                    {
                        txtplp.Text = Convert.ToString(ds.Tables[0].Rows[i]["paPoint"]);
                    }
                    else if (Convert.ToString(ds.Tables[0].Rows[i]["paPointName"]) == "RequestSession")
                    {
                        txtRequestSession.Text = Convert.ToString(ds.Tables[0].Rows[i]["paPoint"]);
                    }
                    else if (Convert.ToString(ds.Tables[0].Rows[i]["paPointName"]) == "SuggestSession")
                    {
                        txtSuggestSession.Text = Convert.ToString(ds.Tables[0].Rows[i]["paPoint"]);
                    }
                    else if (Convert.ToString(ds.Tables[0].Rows[i]["paPointName"]) == "UploadPublicDoc")
                    {
                        txtUploadPublicDoc.Text = Convert.ToString(ds.Tables[0].Rows[i]["paPoint"]);
                    }
                    else if (Convert.ToString(ds.Tables[0].Rows[i]["paPointName"]) == "ReadDOC")
                    {
                        txtread.Text = Convert.ToString(ds.Tables[0].Rows[i]["paPoint"]);
                    }
                    else if (Convert.ToString(ds.Tables[0].Rows[i]["paPointName"]) == "ReadDocByOther")
                    {
                        txtSuggestdoc.Text = Convert.ToString(ds.Tables[0].Rows[i]["paPoint"]);
                    }
                    else if (Convert.ToString(ds.Tables[0].Rows[i]["paPointName"]) == "AcceptProvidedHelp")
                    {
                        txtAcceptHelp.Text = Convert.ToString(ds.Tables[0].Rows[i]["paPoint"]);
                    }
                    else if (Convert.ToString(ds.Tables[0].Rows[i]["paPointName"]) == "OpenCompetencesScreen")
                    {
                        txtCompetences.Text = Convert.ToString(ds.Tables[0].Rows[i]["paPoint"]);
                    }
                    else if (Convert.ToString(ds.Tables[0].Rows[i]["paPointName"]) == "FinalFillCompetences")
                    {
                        txtsetCompetences.Text = Convert.ToString(ds.Tables[0].Rows[i]["paPoint"]);
                    }
                    //PDP
                    else if (Convert.ToString(ds.Tables[0].Rows[i]["paPointName"]) == "Level1_2")
                    {
                        Level1_2.Text = Convert.ToString(ds.Tables[0].Rows[i]["paPoint"]);
                    }
                    else if (Convert.ToString(ds.Tables[0].Rows[i]["paPointName"]) == "Level2_3")
                    {
                        Level2_3.Text = Convert.ToString(ds.Tables[0].Rows[i]["paPoint"]);
                    }
                    else if (Convert.ToString(ds.Tables[0].Rows[i]["paPointName"]) == "Level3_4")
                    {
                        Level3_4.Text = Convert.ToString(ds.Tables[0].Rows[i]["paPoint"]);
                    }
                    else if (Convert.ToString(ds.Tables[0].Rows[i]["paPointName"]) == "Level4_5")
                    {
                        Level4_5.Text = Convert.ToString(ds.Tables[0].Rows[i]["paPoint"]);
                    }
                }
            }

        }
        catch (Exception ex)
        {

        }
    }
    #endregion

    #region Button
    protected void btn_save_change_Click(object sender, EventArgs e)
    {
        try
        {
            PointBM obj = new PointBM();
            obj.InsertUpdatePointAllocation(Convert.ToInt32(Session["OrgCompanyId"]), "SignIn", Convert.ToInt32(txtplp.Text),true,false);
            obj.InsertUpdatePointAllocation(Convert.ToInt32(Session["OrgCompanyId"]), "OpenCompetencesScreen", Convert.ToInt32(txtCompetences.Text), true, false);
            obj.InsertUpdatePointAllocation(Convert.ToInt32(Session["OrgCompanyId"]), "FinalFillCompetences", Convert.ToInt32(txtsetCompetences.Text), true, false);
            obj.InsertUpdatePointAllocation(Convert.ToInt32(Session["OrgCompanyId"]), "RequestSession", Convert.ToInt32(txtRequestSession.Text),false,true);
            obj.InsertUpdatePointAllocation(Convert.ToInt32(Session["OrgCompanyId"]), "SuggestSession", Convert.ToInt32(txtSuggestSession.Text),false,true);
            obj.InsertUpdatePointAllocation(Convert.ToInt32(Session["OrgCompanyId"]), "UploadPublicDoc", Convert.ToInt32(txtUploadPublicDoc.Text),true,false);
            obj.InsertUpdatePointAllocation(Convert.ToInt32(Session["OrgCompanyId"]), "ReadDOC", Convert.ToInt32(txtread.Text),true,false);
            obj.InsertUpdatePointAllocation(Convert.ToInt32(Session["OrgCompanyId"]), "ReadDocByOther", Convert.ToInt32(txtSuggestdoc.Text), true, false);
            obj.InsertUpdatePointAllocation(Convert.ToInt32(Session["OrgCompanyId"]), "AcceptProvidedHelp", Convert.ToInt32(txtAcceptHelp.Text), false, true);
            //pdp  
            obj.InsertUpdatePointAllocation(Convert.ToInt32(Session["OrgCompanyId"]), "Level1_2", Convert.ToInt32(Level1_2.Text), false, false);
            obj.InsertUpdatePointAllocation(Convert.ToInt32(Session["OrgCompanyId"]), "Level2_3", Convert.ToInt32(Level2_3.Text), false, false);
            obj.InsertUpdatePointAllocation(Convert.ToInt32(Session["OrgCompanyId"]), "Level3_4", Convert.ToInt32(Level3_4.Text), false, false);
            obj.InsertUpdatePointAllocation(Convert.ToInt32(Session["OrgCompanyId"]), "Level4_5", Convert.ToInt32(Level4_5.Text), false, false);

            Response.Redirect("Organisation-dashboard.aspx");
        }
        catch (Exception ex)
        {

        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if (Convert.ToString(Session["OrguserType"]) == "3")
        {
            Response.Redirect("Employee_dashboard.aspx");
        }
        else if ((Convert.ToString(Session["OrguserType"]) == "1"))
        {
            Response.Redirect("Organisation-dashboard.aspx");
        }
        else if ((Convert.ToString(Session["OrguserType"]) == "2"))
        {
            Response.Redirect("Manager-dashboard.aspx");
        }
    }
    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Denish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion
}