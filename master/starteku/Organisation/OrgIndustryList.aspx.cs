﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;

public partial class Organisation_OrgIndustryList : System.Web.UI.Page
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            Industry.InnerHtml = "Welcome   " + Convert.ToString(Session["OrgUserName"]) + "!";
            PopulateRootLevel();
            IndustrySelectAll();
            IndustryArchiveAll();
            SetDefaultMessage();
        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion

    #region method
    protected void SetDefaultMessage()
    {
        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["msg"])))
        {
            if (Convert.ToString(Request.QueryString["msg"]) == "del")
            {
                lblMsg.Text = CommonModule.msgRecordHasBeenDeletedSuccessfully;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "ins")
            {
                lblMsg.Text = CommonModule.msgRecordInsertedSuccessfully;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "upd")
            {
                lblMsg.Text = CommonModule.msgRecordUpdatedSuccss;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "arc")
            {
                lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
        }

    }
    protected void IndustrySelectAll()
    {
        IndustryBM obj = new IndustryBM();
        obj.indIsActive = true;
        obj.indIsDeleted = false;
        obj.indCompanyId = Convert.ToInt32(Session["OrgUserId"]);
        obj.GetAllIndustry();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvGrid.DataSource = ds.Tables[0];
                gvGrid.DataBind();

                gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvGrid.DataSource = null;
                gvGrid.DataBind();
            }
        }
        else
        {
            gvGrid.DataSource = null;
            gvGrid.DataBind();
        }
    }
    protected void IndustryArchiveAll()
    {
        IndustryBM obj = new IndustryBM();
        obj.indIsActive = false;
        obj.indIsDeleted = false;
        obj.indCompanyId = Convert.ToInt32(Session["OrgUserId"]);
        obj.GetAllIndustry();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvArchive.DataSource = ds.Tables[0];
                gvArchive.DataBind();

                gvArchive.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvArchive.DataSource = null;
                gvArchive.DataBind();
            }
        }
        else
        {
            gvArchive.DataSource = null;
            gvArchive.DataBind();
        }
    }
    #endregion

    #region grid Event
    protected void gvGrid_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "archive")
        {
            IndustryBM obj = new IndustryBM();
            obj.indId = Convert.ToInt32(e.CommandArgument);
            obj.indIsActive = false;
            obj.indIsDeleted = false;
            obj.IndustryStatusUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    IndustrySelectAll();
                    IndustryArchiveAll();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }
    }
    protected void gvArchive_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "archive")
        {
            IndustryBM obj = new IndustryBM();
            obj.indId = Convert.ToInt32(e.CommandArgument);
            obj.indIsActive = true;
            obj.indIsDeleted = false;
            obj.IndustryStatusUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    IndustrySelectAll();
                    IndustryArchiveAll();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }
        else if (e.CommandName == "permanentlydelete")
        {
            IndustryBM obj = new IndustryBM();
            obj.indId = Convert.ToInt32(e.CommandArgument);
            obj.indIsActive = false;
            obj.indIsDeleted = true;
            obj.IndustryStatusUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    IndustrySelectAll();
                    IndustryArchiveAll();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }
    }
    #endregion

    #region TreeView
    private void FillCategoryChecklist(TreeNodeCollection nodes, int id)
    {

        foreach (TreeNode child in nodes)
        {

            if (Convert.ToString(id) == Convert.ToString(child.Value))
            {
                child.Checked = true;

            }
            FillCategoryChecklist(child.ChildNodes, id);
        }


    }
    protected void TreeView_TreeNodePopulate(object sender, TreeNodeEventArgs e)
    {
        int ParentCatagoryID = Int32.Parse(e.Node.Value);
        PopulateSubLevel(ParentCatagoryID, e.Node);
        FillCategoryChecklist(TreeView1.Nodes, Convert.ToInt32(ViewState["catParentId"]));
    }
    private void PopulateSubLevel(int ParentCatagoryID, TreeNode parentNode)
    {
        //Your sublevel Datatable ie. dtSub
        IndustryBM obj = new IndustryBM();
        obj.intParentId = ParentCatagoryID;
        obj.indCompanyId = Convert.ToInt32(Session["OrgUserId"]);
        obj.GetIndustryFromParentId();
        DataSet ds = obj.ds;
        PopulateNodes(ds.Tables[0], parentNode.ChildNodes);


    }
    private void PopulateNodes(DataTable dt, TreeNodeCollection nodes)
    {
        foreach (DataRow dr in dt.Rows)
        {
            TreeNode tn = new TreeNode();
            tn.Value = dr["indId"].ToString();
            tn.Text = dr["indName"].ToString();
            nodes.Add(tn);
            //If node has child nodes, then enable on-demand populating
            tn.PopulateOnDemand = ((int)(dr["catChildCount"]) > 0);
        }
    }
    private void PopulateRootLevel()
    {
        DataTable table = new DataTable();
        DataColumn column = new DataColumn();
        column.DataType = System.Type.GetType("System.Int32");
        column.ColumnName = "indId";
        table.Columns.Add(column);

        // Create second column.
        column = new DataColumn();
        column.DataType = Type.GetType("System.String");
        column.ColumnName = "indName";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.Int32");
        column.ColumnName = "catChildCount";
        table.Columns.Add(column);

        DataRow dr = table.NewRow();
        dr["indId"] = 0;
        dr["indName"] = "Root";
        dr["catChildCount"] = 10;
        table.Rows.Add(dr);
        table.AcceptChanges();


        IndustryBM obj = new IndustryBM();
        obj.intParentId = 0;
        obj.indCompanyId = Convert.ToInt32(Session["OrgUserId"]);
        obj.GetIndustryFromParentId();
        DataSet ds = obj.ds;
        PopulateNodes(table, TreeView1.Nodes);

    }
    private Int32 getCategory(TreeNodeCollection treeNode, int ret)
    {

        foreach (TreeNode child in treeNode)
        {
            if (child.Checked == true)
            {
                ret = Convert.ToInt32(child.Value);
                return ret;
            }
            ret = getCategory(child.ChildNodes, ret);
        }

        return ret;
    }

    protected Int32 getvalue()
    {

        if (TreeView1.CheckedNodes.Count > 0)
        {

            foreach (TreeNode node in TreeView1.CheckedNodes)
            {

                if (node.Parent != null)
                {
                    string checkedValue = node.Text.ToString();
                    Int32 ret = Convert.ToInt32(node.Value);
                    return ret;
                }

            }
            foreach (TreeNode node in TreeView1.Nodes)
            {
                if (node.Checked == true)
                {
                    string checkedValue = node.Text.ToString();
                    Int32 ret = Convert.ToInt32(node.Value);
                    return ret;
                }
            }
            //stablish the session variable only when the foreach has finished
            //Session["listActivity"] = listActivity;
        }
        return 0;
    }



    #endregion

    #region button event

    protected void btnsubmit_click(object sender, EventArgs e)
    {
        IndustryBM obj2 = new IndustryBM();
        obj2.IndustryCheckDuplication(txtIndustryName.Text, -1);
        String returnMsg = obj2.ReturnString;
        if (returnMsg == "")
        {
            IndustryBM obj = new IndustryBM();
            obj.indName = txtIndustryName.Text;
            //if (Convert.ToInt32(ddlIndustry.SelectedValue) == 0)
            //{
            //    obj.intParentId = 0;
            //}
            //else
            //{
            //    obj.intParentId = Convert.ToInt32(ddlIndustry.SelectedValue);
            //}
            obj.indCompanyId = Convert.ToInt32(Session["OrgUserId"]);
            obj.indIsActive = true;
            obj.indIsDeleted = false;
            obj.indCreatedDate = DateTime.Now;
            obj.indDepId = 0;
            obj.InsertIndustry();
            if (obj.ReturnBoolean == true)
            {
                Response.Redirect("OrgIndustryList.aspx?msg=ins");
            }
        }
        else
        {
            if (returnMsg == "depName")
                lblMsg.Text = CommonModule.msgNameAlreadyExists;
            else
                lblMsg.Text = CommonModule.msgProblemInsertRecord;
        }
    }

    #endregion
}