﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Web.Services;
using starteku_BusinessLogic.Model;
using startetku.Business.Logic;
using System.Configuration;
using LogMaster = starteku_BusinessLogic.LogMasterLogic;

public partial class Organisation_CompetenceSkill : System.Web.UI.Page
{
    Boolean checksetbtn = false;

    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        CommonUtilities.RemoveAllJobCache();
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }


        if (!IsPostBack)
        {
            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                updatenotification();
            }

            Skills.InnerHtml = GetLocalResourceObject("Welcome.Text").ToString() + "  " + Convert.ToString(Session["OrgUserName"]) + "!";

            //GetAllCompetencehelp();
            subCompetenceSelectAll();
            InsertUpdateLog();
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>setTimeout(function () {  $('.active').click(); }, 500);</script>", false);
        }
        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>onLoad();</script>", false);// btn.Visible = false;
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion

    #region method
    public void InsertUpdateLog()
    {
        Common.WriteLog("InsertUpdateLog in competence skill");

        //Lets log : current user goin to read doc.
        var userId = Convert.ToInt32(Session["OrgUserId"]);
        string cacheKey = string.Format("{0}-{1}", "OpenCompetence", userId);
        object cacheItem = HttpRuntime.Cache.Get(cacheKey) as string;

        var cookieVal = CookieMasterLogic.DbCookieNew(cacheKey, DateTime.Now.ToString(), DateTime.Now);
        try
        {
            if (!string.IsNullOrWhiteSpace(cookieVal))
            {
                return;
            }
            else
            {
                cacheItem = DateTime.Now.ToString();
                HttpRuntime.Cache.Insert(cacheKey, cacheItem, null, DateTime.Now.AddDays(1), TimeSpan.Zero);
            }
            var allowPoint = false;

            //if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["userDateTime"])))
            //{
            //    var lastlogintime = Convert.ToDateTime(Session["userDateTime"]);
            //    Common.WriteLog("lastlogintime" + lastlogintime);
            //    var timespan = DateTime.Now - lastlogintime;
            //    Common.WriteLog("timespan" + timespan);


            //    if (timespan.TotalHours > 24)
            //    {
            //        Common.WriteLog("allowPoint");
            //        allowPoint = true;
            //    }
            //    else
            //    {
            //        Common.WriteLog("notallowPoint");
            //        allowPoint = false;
            //    }
            //}
            // insert point
            var db = new startetkuEntities1();
            var username = (from user in db.UserMasters where user.userId == userId select new { user.userFirstName, user.userLastName }).FirstOrDefault();
            var point = LogMaster.InsertUpdateLogParam(
                userId,
                Common.PLP(), Common.OpenCompetencesScreen(),
                string.Format("UserID : {0} Open Competences screen", username.userFirstName + " " + username.userLastName), allowPoint, 0, 0);
            Common.WriteLog("point" + point);
            if (allowPoint)
                PointBM.InsertUpdatePoint(userId, point, 0);
            //
        }
        catch (DataException ex)
        {
            Common.WriteLog("error in InsertUpdateLog  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            throw ex;
        }

        try
        {

        }
        catch (DataException ex)
        {
            Common.WriteLog("error in InsertUpdateLog  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }

    protected void CompetenceSelectAll()
    {
        try
        {
            CompetenceMasterBM obj = new CompetenceMasterBM();
            obj.comIsActive = true;
            obj.comIsDeleted = false;
            obj.comCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            obj.comJobtype = Convert.ToInt32(Session["userJobType"]);
            obj.GetAllCompetenceMasterEmpskill();
            DataSet ds = obj.ds;

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlcompetence.Items.Clear();

                    ddlcompetence.DataSource = ds.Tables[0];
                    if (Session["Culture"] == "Danish")
                    {
                        ddlcompetence.DataTextField = "comCompetenceDN";
                    }
                    else
                    {
                        ddlcompetence.DataTextField = "comCompetence";
                    }
                    ddlcompetence.DataValueField = "comId";
                    ddlcompetence.DataBind();

                    //ddlcompetence.Items.Insert(0, new ListItem(CommonModule.dropDownSelectDepartments, CommonModule.dropDownZeroValue));
                }
                else
                {
                    ddlcompetence.Items.Clear();
                    ddlcompetence.Items.Insert(0, new ListItem(CommonModule.dropDownSelectDepartments, CommonModule.dropDownZeroValue));
                }
            }
            else
            {
                ddlcompetence.Items.Clear();
                ddlcompetence.Items.Insert(0, new ListItem(CommonModule.dropDownSelectDepartments, CommonModule.dropDownZeroValue));
            }
        }
        catch (DataException ex)
        {
            Common.WriteLog("error in CompetenceSelectAll" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }
    protected void subCompetenceSelectAll()
    {
        Session["comId"] = "";
        // int val = 0;
        try
        {
            //Common.WriteLog("subCompetenceSelectAllF");
            SubCompetenceBM obj = new SubCompetenceBM();
            obj.subIsActive = true;
            obj.subIsDeleted = false;
            //obj.subComId = Convert.ToInt32(ddlcompetence.SelectedValue);          
            obj.GetAllCompetenceSkills(Convert.ToInt32(Session["OrgUserId"]));
            DataSet ds = obj.ds;
            txtComment.Text = "";
            txtCommentDummy.Text = "";
            ViewState["data"] = ds;
            // Common.WriteLog("ds" + ds.Tables[0].Rows.Count);
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (ds != null)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["CompId"])))
                                {
                                    Session["comId"] = Convert.ToString(ViewState["CompId"]);
                                    CompetencechildSelectAll();
                                    break;
                                }


                                else if (String.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[i]["skillIsApproved"])))
                                {
                                    Session["comId"] = (Convert.ToString(ds.Tables[0].Rows[i]["comId"]));
                                    // txtComment.Text = (Convert.ToString(ds.Tables[0].Rows[i]["skillComment"]));
                                    CompetencechildSelectAll();
                                    //txtComment.Enabled = true;
                                    break;
                                }
                                else
                                {
                                    // txtComment.Enabled = false;
                                }

                                // txtComment.Text = (Convert.ToString(ds.Tables[0].Rows[i]["skillComment"]));
                            }

                        }
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["CompId"])))
                    {
                        Session["comId"] = Convert.ToString(ViewState["CompId"]);
                        CompetencechildSelectAll();
                    }

                    else if (String.IsNullOrEmpty(Convert.ToString(Session["comId"])))
                    {
                        if (!String.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["comId"])))
                        {
                            Session["comId"] = (Convert.ToString(ds.Tables[0].Rows[0]["comId"]));
                            CompetencechildSelectAll();
                            // Common.WriteLog("End");
                        }
                    }

                    //DataView dv = new DataView();
                    //dv = ds.Tables[0].DefaultView;
                    //dv.RowFilter = ("docCompanyId =  '" + Convert.ToInt32(Session["OrgCompanyId"]) + "'");


                    //DataTable dtitm = dv.ToTable();
                    //DataSet ds1 = new DataSet();
                    //ds1.Tables.Add(dtitm);


                    //DataTable dtTop = ds.Tables[0].Rows.Cast<DataRow>().Take(3).CopyToDataTable();  //Take top 3
                    //DataTable dtBottom = ds.Tables[0].Rows.Cast<DataRow>().Skip(ds.Tables[0].Rows.Count - 3).CopyToDataTable(); // Take bottom 3

                    rep_competence.DataSource = ds;
                    rep_competence.DataBind();

                    //rep_competencehigh.DataSource = dtBottom;
                    //rep_competencehigh.DataBind();

                    //rep_tabmenu.DataSource = ds;
                    //rep_tabmenu.DataBind();

                    //rep_content.DataSource = ds;
                    //rep_content.DataBind();
                }
                else
                {
                    rep_competence.DataSource = null;
                    rep_competence.DataBind();
                    //rep_tabmenu.DataSource = null;
                    //rep_tabmenu.DataBind();
                    //rep_content.DataSource = null;
                    //rep_content.DataBind();
                    // rep_competencehigh.DataSource = null;
                    //rep_competencehigh.DataBind();
                }
            }
            else
            {
                rep_competence.DataSource = null;
                rep_competence.DataBind();

                //rep_tabmenu.DataSource = null;
                //rep_tabmenu.DataBind();
                //rep_content.DataSource = null;
                //rep_content.DataBind();
                // rep_competencehigh.DataSource = null;
                // rep_competencehigh.DataBind();
            }



        }
        catch (Exception ex)
        {
            Common.WriteLog("error in subCompetenceSelectAll" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }
    protected void ddlcompetence_SelectedIndexChanged(object sender, EventArgs e)
    {
        // GetState(Convert.ToInt32(ddlcountry.SelectedValue));
        subCompetenceSelectAll();
    }
    protected void insert(Int32 comId, Int32 level, Int32 Achive, Int32 target)
    {
        EmployeeSkillBM obj = new EmployeeSkillBM();
        obj.skillComId = Convert.ToInt32(comId);
        obj.skillStatus = 3;
        obj.skillUserId = Convert.ToInt32(Session["OrgUserId"]);
        obj.skillIsActive = true;
        obj.skillIsDeleted = false;
        obj.skillCreatedDate = DateTime.Now;
        obj.skillCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.skillLocal = level;
        obj.skillAchive = Achive;
        obj.skilltarget = target;
        //obj.skillComment = string.Empty;
        obj.InsertSkillMaster();
        // insertChild(id, level);
        //if (id > 0)
        //{
        //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('Save SussessFully !');</script>", false);
        //}

    }
    //2nd repeater round shape "SET button metho
    protected void insertset(Int32 comId, Int32 level, Int32 Achive, Int32 target, string Comment)
    {
        try
        {
            Common.WriteLog("insertset" + comId + "-" + level + "-" + Achive + "-" + target);
            EmployeeSkillBM obj = new EmployeeSkillBM();
            obj.skillComId = Convert.ToInt32(comId);
            obj.skillStatus = 3;
            obj.skillUserId = Convert.ToInt32(Session["OrgUserId"]);
            obj.skillIsActive = true;
            obj.skillIsDeleted = false;
            obj.skillIsSaved = false;
            obj.skillCreatedDate = DateTime.Now;
            obj.skillCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            obj.skillLocal = 0;
            obj.skillAchive = Achive;
            obj.skilltarget = target;
            obj.skillSet = level;
            obj.skillComment = Comment;

            Common.WriteLog("InsertsetSkillMaster");
            //obj.InsertsetSkillMaster();

            var db = new startetkuEntities1();


            var data = (from o in db.SkillMasters where o.skillComId == obj.skillComId && o.skillUserId == obj.skillUserId && o.skillCompanyId == obj.skillCompanyId && o.skillIsActive == true select o).FirstOrDefault();
            if (data != null)
            {



                /*Check user moved only one step forward and not downward*/

                if (data.skillIsSaved == false)
                {
                    if (data.skillAchive != null && data.skillAchive > 0)
                    {
                        errorDiv.Visible = true;
                        lblErrorMsg.Text = GetLocalResourceObject("Prevoius_competence_is_not_saved.Text").ToString();//@"Your previous competence level is not saved, Please save it.";
                        return;
                    }
                }

                if (data.skillIsApproved == false && level != 0)
                {
                    if (data.skillAchive != null && data.skillAchive > 0)
                    {
                        errorDiv.Visible = true;
                        lblErrorMsg.Text = GetLocalResourceObject("Previous_competence_level_is_not_approved.Text").ToString();//@"Your previous competence level is not approved, Please set after manager approve previous level. ";
                        return;
                    }
                    else
                    {
                        data.skillLocal = level;
                    }

                }

                if (data.skillSet != 0 && data.skillAchive > level)
                {

                    errorDiv.Visible = true;
                    lblErrorMsg.Text = GetLocalResourceObject("No_longer_move_level_downwards.Text").ToString(); //@"You can no longer move level downwards from level " + level + @" to " + data.skillSet;
                    return;
                }

                if (data.skillAchive == level)
                {
                    errorDiv.Visible = true;

                    lblErrorMsg.Text = GetLocalResourceObject("You_are_on_same_level.Text").ToString(); //@"You are on same level, no change will reflact to system.";
                    return;
                }

                if (data.skillSet != 0 && data.skillAchive + 1 != level)
                {
                    if (data.skillAchive != null && data.skillAchive > 0)
                    {
                        errorDiv.Visible = true;
                        lblErrorMsg.Text = GetLocalResourceObject("You_can_only_move_up_one_level.Text").ToString(); //@"You can only move up one level at a time per competence !";
                        return;
                    }
                    else
                    {
                        data.skillLocal = level;
                    }


                }



                errorDiv.Visible = false;
                if (data.skillIsApproved == true)
                {
                    data.skillAchive = level;
                }
                else if (data.skillAchive != null && data.skillAchive > 0)
                {
                    data.skillAchive = level;
                }
                else
                {
                    data.skillLocal = level;
                }
                if (data.skilltarget < level)
                {
                    data.skilltarget = data.skillAchive;
                }
                data.skillIsApproved = false;
                //data.skillComment = Comment;
                data.skillSet = level;
                data.skillComId = Convert.ToInt32(comId);
                data.skillStatus = 3;
                data.skillUserId = Convert.ToInt32(Session["OrgUserId"]);
                data.skillIsActive = true;
                data.skillIsDeleted = false;
                data.skillIsSaved = false;
                data.skillCreatedDate = DateTime.Now;
                data.skillCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
                // data.skilltarget = target;
                db.SaveChanges();

            }
            else
            {
                Common.WriteLog("insertset" + comId + "-" + level + "-" + Achive + "-" + target);
                obj = new EmployeeSkillBM();
                obj.skillComId = Convert.ToInt32(comId);
                obj.skillStatus = 3;
                obj.skillUserId = Convert.ToInt32(Session["OrgUserId"]);
                obj.skillIsActive = true;
                obj.skillIsDeleted = false;
                obj.skillIsSaved = false;
                obj.skillCreatedDate = DateTime.Now;
                obj.skillCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
                //obj.skillLocal = 0;
                obj.skillLocal = level;
                obj.skillAchive = Achive;
                obj.skilltarget = target;
                obj.skillSet = level;
                obj.skillComment = Comment;
                Common.WriteLog("InsertsetSkillMaster");
                obj.InsertsetSkillMaster();
            }



            /* PointBM pm = new PointBM();
             var pointName = "Level1_2";
             switch (level)
             {
                 case 2:
                     pointName = "Level1_2";
                     break;
                 case 3:
                     pointName = "Level2_3";
                     break;
                 case 4:
                     pointName = "Level3_4";
                     break;
                 case 5:
                     pointName = "Level4_5";
                     break;
             }

             var point = LogMaster.GetPointByPointName(pointName, Convert.ToInt32(Session["OrgCompanyId"]));
             var Description = string.Format("{0} got PDP point {1} for moving on level {2}", Convert.ToString(Session["OrgUserName"]), point, level);
             pm.PDPPointMasterInsert(Convert.ToInt32(Session["OrgUserId"]), Convert.ToInt32(comId),
                                     Convert.ToInt32(Session["OrgCompanyId"]), Convert.ToInt32(Session["OrgCreatedBy"]),
                                     Description, Convert.ToString(level), point);*/
            // insertChild(id, level);
            //if (id > 0)
            //{
            //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('Save SuccessFully !');</script>", false);
            //}
        }
        catch (Exception ex)
        {
            Common.WriteLog("error in insertset" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }
    //not in use insertchild method -saurin 
    protected void insertChild(Int32 comId, Int32 level)
    {
        Int32 j = 5;
        EmployeeSkillBM obj = new EmployeeSkillBM();
        obj.skillId = comId;
        obj.skillDoc = string.Empty;
        obj.skillIsActive = true;
        obj.skillIsDeleted = false;
        obj.skillCreatedDate = DateTime.Now;
        obj.skillLevel = level;
        obj.skillPoint = 0;
        obj.InsertSkillChild();

        //if (obj.ReturnBoolean == true)
        //{
        //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('Save SussessFully !');</script>", false);
        //}

    }
    //Not in used
    protected void inserthelp(Int32 subId)
    {
        Competence_HelpBM obj = new Competence_HelpBM();
        obj.helpUserIdHelper = 0;
        obj.helpUserIdHelped = Convert.ToInt32(Session["OrgUserId"]);
        obj.helpkillStatus = 0;
        obj.helpMessage = "";
        obj.comIsActive = true;
        obj.comIsDeleted = false;
        obj.comCreatedDate = DateTime.Now;
        obj.comCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.helpsubComId = subId;
        obj.helpstatus = true;
        obj.InsertCompetence_Help();
        //GetAllCompetencehelp();
        subCompetenceSelectAll();
        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('Save SuccessFully !');</script>", false);
    }
    //Not in used
    protected void GetAllCompetencehelp()
    {
        try
        {
            Competence_HelpBM obj = new Competence_HelpBM();
            obj.comIsActive = true;
            obj.comIsDeleted = false;
            obj.comCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            obj.GetAllCompetencehelp();
            DataSet ds = obj.ds;
            ViewState["help"] = ds;
        }
        catch (Exception ex)
        {
            Common.WriteLog("error in GetAllCompetencehelp" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }
    protected void GetAllpoint()
    {
        try
        {
            PointBM obj = new PointBM();
            obj.PointIsStatus = true;
            obj.PointIsDeleted = false;
            obj.PointCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            obj.GetAllPoint();
            DataSet ds = obj.ds;
            ViewState["point"] = ds;
        }
        catch (Exception ex)
        {
            Common.WriteLog("error in GetAllpoint" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }
    protected void CompetencechildSelectAll()
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["comId"]))) return;
        var comid = Convert.ToInt32(Session["comId"]);
        Session["newcomId"] = comid;
        //ScriptManager.RegisterStartupScript(Page, GetType(), "JsStatus", "setTimeout(function(){tabMenuClick();},0);", true);
        CompetenceMasterBM obj1 = new CompetenceMasterBM();
        obj1.comIsActive = true;
        obj1.comIsDeleted = false;
        obj1.comchildcomID = Convert.ToInt32(comid);
        obj1.comUserId = Convert.ToInt32(Session["OrgUserId"]);
        obj1.comId = comid;
        obj1.GetAllchildCompetenceskill();
        DataSet ds = obj1.ds;
        if (Convert.ToString(Session["Culture"]) == "Danish")
        {
            ds.Tables[0].Columns["comchildlevel"].ColumnName = "abcd";
            ds.Tables[0].Columns["comchildlevelDN"].ColumnName = "comchildlevel";

            ds.Tables[0].Columns["comCompetence"].ColumnName = "abcde";
            ds.Tables[0].Columns["comCompetenceDN"].ColumnName = "comCompetence";

        }
        ViewState["rep_content"] = ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                //txtComment.Text = Convert.ToString(ds.Tables[0].Rows[0]["skillComment"]);
                //txtCommentDummy.Text = Convert.ToString(ds.Tables[0].Rows[0]["skillComment"]);
                rep_tabmenu.DataSource = ds;
                rep_tabmenu.DataBind();
                rep_content.DataSource = ds;
                rep_content.DataBind();
            }
            else
            {
                rep_tabmenu.DataSource = null;
                rep_tabmenu.DataBind();
                rep_content.DataSource = null;
                rep_content.DataBind();
            }
        }
        ScriptManager.RegisterStartupScript(Page, GetType(), "JsStatus", "setTimeout(function(){tabMenuClick();},500);", true);
        //ScriptManager.RegisterStartupScript(Page, GetType(), "JsStatus", "setTimeout(function(){tabMenuClick();},500);", true);
        
        // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), Guid.NewGuid().ToString(), "<script> tabMenuClick();</script>", false);

    }
    protected void Timer1_Tick(object sender, EventArgs e)
    {
        // For tab item
        CompetencechildSelectAll();
        Timer1.Enabled = false;
        // Page.ClientScript.RegisterStartupScript(this.GetType(), "myScript", "SetTabFirstButtonActive();", true);
    }
    protected void InsertNotification()
    {
        NotificationBM obj = new NotificationBM();
        obj.notsubject = "Has Set Competence.";
        obj.notUserId = Convert.ToInt32(Session["OrgUserId"]);
        obj.notIsActive = true;
        obj.notIsDeleted = false;
        obj.notCreatedDate = DateTime.Now;
        obj.notCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.notpage = "View_request.aspx?not=" + Convert.ToString(Session["OrgUserId"]);
        obj.nottype = "Notification";
        obj.notToUserId = 0;
        obj.InsertNotification();
        sendmail(obj.notToUserId);
    }
    protected void updatenotification()
    {
        NotificationBM obj = new NotificationBM();
        obj.notIsActive = false;
        obj.notUpdatedDate = DateTime.Now;
        obj.notUserId = Convert.ToInt32(Request.QueryString["id"]);
        obj.nottype = "UserAcceptCompetence-Notification";
        obj.UpdateNotification();
    }
    protected void sendmail(Int32 touserid)
    {
        #region Send mail
        // String subject = "Set Competence for " + Convert.ToString(Session["OrgUserName"]);
        Template template = CommonModule.getTemplatebyname1("Set Competence", Convert.ToInt32(Session["OrgCreatedBy"]));
        //String confirmMail = CommonModule.getTemplatebyname("Set Competence", touserid);
        String confirmMail = template.TemplateName;
        if (!String.IsNullOrEmpty(confirmMail))
        {
            string subject = GetLocalResourceObject("SetCompetenceSubjectEngResource.Text").ToString();
            if (template.Language == "Danish")
            {
                subject = GetLocalResourceObject("SetCompetenceSubjectDNResource.Text").ToString();
            }

            UserBM Cust = new UserBM();
            Cust.userId = Convert.ToInt32(Session["OrgCreatedBy"]);
            Cust.SelectmanagerByUserId();
            //  Cust.SelectPasswordByUserId();
            DataSet ds = Cust.ds;
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    String name = Convert.ToString(ds.Tables[0].Rows[0]["userFirstName"]);
                    string fromid = Convert.ToString(ConfigurationManager.AppSettings["fromid"]);
                    String empname = Convert.ToString(Session["OrgUserName"]);
                    string tempString = confirmMail;
                    tempString = tempString.Replace("###name###", name);
                    tempString = tempString.Replace("###empname###", empname);
                    CommonModule.SendMailToUser(Convert.ToString(ds.Tables[0].Rows[0]["userEmail"]), subject, tempString, fromid, Convert.ToString(HttpContext.Current.Session["OrgCompanyId"]));

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'> generate('information', '<b>'" + RequestSentSuccessful.Value + "'</b><br> ', 'bottomCenter');</script>", false);
                }
            }
        }

        #endregion
    }
    #endregion

    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        //Int32 ComId = 0;
        //foreach (RepeaterItem item in rep_content.Items)
        //{
        //    //Label lblCom = (Label)item.FindControl("lblComComptence");
        //    //Label lblLeval = (Label)item.FindControl("lblLeval");

        //    var x = ((HiddenField)item.FindControl("hdnComId")).Value;
        //    if (!string.IsNullOrEmpty(x))
        //    {
        //        ComId = Convert.ToInt32(((HiddenField)item.FindControl("hdnComId")).Value);
        //        insert(ComId);
        //        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('Save SuccessFully !');</script>", false);
        //    }
        //}
        //Int32 comId = 0;
        //foreach (RepeaterItem item in rep_competence.Items)
        //{
        //    CheckBox cbInterest = (CheckBox)item.FindControl("checkbox1");
        //    bool isChecked = cbInterest.Checked;
        //    if (isChecked == true)
        //    {
        //        comId = Convert.ToInt32(((HiddenField)item.FindControl("comId")).Value);
        //        // insert(comId);
        //        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('Save SussessFully !');</script>", false);
        //    }

        //}
        //if (comId == 0)
        //{
        //    foreach (RepeaterItem itemhigh in rep_competencehigh.Items)
        //    {
        //        CheckBox cbInterest = (CheckBox)itemhigh.FindControl("checkbox1");
        //        bool isChecked = cbInterest.Checked;
        //        if (isChecked == true)
        //        {
        //            comId = Convert.ToInt32(((HiddenField)itemhigh.FindControl("comId")).Value);
        //            //insert(comId);
        //            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('Save SussessFully !');</script>", false);
        //        }

        //    }
        //}

    }
    protected void btnsetsave_click(object sender, EventArgs e)
    {
        CommonUtilities.RemoveAllJobCache();
        var db = new startetkuEntities1();
        var isPointAllow = true;
        Int32 UserId = Convert.ToInt32(Session["OrgUserId"]);
        List<SkillMaster> sm1 = db.SkillMasters.Where(x => x.skillUserId == UserId && x.skillIsActive == true).ToList();
        var isSet = false;
        if (sm1.Count > 0)
        {
            var data = sm1.Where(x => x.skillIsSaved == false).ToList();
            if (data.Count > 0)
            {
                isSet = true;
            }


        }
        if (hdnSetPoint.Value == "false")
        {

            isPointAllow = false;
        }

        if (isSet == false)
        {
            errorDiv.Visible = true;
            lblErrorMsg.Text = GetLocalResourceObject("Set_competence_level.Text").ToString();
            return;
        }
        else
        {
            int comId = 0;
            int level = 0;
            int Achive = 0;
            int target = 0;
            String set = "";
            String msg = "";
            String comCompetence = "";
            var txtComment = "";
            db.Database.ExecuteSqlCommand("update skillmaster set skillIsSaved='true' where skillUserId='" + UserId + "' and skillIsActive='true'");
            db.SaveChanges();

            //List<int> CompIdList = new List<int>();
            //foreach (RepeaterItem itemhigh1 in rep_competence.Items)
            //{
            //    if (itemhigh1.ItemType == ListItemType.Item || itemhigh1.ItemType == ListItemType.AlternatingItem)
            //    {
            //        //string   levelq =(((Label)itemhigh1.FindControl("lbllocal")).Text);
            //        string levelq = (((Label)itemhigh1.FindControl("lbllocal")).Text);

            //        comCompetence = Convert.ToString(((HiddenField)itemhigh1.FindControl("comCompetence")).Value);
            //        //comId = Convert.ToInt32(((HiddenField)itemhigh1.FindControl("comId")).Value);

            //        comId = Convert.ToInt32(((Label)itemhigh1.FindControl("lblcomId")).Text);
            //        //CompIdList.Add(comId);
            //        SkillMaster sm = db.SkillMasters.Where(x => x.skillComId == comId && x.skillUserId == UserId).FirstOrDefault();
            //        if (sm != null)
            //        {
            //            sm.skillIsSaved = true;
            //            db.SaveChanges();
            //        }
            //        level = Convert.ToInt32(((Label)itemhigh1.FindControl("lbllocal")).Text);
            //        Achive = Convert.ToInt32(((Label)itemhigh1.FindControl("lblAchive")).Text);
            //        target = Convert.ToInt32(((Label)itemhigh1.FindControl("lbltarget")).Text);
            //        set = Convert.ToString(((HiddenField)itemhigh1.FindControl("hdnSet")).Value);
            //        //  txtComment = Convert.ToString(((TextBox)itemhigh1.FindControl("txtComment")).Text);
            //        if (set == "NO")
            //        {
            //            msg = msg + comCompetence;
            //        }
            //    }

            //}
            //if (msg == "")
            //{
            //    foreach (RepeaterItem itemhigh in rep_competence.Items)
            //    {
            //        //comId = Convert.ToInt32(((HiddenField)itemhigh.FindControl("comId")).Value);
            //        comId = Convert.ToInt32(((Label)itemhigh.FindControl("lblcomId")).Text);
            //        level = Convert.ToInt32(((Label)itemhigh.FindControl("lbllocal")).Text);
            //        Achive = Convert.ToInt32(((Label)itemhigh.FindControl("lblAchive")).Text);
            //        target = Convert.ToInt32(((Label)itemhigh.FindControl("lbltarget")).Text);
            //        set = Convert.ToString(((Label)itemhigh.FindControl("lblset")).Text);


            //        //insert(comId, level, Achive, target);
            //    }

            //}
            //else
            //{
            //    // ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('" + msg + " !');</script>", false);
            //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'> generate('information', '<b>Please set your competence !</b><br> ', 'bottomCenter');</script>", false);

            //    //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>modelPopupDilouge('Competence', 'Please set your competence !', true);</script>", false);
            //}

            InsertNotification();
            subCompetenceSelectAll();
            //  getData();
            // subCompetenceSelectAll();

            //Givehelp  point  to  Final of fill the competences score

            LogMaster obj = new LogMaster();

            obj.LogDescription = string.Format("User {0} got point  for final of fill the competences score  user ID : {1} ", Convert.ToString(Session["OrgUserName"]), Convert.ToString(Session["OrgUserId"]));   // User log description..

            var userID = Convert.ToInt32(Session["OrgUserId"]);





            var finalfillcomp = Common.FinalFillCompetences();
            var user =
                (from o in db.LogMasters
                 where o.logUserId == userID && o.logPointInfo == finalfillcomp
                 select o).FirstOrDefault();

            if (user == null)
            {
                var point = LogMaster.InsertUpdateLogParam(userID, Common.PLP(), Common.FinalFillCompetences(),
                    obj.LogDescription, isPointAllow, 0, 0);

                if (isPointAllow)
                {
                    PointBM.InsertUpdatePoint(userID, point, 0);
                    hdnSetPoint.Value = "false";
                }
            }
            errorDiv.Visible = true;
            lblErrorMsg.Text = GetLocalResourceObject("GreatYourCompHasBeenSaved.Text").ToString();//@"Your previous competence level is not approved, Please set after manager approve previous level. ";
            return;
            //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>modelPopupDilouge('Competence', 'Please set your competence !', true);</script>", false);
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert();modelPopupDilouge('Competence', 'Thank you for save competences.', true);</script>", false);
        }

    }
    protected void btnhelp_click(object sender, EventArgs e)
    {
        CommonUtilities.RemoveAllJobCache();
        Button obj = (Button)sender;
        int sid = Convert.ToInt32(obj.CommandArgument);
        inserthelp(sid);
    }
    protected void btmAll_click(object sender, EventArgs e)
    {
        DataSet dtlocal = new DataSet();
        dtlocal = (DataSet)ViewState["point"];
        //DataView dv = new DataView();
        //dv = dtlocal.Tables[0].DefaultView;
        //dv.RowFilter = "userJobType = '" + Convert.ToInt32(Session["userJobType"]) + "'";
        //DataTable dtitm = dv.ToTable();
        //DataSet ds = new DataSet();
        // ds.Tables.Add(dtitm);
        rptlocal.DataSource = dtlocal;
        rptlocal.DataBind();
        mpe.Show();
    }
    protected void btnlocal_click(object sender, EventArgs e)
    {
        try
        {
            DataSet dtlocal = new DataSet();
            dtlocal = (DataSet)ViewState["point"];
            DataView dv = new DataView();
            dv = dtlocal.Tables[0].DefaultView;
            dv.RowFilter = "userJobType = '" + Convert.ToInt32(Session["userJobType"]) + "'";
            DataTable dtitm = dv.ToTable();
            DataSet ds = new DataSet();
            ds.Tables.Add(dtitm);
            rptlocal.DataSource = ds;
            rptlocal.DataBind();
            mpe.Show();
        }
        catch (Exception ex)
        {
        }
    }
    protected void catClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == "ImageClick")
        {
            //e.CommandArgument -->  photoid value
            //Do something
        }
    }
    #endregion

  
    protected void R1_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {

        try
        {
            //if (rep_competence.Items.Count < 1)
            //{
            //    if (e.Item.ItemType == ListItemType.Footer)
            //    {
            //        HtmlTableRow tr = (HtmlTableRow)e.Item.FindControl("Emptyrow");
            //        tr.Visible = true;
            //    }
            //}
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField comId = e.Item.FindControl("comId") as HiddenField;
                Label lbpoint = e.Item.FindControl("lbpoint") as Label;
                Label lbllocal = e.Item.FindControl("lbllocal") as Label;
                Label lblAchive = e.Item.FindControl("lblAchive") as Label;
                Label lbltarget = e.Item.FindControl("lbltarget") as Label;
                Label lblset = e.Item.FindControl("lblset") as Label;
                HiddenField hdnSet = e.Item.FindControl("hdnSet") as HiddenField;
                DataSet ds = (DataSet)ViewState["data"];




                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {

                        DataRow[] row = ds.Tables[0].Select("comId=" + comId.Value);
                        foreach (var i in row)
                        {
                            lbllocal.Text = (Convert.ToString(i["skillLocal"]));
                            if (!String.IsNullOrEmpty((Convert.ToString(i["skillAchive"]))))
                            {
                                lblAchive.Text = (Convert.ToString(i["skillAchive"]));
                            }
                            if (!String.IsNullOrEmpty((Convert.ToString(i["skilltarget"]))))
                            {
                                lbltarget.Text = (Convert.ToString(i["skilltarget"]));
                            }
                            if (!String.IsNullOrEmpty((Convert.ToString(i["skillIsApproved"]))))
                            {
                                //lblset.Text = (Convert.ToString(ds.Tables[0].Rows[i]["skillSet"]));
                                if (Convert.ToBoolean(i["skillIsApproved"]))
                                {
                                    if (lblset != null)
                                    {
                                        lblset.Text = hdnYes.Value;
                                        hdnSet.Value = "Yes";
                                    }
                                }

                            }
                        }
                        //for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        //{



                        //    if (Convert.ToInt32(comId.Value) == Convert.ToInt32(ds.Tables[0].Rows[i]["comId"]))
                        //    {
                        //        if (!String.IsNullOrEmpty((Convert.ToString(ds.Tables[0].Rows[i]["skillLocal"]))))
                        //        {
                        //            lbllocal.Text = (Convert.ToString(ds.Tables[0].Rows[i]["skillLocal"]));
                        //            /*if ((Convert.ToString(ds.Tables[0].Rows[i]["skillLocal"])) == "0")
                        //            {
                        //                if (!String.IsNullOrEmpty((Convert.ToString(ds.Tables[0].Rows[i]["skillSet"]))))
                        //                {
                        //                    lbllocal.Text = (Convert.ToString(ds.Tables[0].Rows[i]["skillSet"]));
                        //                }
                        //            }
                        //            else
                        //            {
                        //                lbllocal.Text = (Convert.ToString(ds.Tables[0].Rows[i]["skillLocal"]));
                        //            }*/
                        //        }

                        //        if (!String.IsNullOrEmpty((Convert.ToString(ds.Tables[0].Rows[i]["skillAchive"]))))
                        //        {
                        //            lblAchive.Text = (Convert.ToString(ds.Tables[0].Rows[i]["skillAchive"]));
                        //        }
                        //        if (!String.IsNullOrEmpty((Convert.ToString(ds.Tables[0].Rows[i]["skilltarget"]))))
                        //        {
                        //            lbltarget.Text = (Convert.ToString(ds.Tables[0].Rows[i]["skilltarget"]));
                        //        }
                        //        /*  if (!String.IsNullOrEmpty((Convert.ToString(ds.Tables[0].Rows[i]["skillSet"]))))*/
                        //        if (!String.IsNullOrEmpty((Convert.ToString(ds.Tables[0].Rows[i]["skillIsApproved"]))))
                        //        {
                        //            //lblset.Text = (Convert.ToString(ds.Tables[0].Rows[i]["skillSet"]));
                        //            if (Convert.ToBoolean(ds.Tables[0].Rows[i]["skillIsApproved"]))
                        //            {
                        //                if (lblset != null)
                        //                {
                        //                    lblset.Text = hdnYes.Value;
                        //                    hdnSet.Value = "Yes";
                        //                }
                        //            }

                        //        }
                        //        //if (!String.IsNullOrEmpty((Convert.ToString(ds.Tables[0].Rows[i]["skillStatus"]))))
                        //        //{
                        //        //    String status = (Convert.ToString(ds.Tables[0].Rows[i]["skillStatus"]));
                        //        //    if (status == "1")
                        //        //    {
                        //        //        if (!String.IsNullOrEmpty((Convert.ToString(ds.Tables[0].Rows[i]["skillStatus"]))))
                        //        //        {
                        //        //            Int32 month = 1;
                        //        //            DateTime skillAcceptDate = (Convert.ToDateTime(ds.Tables[0].Rows[i]["skillAcceptDate"]));
                        //        //            //DateTime validatedate = skillAcceptDate.AddYears(year);
                        //        //            DateTime validatedate = skillAcceptDate.AddMonths(month);
                        //        //            if (validatedate >= DateTime.Now)
                        //        //            {
                        //        //                // checksetbtn = false;
                        //        //            }
                        //        //            else
                        //        //            {
                        //        //                checksetbtn = true;
                        //        //            }
                        //        //        }
                        //        //    }
                        //        //    else
                        //        //    {
                        //        //        checksetbtn = true;
                        //        //    }
                        //        //}
                        //        //else
                        //        //{
                        //        //    checksetbtn = true;
                        //        //}
                        //    }

                        //}
                    }

                    //if (ds.Tables[1].Rows.Count > 0)
                    //{
                    //    for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                    //    {                            //comId
                    //        if (Convert.ToInt32(comId.Value) == Convert.ToInt32(ds.Tables[1].Rows[i]["pintSubCompetance"]))
                    //        {
                    //            lbpoint.Text = (Convert.ToString(ds.Tables[1].Rows[i]["PointAnsCount"]));
                    //        }
                    //    }
                    //}

                }

                if (checksetbtn == false)
                {
                    // btnsetsave.Visible = false;
                }

            }
            //DataSet dsdata = (DataSet)ViewState["data"];
            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    HiddenField hbsubId = e.Item.FindControl("subId") as HiddenField;
            //    Label lbpoint = e.Item.FindControl("lbpoint") as Label;
            //    //Label lblsubcom = e.Item.FindControl("lblsubcom") as Label;
            //    DataView dv = new DataView();
            //    dv = ds1.Tables[0].DefaultView;
            //    dv.RowFilter = "helpUserIdHelped = '" + Convert.ToInt32(Session["OrgUserId"]) + "'";
            //    DataTable dtitm = dv.ToTable();
            //    DataSet ds = new DataSet();
            //    ds.Tables.Add(dtitm);
            //    if (ds != null)
            //    {
            //        if (ds.Tables[0].Rows.Count > 0)
            //        {
            //            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            //            {
            //                if (Convert.ToInt32(hbsubId.Value) == Convert.ToInt32(ds.Tables[0].Rows[i]["helpsubComId"]))
            //                {
            //                    Button btnhelp = e.Item.FindControl("btnhelp") as Button;
            //                    btnhelp.Visible = false;
            //                    //lbpoint.Text = (Convert.ToString(ds.Tables[1].Rows[i]["skillPoint"]));
            //                    //lblsubcom.ForeColor = System.Drawing.Color.Green;
            //                    //((HtmlGenericControl)(e.Item.FindControl("myDiv"))).Attributes["class"] = "Completed_satus";
            //                    //((CheckBox)(e.Item.FindControl("checkbox"))).Visible = false;
            //                }
            //            }
            //        }
            //    }

            //DataView dvdata = new DataView();
            // dvdata = dsdata.Tables[1].DefaultView;
            // dvdata.RowFilter = "PointUserId = '" + Convert.ToInt32(Session["OrgUserId"]) + "'";
            // DataTable dtitmdata = dvdata.ToTable();
            // DataSet dsdatanew = new DataSet();
            // dsdatanew.Tables.Add(dtitmdata);  
            //if (dsdatanew != null)
            //{
            //    if (dsdatanew.Tables[0].Rows.Count > 0)
            //    {
            //        for (int i = 0; i < dsdatanew.Tables[0].Rows.Count; i++)
            //        {
            //            if (Convert.ToInt32(hbsubId.Value) == Convert.ToInt32(dsdatanew.Tables[0].Rows[i]["pintSubCompetance"]))
            //            {
            //                if (!String.IsNullOrEmpty(Convert.ToString(dsdatanew.Tables[0].Rows[i]["PointAnsCount"])))
            //                {
            //                    lbpoint.Text = (Convert.ToString(dsdatanew.Tables[0].Rows[i]["PointAnsCount"]));
            //                }
            //                else
            //                {
            //                    lbpoint.Text = "0";
            //                }

            //            }
            //        }
            //    }
            //}
            //    }
        }
        catch (Exception ex)
        {
            Common.WriteLog("error in R1_ItemDataBound1" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }
    protected void Repeater1_OnItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Local")
        {
            try
            {
                string col1 = ((Label)e.Item.FindControl("lbpoint")).Text;
                Int32 sid = Convert.ToInt32(e.CommandArgument);
                DataSet dtlocal = new DataSet();
                dtlocal = (DataSet)ViewState["point"];
                DataView dv = new DataView();
                dv = dtlocal.Tables[0].DefaultView;
                //dv.RowFilter = "userJobType = '" + Convert.ToInt32(Session["userJobType"]) + "'";
                dv.RowFilter = "userJobType = '" + Convert.ToInt32(Session["userJobType"]) + "' AND PointAnsCount < '" + Convert.ToInt32(col1) + "' AND pintSubCompetance = '" + Convert.ToInt32(sid) + "'";
                DataTable dtitm = dv.ToTable();
                DataSet ds = new DataSet();
                ds.Tables.Add(dtitm);
                //if (Common.IsDatasetvalid(ds))
                //{
                rptlocal.DataSource = ds;
                rptlocal.DataBind();
                mpe.Show();
                //}
                //else
                //{
                //string res = "<script>alert('" + CommonModule.msgRecordNotfound + "');</script>";

                //}
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in Repeater1_OnItemCommand" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            }
        }

        else if (e.CommandName == "All")
        {
            string col1 = ((Label)e.Item.FindControl("lbpoint")).Text;
            Int32 sid = Convert.ToInt32(e.CommandArgument);

            DataSet dtlocal = new DataSet();
            dtlocal = (DataSet)ViewState["point"];
            DataView dv = new DataView();
            dv = dtlocal.Tables[0].DefaultView;
            dv.RowFilter = "PointAnsCount < '" + Convert.ToInt32(col1) + "' AND pintSubCompetance = '" + Convert.ToInt32(sid) + "'";
            DataTable dtitm = dv.ToTable();
            DataSet ds = new DataSet();
            ds.Tables.Add(dtitm);
            //if (Common.IsDatasetvalid(ds))
            //{
            rptlocal.DataSource = ds;
            rptlocal.DataBind();
            mpe.Show();
            //}
            //else
            //{
            //   
            //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script>alert('" + CommonModule.msgRecordNotfound + "');</script>", false);

            //}
        }
        else if (e.CommandName == "catNameClick")
        {
            Session["IsNull"] = "true";
            string col1 = ((Label)e.Item.FindControl("lbpoint")).Text;
            LinkButton lk = (LinkButton)e.Item.FindControl("lnkCatName");
            Int32 sid = Convert.ToInt32(e.CommandArgument);
            Session["comId"] = sid;
            //txtComment.Text=
            Session["newcomId"] = sid;
            CompetenceMasterBM obj = new CompetenceMasterBM();
            obj.comIsActive = true;
            obj.comIsDeleted = false;
            obj.comchildcomID = Convert.ToInt32(sid);
            obj.comUserId = Convert.ToInt32(Session["OrgUserId"]);
            obj.comId = sid;
            obj.GetAllchildCompetenceskill();
            DataSet ds = obj.ds;
            ViewState["rep_content"] = ds;

            DataSet dscontent = (DataSet)ViewState["data"];
            rep_competence.DataSource = dscontent;
            rep_competence.DataBind();
            //ViewState["data"] = ds;
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {



                    // txtComment.Text = Convert.ToString(ds.Tables[0].Rows[0]["skillComment"]);
                    //txtCommentDummy.Text = Convert.ToString(ds.Tables[0].Rows[0]["skillComment"]);



                    /* rep_tabmenu.DataSource = ds;
                     rep_tabmenu.DataBind();
                     rep_content.DataSource = ds;
                     rep_content.DataBind();*/

                }
            }



            //GetcommentById(Convert.ToString(sid));
            //ScriptManager.GetCurrent(Page).RegisterPostBackControl(rep_tabmenu);
            // ScriptManager.GetCurrent(Page).RegisterPostBackControl(rep_content);
            Timer1.Enabled = true;
        }
    }
    protected void rep_competencehigh_OnItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Local")
        {
            try
            {
                string col1 = ((Label)e.Item.FindControl("lbpoint")).Text;
                Int32 sid = Convert.ToInt32(e.CommandArgument);
                DataSet dtlocal = new DataSet();
                dtlocal = (DataSet)ViewState["point"];
                DataView dv = new DataView();
                dv = dtlocal.Tables[0].DefaultView;
                //dv.RowFilter = "userJobType = '" + Convert.ToInt32(Session["userJobType"]) + "'";
                dv.RowFilter = "userJobType = '" + Convert.ToInt32(Session["userJobType"]) + "' AND PointAnsCount > '" + Convert.ToInt32(col1) + "' AND pintSubCompetance = '" + Convert.ToInt32(sid) + "'";
                DataTable dtitm = dv.ToTable();
                DataSet ds = new DataSet();
                ds.Tables.Add(dtitm);
                //if (Common.IsDatasetvalid(ds))
                //{
                rpllocalhigh.DataSource = ds;
                rpllocalhigh.DataBind();
                //mpe.Show();
                ModalPopupExtender1.Show();
                //}


            }
            catch (Exception ex)
            {
                Common.WriteLog("error in Repeater1_OnItemCommand" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            }
        }

        else if (e.CommandName == "All")
        {
            string col1 = ((Label)e.Item.FindControl("lbpoint")).Text;
            Int32 sid = Convert.ToInt32(e.CommandArgument);

            DataSet dtlocal = new DataSet();
            dtlocal = (DataSet)ViewState["point"];
            DataView dv = new DataView();
            dv = dtlocal.Tables[0].DefaultView;
            dv.RowFilter = "PointAnsCount > '" + Convert.ToInt32(col1) + "' AND pintSubCompetance = '" + Convert.ToInt32(sid) + "'";
            DataTable dtitm = dv.ToTable();
            DataSet ds = new DataSet();
            ds.Tables.Add(dtitm);
            //if (Common.IsDatasetvalid(ds))
            //{
            rpllocalhigh.DataSource = ds;
            rpllocalhigh.DataBind();
            ModalPopupExtender1.Show();
            //}

        }
        else if (e.CommandName == "catNameClick")
        {
            string col1 = ((Label)e.Item.FindControl("lbpoint")).Text;
            LinkButton lk = (LinkButton)e.Item.FindControl("lnkCatName");
            Int32 sid = Convert.ToInt32(e.CommandArgument);
            Session["comId"] = sid;
            CompetenceMasterBM obj = new CompetenceMasterBM();
            obj.comIsActive = true;
            obj.comIsDeleted = false;
            obj.comchildcomID = Convert.ToInt32(sid);
            //obj.comId=sid;
            obj.GetAllchildCompetenceskill();
            DataSet ds = obj.ds;
            //ViewState["data"] = ds;
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    //txtComment.Text = Convert.ToString(ds.Tables[0].Rows[0]["skillComment"]);
                    //txtCommentDummy.Text = Convert.ToString(ds.Tables[0].Rows[0]["skillComment"]);
                    rep_tabmenu.DataSource = ds;
                    rep_tabmenu.DataBind();
                    rep_content.DataSource = ds;
                    rep_content.DataBind();

                }
            }




            //ScriptManager.GetCurrent(Page).RegisterPostBackControl(rep_tabmenu);
            // ScriptManager.GetCurrent(Page).RegisterPostBackControl(rep_content);
            Timer1.Enabled = true;
        }
    }
    protected void rep_competencehigh_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (rep_competencehigh.Items.Count < 1)
            {
                if (e.Item.ItemType == ListItemType.Footer)
                {
                    HtmlTableRow tr = (HtmlTableRow)e.Item.FindControl("Emptyrow");
                    tr.Visible = true;
                }
            }
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField comId = e.Item.FindControl("comId") as HiddenField;
                Label lbpoint = e.Item.FindControl("lbpoint") as Label;
                DataSet ds = (DataSet)ViewState["data"];
                if (ds != null)
                {
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                        {                            //comId
                            if (Convert.ToInt32(comId.Value) == Convert.ToInt32(ds.Tables[1].Rows[i]["pintSubCompetance"]))
                            {
                                lbpoint.Text = (Convert.ToString(ds.Tables[1].Rows[i]["PointAnsCount"]));
                            }
                        }
                    }

                }
            }



            //if (rpllocalhigh.Items.Count < 1)
            //{
            //    if (e.Item.ItemType == ListItemType.Footer)
            //    {
            //        HtmlTableRow tr = (HtmlTableRow)e.Item.FindControl("Emptyrow");
            //        tr.Visible = true;

            //    }
            //}
            //DataSet ds1 = (DataSet)ViewState["help"];
            //DataSet dsdata = (DataSet)ViewState["data"];
            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    HiddenField hbsubId = e.Item.FindControl("subId") as HiddenField;
            //    Label lbpoint = e.Item.FindControl("lbpoint") as Label;
            //    //Label lblsubcom = e.Item.FindControl("lblsubcom") as Label;
            //    DataView dv = new DataView();
            //    dv = ds1.Tables[0].DefaultView;
            //    dv.RowFilter = "helpUserIdHelped = '" + Convert.ToInt32(Session["OrgUserId"]) + "'";
            //    DataTable dtitm = dv.ToTable();
            //    DataSet ds = new DataSet();
            //    ds.Tables.Add(dtitm);
            //    if (ds != null)
            //    {
            //        if (ds.Tables[0].Rows.Count > 0)
            //        {
            //            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            //            {
            //                if (Convert.ToInt32(hbsubId.Value) == Convert.ToInt32(ds.Tables[0].Rows[i]["helpsubComId"]))
            //                {
            //                    Button btnhelp = e.Item.FindControl("btnhelp") as Button;
            //                    btnhelp.Visible = false;
            //                    //lbpoint.Text = (Convert.ToString(ds.Tables[1].Rows[i]["skillPoint"]));
            //                    //lblsubcom.ForeColor = System.Drawing.Color.Green;
            //                    //((HtmlGenericControl)(e.Item.FindControl("myDiv"))).Attributes["class"] = "Completed_satus";
            //                    //((CheckBox)(e.Item.FindControl("checkbox"))).Visible = false;
            //                }
            //            }
            //        }
            //    }

            //    DataView dvdata = new DataView();
            //    dvdata = dsdata.Tables[1].DefaultView;
            //    dvdata.RowFilter = "PointUserId = '" + Convert.ToInt32(Session["OrgUserId"]) + "'";
            //    DataTable dtitmdata = dvdata.ToTable();
            //    DataSet dsdatanew = new DataSet();
            //    dsdatanew.Tables.Add(dtitmdata);
            //    if (dsdatanew != null)
            //    {
            //        if (dsdatanew.Tables[0].Rows.Count > 0)
            //        {
            //            for (int i = 0; i < dsdatanew.Tables[0].Rows.Count; i++)
            //            {
            //                if (Convert.ToInt32(hbsubId.Value) == Convert.ToInt32(dsdatanew.Tables[0].Rows[i]["pintSubCompetance"]))
            //                {
            //                    if (!String.IsNullOrEmpty(Convert.ToString(dsdatanew.Tables[0].Rows[i]["PointAnsCount"])))
            //                    {
            //                        lbpoint.Text = (Convert.ToString(dsdatanew.Tables[0].Rows[i]["PointAnsCount"]));
            //                    }
            //                    else
            //                    {
            //                        lbpoint.Text = "0";
            //                    }

            //                }
            //            }
            //        }
            //    }
            //}
        }
        catch (Exception ex)
        {
            Common.WriteLog("error in R1_ItemDataBound" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }
    protected void rptlocal_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (rptlocal.Items.Count < 1)
            {
                if (e.Item.ItemType == ListItemType.Footer)
                {
                    HtmlTableRow tr = (HtmlTableRow)e.Item.FindControl("Emptyrow");
                    tr.Visible = true;

                }
            }
            //DataSet ds = (DataSet)ViewState["point"];
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField userId = e.Item.FindControl("userId") as HiddenField;
                HiddenField pintSubCompetance = e.Item.FindControl("pintSubCompetance") as HiddenField;
                HiddenField hdnhelpstatus = e.Item.FindControl("hdnhelpstatus") as HiddenField;
                if (!String.IsNullOrEmpty(hdnhelpstatus.Value))
                {
                    if (Convert.ToBoolean(hdnhelpstatus.Value) == true)
                    {
                        HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("tdControl");
                        td.Attributes.Add("style", "background-color:Greenyellow;");

                    }
                    else
                    {
                        HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("tdControl");
                        td.Attributes.Add("style", "background-color:Red;");
                    }
                }
                //else
                //{
                //    HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("tdControl");
                //    td.Attributes.Add("style", "background-color:Red;");

                //}
                //DataView dv = new DataView();
                //dv = ds1.Tables[0].DefaultView;
                //dv.RowFilter = "helpUserIdHelped = '" + Convert.ToInt32(Session["OrgUserId"]) + "'";
                //DataTable dtitm = dv.ToTable();
                //DataSet ds = new DataSet();
                //ds.Tables.Add(dtitm);
                //if (ds != null)
                //{
                //    if (ds.Tables[0].Rows.Count > 0)
                //    {
                //        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                //        {
                //            //if (Convert.ToInt32(userId.Value) == Convert.ToInt32(ds.Tables[0].Rows[i]["userId"]))
                //            if ((Convert.ToInt32(userId.Value) == Convert.ToInt32(ds.Tables[0].Rows[i]["userId"]))) //&& (Convert.ToInt32(pintSubCompetance.Value) == Convert.ToInt32(ds.Tables[1].Rows[i]["helpstatus"])))
                //            {
                //                if (ds.Tables[1].Rows.Count > 0)
                //                {
                //                    for (int j = 0; j < ds.Tables[1].Rows.Count; j++)
                //                    {
                //                        if ((Convert.ToInt32(userId.Value) == Convert.ToInt32(ds.Tables[1].Rows[j]["helpuserIdhelped"])) && (true == Convert.ToBoolean(ds.Tables[1].Rows[j]["helpstatus"])))
                //                        {
                //                            HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("tdControl");
                //                            td.Attributes.Add("style", "background-color:Greenyellow;");
                //                        }
                //                        else
                //                        {
                //                            HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("tdControl");
                //                            td.Attributes.Add("style", "background-color:Red;");
                //                        }
                //                    }
                //                }

                //            }
                //        }
                //    }
                //}

            }
        }
        catch (Exception ex)
        {
            Common.WriteLog("error in rptlocal_ItemDataBound" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }
    protected void rptlocal_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "popupcal")
        {
            try
            {
                HiddenField hdnhelpstatus = e.Item.FindControl("hdnhelpstatus") as HiddenField;
                if (!String.IsNullOrEmpty(hdnhelpstatus.Value))
                {
                    if (Convert.ToBoolean(hdnhelpstatus.Value) == true)
                    {
                        HiddenField pintSubCompetance = e.Item.FindControl("pintSubCompetance") as HiddenField;
                        HiddenField userFirstName = e.Item.FindControl("userFirstName") as HiddenField;
                        Int32 userId = Convert.ToInt32(e.CommandArgument);
                        Session["userId"] = Convert.ToInt32(userId);
                        Session["sid"] = pintSubCompetance.Value;
                        Session["chechtype"] = "0";
                        txtcname.Text = userFirstName.Value;
                        mpecal.Show();

                    }
                    else
                    {

                    }
                }
                else
                {
                    HiddenField pintSubCompetance = e.Item.FindControl("pintSubCompetance") as HiddenField;
                    HiddenField userFirstName = e.Item.FindControl("userFirstName") as HiddenField;
                    Int32 userId = Convert.ToInt32(e.CommandArgument);
                    Session["userId"] = Convert.ToInt32(userId);
                    Session["sid"] = pintSubCompetance.Value;
                    txtcname.Text = userFirstName.Value;
                    Session["chechtype"] = "0";
                    mpecal.Show();
                }
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in rptlocal_ItemCommand" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            }
        }
    }
    protected void rpllocalhigh_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (rpllocalhigh.Items.Count < 1)
            {
                if (e.Item.ItemType == ListItemType.Footer)
                {
                    HtmlTableRow tr = (HtmlTableRow)e.Item.FindControl("Emptyrow");
                    tr.Visible = true;
                }
            }

            //DataSet ds = (DataSet)ViewState["point"];
            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    HiddenField userId = e.Item.FindControl("userId") as HiddenField;
            //    HiddenField pintSubCompetance = e.Item.FindControl("pintSubCompetance") as HiddenField;
            //    HiddenField hdnhelpstatus = e.Item.FindControl("hdnhelpstatus") as HiddenField;
            //    if (!String.IsNullOrEmpty(hdnhelpstatus.Value))
            //    {
            //        if (Convert.ToBoolean(hdnhelpstatus.Value) == true)
            //        {
            //            HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("tdControl");
            //            td.Attributes.Add("style", "background-color:Greenyellow;");

            //        }
            //        else
            //        {
            //            HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("tdControl");
            //            td.Attributes.Add("style", "background-color:Red;");
            //        }
            //    }
            //}
        }
        catch (Exception ex)
        {
            Common.WriteLog("error in rptlocal_ItemDataBound" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }
    protected void rpllocalhigh_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "popupcal")
        {
            try
            {
                HiddenField hdnhelpstatus = e.Item.FindControl("hdnhelpstatus") as HiddenField;
                if (!String.IsNullOrEmpty(hdnhelpstatus.Value))
                {
                    if (Convert.ToBoolean(hdnhelpstatus.Value) == true)
                    {
                        HiddenField pintSubCompetance = e.Item.FindControl("pintSubCompetance") as HiddenField;
                        HiddenField userFirstName = e.Item.FindControl("userFirstName") as HiddenField;
                        Int32 userId = Convert.ToInt32(e.CommandArgument);
                        Session["userId"] = Convert.ToInt32(userId);
                        Session["sid"] = pintSubCompetance.Value;
                        Session["chechtype"] = "1";
                        txtcname.Text = userFirstName.Value;
                        mpecal.Show();
                    }
                    else
                    {

                    }
                }
                else
                {
                    HiddenField pintSubCompetance = e.Item.FindControl("pintSubCompetance") as HiddenField;
                    HiddenField userFirstName = e.Item.FindControl("userFirstName") as HiddenField;
                    Int32 userId = Convert.ToInt32(e.CommandArgument);
                    Session["userId"] = Convert.ToInt32(userId);
                    Session["sid"] = pintSubCompetance.Value;
                    Session["chechtype"] = "1";
                    txtcname.Text = userFirstName.Value;
                    mpecal.Show();
                }
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in rptlocal_ItemCommand" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            }
        }
    }
    protected void rep_content_OnItemCommand(object source, RepeaterCommandEventArgs e)
    {
        //Common.WriteLog("rep_content_OnItemCommandnn");
        //Common.WriteLog("CommandName" + e.CommandName);
        if (e.CommandName == "Set")
        {
             CommonUtilities.RemoveAllJobCache();
            hdnSetPoint.Value = "true";

            Session["IsNull"] = "true";
            try
            {
                HiddenField level = e.Item.FindControl("no") as HiddenField;
                // TextBox Comment = e.Item.FindControl("txtComment") as TextBox;
                Int32 comId = Convert.ToInt32(e.CommandArgument);
                Common.WriteLog("comId" + comId);
                Common.WriteLog("level" + Convert.ToInt32(level.Value));
                /*if (Comment == null)
                {
                    Comment.Text = "";
                }*/

                //2nd repeater round shape "SET button metho
                //insertset(comId, Convert.ToInt32(level.Value), 0, 0, "");
                insertset(comId, Convert.ToInt32(level.Value), 0, 0, "");
                DataSet dsComp = (DataSet)ViewState["data"];
                if (dsComp != null)
                {
                    if (dsComp.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < dsComp.Tables[0].Rows.Count; i++)
                        {
                            if ((i + 1) < dsComp.Tables[0].Rows.Count)
                            {
                                if (Convert.ToInt32(dsComp.Tables[0].Rows[i]["comId"]) == comId)
                                {
                                    ViewState["CompId"] = Convert.ToString(dsComp.Tables[0].Rows[i + 1]["comId"]);
                                }
                            }
                        }
                    }
                }

                txtComment.Text = "";
                txtCommentDummy.Text = "";
                subCompetenceSelectAll();
                // CompetencechildSelectAll();
                //   Common.WriteLog("end");
                //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('Save SussessFully !');</script>", false);
                // subCompetenceSelectAll();
                //getData();
                // Common.WriteLog("end1");
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in rep_content_OnItemCommand" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            }
        }

    }
    protected void rep_content_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        Common.WriteLog("rep_content_ItemDataBound");
        try
        {

            //if (rep_competence.Items.Count < 1)
            //{
            //    if (e.Item.ItemType == ListItemType.Footer)
            //    {
            //        HtmlTableRow tr = (HtmlTableRow)e.Item.FindControl("Emptyrow");
            //        tr.Visible = true;
            //    }
            //}

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField comId = e.Item.FindControl("comId") as HiddenField;
                HiddenField comchildId = e.Item.FindControl("comchildId") as HiddenField;
                Button btnset = e.Item.FindControl("btnset") as Button;

                HiddenField no = e.Item.FindControl("no") as HiddenField;
                DataSet ds = (DataSet)ViewState["rep_content"];
                HtmlGenericControl tab1 = (HtmlGenericControl)e.Item.FindControl("tab1");

                #region
                ///////////////////  Updated on 24/6/2016  ----- Sp

                //if (ds != null)
                //{
                //    if (ds.Tables[0].Rows.Count > 0)
                //    {

                //        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                //        {

                //            if (Convert.ToInt32(comchildId.Value) == Convert.ToInt32(ds.Tables[0].Rows[i]["comchildId"]))
                //            {

                //                if (!String.IsNullOrEmpty((Convert.ToString(ds.Tables[0].Rows[i]["skillStatus"]))))
                //                {

                //                    String status = (Convert.ToString(ds.Tables[0].Rows[i]["skillStatus"]));
                //                    if (status == "1")
                //                    {
                //                        if (!String.IsNullOrEmpty((Convert.ToString(ds.Tables[0].Rows[i]["skillStatus"]))))
                //                        {

                //                            Int32 month = 1;
                //                            DateTime skillAcceptDate = (Convert.ToDateTime(ds.Tables[0].Rows[i]["skillAcceptDate"]));
                //                            DateTime validatedate = skillAcceptDate.AddMonths(month);
                //                            if (validatedate >= DateTime.Now)
                //                            {
                //                                //btnset.Visible = false; // Set button visible  karavyu.
                //                            }
                //                            else
                //                            {
                //                                //checksetbtn = true;
                //                            }
                //                        }

                //                    }
                //                }
                //            }

                //            //if (!String.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[i]["no"])))
                //            //{
                //            //    if (Convert.ToInt32(no.Value) == Convert.ToInt32(ds.Tables[0].Rows[i]["skillLocal"]))
                //            //    {

                //            //       tab1.Attributes.Add("style", "display:block;");
                //            //        // loginLoader.Attributes.Add("style", "display:block");
                //            //    }
                //            //}


                //        }
                //    }
                //}
                #endregion
            }

        }
        catch (Exception ex)
        {
            Common.WriteLog("error in rep_content_ItemDataBound" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }
    protected void rep_tabmenu_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HtmlAnchor nameAnchor = (HtmlAnchor)e.Item.FindControl("name");
                HiddenField no = e.Item.FindControl("no") as HiddenField;
                //nameAnchor.Attributes.Add("class", "sprite id" + (e.Item.ItemIndex + 1));

                DataSet ds = (DataSet)ViewState["rep_content"];
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            if (!String.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[i]["no"])))
                            {
                                //if (Convert.ToInt32(no.Value) == Convert.ToInt32(ds.Tables[0].Rows[i]["skillLocal"]))
                                //{
                                //        nameAnchor.Attributes.Add("class", "active");

                                //}
                                if (!String.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[i]["skillLocal"])))
                                {
                                    if (Convert.ToInt32(no.Value) == Convert.ToInt32(ds.Tables[0].Rows[i]["skillSet"]) || Convert.ToInt32(no.Value) == 1)
                                    {
                                        nameAnchor.Attributes.Add("class", "active");
                                        //ScriptManager.RegisterStartupScript(Page, GetType(), "JsStatus", "setTimeout(function(){tabMenuClick();},1500);", true);
                                        ScriptManager.RegisterStartupScript(Page, GetType(), "JsStatus", "setTimeout(function(){tabMenuClick();},10);", true);
                                    }
                                    else if (Convert.ToInt32(no.Value) == Convert.ToInt32(ds.Tables[0].Rows[i]["skillLocal"]))
                                    {
                                        nameAnchor.Attributes.Add("class", "active");
                                        //ScriptManager.RegisterStartupScript(Page, GetType(), "JsStatus", "setTimeout(function(){tabMenuClick();},1500);", true);
                                        ScriptManager.RegisterStartupScript(Page, GetType(), "JsStatus", "setTimeout(function(){tabMenuClick();},10);", true);
                                        //$(".active").click()
                                        // ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>setTimeout(function () {  $('.active').click(); }, 500);</script>", false);
                                    }
                                }
                                else
                                {
                                    //ScriptManager.RegisterStartupScript(Page, GetType(), "JsStatus", "SetTabFirstButtonActive() ;setTimeout(function(){tabMenuClick();},1500);", true);
                                    ScriptManager.RegisterStartupScript(Page, GetType(), "JsStatus", "SetTabFirstButtonActive() ;setTimeout(function(){tabMenuClick();},10);", true);
                                }

                            }
                        }

                    }
                }



            }

        }
        catch (Exception ex)
        {
            Common.WriteLog("error in R1_ItemDataBound" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }
 

    #region WebMethod
    [WebMethod(EnableSession = true)]
    public static comment[] GetcommentById(string comId)
    {

        DataTable dt = new DataTable();
        List<comment> cmnt = new List<comment>();

        if (string.IsNullOrWhiteSpace(comId))
        {
            return cmnt.ToArray();
        }
        CompetenceBM obj = new CompetenceBM();

        Int32 comuserid = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        obj.GetCompetenceCommentById(Convert.ToInt32(comId), comuserid);
        DataSet ds = obj.ds;




        if (ds.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow dtrow in ds.Tables[0].Rows)
            {
                comment comenmt = new comment();
                comenmt.compId = dtrow["compId"].ToString();
                comenmt.name = dtrow["name"].ToString();
                comenmt.Image = dtrow["userImage"].ToString();
                comenmt.comuserid = dtrow["comuserid"].ToString();
                comenmt.compcomment = dtrow["compcomment"].ToString();
                comenmt.comcreatedate = dtrow["comcreatedate"].ToString();

                cmnt.Add(comenmt);
            }
        }
        return cmnt.ToArray();
    }

    [WebMethod(EnableSession = true)]
    public static string insertmessage(string message, string comid)
    {
        CommonUtilities.RemoveAllJobCache();
        string msg = string.Empty;


        CompetenceBM obj = new CompetenceBM();
        Int32 compid = Convert.ToInt32(comid);
        Int32 userid = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        Int32 touserid = userid;

        if (obj.InsertCompetenceComment(compid, userid, message, touserid))
        {
            msg = "true";
        }
        else
        {
            msg = "false";
        }


        return msg;

    }




    public class comment
    {
        public string compId { get; set; }

        public string name { get; set; }
        public string comuserid { get; set; }
        public string Image { get; set; }
        public string compcomment { get; set; }
        public string comcreatedate { get; set; }

    }
    [WebMethod(EnableSession = true)]
    public static string UpdateData(string txtto, string txtsubject, string txtmessge)
    {
        CommonUtilities.RemoveAllJobCache();
        string msg = string.Empty;

        PointBM objAtt = new PointBM();
        objAtt.PointuserIDhelper = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        objAtt.PointCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
        objAtt.PointDate = Convert.ToDateTime(CommonModule.FormatdtEnter(txtsubject));
        objAtt.Pointmessage = txtmessge;
        objAtt.PointuserIDhelped = Convert.ToInt32(HttpContext.Current.Session["userId"]);
        objAtt.PointComId = Convert.ToInt32(HttpContext.Current.Session["sid"]);
        string checktype = Convert.ToString(HttpContext.Current.Session["chechtype"]);
        objAtt.UpdatePoint(checktype);
        if (!objAtt.UpdatePoint(checktype))
        {
            msg = "false";

        }
        else
        {
            msg = "true";
            HttpContext.Current.Session["userId"] = "";
            HttpContext.Current.Session["sid"] = "";
        }

        return msg;
    }
    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        //string language = "French";
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Danish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion

    protected void btnResposeRedirect_OnClick(object sender, EventArgs e)
    {
        Response.Redirect("myCompetence.aspx");
    }
}