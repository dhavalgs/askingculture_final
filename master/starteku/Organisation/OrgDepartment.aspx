﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true" CodeFile="OrgDepartment.aspx.cs" Inherits="Organisation_OrgDepartment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .treeNode
        {
        }
        .treeNode input
        {
            width: auto;
            margin: 5px;
            float: left !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg"></asp:Label>
    <div class="col-md-6">
        <div class="heading-sec">
            <%--<h1>
                <asp:Label runat="server" ID="lblDataDisplayTitle" Text="Department"></asp:Label>
                <i>Welcome to Flat Lab </i></h1>--%>
            <h1>
                Department <i>
                    <span runat="server" id="Department"></span></i></h1>
        </div>
    </div>
    <br />
    <br />
    <div class="col-md-12">
        <div class="chat-widget widget-body" style="background: #fff;">
            <div class="chat-widget-head yellow">
                <h4>
                    Add Department
                </h4>
            </div>
            <div class="indicatesRequireFiled">
                <i>* Indicates required field</i>
            </div>
            <div class="col-md-6" style="width: 100%;">
                <div class="inline-form">
                    <label class="c-label">
                        Department:</label>
                    <asp:DropDownList ID="ddlDepartment" runat="server" Style="width: 100px; height: 32px;
                        display: inline;" Visible="false">
                    </asp:DropDownList>
                    <br />
                    <br />
                    <asp:TreeView ID="TreeView1" runat="server" ShowCheckBoxes="All" onclick="client_OnTreeNodeChecked(event)"
                        OnTreeNodePopulate="TreeView_TreeNodePopulate" NodeStyle-CssClass="treeNode"
                        RootNodeStyle-CssClass="rootNode" LeafNodeStyle-CssClass="leafNode" Style="cursor: pointer"
                        ShowLines="True" />
                </div>
            </div>
            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label">
                        Department Name:</label>
                    <asp:TextBox runat="server" Text="" ID="txtDepartmentName" MaxLength="50" CssClass="form-control" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtDepartmentName"
                        ErrorMessage="Please enter Department name." CssClass="commonerrormsg" Display="Dynamic"
                        ValidationGroup="chk"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="col-xs-12 profile_bottom">
                <asp:Button runat="server" ID="btnsubmit" Text="Save" CssClass="btn blue pull-right"
                    ValidationGroup="chk" OnClick="btnsubmit_click" />
                <asp:Button runat="server" ID="btnCancel" Text="Cancel" CssClass="btn black pull-right"
                    ValidationGroup="chk" CausesValidation="false" OnClick="btnCancel_click" />
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function client_OnTreeNodeChecked(event) {

            var treeNode = event.srcElement || event.target;
            if (treeNode.tagName == "INPUT" && treeNode.type == "checkbox") {
                if (treeNode.checked) {
                    uncheckOthers(treeNode.id);
                }
            }
        }

        function uncheckOthers(id) {
            var elements = document.getElementsByTagName('input');
            // loop through all input elements in form
            for (var i = 0; i < elements.length; i++) {
                if (elements.item(i).type == "checkbox") {
                    if (elements.item(i).id != id) {
                        elements.item(i).checked = false;
                    }
                }
            }
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            "use strict";

            App.init(); // Init layout and core plugins
            Plugins.init(); // Init all plugins
            FormComponents.init(); // Init all form-specific plugins
        });
    </script>
</asp:Content>
