﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using starteku_BusinessLogic.Model;
using startetku.Business.Logic;
using System.Threading;
using System.Web.Services;
using System.Web.UI.HtmlControls;
using App_code;

public partial class Organisation_Question : System.Web.UI.Page
{
    public void SetPublicVaribale()
    {
        CompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        LoggedUserId = Convert.ToInt32(Session["OrgUserId"]);
        OrguserType = Convert.ToInt32(Session["OrguserType"]);

    }

    public static int LoggedUserId { get; set; }
    public static int OrguserType { get; set; }
    public static int ResLangId = 0;
    //public int ResLangId = 0;
    private static int CompanyId = 0;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        ResLangId = Convert.ToInt32(Session["resLangID"]);
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            GetAllQuestionCategory();
            GetAllQuestionList();
            //  GetAssignQuestionTemplateList();
            GetAssignTeamList();
            GetAllQuestionTemplateList();

            GetAllAssignQuestionList();

            GetPublishQuesUserList();

            if (!String.IsNullOrEmpty(Request.QueryString["notif"]))
            {
                UpdateNotification();
            }

            Session["TempIDs"] = "0";
            Session["TeamIDs"] = "0";
            Session["TempQuesIDs"] = "0";

            List<TemplateQues> temp = new List<TemplateQues>();
            TemplateQues t = new TemplateQues();
            t.TemplateID = 0;
            t.QuesIDs = new List<QuesIDs>();
            temp.Add(t);
            Session["ListTempQues"] = temp;


            List<TeamUser> tempuser = new List<TeamUser>();
            TeamUser tt = new TeamUser();
            tt.TeamID = 0;
            tt.UserIDs = new List<UserIDs>();
            tempuser.Add(tt);
            Session["ListTeamUser"] = tempuser;
        }
    }



    protected void GetAllQuestionList()
    {
        var logginUserType = Convert.ToInt32(Session["OrguserType"]);
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(Session["OrgUserId"]);

        QuestionBM obj = new QuestionBM();
        obj.queId = 0;
        obj.UserId = userId;
        obj.queCompanyId = companyId;
        obj.CreatedByID = userId;
        obj.queIsActive = true;
        obj.queIsDeleted = false;
        if (!String.IsNullOrEmpty(ddlFilterTemplate.SelectedValue))
        {
            obj.quesTmpID = Convert.ToInt32(ddlFilterTemplate.SelectedValue);
        }
        else
        {
            obj.quesTmpID = 0;
        }
        if (!String.IsNullOrEmpty(ddlFilterCategory.SelectedValue))
        {
            obj.quesCatID = Convert.ToInt32(ddlFilterCategory.SelectedValue);
        }
        else
        {
            obj.quesCatID = 0;
        }
        if (!String.IsNullOrEmpty(txtSearchStartDate.Text))
        {
            obj.StartDate = CommonUtility.ConvertStringToDateTimewithformay(txtSearchStartDate.Text);
        }
        else
        {
            obj.StartDate = null;
        }
        if (!String.IsNullOrEmpty(txtSearchEndDate.Text))
        {
            obj.EndDate = CommonUtility.ConvertStringToDateTimewithformay(txtSearchEndDate.Text);
        }
        else
        {
            obj.EndDate = null;
        }


        obj.GetAllQuestionList();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvGrid_Question.DataSource = ds.Tables[0];
                gvGrid_Question.DataBind();

                gvGrid_Question.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvGrid_Question.DataSource = null;
                gvGrid_Question.DataBind();
            }
        }
        else
        {
            gvGrid_Question.DataSource = null;
            gvGrid_Question.DataBind();
        }

    }
    protected DataSet GetQuestionByID(int ID)
    {
        var logginUserType = Convert.ToInt32(Session["OrguserType"]);
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(Session["OrgUserId"]);

        QuestionBM obj = new QuestionBM();
        obj.queId = ID;
        obj.UserId = userId;
        obj.queCompanyId = companyId;
        obj.CreatedByID = userId;
        obj.queIsActive = true;
        obj.queIsDeleted = false;
        obj.quesCatID = 0;
        obj.quesTmpID = 0;
        obj.StartDate = null;
        obj.EndDate = null;
        obj.GetAllQuestionList();
        DataSet ds = obj.ds;
        return ds;
    }
    protected void GetAllQuestionCategory()
    {
        //ddlQuesCat
        QuestionBM obj = new QuestionBM();
        obj.queIsActive = true;
        obj.queIsDeleted = false;
        obj.quesCatID = 0;

        obj.GetAllQuestionCategoryList();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlQuesCat.Items.Clear();
                ddlFilterCategory.Items.Clear();


                ddlQuesCat.DataSource = ds.Tables[0];
                if (Convert.ToString(Session["Culture"]) == "Danish")
                {
                    ddlQuesCat.DataTextField = "qcatNameDN";
                }
                else
                {
                    ddlQuesCat.DataTextField = "qcatName";
                }
                ddlQuesCat.DataValueField = "qcatID";
                ddlQuesCat.DataBind();

                ddlQuesCat.Items.Insert(0, new ListItem(hdnSelectQuesCat.Value, CommonModule.dropDownZeroValue));


                ddlFilterCategory.DataSource = ds.Tables[0];
                if (Convert.ToString(Session["Culture"]) == "Danish")
                {
                    ddlFilterCategory.DataTextField = "qcatNameDN";
                }
                else
                {
                    ddlFilterCategory.DataTextField = "qcatName";
                }
                ddlFilterCategory.DataValueField = "qcatID";
                ddlFilterCategory.DataBind();

                ddlFilterCategory.Items.Insert(0, new ListItem(hdnAll.Value, CommonModule.dropDownZeroValue));
            }
            else
            {
                ddlQuesCat.Items.Clear();
                ddlQuesCat.Items.Insert(0, new ListItem(hdnSelectQuesCat.Value, CommonModule.dropDownZeroValue));

                ddlFilterCategory.Items.Clear();
                ddlFilterCategory.Items.Insert(0, new ListItem(hdnAll.Value, CommonModule.dropDownZeroValue));
            }
        }
        else
        {
            ddlQuesCat.Items.Clear();
            ddlQuesCat.Items.Insert(0, new ListItem(hdnSelectQuesCat.Value, CommonModule.dropDownZeroValue));

            ddlFilterCategory.Items.Clear();
            ddlFilterCategory.Items.Insert(0, new ListItem(hdnSelectQuesCat.Value, CommonModule.dropDownZeroValue));
        }

    }
    protected void GetAllQuestionTemplateList()
    {
        //ddlQuesCat
        //QuestionBM obj = new QuestionBM();
        //obj.quesTmpIDs = "0";
        //obj.queIsActive = true;
        //obj.queIsDeleted = false;

        //obj.GetAllQuestionTemplateList();
        //DataSet ds = obj.ds;

        DataSet ds = GetTemplateByIDs("0");

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddtTemplate.Items.Clear();
                ddlFilterTemplate.Items.Clear();
                ddlAddTemplateQues.Items.Clear();

                ddtTemplate.DataSource = ds.Tables[0];
                if (Convert.ToString(Session["Culture"]) == "Danish")
                {
                    ddtTemplate.DataTextField = "tmpNameDN";
                }
                else
                {
                    ddtTemplate.DataTextField = "tmpName";
                }
                ddtTemplate.DataValueField = "qtmpID";
                ddtTemplate.DataBind();


                ddlAddTemplateQues.DataSource = ds.Tables[0];
                if (Convert.ToString(Session["Culture"]) == "Danish")
                {
                    ddlAddTemplateQues.DataTextField = "tmpNameDN";
                }
                else
                {
                    ddlAddTemplateQues.DataTextField = "tmpName";
                }
                ddlAddTemplateQues.DataValueField = "qtmpID";
                ddlAddTemplateQues.DataBind();
                ddlAddTemplateQues.Items.Insert(0, new ListItem(hdnSelectQuesTemplate.Value, CommonModule.dropDownZeroValue));


                ddlFilterTemplate.DataSource = ds.Tables[0];
                if (Convert.ToString(Session["Culture"]) == "Danish")
                {
                    ddlFilterTemplate.DataTextField = "tmpNameDN";
                }
                else
                {
                    ddlFilterTemplate.DataTextField = "tmpName";
                }
                ddlFilterTemplate.DataValueField = "qtmpID";
                ddlFilterTemplate.DataBind();
                ddlFilterTemplate.Items.Insert(0, new ListItem(hdnAll.Value, CommonModule.dropDownZeroValue));
            }
            else
            {
                ddtTemplate.Items.Clear();
                //  ddtTemplate.Items.Insert(0, new ListItem(hdnSelectQuesTemplate.Value, CommonModule.dropDownZeroValue));


                ddlAddTemplateQues.Items.Clear();
                ddlAddTemplateQues.Items.Insert(0, new ListItem(hdnSelectQuesTemplate.Value, CommonModule.dropDownZeroValue));

                ddlFilterTemplate.Items.Clear();
                ddlFilterTemplate.Items.Insert(0, new ListItem(hdnAll.Value, CommonModule.dropDownZeroValue));
            }
        }
        else
        {
            ddtTemplate.Items.Clear();
            // ddtTemplate.Items.Insert(0, new ListItem(hdnSelectQuesTemplate.Value, CommonModule.dropDownZeroValue));

            ddlAddTemplateQues.Items.Clear();
            ddlAddTemplateQues.Items.Insert(0, new ListItem(hdnSelectQuesTemplate.Value, CommonModule.dropDownZeroValue));

            ddlFilterTemplate.Items.Clear();
            ddlFilterTemplate.Items.Insert(0, new ListItem(hdnAll.Value, CommonModule.dropDownZeroValue));
        }

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlAssignQuesTemplate.Items.Clear();

                ddlAssignQuesTemplate.DataSource = ds.Tables[0];
                if (Convert.ToString(Session["Culture"]) == "Danish")
                {
                    ddlAssignQuesTemplate.DataTextField = "tmpNameDN";
                }
                else
                {
                    ddlAssignQuesTemplate.DataTextField = "tmpName";
                }
                ddlAssignQuesTemplate.DataValueField = "qtmpID";
                ddlAssignQuesTemplate.DataBind();
                ddlAssignQuesTemplate.Items.Insert(0, new ListItem(hdnSelectQuesTemplate1.Value, "-1"));
            }
            else
            {
                ddlAssignQuesTemplate.Items.Clear();
                ddlAssignQuesTemplate.Items.Insert(0, new ListItem(hdnSelectQuesTemplate1.Value, "-1"));
            }
        }
        else
        {
            ddlAssignQuesTemplate.Items.Clear();
            ddlAssignQuesTemplate.Items.Insert(0, new ListItem(hdnSelectQuesTemplate1.Value, "-1"));
        }

    }

    protected void GetAllAssignQuestionList()
    {
        try
        {
            DataSet ds = GetAssignQuestionList();

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    grid_QuestionAssign.DataSource = ds.Tables[0];
                    grid_QuestionAssign.DataBind();

                    gridPublishQues.DataSource = ds.Tables[0];
                    gridPublishQues.DataBind();

                    grid_QuestionAssign.HeaderRow.TableSection = TableRowSection.TableHeader;

                    gridPublishQues.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                else
                {
                    grid_QuestionAssign.DataSource = null;
                    grid_QuestionAssign.DataBind();

                    gridPublishQues.DataSource = null;
                    gridPublishQues.DataBind();
                }
            }
            else
            {
                grid_QuestionAssign.DataSource = null;
                grid_QuestionAssign.DataBind();

                gridPublishQues.DataSource = null;
                gridPublishQues.DataBind();
            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "GetAllAssignQuestionList");
        }
    }

    protected void GetPublishQuesUserList()
    {
        var logginUserType = Convert.ToInt32(Session["OrguserType"]);
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(Session["OrgUserId"]);

        QuestionBM obj = new QuestionBM();
        obj.UserId = userId;
        obj.queCompanyId = companyId;

        obj.GetPublishQuesUserList();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvTeamUser_PublishList.DataSource = ds.Tables[0];
                gvTeamUser_PublishList.DataBind();

                gvTeamUser_PublishList.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvTeamUser_PublishList.DataSource = null;
                gvTeamUser_PublishList.DataBind();
            }
        }
        else
        {
            gvTeamUser_PublishList.DataSource = null;
            gvTeamUser_PublishList.DataBind();
        }

    }

    #region ManageFilter

    protected void ddlFilterTemplate_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "", "DisplayTab(1);", true);

        try
        {
            GetAllQuestionList();
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "ddlFilterTemplate_OnSelectedIndexChanged");
        }
    }


    protected void btnSaveQuesToTemplate_Click(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "", "DisplayTab(1);", true);
        string QuesID = "";
        try
        {
            foreach (GridViewRow row in gvGrid_Question.Rows)
            {
                int ques = Convert.ToInt32(gvGrid_Question.DataKeys[row.RowIndex].Values[0]);
                bool isSelected = (row.FindControl("chkQues") as CheckBox).Checked;
                if (isSelected)
                {
                    QuesID = QuesID + ques + ",";
                }
            }
            QuestionBM obj = new QuestionBM();
            obj.quesIDs = QuesID;
            obj.quesTmpID = Convert.ToInt32(ddlAddTemplateQues.SelectedValue);

            obj.AddQuestionTemplate();

            Response.Redirect("Question.aspx");

        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "ddlAddTemplateQues_OnSelectedIndexChanged");
        }


    }




    #endregion


    #region AssignTab DropDown
    protected void ddlAssignQuesTemplate_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "", "DisplayTab(2);", true);

        DataSet ds = GetQuesByTemplateID_CategoryID(Convert.ToInt32(ddlAssignQuesTemplate.SelectedValue), 0, true, "0");

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlAssignQuesByTemp.Items.Clear();

                ddlAssignQuesByTemp.DataSource = ds.Tables[0];
                if (Convert.ToString(Session["Culture"]) == "Danish")
                {
                    ddlAssignQuesByTemp.DataTextField = "Question";
                }
                else
                {
                    ddlAssignQuesByTemp.DataTextField = "Question";
                }
                ddlAssignQuesByTemp.DataValueField = "quesID";
                ddlAssignQuesByTemp.DataBind();

                ddlAssignQuesByTemp.Items.Insert(0, new ListItem(hdnSelectQues.Value, CommonModule.dropDownZeroValue));
            }
            else
            {
                ddlAssignQuesByTemp.Items.Clear();
                ddlAssignQuesByTemp.Items.Insert(0, new ListItem(hdnSelectQues.Value, CommonModule.dropDownZeroValue));
            }
        }
        else
        {
            ddlAssignQuesByTemp.Items.Clear();
            ddlAssignQuesByTemp.Items.Insert(0, new ListItem(hdnSelectQues.Value, CommonModule.dropDownZeroValue));
        }

    }

    protected void ddlAssignTeam_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "", "DisplayTab(2);", true);
        //UserBM obj = new UserBM();
        //obj.TeamID = Convert.ToInt32(ddlAssignTeam.SelectedValue);
        //obj.UserIDs = "0";
        //obj.GetUserByTeamID();
        //DataSet ds = obj.ds;
        //swati
        if (Convert.ToInt32(ddlAssignTeam.SelectedValue) != 0)
        {


            DataSet ds = GetUserByTeamID(Convert.ToInt32(ddlAssignTeam.SelectedValue), "0");

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlAssignUser.Items.Clear();

                    ddlAssignUser.DataSource = ds.Tables[0];
                    if (Convert.ToString(Session["Culture"]) == "Danish")
                    {
                        ddlAssignUser.DataTextField = "userFirstName";
                    }
                    else
                    {
                        ddlAssignUser.DataTextField = "userFirstName";
                    }
                    ddlAssignUser.DataValueField = "userId";
                    ddlAssignUser.DataBind();

                    ddlAssignUser.Items.Insert(0, new ListItem(hdnSelectUser.Value, CommonModule.dropDownZeroValue));
                }
                else
                {
                    ddlAssignUser.Items.Clear();
                    ddlAssignUser.Items.Insert(0, new ListItem(hdnSelectUser.Value, CommonModule.dropDownZeroValue));
                }
            }
            else
            {
                ddlAssignUser.Items.Clear();
                ddlAssignUser.Items.Insert(0, new ListItem(hdnSelectUser.Value, CommonModule.dropDownZeroValue));
            }
        }
        else
        {
            ddlAssignUser.Items.Clear();
            ddlAssignUser.Items.Insert(0, new ListItem(hdnSelectUser.Value, CommonModule.dropDownZeroValue));
        }
    }

    #endregion

    #region AssignTab Button Event

    protected void btnAssignSelectQuesTemplate_Click(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "", "DisplayTab(2);", true);
        try
        {
            List<TemplateQues> data = (List<TemplateQues>)Session["ListTempQues"];
            if (data == null)
            {
                data = new List<TemplateQues>();
            }
            Int32 tmpID = Convert.ToInt32(ddlAssignQuesTemplate.SelectedValue);

            var searchTemp = data.FirstOrDefault(o => o.TemplateID == tmpID);
            if (searchTemp == null)
            {
                DataSet ds = GetQuesByTemplateID_CategoryID(tmpID, 0, true, "0");


                var quesList = ds.Tables[0].AsEnumerable().Select(dataRow => new QuesIDs
                {
                    quesID = dataRow.Field<int>("quesID"),

                }).ToList();

                searchTemp = new TemplateQues();
                searchTemp.TemplateID = tmpID;
                // searchTemp.QuesIDs = new List<QuesIDs>();
                searchTemp.QuesIDs = quesList;
                data.Add(searchTemp);

                Session["ListTempQues"] = data;
            }



            string strTempIDs = string.Join(", ", data.Select(o => o.TemplateID).ToArray());
            GetGridAssignQuestionTemplateList(strTempIDs);
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "btnAssignSelectQuesTemplate_Click");
        }
        //string str =Convert.ToString( Session["TempIDs"]);
        //if (str == "0")
        //{
        //    Session["TempIDs"] = Convert.ToString(ddlAssignQuesTemplate.SelectedValue);
        //}
        //else
        //{
        //    Session["TempIDs"] = Convert.ToString(Session["TempIDs"])+","+ Convert.ToString(ddlAssignQuesTemplate.SelectedValue);
        //}



    }
    protected void btnAssignResetQuesTemplate_Click(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "", "DisplayTab(2);", true);
        List<TemplateQues> temp = new List<TemplateQues>();
        TemplateQues t = new TemplateQues();
        t.TemplateID = -1;
        t.QuesIDs = new List<QuesIDs>();
        temp.Add(t);
        Session["ListTempQues"] = temp;


        string strTempIDs = string.Join(", ", temp.Select(o => o.TemplateID).ToArray());
        GetGridAssignQuestionTemplateList(strTempIDs);
    }
    protected void btnAssignSelectQues_Click(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "", "DisplayTab(2);", true);
        try
        {


            List<TemplateQues> data = (List<TemplateQues>)Session["ListTempQues"];
            if (data == null)
            {
                data = new List<TemplateQues>();
            }
            Int32 tmpID = Convert.ToInt32(ddlAssignQuesTemplate.SelectedValue);
            Int32 QuesID = Convert.ToInt32(ddlAssignQuesByTemp.SelectedValue);
            if (QuesID != 0)
            {
                var searchTemp = data.FirstOrDefault(o => o.TemplateID == tmpID);
                if (searchTemp == null)
                {
                    searchTemp = new TemplateQues();
                    searchTemp.TemplateID = tmpID;
                    List<QuesIDs> qtm = new List<QuesIDs>();
                    QuesIDs qtmtmp = new QuesIDs();
                    qtmtmp.quesID = QuesID;
                    qtm.Add(qtmtmp);
                    searchTemp.QuesIDs = qtm;

                    data.Add(searchTemp);
                }
                else
                {
                    if (!searchTemp.QuesIDs.Any(o => o.quesID == QuesID))
                    {
                        QuesIDs q = new QuesIDs();
                        q.quesID = QuesID;
                        searchTemp.QuesIDs.Add(q);
                    }
                    data.Remove(searchTemp);
                    data.Add(searchTemp);
                }

                Session["ListTempQues"] = data;

                string strTempIDs = string.Join(", ", data.Select(o => o.TemplateID).ToArray());
                GetGridAssignQuestionTemplateList(strTempIDs);
            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "btnAssignSelectQues_Click");
        }
        //string str = Convert.ToString(Session["TempQuesIDs"]);
        //if (str == "0")
        //{
        //    Session["TempQuesIDs"] = Convert.ToString(ddlAssignQuesByTemp.SelectedValue);
        //}
        //else
        //{
        //    Session["TempQuesIDs"] = Convert.ToString(Session["TempQuesIDs"]) + "," + Convert.ToString(ddlAssignQuesByTemp.SelectedValue);
        //}
        // GetGridAssignTeamList(Convert.ToString(Session["TempQuesIDs"]));
    }
    protected void btnAssignSelectTeam_Click(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "", "DisplayTab(2);", true);
        try
        {
            List<TeamUser> data = (List<TeamUser>)Session["ListTeamUser"];
            if (data == null)
            {
                data = new List<TeamUser>();
            }
            Int32 tmpID = Convert.ToInt32(ddlAssignTeam.SelectedValue);

            var searchTemp = data.FirstOrDefault(o => o.TeamID == tmpID);
            if (searchTemp == null)
            {
                DataSet ds = GetUserByTeamID(tmpID, "0");

                var userList = ds.Tables[0].AsEnumerable().Select(dataRow => new UserIDs
                {
                    userID = dataRow.Field<int>("userId"),

                }).ToList();

                searchTemp = new TeamUser();
                searchTemp.TeamID = tmpID;
                //  searchTemp.UserIDs = new List<UserIDs>();
                searchTemp.UserIDs = userList;
                data.Add(searchTemp);

                Session["ListTeamUser"] = data;
            }
            else
            {
                //DataSet ds = GetUserByTeamID(tmpID, "0");

                //var userList = ds.Tables[0].AsEnumerable().Select(dataRow => new UserIDs
                //{
                //    userID = dataRow.Field<int>("userId"),

                //}).ToList();
                GetTeamListByIDs(Convert.ToString(searchTemp.TeamID));

                if (!String.IsNullOrEmpty(hdnAllTeamIDs.Value))
                {
                    var str = hdnAllTeamIDs.Value;
                    string[] strtmp = str.Split(',');
                    foreach (var i in strtmp)
                    {
                        DataSet ds = GetUserByTeamID(Convert.ToInt32(i), "0");

                        var userList = ds.Tables[0].AsEnumerable().Select(dataRow => new UserIDs
                        {
                            userID = dataRow.Field<int>("userId"),

                        }).ToList();

                        searchTemp = new TeamUser();
                        searchTemp.TeamID = Convert.ToInt32(i); ;
                        // searchTemp.UserIDs = new List<UserIDs>();
                        searchTemp.UserIDs = userList;
                        data.Add(searchTemp);
                    }
                }

                Session["ListTeamUser"] = data;
            }
            string strTeamIDs = string.Join(", ", data.Select(o => o.TeamID).ToArray());
            GetGridAssignTeamList(strTeamIDs);
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "btnAssignSelectTeam_Click");
        }
        //string str = Convert.ToString(Session["TeamIDs"]);
        //if (str == "0")
        //{
        //    Session["TeamIDs"] = Convert.ToString(ddlAssignTeam.SelectedValue);
        //}
        //else
        //{
        //    Session["TeamIDs"] = Convert.ToString(Session["TeamIDs"]) + "," + Convert.ToString(ddlAssignTeam.SelectedValue);
        //}
        //GetGridAssignTeamList(Convert.ToString(Session["TeamIDs"]));
    }

    protected void btnAssignResetTeam_Click(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "", "DisplayTab(2);", true);
        List<TeamUser> tempuser = new List<TeamUser>();
        TeamUser tt = new TeamUser();
        tt.TeamID = -1;
        tt.UserIDs = new List<UserIDs>();
        tempuser.Add(tt);
        Session["ListTeamUser"] = tempuser;

        string strTeamIDs = string.Join(", ", tempuser.Select(o => o.TeamID).ToArray());
        GetGridAssignTeamList(strTeamIDs);

    }

    protected void btnAssignSelectTeamUser_Click(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "", "DisplayTab(2);", true);
        try
        {
            List<TeamUser> data = (List<TeamUser>)Session["ListTeamUser"];
            if (data == null)
            {
                data = new List<TeamUser>();
            }
            Int32 tmpID = Convert.ToInt32(ddlAssignTeam.SelectedValue);
            Int32 UserID = Convert.ToInt32(ddlAssignUser.SelectedValue);

            if (UserID != 0)
            {
                var searchTemp = data.FirstOrDefault(o => o.TeamID == tmpID);
                if (searchTemp == null)
                {
                    searchTemp = new TeamUser();
                    searchTemp.TeamID = tmpID;
                    List<UserIDs> tmpu = new List<UserIDs>();
                    UserIDs tmputmp = new UserIDs();
                    tmputmp.userID = UserID;
                    tmpu.Add(tmputmp);

                    searchTemp.UserIDs = tmpu;
                    data.Add(searchTemp);
                }
                else
                {
                    if (!searchTemp.UserIDs.Any(o => o.userID == UserID))
                    {
                        UserIDs q = new UserIDs();
                        q.userID = UserID;
                        searchTemp.UserIDs.Add(q);
                    }
                    data.Remove(searchTemp);
                    data.Add(searchTemp);
                }

                Session["ListTeamUser"] = data;

                string strTeamIDs = string.Join(", ", data.Select(o => o.TeamID).ToArray());
                GetGridAssignTeamList(strTeamIDs);
            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "btnAssignSelectTeamUser_Click");
        }
    }

    #endregion

    #region AssignQuestion



    protected void GetGridAssignQuestionTemplateList(string TmpIDs)
    {
        try
        {
            var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
            var userId = Convert.ToInt32(Session["OrgUserId"]);
            try
            {
                DataSet ds = GetTemplateByIDs(TmpIDs);

                gvQuesTemplate.DataSource = ds.Tables[0];
                gvQuesTemplate.DataBind();
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex);
        }

    }

    protected void GetGridAssignTeamList(string TeamIDs)
    {
        DataSet ds = GetTeamListByIDs(TeamIDs);

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                gridTeamList.DataSource = ds.Tables[0];
                gridTeamList.DataBind();

                gridTeamList.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gridTeamList.DataSource = null;
                gridTeamList.DataBind();
            }
        }
        else
        {
            gridTeamList.DataSource = null;
            gridTeamList.DataBind();
        }
    }

    protected void GetAssignTeamList()
    {
        //JobTypeBM obj = new JobTypeBM();
        //obj.jobIsActive = true;
        //obj.jobIsDeleted = false;
        //obj.jobCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        //obj.GetAllTeam();
        //DataSet ds = obj.ds;

        DataSet ds = GetTeamListByIDs("0");




        #region AssignTab
        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (Convert.ToString(Session["Culture"]) == "Danish")
                {

                    ddlAssignTeam.Items.Clear();

                    ddlAssignTeam.DataSource = ds.Tables[0];
                    ddlAssignTeam.DataTextField = "TeamNameDN";
                    ddlAssignTeam.DataValueField = "TeamID";
                    ddlAssignTeam.DataBind();

                    ddlAssignTeam.Items.Insert(0, new ListItem(hdnSelectTeam.Value, CommonModule.dropDownZeroValue));
                }

                else
                {
                    ddlAssignTeam.Items.Clear();

                    ddlAssignTeam.DataSource = ds.Tables[0];
                    ddlAssignTeam.DataTextField = "TeamName";
                    ddlAssignTeam.DataValueField = "TeamID";
                    ddlAssignTeam.DataBind();

                    ddlAssignTeam.Items.Insert(0, new ListItem(hdnSelectTeam.Value, CommonModule.dropDownZeroValue));

                }
            }
            else
            {
                ddlAssignTeam.Items.Clear();
                ddlAssignTeam.Items.Insert(0, new ListItem(hdnSelectTeam.Value, CommonModule.dropDownZeroValue));
            }
        }
        else
        {
            ddlAssignTeam.Items.Clear();
            ddlAssignTeam.Items.Insert(0, new ListItem(hdnSelectTeam.Value, CommonModule.dropDownZeroValue));
        }
        #endregion
    }
    protected void gvQues_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(Session["OrgUserId"]);
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hdnquesIsMandatory_gvQues = (HiddenField)e.Row.FindControl("hdnquesIsMandatory_gvQues");
                Label lblmandatory_gvQues = (Label)e.Row.FindControl("lblmandatory_gvQues");
                if (Convert.ToBoolean(hdnquesIsMandatory_gvQues.Value) == true)
                {
                    lblmandatory_gvQues.Text = GetLocalResourceObject("TRUE.Text").ToString();
                }
                else
                {
                    lblmandatory_gvQues.Text = GetLocalResourceObject("FALSE.Text").ToString();
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void gvQuesTemplate_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(Session["OrgUserId"]);
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                List<TemplateQues> data = new List<TemplateQues>();
                try
                {
                    data = (List<TemplateQues>)Session["ListTempQues"];
                }
                catch (Exception ex)
                {
                    ExceptionLogger.LogException(ex, 0, "gvQuesTemplate_OnRowDataBound");
                }


                HiddenField QuesTmpID = (HiddenField)e.Row.FindControl("hdnqtmpID");
                HiddenField IsTmpMandatory = (HiddenField)e.Row.FindControl("hdnIsTmpMandatory");
                Label lblmandatory_gvQuesTemplate = (Label)e.Row.FindControl("lblmandatory_gvQuesTemplate");

                if (Convert.ToBoolean(IsTmpMandatory.Value) == true)
                {
                    lblmandatory_gvQuesTemplate.Text = GetLocalResourceObject("TRUE.Text").ToString();
                }
                else
                {
                    lblmandatory_gvQuesTemplate.Text = GetLocalResourceObject("FALSE.Text").ToString();
                }

                Label NoRecordFound = (Label)e.Row.FindControl("lblNoRecordFoundQues");
                GridView gvQues = e.Row.FindControl("gvQues") as GridView;

                Int32 qTmpID = Convert.ToInt32(QuesTmpID.Value);
                string strTmpQues = "0";

                var search = data.FirstOrDefault(o => o.TemplateID == qTmpID);
                if (search != null)
                {
                    strTmpQues = string.Join(",", search.QuesIDs.Select(o => o.quesID).ToArray());
                }

                if (gvQues != null)
                {
                    //QuestionBM obj = new QuestionBM();
                    //obj.quesTmpID = Convert.ToInt32(QuesTmpID.Value);
                    //obj.quesCatID = 0;
                    //obj.quesIsMandatory = Convert.ToBoolean(IsTmpMandatory.Value);

                    //obj.GetQuestionByTemplateCategory();
                    //DataSet ds = obj.ds;


                    DataSet ds = GetQuesByTemplateID_CategoryID(qTmpID, 0, Convert.ToBoolean(IsTmpMandatory.Value), strTmpQues);
                    if (ds != null)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            gvQues.DataSource = ds.Tables[0];
                            gvQues.DataBind();
                            NoRecordFound.Visible = false;
                        }
                        else
                        {
                            NoRecordFound.Visible = true;
                        }
                    }
                    else
                    {
                        NoRecordFound.Visible = true;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "gvQuesTemplate->OnRowDataBound");
        }


    }

    protected void gridTeamList_OnRowDataBoundTeam(object sender, GridViewRowEventArgs e)
    {
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(Session["OrgUserId"]);
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                List<TeamUser> data = new List<TeamUser>();
                try
                {
                    data = (List<TeamUser>)Session["ListTeamUser"];
                }
                catch (Exception ex)
                {
                    ExceptionLogger.LogException(ex, 0, "gridTeamList_OnRowDataBoundTeam");
                }


                HiddenField UserTeamID = (HiddenField)e.Row.FindControl("hdnTeamID");
                Label NoRecordFound = (Label)e.Row.FindControl("lblNoRecordFound");
                GridView gvTeamUser = e.Row.FindControl("gvTeamUser") as GridView;

                Int32 qTmpID = Convert.ToInt32(UserTeamID.Value);
                string strTmpUser = "0";

                var search = data.FirstOrDefault(o => o.TeamID == qTmpID);
                if (search != null)
                {
                    strTmpUser = string.Join(",", search.UserIDs.Select(o => o.userID).ToArray());
                }

                if (gvTeamUser != null)
                {
                    //UserBM obj = new UserBM();
                    //obj.TeamID = Convert.ToInt32(UserTeamID.Value);
                    //obj.UserIDs = strTmpUser;
                    //obj.GetUserByTeamID();
                    //DataSet ds = obj.ds;

                    DataSet ds = GetUserByTeamID(Convert.ToInt32(UserTeamID.Value), strTmpUser);

                    if (ds != null)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            gvTeamUser.DataSource = ds.Tables[0];
                            gvTeamUser.DataBind();
                            NoRecordFound.Visible = false;
                        }
                        else
                        {
                            NoRecordFound.Visible = true;
                        }
                    }
                    else
                    {
                        NoRecordFound.Visible = true;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "gvQues->OnRowDataBoundTeam");
        }


    }

    protected void grid_QuestionAssign_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(Session["OrgUserId"]);
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField AssignID = (HiddenField)e.Row.FindControl("hdngridAssignID");
                HiddenField TemplateID = (HiddenField)e.Row.FindControl("hdngridAssignTempID");
                HiddenField TeamID = (HiddenField)e.Row.FindControl("hdngridAssignTeamID");
                HiddenField hdnIsmandatory = (HiddenField)e.Row.FindControl("hdnIsmandatory");

                HiddenField QuesIDs = (HiddenField)e.Row.FindControl("hdngridAssignQuesIDs");
                HiddenField UserIDs = (HiddenField)e.Row.FindControl("hdngridAssignUserIDs");

                Label lblmandatory = (Label)e.Row.FindControl("lblmandatory");

                if (Convert.ToBoolean(hdnIsmandatory.Value) == true)
                {
                    lblmandatory.Text = GetLocalResourceObject("TRUE.Text").ToString();
                }
                else
                {
                    lblmandatory.Text = GetLocalResourceObject("FALSE.Text").ToString();
                }

                DataSet dst = new DataSet();
                dst = GetQuesByTemplateID_CategoryID(Convert.ToInt32(TemplateID.Value), 0, true, QuesIDs.Value);

                DataSet dsTeam = new DataSet();
                dsTeam = GetUserByTeamID(Convert.ToInt32(TeamID.Value), UserIDs.Value);
                GridView gvQues_AssignList = e.Row.FindControl("gvQues_AssignList") as GridView;

                GridView gvTeamUser_AssignList = e.Row.FindControl("gvTeamUser_AssignList") as GridView;
                if (dst != null)
                {
                    if (dst.Tables[0].Rows.Count > 0)
                    {
                        gvQues_AssignList.DataSource = dst.Tables[0];
                        gvQues_AssignList.DataBind();

                        gvQues_AssignList.HeaderRow.TableSection = TableRowSection.TableHeader;
                    }
                    else
                    {
                        gvQues_AssignList.DataSource = null;
                        gvQues_AssignList.DataBind();
                    }
                }
                else
                {
                    gvQues_AssignList.DataSource = null;
                    gvQues_AssignList.DataBind();
                }

                if (dsTeam != null)
                {
                    if (dsTeam.Tables[0].Rows.Count > 0)
                    {
                        gvTeamUser_AssignList.DataSource = dsTeam.Tables[0];
                        gvTeamUser_AssignList.DataBind();

                        gvTeamUser_AssignList.HeaderRow.TableSection = TableRowSection.TableHeader;
                    }
                    else
                    {
                        gvTeamUser_AssignList.DataSource = null;
                        gvTeamUser_AssignList.DataBind();
                    }
                }
                else
                {
                    gvTeamUser_AssignList.DataSource = null;
                    gvTeamUser_AssignList.DataBind();
                }

            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "grid_QuestionAssign_RowDataBound");
        }
    }
    protected void grid_QuestionAssign_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "", "DisplayTab(2);", true);
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(Session["OrgUserId"]);
        if (e.CommandName == "archive")
        {
            QuestionBM obj = new QuestionBM();
            obj.AssignID = Convert.ToInt32(e.CommandArgument);

            obj.DeleteQuestionAssign();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    GetAllAssignQuestionList();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }

        }


    }

    protected void btnAssign_click(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "", "DisplayTab(2);", true);

        var logginUserType = Convert.ToInt32(Session["OrguserType"]);
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(Session["OrgUserId"]);

        //DateTime stdt = CommonUtility.ConvertStringToDateTimewithformay(txtSearchStartDate_Public.Text);
        //return;

        try
        {
            string controlID = string.Empty;
            string innercontrolID = string.Empty;
            List<TemplateQues> qData = new List<TemplateQues>();
            List<TemplateQues> tmpqData = new List<TemplateQues>();
            qData = (List<TemplateQues>)Session["ListTempQues"];


            List<TeamUser> tData = new List<TeamUser>();
            List<TeamUser> tmptData = new List<TeamUser>();
            tData = (List<TeamUser>)Session["ListTeamUser"];

            #region Template
            foreach (var a in qData)
            {
                TemplateQues tq = new TemplateQues();
                tq.TemplateID = a.TemplateID;
                tq.QuesIDs = new List<QuesIDs>();

                foreach (GridViewRow row in gvQuesTemplate.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        //  var mp = ((HiddenField)row.FindControl("hdnChkMainQuesTemp"));

                        var mp = (row.Cells[3].FindControl("hdnChkMainQuesTemp") as HiddenField);
                        GridView gvQues = (row.Cells[0].FindControl("gvQues") as GridView);

                        foreach (var b in a.QuesIDs)
                        {
                            foreach (GridViewRow innerrow in gvQues.Rows)
                            {
                                if (row.RowType == DataControlRowType.DataRow)
                                {
                                    var p = (innerrow.Cells[2].FindControl("hdnChkQuesTemp") as HiddenField);
                                    var pQues = (innerrow.Cells[2].FindControl("hdnID") as HiddenField);

                                    string[] pQuesIds = pQues.Value.Split(',');

                                    if (pQuesIds.Contains(Convert.ToString(b.quesID)))
                                    {
                                        if (p.Value == "1,1" || p.Value == "1")
                                        {
                                            if (!tq.QuesIDs.Any(o => o.quesID == b.quesID))
                                            {
                                                tq.QuesIDs.Add(b);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
                if (tq.QuesIDs.Count > 0)
                {
                    tmpqData.Add(tq);
                }
            }
            #endregion

            #region Team
            foreach (var a in tData)
            {
                TeamUser tq = new TeamUser();
                tq.TeamID = a.TeamID;
                tq.UserIDs = new List<UserIDs>();

                foreach (GridViewRow row in gridTeamList.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        //  var mp = ((HiddenField)row.FindControl("hdnChkMainQuesTemp"));
                        var mp = (row.Cells[2].FindControl("hdnChkMainTeam") as HiddenField);
                        GridView gvTeamUser = (row.Cells[0].FindControl("gvTeamUser") as GridView);

                        foreach (var b in a.UserIDs)
                        {
                            foreach (GridViewRow innerrow in gvTeamUser.Rows)
                            {
                                if (row.RowType == DataControlRowType.DataRow)
                                {
                                    var p = (innerrow.Cells[1].FindControl("hdnChkTeamUser") as HiddenField);
                                    var pUser = (innerrow.Cells[1].FindControl("hdnUsernoID") as HiddenField);
                                    string[] pUserIds = pUser.Value.Split(',');

                                    if (pUserIds.Contains(Convert.ToString(b.userID)))
                                    {
                                        if (p.Value == "1,1" || p.Value == "1")
                                        {
                                            if (!tq.UserIDs.Any(o => o.userID == b.userID))
                                            {
                                                tq.UserIDs.Add(b);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
                if (tq.UserIDs.Count > 0)
                {
                    tmptData.Add(tq);
                }
            }
            #endregion

            if (tmptData.Count > 0 && tmpqData.Count > 0)
            {

                foreach (var x in tmpqData)
                {
                    string quesIDs = string.Join(",", x.QuesIDs.Select(o => o.quesID).ToArray());
                    foreach (var y in tmptData)
                    {
                        string userIDs = string.Join(",", y.UserIDs.Select(o => o.userID).ToArray());
                        QuestionBM q = new QuestionBM();
                        q.AssignID = 0;
                        q.UserId = userId;
                        q.queCompanyId = companyId;
                        q.StartDate = CommonUtility.ConvertStringToDateTimewithformay(txtSearchStartDate_Public.Text);
                        q.EndDate = CommonUtility.ConvertStringToDateTimewithformay(txtSearchEndDate_Public.Text);
                        q.queIsActive = true;
                        q.queIsDeleted = false;
                        q.Status = "Assign";
                        q.quesTmpID = x.TemplateID;
                        q.quesIDs = quesIDs;
                        q.TeamID = y.TeamID;
                        q.UserIDs = userIDs;

                        q.InsertQuestionAssignData();
                    }
                }
            }
            //  Session["ListTempQues"] = qData;

            GetAllAssignQuestionList();
            GetPublishQuesUserList();
            //Note: After saving Data reinitialize the List
            List<TemplateQues> temp = new List<TemplateQues>();
            TemplateQues t = new TemplateQues();
            t.TemplateID = -1;
            t.QuesIDs = new List<QuesIDs>();
            temp.Add(t);
            Session["ListTempQues"] = temp;

            string strTempIDs = string.Join(", ", temp.Select(o => o.TemplateID).ToArray());
            GetGridAssignQuestionTemplateList(strTempIDs);

            List<TeamUser> tempuser = new List<TeamUser>();
            TeamUser tt = new TeamUser();
            tt.TeamID = -1;
            tt.UserIDs = new List<UserIDs>();
            tempuser.Add(tt);
            string strTeamIDs = string.Join(", ", tempuser.Select(o => o.TeamID).ToArray());
            GetGridAssignTeamList(strTeamIDs);

            TeamUser tt1 = new TeamUser();
            tt1.TeamID = 0;
            tt1.UserIDs = new List<UserIDs>();
            tempuser.Add(tt1);
            Session["ListTeamUser"] = tempuser;



        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "btnAssign_click");
        }
    }
    #endregion

    #region Publish Tab
    protected void gridPublishQues_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(Session["OrgUserId"]);
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField AssignID = (HiddenField)e.Row.FindControl("hdngridPublishAssignID");
                HiddenField TemplateID = (HiddenField)e.Row.FindControl("hdngridPublishTempID");
                HiddenField TeamID = (HiddenField)e.Row.FindControl("hdngridPublishTeamID");

                HiddenField QuesIDs = (HiddenField)e.Row.FindControl("hdngridPublishQuesIDs");
                HiddenField UserIDs = (HiddenField)e.Row.FindControl("hdngridPublishUserIDs");

                HiddenField hdnIsmandatory = (HiddenField)e.Row.FindControl("hdnIsmandatoryPublish");
                Label lblmandatory = (Label)e.Row.FindControl("lblmandatoryPublish");

                HiddenField hdnStatusPublish = (HiddenField)e.Row.FindControl("hdnStatusPublish");
                Label lblStatusPublish = (Label)e.Row.FindControl("lblStatusPublish");
                LinkButton lbkPublishQues = (LinkButton)e.Row.FindControl("lbkPublishQues");

                if (Convert.ToBoolean(hdnIsmandatory.Value) == true)
                {
                    lblmandatory.Text = GetLocalResourceObject("TRUE.Text").ToString();
                }
                else
                {
                    lblmandatory.Text = GetLocalResourceObject("FALSE.Text").ToString();
                }

                if (Convert.ToString(hdnStatusPublish.Value) == "Assign")
                {
                    lblStatusPublish.Text = GetLocalResourceObject("assignres1.Text").ToString();
                }
                else if (Convert.ToString(hdnStatusPublish.Value) == "Publish")
                {
                    lblStatusPublish.Text = GetLocalResourceObject("publishres1.Text").ToString();
                }
                else
                {
                    lblStatusPublish.Text = GetLocalResourceObject("unpublishres1.Text").ToString();
                }
                if (Convert.ToString(hdnStatusPublish.Value) == "Publish")
                {
                    lbkPublishQues.Text = GetLocalResourceObject("unpublishres.Text").ToString();
                }
                else
                {
                    lbkPublishQues.Text = GetLocalResourceObject("publishres.Text").ToString();
                }
                DataSet dst = new DataSet();
                dst = GetQuesByTemplateID_CategoryID(Convert.ToInt32(TemplateID.Value), 0, true, QuesIDs.Value);

                DataSet dsTeam = new DataSet();
                dsTeam = GetUserByTeamID(Convert.ToInt32(TeamID.Value), UserIDs.Value);
                GridView gvPublishQues_AssignList = e.Row.FindControl("gvPublishQues_AssignList") as GridView;

                GridView gvPublishTeamUser_AssignList = e.Row.FindControl("gvPublishTeamUser_AssignList") as GridView;
                if (dst != null)
                {
                    if (dst.Tables[0].Rows.Count > 0)
                    {
                        gvPublishQues_AssignList.DataSource = dst.Tables[0];
                        gvPublishQues_AssignList.DataBind();

                        gvPublishQues_AssignList.HeaderRow.TableSection = TableRowSection.TableHeader;
                    }
                    else
                    {
                        gvPublishQues_AssignList.DataSource = null;
                        gvPublishQues_AssignList.DataBind();
                    }
                }
                else
                {
                    gvPublishQues_AssignList.DataSource = null;
                    gvPublishQues_AssignList.DataBind();
                }

                if (dsTeam != null)
                {
                    if (dsTeam.Tables[0].Rows.Count > 0)
                    {
                        gvPublishTeamUser_AssignList.DataSource = dsTeam.Tables[0];
                        gvPublishTeamUser_AssignList.DataBind();

                        gvPublishTeamUser_AssignList.HeaderRow.TableSection = TableRowSection.TableHeader;
                    }
                    else
                    {
                        gvPublishTeamUser_AssignList.DataSource = null;
                        gvPublishTeamUser_AssignList.DataBind();
                    }
                }
                else
                {
                    gvPublishTeamUser_AssignList.DataSource = null;
                    gvPublishTeamUser_AssignList.DataBind();
                }

            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "grid_QuestionAssign_RowDataBound");
        }
    }
    protected void gridPublishQues_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "", "DisplayTab(3);", true);
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(Session["OrgUserId"]);
        if (e.CommandName == "archive")
        {
            QuestionBM obj = new QuestionBM();
            obj.AssignID = Convert.ToInt32(e.CommandArgument);

            obj.DeleteQuestionAssign();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    GetAllAssignQuestionList();
                    GetPublishQuesUserList();
                    gvTeamUser_PublishQuesList.DataSource = null;
                    gvTeamUser_PublishQuesList.DataBind();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }

        }
        else if (e.CommandName == "Publish")
        {
            QuestionBM obj = new QuestionBM();
            obj.AssignID = Convert.ToInt32(e.CommandArgument);
            obj.Status = "Publish";
            obj.UpdateAssignQuestionStatus();

            DataSet ds = obj.ds;
            try
            {


                if (ds != null)
                {
                    string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                    if (returnMsg == "success")
                    {
                        var db = new startetkuEntities1();
                        db.InsertUpdateEmailData(companyId, userId, "0", obj.AssignID, "Question","");
                        db.InsertNotification_Publish("Question", companyId, userId, obj.AssignID, "quesPub", "New Question Publish");
                        GetAllAssignQuestionList();
                        GetPublishQuesUserList();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex, 0, "Publish -> InsertNotification_Publish");
            }
        }
        else if (e.CommandName == "UnPublish")
        {
            QuestionBM obj = new QuestionBM();
            obj.AssignID = Convert.ToInt32(e.CommandArgument);
            obj.Status = "UnPublish";
            obj.UpdateAssignQuestionStatus();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    GetAllAssignQuestionList();
                    GetPublishQuesUserList();

                    gvTeamUser_PublishQuesList.DataSource = null;
                    gvTeamUser_PublishQuesList.DataBind();
                }
            }
        }

    }

    protected void gvTeamUser_PublishList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "", "DisplayTab(3);", true);
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(Session["OrgUserId"]);
        if (e.CommandName == "ViewRow")
        {
            try
            {


                Int32 assignUserID = Convert.ToInt32(e.CommandArgument);
                hdnPublishTeamUserID.Value = Convert.ToString(assignUserID);
                DataSet ds = GetPublishUserQuestionList(assignUserID);


                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        gvTeamUser_PublishQuesList.DataSource = ds.Tables[0];
                        gvTeamUser_PublishQuesList.DataBind();

                        gvTeamUser_PublishQuesList.HeaderRow.TableSection = TableRowSection.TableHeader;
                    }
                    else
                    {
                        gvTeamUser_PublishQuesList.DataSource = null;
                        gvTeamUser_PublishQuesList.DataBind();
                    }
                }
                else
                {
                    gvTeamUser_PublishQuesList.DataSource = null;
                    gvTeamUser_PublishQuesList.DataBind();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex, 0, "gvTeamUser_PublishList_RowCommand->ViewRow");
            }
        }
    }

    protected void ddlFilterAssignPublishQuestion_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "", "DisplayTab(3);", true);

        try
        {
            Int32 assignUserID = Convert.ToInt32(hdnPublishTeamUserID.Value);
            DataSet ds = GetPublishUserQuestionList(assignUserID);


            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvTeamUser_PublishQuesList.DataSource = ds.Tables[0];
                    gvTeamUser_PublishQuesList.DataBind();

                    gvTeamUser_PublishQuesList.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                else
                {
                    gvTeamUser_PublishQuesList.DataSource = null;
                    gvTeamUser_PublishQuesList.DataBind();
                }
            }
            else
            {
                gvTeamUser_PublishQuesList.DataSource = null;
                gvTeamUser_PublishQuesList.DataBind();
            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "ddlFilterAssignPublishQuestion_OnSelectedIndexChanged");
        }
    }

    protected void gvTeamUser_PublishQuesList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(Session["OrgUserId"]);
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                HiddenField hdntmpNamePublishUser = (HiddenField)e.Row.FindControl("hdntmpNamePublishUser");
                HiddenField hdntmpNamePublishUserDN = (HiddenField)e.Row.FindControl("hdntmpNamePublishUserDN");
                Label lbltmpNamePublishUser = (Label)e.Row.FindControl("lbltmpNamePublishUser");
                if (Convert.ToString(Session["Culture"]) == "Danish")
                {
                    lbltmpNamePublishUser.Text = hdntmpNamePublishUserDN.Value;
                }
                else
                {
                    lbltmpNamePublishUser.Text = hdntmpNamePublishUser.Value;
                }

                HiddenField hdnmandatoryPublishUser = (HiddenField)e.Row.FindControl("hdnmandatoryPublishUser");
                Label lblmandatoryPublishUser = (Label)e.Row.FindControl("lblmandatoryPublishUser");


                if (Convert.ToBoolean(hdnmandatoryPublishUser.Value) == true)
                {
                    lblmandatoryPublishUser.Text = GetLocalResourceObject("TRUE.Text").ToString();
                }
                else
                {
                    lblmandatoryPublishUser.Text = GetLocalResourceObject("FALSE.Text").ToString();
                }


            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "gvTeamUser_PublishQuesList_RowDataBound");
        }
    }
    //protected void gvTeamUser_PublishList_RowDataBound(object sender, GridViewRowEventArgs e)
    //{
    //    var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
    //    var userId = Convert.ToInt32(Session["OrgUserId"]);
    //    try
    //    {
    //        if (e.Row.RowType == DataControlRowType.DataRow)
    //        {

    //            HiddenField hdnTeamNamePublishUser = (HiddenField)e.Row.FindControl("hdnTeamNamePublishUser");
    //            HiddenField hdnTeamNamePublishUserDN = (HiddenField)e.Row.FindControl("hdnTeamNamePublishUserDN");
    //            Label lblTeamNamePublishUser = (Label)e.Row.FindControl("lblTeamNamePublishUser");
    //            if (Convert.ToString(Session["Culture"]) == "Danish")
    //            {
    //                lblTeamNamePublishUser.Text = hdnTeamNamePublishUserDN.Value;
    //            }
    //            else
    //            {
    //                lblTeamNamePublishUser.Text = hdnTeamNamePublishUser.Value;
    //            }
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        ExceptionLogger.LogException(ex, 0, "gvTeamUser_PublishList_RowDataBound");
    //    }
    //}
    #endregion

    #region Grid Method
    protected void gvGrid_Question_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }
    protected void gvGrid_Question_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "archive")
        {
            try
            {
                QuestionBM obj = new QuestionBM();
                obj.queId = Convert.ToInt32(e.CommandArgument);
                obj.queIsActive = false;
                obj.queIsDeleted = true;
                obj.QuestionStatusUpdate();

                DataSet ds = obj.ds;
                if (ds != null)
                {
                    string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                    if (returnMsg == "success")
                    {
                        GetAllQuestionList();
                        lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                        lblMsg.ForeColor = System.Drawing.Color.White;
                    }
                    else
                    {
                        lblMsg.Text = CommonModule.msgSomeProblemOccure;
                    }
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex, 0, "gvGrid_Question_RowCommand->Archive");
            }
        }
        else if (e.CommandName == "EditRow")
        {

            lblModelHeader.Text = "Edit Question";
            try
            {


                GetAllQuestionCategory();
                GetAllQuestionTemplateList();
                int quesID = Convert.ToInt32(e.CommandArgument);

                DataSet ds = GetQuestionByID(quesID);
                btnUpdateQuestion.Visible = true;
                btnCreateQuestion.Visible = false;
                hdnQuesID.Value = Convert.ToString(quesID);
                ddlQuesCat.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["qCatID"]);
                txtQuestion.Text = Convert.ToString(ds.Tables[0].Rows[0]["Question"]);
                txtquesDescription.Text = Convert.ToString(ds.Tables[0].Rows[0]["quesDescription"]);
                txtSeqNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["SeqNo"]);
                ddtTemplateVal.Value = Convert.ToString(ds.Tables[0].Rows[0]["TemplateID"]);
                lblCreateName.Visible = false;
                txtCreateName.Visible = false;
                lblCreateDate.Visible = false;
                txtCreateDate.Visible = false;
                ScriptManager.RegisterStartupScript((sender as Control), this.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lblQuestion').click();", true);
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex, 0, "gvGrid_Question_RowCommand->EditRow");
            }
        }
        else if (e.CommandName == "ViewRow")
        {
            ScriptManager.RegisterStartupScript((sender as Control), this.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lblQuestion').click();", true);
            lblModelHeader.Text = "View Question";
            try
            {


                GetAllQuestionCategory();
                GetAllQuestionTemplateList();
                int quesID = Convert.ToInt32(e.CommandArgument);

                DataSet ds = GetQuestionByID(quesID);
                btnUpdateQuestion.Visible = false;
                btnCreateQuestion.Visible = false;
                hdnQuesID.Value = Convert.ToString(quesID);
                ddlQuesCat.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["qCatID"]);
                txtQuestion.Text = Convert.ToString(ds.Tables[0].Rows[0]["Question"]);
                txtquesDescription.Text = Convert.ToString(ds.Tables[0].Rows[0]["quesDescription"]);
                txtSeqNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["SeqNo"]);
                ddtTemplateVal.Value = Convert.ToString(ds.Tables[0].Rows[0]["TemplateID"]);
                lblCreateName.Visible = true;
                txtCreateName.Visible = true;
                lblCreateDate.Visible = true;
                txtCreateDate.Visible = true;
                txtCreateDate.Text = Convert.ToDateTime(ds.Tables[0].Rows[0]["CreateDate"]).ToShortDateString();
                txtCreateName.Text = Convert.ToString(ds.Tables[0].Rows[0]["userFirstName"]) + " " + Convert.ToString(ds.Tables[0].Rows[0]["userLastName"]);
            }

            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex, 0, "gvGrid_Question_RowCommand->ViewRow");
            }
        }
    }
    #endregion

    #region Methods

    protected void OpenCreateQuestion(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lblQuestion').click();", true);
        lblModelHeader.Text = "Add Question";
        txtQuestion.Text = "";
        txtquesDescription.Text = "";
        txtSeqNo.Text = "";
        txtqcatName.Text = "";
        txtqcatNameDN.Text = "";
        txtqcatDescription.Text = "";
        txttmpName.Text = "";
        txttmpNameDN.Text = "";
        txttmpDescription.Text = "";
        ddtTemplateVal.Value = "";

        GetAllQuestionCategory();
        GetAllQuestionTemplateList();
        btnUpdateQuestion.Visible = false;
        btnCreateQuestion.Visible = true;
        lblCreateName.Visible = false;
        txtCreateName.Visible = false;
        lblCreateDate.Visible = false;
        txtCreateDate.Visible = false;
    }

    protected void OpenCreateQuestionCat(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lblQuestionCat').click();", true);
        lblQuesCatModalHeader.Text = "Add Question Category";
        txtQuestion.Text = "";
        txtquesDescription.Text = "";
        txtSeqNo.Text = "";
        txtqcatName.Text = "";
        txtqcatNameDN.Text = "";
        txtqcatDescription.Text = "";
        txttmpName.Text = "";
        txttmpNameDN.Text = "";
        txttmpDescription.Text = "";
    }

    protected void OpenCreateQuestionTemplate(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lblQuestionTemp').click();", true);
        txtQuestion.Text = "";
        txtquesDescription.Text = "";
        txtSeqNo.Text = "";
        txtqcatName.Text = "";
        txtqcatNameDN.Text = "";
        txtqcatDescription.Text = "";
        txttmpName.Text = "";
        txttmpNameDN.Text = "";
        txttmpDescription.Text = "";
    }


    protected void ddlQuesCat_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            //ScriptManager.RegisterStartupScript(updtPnl, updtPnl.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lbl').click();", true);

            ClientScript.RegisterStartupScript(this.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lbl').click();", true);
            var selectedCatId = ddlQuesCat.SelectedValue;

            //var getCatObj = ActivityCategoryLogic.GetActivityCategoryList(ResLangId, 0, -1, -1, Convert.ToInt32(selectedCatId));

            //if (getCatObj.Any())
            //{
            //    var activityCategoryGetResult = getCatObj.FirstOrDefault();
            //    var isCompetenceCat = activityCategoryGetResult != null && Convert.ToBoolean(activityCategoryGetResult.ActCompEnabled);
            //    chkIsCompDevDoc.SelectedValue = isCompetenceCat ? "true" : "false";

            //    if (!isCompetenceCat)
            //    {
            //        ddCompetence.Enabled = false;
            //        chkIsCompDevDoc.Enabled = false;
            //        rdoLevel.Enabled = false;
            //    }
            //    else
            //    {
            //        ddCompetence.Enabled = true;
            //        chkIsCompDevDoc.Enabled = true;
            //        rdoLevel.Enabled = true;
            //    }
            //}
        }
        catch (Exception)
        {


        }
    }
    #endregion

    #region ModalPopup Method
    protected void CreateQuestion(object sender, EventArgs e)
    {
        CreateQuestionMethod();
        GetAllQuestionList();
        Response.Redirect("Question.aspx");

    }
    protected void UpdateQuestion(object sender, EventArgs e)
    {
        UpdateQuestionMethod();
        GetAllQuestionList();
        Response.Redirect("Question.aspx");
    }
    protected void CreateQuestionCategory(object sender, EventArgs e)
    {
        CreateQuestionCatMethod();
        GetAllQuestionList();
        Response.Redirect("Question.aspx");
    }
    protected void CreateQuestionTemplate(object sender, EventArgs e)
    {
        CreateQuestionTemplateMethod();
        GetAllQuestionList();
        Response.Redirect("Question.aspx");
    }
    private void clearQuesCat()
    {
        txtqcatName.Text = "";
        txtqcatNameDN.Text = "";
        txtquesDescription.Text = "";
    }
    protected void CreateQuestionCatMethod()
    {
        try
        {
            // check Duplicate
            var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
            var userId = Convert.ToInt32(Session["OrgUserId"]);
            var logginUserType = Convert.ToInt32(Session["OrguserType"]);
            QuestionBM obj = new QuestionBM();
            if (txtqcatName.Text != "")
            {


                obj.quesCatID = 0;
                obj.quesCategoryName = txtqcatName.Text;
                obj.quesCategoryNameDN = txtqcatNameDN.Text;
                obj.quesDescription = txtquesDescription.Text;

                obj.CreatedByID = userId;
                obj.queCompanyId = companyId;

                obj.queIsActive = true;
                obj.queIsDeleted = false;

                obj.queCreatedDate = DateTime.Now;

                obj.InsertQuestionCategoryData();
            }
            DataSet ds = obj.ds;

            clearQuesCat();
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    lblMsg.Text = CommonModule.msgRecordInsertedSuccessfully;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgAlreadyExists;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }

        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex);
        }
    }
    protected void CreateQuestionMethod()
    {
        try
        {
            // check Duplicate
            var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
            var userId = Convert.ToInt32(Session["OrgUserId"]);
            var logginUserType = Convert.ToInt32(Session["OrguserType"]);
            if (!String.IsNullOrEmpty(txtQuestion.Text) || ddlQuesCat.SelectedIndex != 0)
            {
                QuestionBM obj = new QuestionBM();
                obj.queId = 0;
                obj.quesCatID = Convert.ToInt32(ddlQuesCat.SelectedValue);
                obj.queQuestion = txtQuestion.Text;
                obj.quesDescription = txtquesDescription.Text;
                obj.SeqNo = Convert.ToInt32(txtSeqNo.Text);
                obj.CreatedByID = userId;
                obj.queCompanyId = companyId;
                obj.quesTemplateID = String.IsNullOrEmpty(ddtTemplateVal.Value) ? "0" : ddtTemplateVal.Value;
                obj.queIsActive = true;
                obj.queIsDeleted = false;
                obj.queCreatedDate = DateTime.Now;
                obj.InsertQuestionData();
                DataSet ds = obj.ds;
            }
            cleatQuesData();

        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex);
        }
    }
    private void cleatQuesData()
    {
        ddlQuesCat.SelectedIndex = 0;
        txtQuestion.Text = "";
        txtquesDescription.Text = "";
        txtSeqNo.Text = "";
        ddtTemplateVal.Value = "";
    }
    protected void UpdateQuestionMethod()
    {
        try
        {
            // check Duplicate
            var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
            var userId = Convert.ToInt32(Session["OrgUserId"]);
            var logginUserType = Convert.ToInt32(Session["OrguserType"]);
            if (ddlQuesCat.SelectedIndex != 0 && !String.IsNullOrEmpty(txtQuestion.Text))
            {
                QuestionBM obj = new QuestionBM();
                obj.queId = Convert.ToInt32(hdnQuesID.Value);
                obj.quesCatID = Convert.ToInt32(ddlQuesCat.SelectedValue);
                obj.queQuestion = txtQuestion.Text;
                obj.quesDescription = txtquesDescription.Text;
                obj.SeqNo = Convert.ToInt32(txtSeqNo.Text);
                obj.CreatedByID = userId;
                obj.queCompanyId = companyId;
                obj.quesTemplateID = String.IsNullOrEmpty(ddtTemplateVal.Value) ? "0" : ddtTemplateVal.Value;
                obj.queIsActive = true;
                obj.queIsDeleted = false;
                obj.queCreatedDate = DateTime.Now;
                obj.InsertQuestionData();
                DataSet ds = obj.ds;
            }
            ddlQuesCat.SelectedIndex = 0;
            txtQuestion.Text = "";
            txtquesDescription.Text = "";
            txtSeqNo.Text = "";


        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex);
        }
    }
    private void clearQuesTemplate()
    {
        txttmpName.Text = "";
        txttmpNameDN.Text = "";
        txttmpDescription.Text = "";
    }
    protected void CreateQuestionTemplateMethod()
    {
        try
        {
            // check Duplicate
            var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
            var userId = Convert.ToInt32(Session["OrgUserId"]);
            var logginUserType = Convert.ToInt32(Session["OrguserType"]);
            QuestionBM obj = new QuestionBM();
            if (!String.IsNullOrEmpty(txttmpName.Text))
            {
                obj.quesTmpID = 0;
                obj.quesTemplateName = txttmpName.Text;
                obj.quesTemplateNameDN = txttmpNameDN.Text;
                obj.quesDescription = txttmpDescription.Text;
                obj.CreatedByID = userId;
                obj.queCompanyId = companyId;

                obj.queIsActive = Convert.ToBoolean(rdoIsActive.SelectedItem.Value);
                obj.quesIsPublic = Convert.ToBoolean(roIsPublic.SelectedItem.Value);
                obj.quesIsMandatory = Convert.ToBoolean(roIsMandatory.SelectedItem.Value);

                obj.queIsDeleted = false;
                obj.queCreatedDate = DateTime.Now;
                obj.InsertQuestionTemplateData();
            }
            DataSet ds = obj.ds;

            clearQuesTemplate();

        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex);
        }
    }

    #endregion


    #region GeneralMethod
    private DataSet GetTemplateByIDs(string TmpIDs)
    {
        QuestionBM obj = new QuestionBM();
        obj.quesTmpIDs = TmpIDs;
        obj.queIsActive = true;
        obj.queIsDeleted = false;
        obj.queCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.CreatedByID = Convert.ToInt32(Session["OrgUserId"]);

        obj.GetAllQuestionTemplateList();
        DataSet ds = obj.ds;

        return ds;
    }

    private DataSet GetQuesByTemplateID_CategoryID(Int32 TempID, Int32 CatID, bool IsTmpMandatory, string QuesIDs)
    {
        QuestionBM obj = new QuestionBM();
        obj.quesTmpID = Convert.ToInt32(TempID);
        obj.quesCatID = CatID;
        obj.quesIsMandatory = Convert.ToBoolean(IsTmpMandatory);
        obj.quesIDs = QuesIDs;

        obj.GetQuestionByTemplateCategory();
        DataSet ds = obj.ds;

        return ds;
    }
    private DataSet GetTeamListByIDs(string TeamIDs)
    {
        JobTypeBM obj = new JobTypeBM();
        obj.TeamIDs = TeamIDs;
        obj.jobIsActive = true;
        obj.jobIsDeleted = false;
        obj.jobCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllTeam();
        DataSet ds = obj.ds;

        var userList = ds.Tables[0].AsEnumerable().Select(dataRow => new UserIDs
        {
            userID = dataRow.Field<int>("TeamID"),

        }).ToList();
        string strTeamIDs = string.Join(", ", userList.Select(o => o.userID).ToArray());
        hdnAllTeamIDs.Value = strTeamIDs;

        return ds;
    }

    private DataSet GetUserByTeamID(Int32 TeamID, string UserIDs)
    {
        DataSet ds = new DataSet();
        try
        {
            UserBM obj = new UserBM();
            //swati
            //if (TeamID == 0)
            //{
            //    Team
            //}
            obj.TeamID = TeamID;
            obj.UserIDs = UserIDs;
            obj.GetUserByTeamID();
            ds = obj.ds;
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "GetUserByTeamID");
        }

        return ds;
    }

    private DataSet GetAssignQuestionList()
    {
        var logginUserType = Convert.ToInt32(Session["OrguserType"]);
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(Session["OrgUserId"]);

        QuestionBM obj = new QuestionBM();
        obj.UserId = userId;
        obj.queCompanyId = companyId;
        obj.queIsActive = true;
        obj.queIsDeleted = false;
        obj.GetAllQuestionAssignList();
        DataSet ds = obj.ds;
        return ds;
    }

    private DataSet GetPublishUserQuestionList(int assignuserID)
    {
        var logginUserType = Convert.ToInt32(Session["OrguserType"]);
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(Session["OrgUserId"]);

        QuestionBM obj = new QuestionBM();
        obj.UserId = userId;
        obj.queCompanyId = companyId;
        obj.AssignID = assignuserID;
        obj.Status = Convert.ToString(ddlFilterAssignPublishQuestion.SelectedValue);
        obj.GetPublishUserQuestionList();
        DataSet ds = obj.ds;
        return ds;
    }

    #endregion

    #region Notification
    private void UpdateNotification()
    {
        var db = new startetkuEntities1();
        try
        {
            if (!String.IsNullOrEmpty(Request.QueryString["notif"]))
            {
                var notId = Convert.ToInt32(Request.QueryString["notif"]);
                var notObj = db.Notifications.FirstOrDefault(o => o.notId == notId);
                if (notObj != null)
                {
                    notObj.notIsActive = false;
                    db.SaveChanges();
                }
            }

        }
        catch (Exception)
        {

            throw;
        }
    }
    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        // string language = "French";
        string language = Convert.ToString(Session["Culture"]);

        string languageId = "";

        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }

        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion
}


public partial class TemplateQues
{
    public int TemplateID;
    public List<QuesIDs> QuesIDs;
}

public partial class QuesIDs
{
    public int quesID { get; set; }
}

//public partial class TeamUser
//{
//    public int TeamID;
//    public List<UserIDs> UserIDs;
//}

//public partial class UserIDs
//{
//    public int userID { get; set; }
//}