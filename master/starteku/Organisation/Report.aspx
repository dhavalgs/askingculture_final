﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    EnableEventValidation="false" AutoEventWireup="true" CodeFile="Report.aspx.cs"
    Inherits="Organisation_Report" Culture="auto" meta:resourcekey="PageResource1"
    UICulture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--Chart--%>
    <%--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script src="../Scripts/Scripts/exporting.js"></script>
    <script src="../Scripts/highcharts.js"></script>--%>
    <style type="text/css">
        .report_table td {
            text-align: left;
        }
    </style>

</asp:Content>




<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link href="../plugins/joyride/joyride-2.1.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/multiselect.css" rel="stylesheet" />
    <link href="../Styles/UpdateCustom.css" rel="stylesheet" type="text/css" />
    <script src="../plugins/joyride/jquery.cookie.js" type="text/javascript"></script>
    <script src="../plugins/joyride/jquery.joyride-2.1.js" type="text/javascript"></script>
    <script src="../plugins/joyride/modernizr.mq.js" type="text/javascript"></script>


    <style type="text/css">
        .radio input[type="radio"], .radio-inline input[type="radio"], .checkbox input[type="checkbox"], .checkbox-inline input[type="checkbox"] {
            margin-left: -20px !important;
            width: auto;
        }

        .dropdown-menu > li > a {
            padding: 8px 20px !important;
        }

        .multiselect.dropdown-toggle.btn.btn-default {
            max-width: 560px;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
        }

        /*.multiselect-container ul {
            height: 500px;
            overflow: scroll;
        }*/
    </style>
    <script type="text/javascript">


        function ChangeColor(a) {
            // alert(a);
            $("#" + a).parent().css('background', 'ActiveBorder');
        }
    </script>
    <%-- <asp:UpdatePanel runat="server" ID="up_report">
       <ContentTemplate>--%>

    <asp:HiddenField runat="server" ID="hdnAlpPlpChart" meta:resourcekey="resourceAlpPlarChart" Value="Active learningspoints And Passive learning points Chart"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdnPdpChart" meta:resourcekey="resourcePdpChart" Value="Personal Development Points Chart"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdnCompCatNaME" meta:resourcekey="resourceCompCatNaME" Value="Competence Category Name"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdnBasic" meta:resourcekey="resourceBasic" Value="Basic"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdnActual" meta:resourcekey="resourceActual" Value="Actual"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdnTarget" meta:resourcekey="resourceTarget" Value="Target"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdnreportApproveBy" meta:resourcekey="resourcereportApproveBy" Value="This report was approved by"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdnReportGraph" meta:resourcekey="resourceReportGraph" Value=""></asp:HiddenField>

    <asp:HiddenField runat="server" ID="hdnDocumentShared" meta:resourcekey="resourceDocumentShared" Value="Document Shared"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdndocType" meta:resourcekey="resourcedocType" Value="Document Type"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdndocSharedName" meta:resourcekey="resourcedocSharedName" Value="Name"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdndocName" meta:resourcekey="resourcedocName" Value="Document Name"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdndocDate" meta:resourcekey="resourcedocDate" Value="Date"></asp:HiddenField>

    <asp:HiddenField ID="hdnNoOption" runat="server" Value="" meta:resourcekey="NoOptionRes" />

    <asp:HiddenField runat="server" ID="GraphLevel" meta:resourcekey="GraphLevel"></asp:HiddenField>

    <%-- <asp:HiddenField runat="server" ID="SelectTeam" meta:resourcekey="SelectTeam"></asp:HiddenField>--%>
    <%--   <asp:Label ID="SelectTeam" Text="0" Visible="true" runat="server" meta:resourcekey="SelectTeam"></asp:Label>--%>
    <div class="">
        <div class="container" id="div1" runat="server">
            <div class="col-md-8">
                <div class="heading-sec">
                    <h1>
                        <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Report" EnableViewState="false" />
                        <i><span runat="server" id="Settings"></span></i>
                    </h1>
                </div>
            </div>
            <%-- <div class="col-md-3" style="display: none">
                <div class="dropdown-example">
                    <ul class="nav nav-pills">
                        <li class="dropdown" style="float: left!important; width: 100%;"><a class="skill_dropdown"
                            id="drop7" role="button" data-toggle="dropdown" href="#">Account Department<b class="skill_caret"></b></a>
                            <ul id="menu7" class="dropdown-menu" role="menu" aria-labelledby="drop7" style="width: 100%;">
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Account Department</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Sales Department</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Human Resources Department
                                </a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Software Department</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Network Department</a></li>
                                <li role="presentation" class="divider"></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="ReportAll.aspx">All</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>--%>
            <div class="col-md-4">
                <asp:HiddenField runat="server" ID="GraphSubTitle" meta:resourcekey="ResourceGraphSubTitle"></asp:HiddenField>
                <asp:HiddenField runat="server" ID="GraphAxisLevel" meta:resourcekey="ResourceGraphAxisLevel"></asp:HiddenField>
                <asp:HiddenField runat="server" ID="GraphToolTip" meta:resourcekey="ResourceGraphToolTip"></asp:HiddenField>
                <div class="dropdown-example">
                    <ul class="nav nav-pills">
                        <li class="dropdown" style="float: left!important; width: 100%;">
                            <a class="skill_dropdown" id="drop7" role="button" data-toggle="dropdown" href="#"><span
                                id="ddLabel"><%--Original Competence Level--%><asp:Label ID="Label2" runat="server" meta:resourcekey="ResourceGraphLabelOriginal"></asp:Label></span><b class="skill_caret"></b> </a>
                            <ul id="ddCompetencesTable" class="dropdown-menu" role="menu" aria-labelledby="drop7"
                                style="width: 100%;">
                                <li role="presentation"><a id="A1" role="menuitem" runat="server" tabindex="-1" href="#" onclick="showThisGraph('Original')">
                                    <%--  Original Competence Level--%><asp:Label ID="GraphLabelOriginal" runat="server" meta:resourcekey="ResourceGraphLabelOriginal"></asp:Label></a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#" onclick="showThisGraph('New')">
                                    <%--New Competence Level--%><asp:Label ID="GraphLabelNew" runat="server" meta:resourcekey="ResourceGraphLabelNew"></asp:Label></a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#" onclick="showThisGraph('Development')">
                                    <%--Development Points--%><asp:Label ID="GraphLabelDevelopment" runat="server" meta:resourcekey="ResourceGraphLabelDevelopment"></asp:Label></a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#" onclick="showThisGraph('Knowledge')">
                                    <%--Knowledge Share--%><asp:Label ID="GraphLabelKnowledge" runat="server" meta:resourcekey="ResourceGraphLabelKnowledge"></asp:Label></a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="LearningPoint.aspx">
                                    <%--Knowledge Share--%><asp:Label ID="GraphLabelLearningPoint" runat="server" meta:resourcekey="ResourceGraphLabelLearningPoints"></asp:Label></a></li>
                                <li role="presentation" class="divider"></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="ReportAll.aspx">
                                    <asp:Label ID="Label3" runat="server" meta:resourcekey="ResourceGraphLabelAll"></asp:Label></a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <!--<li class="range">
<a href="#">
<i class="icon-calendar"></i>
<span>August 5, 2014 - September 3, 2014</span>
<i class="icon-angle-down"></i>
</a>
</li>-->

            <div class="col-md-12">
                <div id="graph-wrapper">
                    <h3 style="margin-bottom: 0px;">
                        <div class="otherDiv" style="float: right">
                            <asp:DropDownList ID="ddlDepartment" runat="server" Style="margin-left: -30px; width: 100%; height: 32px; display: inline; margin-left: -15px; margin-bottom: 20px;"
                                CssClass="form-control" OnSelectedIndexChanged="ddlcompetence_SelectedIndexChanged"
                                AutoPostBack="True">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="ddlDepartment"
                                ErrorMessage="Please select department." InitialValue="0" CssClass="commonerrormsg"
                                Display="Dynamic" ValidationGroup="chkNewEmp"></asp:RequiredFieldValidator>
                        </div>
                        <div class="otherDiv" style="float: right">
                            <asp:DropDownList ID="ddljobtype1" runat="server" Style="margin-left: -30px; width: 100%; height: 32px; margin-bottom: 20px;"
                                CssClass="form-control" OnSelectedIndexChanged="ddljobtype_SelectedIndexChanged"
                                AutoPostBack="True">
                            </asp:DropDownList>
                            <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddljobtype"
                                                            ErrorMessage="Please select job type." InitialValue="0" CssClass="commonerrormsg"
                                                            Display="Dynamic" ValidationGroup="chkNewEmp" meta:resourcekey="RequiredFieldValidator1Resource1"></asp:RequiredFieldValidator>--%>
                        </div>

                        <div class="otherDiv" style="float: right; margin-right: 4%;">
                            <asp:DropDownList ID="ddlTeam" runat="server" Style="margin-left: 0px; margin-right: 0px; width: 100%; height: 32px; margin-bottom: 20px;"
                                CssClass="form-control" OnSelectedIndexChanged="ddlTeam_SelectedIndexChanged"
                                AutoPostBack="True">
                            </asp:DropDownList>
                        </div>

                        <div class="otherDiv" style="float: right; margin-right: 4%;">
                            <asp:HiddenField runat="server" ID="ddtTemplateVal" Value="0" meta:resourcekey="nooptionselected" />

                            <asp:DropDownList ID="managercat" runat="server" multiple="multiple">
                            </asp:DropDownList>

                            <asp:Button ID="managercatTmp" runat="server" Text="Temporart" CssClass="btns  yellow  col-md-11 libray_btn" Style="float: right; width: 130px; font-size: 17px; display: none;"
                                OnClick="managercat_Click" />
                        </div>
                    </h3>
                    <div id="Original">
                        <h3 class="custom-heading blue" style="margin-bottom: 0px;">
                            <asp:Literal ID="Originalcompetencelevel" runat="server" meta:resourcekey="Originalcompetencelevel"
                                EnableViewState="false" />


                            <asp:Button ID="btnPDF" runat="server" CommandArgument="Originalcompetencelevel"
                                CommandName="Original" Text="Export to PDF" CssClass="btns  yellow  col-md-11 libray_btn"
                                Style="float: right; width: 130px; font-size: 17px; display: none;" OnClick="btnPDF_Click" />
                        </h3>

                        <div class="col-md-12 comp_table" style="background: #fff; overflow-x: auto;">

                            <asp:Repeater runat="server" ID="rowRepeater" OnItemDataBound="rowRepeater_ItemBound">
                                <HeaderTemplate>
                                    <table width="100%" border="1" class="report_table report_table1" style="background-color: #E8E8E8; font-size: 12px; margin-top: 0;">
                                        <tr style="height: 35px;">
                                            <th>
                                                <asp:Label runat="server" ID="lblrName" Text="Name" meta:resourcekey="lblrNameResource1" />
                                            </th>

                                            <asp:Repeater runat="server" ID="headerRepeater">
                                                <ItemTemplate>
                                                    <th>
                                                        <asp:Label runat="server" ID="lblUserName" Text='<%# Eval("comCompetence") %>' meta:resourcekey="lblUserNameResource1" />
                                                        <asp:Label runat="server" ID="lblcid" Text='<%# Eval("comId") %>' Visible="False"
                                                            meta:resourcekey="lblcidResource1" />
                                                        <asp:Label ID="comId1" runat="server" Value='<%# Eval("comId") %>' Style="display: none" />
                                                        <%--udpate from hiddn to lable--%>
                                                    </th>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <th>
                                                <asp:Label runat="server" ID="lblEmployer" Text="Employer" meta:resourcekey="lblEmployerResource1" />
                                            </th>
                                            <th>
                                                <asp:Label runat="server" ID="lblheadpdf" Text="PDF" />
                                            </th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <strong>
                                                <asp:HiddenField ID="userId" runat="server" Value='<%# Eval("userId") %>' />
                                                <asp:Label ID="lblhead1" Text='<%# Eval("name") %>' runat="server" meta:resourcekey="lblhead1Resource1"></asp:Label>
                                            </strong>
                                        </td>
                                        <asp:Repeater runat="server" ID="columnRepeater">
                                            <ItemTemplate>
                                                <td>
                                                    <asp:Label ID="lblhead" Text="0" runat="server" meta:resourcekey="lblheadResource1"></asp:Label>
                                                    <asp:Label ID="Lacname" Text='<%# Eval("comCompetence") %>' runat="server" Visible="False"
                                                        meta:resourcekey="LacnameResource1"></asp:Label>
                                                    <asp:HiddenField ID="hdnComid" Value='<%#Eval("comId")%>' runat="server" />
                                                </td>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <td>
                                            <strong>
                                                <asp:Label ID="lblavg" Text="0" runat="server" meta:resourcekey="lblavgResource1"></asp:Label>
                                            </strong>
                                        </td>
                                        <td>
                                            <%-- <asp:Label ID="lblpdf" runat="server" Text="PDF"></asp:Label>--%>
                                            <%--  <asp:LinkButton ID="likpdf" runat="server" ClientIDMode="Static" OnClientClick="myFunction(<%# Eval('userId') %>);" Text="PDF1"></asp:LinkButton>--%>
                                            <%--<div id="btn" onmousemove="myclick();">--%>
                                            <div id="btn" onmousemove="myclick();">
                                                <a href="#add-post-title" id="btnpdp" data-toggle="modal" class="btn" title="" onclick='javascript:showpopup1(<%# Eval("userId") %>,this.id);'
                                                    style="display: none">
                                                    <input type="button" value="PDF" class="btn btn-success yellow"
                                                        style="padding: 0px 0px !important;" />
                                                    </button>
                                                </a>
                                                <a href="PersonalDevelopmentPlan.aspx?umId=<%# Eval("userId") %>" id="A2">
                                                    <input type="button" value="PDP" class="btn-danger"
                                                        style="padding: 0px 0px !important;" />
                                                    </button>
                                                </a>
                                            </div>
                                            <%-- <div id="btn" onmousemove="myclick();">
                                               <%-- <input type="button" value="PDF" class="btn btn-success yellow" onclick='myFunction(<%# Eval("userId") %>,this.id);'
                                                    id="btnpdp" style="padding: 0px 0px !important;" />--%>
                                            <%--</div>--%>
                                            <div id="loader" class="loader" style="margin: 0px 0px; display: none;">
                                                <img src="../images/ajax-loader.gif" />
                                            </div>
                                            <%-- <asp:Button ID="btnPDF" runat="server" CommandArgument="Originalcompetencelevel"
                                                 Text="Export to PDF" CssClass="btns  yellow  col-md-11 libray_btn"
                                                OnClick="btnPDF_Click1" />--%>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <tr class="average" style="border: none;">
                                        <td style="border: none;">
                                            <asp:Label ID="lblAverage" Text="Average" runat="server" meta:resourcekey="lblAverageResource1"></asp:Label>
                                        </td>
                                        <asp:Repeater runat="server" ID="FooterRepeater">
                                            <ItemTemplate>
                                                <td style="border: none;">
                                                    <asp:Label runat="server" ID="lblFavg" Text='<%# Eval("Local") %>' meta:resourcekey="lblFavgResource1" />
                                                </td>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <td style="border: none;">
                                            <asp:Label runat="server" ID="lblextra" meta:resourcekey="lblextraResource1" />
                                        </td>
                                        <td style="border: none;">
                                            <asp:Label runat="server" ID="Label1" />
                                        </td>
                                    </tr>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                        <div class="home_grap">

                            <div id="containerOriginal" style="min-width: 310px; height: 400px; margin: 0 auto">
                                <div id="wait" style="margin: 190px 612px">
                                    <img src="../images/wait.gif" />
                                </div>
                            </div>
                        </div>
                    </div>



                    <%-----------------------------------------------------------------------------------------------------------------Newcompetencelevel-----------------------------------------------%>





                    <div id="New" style="display: none">
                        <h3 class="custom-heading blue" style="margin-bottom: 0px;">
                            <asp:Literal ID="Newcompetencelevel" runat="server" meta:resourcekey="Newcompetencelevel"
                                EnableViewState="false" />
                            <asp:Button ID="Button1" runat="server" CommandArgument="Newcompetencelevel" CommandName="New"
                                Text="Export to PDF" CssClass="btns  yellow  col-md-11 libray_btn" Style="float: right; width: 130px; font-size: 17px; display: none;"
                                OnClick="btnPDF_Click" />
                        </h3>
                        <div class="col-md-12 comp_table" style="background: #fff; overflow-x: auto;">
                            <asp:Repeater runat="server" ID="rptnew" OnItemDataBound="rptnew_ItemBound">
                                <HeaderTemplate>
                                    <table width="100%" border="1" class="report_table" style="background-color: #E8E8E8; font-size: 12px;">
                                        <tr style="height: 35px;">
                                            <th>
                                                <asp:Label runat="server" ID="lblrName" Text="Name" meta:resourcekey="lblrNameResource2" />
                                            </th>
                                            <asp:Repeater runat="server" ID="headerRepeater">
                                                <ItemTemplate>
                                                    <th>
                                                        <asp:Label runat="server" ID="lblUserName" Text='<%# Eval("comCompetence") %>' meta:resourcekey="lblUserNameResource2" />
                                                        <asp:Label runat="server" ID="lblcid" Text='<%# Eval("comId") %>' Visible="False"
                                                            meta:resourcekey="lblcidResource2" />
                                                        <asp:HiddenField ID="comId1" runat="server" Value='<%# Eval("comId") %>' />
                                                        </td>
                                                    </th>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <th>
                                                <asp:Label runat="server" ID="lblEmployer" Text="Employer" meta:resourcekey="lblEmployerResource2" />
                                            </th>
                                            <th>
                                                <asp:Label runat="server" ID="lblheadpdf" Text="PDF" />
                                            </th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <strong>
                                                <asp:HiddenField ID="userId" runat="server" Value='<%# Eval("userId") %>' />
                                                <asp:Label ID="lblhead1" Text='<%# Eval("name") %>' runat="server" meta:resourcekey="lblhead1Resource2"></asp:Label>
                                            </strong>
                                        </td>
                                        <asp:Repeater runat="server" ID="columnRepeater">
                                            <ItemTemplate>
                                                <td>
                                                    <asp:Label ID="lblhead" Text="0" runat="server" meta:resourcekey="lblheadResource2"></asp:Label>
                                                    <asp:Label ID="Lacname" Text='<%# Eval("comCompetence") %>' runat="server" Visible="False"
                                                        meta:resourcekey="LacnameResource2"></asp:Label>
                                                    <asp:HiddenField ID="hdnComid" Value='<%#Eval("comId")%>' runat="server" />

                                                </td>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <td>
                                            <strong>
                                                <asp:Label ID="lblavg" Text="0" runat="server" meta:resourcekey="lblavgResource2"></asp:Label>
                                            </strong>
                                        </td>
                                        <td>

                                            <%-- <div id="btn" onmousemove="myclick();">
                                                <input type="button" value="PDF" class="btn btn-success yellow" onclick='myFunction(<%# Eval("userId") %>,this.id);'
                                                    id="btnpdp" style="padding: 0px 0px !important;" />
                                            </div>--%>
                                            <div id="btn" onmousemove="myclick();">
                                                <a href="#add-post-title" id="btnpdp" data-toggle="modal" class="btn" title="" onclick='javascript:showpopup1(<%# Eval("userId") %>,this.id);'>
                                                    <input type="button" value="PDF" class="btn btn-success yellow"
                                                        style="padding: 0px 0px !important; display: none" />
                                                    </button>
                                                </a>
                                                <a href="PersonalDevelopmentPlan.aspx?umId=<%# Eval("userId") %>" id="A2">
                                                    <input type="button" value="PDP" class="btn-danger"
                                                        style="padding: 0px 0px !important;" />
                                                    </button>
                                                </a>
                                            </div>
                                            <div id="loader" class="loader" style="margin: 0px 0px; display: none;">
                                                <img src="../images/ajax-loader.gif" />
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <tr class="average" style="border: none;">
                                        <td style="border: none;">
                                            <asp:Label ID="lblAverage" Text="Average" runat="server" meta:resourcekey="lblAverageResource2"></asp:Label>
                                        </td>
                                        <asp:Repeater runat="server" ID="FooterRepeater">
                                            <ItemTemplate>
                                                <td style="border: none;">
                                                    <asp:Label runat="server" ID="lblFavg" Text='<%# Eval("Achive") %>' meta:resourcekey="lblFavgResource2" />
                                                </td>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <td style="border: none;">
                                            <asp:Label runat="server" ID="lblextra" meta:resourcekey="lblextraResource2" />
                                        </td>
                                        <td style="border: none;">
                                            <asp:Label runat="server" ID="Label1" />
                                        </td>
                                    </tr>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                        <div class="home_grap">
                            <div id="ContainerNew" style="min-width: 310px; height: 400px; margin: 0 auto">
                                <div id="waitNew" style="margin: 190px 612px">
                                    <img src="../images/wait.gif" />
                                </div>
                            </div>
                        </div>
                    </div>


                    <%-----------------------------------------------------------------------------------------------------------------Developmentpoints-----------------------------------------------%>


                    <div id="Development" style="display: none">
                        <h3 class="custom-heading blue" style="margin-bottom: 0px;">
                            <asp:Literal ID="Developmentpoints" runat="server" meta:resourcekey="Developmentpoints"
                                EnableViewState="false" />
                            <asp:Button ID="btn" runat="server" CommandArgument="Developmentpoints" CommandName="Developmentpoints"
                                Text="Export to PDF" CssClass="btns  yellow  col-md-11 libray_btn" Style="float: right; width: 130px; font-size: 17px; display: none;"
                                OnClick="btnPDF_Click" />
                        </h3>
                        <div class="col-md-12 comp_table" style="background: #fff; overflow-x: auto; overflow-x: auto;">
                            <asp:Repeater runat="server" ID="rptDevelopment" OnItemDataBound="rptDevelopment_ItemBound">
                                <HeaderTemplate>
                                    <table width="100%" border="1" class="report_table" style="background-color: #E8E8E8; font-size: 12px;">
                                        <tr style="height: 35px;">
                                            <th>
                                                <asp:Label runat="server" ID="lblrName" Text="Name" meta:resourcekey="lblrNameResource3" />
                                            </th>
                                            <asp:Repeater runat="server" ID="headerRepeater">
                                                <ItemTemplate>
                                                    <th>
                                                        <asp:Label runat="server" ID="lblUserName" Text='<%# Eval("comCompetence") %>' meta:resourcekey="lblUserNameResource3" />
                                                        <asp:Label runat="server" ID="lblcid" Text='<%# Eval("comId") %>' Visible="False"
                                                            meta:resourcekey="lblcidResource3" />
                                                        <asp:HiddenField ID="comId1" runat="server" Value='<%# Eval("comId") %>' />
                                                        </td>
                                                    </th>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <th>
                                                <asp:Label runat="server" ID="lblEmployer" Text="Employer" meta:resourcekey="lblEmployerResource3" />
                                            </th>
                                            <th>
                                                <asp:Label runat="server" ID="lblheadpdf" Text="PDF" />
                                            </th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <strong>
                                                <asp:HiddenField ID="userId" runat="server" Value='<%# Eval("userId") %>' />
                                                <asp:Label ID="lblhead1" Text='<%# Eval("name") %>' runat="server" meta:resourcekey="lblhead1Resource3"></asp:Label>
                                            </strong>
                                        </td>
                                        <asp:Repeater runat="server" ID="columnRepeater">
                                            <ItemTemplate>
                                                <td>
                                                    <asp:Label ID="lblhead" Text="0" runat="server" meta:resourcekey="lblheadResource3"></asp:Label>
                                                    <asp:Label ID="Lacname" Text='<%# Eval("comCompetence") %>' runat="server" Visible="False"
                                                        meta:resourcekey="LacnameResource3"></asp:Label>
                                                    <asp:HiddenField ID="hdnComid" Value='<%#Eval("comId")%>' runat="server" />
                                                </td>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <td>
                                            <strong>
                                                <asp:Label ID="lblavg" Text="0" runat="server" meta:resourcekey="lblavgResource3"></asp:Label>
                                            </strong>
                                        </td>
                                        <td>

                                            <%--<div id="btn" onmousemove="myclick();">
                                                <input type="button" value="PDF" class="btn btn-success yellow" onclick='myFunction(<%# Eval("userId") %>,this.id);'
                                                    id="btnpdp" style="padding: 0px 0px !important;" />
                                            </div>--%>
                                            <div id="btn" onmousemove="myclick();">
                                                <a href="#add-post-title" id="btnpdp" data-toggle="modal" class="btn" title="" onclick='javascript:showpopup1(<%# Eval("userId") %>,this.id);'>
                                                    <input type="button" value="PDF" class="btn btn-success yellow"
                                                        style="padding: 0px 0px !important; display: none" />
                                                    </button>
                                                </a>
                                                <a href="PersonalDevelopmentPlan.aspx?umId=<%# Eval("userId") %>" id="A2">
                                                    <input type="button" value="PDP" class="btn-danger"
                                                        style="padding: 0px 0px !important;" />
                                                    </button>
                                                </a>

                                            </div>

                                            <div id="loader" class="loader" style="margin: 0px 0px; display: none;">
                                                <img src="../images/ajax-loader.gif" />
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <tr class="average" style="border: none;">
                                        <td style="border: none;">
                                            <asp:Label ID="lblAverage" Text="Average" runat="server" meta:resourcekey="lblAverageResource3"></asp:Label>
                                        </td>
                                        <asp:Repeater runat="server" ID="FooterRepeater">
                                            <ItemTemplate>
                                                <td style="border: none;">
                                                    <asp:Label runat="server" ID="lblFavg" Text='<%# Eval("Achive") %>' meta:resourcekey="lblFavgResource3" />
                                                    <asp:Label runat="server" ID="lblComIdFooter" Text='<%# Eval("comId") %>' Style="display: none;"
                                                        meta:resourcekey="lblComIdFooterResource1" />
                                                </td>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <td style="border: none;">
                                            <asp:Label runat="server" ID="lblextra" meta:resourcekey="lblextraResource3" />
                                        </td>
                                        <td style="border: none;">
                                            <asp:Label runat="server" ID="Label1" />
                                        </td>
                                    </tr>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                        <div class="home_grap">
                            <div id="ContainerDevelopment" style="min-width: 310px; height: 400px; margin: 0 auto">
                                <div id="waitDevelopment" style="margin: 190px 612px">
                                    <img src="../images/wait.gif" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <%---------------------------------------------------------------------------------------------Knowledgeshare---------------------------------------------%>



                    <div id="Knowledge" style="display: none">
                        <h3 class="custom-heading blue" style="margin-bottom: 0px;">
                            <asp:Literal ID="Knowledgeshare" runat="server" meta:resourcekey="Knowledgeshare"
                                EnableViewState="false" />
                            <asp:Button ID="Button2" runat="server" CommandArgument="Knowledgeshare" CommandName="Knowledgeshare"
                                Text="Export to PDF" CssClass="btns  yellow  col-md-11 libray_btn" Style="float: right; width: 130px; font-size: 17px; display: none;"
                                OnClick="btnPDF_Click" />
                        </h3>
                        <div class="col-md-12 comp_table" style="background: #fff; overflow-x: auto;">
                            <asp:Repeater runat="server" ID="rpt_Knowledge" OnItemDataBound="rpt_Knowledge_ItemBound">
                                <HeaderTemplate>
                                    <table width="100%" border="1" class="report_table" style="background-color: #E8E8E8; font-size: 12px;">
                                        <tr style="height: 35px;">
                                            <th>
                                                <asp:Label runat="server" ID="lblrName" Text="Name" meta:resourcekey="lblrNameResource2" />
                                            </th>
                                            <asp:Repeater runat="server" ID="headerRepeater">
                                                <ItemTemplate>
                                                    <th>
                                                        <asp:Label runat="server" ID="lblUserName" Text='<%# Eval("comCompetence") %>' meta:resourcekey="lblUserNameResource2" />
                                                        <asp:Label runat="server" ID="lblcid" Text='<%# Eval("comId") %>' Visible="False"
                                                            meta:resourcekey="lblcidResource2" />
                                                        <asp:HiddenField ID="comId1" runat="server" Value='<%# Eval("comId") %>' />
                                                        </td>
                                                    </th>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <th>
                                                <asp:Label runat="server" ID="lblEmployer" Text="Employer" meta:resourcekey="lblEmployerResource2" />
                                            </th>
                                            <th>
                                                <asp:Label runat="server" ID="lblheadpdf" Text="PDF" />
                                            </th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <strong>
                                                <asp:HiddenField ID="userId" runat="server" Value='<%# Eval("userId") %>' />
                                                <asp:Label ID="lblhead1" Text='<%# Eval("name") %>' runat="server" meta:resourcekey="lblhead1Resource2"></asp:Label>
                                            </strong>
                                        </td>
                                        <asp:Repeater runat="server" ID="columnRepeater">
                                            <ItemTemplate>
                                                <td>
                                                    <asp:Label ID="lblhead" Text="0" runat="server" meta:resourcekey="lblheadResource2"></asp:Label>
                                                    <asp:Label ID="Lacname" Text='<%# Eval("comCompetence") %>' runat="server" Visible="False"
                                                        meta:resourcekey="LacnameResource2"></asp:Label>
                                                    <asp:HiddenField ID="hdnComid" Value='<%#Eval("comId")%>' runat="server" />
                                                </td>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <td>
                                            <strong>
                                                <asp:Label ID="lblavg" Text="0" runat="server" meta:resourcekey="lblavgResource2"></asp:Label>
                                            </strong>
                                        </td>
                                        <td>

                                            <%-- <div id="btn" onmousemove="myclick();">
                                                <input type="button" value="PDF" class="btn btn-success yellow" onclick='myFunction(<%# Eval("userId") %>,this.id);'
                                                    id="btnpdp" style="padding: 0px 0px !important;" />
                                            </div>--%>
                                            <div id="btn" onmousemove="myclick();">
                                                <a href="#add-post-title" id="btnpdp" data-toggle="modal" class="btn" title="" onclick='javascript:showpopup1(<%# Eval("userId") %>,this.id);'>
                                                    <input type="button" value="PDF" class="btn btn-success yellow"
                                                        style="padding: 0px 0px !important; display: none" />
                                                    </button>
                                                </a>

                                                <a href="PersonalDevelopmentPlan.aspx?umId=<%# Eval("userId") %>" id="A2">
                                                    <input type="button" value="PDP" class="btn-danger"
                                                        style="padding: 0px 0px !important;" />
                                                    </button>
                                                </a>

                                            </div>
                                            <div id="loader" class="loader" style="margin: 0px 0px; display: none;">
                                                <img src="../images/ajax-loader.gif" />
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <tr class="average" style="border: none;">
                                        <td style="border: none;">
                                            <asp:Label ID="lblAverage" Text="Average" runat="server" meta:resourcekey="lblAverageResource2"></asp:Label>
                                        </td>
                                        <asp:Repeater runat="server" ID="FooterRepeater">
                                            <ItemTemplate>
                                                <td style="border: none;">
                                                    <asp:Label runat="server" ID="lblFavg" Text='<%# Eval("Knowledge_Point") %>' meta:resourcekey="lblFavgResource2" />
                                                </td>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <td style="border: none;">
                                            <asp:Label runat="server" ID="lblextra" meta:resourcekey="lblextraResource2" />
                                        </td>
                                        <td style="border: none;">
                                            <asp:Label runat="server" ID="Label1" />
                                        </td>
                                    </tr>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                        <div class="home_grap">
                            <div id="ContainerKnowledgeDevelopment" style="min-width: 310px; height: 400px; margin: 0 auto">
                                <div id="waitKnowledgeDevelopment" style="margin: 190px 612px">
                                    <img src="../images/wait.gif" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="clear: both">
                </div>
                <!-- TIME LINE -->
            </div>
        </div>
        <!-- Chat Widget -->
        <!-- Recent Post -->
        <div class="col-md-3">
        </div>
        <!-- Twitter Widget -->
        <!-- Weather Widget -->
    </div>
    <!-- Container -->
   <!-- Wrapper -->
    <!-- RAIn ANIMATED ICON-->
    <div class="home_grap" style="display: none; margin-top: 2000px;" id="chartpdp">
        <div class="graph-info">
            <a href="#" id="AlpBars"><span><i class="fa fa-bar-chart-o"></i></span></a><a href="#"
                id="Alplines" class="active"><span><i class="fa fa-code-fork"></i></span></a>
        </div>
        <div class="graph-container">
            <div id="graph-lines-alpplp">
            </div>
            <div id="graph-bars-alpplp">
            </div>
        </div>
    </div>
    <div class="home_grap" id="chart" style="display: none;">
        <div class="graph-info">
            <a href="#" id="PDPBars"><span><i class="fa fa-bar-chart-o"></i></span></a><a href="#"
                id="PDPlines" class="active"><span><i class="fa fa-code-fork"></i></span></a>
        </div>
        <div class="graph-container">
            <div id="graph-lines-pdp">
            </div>
            <div id="graph-bars-pdp">
            </div>
        </div>
    </div>

    <asp:HiddenField runat="server" ID="PDPImgBase64" Value="asdc" />
    <asp:HiddenField runat="server" ID="alpImgBase64" Value="sef" />

    <asp:HiddenField runat="server" ID="hdnPdfUserId" />

    <asp:HiddenField runat="server" ID="hdnPdfButtonId" />

    <div aria-hidden="true" role="dialog" tabindex="-1" class="modal fade" id="add-post-title"
        style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header blue yellow-radius">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button" onclick="ShowOriginal();">
                        ×
                    </button>
                    <h4 class="modal-title">
                        <asp:Literal ID="Literal6" runat="server" meta:resourcekey="resourcelblConclusion" EnableViewState="false" />
                        <%-- <%= CommonMessages.AddNewEmployee%>--%>
                    </h4>
                </div>

                <div class="modal-body">
                    <div class="otherDiv">
                        <asp:Literal ID="Literal3" runat="server" meta:resourcekey="resourcelblConclusionOpt" EnableViewState="false" />
                        <asp:TextBox runat="server" placeholder="Conclusion" ID="txtConclusion" MaxLength="3000" TextMode="MultiLine" Rows="10" Columns="80" />

                    </div>


                </div>

                <div class="modal-footer" id="submitButtons">

                    <button data-dismiss="modal" class="btn btn-primary black" type="button" onclick="myFunction();">
                        <%--  <%= CommonMessages.Close%>--%><asp:Literal ID="Literal8" meta:resourcekey="resourcebtnAddConclusion" runat="server" Text="Add"
                            EnableViewState="false" />
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>

    </div>
    <%-- </ContentTemplate>
    </asp:UpdatePanel>--%>
    <!-- Container -->
    <%-- </div>--%><!-- Wrapper -->
    <!-- RAIn ANIMATED ICON-->
    <%--Chart--%>
    <script type="text/javascript" src="js/chart-line-and-graph.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script src="../Scripts/Scripts/exporting.js" type="text/javascript"></script>

    <script src="../Scripts/jquery321.js" type="text/javascript"></script>
    <script src="../assets/js/multiselect.js" type="text/javascript"></script>
    <script src="../Scripts/highcharts.js" type="text/javascript"></script>
    <script src="../Scripts/jsPDF-min.js" type="text/javascript"></script>
    <%-- Use for Chart--%>
    <%-- <script src="../Scripts/chartCat.js" type="text/javascript"></script>--%>
    <script src="../Scripts/flotJs.js" type="text/javascript"></script>
    <script src="../Scripts/chartCat.js" type="text/javascript"></script>
    <%-- <script src="http://mrrio.github.io/jsPDF/dist/jspdf.min.js" type="text/javascript"></script>--%>
    <script src=" https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"
        type="text/javascript"></script>


    <script type="text/javascript">
        $(document).ready(function () {


            $('#ContentPlaceHolder1_managercat').multiselect({
                //enableFiltering: true,
                includeSelectAllOption: true,
                //buttonWidth: '400px',
                selectAllText: 'All Competence',
                //  //nonSelectedText: 'None Location selected',
                allSelectedText: 'All Competence',
                //  nSelectedText: 'selected',
                //  numberDisplayed: 2,
                buttonText: function (options) {
                    var labels = [];
                    var num = [];

                    if (options.length === 0) {
                        sessionStorage.setItem("multi", "0");
                        $('#ContentPlaceHolder1_ddtTemplateVal').val('0');
                        return $('#ContentPlaceHolder1_hdnNoOption').val();
                    }
                    options.each(function () {
                        if ($(this).attr('value') !== undefined) {
                            labels.push($(this).text());
                            num.push($(this).val());
                        }
                    });

                    $('#ContentPlaceHolder1_ddtTemplateVal').val(num.join(','));

                    return labels.join(', ');
                },
                onChange: function (event) {
                    // alert($('#ContentPlaceHolder1_ddtTemplateVal').val());
                    sessionStorage.setItem("multi", $('#ContentPlaceHolder1_ddtTemplateVal').val());
                    $('#ContentPlaceHolder1_managercatTmp').click();
                }
            });
            var curTab = sessionStorage.getItem("multi");

            //alert('curTab' + curTab);

            if (curTab != null && curTab != '') {
                var dataarray = curTab.split(",");

                $("#ContentPlaceHolder1_managercat").val(dataarray);
                $("#ContentPlaceHolder1_managercat").multiselect("refresh");
                sessionStorage.setItem("multi", "0");
            }

            setTimeout(function () {
                $('.multiselect-container').css('height', '500px');
                $('.multiselect-container').css('overflow', 'scroll');
            }, 500);

        });
    </script>
    <script type="text/javascript">
        function SetSesssionVariableValue() {
            sessionStorage.setItem("multi", "0");
        }
    </script>
    <script type="text/javascript">

        function DropDownLableChange() {
            $("#ddCompetencesTable li a").click(function () {
                $("#ddLabel").text($(this).text());
                var text = $('#<%=Label2.ClientID %>').text();
                var subtext = $('#<%=GraphSubTitle.ClientID %>').val();
                var AxisText = $('#<%=GraphAxisLevel.ClientID %>').val();
                ToolTipText = $('#<%=GraphToolTip.ClientID %>').val();

                if (subtext == "") {
                    subtext = "Click and drag mouse arrow to zoom chart";
                }
                if (text == "") {
                    text = $("#ddLabel").text();
                    if (text == "") {
                        text = "Original Competence Level";
                    }
                }
                if (AxisText == "") {

                    text = "Level";
                }
                if (ToolTipText == "") {

                    ToolTipText = "Level";
                }

                $(".highcharts-title").text(text);
                $(".highcharts-subtitle").text(subtext);
                $(".highcharts-yaxis-title").text(AxisText);
                $(".spanText").text(ToolTipText);
            });

            $("#ddCompetencesTable li a").click(function () { $(".highcharts-title").text($(this).text()); });


        }

        function showDivByQueryString() {

            var url = window.location.href;
            var dd = GetQueryParameterByName("dd", url);

            if (dd == "o" || dd == "o#") {
                dd = "Original";
                $('#<%=Label2.ClientID %>').text($("#ddCompetencesTable li a:eq(0)").text().trim());
                $("#ddLabel").text($("#ddCompetencesTable li a:eq(0)").text());
            } else if (dd == "d" || dd == "d#") {
                dd = "Development";
                $('#<%=Label2.ClientID %>').text($("#ddCompetencesTable li a:eq(2)").text().trim());
                $("#ddLabel").text($("#ddCompetencesTable li a:eq(2)").text());
            } else if (dd == "n" || dd == "n#") {
                dd = "New";
                $('#<%=Label2.ClientID %>').text($("#ddCompetencesTable li a:eq(1)").text().trim());
                $("#ddLabel").text($("#ddCompetencesTable li a:eq(1)").text());
            } else if (dd == "k" || dd == "k#") {
                dd = "Knowledge";
                $('#<%=Label2.ClientID %>').text($("#ddCompetencesTable li a:eq(3)").text().trim());
                $("#ddLabel").text($("#ddCompetencesTable li a:eq(3)").text());
            }

    showThisGraph(dd);
}

function showThisGraph(id) {

    if (id == "Original") {

        $("#Original").fadeIn();
        $("#New").fadeOut();
        $("#Development").fadeOut();
        $("#Knowledge").fadeOut();
    }
    else if (id == "New") {

        $("#Original").fadeOut();
        $("#New").fadeIn();
        $("#Development").fadeOut();
        $("#Knowledge").fadeOut();
        drwaChartNewCompLevel();
    }
    else if (id == "Development") {

        $("#Original").fadeOut();
        $("#New").fadeOut();
        $("#Development").fadeIn();
        $("#Knowledge").fadeOut();
        drwaChartDevelopmentCompLevel();
    }
    else if (id == "Knowledge") {

        $("#Original").fadeOut();
        $("#New").fadeOut();
        $("#Development").fadeOut();
        $("#Knowledge").fadeIn();
        drwaKnowledgeDevelopment();
    }
}

var icons = new Skycons();
icons.set("rain", Skycons.RAIN);
icons.play();
var ToolTipText;

$(document).ready(function () {
    SetExpandCollapse();
    // ExampanCollapsMenu();
    showDivByQueryString();
    DropDownLableChange();
    setTimeout(function () {
        var text = $('#<%=Label2.ClientID %>').text();
        var subtext = $('#<%=GraphSubTitle.ClientID %>').val();
        var AxisText = $('#<%=GraphAxisLevel.ClientID %>').val();
        ToolTipText = $('#<%=GraphToolTip.ClientID %>').val();

        if (subtext == "") {
            subtext = "Click and drag mouse arrow to zoom chart";
        }
        if (text == "") {
            text = $("#ddLabel").text();
            if (text == "") {
                text = "Original Competence Level";
            }
        }
        if (AxisText == "") {

            text = "Level";
        }
        if (ToolTipText == "") {

            ToolTipText = "Level";
        }

        $(".highcharts-title").text(text);
        $(".highcharts-subtitle").text(subtext);
        $(".highcharts-yaxis-title").text(AxisText);
        $(".spanText").text(ToolTipText);
    }, 3000);
});


var doc = new jsPDF();
var specialElementHandlers = {
    '#editor': function (element, renderer) {
        return true;
    }
};

$('#cmd').click(function () {
    doc.fromHTML($('#ContentPlaceHolder1_div1').html(), 15, 15, {
        'width': 170,
        'elementHandlers': specialElementHandlers
    });
    doc.save('sample-file.pdf');
});
    </script>
    <script>


        function ShowOriginal() {
            $(".btn").show();
            $(".loader").hide();
            document.getElementById('<%=txtConclusion.ClientID %>').value = "";
        }

        function showpopup1(id, btnid) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Report.aspx/GetConclusion",
                data: "{'id':'" + id + "'}",
                dataType: "json",
                success: function (data) {
                    var obj = data.d;
                    if (obj != '') {
                        document.getElementById('<%=txtConclusion.ClientID %>').value = data.d;
                    }
                    else {
                        document.getElementById('<%=txtConclusion.ClientID %>').value = "";
                    }
                },
                error: function (result) {
                    document.getElementById('<%=txtConclusion.ClientID %>').value = "";
                }
            });

                document.getElementById('<%=hdnPdfUserId.ClientID %>').value = id;
            document.getElementById('<%=hdnPdfButtonId.ClientID %>').value = btnid;
        }



        function myclick() {
            $(".btn").click(function () {
                $(this).hide();
                $(this).parent().next().show();
            });
        }

        //function myFunction(id, btnid) {
        function myFunction() {
            var id = document.getElementById('<%=hdnPdfUserId.ClientID %>').value;
            var btnid = document.getElementById('<%=hdnPdfButtonId.ClientID %>').value;
            var conclusion = document.getElementById('<%=txtConclusion.ClientID %>').value;
            $('#chartpdp').show();
            $('#chart').show();
            setTimeout(function () {
                DrawChartAlpPLpPDP(id);
            }, 1000);

            setTimeout(function () {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Report.aspx/Getdata",
                    data: "{'id':'" + id + "','pdp':'" + PDPImgBase64 + "','apl':'" + alpImgBase64 + "','conclusion':'" + conclusion + "'}",
                    dataType: "json",
                    success: function (data) {
                        var obj = data.d;
                        if (obj != '') {
                            $(".loader").hide();
                            $(".btn").show();
                            $('#chartpdp').hide();
                            $('#chart').hide();
                            window.open(data.d, "_blank");
                        }
                    },
                    error: function (result) {
                        alert("Error");
                        $(".loader").hide();
                        $(".btn").show();
                    }
                });
            }, 4000);

        }

        /*2015-02-19 |GetTotalAlpPlpGroupByMonthByUserId|*/
        /*Function to get data and draw chart for employee skill by month |Saurin |20150219*/

        function DrawChartAlpPLpPDP(userId) {
            //alert(userId);
            //This two function will draw chart for particular user..
            GetTotalAlpPlpGroupByMonthByUserId(userId);
            PDPdataByMonthForPdfReport(userId);
            //Now convert chart to IMAGE
            setTimeout(function () {
                //GetChartImage("#graph-lines-pdp");
                GetChartImage("#graph-bars-pdp"); // Pass Id Which is used for chart DIV :        

                //GetChartImage("#graph-lines-alpplp");
                GetChartImage("#graph-bars-alpplp");
            }, 0);

        }



        var alpImgBase64 = "";
        var PDPImgBase64 = "";
        function GetChartImage(GraphID) {

            /* || Saurin || 20150220*/
            /*include this two script on page where u need to apply this function. 
            "http://mrrio.github.io/jsPDF/dist/jspdf.min.js  // <--- optional where need to  save chart in pdf format
              
            and  https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js    */
            //GraphID = "#graph-linesEmployee";


            html2canvas($(GraphID).get(0), {
                onrendered: function (canvas) {
                    //document.body.appendChild(canvas);

                    var imgData = canvas.toDataURL('image/png');
                    // alert(imgData);
                    // if (GraphID == "#graph-lines-pdp") {
                    if (GraphID == "#graph-bars-pdp") {
                        PDPImgBase64 = imgData;

                    } else if (GraphID == "#graph-bars-alpplp") {
                        alpImgBase64 = imgData;

                    }

                    return;

                    //                    console.log('Report Image URL: ' + imgData);
                    //                    var doc = new jsPDF('landscape');

                    //                    doc.addImage(imgData, 'PNG', 10, 10, 190, 10);
                    //                    doc.save('c:/sample-file.jpeg');
                }
            });

        }
        var alpPlpDataByMonth;
        function GetTotalAlpPlpGroupByMonthByUserId(dataNeedsOf) {

            // ie. : >>  var checkVideo = CommonAJSON("VideoList.asmx/IsItABadVideo", '{watchID:"' + watchID + '",platform:"Web"}'); 
            var ReturnData;
            $.ajax({
                type: "POST",
                async: false,
                url: "webservice1.asmx/GetTotalAlpPlpGroupByMonthByUserId",
                data: "{'dataNeedsOf':'" + dataNeedsOf + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#divResult").html(msg.d);
                    alpPlpDataByMonth = msg.d;

                    setTimeout(function () {
                        DrawChartAlpPlpByMonth();
                        drwaGraphAlpPlpByMonth();
                        ForAlpPlpGraph1();
                    }, 0);

                    //return msg.d;

                },
                error: function (e) {
                    $("#divResult").html("WebSerivce unreachable");
                }
            });

            //  alert("aaa");
        }
        function DrawChartAlpPlpByMonth() {
            //var data1111a = dataa;
            var data1 = [["January", 10], ["February", 8], ["March", 4], ["April", 13], ["May", 17], ["June", 9]];
            var data11 = [["dfgr", 1], ["comp 10", 4], ["test", 5], []];
            var data1111 = [["dfgr", 2], ["comp 10", 3], ["test", 1], []];
            var x = [["dfgr", 2], ["comp 10", 3], ["test", 1], []];


            var Achive = JSON.parse(alpPlpDataByMonth[1]);
            var Local = JSON.parse(alpPlpDataByMonth[0]);
            // var target = JSON.parse(data[2]);
            var graphData = [{
                // Visits
                label: "PLP Points",
                data: Local,
                color: '#a6d87a'
            }, {
                // Returning Visits
                label: "ALP Points",
                data: Achive,
                color: '#282828',
                points: { radius: 4, fillColor: '#ffffff' }
            }
            ];



            setTimeout(function () {
                $.plot("#graph-lines-alpplp", graphData, {
                    series: {
                        points: {
                            show: true,
                            radius: 5
                        },
                        lines: {
                            show: true
                        },
                        shadowSize: 0
                    },
                    grid: {
                        color: '#282828',
                        borderColor: 'transparent',
                        borderWidth: 60,
                        hoverable: true
                    },
                    xaxis: {
                        mode: "categories",
                        tickLength: 0
                    }
                });
            }, 0);

        }
        function drwaGraphAlpPlpByMonth() {

            var Achive = JSON.parse(alpPlpDataByMonth[1]);
            var Local = JSON.parse(alpPlpDataByMonth[0]);

            var graphData = [{
                label: "PLP Points",
                data: Local,
                color: '#a6d87a'
            }, {
                label: "ALP Points",
                data: Achive,
                color: '#282828',
                points: { radius: 4, fillColor: '#282828' }
            }
            ];
            $.plot($('#graph-bars-alpplp'), graphData, {
                series: {
                    bars: {
                        show: true,
                        barWidth: .7,
                        align: 'center'
                    },
                    shadowSize: 0
                },
                grid: {

                    borderColor: 'transparent',
                    borderWidth: 40,
                    hoverable: true
                },
                xaxis: {
                    mode: "categories",
                    tickLength: 0
                }
            });

        }
        function ForAlpPlpGraph1() {
            $('#graph-bars-alpplp').show();

            $('#Alplines').on('click', function (e) {
                $('#AlpBars').removeClass('active');
                $('#graph-bars-alpplp').fadeOut();
                $(this).addClass('active');
                $('#graph-lines-alpplp').fadeIn();
                e.preventDefault();
            });

            $('#AlpBars').on('click', function (e) {
                $('#Alplines').removeClass('active');
                $('#graph-lines-alpplp').fadeOut();
                $(this).addClass('active');
                $('#graph-bars-alpplp').fadeIn().removeClass('hidden');
                e.preventDefault();
            });

            var previousPoint = null;

            $('#graph-lines-alpplp, #graph-bars-alpplp').bind('plothover', function (event, pos, item) {
                if (item) {
                    if (previousPoint != item.dataIndex) {
                        previousPoint = item.dataIndex;
                        $('#tooltip').remove();
                        var x = item.datapoint[0],
                            y = item.datapoint[1];
                        showTooltip(item.pageX, item.pageY, ' Point ' + y);
                    }
                } else {
                    $('#tooltip').remove();
                    previousPoint = null;
                }
            });

        }
        /*2015-02-19 |GetTotalAlpPlpGroupByMonthByUserId|*/
        var PDPdataByMonth;
        function PDPdataByMonthForPdfReport(dataNeedsOf) {

            // ie. : >>  var checkVideo = CommonAJSON("VideoList.asmx/IsItABadVideo", '{watchID:"' + watchID + '",platform:"Web"}'); 
            var ReturnData;
            $.ajax({
                type: "POST",
                async: false,
                url: "webservice1.asmx/GetPDPpointByUserIdByMonth",
                data: "{'dataNeedsOf':'" + dataNeedsOf + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#divResult").html(msg.d);
                    PDPdataByMonth = msg.d;

                    setTimeout(function () {
                        DrawChartPDPByMonth();
                        drwaGraphPDPByMonth();
                        ForPDPGraphByMonth();
                    }, 0);

                    //return msg.d;

                },
                error: function (e) {
                    $("#divResult").html("WebSerivce unreachable");

                }
            });

            //  alert("aaa");
        }

        function ForPDPGraphByMonth() {

            $('#graph-bars-pdp').show();

            $('#PDPlines').on('click', function (e) {
                $('#PDPBars').removeClass('active');
                $('#graph-bars-pdp').fadeOut();
                $(this).addClass('active');
                $('#graph-lines-pdp').fadeIn();
                e.preventDefault();
            });

            $('#PDPBars').on('click', function (e) {
                $('#PDPlines').removeClass('active');
                $('#graph-lines-pdp').fadeOut();
                $(this).addClass('active');
                $('#graph-bars-pdp').fadeIn().removeClass('hidden');
                e.preventDefault();
            });

            var previousPoint = null;

            $('#graph-lines-pdp, #graph-bars-pdp').bind('plothover', function (event, pos, item) {
                if (item) {
                    if (previousPoint != item.dataIndex) {
                        previousPoint = item.dataIndex;
                        $('#tooltip').remove();
                        var x = item.datapoint[0],
                            y = item.datapoint[1];
                        showTooltip(item.pageX, item.pageY, ' Point ' + y);
                    }
                } else {
                    $('#tooltip').remove();
                    previousPoint = null;
                }
            });

        }
        function DrawChartPDPByMonth() {
            //var data1111a = dataa;


            var data1 = [["January", 10], ["February", 8], ["March", 4], ["April", 13], ["May", 17], ["June", 9]];
            var data11 = [["dfgr", 1], ["comp 10", 4], ["test", 5], []];
            var data1111 = [["dfgr", 2], ["comp 10", 3], ["test", 1], []];
            var x = [["dfgr", 2], ["comp 10", 3], ["test", 1], []];


            var Achive = JSON.parse(PDPdataByMonth[0]);

            // var target = JSON.parse(data[2]);
            var graphData = [{
                // Returning Visits
                label: "PDP Points",
                data: Achive,
                color: '#282828',
                points: { radius: 4, fillColor: '#ffffff' }
            }
            ];



            setTimeout(function () {
                $.plot("#graph-lines-pdp", graphData, {
                    series: {
                        points: {
                            show: true,
                            radius: 5
                        },
                        lines: {
                            show: true
                        },
                        shadowSize: 0
                    },
                    grid: {
                        color: '#282828',
                        borderColor: 'transparent',
                        borderWidth: 60,
                        hoverable: true
                    },
                    xaxis: {
                        mode: "categories",
                        tickLength: 0
                    }
                });
            }, 0);

        }
        function drwaGraphPDPByMonth() {

            var Achive = JSON.parse(PDPdataByMonth[0]);


            var graphData = [{
                label: "PDP Points",
                data: Achive,
                color: '#282828',
                points: { radius: 4, fillColor: '#282828' }
            }
            ];
            $.plot($('#graph-bars-pdp'), graphData, {
                series: {
                    bars: {
                        show: true,
                        barWidth: .7,
                        align: 'center'
                    },
                    shadowSize: 0
                },
                grid: {

                    borderColor: 'transparent',
                    borderWidth: 40,
                    hoverable: true
                },
                xaxis: {
                    mode: "categories",
                    tickLength: 0
                }
            });

        }


    </script>
</asp:Content>
