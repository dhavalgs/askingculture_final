﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true" CodeFile="CompetenceSkill.aspx.cs" Inherits="Organisation_CompetenceSkill"
    Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/ajaxtab.css" rel="stylesheet" />
    <style type="text/css">
        .treeNode {
        }

            .treeNode input {
                width: auto;
                margin: 5px;
                float: left !important;
            }

        .chat-widget-head {
            float: left;
            width: 100%;
        }
    </style>
    <style type="text/css">
        body {
            font-family: Arial;
            font-size: 11pt;
        }

        .modalBackground {
            background-color: Black;
            filter: alpha(opacity=40);
            opacity: 0.4;
        }

        .modalPopup1 {
            background-color: #FFFFFF;
            width: 300px;
            border: 3px solid #0DA9D0;
        }

        .modalPopup {
            background: none repeat scroll 0 0 #ffffff;
            display: table;
            float: none;
            margin: 96px auto;
            width: 600px !important;
            top: 10px !important;
        }

            .modalPopup .header {
                background-color: #2FBDF1;
                height: 30px;
                color: White;
                line-height: 30px;
                text-align: center;
                font-weight: bold;
            }

            .modalPopup .body {
                min-height: 50px;
                line-height: 30px;
                text-align: center;
                padding: 5px;
            }

            .modalPopup .footer {
                padding: 3px;
            }

            .modalPopup .button {
                height: 23px;
                color: White;
                line-height: 23px;
                text-align: center;
                font-weight: bold;
                cursor: pointer;
                background-color: #9F9F9F;
                border: 1px solid #5C5C5C;
            }

            .modalPopup td {
                text-align: left;
            }

        .button {
            background-color: transparent;
        }

        .yellow {
            border-radius: 0px;
        }

        .reply {
            clear: both;
        }

        #scrollbox6 {
            overflow-x: hidden;
            overflow-y: auto;
        }
    </style>
    <%-- <style type="text/css">
        .accordion-header, .accordion-selected
        {
            width: 100%;
            background-color: #e6e6e6;
            margin-bottom: 2px;
            padding: 2px;
            color: #444444;
            font-weight: bold;
            cursor: pointer;
        }
        
        .accordion-content
        {
            width: 100%;
            margin-bottom: 2px;
            padding: 2px;
        }
        
        .accordion-selected, .accordion-content
        {
            border: solid 1px #666666;
        }
    </style>--%>
    <style>
        .HighlightComp {
            background-color: antiquewhite;
            color: red;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" meta:resourcekey="lblMsgResource1"></asp:Label>
    <asp:HiddenField runat="server" ID="hdnSaveMessage" meta:resourcekey="SaveMessageResource" Value="Please wait,We are processing to save competences." />
    <asp:HiddenField runat="server" ID="hdnNo" meta:resourcekey="NoResource" Value="No" />
    <asp:HiddenField runat="server" ID="hdnYes" meta:resourcekey="YesResource" Value="Yes" />
    <asp:HiddenField runat="server" ID="hdnNorecord" meta:resourcekey="NoRecordResource" />

    <div class="container">
        <div class="col-md-6">
            <div class="heading-sec">
                <h1>
                    <%-- <%= CommonMessages.Competence%> Skills--%>
                    <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Competence" EnableViewState="false" />
                    <i><span runat="server" id="Skills"></span></i>

                </h1>
            </div>
        </div>
        <div class="col-md-6">
            <div class="heading-sec">
                <h1>
                    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanedrop">
                        <ProgressTemplate>

                            <div class="center">
                                <i class="fa fa-spinner fa-spin fa-pulse fa-3x fa-fw margin-bottom"></i>
                            </div>

                        </ProgressTemplate>
                    </asp:UpdateProgress>

                    <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="updlocal">
                        <ProgressTemplate>

                            <div class="center">
                                <i class="fa fa-spinner fa-spin fa-pulse fa-3x fa-fw margin-bottom"></i>
                            </div>

                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </h1>
            </div>
        </div>
    </div>

    <div class="col-md-12">

        <asp:UpdatePanel runat="server" ID="UpdatePanedrop" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="alert alert-info fade in" id="errorDiv" runat="server" visible="False">
                    <i class="icon-remove close" data-dismiss="alert"></i>
                    <asp:Label runat="server" ID="lblErrorMsg"></asp:Label>

                </div>
                <div class="col-md-5">

                    <div class="col-md-12" style="padding-bottom: 22px; display: none;">
                        <div class="dropdown-example">
                            <ul class="nav nav-pills">
                                <li class="dropdown" style="float: left!important; width: 100%;">

                                    <asp:DropDownList ID="ddlcompetence" runat="server" Class="skill_dropdown" Style="float: left!important; width: 100%; background-color: #02406d; color: White; -webkit-appearance: none;"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlcompetence_SelectedIndexChanged">
                                    </asp:DropDownList>

                                </li>

                            </ul>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="chat-widget widget-body" style="border-radius: 5px;">
                            <div class="chat-widget-head yellow yellow-radius">
                                <h4>
                                    <%--  <%= CommonMessages.Competence%>--%>
                                    <asp:Literal ID="Literal2" runat="server" meta:resourcekey="lblcompetence" EnableViewState="false" /></h4>
                            </div>

                            <div class="col-md-12" style="padding: 0!important;">
                                <asp:HiddenField ID="RequestSentSuccessful" Value="Request sent successfully" meta:resourcekey="RequestSentSuccess" runat="server" />
                                <asp:Repeater ID="rep_competence" runat="server" OnItemDataBound="R1_ItemDataBound"
                                    OnItemCommand="Repeater1_OnItemCommand">
                                    <HeaderTemplate>
                                        <table width="100%" border="1" bordercolor="#dfdfdf" class="skill_table" id="tblContacts">
                                            <thead>
                                                <th style="text-align: left">
                                                    <%-- <%= CommonMessages.Competence%>--%>
                                                    <asp:Literal ID="Literal2" runat="server" meta:resourcekey="lblcompetenceres" EnableViewState="false" />
                                                </th>
                                                <th style="text-align: left; display: none;">
                                                    <%-- <%= CommonMessages.value%>--%>
                                                    <asp:Literal ID="Literal3" runat="server" meta:resourcekey="value" EnableViewState="false" />
                                                </th>
                                                <th style="text-align: left; display: none;">
                                                    <%--<%= CommonMessages.check%>--%>
                                                    Set
                                                </th>
                                                <th style="text-align: left">
                                                    <%-- <%= CommonMessages.Local%>--%>
                                                    <asp:Literal ID="Literal4" runat="server" meta:resourcekey="Local" EnableViewState="false" />
                                                </th>
                                                <th style="text-align: left">
                                                    <%-- <%= CommonMessages.Achieve%>--%>
                                                    <asp:Literal ID="Literal5" runat="server" meta:resourcekey="Achieve" EnableViewState="false" />
                                                </th>
                                                <th style="text-align: left">
                                                    <%-- <%= CommonMessages.Help%>--%>
                                                    <asp:Literal ID="Literal12" runat="server" meta:resourcekey="Target" EnableViewState="false" />

                                                </th>
                                                <th style="text-align: left">
                                                    <%-- Set--%>
                                                    <%--   <%= CommonMessages.Set%>--%>
                                                    <asp:Literal ID="Literal6" runat="server" meta:resourcekey="Set" EnableViewState="false" />
                                                </th>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr class='<%# Convert.ToString(Session["newcomId"])==Convert.ToString(Eval("ComId"))?"HighlightComp":""%>'>
                                            <td width="49%" style="text-align: left">
                                                <%--  <a href="">--%>
                                                <%
                                                   
                                                    if (Convert.ToString(Session["Culture"]).Trim().ToLower() == Convert.ToString("Danish").Trim().ToLower())
                                                    {%>

                                                <asp:LinkButton ID="lnkCatName1" runat="server" Text='<%#Eval("comCompetenceDN" )%>'
                                                    CommandName="catNameClick" OnClientClick="return GetcommentById1(this);" CommandArgument='<%#Eval("ComId") %>'></asp:LinkButton>

                                                <% }
                                                    else
                                                    {
                                                %>
                                                <asp:LinkButton ID="lnkCatName" runat="server" Text='<%#Eval("comCompetence" )%>'
                                                    CommandName="catNameClick" OnClientClick="return GetcommentById1(this);" CommandArgument='<%#Eval("ComId") %>'></asp:LinkButton>
                                                <% } %>

                                                <%--<asp:Label ID="lblsubcom" runat="server" Text='<%# Eval("comCompetence")%>'  OnCommand="Image_Click" CommandName="ImageClick" CommandArgument='<%# Eval("ComId") %> ></asp:Label>--%>
                                                <%-- </a>--%>
                                                <asp:HiddenField ID="comId" runat="server" Value='<%#Eval("comId") %>' />
                                                <asp:HiddenField ID="comCompetence" runat="server" Value='<%# Eval("comCompetence") %>' />
                                            </td>
                                            <td width="13%" style="display: none; text-align: left">
                                                <asp:Label ID="lbpoint" runat="server" Text="0"></asp:Label>
                                                <asp:Label ID="lblcomId" runat="server" Text='<%#Eval("comId") %>'></asp:Label>
                                            </td>
                                            <td width="13%" style="display: none; text-align: left">
                                                <%--<form name="form1" method="post" action="">--%>
                                                <label>
                                                    <%-- <input type="checkbox" name="checkbox" value="checkbox" onclick="CheckBoxCheck(this);" id="checkbox1"/>--%>
                                                    <asp:CheckBox ID="checkbox1" runat="server" onclick="CheckBoxCheck(this);" />
                                                </label>
                                                <%--</form>--%>
                                            </td>
                                            <td width="13%" style="text-align: left">
                                                <%--<asp:Button runat="server" ID="btnlocal" Text="L" CssClass="yellow" Style="border-radius: 100%;
                                                    height: 25px; width: 25px; color: white;" CommandName="Local" CommandArgument='<%# Eval("comId") %>' />--%>
                                                <asp:Label ID="lbllocal" runat="server" Text="0"></asp:Label>
                                                <%-- OnClick="btnlocal_click"--%>
                                            </td>
                                            <td width="15%" style="text-align: left">
                                                <asp:Label ID="lblAchive" runat="server" Text="0"></asp:Label>
                                            </td>
                                            <td width="12%" style="text-align: left">
                                                <asp:Label ID="lbltarget" runat="server" Text="0"></asp:Label>
                                            </td>
                                            <td width="12%" style="text-align: left">
                                                <asp:Label ID="lblset" runat="server" Text="NO" meta:resourcekey="lblNoResource"></asp:Label>
                                                <asp:HiddenField ID="hdnSet" runat="server" Value="NO" />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <tr id="Emptyrow" runat="server" visible="false">
                                            <td>
                                                <asp:Label ID="lblEmptyData" Text="No Data To Display" runat="server">
                                                </asp:Label>
                                            </td>
                                        </tr>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>

                                <asp:Repeater ID="rep_competencehigh" runat="server" OnItemDataBound="rep_competencehigh_ItemDataBound"
                                    OnItemCommand="rep_competencehigh_OnItemCommand" Visible="false">
                                    <HeaderTemplate>
                                        <table width="100%" border="1" bordercolor="#dfdfdf" class="skill_table" id="tblContacts1">
                                            <thead>
                                                <th style="text-align: left" colspan="6">
                                                    <%--  <%= CommonMessages.NeedHelp%>--%>
                                                    <asp:Literal ID="Literal6" runat="server" meta:resourcekey="NeedHelp" EnableViewState="false" />
                                                </th>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td width="42%">
                                                <%--  <a href="">--%>
                                                <asp:LinkButton ID="lnkCatName" runat="server" Text='<%#Eval("comCompetence" )%>'
                                                    CommandName="catNameClick" CommandArgument='<%#Eval("ComId") %>'></asp:LinkButton>
                                                <%--<asp:Label ID="lblsubcom" runat="server" Text='<%# Eval("comCompetence")%>'></asp:Label>--%>
                                                <%-- </a>--%>
                                                <asp:HiddenField ID="comId" runat="server" Value='<%# Eval("comId") %>' />
                                            </td>
                                            <td width="13%">
                                                <asp:Label ID="lbpoint" runat="server" Text="0"></asp:Label>
                                            </td>
                                            <td width="13%">
                                                <%--<form name="form1" method="post" action="">--%>
                                                <label>
                                                    <%-- <input type="checkbox" name="checkbox" value="checkbox" onclick="CheckBoxCheck(this);" id="checkbox1"/>--%>
                                                    <asp:CheckBox ID="checkbox1" runat="server" onclick="CheckBoxCheck(this);" />
                                                </label>
                                                <%--</form>--%>
                                            </td>
                                            <td width="12%">
                                                <asp:Button runat="server" ID="btnlocal" Text="L" CssClass="yellow" Style="border-radius: 100%; height: 25px; width: 25px; color: white;"
                                                    CommandName="Local" CommandArgument='<%# Eval("comId") %>'
                                                    OnClientClick="ExpandCollapse()" />
                                                <%-- OnClick="btnlocal_click"--%>
                                            </td>
                                            <td width="11%">
                                                <asp:Button runat="server" ID="btmAll" Text="A" CssClass="yellow" Style="border-radius: 100%; height: 25px; width: 25px; color: white;"
                                                    CommandName="All" CommandArgument='<%# Eval("comId") %>' /><%-- OnClick="btmAll_click""--%>
                                            </td>
                                            <td>
                                                <asp:Button runat="server" ID="btnhelp" Text="H" CssClass="yellow" Style="border-radius: 100%; height: 25px; width: 25px; color: white;"
                                                    OnClick="btnhelp_click" CommandName="help"
                                                    CommandArgument='<%# Eval("comId") %>' />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <tr id="Emptyrow" runat="server" visible="false">
                                            <td>
                                                <asp:Label ID="lblEmptyData" Text="No Data To Display" runat="server">
                                                </asp:Label>
                                            </td>
                                        </tr>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>

                                <div style="clear: both">
                                </div>

                            </div>

                            <div class="modal-footer" style="border-top: 0px !important;">
                                <asp:Button runat="server" ID="btnsetsave" meta:resourcekey="savecompetence" Text="Save Competence" OnClientClick="GenerateMessage();" CssClass="btn btn-primary yellow"
                                    OnClick="btnsetsave_click" Style="margin: 4px;" />
                                <%--<asp:Button runat="server" OnClientClick="return history.back()" meta:resourcekey="back" CssClass="btn black pull-right" Style="border-radius: 5px; margin-left: 0px; margin-top: 5px; float: right;" Text="Back" />--%>
                                <a href="myCompetence.aspx" class="btn black pull-right" style="border-radius: 5px; margin-left: 0px; margin-top: 5px; float: right;">
                                    <asp:Label runat="server" meta:resourcekey="btnbackres"></asp:Label></a>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- Resualt-->
                <asp:Timer ID="Timer1" runat="server" OnTick="Timer1_Tick" Interval="1000" Enabled="false">
                </asp:Timer>
                <div class="col-md-3" style="margin-top: 0px; background: #f4f4f4; float: left; border-radius: 5px; margin-left: -15px;">
                    <div id="tabmenu">
                        <asp:HiddenField runat="server" ID="hdnSetPoint" Value="false"></asp:HiddenField>

                        <ul id="nav1">

                            <asp:Repeater ID="rep_tabmenu" runat="server" OnItemDataBound="rep_tabmenu_ItemDataBound">
                                <ItemTemplate>
                                    <li><a id="name" runat="server" href="#">
                                        <%# Eval("no")%></a>
                                        <asp:HiddenField ID="no" runat="server" Value='<%# Eval("no") %>' />
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                        <div id="tab-content">
                            <asp:Repeater ID="rep_content" runat="server" OnItemCommand="rep_content_OnItemCommand"
                                OnItemDataBound="rep_content_ItemDataBound">
                                <HeaderTemplate>
                                    <p>
                                        <%# Eval("comCompetence")%>
                                    </p>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div id="tab1" runat="server" class="tab17" style="display: none; overflow: auto; width: 100%; height: auto;">
                                        <h2 class="step_titel" style="z-index: 0 !important">

                                            <p>
                                                <%# Eval("comCompetence")%>
                                            </p>

                                            <asp:Literal runat="server" ID="lblLevel" meta:resourcekey="LabelResource" Text="'s Level"></asp:Literal>

                                            <%# Eval("no")%>
                                            <asp:HiddenField ID="no" runat="server" Value='<%# Eval("no") %>' />
                                        </h2>
                                        <%-- <div class="sub_competencie" style="display: block !important; overflow: auto; width: 100%; height: auto;">--%>

                                        <p>
                                            <%#
                                                    
                                                         string.Format("{0}", Eval("comChildLevel")!=null ?Eval("comChildLevel").ToString().Replace("<div>", "").Replace("</div>", ""):"")
                                            %>
                                        </p>
                                        <%-- <asp:Label ID="lblLeval" Text=' <%# Eval("comChildLevel")%>' runat="server"></asp:Label>--%>

                                        <asp:HiddenField ID="hdnComId" runat="server" Value='<%# Eval("comId") %>' />
                                        <asp:HiddenField ID="comchildId" runat="server" Value='<%# Eval("comchildId") %>' />

                                        <%--  </div>--%>
                                        <br />
                                        <asp:Button runat="server" ID="btnset" meta:resourcekey="setcompetence" Text="Set" CssClass="yellow" Style="height: 30px; width: 20%; float: right; color: white; position: absolute; border-radius: 21%; /*left: 80%; */ margin-top: 136px;"
                                            CommandName="Set"
                                            CommandArgument='<%# Eval("comId") %>' OnClientClick=" return insertcomment(); generateTimer('warning','Please save competence after set competence.','bottomCenter','5000')" />
                                    </div>
                                    <input type="hidden" id="hdncom" value='<%# Eval("comId") %>' />
                                </ItemTemplate>
                            </asp:Repeater>
                            <asp:TextBox runat="server" CssClass="form-control" placeholder="Please enter comments." meta:resourcekey="txtcommentResource" TextMode="MultiLine" ID="txtComment"></asp:TextBox>
                            <asp:TextBox runat="server" CssClass="form-control txtDummy" placeholder="Please enter comments." TextMode="MultiLine" ID="txtCommentDummy" Style="display: none"></asp:TextBox>
                            <a href="#" onclick="return insertcomment();" class="yellow"
                                style="height: 30px; margin-top: 32px; margin-left: 10px; width: 40%; float: right; color: white; border-radius: 13%; padding-top: 4px; text-align: center;">
                                <asp:Label ID="lblcomment" runat="server" meta:resourcekey="Comment"></asp:Label></a>


                            <%--<asp:LinkButton ID='<%# Eval("comId") %>' runat="server" CommandName="Comment"  CommandArgument='<%# Eval("comId") %>'
                                                            ToolTip="Comment" OnClientClick="return GetmessgeById(this);">Comment</asp:LinkButton>--%>

                            <%--  <asp:Button runat="server" ID="btnset" Text="Set" CssClass="yellow" Style="height: 30px; margin-top: 32px;
                                            width: 41px;float: right; color: white; border-radius: 21%;" CommandName="Set"
                                            CommandArgument='<%# Eval("comId") %>' OnClientClick="generateTimer('warning','Please save competence after set competence.','bottomCenter','5000')" />--%>
                        </div>
                        <%-- </ContentTemplate>--%>
                        <%--   <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlcompetence" />
                    </Triggers>--%>
                        <%-- </asp:UpdatePanel>--%>
                    </div>
                    <!-- TIME LINE -->
                    <!-- Recent Post -->
                </div>


            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="Timer1" />
            </Triggers>
        </asp:UpdatePanel>
        <div class="col-md-4">
            <div id="pnlcomment" class="chat-widget widget-body" style="border-radius: 5px;">
                <div class="chat-widget-head yellow yellow-radius">
                    <h4>
                        <%--  <%= CommonMessages.Competence%>--%>
                        <asp:Literal ID="Literal11" runat="server" meta:resourcekey="Comment" EnableViewState="false" /></h4>
                </div>

                <div class="col-md-12" style="padding: 0!important;">



                    <div class="modal-body">
                        <ul id="scrollbox6">
                            <%--<li class="reply">No Record Found. </li>--%>
                            <li class="reply"></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6" style="margin-top: 20px; border-radius: 5px; background: #fff; display: none;">
            <div class="home_grap" style="border-radius: 5px;">
                <div id="graph-wrapper">
                    <h3 class="custom-heading" style="color: #000;">
                        <%-- <%= CommonMessages.EmployeeSkills%>--%>
                        <asp:Literal ID="Literal6" runat="server" meta:resourcekey="EmployeeSkills" EnableViewState="false" /></h3>
                    <div class="graph-info">
                        <a href="#" id="bars"><span><i class="fa fa-bar-chart-o"></i></span></a><a href="#"
                            id="lines" class="active"><span><i class="fa fa-code-fork"></i></span></a>
                    </div>
                    <div class="graph-container">
                        <div id="graph-lines">
                        </div>
                        <div id="graph-bars">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%--<div class="modal-footer" style="border: 0px; width: 50%;">
            <%-- <button class="btn btn-primary yellow" type="button">
                Save Changes</button>--%>
        <%--<asp:Button runat="server" ID="btnsubmit" Text="Submit" CssClass="btn btn-primary yellow"
                OnClick="btnsubmit_click" />
        </div>--%>
        <%--div class="modal-footer" style="border: 0px; margin-left: -54%; margin-top: -27%;">
          
            <asp:Button runat="server" ID="Button1" Text="Save Changes" CssClass="btn btn-primary yellow"
                ValidationGroup="chk" OnClick="btnsubmit_click" />
        </div>--%>
        <!-- Container -->
    </div>
    <!-- Wrapper -->
    <!-- RAIn ANIMATED ICON-->

    <asp:UpdatePanel runat="server" ID="updlocal">
        <ContentTemplate>
            <asp:LinkButton Text="" ID="lnkFake" runat="server" />
            <cc1:ModalPopupExtender ID="mpe" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkFake"
                CancelControlID="btnClose" BackgroundCssClass="modalBackground">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="margin-left: 170px !important; width: 17% !important; position: absolute !important; z-index: 100001 !important; top: 117px !important; display: none;">
                <div class="modal-header blue" style="color: #ffffff;">
                    <h4 class="modal-title">
                        <%-- <%= CommonMessages.Competence%>--%>
                        <asp:Literal ID="Literal7" runat="server" meta:resourcekey="Competence" EnableViewState="false" />
                    </h4>
                    <div style="float: right; margin-top: -26px;">
                        <asp:Button ID="btnClose" runat="server" Text="×" CssClass="close" />
                    </div>
                </div>
                <div class="modal-body">
                    <%--<table border="0" cellpadding="3" cellspacing="0">
                <tr>
                    <td >--%>
                    <asp:Repeater ID="rptlocal" runat="server" OnItemDataBound="rptlocal_ItemDataBound"
                        OnItemCommand="rptlocal_ItemCommand">
                        <HeaderTemplate>
                            <table width="100%" border="1" bordercolor="#dfdfdf" class="skill_table" id="tblContacts">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td width="49%">
                                    <asp:Label ID="lblsubcom" runat="server" Text='<%# Eval("userFirstName")%>'></asp:Label>
                                    <asp:HiddenField ID="userId" runat="server" Value='<%# Eval("userId") %>' />
                                    <asp:HiddenField ID="pintSubCompetance" runat="server" Value='<%# Eval("pintSubCompetance") %>' />
                                    <asp:HiddenField ID="hdnhelpstatus" runat="server" Value='<%# Eval("helpstatus") %>' />
                                    <asp:HiddenField ID="userFirstName" runat="server" Value='<%# Eval("userFirstName") %>' />
                                </td>
                                <td width="13%" id="tdControl" runat="server">
                                    <asp:LinkButton ID="lnkpoint" runat="server" Text='<%# Eval("pointanscount")%>' CommandArgument='<%# Eval("userId") %>'
                                        ForeColor="Black" CommandName="popupcal"></asp:LinkButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            <tr id="Emptyrow" runat="server" visible="false">
                                <td>
                                    <asp:Label ID="lblEmptyData" Text="No Data To Display" runat="server">
                                    </asp:Label>
                                </td>
                            </tr>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <%--</td>
                </tr>
                
            </table>--%>
                </div>
            </asp:Panel>
            <%--  <asp:LinkButton ID="lnkFakecomment" runat="server" meta:resourcekey="lnkFakeResource1" />--%>
            <%--  <cc1:ModalPopupExtender ID="mpecomment" runat="server" PopupControlID="pnlcomment" TargetControlID="lnkFakecomment"
                CancelControlID="btnClose" BackgroundCssClass="modalBackground" DynamicServicePath=""
                Enabled="True">
            </cc1:ModalPopupExtender>--%>

            <asp:LinkButton Text="" ID="LinkButton1" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" PopupControlID="Panel1"
                TargetControlID="LinkButton1" CancelControlID="btnClose" BackgroundCssClass="modalBackground">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" Style="margin-left: 170px !important; width: 17% !important; position: absolute !important; z-index: 100001 !important; top: 117px !important; display: none;">
                <div class="modal-header blue" style="color: #ffffff;">
                    <h4 class="modal-title">
                        <%-- <%= CommonMessages.Competence%> --%>
                        <asp:Literal ID="Literal8" runat="server" meta:resourcekey="Competence" EnableViewState="false" /></h4>
                    <div style="float: right; margin-top: -26px;">
                        <asp:Button ID="Button2" runat="server" Text="×" CssClass="close" />
                    </div>
                </div>
                <div class="modal-body">
                    <%--<table border="0" cellpadding="3" cellspacing="0">
                <tr>
                    <td >--%>
                    <asp:Repeater ID="rpllocalhigh" runat="server" OnItemCommand="rpllocalhigh_ItemCommand"
                        OnItemDataBound="rpllocalhigh_ItemDataBound">
                        <HeaderTemplate>
                            <table width="100%" border="1" bordercolor="#dfdfdf" class="skill_table" id="tblContacts">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td width="49%">
                                    <asp:Label ID="lblsubcom" runat="server" Text='<%# Eval("userFirstName")%>'></asp:Label>
                                    <asp:HiddenField ID="userId" runat="server" Value='<%# Eval("userId") %>' />
                                    <asp:HiddenField ID="pintSubCompetance" runat="server" Value='<%# Eval("pintSubCompetance") %>' />
                                    <asp:HiddenField ID="hdnhelpstatus" runat="server" Value='<%# Eval("helpstatus") %>' />
                                    <asp:HiddenField ID="userFirstName" runat="server" Value='<%# Eval("userFirstName") %>' />
                                </td>
                                <td width="13%" id="tdControl" runat="server">
                                    <asp:LinkButton ID="lnkpoint" runat="server" Text='<%# Eval("pointanscount")%>' CommandArgument='<%# Eval("userId") %>'
                                        ForeColor="Black" CommandName="popupcal"></asp:LinkButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            <tr id="Emptyrow" runat="server" visible="false">
                                <td>
                                    <asp:Label ID="lblEmptyData" Text="No Data To Display" runat="server">
                                    </asp:Label>
                                </td>
                            </tr>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <%--</td>
                </tr>
                
            </table>--%>
                </div>

            </asp:Panel>
            <asp:LinkButton Text="" ID="lnkcalpopup" runat="server" />
            <cc1:ModalPopupExtender ID="mpecal" runat="server" PopupControlID="pnlPopupcal" TargetControlID="lnkcalpopup"
                CancelControlID="btnClosecal" BackgroundCssClass="modalBackground">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlPopupcal" runat="server" CssClass="modalPopup" Style="z-index: 100001 !important; top: 117px !important; display: none;">
                <div class="modal-header blue" style="color: #ffffff;">
                    <h4 class="modal-title">
                        <%-- <%= CommonMessages.Competence%>--%>
                        <asp:Literal ID="Literal9" runat="server" meta:resourcekey="Competence" EnableViewState="false" /></h4>
                    <div style="float: right; margin-top: -26px;">
                        <asp:Button ID="btnClosecal" runat="server" Text="×" CssClass="close" />
                    </div>
                </div>
                <div class="s-body">
                    <table border="0" cellpadding="3" cellspacing="0" width="100%">
                        <tr>
                            <%--<td style="width: 80px; padding-bottom: 25px;">
                        <b>Name: </b>
                    </td>--%>
                            <td style="padding-left: 25px;">
                                <asp:TextBox runat="server" placeholder="Name" ID="txtcname" MaxLength="50" Style="border: 1px solid #FFF; float: left; font-family: open sans; font-size: 15px; margin-bottom: 20px; padding: 10px 13px; width: 100%; transition: all 0.4s ease 0s;" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtcname"
                                    ErrorMessage="Please enter Name." CssClass="commonerrormsg" Display="Dynamic"
                                    ValidationGroup="chhk"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left: 25px;">
                                <asp:TextBox runat="server" Text="" ID="txtfromdate" MaxLength="10" placeholder="Date"
                                    Style="border: 1px solid #FFF; float: left; font-family: open sans; font-size: 15px; margin-bottom: 20px; padding: 10px 13px; width: 100%; transition: all 0.4s ease 0s;" />
                                <cc1:CalendarExtender ID="CalendarExtender2" PopupPosition="TopLeft" runat="server"
                                    TargetControlID="txtfromdate" Format="dd/MM/yyyy">
                                </cc1:CalendarExtender>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtfromdate"
                                    ErrorMessage="Please select Job Type." InitialValue="0" CssClass="commonerrormsg"
                                    Display="Dynamic" ValidationGroup="chhk"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left: 25px;">
                                <asp:TextBox runat="server" placeholder="Message" ID="txtmsg" TextMode="MultiLine"
                                    Style="border: 1px solid #FFF; float: left; font-family: open sans; font-size: 15px; margin-bottom: 20px; padding: 10px 13px; width: 100%; transition: all 0.4s ease 0s;"
                                    Width="100%" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtmsg"
                                    ErrorMessage="Please enter Message." CssClass="commonerrormsg" Display="Dynamic"
                                    ValidationGroup="chhk"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left: 25px;"></td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <input type="button" onclick="myFunction()" id="btnSend" value="Save" class="btn btn-primary yellow" />
                    <button data-dismiss="modal" class="btn btn-default black" type="button">
                        <%-- <%= CommonMessages.Close%>--%>
                        <asp:Literal ID="Literal10" runat="server" meta:resourcekey="Close" EnableViewState="false" />
                    </button>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script>
        var icons = new Skycons();
        icons.set("rain", Skycons.RAIN);
        icons.play();
    </script>
    <script src="../assets/assets/js/libs/jquery-1.10.2.min.js"></script>
    <script type="text/javascript">

        function GenerateMessage() {
            generate('warning', $("#ContentPlaceHolder1_hdnSaveMessage").val(), 'bottomCenter');
        }

        function insertcomment() {

            var message = $("#ContentPlaceHolder1_txtComment").val().trim();
            var dupText = $(".txtDummy").text();


            var comid = document.getElementById("hdncom").value;
            if (message == dupText) {
                GetcommentById(comid);
                return;
            } else {
                $(".txtDummy").text(message);
            }
            if (comid != '0') {
                if (message != '') {

                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "CompetenceSkill.aspx/insertmessage",
                        data: "{'message':'" + message + "','comid':'" + comid + "'}",
                        dataType: "json",
                        success: function (data) {
                            var obj = data.d;

                            if (obj == 'true') {
                                $("#<%=txtComment.ClientID%>").val("");
                                $("#txtComment").val("");
                                $("#ContentPlaceHolder1_rep_content_txtComment_0").val("");
                                // GetmessgeById(touserid);
                                //GetmessgeById_afterinsert(touserid)
                                GetcommentById(comid);
                            }
                        },
                        error: function (result) {
                            // alert("Error");
                        }
                    });
                }

                else {

                    GetcommentById(comid);
                }

            }

        }

        function GetcommentById(a) {

            var comid = document.getElementById("hdncom").value;

            var html = "";
            $("#scrollbox6").html('');
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "CompetenceSkill.aspx/GetcommentById",
                data: "{'comId':'" + comid + "'}",
                dataType: "json",
                success: function (data) {
                    cat = data.d;
                    for (var i = 0; i < data.d.length; i++) {
                        var cls = "reply";

                        html += "<li class='" + cls + "'><div class='chat-thumb'><img src='../Log/upload/Userimage/" + data.d[i].Image + "' alt='' onerror='../Organisation/images/sign-in.jpg' /></div>";
                        html += "<div class='chat-desc'><p>" + data.d[i].compcomment + "</p><i class='chat-time'>" + data.d[i].comcreatedate + "</i>";
                        html += "</div></li>";
                    }
                    //setInterval(function () {
                    //    GetmessgeById_afterinsert(touserid);
                    //}, 10000);

                    if (data.d.length === 0) {

                        html += $("#ContentPlaceHolder1_hdnNorecord").val();
                        //html += "No records found.";
                    }
                    $("#scrollbox6").append(html);
                    /*var modalPopup = $find('<mpecomment.ClientID %>');
  
                          if (modalPopup != null) {
  
                              modalPopup.show();
  
                          }*/
                },
                error: function (result) {
                    //alert("Error");

                }
            });
        }

        function GetcommentById1(a) {

            var comid = $("#" + a.id).next('input[type=hidden]').val();
            var html = "";
            $("#scrollbox6").html('');
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "CompetenceSkill.aspx/GetcommentById",
                data: "{'comId':'" + comid + "'}",
                dataType: "json",
                success: function (data) {
                    cat = data.d;
                    for (var i = 0; i < data.d.length; i++) {
                        var cls = "reply";

                        html += "<li class='" + cls + "'><div class='chat-thumb'><img src='../Log/upload/Userimage/" + data.d[i].Image + "' alt='' onerror='../Organisation/images/sign-in.jpg' /></div>";
                        html += "<div class='chat-desc'><p>" + data.d[i].compcomment + "</p><i class='chat-time'>" + data.d[i].comcreatedate + "</i>";
                        html += "</div></li>";
                    }
                    //setInterval(function () {
                    //    GetmessgeById_afterinsert(touserid);
                    //}, 10000);

                    if (data.d.length === 0) {

                        html += $("#ContentPlaceHolder1_hdnNorecord").val();
                        //html += "No records found.";
                    }
                    $("#scrollbox6").append(html);
                    /*var modalPopup = $find('<mpecomment.ClientID %>');
  
                          if (modalPopup != null) {
  
                              modalPopup.show();
  
                          }*/
                },
                error: function (result) {
                    //alert("Error");

                }
            });
        }

        $(document).ready(function () {
            SetExpandCollapse();
            setTimeout(function () {
                $(".active").click();
                // tabMenuClick();
                $(".myCompetence").addClass("active_page");

                GetcommentById(<%Convert.ToString(Session["comId"]);%>)
                //insertcomment();
                //    ExampanCollapsMenu();

            }, 500);
        });
        function onLoad() {

            setTimeout(function () {
                // ExampanCollapsMenu();
                //GetcommentById(<%Convert.ToString(Session["comId"]);%>)
                // tabMenuClick();
                $(".myCompetence").addClass("active_page");
                // insertcomment();
            }, 500);
        }
        //        function SetTabFirstButtonActive() { setTimeout(function () { $("#nav1 a").first().addClass("active"); }, 500); }
        function SetTabFirstButtonActive() {
            setTimeout(function () {
                $("#nav1 a").first().addClass("active");
                $('.active').click();
            }, 500);
        }


        function Validate(sender, args) {
            //debugger
            var gridView = document.getElementById("ContentPlaceHolder1_UpdatePanedrop");
            var checkBoxes = gridView.getElementsByTagName("input");
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {
                    args.IsValid = true;
                    return;
                }
            }
            args.IsValid = false;

        }

        function myFunction() {
            var txtto = $('#<%=txtcname.ClientID %>').val();
            var txtsubject = $('#<%=txtfromdate.ClientID %>').val();
            var txtmessge = $('#<%=txtmsg.ClientID %>').val();

            if (txtto != '' && txtsubject != '' && txtmessge != '') {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Employee_Skill.aspx/UpdateData",
                    data: "{'txtto':'" + txtto + "','txtsubject':'" + txtsubject + "','txtmessge':'" + txtmessge + "'}",
                    dataType: "json",
                    success: function (data) {
                        var obj = data.d;
                        if (obj == 'true') {
                            $('#<%=txtcname.ClientID %>').val('');
                            $('#<%=txtfromdate.ClientID %>').val('');
                            $('#<%=txtmsg.ClientID %>').val('');
                            generate('warning', '<b>Save Successfully!</b>', 'bottomCenter');
                            //alert('Save SuccessFully');
                            location.reload();
                            // mpe.Show();
                            self.location.assign(location)
                            //document.getElementById('btnClosecal').click();
                        }
                    },
                    error: function (result) {
                        alert("Error");
                    }
                });
            }
            else {
                alert('Please enter all the fields');
                return false;
            }

        }



        function CheckBoxCheck(rb) {
            //debugger;
            var gv = document.getElementById('<%=UpdatePanedrop.ClientID%>');
            // var row = rb.parentNode.parentNode;
            var rbs = gv.getElementsByTagName("input");
            for (var i = 0; i < rbs.length; i++) {
                if (rbs[i].type == "checkbox") {
                    if (rbs[i].checked && rbs[i] != rb) {
                        rbs[i].checked = false;
                        break;
                    }
                }
            }
        }

        //saurin | 20150103 
        function tabMenuClickSaurin() {


            var indexer;

            $(".active").click();
            $('#tabmenu #nav1 li').click(function () {

                indexer = $(this).index();
                //alert(indexer);
                $(this).addClass("active");
                var countz = $('#tabmenu #nav1 li').length;

                //alert(countz);
                for (var i = 0; i < countz; i++) {
                    //$('#tab-content div:eq(' + i + ')').hide();

                    $("#ContentPlaceHolder1_rep_content_tab1_" + i).hide();


                }
                setTimeout(function () {
                    $("#ContentPlaceHolder1_rep_content_tab1_" + indexer).fadeIn();
                    //$('#tab-content div:eq(' + indexer + ')').show();
                }, 500);

            });

            $('#tabmenu #nav1 li a').click(function () {

                $('#tabmenu #nav1 li a').removeClass("active");
                jQuery(this).addClass("active");
            });
            // $('#tab-content div').hide();

            $(".active").click();
            //            setTimeout(function() {
            //               
            //            }, 2000);
        }

        function tabMenuClick() {
            var indexer;

            $(".active").click();
            $('#tabmenu #nav1 li').click(function () {

                indexer = $(this).index();

                //alert(indexer);
                $(this).addClass("active");
                var countz = $('#tabmenu #nav1 li').length;

                //alert(countz);
                for (var i = 0; i < countz; i++) {
                    $('#tab-content div:eq(' + i + ')').hide();
                }
                setTimeout(function () {

                    $('#tab-content div:eq(' + indexer + ')').show();
                }, 500);
            });

            $('#tabmenu #nav1 li a').click(function () {

                $('#tabmenu #nav1 li a').removeClass("active");
                jQuery(this).addClass("active");
            });
            // $('#tab-content div').hide();

            $(".active").click();
            //            setTimeout(function() {
            //               
            //            }, 2000);
        }

    </script>
</asp:Content>
