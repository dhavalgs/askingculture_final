﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master" AutoEventWireup="true" CodeFile="ForumAll.aspx.cs"
    Inherits="Organisation_ForumAll" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">


    <style>
        #notificationWidget {
            display: none;
        }

        #userActionWidget {
            display: none;
        }

        #searchWidget {
            display: none;
        }
    </style>


    <link href="css/forumStyle.css" rel="stylesheet" />

    <!--jsp:include page="/jsp/includePulseFiles.jsp" flush = "true"/-->

    <link href="../assets/assets/css/plugins/jquery-ui.css" rel="stylesheet" />

    <%--<script src="../assets/assets/js/libs/jquery-1.10.2.min.js"></script>--%>
    <link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/themes/start/jquery-ui.css" />
    <script src="../plugins/bootstrap-inputmask/jquery.inputmask.min.js" type="text/javascript"></script>

   <%-- <link href="../Scripts/fullcalendor/dATEPICKER/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <script src="../Scripts/fullcalendor/dATEPICKER/bootstrap-datetimepicker.min.js"></script>--%>

    <script type="text/javascript">

        $(document).ready(function () {

            SetExpandCollapse();

            //DatePickerActivate();

        });

        function DatePickerActivate() {

            var jq = $.noConflict();
            jq(".datepicker1").datepicker({
                //minDate: 0,
                changeMonth: true,
                changeYear: true,
                // numberOfMonths: 1,
                dateFormat: "dd/mm/yy"
                //onClose: function (selectedDate) {
                //    jq(".datepicker12").datepicker("option", "minDate", selectedDate);
                //    jq("#jobpickdatehidden1").val(selectedDate);
                //}
            });
            jq(".datepicker12").datepicker({

                changeMonth: true,
                changeYear: true,
                //  numberOfMonths: 1,
                dateFormat: "dd/mm/yy"
                //onClose: function (selectedDate) {
                //    jq(".datepicker1").datepicker("option", "maxDate", selectedDate);
                //    jq("#jobdropdatehidden1").val(selectedDate);

                //}
            });
        }
        function GetAdvanceSettings() {
            //DatePickerActivate();
            var clas = $(".searchResultBox").attr("class");

            $(".filterBox").slideToggle(1000);
            if (clas.indexOf("col-md-12") >= 0) {
                // debugger;
                $(".searchResultBox").removeClass("col-md-12");
                $(".searchResultBox").addClass("col-md-8");

                //$(".filterBox").show();
                searchToggle = false;

            } else {
                searchToggle = true;
                //$(".filterBox").hide();
                $(".searchResultBox").removeClass("col-md-8");
                $(".searchResultBox").addClass("col-md-12");
            }
            //return true;
            //$.confirm({
            //    title: 'Advance Filter ', content: $("#Ul1").html(), //autoClose: 'cancel|10000',
            //    //confirm: function () {
            //    //   // alert('confirmed');
            //    //},
            //    columnClass: 'col-md-4 col-md-offset-10',
            //    cancel: function () {
            //        //alert('canceled');
            //    }
            //});

        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:HiddenField ID="hdnSelectCategoty" runat="server" meta:resourcekey="drpSelectCategoty"></asp:HiddenField>
    <asp:HiddenField ID="hdnSelectCompetence" runat="server" meta:resourcekey="drpSelectCompetence"></asp:HiddenField>

    <asp:HiddenField ID="hdnSelectPerson" runat="server" meta:resourcekey="drpSelectPerson"></asp:HiddenField>
    <asp:HiddenField ID="hdnSelectDivision" runat="server" meta:resourcekey="drpSelectDivision"></asp:HiddenField>
    <asp:HiddenField ID="hdnSelectJob" runat="server" meta:resourcekey="drpSelectJob"></asp:HiddenField>

    <div class="col-md-6">
        <div class="heading-sec">
            <h1>  <asp:Literal ID="Literal1" runat="server" meta:resourcekey="ForumList" Text="Forum List" enableviewstate="false"/> <i><span runat="server" id="Conatct"></span></i>
            </h1>
        </div>
    </div>
    <div class="col-md-6">
        <%--<div class="heading-sec">
            <h1>Dashboard  <i>Welcome To Bette Bayer!</i></h1>
        </div>--%>
        <div class="heading-seca add_forum">
            <asp:Button runat="server" ID="btnsubmit" class="btn btn-primary yellow"
                Text="Add New Forum" PostBackUrl="ForumNewTopic.aspx"
                Style="border-radius: 5px;"
                meta:resourcekey="btnsubmitResource1"></asp:Button>
        </div>
    </div>


    <%--   <div class="col-md-12">
        <div class="chat-widget widget-body" style="background: #fff;">
            <div class="chat-widget-head yellow yellow-radius">
                <h4 style="margin: -5px 0px 0px;">
                     Forum List
                </h4>
            </div>--%>




    <!--Main Menu-->
    <div class="wrapper" style="background: white; border-radius: 5px; margin-top: 15px;">

        <div class="topicListCont" style="height: 800px;">

            <div class="topicListHeader" id="sListHeader" style="border-bottom: 0px; padding: 0 0 5px;">
                <div class="ulNoStyle ndbreadCrumbNav" istypeurl="no" id="topicListHeaderCont" style="overflow: hidden;">
                    <div class="col-md-12">
                        <div class="col-md-5">
                            <ul class="ulNoStyle flLeft">
                                <li class="mediumHeaderNew">
                                    <h2>
                                        <asp:Label runat="server" ID="Label2" CssClass="" meta:resourcekey="Topics"></asp:Label>
                                    </h2>
                                </li>
                            </ul>

                            <ul class="ulNoStyle flLeft">
                                <li class="dottedArrow"></li>
                            </ul>

                            <asp:UpdatePanel runat="server" ID="UpdatePanellist" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <ul class="ulNoStyle flLeft ndbreadCrumb" id="forumComboCont" forumid="-1" categoryid="-1" forniceurl="" hashpurpose="Home" fetchpage="1" style="display: none;">
                                        <li class="ndbreadItem">

                                            <a class="ndallForumFilter" hashpurpose="Home" fetchpage="1">

                                                <asp:Label runat="server" ID="Label3" CssClass="" meta:resourcekey="Category"></asp:Label>
                                                : 
                                                <asp:DropDownList runat="server" ID="ddForumCat"
                                                    OnSelectedIndexChanged="ddForumCat_OnSelectedIndexChanged" AutoPostBack="True"
                                                    meta:resourcekey="ddForumCatResource1" />

                                                <span class="ndcomboDownArrow" parentcont="#forumComboCont"></span>
                                            </a>


                                        </li>

                                    </ul>
                                    </div>
                 <div class="col-md-3" style="float: right; margin-right: 10px;">
                 </div>

                                </ContentTemplate>
                                <%--<Triggers>
                        <asp:AsyncPostBackTrigger ControlID="txtsearch" />
                    </Triggers>--%>
                            </asp:UpdatePanel>
                            <div style="display: none">
                                <asp:Literal ID="ResetFilter" runat="server" meta:resourcekey="ResetFilter" />
                            </div>


                            <div class="col-md-4" style="">

                                <div class="form-group" style="clear: both; margin-top: 8px;">
                                    <div class="col-md-12">
                                        <button class="btn btn-primary black" onclick="GetAdvanceSettings(); return false;">
                                            <asp:Label ID="AdvanceSettings" runat="server" meta:resourcekey="AdvanceSettings" />

                                        </button>
                                        <asp:Button runat="server" ID="btnresetfilter" Text="<%# ResetFilter.Text %>" OnClick="btnresetfilter_OnClick" CssClass="btn btn-primary black"
                                            CausesValidation="False"
                                            Style="float: right; margin-bottom: 10px;"></asp:Button>
                                        <%--  <asp:Label runat="server" ID="Label2" CssClass="" meta:resourcekey="lbllavel" Style="color: white; font-size: 19px; margin-top: 4px;"></asp:Label>--%>
                                    </div>
                                    <div class="col-md-8">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <asp:TextBox ID="txtsearch" class="library_search" runat="server" AutoPostBack="True"
                                    OnTextChanged="txtsearch_TextChanged" meta:resourcekey="txtsearchResource1"></asp:TextBox>

                            </div>

                        </div>

                        <!-- FILTER NAVIGATION BTN AND LIST-->

                    </div>

                    <ul class="ndfilterContainer ulNoStyle" id="innerTabFilter" style="display: none">
                        <ul class="flLeft ulNoStyle nditem">
                            <li filterby="All" purpose="filterTab" onclick="">
                                <a href="#" onclick="">
                                    <span>All</span>

                                </a>
                            </li>
                        </ul>


                        <ul class="flLeft ulNoStyle nditem">
                            <li purpose="filterTab" class="flLeft ">
                                <a filterby="Announcement" href="#">
                                    <span>Announcements</span>
                                </a>
                            </li>
                        </ul>

                        <ul class="flLeft ulNoStyle nditem">
                            <li purpose="filterTab" class="flLeft ">
                                <a filterby="Discussions" href="#">
                                    <span>Discussions</span>
                                </a>
                            </li>
                        </ul>

                        <ul class="flLeft ulNoStyle nditem">
                            <li purpose="filterTab" id="QuestionsFilterMenuList" class="flLeft ">
                                <a filterby="Questions" href="#">
                                    <span>Questions</span>
                                    <span class="ndcomboDownArrow"></span>
                                </a>
                                <ul id="questionsStatusList" class="ndmenuContainer ulNoStyle" filtertype="Questions">
                                    <li purpose="filterTabMenu" filterparent="QuestionsFilterMenuList" labelname="nostatus">
                                        <a href="">No Status
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>

                        <ul class="flLeft ulNoStyle nditem">
                            <li purpose="filterTab" id="IdeasFilterMenuList" class="flLeft ">
                                <a filterby="Ideas" href="#">
                                    <span>Ideas</span>
                                    <span class="ndcomboDownArrow"></span>
                                </a>
                                <ul filtertype="Ideas" id="ideasStatusList" class="ulNoStyle ndmenuContainer">
                                    <li purpose="" filterparent="IdeasFilterMenuList" labelname="nostatus">
                                        <a href="#">No Status
                                        </a>
                                    </li>


                                </ul>
                            </li>
                        </ul>

                        <ul class="flLeft ulNoStyle nditem">
                            <li purpose="filterTab" id="ProblemsFilterMenuList" class="flLeft ">
                                <a filterby="Problems" onclick="" href="#">
                                    <span>Problems</span>
                                    <span class="ndcomboDownArrow"></span>
                                </a>
                                <ul class="ulNoStyle ndmenuContainer" filtertype="Problems" id="problemsStatusList">
                                    <li purpose="filterTabMenu" filterparent="ProblemsFilterMenuList" labelname="nostatus">
                                        <a href="#">No status
                                        </a>
                                    </li>

                                </ul>
                            </li>
                        </ul>

                        <ul class="flLeft ulNoStyle nditem">
                            <li purpose="filterTab" class="flLeft " onclick="">
                                <a filterby="UnrepliedPosts" href="#">
                                    <span>Unreplied posts</span>
                                </a>
                            </li>
                        </ul>

                        <ul class="flRight ulNoStyle nditem">
                            <li purpose="filterTab" filterby="twitter" id="searchTwitterTab" forumid="-1" style="color: #2C539A; font-weight: bold; border: 0pt none; padding: 0pt 9px 0pt 32px; margin-right: -3px; margin-top: -1px; float: right;">

                                <a filterby="twitter" purpose="" href="#"></a><%--Point--%>
                            </li>
                        </ul>

                    </ul>

                </div>



                <div id="topicList">
                    <div class="ndtopicsItemsContainer">
                        <div class="ndwrapperIn">
                            <%-- <asp:UpdatePanel runat="server" ID="UpdatePanellist1" UpdateMode="Conditional">
                                <ContentTemplate>--%>
                            <div class="col-md-4 filterBox" style="display: none;">
                                <ul id="Ul1">

                                    <p class="help-block">
                                        <asp:Label runat="server" ID="lblTimespan" meta:resourcekey="DateFrom"></asp:Label>
                                    </p>
                                    <asp:TextBox runat="server" CssClass="library_search datepicker1" ID="txtFromDate" data-mask="99/99/2099" AutoPostBack="False" Style="height: 51px;"></asp:TextBox>
                                    <br />
                                    <p class="help-block">
                                        <asp:Label runat="server" ID="Label11" meta:resourcekey="DateTo"></asp:Label>
                                    </p>
                                    <asp:TextBox ID="txtTodate" runat="server" CssClass="library_search datepicker12" data-mask="99/99/2099" Style="height: 51px;"></asp:TextBox>

                                    <br />
                                    <p class="help-block">
                                        <asp:Label runat="server" ID="Label12" meta:resourcekey="Person"></asp:Label>
                                    </p>
                                    <asp:DropDownList runat="server" ID="ddSearchPerson" CssClass="form-control" Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; color: white;">
                                    </asp:DropDownList>
                                    <br />
                                    <p class="help-block">
                                        <asp:Label runat="server" ID="Label13" meta:resourcekey="Division"></asp:Label>
                                    </p>
                                    <asp:DropDownList runat="server" ID="ddSeachDivision" CssClass="form-control" Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; color: white;">
                                    </asp:DropDownList>
                                    <br />
                                    <p class="help-block">
                                        <asp:Label runat="server" ID="Label14" meta:resourcekey="JobType"></asp:Label>
                                    </p>
                                    <asp:DropDownList runat="server" ID="ddSearchJobType"
                                        CssClass="form-control" Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; color: white;">
                                    </asp:DropDownList>
                                    <br />
                                    <p class="help-block">
                                        <asp:Label runat="server" ID="Label1" CssClass="" meta:resourcekey="lblCompetence" Style=""></asp:Label>
                                    </p>
                                    <asp:DropDownList runat="server" ID="ddlcompetencefilter" CssClass="form-control" Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; color: white;">
                                    </asp:DropDownList>
                                    <br />
                                    <p class="help-block">
                                        <asp:Label runat="server" ID="Label15" meta:resourcekey="Level"></asp:Label>
                                    </p>
                                    <asp:DropDownList runat="server" ID="ddSearchLevel" CssClass="form-control" Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; color: white;">
                                        <asp:ListItem Text="All" Value="0" meta:resourcekey="drpAllResourse">All</asp:ListItem>
                                        <asp:ListItem Text="1 to 2" Value="1 to 2" meta:resourcekey="OneToTwoResource">1 to 2</asp:ListItem>
                                        <asp:ListItem Text="2 to 3" Value="2 to 3" meta:resourcekey="TwoToThreeResource">2 to 3</asp:ListItem>
                                        <asp:ListItem Text="3 to 4" Value="3 to 4" meta:resourcekey="ThreeToFourResource">3 to 4</asp:ListItem>
                                        <asp:ListItem Text="4 to 5" Value="4 to 5" meta:resourcekey="FourToFiveResource">4 to 5</asp:ListItem>
                                    </asp:DropDownList>
                                    <br />

                                    <p class="help-block">
                                        <asp:Label runat="server" ID="Label16" meta:resourcekey="SortBy"></asp:Label>
                                    </p>
                                    <asp:DropDownList runat="server" ID="ddSearchSort" CssClass="form-control" Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; color: white;">

                                        <asp:ListItem Text="Ascending" Value="asc" meta:resourcekey="drpSortingResourse1"></asp:ListItem>
                                        <asp:ListItem Text="Descending" Value="dsc" meta:resourcekey="drpSortingResourse2"></asp:ListItem>
                                    </asp:DropDownList>
                                    <br />
                                    <asp:Button runat="server" ID="btnFilter" OnClick="btnFilter_OnClick" Text="Filter"  meta:resourcekey="btnFilter" CssClass="btn btn-primary black col-md-12" CausesValidation="False"></asp:Button>
                                </ul>
                            </div>
                            <div class="col-md-12 searchResultBox">
                                <asp:UpdatePanel runat="server" ID="upFilter" UpdateMode="Conditional" ChildrenAsTriggers="True">
                                    <ContentTemplate>
                                        <ul class="ndtopicsList ulNoStyle">
                                            <asp:Label runat="server" ID="lblNoRecordsFound" Text="No Record Found."
                                                Style="margin-left: 10px" meta:resourcekey="lblNoRecordsFoundResource1"></asp:Label>
                                            <asp:Repeater runat="server" ID="rptForumAll">
                                                <ItemTemplate>

                                                    <li class="ndsingleList ndquestion" id="eachTopic_2266000006396567">

                                                        <div class="ndflLeft ndphoto">
                                                            <img purpose="authorProfile" authorid="2396358" authorname="quasaur1"
                                                                src='../Log/upload/Userimage/<%# Eval("userImage") %>' onerror="../Organisation/manager/images/profile_img.png">
                                                        </div>

                                                        <div class="ndflLeft ndtypeIcon"></div>
                                                        <div class="ndhideOverflow">
                                                            <div class="ndhideOverflow">
                                                                <div class="ndflLeft width89">

                                                                    <h3>
                                                                        <a class="topicText mediumUnderlineLink" purpose="postTitle" id=""
                                                                            href='forumpost.aspx?fmid=<%# Eval("forumMasterId") %>' isresponse="true"
                                                                            target="_parent" style="color:blue;">
                                                                            <asp:Label runat="server" ID="txtQuestionName"
                                                                                Text='<%# Eval("fmQuestionName") %>'
                                                                                meta:resourcekey="txtQuestionNameResource1" style="color:blue;"/>
                                                                        </a>

                                                                       <%-- <a class="orangeLink" onclick='topiconClickAction("2266000000002054","2266000006396567",true, "migration-failure-user-id-not-coming-across");' href="#" isresponse="true" target="_parent">[<asp:Label runat="server" ID="Label10" CssClass="" meta:resourcekey="Totalreplay"></asp:Label>
                                                                            : 0<%#Eval("TotalReply") %>]</a>--%>
                                                                         <div class="orangeLink">[<asp:Label runat="server" ID="Label10" CssClass="" meta:resourcekey="Totalreplay"></asp:Label>
                                                                            : 0<%#Eval("TotalReply") %>]</div>

                                                                    </h3>
                                                                </div>

                                                                <div class="ndflRight ndstatusCont" votetopic="true" firstresponseid="2266000006428103">
                                                                    <div class="ndthumbsUpIcon flLeft" title=" 1 user has this question"></div>
                                                                    <span class="ndvoteCount"></span>
                                                                </div>
                                                            </div>
                                                            <p class="flLeft ndPostMeta width78">


                                                                <span>
                                                                    <asp:Label runat="server" ID="Label1" CssClass="" meta:resourcekey="LastReplay"></asp:Label></span>

                                                                <a authorprof="show" href="#" purpose="authorProfile" target="_parent" authorid="2396358" class="authorChatPresence" authorname="quasaur1" onclick="return false;"><%#Eval("lastReplyBy") %></a>

                                                                <span><asp:Label ID="Labelin" runat="server" Text="Label" meta:resourcekey="in"></asp:Label></span>&nbsp;<a purpose="" hashpurpose="" title="">

                                                                    <%#Eval("fmCategoryName") %>                  
                                                                </a>

                                                                <span class="ndsep">&nbsp;•&nbsp;</span>

                                                                <em class="ndboldem" title="23-Jan-2015 09:46 PM">

                                                                    <%#Eval("MinAgo") %>          
                                
                                                                </em>

                                                                <span class="ndsep"></span>


                                                                <span>
                                                                    <asp:Label runat="server" ID="Label2" CssClass="" meta:resourcekey="AskedBy"></asp:Label></span>

                                                                <a href="#" class="authorChatPresence"><%#Eval("usreName")%></a>

                                                            </p>
                                                            &nbsp;
                        
                                                        </div>

                                                    </li>
                                                </ItemTemplate>

                                            </asp:Repeater>
                                        </ul>
                                        <script src="../Scripts/Scripts/jquery-1.4.1.min.js"></script>
                                    </ContentTemplate>
                                    <Triggers>
                                        <%--<asp:AsyncPostBackTrigger ControlID="btnFilter" EventName="Click"/>--%>
                                        <asp:PostBackTrigger ControlID="btnFilter" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>

                            <%--</ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="txtsearch" />
                                    <asp:AsyncPostBackTrigger ControlID="ddForumCat" />
                                </Triggers>
                            </asp:UpdatePanel>--%>



                            <div class="clearAll"></div>




                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a href="#" onclick="javascript:history.back(); return false;" class="btn black pull-right" style="border-radius: 5px; margin-left: 0px; margin-top: 5px; float: right; color: white; display: none">Back</a>
        <%--
            </div>
         </div>--%>

        <style>
            /*a:link, a:visited {
                color: #555555;
            }*/

            .authorChatPresence:link {
                color: #555555;
            }

            .authorChatPresence:visited {
                color: #555555;
            }

            .textWidgetContainer {
                max-height: 100%;
                max-width: 250px;
            }



            .clear2:after {
                clear: both;
                content: ".";
                display: block;
                height: 0;
                visibility: hidden;
            }

            .copy-right2 {
                border-top: 1px dotted #EFEFEF;
                text-align: center;
            }
        </style>


        <div id="messageContainer" class="info" style="display: none;">
            <div class="borderBox" style="z-index: 10000;">
                <div class="flRight whiteCloseButton" id="messageContainerClose" onclick="hideMessage();"></div>
                <div class="flLeft icon"></div>
                <div class="innerMessage" id="messageContentCont"></div>
            </div>
        </div>
        <div id="ajaxLoaderSmallTemp" style="display: none; position: absolute; z-index: 5000;">
            <img alt="AjaxLoadingImage" src="" align="middle" width="16" height="16">
        </div>
        <div id="navigationLoading" class="flRight" style="display: none; position: absolute; left: 48%">
            <img alt="MediumLoadingImage" src="" style="width: 15px; height: 15px;" align="absMiddle" width="32" height="32">
        </div>



    </div>
</asp:Content>

