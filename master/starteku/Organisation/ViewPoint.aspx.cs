﻿using starteku_BusinessLogic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Globalization;

public partial class Organisation_ViewPoint : System.Web.UI.Page
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "my", " SetExpandCollapse();", true);

        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }

        if (!IsPostBack)
        {
            Employee.InnerHtml = GetLocalResourceObject("Welcome.Text").ToString() + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            GetAllEmployeeList();
            //EmployeeListArchiveAll();
            SetDefaultMessage();

            // dharmesh 30042015
            GetAllDepartments();
            GetAllJobTypes();
        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion


    #region method
    protected void SetDefaultMessage()
    {
        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["msg"])))
        {
            if (Convert.ToString(Request.QueryString["msg"]) == "del")
            {
                lblMsg.Text = CommonModule.msgRecordHasBeenDeletedSuccessfully;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "ins")
            {
                lblMsg.Text = CommonModule.msgRecordInsertedSuccessfully;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "upd")
            {
                lblMsg.Text = CommonModule.msgRecordUpdatedSuccss;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "arc")
            {
                lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
        }

    }

    public static DataView GetView(DataSet ds, string filter, string sort)
    {
        try
        {
            DataView dv = new DataView(ds.Tables[0]);
            dv.Sort = sort;
            dv.RowFilter = filter;
            return dv;
        }
        catch (Exception ex)
        {

            Common.WriteLog("GetView===============Error============" + ex.StackTrace);
            return null;
        }
    }

    protected void GetAllEmployeeList()
    {
        UserBM obj = new UserBM();
        obj.userIsActive = true;
        obj.userIsDeleted = false;
        if (Convert.ToString(Session["OrguserType"]) == "1" || Convert.ToString(Session["OrguserType"]) == "2")
        {
            obj.userType = 2;
        }
        else
        {
            obj.userType = 3;
        }
        obj.userCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.userCreateBy = Convert.ToString(Session["OrgUserId"]);
        obj.GetAllEmployeecreatebymanager_ViewPoint();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvGrid.DataSource = ds.Tables[0];
                gvGrid.DataBind();

                gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;

                DataTable dtCloned = ds.Tables[0].Clone();
                //change data type of column
                dtCloned.Columns[3].DataType = typeof(Int32);
                dtCloned.Columns[4].DataType = typeof(Int32);
                dtCloned.Columns[6].DataType = typeof(Int32);
                dtCloned.Columns[5].DataType = typeof(decimal);
                //import row to cloned datatable
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    dtCloned.ImportRow(row);
                }

                
                decimal total = dtCloned.AsEnumerable().Sum(row => row.Field<int>("Apl"));
                gvGrid.FooterRow.Cells[1].Text = hdnTotal.Value;
                gvGrid.FooterRow.Cells[1].HorizontalAlign = HorizontalAlign.Right;
                gvGrid.FooterRow.Cells[2].Text = total.ToString();
                gvGrid.FooterRow.Cells[3].Text = dtCloned.AsEnumerable().Sum(row => row.Field<int>("Ppl")).ToString();
                try
                {
                    gvGrid.FooterRow.Cells[4].Text = dtCloned.AsEnumerable().Sum(row => row.Field<int>("pdp")).ToString();
                }
                catch (Exception)
                {

                    gvGrid.FooterRow.Cells[4].Text = "0";
                }
               
                gvGrid.FooterRow.Cells[5].Text = dtCloned.AsEnumerable().Sum(row => row.Field<decimal>("allpoint")).ToString();

            }
            else
            {
                gvGrid.DataSource = null;
                gvGrid.DataBind();
            }
        }
        else
        {
            gvGrid.DataSource = null;
            gvGrid.DataBind();
        }
    }
    #endregion

    #region grid Event
    protected void gvGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvGrid.PageIndex = e.NewPageIndex;
        GetAllEmployeeList();
    }
    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        //string language = "French";
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Danish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }

        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion

    //dharmesh 2015 04 30
    #region Dropdown

    protected void GetAllDepartments()
    {
        //DepartmentsBM obj = new DepartmentsBM();
        //obj.depIsActive = true;
        //obj.depIsDeleted = false;
        //obj.depCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        //obj.GetAllDepartments();
        //DataSet ds = obj.ds;

        DivisionBM obj = new DivisionBM();
        obj.depIsActive = true;
        obj.depIsDeleted = false;
        obj.depCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllDivision();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                 if (Convert.ToString(Session["Culture"]) == "Danish")
                {

                    ddlDepartment.Items.Clear();

                    ddlDepartment.DataSource = ds.Tables[0];
                    ddlDepartment.DataTextField = "divNameDN";
                    ddlDepartment.DataValueField = "divId";
                    ddlDepartment.DataBind();

                    ddlDepartment.Items.Insert(0, new ListItem(Convert.ToString(GetLocalResourceObject("DivisionResource.Text")), CommonModule.dropDownZeroValue));


                   // ddlDepartment.Items.Insert(0, Convert.ToString(GetLocalResourceObject("DivisionResource.Text")));
                }

                else
                {
                    ddlDepartment.DataSource = ds.Tables[0];
                    ddlDepartment.DataTextField = "divName";
                    ddlDepartment.DataValueField = "divId";
                    ddlDepartment.DataBind();

                    ddlDepartment.Items.Insert(0, new ListItem(Convert.ToString(GetLocalResourceObject("DivisionResource.Text")), CommonModule.dropDownZeroValue));
                }
            }
            else
            {
                ddlDepartment.Items.Clear();
                ddlDepartment.Items.Insert(0, new ListItem(Convert.ToString(GetLocalResourceObject("DivisionResource.Text")), CommonModule.dropDownZeroValue));
            }
        }
        else
        {
            ddlDepartment.Items.Clear();
            ddlDepartment.Items.Insert(0, new ListItem(Convert.ToString(GetLocalResourceObject("DivisionResource.Text")), CommonModule.dropDownZeroValue));
        }

    }

    protected void ddlDepartment_Change(object sender, EventArgs e)
    {
        //if (ddlDepartment.SelectedValue == "0")
        //{
        //    GetAllEmployeeList();
        //    return;
        //}

        UserBM obj = new UserBM();
        obj.userIsActive = true;
        obj.userIsDeleted = false;
        if (Convert.ToString(Session["OrguserType"]) == "1" || Convert.ToString(Session["OrguserType"]) == "2")
        {
            obj.userType = 2;
        }
        else
        {
            obj.userType = 3;
        }
        obj.userCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.userCreateBy = Convert.ToString(Session["OrgUserId"]);
        obj.GetAllEmployeecreatebymanager_ViewPoint();
        DataSet ds = obj.ds;

        var filter = "";

        if (ddlDepartment.SelectedValue == "0")
        {

            if (ddlJobType.SelectedValue == "0")
            {
                GetAllEmployeeList();
            }
            else
            {
                filter = "userJobType=" + ddlJobType.SelectedValue;
            }
        }        
        else
        {
            if (ddlJobType.SelectedValue == "0")
            {
                filter = "userdepId =" + ddlDepartment.SelectedValue;
            }
            else
            {
                filter = "userdepId=" + ddlDepartment.SelectedValue + "and userJobType=" + ddlJobType.SelectedValue;
            }
           
        }

        //var filter = "userdivId=" + ddlDepartment.SelectedValue;
        DataView dv = GetView(obj.ds, filter, "");
        
        if (dv != null)
        {
            var data = dv.ToTable();
            if (data.Rows.Count > 0)
            {
                gvGrid.DataSource = data;
                gvGrid.DataBind();

                gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
                DataTable dtCloned = data.Clone();
                //change data type of column
                dtCloned.Columns[3].DataType = typeof(Int32);
                dtCloned.Columns[4].DataType = typeof(Int32);
                dtCloned.Columns[6].DataType = typeof(Int32);
                dtCloned.Columns[5].DataType = typeof(decimal);
                //import row to cloned datatable
                foreach (DataRow row in data.Rows)
                {
                    dtCloned.ImportRow(row);
                }


                decimal total = dtCloned.AsEnumerable().Sum(row => row.Field<int>("Apl"));
                gvGrid.FooterRow.Cells[1].Text = hdnTotal.Value;
                gvGrid.FooterRow.Cells[1].HorizontalAlign = HorizontalAlign.Right;
                gvGrid.FooterRow.Cells[2].Text = total.ToString();
                gvGrid.FooterRow.Cells[3].Text = dtCloned.AsEnumerable().Sum(row => row.Field<int>("Ppl")).ToString();
                gvGrid.FooterRow.Cells[4].Text = dtCloned.AsEnumerable().Sum(row => row.Field<int>("pdp")).ToString();
                gvGrid.FooterRow.Cells[5].Text = dtCloned.AsEnumerable().Sum(row => row.Field<decimal>("allpoint")).ToString();
            }
            else
            {
                gvGrid.DataSource = null;
                gvGrid.DataBind();
            }
        }
        else
        {
            gvGrid.DataSource = null;
            gvGrid.DataBind();
        }
    }

    protected void GetAllJobTypes()
    {
        JobTypeBM obj = new JobTypeBM();
        obj.jobIsActive = true;
        obj.jobIsDeleted = false;
        obj.jobCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllJobType();
        DataSet ds = obj.ds;
           
        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                   if (Convert.ToString(Session["Culture"]) == "Danish")
                {

                    ddlJobType.Items.Clear();

                    ddlJobType.DataSource = ds.Tables[0];
                    ddlJobType.DataTextField = "jobNameDN";
                    ddlJobType.DataValueField = "jobId";
                    ddlJobType.DataBind();

                    ddlJobType.Items.Insert(0, new ListItem(Convert.ToString(GetLocalResourceObject("jobTypeResource.Text")), CommonModule.dropDownZeroValue));
                }

                else
                {
                    ddlJobType.Items.Clear();

                    ddlJobType.DataSource = ds.Tables[0];
                    ddlJobType.DataTextField = "jobName";
                    ddlJobType.DataValueField = "jobId";
                    ddlJobType.DataBind();

                    ddlJobType.Items.Insert(0, new ListItem(Convert.ToString(GetLocalResourceObject("jobTypeResource.Text")), CommonModule.dropDownZeroValue));

                }
            }
            else
            {
                ddlJobType.Items.Clear();
                ddlJobType.Items.Insert(0, new ListItem(Convert.ToString(GetLocalResourceObject("jobTypeResource.Text")), CommonModule.dropDownZeroValue));
            }
        }
        else
        {
            ddlJobType.Items.Clear();
            ddlJobType.Items.Insert(0, new ListItem(Convert.ToString(GetLocalResourceObject("jobTypeResource.Text")), CommonModule.dropDownZeroValue));
        }

    }
    
    protected void ddlJobType_Change(object sender, EventArgs e)
    {
        //if (ddlJobType.SelectedValue == "0")
        //{
        //    GetAllEmployeeList();
        //    return;
        //}

        UserBM obj = new UserBM();
        obj.userIsActive = true;
        obj.userIsDeleted = false;
        if (Convert.ToString(Session["OrguserType"]) == "1" || Convert.ToString(Session["OrguserType"]) == "2")
        {
            obj.userType = 2;
        }
        else
        {
            obj.userType = 3;
        }
        obj.userCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.userCreateBy = Convert.ToString(Session["OrgUserId"]);
        obj.GetAllEmployeecreatebymanager_ViewPoint();
        DataSet ds = obj.ds;

        var filter="";

        if (ddlJobType.SelectedValue == "0" )
        {

            if (ddlDepartment.SelectedValue == "0")
            {
                GetAllEmployeeList();
            }
            else
            {
                filter = "userdepId=" + ddlDepartment.SelectedValue;
            }
        }
        else
        {
            if (ddlDepartment.SelectedValue == "0")
            {
                filter = "userJobType=" + ddlJobType.SelectedValue;
            }
            else
            {
                filter = "userdepId=" + ddlDepartment.SelectedValue + "and userJobType=" + ddlJobType.SelectedValue;
            }
           
        }
        
        DataView dv = GetView(obj.ds, filter, "");
        
        if (dv != null)
        {
            var data = dv.ToTable();
            if (data.Rows.Count > 0)
            {
                gvGrid.DataSource = data;
                gvGrid.DataBind();

                gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
                DataTable dtCloned = data.Clone();
                //change data type of column
                dtCloned.Columns[3].DataType = typeof(Int32);
                dtCloned.Columns[4].DataType = typeof(Int32);
                dtCloned.Columns[6].DataType = typeof(Int32);
                dtCloned.Columns[5].DataType = typeof(decimal);
                //import row to cloned datatable
                foreach (DataRow row in data.Rows)
                {
                    dtCloned.ImportRow(row);
                }


                decimal total = dtCloned.AsEnumerable().Sum(row => row.Field<int>("Apl"));
                gvGrid.FooterRow.Cells[1].Text = hdnTotal.Value;
                gvGrid.FooterRow.Cells[1].HorizontalAlign = HorizontalAlign.Right;
                gvGrid.FooterRow.Cells[2].Text = total.ToString();
                try
                {
                    gvGrid.FooterRow.Cells[3].Text = dtCloned.AsEnumerable().Sum(row => row.Field<int>("Ppl")).ToString();
                }
                catch (Exception)
                {

                    gvGrid.FooterRow.Cells[3].Text ="0";
                }
              
                try
                {
                    gvGrid.FooterRow.Cells[4].Text = dtCloned.AsEnumerable().Sum(row => row.Field<int>("pdp")).ToString();
                }
                catch (Exception)
                {

                    gvGrid.FooterRow.Cells[4].Text ="0";
                }
               
                gvGrid.FooterRow.Cells[5].Text = dtCloned.AsEnumerable().Sum(row => row.Field<decimal>("allpoint")).ToString();
            }
            else
            {
                gvGrid.DataSource = null;
                gvGrid.DataBind();
            }
        }
        else
        {
            gvGrid.DataSource = null;
            gvGrid.DataBind();
        }
    }

    #endregion
    Int32 AlPsum = 0;
    protected void gvGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    
    {
        //Label l1 = new Label();
        //if (e.Row.RowType == DataControlRowType.Header)
        //{
        //    l1 = (Label)(e.Row.FindControl("lblTotalALPPoint"));
        //}

        
        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    Label l = (Label)(e.Row.FindControl("lblALP"));
        //    AlPsum = AlPsum + Convert.ToInt32(((Label)(e.Row.FindControl("lblALP"))).Text);
        //    ViewState["AlpTotal"] = AlPsum;
           
        //}
      

        
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        if (Convert.ToString(Session["OrguserType"]) == "1")
        {
            Response.Redirect("AllManager.aspx");
        }
        else
        {
            Response.Redirect("add_employee.aspx");
        }
       
    }
}