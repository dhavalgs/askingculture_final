﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true" CodeFile="ReportAll.aspx.cs" Inherits="Organisation_ReportAll"
    Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <%--<style type="text/css">
        th
        {
            border: 1px solid #c0c0c0;
        }
    </style>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link href="../assets/css/multiselect.css" rel="stylesheet" />

    <asp:HiddenField runat="server" ID="GraphSubTitle" meta:resourcekey="ResourceGraphSubTitle"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="GraphAxisLevel" meta:resourcekey="ResourceGraphAxisLevel"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="GraphToolTip" meta:resourcekey="ResourceGraphToolTip"></asp:HiddenField>
    <div class="">
        <div class="container">
            <div class="col-md-8">
                <div class="heading-sec">
                    <h1>
                        <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Report" EnableViewState="false" />
                        <i><span runat="server" id="Settings"></span></i>
                    </h1>
                </div>
            </div>
            <%--<div class="col-md-3" style="display: none">
                <div class="dropdown-example">
                    <ul class="nav nav-pills">
                        <li class="dropdown" style="float: left!important; width: 100%;"><a class="skill_dropdown"
                            id="drop7" role="button" data-toggle="dropdown" href="#">Account Department<b class="skill_caret"></b></a>
                            <ul id="menu7" class="dropdown-menu" role="menu" aria-labelledby="drop7" style="width: 100%;">
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Account Department</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Sales Department</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Human Resources Department
                                </a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Software Department</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Network Department</a></li>
                                <li role="presentation" class="divider"></li>
                                <%-- <li role="presentation"><a role="menuitem" tabindex="-1" href="report_all.html">All</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>--%>
            <div class="col-md-4">
                <div class="dropdown-example">
                    <ul class="nav nav-pills">

                        <li class="dropdown" style="float: left!important; width: 100%;">
                            <a class="skill_dropdown" id="drop7" role="button" data-toggle="dropdown" href="report_all.html">
                                <asp:Label ID="Label2" runat="server" meta:resourcekey="ResourceGraphLabelAll"></asp:Label><b class="skill_caret"></b></a>
                            <ul id="ddCompetencesTable" class="dropdown-menu" role="menu" aria-labelledby="drop7"
                                style="width: 100%;">
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="Report.aspx?dd=o">
                                    <%--  Original Competence Level--%><asp:Label ID="GraphLabelOriginal" runat="server" meta:resourcekey="ResourceGraphLabelOriginal"></asp:Label></a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="Report.aspx?dd=n">
                                    <%--New Competence Level--%><asp:Label ID="GraphLabelNew" runat="server" meta:resourcekey="ResourceGraphLabelNew"></asp:Label></a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="Report.aspx?dd=d">
                                    <%--Development Points--%><asp:Label ID="GraphLabelDevelopment" runat="server" meta:resourcekey="ResourceGraphLabelDevelopment"></asp:Label></a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="Report.aspx?dd=k">
                                    <%--Knowledge Share--%><asp:Label ID="GraphLabelKnowledge" runat="server" meta:resourcekey="ResourceGraphLabelKnowledge"></asp:Label></a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="LearningPoint.aspx">
                                    <%--Knowledge Share--%><asp:Label ID="GraphLabelLearningPoint" runat="server" meta:resourcekey="ResourceGraphLabelLearningPoints"></asp:Label></a></li>
                                <%-- <li role="presentation"><a role="menuitem" tabindex="-1" href="#" onclick="showThisGraph('original')">
                                    Potential for development</a></li>--%>
                                <li role="presentation" class="divider"></li>

                            </ul>
                        </li>

                        <%-- <li class="dropdown" style="float: left!important; width: 100%;">
                            <a class="skill_dropdown" id="drop7" role="button" data-toggle="dropdown" href="report_all.html">
                                All<b class="skill_caret"></b></a>
                              <ul id="menu7" class="dropdown-menu" role="menu" aria-labelledby="drop7" style="width: 100%;">
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="Report.aspx?dd=d">Original
                                    Competence Level</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="Report.aspx?dd=n">New
                                    Competence Level</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="Report.aspx?dd=d">Development
                                    Points</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="Report.aspx?dd=k">Knowledge
                                    Share</a></li>
                              <%--  <li role="presentation"><a role="menuitem" tabindex="-1" href="Report.aspx?dd=p">Potential
                                    for development</a></li>--%>
                        <%--<li role="presentation" class="divider"></li>--%>
                        <%--<li role="presentation"><a role="menuitem" tabindex="-1" href="#">All</a></li>--%>
                        <%--</ul>
                        </li>--%>
                    </ul>
                </div>
            </div>
            <!--<li class="range">
<a href="#">
<i class="icon-calendar"></i>
<span>August 5, 2014 - September 3, 2014</span>
<i class="icon-angle-down"></i>
</a>
</li>-->
            <div class="col-md-12">
                 <h3 style="margin-bottom: 0px;">
                                <div class="otherDiv" style="float: right">
                                    <asp:DropDownList ID="ddlDepartment" runat="server" Style="margin-left: -30px; width: 100%; height: 32px; display: inline; margin-left: -15px; margin-bottom: 20px;"
                                        CssClass="form-control" OnSelectedIndexChanged="ddlcompetence_SelectedIndexChanged"
                                        AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="ddlDepartment"
                                        ErrorMessage="Please select department." InitialValue="0" CssClass="commonerrormsg"
                                        Display="Dynamic" ValidationGroup="chkNewEmp"></asp:RequiredFieldValidator>
                                </div>
                                <div class="otherDiv" style="float: right">
                                    <asp:DropDownList ID="ddljobtype1" runat="server" Style="margin-left: -30px; width: 100%; height: 32px; margin-bottom: 20px;"
                                        CssClass="form-control" OnSelectedIndexChanged="ddljobtype_SelectedIndexChanged"
                                        AutoPostBack="True">
                                    </asp:DropDownList>
                                    <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddljobtype"
                                                            ErrorMessage="Please select job type." InitialValue="0" CssClass="commonerrormsg"
                                                            Display="Dynamic" ValidationGroup="chkNewEmp" meta:resourcekey="RequiredFieldValidator1Resource1"></asp:RequiredFieldValidator>--%>
                                </div>

                                <div class="otherDiv" style="float: right; margin-right: 4%;">
                                    <asp:DropDownList ID="ddlTeam" runat="server" Style="margin-left: 0px; margin-right: 0px; width: 100%; height: 32px; margin-bottom: 20px;"
                                        CssClass="form-control" OnSelectedIndexChanged="ddlTeam_SelectedIndexChanged"
                                        AutoPostBack="True">
                                    </asp:DropDownList>
                                </div>

                                <div class="otherDiv" style="float: right; margin-right: 4%;">
                                    <asp:HiddenField runat="server" ID="ddtTemplateVal" Value="0" meta:resourcekey="nooptionselected" />

                                    <asp:DropDownList ID="managercat" runat="server" multiple="multiple">
                                    </asp:DropDownList>

                                    <asp:Button ID="managercatTmp" runat="server" Text="Temporart" CssClass="btns  yellow  col-md-11 libray_btn" Style="float: right; width: 130px; font-size: 17px; display: none;"
                                        OnClick="managercat_Click" />
                                </div>
                            </h3>
            </div>
            <div class="col-md-12" style="margin-top: 20px;">
                <div id="graph-wrapper">
                    <div class="col-md-12">
                        <ul class="tabs-menu">
                            <li class="current"><a href="#tab-1" onclick="SetVal('o');">
                                <asp:Literal ID="Literal7" runat="server" meta:resourcekey="Originalcompetencelevel"
                                    EnableViewState="false" /></a></li>
                            <li><a href="#tab-2" onclick="setTimeout(function (){drwaChartNewCompLevel(); SetVal('n');},500);">
                                <asp:Literal ID="Literal8" runat="server" meta:resourcekey="Newcompetencelevel" EnableViewState="false" /></a></li>
                            <li><a href="#tab-3" onclick="setTimeout(function (){drwaChartDevelopmentCompLevel(); SetVal('d');},500);">
                                <asp:Literal ID="Literal9" runat="server" meta:resourcekey="Developmentpoints" EnableViewState="false" /></a></li>
                            <li><a href="#tab-4" onclick="setTimeout(function (){drwaKnowledgeDevelopment(); SetVal('k');},500);">
                                <asp:Literal ID="Literal10" runat="server" meta:resourcekey="Knowledgeshare" EnableViewState="false" /></a></li>
                            <li><a href="#tab-5" onclick="setTimeout(function (){drwaLearningpointGraph(); SetVal('l');},500);">
                                <asp:Literal ID="Literal2" runat="server" meta:resourcekey="LearningPoint" EnableViewState="false" /></a></li>
                            <%-- <li><a href="#tab-5">
                                <asp:Literal ID="Literal11" runat="server" meta:resourcekey="Potentialfordevelopment"
                                    EnableViewState="false" /></a></li>--%>
                        </ul>
                        <div class="chart-tab">

                           

                            <div id="tabs-container">
                                <div class="tab">
                                    <div id="tab-1" class="tab-content">
                                        <p>
                                            <%-- Original competence level--%>

                                            <asp:Literal ID="Originalcompetencelevel" runat="server" meta:resourcekey="Originalcompetencelevel"
                                                EnableViewState="false" />
                                            <asp:Button ID="btnPDF" runat="server" CommandArgument="Originalcompetencelevel"
                                                CommandName="Original" Text="Export to PDF" CssClass="btns  yellow  col-md-11 libray_btn"
                                                Style="width: 130px; font-size: 15px; display: none;" OnClick="btnPDF_Click" />
                                        </p>

                                        <%-- <div class="progress small-progress">
                                            <div style="width: 45%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40"
                                                role="progressbar" class="progress-bar blue">
                                            </div>
                                        </div>--%>
                                        <div class="col-md-12 comp_table" style="overflow-x: auto;">
                                            <asp:Repeater runat="server" ID="rowRepeater" OnItemDataBound="rowRepeater_ItemBound">
                                                <HeaderTemplate>
                                                    <table width="100%" border="1" class="report_table">
                                                        <tr>
                                                            <th>
                                                                <asp:Label runat="server" ID="lblrName" Text="Name" meta:resourcekey="lblrNameResource1" />
                                                            </th>
                                                            <asp:Repeater runat="server" ID="headerRepeater">
                                                                <ItemTemplate>
                                                                    <th>
                                                                        <asp:Label runat="server" ID="lblUserName" Text='<%# Eval("comCompetence") %>' meta:resourcekey="lblUserNameResource1" />
                                                                        <asp:Label runat="server" ID="lblcid" Text='<%# Eval("comId") %>' Visible="False"
                                                                            meta:resourcekey="lblcidResource1" />
                                                                        <asp:HiddenField ID="comId1" runat="server" Value='<%# Eval("comId") %>' />
                                                                        </td>
                                                                    </th>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                            <th>
                                                                <asp:Label runat="server" ID="lblEmployer" Text="Employer" meta:resourcekey="lblEmployerResource1" />
                                                            </th>
                                                        </tr>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <strong>
                                                                <asp:HiddenField ID="userId" runat="server" Value='<%# Eval("userId") %>' />
                                                                <asp:Label ID="lblhead1" Text='<%# Eval("name") %>' runat="server" meta:resourcekey="lblhead1Resource1"></asp:Label>
                                                            </strong>
                                                        </td>
                                                        <asp:Repeater runat="server" ID="columnRepeater">
                                                            <ItemTemplate>
                                                                <td>
                                                                    <asp:Label ID="lblhead" Text="0" runat="server" meta:resourcekey="lblheadResource1"></asp:Label>
                                                                    <asp:Label ID="Lacname" Text='<%# Eval("comCompetence") %>' runat="server" Visible="False"
                                                                        meta:resourcekey="LacnameResource1"></asp:Label>
                                                                    <asp:HiddenField ID="hdnComid" Value='<%#Eval("comId")%>' runat="server" />
                                                                </td>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                        <td>
                                                            <strong>
                                                                <asp:Label ID="lblavg" Text="0" runat="server" meta:resourcekey="lblavgResource1"></asp:Label>
                                                            </strong>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <tr class="average" style="border: none;">
                                                        <td style="border: none;">
                                                            <asp:Label ID="lblAverage" Text="Average" runat="server" meta:resourcekey="lblAverageResource1"></asp:Label>
                                                        </td>
                                                        <asp:Repeater runat="server" ID="FooterRepeater">
                                                            <ItemTemplate>
                                                                <td style="border: none;">
                                                                    <asp:Label runat="server" ID="lblFavg" Text='<%# Eval("Local") %>' meta:resourcekey="lblFavgResource1" />
                                                                </td>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                        <td style="border: none;">
                                                            <asp:Label runat="server" ID="lblextra" meta:resourcekey="lblextraResource1" />
                                                        </td>
                                                    </tr>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                        <div class="home_grap">
                                            <div id="containerOriginal" style="min-width: 310px; height: 400px; margin: 0 auto">
                                                <div id="wait" style="margin: 190px 612px">
                                                    <img src="../images/wait.gif" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="tab-2" class="tab-content">
                                        <p>
                                            <asp:Literal ID="Newcompetencelevel" runat="server" meta:resourcekey="Newcompetencelevel"
                                                EnableViewState="false" />
                                            <asp:Button ID="Button1" runat="server" CommandArgument="Newcompetencelevel" CommandName="New"
                                                Text="Export to PDF" CssClass="btns  yellow  col-md-11 libray_btn" Style="width: 130px; font-size: 15px; display: none;"
                                                OnClick="btnPDF_Click" />
                                        </p>
                                        <%-- <asp:Literal ID="Newcompetencelevel" runat="server" meta:resourcekey="Newcompetencelevel"
                                            EnableViewState="false" />--%>
                                        <%--<div class="progress small-progress">
                                            <div style="width: 30%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40"
                                                role="progressbar" class="progress-bar blue">
                                            </div>
                                        </div>--%>
                                        <div class="col-md-12 comp_table" style="overflow-x: auto;">
                                            <asp:Repeater runat="server" ID="rptnew" OnItemDataBound="rptnew_ItemBound">
                                                <HeaderTemplate>
                                                    <table width="100%" border="1" class="report_table">
                                                        <tr>
                                                            <th>
                                                                <asp:Label runat="server" ID="lblrName" Text="Name" meta:resourcekey="lblrNameResource2" />
                                                            </th>
                                                            <asp:Repeater runat="server" ID="headerRepeater">
                                                                <ItemTemplate>
                                                                    <th>
                                                                        <asp:Label runat="server" ID="lblUserName" Text='<%# Eval("comCompetence") %>' meta:resourcekey="lblUserNameResource2" />
                                                                        <asp:Label runat="server" ID="lblcid" Text='<%# Eval("comId") %>' Visible="False"
                                                                            meta:resourcekey="lblcidResource2" />
                                                                        <asp:HiddenField ID="comId1" runat="server" Value='<%# Eval("comId") %>' />
                                                                        </td>
                                                                    </th>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                            <th>
                                                                <asp:Label runat="server" ID="lblEmployer" Text="Employer" meta:resourcekey="lblEmployerResource2" />
                                                            </th>
                                                        </tr>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <strong>
                                                                <asp:HiddenField ID="userId" runat="server" Value='<%# Eval("userId") %>' />
                                                                <asp:Label ID="lblhead1" Text='<%# Eval("name") %>' runat="server" meta:resourcekey="lblhead1Resource2"></asp:Label>
                                                            </strong>
                                                        </td>
                                                        <asp:Repeater runat="server" ID="columnRepeater">
                                                            <ItemTemplate>
                                                                <td>
                                                                    <asp:Label ID="lblhead" Text="0" runat="server" meta:resourcekey="lblheadResource2"></asp:Label>
                                                                    <asp:Label ID="Lacname" Text='<%# Eval("comCompetence") %>' runat="server" Visible="False"
                                                                        meta:resourcekey="LacnameResource2"></asp:Label>
                                                                    <asp:HiddenField ID="hdnComid" Value='<%#Eval("comId")%>' runat="server" />
                                                                </td>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                        <td>
                                                            <strong>
                                                                <asp:Label ID="lblavg" Text="0" runat="server" meta:resourcekey="lblavgResource2"></asp:Label>
                                                            </strong>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <tr class="average" style="border: none;">
                                                        <td style="border: none;">
                                                            <asp:Label ID="lblAverage" Text="Average" runat="server" meta:resourcekey="lblAverageResource2"></asp:Label>
                                                        </td>
                                                        <asp:Repeater runat="server" ID="FooterRepeater">
                                                            <ItemTemplate>
                                                                <td style="border: none;">
                                                                    <asp:Label runat="server" ID="lblFavg" Text='<%# Eval("Achive") %>' meta:resourcekey="lblFavgResource2" />
                                                                </td>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                        <td style="border: none;">
                                                            <asp:Label runat="server" ID="lblextra" meta:resourcekey="lblextraResource2" />
                                                        </td>
                                                    </tr>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                        <div class="home_grap">
                                            <div id="ContainerNew" style="min-width: 310px; height: 400px; margin: 0 auto">
                                                <div id="waitNew" style="margin: 190px 612px">
                                                    <img src="../images/wait.gif" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="tab-3" class="tab-content">
                                        <p>
                                            <asp:Literal ID="Developmentpoints" runat="server" meta:resourcekey="Developmentpoints"
                                                EnableViewState="false" />
                                            <asp:Button ID="btn" runat="server" CommandArgument="Developmentpoints" CommandName="Developmentpoints"
                                                Text="Export to PDF" CssClass="btns  yellow  col-md-11 libray_btn" Style="width: 130px; font-size: 15px; display: none;"
                                                OnClick="btnPDF_Click" />
                                        </p>
                                        <%-- <asp:Literal ID="Developmentpoints" runat="server" meta:resourcekey="Developmentpoints"
                                            EnableViewState="false" />--%>
                                        <%--<div class="progress small-progress">
                                            <div style="width: 40%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40"
                                                role="progressbar" class="progress-bar blue">
                                            </div>
                                        </div>--%>
                                        <div class="col-md-12 comp_table" style="overflow-x: auto;">
                                            <asp:Repeater runat="server" ID="rptDevelopment" OnItemDataBound="rptDevelopment_ItemBound">
                                                <HeaderTemplate>
                                                    <table width="100%" border="1" class="report_table">
                                                        <tr>
                                                            <th>
                                                                <asp:Label runat="server" ID="lblrName" Text="Name" meta:resourcekey="lblrNameResource3" />
                                                            </th>
                                                            <asp:Repeater runat="server" ID="headerRepeater">
                                                                <ItemTemplate>
                                                                    <th>
                                                                        <asp:Label runat="server" ID="lblUserName" Text='<%# Eval("comCompetence") %>' meta:resourcekey="lblUserNameResource3" />
                                                                        <asp:Label runat="server" ID="lblcid" Text='<%# Eval("comId") %>' Visible="False"
                                                                            meta:resourcekey="lblcidResource3" />
                                                                        <asp:HiddenField ID="comId1" runat="server" Value='<%# Eval("comId") %>' />
                                                                        </td>
                                                                    </th>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                            <th>
                                                                <asp:Label runat="server" ID="lblEmployer" Text="Employer" meta:resourcekey="lblEmployerResource3" />
                                                            </th>
                                                        </tr>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <strong>
                                                                <asp:HiddenField ID="userId" runat="server" Value='<%# Eval("userId") %>' />
                                                                <asp:Label ID="lblhead1" Text='<%# Eval("name") %>' runat="server" meta:resourcekey="lblhead1Resource3"></asp:Label>
                                                            </strong>
                                                        </td>
                                                        <asp:Repeater runat="server" ID="columnRepeater">
                                                            <ItemTemplate>
                                                                <td>
                                                                    <asp:Label ID="lblhead" Text="0" runat="server" meta:resourcekey="lblheadResource3"></asp:Label>
                                                                    <asp:Label ID="Lacname" Text='<%# Eval("comCompetence") %>' runat="server" Visible="False"
                                                                        meta:resourcekey="LacnameResource3"></asp:Label>
                                                                    <asp:HiddenField ID="hdnComid" Value='<%#Eval("comId")%>' runat="server" />
                                                                </td>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                        <td>
                                                            <strong>
                                                                <asp:Label ID="lblavg" Text="0" runat="server" meta:resourcekey="lblavgResource3"></asp:Label>
                                                            </strong>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <tr class="average" style="border: none;">
                                                        <td style="border: none;">
                                                            <asp:Label ID="lblAverage" Text="Average" runat="server" meta:resourcekey="lblAverageResource3"></asp:Label>
                                                        </td>
                                                        <asp:Repeater runat="server" ID="FooterRepeater">
                                                            <ItemTemplate>
                                                                <td style="border: none;">
                                                                    <asp:Label runat="server" ID="lblFavg" Text='<%# Eval("Achive") %>' meta:resourcekey="lblFavgResource3" />
                                                                    <asp:Label runat="server" ID="lblComIdFooter" Text='<%# Eval("comId") %>' Style="display: none;"
                                                                        meta:resourcekey="lblComIdFooterResource1" />
                                                                </td>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                        <td style="border: none;">
                                                            <asp:Label runat="server" ID="lblextra" meta:resourcekey="lblextraResource3" />
                                                        </td>
                                                    </tr>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                        <div class="home_grap">
                                            <div id="ContainerDevelopment" style="min-width: 310px; height: 400px; margin: 0 auto">
                                                <div id="waitDevelopment" style="margin: 190px 612px">
                                                    <img src="../images/wait.gif" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="tab-4" class="tab-content">
                                        <p>
                                            <asp:Literal ID="Knowledgeshare" runat="server" meta:resourcekey="Knowledgeshare"
                                                EnableViewState="false" />
                                            <asp:Button ID="Button2" runat="server" CommandArgument="Knowledgeshare" CommandName="Knowledgeshare"
                                                Text="Export to PDF" CssClass="btns  yellow  col-md-11 libray_btn" Style="width: 130px; font-size: 15px; display: none;"
                                                OnClick="btnPDF_Click" />
                                        </p>
                                        <%-- <asp:Literal ID="Knowledgeshare" runat="server" meta:resourcekey="Knowledgeshare"
                                            EnableViewState="false" />--%>
                                        <%--<div class="progress small-progress">
                                            <div style="width: 30%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40"
                                                role="progressbar" class="progress-bar blue">
                                            </div>
                                        </div>--%>
                                        <div class="col-md-12 comp_table" style="overflow-x: auto;">
                                            <asp:Repeater runat="server" ID="rpt_Knowledge" OnItemDataBound="rpt_Knowledge_ItemBound">
                                                <HeaderTemplate>
                                                    <table width="100%" border="1" class="report_table">
                                                        <tr>
                                                            <th>
                                                                <asp:Label runat="server" ID="lblrName" Text="Name" meta:resourcekey="lblrNameResource2" />
                                                            </th>
                                                            <asp:Repeater runat="server" ID="headerRepeater">
                                                                <ItemTemplate>
                                                                    <th>
                                                                        <asp:Label runat="server" ID="lblUserName" Text='<%# Eval("comCompetence") %>' meta:resourcekey="lblUserNameResource2" />
                                                                        <asp:Label runat="server" ID="lblcid" Text='<%# Eval("comId") %>' Visible="False"
                                                                            meta:resourcekey="lblcidResource2" />
                                                                        <asp:HiddenField ID="comId1" runat="server" Value='<%# Eval("comId") %>' />
                                                                        </td>
                                                                    </th>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                            <th>
                                                                <asp:Label runat="server" ID="lblEmployer" Text="Employer" meta:resourcekey="lblEmployerResource2" />
                                                            </th>
                                                        </tr>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <strong>
                                                                <asp:HiddenField ID="userId" runat="server" Value='<%# Eval("userId") %>' />
                                                                <asp:Label ID="lblhead1" Text='<%# Eval("name") %>' runat="server" meta:resourcekey="lblhead1Resource2"></asp:Label>
                                                            </strong>
                                                        </td>
                                                        <asp:Repeater runat="server" ID="columnRepeater">
                                                            <ItemTemplate>
                                                                <td>
                                                                    <asp:Label ID="lblhead" Text="0" runat="server" meta:resourcekey="lblheadResource2"></asp:Label>
                                                                    <asp:Label ID="Lacname" Text='<%# Eval("comCompetence") %>' runat="server" Visible="False"
                                                                        meta:resourcekey="LacnameResource2"></asp:Label>
                                                                    <asp:HiddenField ID="hdnComid" Value='<%#Eval("comId")%>' runat="server" />
                                                                </td>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                        <td>
                                                            <strong>
                                                                <asp:Label ID="lblavg" Text="0" runat="server" meta:resourcekey="lblavgResource2"></asp:Label>
                                                            </strong>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <tr class="average" style="border: none;">
                                                        <td style="border: none;">
                                                            <asp:Label ID="lblAverage" Text="Average" runat="server" meta:resourcekey="lblAverageResource2"></asp:Label>
                                                        </td>
                                                        <asp:Repeater runat="server" ID="FooterRepeater">
                                                            <ItemTemplate>
                                                                <td style="border: none;">
                                                                    <asp:Label runat="server" ID="lblFavg" Text='<%# Eval("Knowledge_Point") %>' meta:resourcekey="lblFavgResource2" />
                                                                </td>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                        <td style="border: none;">
                                                            <asp:Label runat="server" ID="lblextra" meta:resourcekey="lblextraResource2" />
                                                        </td>
                                                    </tr>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                        <div class="home_grap">
                                            <div id="ContainerKnowledgeDevelopment" style="min-width: 310px; height: 400px; margin: 0 auto">
                                                <div id="waitKnowledgeDevelopment" style="margin: 190px 612px">
                                                    <img src="../images/wait.gif" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="tab-5" class="tab-content">
                                        <p>
                                            <asp:Literal ID="Literal3" runat="server" meta:resourcekey="LearningPoint"
                                                EnableViewState="false" />
                                            <asp:Button ID="Button3" runat="server" CommandArgument="Newcompetencelevel" CommandName="New"
                                                Text="Export to PDF" CssClass="btns  yellow  col-md-11 libray_btn" Style="width: 130px; font-size: 15px; display: none;"
                                                OnClick="btnPDF_Click" />
                                        </p>
                                        <%-- <asp:Literal ID="Newcompetencelevel" runat="server" meta:resourcekey="Newcompetencelevel"
                                            EnableViewState="false" />--%>
                                        <%-- <div class="progress small-progress">
                                            <div style="width: 30%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40"
                                                role="progressbar" class="progress-bar blue">
                                            </div>
                                        </div>--%>
                                        <div class="col-md-12 comp_table" style="text-align: left;">
                                            <asp:Repeater runat="server" ID="rpt_Learngpoint" OnItemDataBound="rpt_Learngpoint_ItemDataBound">
                                                <HeaderTemplate>
                                                    <table width="100%" border="1" class="report_table report_table1" style="background-color: #E8E8E8; font-size: 12px; margin-top: 0;">
                                                        <tr style="height: 35px;">
                                                            <th>
                                                                <asp:Label runat="server" ID="lblrName" Text="Name" meta:resourcekey="lblrNameResource1" />
                                                            </th>
                                                            <th>
                                                                <asp:Label runat="server" ID="lblAlpHdr" Text="ALP" meta:resourcekey="lblAlpHdrResource" />
                                                            </th>
                                                            <th>
                                                                <asp:Label runat="server" ID="lblplpHdr" Text="PLP" meta:resourcekey="lblplpHdrResource" />
                                                            </th>
                                                            <th>
                                                                <asp:Label runat="server" ID="lblTotalHdr" Text="Total" meta:resourcekey="lblTotalHdrResource" />
                                                            </th>

                                                        </tr>
                                                </HeaderTemplate>
                                                <ItemTemplate>

                                                    <tr style="height: 35px;">
                                                        <td>
                                                            <strong>
                                                                <asp:HiddenField ID="userIdLP" runat="server" Value='<%# Eval("userid") %>' />
                                                                <asp:Label ID="lblhead1LP" Text='<%# Eval("username") %>' runat="server" meta:resourcekey="lblhead1Resource1"></asp:Label>
                                                                <strong></td>
                                                        <td>

                                                            <asp:Label ID="lblContentALP" Text='<%# Eval("ALP") %>' runat="server" meta:resourcekey="lblhead1Resource1"></asp:Label>

                                                        </td>
                                                        <td>

                                                            <asp:Label ID="lblContentPLP" Text='<%# Eval("PLP") %>' runat="server" meta:resourcekey="lblhead1Resource1"></asp:Label>

                                                        </td>
                                                        <td>

                                                            <asp:Label ID="lblContentTOTAL" Text='<%# Eval("TOTAL") %>' runat="server" meta:resourcekey="lblhead1Resource1"></asp:Label>

                                                        </td>
                                                    </tr>

                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                        <div class="home_grap">
                                            <div id="ContainerLearninipoint" style="min-width: 310px; height: 400px; margin: 0 auto">
                                                <div id="waitLearninipoint" style="margin: 190px 612px">
                                                    <img src="../images/wait.gif" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--  <h3 class="custom-heading darkblue">Original competence level</h3>
				 <div class="visitor-stats widget-body blue">
					
					<div class="home_grap"><div class="graph-info">
								
								 <a href="#" id="bars"><span><i class="fa fa-bar-chart-o"></i></span></a>
								 <a href="#" id="lines" class="active"><span><i class="fa fa-code-fork"></i></span></a>
							 </div>
							 <div class="graph-container">
							
								 <div id="graph-lines"></div>
								 <div id="graph-bars"></div>
							 </div></div>
							 <div class="col-md-12" style="background:#f4f4f4; padding-left:0px !important;" >
							 <div class="col-md-1 profile-details" style="padding-left:0px !important;" >
							 
							 
							 </div>
					
					
				 </div>
					 -->
                </div>
            </div>
            <div style="clear: both">
            </div>
            <!-- TIME LINE -->
        </div>
    </div>
    <!-- Chat Widget -->
    <!-- Recent Post -->
    <div class="col-md-3">
    </div>
    <!-- Twitter Widget -->
    <!-- Weather Widget -->
    </div>
    <!-- Container -->
    </div><!-- Wrapper -->
    <!-- RAIn ANIMATED ICON-->
    <%--Chart--%>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js" type="text/javascript"></script>
    <script src="../Scripts/Scripts/exporting.js" type="text/javascript"></script>


     <script src="../Scripts/jquery321.js" type="text/javascript"></script>
    <script src="../assets/js/multiselect.js" type="text/javascript"></script>

    <script src="../Scripts/highcharts.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/chart-line-and-graph.js"></script>
    <script type="text/javascript">
        var icons = new Skycons();
        icons.set("rain", Skycons.RAIN);
        icons.play();
    </script>



     <script type="text/javascript">
         $(document).ready(function () {

             sessionStorage.setItem("multi", "0");
             $('#ContentPlaceHolder1_managercat').multiselect({
                 //enableFiltering: true,
                 includeSelectAllOption: true,
                 //buttonWidth: '400px',
                 selectAllText: 'All Competence',
                 //  //nonSelectedText: 'None Location selected',
                 allSelectedText: 'All Competence',
                 //  nSelectedText: 'selected',
                 //  numberDisplayed: 2,
                 buttonText: function (options) {
                     var labels = [];
                     var num = [];

                     if (options.length === 0) {
                         sessionStorage.setItem("multi", "0");
                         $('#ContentPlaceHolder1_ddtTemplateVal').val('0');
                         return $('#ContentPlaceHolder1_hdnNoOption').val();
                     }
                     options.each(function () {
                         if ($(this).attr('value') !== undefined) {
                             labels.push($(this).text());
                             num.push($(this).val());
                         }
                     });

                     $('#ContentPlaceHolder1_ddtTemplateVal').val(num.join(','));

                     return labels.join(', ');
                 },
                 onChange: function (event) {
                     // alert($('#ContentPlaceHolder1_ddtTemplateVal').val());
                     sessionStorage.setItem("multi", $('#ContentPlaceHolder1_ddtTemplateVal').val());
                     $('#ContentPlaceHolder1_managercatTmp').click();
                 }
             });

             var curTab = sessionStorage.getItem("multi");

             //alert('curTab' + curTab);

             if (curTab != null && curTab != '') {
                 var dataarray = curTab.split(",");

                 $("#ContentPlaceHolder1_managercat").val(dataarray);
                 $("#ContentPlaceHolder1_managercat").multiselect("refresh");
                 sessionStorage.setItem("multi", "0");
             }


             setTimeout(function () {
                 $('.multiselect-container').css('height', '500px');
                 $('.multiselect-container').css('overflow', 'scroll');
             }, 500);
         });
    </script>

     <script type="text/javascript">
         function SetSesssionVariableValue() {
             sessionStorage.setItem("multi", "0");
         }
    </script>
    <script>

        function ChangeColor(a) {
            $("#" + a).parent().css('background', 'ActiveBorder');
        }

        function SetVal(a) {

            if (a == "o") {
                text = $('#<%=GraphLabelOriginal.ClientID %>').text();
            }
            else if (a == "d") {
                text = $('#<%=GraphLabelDevelopment.ClientID %>').text();
            }
            else if (a == "n") {
                text = $('#<%=GraphLabelNew.ClientID %>').text();
            }
            else if (a == "k") {
                text = $('#<%=GraphLabelKnowledge.ClientID %>').text();
            }
            else if (a == "l") {
                text = $('#<%=GraphLabelLearningPoint.ClientID %>').text();
            }


    var subtext = $('#<%=GraphSubTitle.ClientID %>').val();
            var AxisText = $('#<%=GraphAxisLevel.ClientID %>').val();
            ToolTipText = $('#<%=GraphToolTip.ClientID %>').val();

            if (subtext == "") {
                subtext = "Click and drag mouse arrow to zoom chart";
            }
            if (text == "") {
                text = $("#ddLabel").text();
                if (text == "") {
                    text = "Original Competence Level";
                }
            }
            if (AxisText == "") {

                text = "Level";
            }
            if (ToolTipText == "") {

                ToolTipText = "Level";
            }


            $(".highcharts-title").text(text);
            $(".highcharts-subtitle").text(subtext);
            $(".highcharts-yaxis-title").text(AxisText);
            $(".spanText").text(ToolTipText);



        }

        $(document).ready(function () {

            SetExpandCollapse();
            setTimeout(function () {
                var text = $('#<%=Label2.ClientID %>').text();
                var subtext = $('#<%=GraphSubTitle.ClientID %>').val();
                var AxisText = $('#<%=GraphAxisLevel.ClientID %>').val();
                ToolTipText = $('#<%=GraphToolTip.ClientID %>').val();

                if (subtext == "") {
                    subtext = "Click and drag mouse arrow to zoom chart";
                }
                if (text == "") {
                    text = $("#ddLabel").text();
                    if (text == "") {
                        text = "Original Competence Level";
                    }
                }
                if (AxisText == "") {

                    text = "Level";
                }
                if (ToolTipText == "") {

                    ToolTipText = "Level";
                }

                $(".highcharts-title").text(text);
                $(".highcharts-subtitle").text(subtext);
                $(".highcharts-yaxis-title").text(AxisText);
                $(".spanText").text(ToolTipText);
            }, 3000);
        });

    </script>
    <script>
        var totalLevel;
        var dataLP;
        function SubmitBtn1() {

            GetPageName();
            var ReturnDataLP;
            $.ajax({
                type: "POST",
                cache: false,
                async: true,
                url: "ReportALL.aspx/BtnSubmitAJAXLP",
                data: '{txtName:"' + '' + '",txtDesc:"' + '' + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    dataLP = response.d;

                    Developmentcomnames1();
                    setTimeout(function () {


                        //$("#waitLearninipoint").fadeOut();

                    }, 500);


                },
                failure: function (msg) {

                    alert(msg);
                    ReturnDataLP = msg;
                }
            });

        }

        function Developmentcomnames1() {

            var ReturnDataLP;
            $.ajax({
                type: "POST",
                cache: false,
                async: true,
                url: "ReportALL.aspx/DevelopmentcomnamesLP",
                data: '{txtName:"' + '' + '",txtDesc:"' + '' + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    totalLevel = response.d;
                    return totalLevel;

                },
                failure: function (msg) {

                    alert(msg);
                    ReturnDataLP = msg;
                }
            });

        }


        function drwaLearningpointGraph() {
            console.log(totalLevel);
            console.log(dataLP);

            $("#waitLearninipoint").fadeOut();
            $(function () {
                $('#ContainerLearninipoint').highcharts({
                    chart: {
                        type: 'column',
                        zoomType: 'x'
                    },
                    title: {
                        text: 'Graph'
                    },
                    subtitle: {
                        text: 'Click and drag mouse arrow to zoom chart'
                    },

                    xAxis: {
                        categories: totalLevel

                    },
                    yAxis: {

                        min: 0,
                        title: {
                            text: 'Level'
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                            //'<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                            '<td style="padding:0"><b>{point.y:.1f} <span class="spanText">Level</span></b></td></tr>',
                        footerFormat: '</table>',
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series: dataLP
                });
            });
        }





    </script>
</asp:Content>
