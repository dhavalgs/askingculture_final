﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Threading;
using System.Globalization;
using starteku_BusinessLogic.Model;

public partial class Organisation_OrganisationMaster : System.Web.UI.MasterPage
{
    string country = "";
    string state = "";
    string lastLoginDateTime = "";
    startetkuEntities1 db = new startetkuEntities1();
    protected void Page_Load(object sender, EventArgs e)
    {
        //Common.WriteLog("Page_Load 1");
        try
        {
            var userId = Convert.ToInt32(Session["OrgUserId"]);
            hdnLoginUserIDType.Value = Convert.ToString(Session["OrguserType"]);
            Int32 userCompanyID = Convert.ToInt32(Session["OrgCompanyId"]);
            var EnaData = db.GetItemsEnableList(userId, userCompanyID).FirstOrDefault();

            Session["Aemcompid"] = EnaData.Aemcompid;
            Session["Cemcompid"] = EnaData.comdirCompID;
            Session["Compcompid"] = EnaData.compCompID;
            Session["Pointcompid"] = EnaData.pointCompID;
            Session["Quescompid"] = EnaData.quesemID;

            if (string.IsNullOrWhiteSpace(Convert.ToString(Session["Aemcompid"])))
            {
                activityPanel.Visible = false;
            }
            if (string.IsNullOrWhiteSpace(Convert.ToString(Session["Cemcompid"])))
            {
                companyDirPanel.Visible = false;
            }
            if (string.IsNullOrWhiteSpace(Convert.ToString(Session["Quescompid"])))
            {
                questionPanel.Visible = false;
            }
            if (string.IsNullOrWhiteSpace(Convert.ToString(Session["Pointcompid"])))
            {
                pointPanel.Visible = false;
            }
            if (string.IsNullOrWhiteSpace(Convert.ToString(Session["Compcompid"])))
            {
                competencePanel.Visible = false;

            }
            LogoMaster logo = db.LogoMasters.FirstOrDefault();
            //Common.WriteLog("Page_Load 2");
            if (logo != null)
            {

                imgPageLogo.ImageUrl = "../Log/upload/Userimage/" + logo.PageLogo;
                ViewState["imgLoginLogo"] = logo.PageLogo;
            }
            if (!IsPostBack)
            {
                if (!String.IsNullOrEmpty(Convert.ToString(Session["OrgUserName"])))
                {
                    Setlanguage();
                    detdata();
                }
            }

            if (!String.IsNullOrEmpty(Convert.ToString(Session["OrgUserName"])))
            {
                span2.InnerHtml = Convert.ToString(Session["OrgUserName"]);
                span1.InnerHtml = Convert.ToString(Session["OrgUserName"]);
                span3.InnerHtml = state + "," + country;
                span4.InnerHtml = state + "," + country;
                if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["userDateTime"])))
                {
                    login.InnerHtml = Convert.ToString(GetLocalResourceObject("LastLoginResource.Text")) + " : " + Convert.ToString(Session["userDateTime"]);
                }
            }
            if (!String.IsNullOrEmpty(Convert.ToString(Session["OrgUserImage"])))
            {
                img.ImageUrl = "../Log/upload/Userimage/" + Convert.ToString(Session["OrgUserImage"]) + "";
                img1.ImageUrl = "../Log/upload/Userimage/" + Convert.ToString(Session["OrgUserImage"]) + "";
            }
        }
        catch (Exception ex)
        {
           // Common.WriteLog("error in Page_Load" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            ExceptionLogger.LogException(ex, 0, "OrganisationMaster.master->Page_Load");
        }
    }


    protected void detdata()
    {
        UserBM obj = new UserBM();
        obj.userId = Convert.ToInt32(Session["OrgUserId"]);
        obj.GetEmployeedetailsbyid();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["couName"])))
                    country = Convert.ToString(ds.Tables[0].Rows[0]["couName"]);

                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["staName"])))
                    state = Convert.ToString(ds.Tables[0].Rows[0]["staName"]);

                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["staName"])))
                    state = Convert.ToString(ds.Tables[0].Rows[0]["staName"]);
            }
        }
    }
    protected void Setlanguage()
    {
        try
        {
            string language = Convert.ToString(Session["Culture"]);
            //string language = "Denish";
            string languageId = "";
            //if (!string.IsNullOrEmpty(language))
            //{
            //    if (language.EndsWith("Denish")) languageId = "da-DK";
            //    else languageId = "en-GB";
            //    SetCulture(languageId);
            //}
            //ResourceLanguageBM obj = new ResourceLanguageBM();
            //DataSet resds = obj.GetAllResourceLanguage();
            //if (!string.IsNullOrEmpty(language))
            //{
            //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
            //    {
            //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
            //        {
            //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
            //        }
            //    }

            //    SetCulture(languageId);
            //}
            ResourceLanguageBM obj = new ResourceLanguageBM();
            if (!string.IsNullOrEmpty(language))
            {
                DataSet resds = obj.GetResourceLanguage(language);
                languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

                SetCulture(languageId);
            }


            if (Session["Language"] != null)
            {
                if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
            }
        }
        catch { }
    }
    protected void SetCulture(string languageId)
    {
        try
        {
            Session["Language"] = languageId;
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
        }
        catch { }
    }

    protected string GetToolTip(string tag)
    {
        var localResourceObject = "";
        try
        {
            localResourceObject = Convert.ToString(GetLocalResourceObject(tag));
        }
        catch (Exception)
        {
            localResourceObject = "";

        }
        return localResourceObject;
    }
}

