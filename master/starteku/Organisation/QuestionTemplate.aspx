﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Organisation/OrganisationMaster.master"
    CodeFile="QuestionTemplate.aspx.cs" Inherits="Organisation_QuestionTemplate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style>
        .tabs-menu li {
            width: 33.33%;
        }

        .tab {
            padding-top: 0px !important;
        }

        .tab-content {
            padding: 0px !important;
        }



        .dropdown-menu > li > a {
            display: block;
            padding: 8px 65px !important;
            padding-bottom: 8px;
            clear: both;
            font-weight: normal;
            line-height: 1.428571429;
            color: #333;
            white-space: nowrap;
        }

        .radio input[type="radio"], .radio-inline input[type="radio"], .checkbox input[type="checkbox"], .checkbox-inline input[type="checkbox"] {
            float: left;
            margin-left: -80px !important;
        }

        .radiobuttonlist {
            font: 12px Verdana, sans-serif;
            color: #000; /* non selected color */
        }

        .inline-rb input[type="radio"] {
            width: auto;
            margin-right: 10px;
            margin-left: 20px;
        }

        .inline-rb label {
            display: inline;
        }
    </style>
    <%--joyride---------------------------------------------%>
    <link href="../plugins/joyride/joyride-2.1.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/multiselect.css" rel="stylesheet" />
    <link href="../Styles/UpdateCustom.css" rel="stylesheet" type="text/css" />
    <script src="../plugins/joyride/jquery.cookie.js" type="text/javascript"></script>
    <script src="../plugins/joyride/jquery.joyride-2.1.js" type="text/javascript"></script>
    <script src="../plugins/joyride/modernizr.mq.js" type="text/javascript"></script>

    <script src="../plugins/highchart/highcharts.js" type="text/javascript"></script>


    <script src="../plugins/highchart/highcharts-more.js" type="text/javascript"></script>
    <script src="../Scripts/plugins/exporting.js" type="text/javascript"></script>
    <script src="../Scripts/jquery321.js"></script>

    <script src="../assets/js/multiselect.js" type="text/javascript"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            setTimeout(function () {
                SetExpandCollapse();
                //ExampanCollapsMenu();
                //ExampanCollapssubMenu();
                // tabMenuClick();
                $(".OrgDivisionList").addClass("active_page");
            }, 500);
        });
    </script>
    <div class="col-md-6">
        <div class="heading-sec">
            <h1>
                <asp:Label runat="server" ID="Label1" CssClass="lblModel" meta:resourcekey="Quetemp" Font-Bold="True" ForeColor="white" Font-Size="22px"></asp:Label>
                   
            </h1>
        </div>
    </div>
    <div class="col-md-12" style="margin-top: 20px;">

        <div id="Div1">
            <div class="col-md-12">
                <div class="add-btn1" style="float: right;">


                   <a href="#popupAddNewQuesTemplate" data-toggle="modal" title="" style="display: none">
                        <asp:Label runat="server" ID="lblQuestionTemp" CssClass="lblModel" meta:resourcekey="AddQuestionTemplate" Style="font-size: 19px; margin-top: 4px;"></asp:Label>
                    </a>
                    <asp:Button runat="server" ID="btnAddTemplate" Text="Add Question Template" CssClass="btn btn-default yellow " meta:resourcekey="AddQuestionTemp"
                        type="button" Style="border-radius: 5px; height: 38px; color: white;" OnClick="OpenCreateQuestionTemplate" />
                </div>

                <div style="clear: both;"></div>
            </div>
        </div>
    </div>

    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" meta:resourcekey="lblMsgResource1"></asp:Label>

    <%--Template--%>
    <div>
        <div aria-hidden="true" role="dialog" tabindex="-1" class="modal fade col-md-12" id="popupAddNewQuesTemplate" style="display: none;">
            <div class="modal-dialog" style="width: 55%">
                <div class="modal-content">
                    <div class="modal-header blue yellow-radius">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                            ×
                        </button>
                        <h4 class="modal-title">
                            <asp:Label runat="server" ID="Label12" CssClass="" Style="color: white; font-size: 19px; margin-top: 4px;">
                                <asp:Literal ID="Literal14" runat="server" Text="Add Question Template"
                                    EnableViewState="true" meta:resourcekey="AddQuestionTemp"/>
                            </asp:Label></h4></div><div class="modal-body">
                        <div class="form-group">
                            <div id="Div4" runat="server" style="clear: both" visible="false">
                                <div class="col-md-3" style="padding-top: 10px;">
                                    <asp:Label runat="server" ID="Label13" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                        <asp:Literal ID="Literal15" runat="server" meta:resourcekey="Error" EnableViewState="false" />
                                    </asp:Label></div><div class="col-md-9">
                                    <asp:Label runat="server" ID="Label14" ForeColor="red" />
                                </div>
                            </div>
                        </div>
                        <br />

                        <div class="form-group">
                            <div class="col-md-3" style="padding-top: 10px;">
                                <asp:Label runat="server" ID="Label15" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                    <asp:Literal ID="Literal16" runat="server" Text="Question Template Name" meta:resourcekey="QuestionTemp_Name" EnableViewState="false" />
                                </asp:Label></div><div class="col-md-9" style="margin-top: 10px;">
                                <asp:TextBox runat="server" ID="txttmpName" placeholder="Enter Question Template Name" MaxLength="50" meta:resourcekey="quetempname"/>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txttmpName"
                                    ErrorMessage="Please enter Template Name." CssClass="commonerrormsg" Display="Dynamic"
                                    ValidationGroup="chkdoc3" meta:resourcekey="plsentertemnameres"></asp:RequiredFieldValidator></div></div><div class="form-group">
                            
                            <div class="col-md-3" style="padding-top: 10px;">
                                <asp:Label runat="server" ID="Label16" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                    <asp:Literal ID="Literal18" runat="server" Text="Question Template Name (Danish)" meta:resourcekey="QuestionTempName" EnableViewState="false" />
                                </asp:Label></div><div class="col-md-9" style="margin-top: 10px;">
                                <asp:TextBox runat="server" ID="txttmpNameDN" placeholder="Enter Question Template Name" MaxLength="50" meta:resourcekey="quetempnamedn"/>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txttmpNameDN"
                                    ErrorMessage="Please enter Template Name." CssClass="commonerrormsg" Display="Dynamic"
                                    ValidationGroup="chkdoc3" meta:resourcekey="plsenterquetemnmdnres"></asp:RequiredFieldValidator></div>
                        </div><div class="form-group">
                            <div class="col-md-3" style="padding-top: 10px;">
                                <asp:Label runat="server" ID="Label18" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                    <asp:Literal ID="Literal19" runat="server" Text="Description" EnableViewState="false" meta:resourcekey="Descriptionres" />
                                </asp:Label></div><div class="col-md-9">
                                <asp:TextBox runat="server" ID="txttmpDescription" placeholder="Enter Description" TextMode="MultiLine" MaxLength="200" meta:resourcekey="descres"/>

                            </div>
                        </div>

                        <div class="form-group" runat="server" id="enableDiv">
                            <div class="col-md-3" style="padding-top: 10px;">
                                <asp:Label runat="server" ID="Label19" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                    <asp:Literal ID="Literal20" runat="server" Text="Active" meta:resourcekey="Enabled" /></asp:Label></div><div class="col-md-9" style="margin-top: 10px">
                                <asp:RadioButtonList runat="server" ID="rdoIsActive" RepeatDirection="Horizontal" CssClass="inline-rb" TextAlign="Right">
                                    <asp:ListItem Text="Yes" meta:resourcekey="lblYesres" Value="true" Selected="True" />
                                    <asp:ListItem Text="No" meta:resourcekey="lblNores" Value="false" />
                                </asp:RadioButtonList>
                                <%--meta:resourcekey="txttitleResource1"--%>
                            </div>
                        </div>

                        <div class="form-group" runat="server" id="publicDiv">
                            <div class="col-md-3" style="padding-top: 10px;">
                                <asp:Label runat="server" ID="Label21" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                    <asp:Literal ID="Literal21" runat="server" Text="Public" meta:resourcekey="Public" EnableViewState="false" /></asp:Label></div><div class="col-md-9" style="margin-top: 10px">
                                <asp:RadioButtonList runat="server" ID="roIsPublic" RepeatDirection="Horizontal" CssClass="inline-rb" TextAlign="Right">
                                    <asp:ListItem Text="Yes" meta:resourcekey="lblYesres" Value="true" Selected="True" />
                                    <asp:ListItem Text="No" meta:resourcekey="lblNores" Value="false" />
                                </asp:RadioButtonList>
                                <%--meta:resourcekey="txttitleResource1"--%>
                            </div>
                        </div>

                        <div class="form-group" runat="server" id="mandatoryDiv">
                            <div class="col-md-3" style="padding-top: 10px;">
                                <asp:Label runat="server" ID="Label22" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                    <asp:Literal ID="Literal22" runat="server" Text="Mandatory" meta:resourcekey="Mandatory" EnableViewState="false" /></asp:Label></div><div class="col-md-9" style="margin-top: 10px">
                                <asp:RadioButtonList runat="server" ID="roIsMandatory" RepeatDirection="Horizontal" CssClass="inline-rb" TextAlign="Right">
                                    <asp:ListItem Text="Yes" meta:resourcekey="lblYesres" Value="true" Selected="True" />
                                    <asp:ListItem Text="No" meta:resourcekey="lblNores" Value="false" />
                                </asp:RadioButtonList>

                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">

                        <asp:Button runat="server" ID="btnCreateTemplate"
                            Text="Create" CssClass="btn btn-primary yellow" meta:resourcekey="btncreate"
                            type="button" OnClick="CreateQuestionTemplate" OnClientClick="CheckValidations('chkdoc3');closeModelCancel('popupAddNewQuesTemplate');"
                            Style="border-radius: 5px; width: 100px; height: 38px; margin-left: 359px;" />

                        <asp:Button runat="server" ID="Button4"
                            Text="Close" CssClass="btn btn-default black" meta:resourcekey="btnclose"
                            type="button" ValidationGroup="chkdoc3" OnClientClick="closeModelCancel('popupAddNewQuesTemplate');"
                            Style="border-radius: 5px; width: 100px; height: 38px;" />


                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12" style="margin-top: 20px;" id="htmlData">
        <div id="graph-wrapper">
            <div class="col-md-12">

                <div class="chart-tab">
                    <div id="tabs-container">
                        <div class="loaderDiv" style="display: none">
                            <div class="progress small-progress">
                                <div style="width: 40%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40"
                                    role="progressbar" class="progress-bar blue">
                                </div>
                            </div>

                            <div class="home_grap">
                                <div id="ContainerDevelopment" style="min-width: 310px; height: 400px; margin: 0 auto">
                                    <div id="waitDevelopment" style="margin: 190px 602px">
                                        <img src="../images/wait.gif" /> </div></div></div></div><div class="tab">

                            <div id="tab-1" class="tab-content">

                                <div class="chart-tab manager_table">
                                    <div id="tabs-container manager_table">
                                        <asp:GridView ID="gvGrid_QuestionTemp" runat="server" AutoGenerateColumns="False" CellPadding="0"
                                            Width="100%" GridLines="None" DataKeyNames="qtmpID" CssClass="table1 table-striped table-bordered table-hover table-checkable datatable"
                                             EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>'
                                            BackColor="White"
                                            meta:resourcekey="GridRecordNotfound"
                                            OnRowDataBound="gvGrid_QuestionTemp_RowDataBound"
                                            OnRowCommand="gvGrid_QuestionTemp_RowCommand">
                                            <%--  OnRowDataBound="gvGrid_Question_RowDataBound"
                                            OnRowCreated="gvGrid_RowCreated"
                                            OnPreRender="gvGrid_PreRender">--%>


                                            <HeaderStyle CssClass="aa" />
                                            <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                            <AlternatingRowStyle BackColor="White" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="SrNo" meta:resourcekey="SrNo">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                        Width="3%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />


                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Question Template Name" meta:resourcekey="TemplateFieldResource2">
                                                    <ItemTemplate>
                                                         <%if (Convert.ToString(Session["Culture"]) == "Danish")
                                                          {%>
                                                          <asp:Label ID="Label2" runat="server" Text='<%# Eval("tmpNameDN") %>'></asp:Label>
                                                         <%} %>
                                                        <%else
                                                          { %>
                                                        <asp:Label ID="lblrNamer" runat="server" Text='<%# Eval("tmpName") %>'></asp:Label>
                                                          <%} %>
                                                        
                                                        <asp:HiddenField ID="divCompanyId" runat="server" Value='<%# Eval("tmpCompanyID") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Font-Size="14px" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                        Width="50%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="IsActive" meta:resourcekey="Isactive">
                                                    <ItemTemplate>
                                                          <asp:HiddenField runat="server" ID="hdnIsActive" Value='<%# Eval("IsActive") %>' />
                                                          <asp:HiddenField runat="server" ID="hdnIspublic" Value='<%# Eval("IsPublic") %>' />
                                                          <asp:HiddenField runat="server" ID="hdnIsmandatory" Value='<%# Eval("IsMandatory") %>' />
                                                        <asp:Label runat="server" ID="lblactive"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Font-Size="14px" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow" Width="10px" ForeColor="white" Font-Size="18px" BorderWidth="0px" Height="55px">
                                                        
                                                    </HeaderStyle>
                                                </asp:TemplateField>
                                                
                                                 <asp:TemplateField HeaderText="IsPublic" meta:resourcekey="Ispublic">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblpublic" ></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Font-Size="14px" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"  Width="10px"  ForeColor="white" Font-Size="18px" BorderWidth="0px" Height="55px">
                                                        
                                                    </HeaderStyle>
                                                </asp:TemplateField>
                                                
                                                 <asp:TemplateField HeaderText="IsMandatory" meta:resourcekey="Ismandatory">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblmandatory" ></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Font-Size="14px" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"  Width="10px" ForeColor="white" Font-Size="18px" BorderWidth="0px" Height="55px">
                                                        
                                                    </HeaderStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="" ItemStyle-Width="50px" meta:resourcekey="TemplateFieldResource3">
                                                    <ItemTemplate>
                                                        <div class="vat" style="width: 90px;" id="divEdit" runat="server">
                                                           
                                                                <i class="fa fa-pencil"></i><a href="<%# String.Format("QuestionTemplateEdit.aspx?id={0}", Eval("qtmpID")) %>"
                                                                    title="Edit">
                                                                <asp:Label runat="server" ID="lbledit" meta:resourcekey="btnedit"></asp:Label>

                                                                                            </a> </div><div class="total" style="width: 70px;" id="divDeletet" runat="server">
                                                           
                                                                <i class="fa fa-trash-o"></i>
                                                                <asp:LinkButton ID="lnkBtnName" runat="server" CommandName="archive" CommandArgument='<%# Eval("qtmpID") %>'
                                                                    ToolTip="Delete " OnClientClick="return confirm('Are you sure you want to Delete  this record?');"
                                                                    meta:resourcekey="lnkBtnNameResource1"> </asp:LinkButton>

                                                                                                </div></ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" Font-Size="14px" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                        Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <FooterStyle BackColor="#cccccc" Font-Bold="True" ForeColor="Black" HorizontalAlign="Left" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>

                            <div id="tab-2" class="tab-content">
                            </div>

                            <div id="tab-3" class="tab-content">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function closeModel(a) {

            if (CheckValidations('chkdoc')) { $('#' + a).modal('hide'); }

        }


        function closeModelCancel(a) { $('#' + a).modal('hide'); }
    </script>
   

</asp:Content>
