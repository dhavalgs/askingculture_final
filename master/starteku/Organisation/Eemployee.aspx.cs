﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using System.Configuration;
using System.Threading;
using System.Globalization;
using startetku.Business.Logic;
using starteku_BusinessLogic.Model;

public partial class Organisation_Eemployee : System.Web.UI.Page
{
    string temp = "0";
    protected void Page_Load(object sender, EventArgs e)
    {

        Page.ClientScript.RegisterStartupScript(Page.GetType(), "my", " SetExpandCollapse();", true);

        if (!IsPostBack)
        {
            setDefault();
            GetAllJobtype();
            GetAllDepartments();
            GetAllTeam();
            ddlgender.Items.Insert(0, new ListItem(hdnSelectGender.Value, CommonModule.dropDownZeroValue));
            Eemployee.InnerHtml = GetLocalResourceObject("Welcome.Text").ToString() + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                temp = Convert.ToString(Request.QueryString["id"]);
                div2.Visible = false;
            }
            else if (!String.IsNullOrEmpty(Request.QueryString["view"]))
            {
                temp = Convert.ToString(Request.QueryString["view"]);
                div1.Visible = false;
                btnsubmit.Visible = false;
            }
            GetAllEmployeebyid();
            // GetCheckBoxListData();
        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }


    #region method
    protected void GetAllEmployeebyid()
    {
        UserBM obj1 = new UserBM();
        obj1.userId = Convert.ToInt32(temp);

        obj1.GetAllEmployeebyid();
        DataSet ds = obj1.ds;

        if (ds != null && obj1.ds.Tables[0].Rows.Count > 0)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userFirstName"])))
            {
                txtName.Text = Convert.ToString(ds.Tables[0].Rows[0]["userFirstName"]);
                lblName.Text = Convert.ToString(ds.Tables[0].Rows[0]["userFirstName"]);
            }

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userLastName"])))
            {
                txtlastName.Text = Convert.ToString(ds.Tables[0].Rows[0]["userLastName"]);
                lblLastName.Text = Convert.ToString(ds.Tables[0].Rows[0]["userLastName"]);
            }

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userAddress"])))
            {
                txtaddress.Text = Convert.ToString(ds.Tables[0].Rows[0]["userAddress"]);
                lblAddress.Text = Convert.ToString(ds.Tables[0].Rows[0]["userAddress"]);
            }

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["usercontact"])))
            {
                txtMobile.Text = Convert.ToString(ds.Tables[0].Rows[0]["usercontact"]);
                lblPhoneNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["usercontact"]);
            }

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userEmail"])))
            {
                txtEmail.Text = Convert.ToString(ds.Tables[0].Rows[0]["userEmail"]);
                lblEmail.Text = Convert.ToString(ds.Tables[0].Rows[0]["userEmail"]);
            }
            //if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userPassword"])))
            //{
            //    string Password = CommonModule.decrypt(Convert.ToString(ds.Tables[0].Rows[0]["userPassword"]));
            //    txtPassword.Attributes.Add("value", Password);
            //    txtConfirmPassword.Attributes.Add("value", Password);
            //}

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userJobType"])))
            {
                ddljobtype.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["userJobType"]);
                lbljob.Text = ddljobtype.SelectedItem.Text;
            }

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userdepId"])))
            {
                ddlDepartment.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["userdepId"]);
                lblDepartment.Text = ddlDepartment.SelectedItem.Text;
            }

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userTeamID"])))
            {
                ddlTeam.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["userTeamID"]);
                ddlTeam.Text = ddlTeam.SelectedItem.Text;
            }
            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userGender"])))
            {
                ddlgender.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["userGender"]);
                ddlgender.Text = ddlgender.SelectedItem.Text;
            }

            



            //Code for set checkbox selected as per user selection saved
            Session["catIds"] = Convert.ToString(ds.Tables[0].Rows[0]["userCategory"]);

            string catids = Convert.ToString(ds.Tables[0].Rows[0]["userCategory"]);
            CompetenceMasterBM obj = new CompetenceMasterBM();
            obj.catIsActive = true;

            obj.catIsDeleted = false;
            if (string.IsNullOrEmpty(ddlDepartment.SelectedValue)) return;
            obj.comDivId = Convert.ToInt32(ddlDepartment.SelectedValue);
            obj.comJobtype = Convert.ToInt32(ddljobtype.SelectedValue);
            obj.GetAllCategoryByJobTypeId_DivId();
            ds = obj.ds;

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    repCompCat.DataSource = ds.Tables[0];
                    //repCompCat.DataTextField = "catName";
                    //chkList.DataValueField = "catId";
                    repCompCat.DataBind();
                    //chkList.SelectedIndex = 0;


                    //CheckBoxList1.DataSource = ds.Tables[0];
                    //CheckBoxList1.DataTextField = "catName";
                    //CheckBoxList1.DataValueField = "catId";
                    //CheckBoxList1.DataBind();
                    foreach (RepeaterItem aItem in repCompCat.Items)
                    {
                        var repCompAdd = (Repeater)aItem.FindControl("repCompAdd");
                        foreach (RepeaterItem bItem in repCompAdd.Items)
                        {
                            CheckBoxList chkDisplayTitle = (CheckBoxList)bItem.FindControl("chkList");
                            // Label hdnChkboxValue = (Label)bItem.FindControl("hdnChkboxValue");
                            for (int i = 0; i < chkDisplayTitle.Items.Count; i++)
                            {
                                chkDisplayTitle.Items[i].Selected = false;

                                if (!string.IsNullOrEmpty(catids))
                                {
                                    foreach (var j in catids.Split(','))
                                    {

                                        if (!string.IsNullOrWhiteSpace(j) && chkDisplayTitle.Items[i].Value == j)
                                        {
                                            chkDisplayTitle.Items[i].Selected = true;
                                        }

                                    }
                                }


                            }

                        }

                    }


                }


            }




        }

    }
    protected void GetAllJobtype()
    {
        JobTypeBM obj = new JobTypeBM();
        obj.jobIsActive = true;
        obj.jobIsDeleted = false;
        obj.jobCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllJobType();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {


                ddljobtype.Items.Clear();

                ddljobtype.DataSource = ds.Tables[0];
                if (Convert.ToString(Session["Culture"]) == "Danish")
                {
                    ddljobtype.DataTextField = "jobNameDN";
                }
                else
                {
                    ddljobtype.DataTextField = "jobName";
                }
                ddljobtype.DataValueField = "jobId";
                ddljobtype.DataBind();

                ddljobtype.Items.Insert(0, new ListItem(hdnjobtype.Value, CommonModule.dropDownZeroValue));

            }
            else
            {
                ddljobtype.Items.Clear();
                ddljobtype.Items.Insert(0, new ListItem(hdnjobtype.Value, CommonModule.dropDownZeroValue));
            }
        }
        else
        {
            ddljobtype.Items.Clear();
            ddljobtype.Items.Insert(0, new ListItem(hdnjobtype.Value, CommonModule.dropDownZeroValue));
        }

    }
    protected void GetAllTeam()
    {
        JobTypeBM obj = new JobTypeBM();
        obj.TeamIDs = "0";
        obj.jobIsActive = true;
        obj.jobIsDeleted = false;
        obj.jobCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllTeam();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {


                ddlTeam.Items.Clear();

                ddlTeam.DataSource = ds.Tables[0];
                if (Convert.ToString(Session["Culture"]) == "Danish")
                {
                    ddlTeam.DataTextField = "TeamNameDN";
                }
                else
                {
                    ddlTeam.DataTextField = "TeamName";
                }
                ddlTeam.DataValueField = "TeamID";
                ddlTeam.DataBind();

                ddlTeam.Items.Insert(0, new ListItem(hdnteam.Value, CommonModule.dropDownZeroValue));

            }
            else
            {
                ddlTeam.Items.Clear();
                ddlTeam.Items.Insert(0, new ListItem(hdnteam.Value, CommonModule.dropDownZeroValue));
            }
        }
        else
        {
            ddlTeam.Items.Clear();
            ddlTeam.Items.Insert(0, new ListItem(hdnteam.Value, CommonModule.dropDownZeroValue));
        }

    }
    protected void GetAllDepartments()
    {
        //DepartmentsBM obj = new DepartmentsBM();
        //obj.depIsActive = true;
        //obj.depIsDeleted = false;
        //obj.depCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        //obj.GetAllDepartments();
        //DataSet ds = obj.ds;
        DivisionBM obj = new DivisionBM();
        obj.depIsActive = true;
        obj.depIsDeleted = false;
        obj.depCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllDivision();
        DataSet ds = obj.ds;
        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {

                if (Convert.ToString(Session["Culture"]) == "Danish")
                {

                    ddlDepartment.Items.Clear();

                    ddlDepartment.DataSource = ds.Tables[0];
                    ddlDepartment.DataTextField = "divNameDN";
                    ddlDepartment.DataValueField = "divId";
                    ddlDepartment.DataBind();

                  //  ddlDepartment.Items.Insert(0, "Vælg afdeling");
                    ddlDepartment.Items.Insert(0, new ListItem(hdndivision.Value, CommonModule.dropDownZeroValue));
                }

                else
                {
                    ddlDepartment.DataSource = ds.Tables[0];
                    ddlDepartment.DataTextField = "divName";
                    ddlDepartment.DataValueField = "divId";
                    ddlDepartment.DataBind();

                    ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownSelectDivisions, CommonModule.dropDownZeroValue));
                }
            }
            else
            {
                ddlDepartment.Items.Clear();
                ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownSelectDivisions, CommonModule.dropDownZeroValue));
            }
        }
        else
        {
            ddlDepartment.Items.Clear();
            ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownSelectDivisions, CommonModule.dropDownZeroValue));
        }

    }


    protected void UpdateUser()
    {

        string QueryString = "0";
        QueryString = Convert.ToString(Request.QueryString["id"]);

        UserBM obj2 = new UserBM();
        obj2.useremailCheckDuplication(txtEmail.Text, Convert.ToInt32(QueryString));
        String returnMsg = obj2.ReturnString;
        if (returnMsg == "")
        {
            UserBM obj = new UserBM();
            // Get Cattegory list comma sap list. |Saurin | 20141216 |
            var category = string.Empty;
            var aa = new List<string>();
            //
            //dont mess with category :/
            // category = string.Join(",", (from RepeaterItem aItem in repCompCat.Items select (Repeater)aItem.FindControl("repCompAdd") into repCompAdd from RepeaterItem bItem in repCompAdd.Items let chkDisplayTitle = (CheckBoxList)bItem.FindControl("chkList") where chkDisplayTitle.SelectedItem.Selected select chkDisplayTitle.SelectedValue).ToArray());
            //
            foreach (RepeaterItem aItem in repCompCat.Items)
            {
                var repCompAdd = (Repeater)aItem.FindControl("repCompAdd");
                foreach (RepeaterItem bItem in repCompAdd.Items)
                {
                    CheckBoxList chkDisplayTitle = (CheckBoxList)bItem.FindControl("chkList");
                    Label hdnChkboxValue = (Label)bItem.FindControl("hdnChkboxValue");

                    foreach (ListItem item in chkDisplayTitle.Items)
                    {
                        if (item.Selected)
                        {
                            aa.Add(item.Value);
                            //string selectedValue = item.Value;
                        }
                    }


                    //if (!string.IsNullOrWhiteSpace(chkDisplayTitle.SelectedValue))
                    //{

                    //}
                }



            }
            category = string.Join(",", aa.ToArray());
            obj.userId = Convert.ToInt32(QueryString);
            obj.userFirstName = txtName.Text;
            obj.userLastName = txtlastName.Text;
            obj.userAddress = txtaddress.Text;
            obj.usercontact = txtMobile.Text;
            obj.userEmail = txtEmail.Text;
            obj.userUpdatedDate = DateTime.Now;
            obj.userJobType = Convert.ToInt32(ddljobtype.SelectedValue);
            obj.userdepId = Convert.ToString(ddlDepartment.SelectedValue);
            obj.TeamID = Convert.ToInt32(ddlTeam.SelectedValue);
            obj.UserCategory = category;
            obj.userGender = ddlgender.SelectedValue;
            obj.UpdateEmployees();

            EmployeeSkillBM skillobj = new EmployeeSkillBM();
            skillobj.skillIsDeleted = false;
            skillobj.skillIsActive = true;
            skillobj.skillCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
            skillobj.skillUserId = Convert.ToInt32(QueryString);
            skillobj.GetSkillByUserId();
            DataSet ds = skillobj.ds;
            if (!string.IsNullOrEmpty(category))
            {
                //string[] UpdatedCatId = category.Split(',');
                //foreach (var catid in UpdatedCatId)
                //{
                //    if (ds != null)
                //    {
                //        if (ds.Tables[0].Rows.Count > 0)
                //        {

                //            var chkFlag = 0;
                //            for (int i=0;i<ds.Tables[0].Rows.Count;i++)
                //            {
                //                if (catid == ds.Tables[0].Rows[i]["skillComId"])
                //                {
                //                    chkFlag = 1;
                //                }
                //            }

                //            if (chkFlag == 0)
                //            {
                //                Int32 CompId = 0;
                //                if (!string.IsNullOrEmpty(catid))
                //                {
                //                    CompId = Convert.ToInt32(catid);
                //                }


                //                //InsertSkill(QueryString, Convert.ToString(CompId));
                //            }

                //        }


                //    }
                //}
                var db = new startetkuEntities1();
                db.InsertIntoSkillMaster(3, Convert.ToInt32(QueryString), true, false, DateTime.Now, Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]), 0, 0, 0, string.Empty, true, category);

            }



            if (obj.ReturnBoolean == true)
            {
                switch (Convert.ToInt32(Session["OrguserType"]))
                {
                    case 2:
                        Response.Redirect("add_employee.aspx");
                        break;
                    case 1:
                        Response.Redirect("AllManager.aspx");
                        break;
                    default:
                        Response.Redirect("Employee_dashboard.aspx");
                        break;
                }
                //Response.Redirect("add_employee.aspx?msg=upd");
            }
        }
        else
        {
            if (returnMsg == "userEmail")
                lblMsg.Text = CommonModule.msgEmailAlreadyExists;
            else
                lblMsg.Text = CommonModule.msgProblemInsertRecord;
        }
    }

    public void InsertSkill(string QueryString, string comId)
    {
        EmployeeSkillBM skillobj = new EmployeeSkillBM();
        skillobj.skillComId = Convert.ToInt32(comId);
        skillobj.skillStatus = 3;
        skillobj.skillUserId = Convert.ToInt32(QueryString);
        skillobj.skillIsActive = true;
        skillobj.skillIsDeleted = false;
        skillobj.skillCreatedDate = DateTime.Now;
        skillobj.skillCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
        skillobj.skillLocal = 0;
        skillobj.skillAchive = 0;
        skillobj.skilltarget = 0;
        skillobj.skillComment = string.Empty;
        skillobj.InsertSkillMaster();
    }


    protected void GetCheckBoxListData()
    {
        CompetenceMasterBM obj = new CompetenceMasterBM();
        obj.catIsActive = true;
        obj.catIsDeleted = false;
        if (string.IsNullOrEmpty(ddlDepartment.SelectedValue))
        {
            NoRecords.Visible = true;
            return;
        }
        obj.comDivId = Convert.ToInt32(ddlDepartment.SelectedValue);
        obj.comJobtype = Convert.ToInt32(ddljobtype.SelectedValue);
        obj.GetAllCategoryByJobTypeId_DivId();
        // lblDivIdName.Text = ddlDepartment.SelectedItem.Text;

        DataSet ds = obj.ds;

        if (ds != null)
        {


            if (ds.Tables[0].Rows.Count > 0)
            {
                repCompCat.DataSource = ds.Tables[0];
                //chkList.DataTextField = "catName";
                //chkList.DataValueField = "catId";
                repCompCat.DataBind();
                //chkList.SelectedIndex = 0;

                NoRecords.Visible = false;
                //foreach (ListItem li in chkList.Items)
                //{
                //    li.Selected = true;
                //}
            }
            else
            {
                repCompCat.DataSource = null;
                //chkList.DataTextField = "catName";
                //chkList.DataValueField = "catId";
                repCompCat.DataBind();
                NoRecords.Visible = true;
                //chkList.Items.Clear();
                //ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownCategory, CommonModule.dropDownZeroValue));
            }

        }
        else
        {
            repCompCat.DataSource = null;
            //chkList.DataTextField = "catName";
            //chkList.DataValueField = "catId";
            repCompCat.DataBind();
            NoRecords.Visible = true;
            //chkList.Items.Clear();
        }
    }


    protected DataTable GetCheckBoxListDataJobType()
    {
        CompetenceMasterBM obj = new CompetenceMasterBM();
        obj.catIsActive = true;
        obj.catIsDeleted = false;
        if (string.IsNullOrEmpty(ddlDepartment.SelectedValue)) return null;
        obj.comDivId = Convert.ToInt32(ddlDepartment.SelectedValue);
        obj.comJobtype = Convert.ToInt32(ddljobtype.SelectedValue);
        obj.GetAllCategoryByJobTypeId_DivId();


        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[1].Rows.Count > 0)
            {
                return ds.Tables[1];


            }
        } return null;
    }

    protected void setDefault()
    {
        btnNext.Value = GetLocalResourceObject("Next.Text").ToString();
        Button2.Value = GetLocalResourceObject("Back.Text").ToString();
    }
    #endregion

    #region repeater
    protected void Repeater1_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DataRowView drv = (DataRowView)e.Item.DataItem;
            int categoryID = Convert.ToInt32(drv["catId"]);
            Label compCategoryName = (Label)e.Item.FindControl("lblCompCategoryName");
            var categoryName = Convert.ToString(drv["catName"]);
            var categoryNameDN = Convert.ToString(drv["catNameDN"]);

            if (Convert.ToString(Session["Culture"]) == "Danish")
            {
                compCategoryName.Text = categoryNameDN;

            }
            else
            {
                compCategoryName.Text = categoryName;
            }
            Repeater Repeater2 = (Repeater)e.Item.FindControl("repCompAdd");

            Repeater2.DataSource = GetAllCompetenceAddbyComCatId(categoryID);
            Repeater2.DataBind();



            foreach (RepeaterItem bItem in Repeater2.Items)
            {
                CheckBoxList chk = (CheckBoxList)bItem.FindControl("chkList");


                var data = GetAllCompetenceAddbyComCatId(categoryID);
                if (data == null) return;
                DataSet ds = data.DataSet;
                var catIds = Session["catIds"].ToString();
                /////////////
                for (int intCount = 0; intCount < ds.Tables[0].Rows.Count; intCount++)
                {

                    if (Convert.ToString(Session["Culture"]) == "Danish")
                    {
                        ds.Tables[0].Rows[intCount][3] += "  (" + categoryNameDN + ")";
                    }
                    else
                    {
                        ds.Tables[0].Rows[intCount][1] += "  (" + categoryName + ")";
                    }

                }

                ds.Tables[0].AcceptChanges();


                /////////////////
                if (Convert.ToInt32(ds.Tables[0].Rows[0]["catId"]) == categoryID)
                {
                    chk.DataSource = data;
                    if (Convert.ToString(Session["Culture"]) == "Danish")
                    {
                        chk.DataTextField = "catNameDN";
                    }
                    else
                    {
                        chk.DataTextField = "catName";
                    }
                    chk.DataValueField = "comId";

                    chk.DataBind();
                    foreach (ListItem li in chk.Items)
                    {
                        if (string.IsNullOrWhiteSpace(catIds))
                        {
                            li.Selected = true;
                        }

                        else
                        {
                            foreach (var j in catIds.Split(','))
                            {

                                if (li.Value == j)
                                {
                                    li.Selected = true;
                                    break;
                                }
                                else
                                {
                                    li.Selected = false;
                                }
                            }

                        }




                    }

                    break;

                }
            }
        }
    }

    protected void Repeater2_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //{
        //    CheckBoxList chkDisplayTitle = (CheckBoxList)e.Item.FindControl("chkList");
        //    foreach (ListItem item in chkDisplayTitle.Items)
        //    {
        //        item.Selected = false;
        //    }
        //}
        //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //{
        //    DataRowView drv = (DataRowView)e.Item.DataItem;
        //    int categoryID = Convert.ToInt32(drv["catId"]);
        //    CheckBoxList chk = (CheckBoxList)e.Item.FindControl("chkList");
        //    var data = GetAllCompetenceAddbyComCatId(categoryID);
        //    if (data == null) return;
        //    DataSet ds = data.DataSet;
        //    if (Convert.ToInt32(ds.Tables[0].Rows[0]["catId"]) == categoryID)
        //    {
        //        chk.DataSource = data;
        //        chk.DataTextField = "catName";
        //        chk.DataValueField = "catId";
        //        chk.DataBind();
        //    }
        //}
    }
    #endregion

    private DataTable GetAllCompetenceAddbyComCatId(int categoryId)
    {
        CompetenceMasterBM obj = new CompetenceMasterBM();
        obj.catIsActive = true;
        obj.catIsDeleted = false;
        if (categoryId < 0) return null;
        obj.comDivId = Convert.ToInt32(ddlDepartment.SelectedValue);
        obj.comJobtype = Convert.ToInt32(ddljobtype.SelectedValue);
        obj.comCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllCompetenceAddbyComCatId(categoryId);
        // lblDivIdName.Text = ddlDepartment.SelectedItem.Text;

        DataSet ds = obj.ds;

        if (ds != null)
        {
            int selectTable = 0;

            if (ds.Tables[selectTable].Rows.Count > 0)
            {
                return ds.Tables[selectTable];

            }
            else
            {
                //chkList.Items.Clear();
                //ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownCategory, CommonModule.dropDownZeroValue));
            }

        }
        return null;
    }

    protected void ss()
    {
        //foreach (RepeaterItem aItem in repCompCat.Items)
        //{
        //    var repCompAdd = (Repeater) aItem.FindControl("repCompAdd");

        //    CheckBoxList chk = (CheckBoxList)e.Item.FindControl("chkList");
        //    var data = GetAllCompetenceAddbyComCatId(categoryID);
        //    foreach (RepeaterItem bItem in repCompAdd.Items)
        //    {

        //    }
        //}
    }


    #region DropDown Event
    protected void ddlcompetence_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["catIds"] = "0";
        GetCheckBoxListData();


    }
    protected void ddljobtype_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["catIds"] = "0";
        GetCheckBoxListData();

    }


    #endregion

    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Request.QueryString["id"]))
        {
            UpdateUser();
        }

    }
    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        // string language = "French";
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Danish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion

    protected void btnCancel_OnClick(object sender, EventArgs e)
    {
        switch (Convert.ToInt32(Session["OrguserType"]))
        {
            case 2:
                Response.Redirect("add_employee.aspx");
                break;
            case 1:
                Response.Redirect("AllManager.aspx");
                break;
            default:
                Response.Redirect("Employee_dashboard.aspx");
                break;
        }
    }
}