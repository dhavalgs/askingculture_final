﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using System.Configuration;
using startetku.Business.Logic;
using System.Web.UI.HtmlControls;
using System.Threading;
using System.Globalization;
using starteku_BusinessLogic.Model;
using starteku_BusinessLogic.View;

public partial class Organisation_pending_request : System.Web.UI.Page
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }

        if (!IsPostBack)
        {

            Employee.InnerHtml = GetLocalResourceObject("Welcome.Text").ToString() + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            if (Convert.ToString(Session["OrguserType"]) == "1")
            {
                //sp1.InnerHtml = " Competence ";
                sp1.InnerHtml = GetLocalResourceObject("Competence.Text").ToString();
            }
            else
            {
                //sp1.InnerHtml = "Pending Request ";
                sp1.InnerHtml = GetLocalResourceObject("PendingRequest.Text").ToString();

            }

            GetAllEmployeeList();
            //EmployeeListArchiveAll();
            SetDefaultMessage();
        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion

    #region method
    protected void SetDefaultMessage()
    {
        // lblMsg.Text = GetLocalResourceObject("lblHelloWorld.Text").ToString();

        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["msg"])))
        {
            if (Convert.ToString(Request.QueryString["msg"]) == "del")
            {
                //lblMsg.Text = CommonModule.msgRecordHasBeenDeletedSuccessfully;
                //lblMsg.Text = CommonMessages.msgRecordHasBeenDeletedSuccessfully;
                lblMsg.Text = GetLocalResourceObject("msgRecordHasBeenDeletedSuccessfully.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "ins")
            {
                //lblMsg.Text = CommonMessages.msgRecordInsertedSuccessfully;
                lblMsg.Text = GetLocalResourceObject("msgRecordInsertedSuccessfully.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "upd")
            {
                //lblMsg.Text = CommonMessages.msgRecordUpdatedSuccss;
                lblMsg.Text = GetLocalResourceObject("msgRecordUpdatedSuccss.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "arc")
            {
                // lblMsg.Text = CommonMessages.msgRecordArchiveSucces;
                lblMsg.Text = GetLocalResourceObject("CommonMessages.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
        }

    }


    private SkillMasterView GetActReqInSkillMasterView(GetActivityRequestLists_Result sm)
    {


        var v = new SkillMasterView();
        v.name = sm.userFirstName;
        v.skillAcceptDate = sm.ActReqUpdateDate;
        v.skillAchive = 0;
        v.skillComId = 0;
        v.skillComment = "";
        v.skillCompanyId = sm.ActReqOrgId;
        v.skillCreatedDate = sm.ActReqCreateDate;
        v.skillId = sm.ActReqId;
        v.skillIsActive = sm.ActIsActive;
        v.skillIsApproved = sm.skillIsApproved;
        v.skillUserId = sm.ActReqUserId;
        v.status = GetActivityStatusByStatusId(sm.ActReqStatus, false);


        v.SkillType = "Activity Requests";


        return v;
    }
    public string GetActivityStatusByStatusId(string actReqStatus, bool isPrivate)
    {
        /*
     * About Activity Request Stauts:  2017 04 01
     
     * We need to change status on an activity
    Available” ==1
            Activity can be requested
            Should be auto set when creating a public activity
    Requested   ==2
            Activity have been requested by user
            Should be auto set when user request a public activity
    Request approved ==3
            Manager have approved requested activity
            Should be auto set when manager approve users activity request
    Ongoing ==4
            Should be auto set when start date have been reached
    Completed ==5
            employee flag activity has been completed
            Should be set manuel by user (or manager)
    Completed approval ==6
            Manager have approved that activity have been completed by user
            Should be set manual by manager (or system owner)
     * 
     * REJECTED == 7
    */
        //var status = isPrivate ? StringFor.CREATED : StringFor.Available;

        GetResource();
        var status = false ? StringFor.CREATED : StringFor.Available;

        switch (actReqStatus)
        {
            case "2":
                {
                    status = REQUESTED;//getResource.GetResource("REQUESTED.Text");//StringFor.REQUESTED;
                    break;
                }
            case "3":
                {
                    status = APPROVED;// getResource.GetResource("REQUESTED.Text"); //StringFor.APPROVED;
                    break;
                }
            case "4":
                {
                    status = ONGOING;// getResource.GetResource("REQUESTED.Text"); //StringFor.ONGOING;
                    break;
                }
            case "5":
                {
                    status = COMPLETED;// getResource.GetResource("REQUESTED.Text"); //StringFor.COMPLETED;
                    break;
                }
            case "6":
                {
                    status = COMPLETED_APPROVE;//getResource.GetResource("REQUESTED.Text"); //StringFor.COMPLETED_APPROVE;
                    break;
                }
            case "7":
                {
                    status = REJECTED;//getResource.GetResource("REQUESTED.Text"); //StringFor.REJECTED;
                    break;
                }


        }
        return status;
    }

    public string REQUESTED { get; set; }
    public string APPROVED { get; set; }
    public string ONGOING { get; set; }

    public string COMPLETED { get; set; }
    public string COMPLETED_APPROVE { get; set; }
    public string REJECTED { get; set; }

    private void GetResource()
    {
        REQUESTED = GetLocalResourceObject("REQUESTED.Text").ToString();
        APPROVED = GetLocalResourceObject("APPROVED.Text").ToString();
        ONGOING = GetLocalResourceObject("ONGOING.Text").ToString();

        COMPLETED = GetLocalResourceObject("COMPLETED.Text").ToString();
        COMPLETED_APPROVE = GetLocalResourceObject("COMPLETED_APPROVE.Text").ToString();
        REJECTED = GetLocalResourceObject("REJECTED.Text").ToString();
    }

    private SkillMasterView GetSkillMasterView(GetAllSkillPendingRequest_Result sm, string sts)
    {


        var v = new SkillMasterView();
        v.name = sm.name;
        v.skillAcceptDate = Convert.ToDateTime(sm.skillCreatedDate);

        v.skillComId = sm.skillUserId;
        //v.skillComment = sm.skillComment;
        // v.skillCompanyId = sm.skillCompanyId;
        v.skillCreatedDate = Convert.ToDateTime(sm.skillCreatedDate);
        // v.skillId = sm.skillId;
        // v.skillIsActive = sm.skillIsActive;
        // v.skillIsApproved = sm.skillIsApproved;
        v.skillUserId = sm.skillUserId;
        v.skillStatus = sm.skillStatus;
        v.SkillType = "Competence Setting";
        if (!string.IsNullOrEmpty(sts))
        {

            v.skillIsSaved = false;
            sm.skillIsSaved = false;
        }
        else
        {
            v.skillIsSaved = sm.skillIsSaved;
        }
        if (sm.skillIsSaved == false)
        {
            v.status = "Not Saved";
        }

        else
        {
            if (sm.skillStatus == 1)
            {
                v.status = hdnAccept.Value;
            }
            else if (sm.skillStatus == 2)
            {
                v.status = hdnCancel.Value;

            }
            else
            {
                v.status = hdnPending.Value;

            }
        }

        return v;
    }
    protected void GetAllEmployeeList()
    {
        EmployeeSkillBM obj = new EmployeeSkillBM();
        obj.skillIsActive = true;
        obj.skillIsDeleted = false;
        obj.skillStatus = 3;
        obj.skillCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.skillCreateBy = Convert.ToInt32(Session["OrgUserId"]);
        //obj.GetAllSkillPendingRequest();

        var db = new startetkuEntities1();
        var getData = db.GetAllSkillPendingRequest(false, true, Convert.ToInt32(Session["OrgCompanyId"]), 3, Convert.ToInt32(Session["OrgUserId"])).ToList();
        var skillMaster = new List<SkillMasterView>();
        if (getData.Any())
        {
            foreach (var sm in getData)
            {

                skillMaster.Add(GetSkillMasterView(sm, "false"));
            }
        }
        //DataSet ds = obj.ds;

        var userId = Convert.ToInt32(Session["OrgUserId"]);

        /*And Activity requests on All Requests and Pending Requests pages should hide if Company has disbaled for activities*/
        if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["Aemcompid"])))
        {
            var actReq = ActivityRequestLogic.GetActivityRequestList(userId).Where(o => o.ActReqStatus == "2");

            foreach (var i in actReq)
            {
                skillMaster.Add(GetActReqInSkillMasterView(i));
            }
        }
        if (skillMaster.Any())
        {

            gvGrid.DataSource = skillMaster;
            gvGrid.DataBind();
            gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;

        }
        else
        {
            gvGrid.DataSource = null;
            gvGrid.DataBind();
        }
    }
    #endregion

    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {

    }

    #endregion

    #region grid Event
    protected void gvGrid_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "archive")
        {
            EmployeeSkillBM obj = new EmployeeSkillBM();
            obj.skillId = Convert.ToInt32(e.CommandArgument);
            obj.skillIsActive = false;
            obj.skillIsDeleted = false;
            obj.SkillRequestStatusUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    GetAllEmployeeList();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }

    }
    protected void gvGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //foreach (TableCell tc in e.Row.Cells)
        //{
        //    tc.BorderStyle = BorderStyle.None;
        //}
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (Convert.ToString(Session["OrguserType"]) == "1")
            {
                HtmlGenericControl Edit = (HtmlGenericControl)e.Row.FindControl("Edit");
                HtmlGenericControl Delete = (HtmlGenericControl)e.Row.FindControl("Delete");
                Edit.Visible = true;
                Delete.Visible = true;

            }

            Label lblUserStatus = (Label)e.Row.FindControl("lblUserStatus");
            var status = lblUserStatus.Text;

            if (status == StringFor.REQUESTED)
            {
                lblUserStatus.Text = status;
            }

            else if (status == "Accept")
            {
                lblUserStatus.Text = hdnAccept.Value;
            }
            else if (status == "Cancel")
            {
                lblUserStatus.Text = hdnCancel.Value;

            }
            else
            {
                lblUserStatus.Text = hdnPending.Value;

            }
        }
    }

    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Denish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion

    protected void btnBack_Click(object sender, EventArgs e)
    {

        Response.Redirect("pending_helprequest.aspx");

        //switch (Convert.ToInt32(Session["OrguserType"]))
        //{
        //    case 2:
        //        Response.Redirect("Manager-dashboard.aspx");
        //        break;
        //    case 1:
        //        Response.Redirect("Organisation-dashboard.aspx");
        //        break;
        //    default:
        //        Response.Redirect("Employee_dashboard.aspx");
        //        break;
        //}
    }
}