﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;

public partial class Organisation_OrgCity : System.Web.UI.Page
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            City.InnerHtml = "Welcome   " + Convert.ToString(Session["OrgUserName"]) + "!";
            GetAllState();
            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                GetAllcitybyid();
            }
        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion


    #region method
    protected void SetDefaultMessage()
    {

        string id = "?id=";
        string childurl = "child.aspx";
        if (Request.RawUrl.ToString().ToLower().Contains(childurl.ToLower()))
        {
            if (Request.RawUrl.ToString().ToLower().Contains(id.ToLower()))
            {
                lblDataDisplayTitle.Text = "Edit Children";

            }
            else
            {
                lblDataDisplayTitle.Text = "Add Children";
            }
        }
    }
    protected void GetAllState()
    {
        StateBM obj = new StateBM();
        obj.staIsActive = true;
        obj.staIsDeleted = false;
        obj.staCompanyId = Convert.ToInt32(Session["OrgUserId"]);
        obj.GetAllState();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlstate.Items.Clear();

                ddlstate.DataSource = ds.Tables[0];
                ddlstate.DataTextField = "staName";
                ddlstate.DataValueField = "staId";
                ddlstate.DataBind();

                ddlstate.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
            }
            else
            {
                ddlstate.Items.Clear();
                ddlstate.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
            }
        }
        else
        {
            ddlstate.Items.Clear();
            ddlstate.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
        }

    }
    protected void Insertcity()
    {
        CityBM obj2 = new CityBM();
        obj2.CityCheckDuplication(txtcityName.Text, -1);
        String returnMsg = obj2.ReturnString;
        if (returnMsg == "")
        {
            CityBM obj = new CityBM();
            obj.citName = txtcityName.Text;
            obj.citStateId = Convert.ToInt32(ddlstate.SelectedValue);
            obj.citCompanyId = Convert.ToInt32(Session["OrgUserId"]);
            obj.citIsActive = true;
            obj.citIsDeleted = false;
            obj.citCreatedDate = DateTime.Now;
            obj.InsertCity();
            //  DataSet ds = obj.ds;
            if (obj.ReturnBoolean == true)
            {
                Response.Redirect("OrgCityList.aspx?msg=ins");
            }
        }
        else
        {
            if (returnMsg == "cityName")
                lblMsg.Text = CommonModule.msgNameAlreadyExists;
            else
                lblMsg.Text = CommonModule.msgProblemInsertRecord;
        }
    }
    protected void updatecity()
    {
        CityBM obj2 = new CityBM();
        obj2.CityCheckDuplication(txtcityName.Text, Convert.ToInt32(Request.QueryString["id"]));
        String returnMsg = obj2.ReturnString;
        if (returnMsg == "")
        {
            CityBM obj = new CityBM();
            obj.citId = Convert.ToInt32(Request.QueryString["id"]);
            obj.citName = txtcityName.Text;
            obj.citStateId = Convert.ToInt32(ddlstate.SelectedValue);
            obj.citUpdatedDate = DateTime.Now;
            obj.UpdateCity();
            //  DataSet ds = obj.ds;
            if (obj.ReturnBoolean == true)
            {
                Response.Redirect("OrgCityList.aspx?msg=upd");
            }
        }
        else
        {
            if (returnMsg == "cityName")
                lblMsg.Text = CommonModule.msgNameAlreadyExists;
            else
                lblMsg.Text = CommonModule.msgProblemInsertRecord;
        }

    }
    protected void GetAllcitybyid()
    {
        CityBM obj = new CityBM();
        obj.citId = Convert.ToInt32(Request.QueryString["id"]);
        obj.GetAllcitybyid();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["citName"])))
                txtcityName.Text = Convert.ToString(ds.Tables[0].Rows[0]["citName"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["citStateId"])))
                ddlstate.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["citStateId"]);
        }

    }
    #endregion

    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Request.QueryString["id"]))
        {
            updatecity();

        }
        else
        {
            Insertcity();
        }
    }
    protected void btnCancel_click(object sender, EventArgs e)
    {
        Response.Redirect("OrgCityList.aspx");
    }
    #endregion
}