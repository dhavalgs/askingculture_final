﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using starteku_BusinessLogic.View;
using startetku.Business.Logic;
using System.Web.Services;
using System.Threading;


public partial class Organisation_Calendar : System.Web.UI.Page
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            Calendar.InnerHtml = GetLocalResourceObject("Welcome.Text").ToString() +"  " + Convert.ToString(Session["OrgUserName"]) + "!";
            GetAllContact();
        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion

    #region method
    protected void insertnote()
    {
        NoteBM obj = new NoteBM();
        obj.noteUserId = Convert.ToInt32(Session["OrgUserId"]);
        obj.noteName = txtnote.Value;
        obj.noteCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.noteIsActive = true;
        obj.noteIsDeleted = false;
        obj.noteCreatedDate = DateTime.Now;
        obj.InsertNote();
        if (obj.ReturnBoolean == true)
        {
            GetAllContact();
            txtnote.Value = "";
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('" + CommonModule.msgSomeProblemOccure + "');</script>", false);
        }
    }
    protected void GetAllContact()
    {
        try
        {
            NoteBM obj = new NoteBM();
            obj.noteUserId = Convert.ToInt32(Session["OrgUserId"]);
            obj.noteCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            obj.noteIsActive = true;
            obj.noteIsDeleted = false;
            obj.GetAllNote();
            DataSet ds1 = obj.ds;
            if (ds1 != null)
            {
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    RepeaterNote.DataSource = ds1.Tables[0];
                    RepeaterNote.DataBind();

                    // Repeateremp.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                else
                {
                    RepeaterNote.DataSource = null;
                    RepeaterNote.DataBind();
                }
            }
            else
            {
                RepeaterNote.DataSource = null;
                RepeaterNote.DataBind();
            }
        }
        catch (Exception ex)
        {

        }
    }
    #endregion

    #region Button Event
    protected void btnnote_click(object sender, EventArgs e)
    {
        insertnote();
    }
    protected void Button_Click(object sender, EventArgs e)
    {
        //Get the reference of the clicked button.
        LinkButton button = (sender as LinkButton);
        //Get the command argument
        string commandArgument = button.CommandArgument;
        ////Get the Repeater Item reference
        //RepeaterItem item = button.NamingContainer as RepeaterItem;

        ////Get the repeater item index
        //int index = item.ItemIndex;

        NoteBM obj = new NoteBM();
        obj.noteId = Convert.ToInt32(commandArgument);
        obj.noteIsActive = false;
        obj.noteIsDeleted = false;
        obj.NoteStatusUpdate();
        DataSet ds = obj.ds;
        if (ds != null)
        {
            string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
            if (returnMsg == "success")
            {
                GetAllContact();
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('" + CommonModule.msgSomeProblemOccure + "');</script>", false);
            }
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('" + CommonModule.msgSomeProblemOccure + "');</script>", false);
        }
    }
    protected void btnSend_click(object sender, EventArgs e)
    {
        String lbl =Label1.Text;
    }
    #endregion

    #region webMethod 


    [WebMethod(EnableSession = true)]
    public static List<CalendarJsonView> GetCalendarData()
    {
        InvitationBM objAtt = new InvitationBM();
        objAtt.InvitationIsDeleted = false;
        objAtt.InvitationIsActive = true;
        //objAtt.InvitationCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
        objAtt.InvitationUserId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);

        objAtt.GetAllInvitationByUserId();
        DataSet ds = objAtt.ds;
        var abc = new List<CalendarJsonView>();

        if (ds != null)
        {
            var dr = ds.Tables[0];
            for (var i = 0; i < dr.Rows.Count; i++)
            {
                abc.Add(GetCalendarDataView(dr.Rows[i]));
            }

        }
        return abc;
    }

    private static CalendarJsonView GetCalendarDataView(DataRow dataRow)
    {
        var view = new CalendarJsonView();
        view.title = Convert.ToString(dataRow["InvitationSubject"]);
        view.start = Convert.ToDateTime(dataRow["InvitationStartDate"]).ToLongDateString();
        view.end = Convert.ToDateTime(dataRow["InvitationEndDate"]).ToLongDateString();
        view.message = Convert.ToString(dataRow["InvitationMessage"]);
       // view.allDay = false;

        return view;
    }

    [WebMethod(EnableSession = true)]
    public static string InsertDataCalendar(string title, string start, string end, string allDay)
    {
        if (string.IsNullOrWhiteSpace(title) || title=="null") return string.Empty;
        string msg = string.Empty;
        
        InvitationBM objAtt = new InvitationBM();
        objAtt.InvitationToEmail = string.Empty;
        try
        {
            objAtt.InvitationCreatedDate = DateTime.ParseExact(start.Substring(0, 24),
                              "ddd MMM d yyyy HH:mm:ss",
                              CultureInfo.InvariantCulture);
        }
        catch (Exception e)
        {
            
            
        }
        
        objAtt.InvitationIsDeleted = false;
        objAtt.InvitationIsActive = true;
        objAtt.InvitationCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
        objAtt.InvitationUserId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        objAtt.InvitationToUserId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        objAtt.InvitationSubject = title;
        objAtt.InvitationStartDate = DateTime.ParseExact(start.Substring(0, 24),
                              "ddd MMM d yyyy HH:mm:ss",
                              CultureInfo.InvariantCulture);
        objAtt.InvitationEndDate = DateTime.ParseExact(end.Substring(0, 24),
                              "ddd MMM d yyyy HH:mm:ss",
                              CultureInfo.InvariantCulture);
        objAtt.InvitationMessage = title;
        objAtt.InsertInvitation();
        if (objAtt.ReturnBoolean == true)
        {
            msg = "true";
        }
        else
        {
            msg = "false";
        }
        return msg;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<string> GetAutoCompleteCourt(string court)
    {
        List<string> result = new List<string>();

        //THEN LOOP THROUGH THE VALUES AND PRINT THEM TO THE XML USING THE METHOD BELOW
        //MyConnection cnn = new MyConnection();
        //SqlCommand cmd = new SqlCommand("GetActs", cnn.GetConnection());
        //SqlCommand cmd = new SqlCommand("GetCourtsForFilter", cnn.GetConnection());
        //cmd.CommandType = CommandType.StoredProcedure;
        //cmd.Parameters.Add(new SqlParameter("@ActLike", '%' + "str" + '%'));
        //cmd.Parameters.Add(new SqlParameter("@CourtLike", '%' + court + '%'));
        //DBOperations dbopr = new DBOperations();
        UserBM obj = new UserBM();
        obj.userFirstName = Convert.ToString(court);
        obj.userCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
        obj.userIsActive = true;
        obj.userIsDeleted = false;
        obj.GetAllEmployeeByCompanyId();
        DataSet ds = obj.ds;

        if (ds.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow dr in ds.Tables[0].Rows)
            {

                result.Add(dr["userEmail"].ToString());
            }
            return result;
        }
        return result;
    }
    [WebMethod(EnableSession = true)]
    public static string InsertData(string txtto, string txtsubject, string txtmessge, string add)
    {
        string msg = string.Empty;
        string[] str = add.Split('-');
        InvitationBM objAtt = new InvitationBM();
        objAtt.InvitationToEmail = txtto;
        objAtt.InvitationCreatedDate = DateTime.Now;
        objAtt.InvitationIsDeleted = false;
        objAtt.InvitationIsActive = true;
        objAtt.InvitationCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
        objAtt.InvitationUserId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        objAtt.InvitationToUserId = GetIdByEmailId(txtto);//tes
        objAtt.InvitationSubject = txtsubject;
        objAtt.InvitationStartDate = Convert.ToDateTime(str[0]);
        objAtt.InvitationEndDate = Convert.ToDateTime(str[1]);
        objAtt.InvitationMessage = txtmessge;
        objAtt.InsertInvitation();
        if (objAtt.ReturnBoolean == true)
        {
            msg = "true";
        }
        else
        {
            msg = "false";
        }
        return msg;
    }

    private static int GetIdByEmailId(string email)
    {
       UserBM Cust = new UserBM();
       Cust.userEmail = email.Trim();
        Cust.SelectPasswordByUserName();
        DataSet ds = Cust.ds;
                    
        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {

              
                return (Convert.ToInt32(ds.Tables[0].Rows[0]["userId"]));
            }
        }
        return 0;
    }

    #endregion

    protected void txt_name_TextChanged(object sender, EventArgs e)
    {
        throw new NotImplementedException();
    }

    #region Culture
    protected override void InitializeCulture()
    {
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Danish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();
            //if (!string.IsNullOrEmpty(language))
            //{
            //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
            //    {
            //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
            //        {
            //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
            //        }
            //    }
            //    //if (language.EndsWith("Danish")) languageId = "da-DK";
            //    //else languageId = "en-GB";
            //    
            //}
            SetCulture(languageId);
        }

        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion
}

public class CalendarJsonView
{
    public string message{ get; set; }
    public string start { get; set; }
    public string end { get; set; }
    public string title { get; set; }
   /* public bool allDay { get; set; }*/

}