﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using System.Globalization;
using System.Threading;
using System.Resources;
using System.Reflection;
using System.Collections;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using starteku_BusinessLogic.View;


public partial class Organisation_ContactList : System.Web.UI.Page
{

    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }

        if (!IsPostBack)
        {
            Conatct.InnerHtml = CommonMessages.Welcome + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            GetAllEmployeeByCompanyId();
            GetAllContact();//GetTab1Contact();
        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion

    #region Methods
   
    //TAB 0
    protected void GetAllContact()
    {
        try
        {
            ContactBM obj = new ContactBM();
            obj.UserId = Convert.ToInt32(Session["OrgUserId"]);
            obj.ContactIsActive = true;
            obj.ContactIsDeleted = false;
            obj.GetAllContact();
            DataSet ds = obj.ds;
            DataView dv = new DataView();
            dv = ds.Tables[0].DefaultView;
            dv.RowFilter = "userFullname LIKE '%" + Convert.ToString(txtsearch.Text.Trim()) + "%'";
            DataTable dtitm = dv.ToTable();
            DataSet ds1 = new DataSet();
            ds1.Tables.Add(dtitm);

            if (ds1.Tables[0].Rows.Count > 0)
            {
                rep_contact.DataSource = ds1;
                rep_contact.DataBind();
               
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void GetAllEmployeeByCompanyId()
    {
        try
        {
            UserBM obj = new UserBM();
            obj.userCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            obj.userIsActive = true;
            obj.userIsDeleted = false;
            obj.GetAllEmployeeByCompanyId();
            DataSet ds = obj.ds;

            if (ds.Tables[0].Rows.Count > 0)
            {
                Dictionary<int, string> list_contact = new Dictionary<int, string>();

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    list_contact.Add(
                        Convert.ToInt32(ds.Tables[0].Rows[i]["userId"]),
                        Convert.ToString(ds.Tables[0].Rows[i]["name"])
                        );
                }

                ViewState["ContactList"] = list_contact;

            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void clear()
    {
        txt_name.Text = "";
        txt_address.Text = "";
        txt_email.Text = "";
        txt_phone.Text = "";
    }
    protected void txtsearch_TextChanged(object sender, EventArgs e)
    {
        GetAllContact();

    }
    #endregion
    public static List<UserMasterView> UserList = new List<UserMasterView>();
    #region Events
    protected void txt_name_TextChanged(object sender, EventArgs e)
    {

        UserBM obj = new UserBM();

        var empName = txt_name.Text;
        empName = (empName.Trim()).Replace(' ', '%');//Help to get proper result. //saurin | 20150107
        obj.UserFullName = empName;
        obj.GetAllEmployeeByName();
        DataSet ds = obj.ds;
        UserList = new List<UserMasterView>();
        if (ds.Tables[0].Rows.Count > 0)
        {
            UserList.Add(GetUserView(ds));

            txt_address.Text = Convert.ToString(ds.Tables[0].Rows[0]["userAddress"]);
            txt_email.Text = Convert.ToString(ds.Tables[0].Rows[0]["userEmail"]);
            txt_phone.Text = Convert.ToString(ds.Tables[0].Rows[0]["usercontact"]);
            hf_userid.Value = Convert.ToString(ds.Tables[0].Rows[0]["userId"]);
            hf_jobid.Value = Convert.ToString(ds.Tables[0].Rows[0]["userJobType"]);
            hf_userlevel.Value = Convert.ToString(ds.Tables[0].Rows[0]["userLevel"]);
            hdnImage.Value = Convert.ToString(ds.Tables[0].Rows[0]["userImage"]);
        }

        ////pnlresult.Visible = true;
        //if (!string.IsNullOrEmpty(txt_name.Text.Trim()))
        //{
        //    Dictionary<int, string> ContactList = (Dictionary<int, string>)ViewState["ContactList"];
        //    if (ContactList.Count > 0)
        //    {
        //        var content = from c in ContactList
        //                      where (c.Value.ToLower()).Contains(txt_name.Text.ToLower())
        //                      select new
        //                      {
        //                          c.Key,
        //                          c.Value
        //                      };
        //        //lst_contacts.DataSource = null;
        //        //lst_contacts.DataSource = content;
        //        //lst_contacts.DataBind();
        //    }
        //}
        //else
        //{
        //    lst_contacts.DataSource = null;
        //    lst_contacts.DataBind();
        //}

        //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>AnotherFunction();</script>", false);


    }

    private UserMasterView GetUserView(DataSet ds)
    {
        var view = new UserMasterView();
        view.Address = Convert.ToString(ds.Tables[0].Rows[0]["userAddress"]);
        view.UserEmail = Convert.ToString(ds.Tables[0].Rows[0]["userEmail"]);
        view.Contact = Convert.ToString(ds.Tables[0].Rows[0]["usercontact"]);
        view.UserId = Convert.ToInt32(ds.Tables[0].Rows[0]["userId"]);
        if (!string.IsNullOrWhiteSpace(Convert.ToString(ds.Tables[0].Rows[0]["userJobType"])))
            view.UserJobType = Convert.ToInt32(ds.Tables[0].Rows[0]["userJobType"]);

        if (!string.IsNullOrWhiteSpace(Convert.ToString(ds.Tables[0].Rows[0]["userLevel"])))
            view.UserLevel = Convert.ToInt32(ds.Tables[0].Rows[0]["userLevel"]);

        return view;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<string> GetAutoCompleteCourt(string court)
    {
        List<string> result = new List<string>();

        //THEN LOOP THROUGH THE VALUES AND PRINT THEM TO THE XML USING THE METHOD BELOW
        //MyConnection cnn = new MyConnection();
        //SqlCommand cmd = new SqlCommand("GetActs", cnn.GetConnection());
        //SqlCommand cmd = new SqlCommand("GetCourtsForFilter", cnn.GetConnection());
        //cmd.CommandType = CommandType.StoredProcedure;
        //cmd.Parameters.Add(new SqlParameter("@ActLike", '%' + "str" + '%'));
        //cmd.Parameters.Add(new SqlParameter("@CourtLike", '%' + court + '%'));
        //DBOperations dbopr = new DBOperations();
        UserBM obj = new UserBM();
        obj.userFirstName = Convert.ToString(court);
        obj.userCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
        obj.userIsActive = true;
        obj.userIsDeleted = false;
        obj.GetAllEmployeeByCompanyId();
        DataSet ds = obj.ds;

        if (ds.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow dr in ds.Tables[0].Rows)
            {

                result.Add(dr["name"].ToString());
            }
            return result;
        }
        return result;
    }

    protected void lb_contact_Click(object sender, EventArgs e)
    {
        LinkButton lb = (LinkButton)sender;
        UserBM obj = new UserBM();
        obj.userId = Convert.ToInt32(lb.CommandName);
        obj.GetEmployeedetailsbyid();
        DataSet ds = obj.ds;
        if (ds.Tables[0].Rows.Count > 0)
        {
            txt_name.Text = lb.Text;
            txt_address.Text = Convert.ToString(ds.Tables[0].Rows[0]["userAddress"]);
            txt_email.Text = Convert.ToString(ds.Tables[0].Rows[0]["userEmail"]);
            txt_phone.Text = Convert.ToString(ds.Tables[0].Rows[0]["usercontact"]);
            hf_userid.Value = lb.CommandName;
            hf_jobid.Value = Convert.ToString(ds.Tables[0].Rows[0]["UserJobId"]);


            hf_userlevel.Value = Convert.ToString(ds.Tables[0].Rows[0]["UserLevel"]);
        }
        //pnlresult.Visible = false;
    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        return;
        ContactBM obj = new ContactBM();

        obj.UserId = Convert.ToInt32(Session["OrgUserId"]);

        if (!string.IsNullOrWhiteSpace(hf_userid.Value)) obj.ContactUserId = Convert.ToInt32(hf_userid.Value);
        obj.ContactCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        if (!string.IsNullOrWhiteSpace(hf_jobid.Value)) obj.ContactJobId = Convert.ToInt32(hf_jobid.Value);
        if (!string.IsNullOrWhiteSpace(hf_userlevel.Value)) obj.ContactLevel = Convert.ToInt32(hf_userlevel.Value);
        obj.ContactCreatedDate = DateTime.Now;
        obj.ContactIsActive = true;
        obj.ContactIsDeleted = false;

        Boolean status = obj.InsertContact();
        if (status)
        {
            GetAllContact();
            clear();
        }
        else
        {
            string lbl_message = "Contact already added or not found.";
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('" + lbl_message + "')</script>", false);
        }

    }
    [WebMethod(EnableSession = true)]
    // [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string BtnSubmitAJAX(string txtName, string txtDesc)
    {
        ContactBM obj = new ContactBM();
        var aa = obj.UserId;
        obj.UserId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        var userDetail = UserList.FirstOrDefault();
        if (userDetail != null)
        {
            obj.ContactUserId = userDetail.UserId;

            obj.ContactCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
            obj.ContactJobId = userDetail.UserJobType;
            obj.ContactLevel = userDetail.UserLevel;
            obj.ContactCreatedDate = DateTime.Now;
            obj.ContactIsActive = true;
            obj.ContactIsDeleted = false;
        }
        Boolean status = obj.InsertContact();
        if (status)
        {
            return "success";
           
        }
        else
        {
            return "Contact already added or not found.";
            //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('" + lbl_message + "')</script>", false);
        }
        return "";
    }

    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        // string language = "French";
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Danish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion

    protected void refreshPage(object sender, EventArgs e)
    {
        GetAllContact();
        clear();
    }

}