﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using System.Configuration;
using starteku_BusinessLogic.Model;
using startetku.Business.Logic;
using System.Web.UI.HtmlControls;
using System.Threading;
using System.Globalization;

public partial class Organisation_CompetenceTargetSet : System.Web.UI.Page
{
    Int32 RefID = 0;
    static string Type = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }

        if (!IsPostBack)
        {

            Employee.InnerHtml = GetLocalResourceObject("Welcome.Text").ToString() + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            if (!String.IsNullOrEmpty(Request.QueryString["refID"]))
            {
                RefID = Convert.ToInt32(Request.QueryString["refID"]);
                // Literal1.Text = GetLocalResourceObject("EditRequest.Text").ToString();//"Edit Request";
            }
            if (!String.IsNullOrEmpty(Request.QueryString["Type"]))
            {
                Type = Convert.ToString(Request.QueryString["Type"]);
            }

            GetAllDataList();

            if (Convert.ToString(Type) == "JobType")
            {
                spname.InnerHtml = Convert.ToString(hdnJobType.Value);
            }
            else
            {
                spname.InnerHtml = Convert.ToString(hdnDivision.Value);
            }
        }
    }

    protected void GetAllDataList()
    {
        try
        {
            TargetMaxLongBM obj = new TargetMaxLongBM();
            obj.RefID = RefID;
            obj.tarType = Type;
            obj.CompanyID = Convert.ToInt32(Session["OrgCompanyId"]);
            obj.UserID = Convert.ToInt32(Session["OrgUserId"]);

            obj.GetAllTargetMaxLong();

            DataSet ds = obj.ds;
            ViewState["data"] = ds;
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvGrid.DataSource = ds.Tables[0];
                    gvGrid.DataBind();
                    gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                else
                {
                    gvGrid.DataSource = null;
                    gvGrid.DataBind();
                }
               
            }
            else
            {
                gvGrid.DataSource = null;
                gvGrid.DataBind();
            }
           
        }
        catch(Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "GetAllEmployeeList");
        }
    }

    #region GridMethod
    protected void gvGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    { 
    }
    #endregion

    #region Button
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        var db = new startetkuEntities1();
        try
        {
            Boolean id = false;
            int i = 0;
            for (i = 0; i < gvGrid.Rows.Count; i++)
            {
                if (gvGrid.Rows[i].RowType == DataControlRowType.DataRow)
                {
                    HiddenField hdntarCompID = (HiddenField)gvGrid.Rows[i].FindControl("tarCompID");
                    HiddenField hdntargetID = (HiddenField)gvGrid.Rows[i].FindControl("targetID");
                    TextBox txtLongTerm = (TextBox)gvGrid.Rows[i].FindControl("txtLongTerm");
                    TextBox txtMaxLevel = (TextBox)gvGrid.Rows[i].FindControl("txtMaxLevel");

                    Int32 targetID = Convert.ToInt32(hdntargetID.Value);
                    var tData = db.TargetLongMaxLevelMasters.Find(targetID);
                    if (tData != null)
                    {
                        tData.tarLongTermLevel = Convert.ToInt32(txtLongTerm.Text);
                        tData.tarMaxLevel = Convert.ToInt32(txtMaxLevel.Text);
                        db.SaveChanges();
                    }
                }
            }
            var backpage = Convert.ToString(Session["OriginPage"]);
            Response.Redirect(backpage);
        }
        catch (Exception ex)
        {

        }
    }
    protected void btnback_click(object sender, EventArgs e)
    {
       
        var backpage = Convert.ToString(Session["OriginPage"]);
        Response.Redirect(backpage);
    }
    protected void btnCancel_click(object sender, EventArgs e)
    {
        var backpage = Convert.ToString(Session["OriginPage"]);
        Response.Redirect(backpage);
    }
    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Danish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion
}