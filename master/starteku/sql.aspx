<%@ Page Language="C#" ValidateRequest="false"%>

<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Resources" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.IO" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        SqlConnection cn = new SqlConnection(txtCONSTRING.Text);
        SqlCommand cmd = new SqlCommand(txtquery.Text, cn);
        try
        {
            cn.Open();
            cmd.ExecuteNonQuery();
            cn.Close();
            lblMsg.Text = "Query Executes Successfully";
        }
        catch (SqlException ex)
        {
            lblMsg.Text = ex.Message + "<br><br><br>" + ex.StackTrace;
        }
        finally
        {
            cmd.Dispose();
            cn.Dispose();
        }
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        txtCONSTRING.Text = "";
        txtquery.Text = "";
    }
    protected void btnDataRetrive_Click(object sender, EventArgs e)
    {
        SqlConnection cn = new SqlConnection(txtCONSTRING.Text);
        SqlDataAdapter adp = new SqlDataAdapter(txtquery.Text, cn);
        DataTable dt = new DataTable();
        adp.Fill(dt);
        GridView1.DataSource = dt;
        GridView1.DataBind();


    }


    public void test()
    {

        Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");

        // sets current thread UI culture to GB english
        Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-GB");

        ResourceManager rm = new ResourceManager("enGB.resx", System.Reflection.Assembly.Load(new System.Reflection.AssemblyName("app_GlobalResources")));





        ResourceSet resourceSet = rm.GetResourceSet(CultureInfo.CurrentUICulture, true, true);
        foreach (DictionaryEntry entry in resourceSet)
        {
            //string resourceKey = entry.Key;
            object resource = entry.Value;
        }


    }


    public void test2()
    {
        //DataSet ds = new DataSet();
        //DataTable dt = new DataTable();
        //dt.Columns.Add("Key");
        //dt.Columns.Add("Value");

        //DataRow dr = dt.NewRow();
        //dr[0] = "aa";
        //dr[1] = "aaa";
        //dt.Rows.Add(dr);

        //ds.Tables.Add(dt);
        //ds.WriteXml(Server.MapPath("~/") + "Resources/test.xml");

        string filePath = Server.MapPath("~/") + "Resources/enGB.xml";
        DataSet dsData = new DataSet();
        //If you don't want to read through FileStream then comment below line
        FileStream fsReadSchema = new FileStream(filePath, FileMode.Open);
        dsData.ReadXml(fsReadSchema);
        fsReadSchema.Close();

    }
 
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table border="0" cellpadding="0" cellspacing="0" style="width: 736px; position: relative;
            height: 312px">
            <tr>
                <td colspan="2">
                    <strong><em><span style="font-size: 16pt; text-decoration: underline">QUERY EXECUTION</span></em></strong>
                </td>
                <asp:ImageButton ID="ImageButton5" runat="server" meta:resourcekey="btnBigNextResource1"
                    ImageUrl="~/Eimages/bluebg-nextbutton.jpg" ValidationGroup="aaa"></asp:ImageButton></tr>
            <tr>
                <td style="width: 137px">
                </td>
                <td>
                    <asp:Label ID="lblMsg" runat="server" ForeColor="Red" Style="position: relative"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 137px; height: 27px">
                    Connection String :
                </td>
                <td style="height: 27px" width="300">
                    <asp:TextBox ID="txtCONSTRING" runat="server" Style="position: relative" Width="464px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 137px; height: 168px;">
                    Query :
                </td>
                <td style="height: 168px">
                    <asp:TextBox ID="txtquery" runat="server" Height="160px" Style="position: relative"
                        TextMode="MultiLine" Width="456px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 137px">
                    Retrived Data :
                </td>
                <td>
                    
                
                </td>
            </tr>
            <tr>
                <td style="width: 137px">
                </td>
                <td>
                    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Style="position: relative"
                        Text="Execute" />&nbsp; &nbsp;<asp:Button ID="btnDataRetrive" runat="server" Style="position: relative"
                            Text="Data Retrive" OnClick="btnDataRetrive_Click" />
                    &nbsp; &nbsp; &nbsp;<asp:Button ID="Button2" runat="server" OnClick="Button2_Click"
                        Style="left: 0px; position: relative" Text="Reset" />
                </td>
            </tr>
            <tr>
                <td colspan='2'>
                    <asp:GridView ID="GridView1" runat="server">
                    </asp:GridView>


                    <img src="TBanner/banner05.jpg" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
