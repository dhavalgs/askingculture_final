﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_code;
using starteku_BusinessLogic;
using System.Data;
using System.Configuration;
using System.IO;
using System.Threading;
using System.Globalization;

public partial class webadmin_Registration : System.Web.UI.Page
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GetAllCountry();
            GetAllDepartmentsByPerentId();
            GetAllDivisionsByPerentId();

            if (!String.IsNullOrEmpty(Request.QueryString["org"]))
            {
                this.Master.FindControl("menu").Visible = false;
            }
            else if (!String.IsNullOrEmpty(Request.QueryString["emp"]))
            {
                state.Attributes.Add("style", "display:block");
                // city.Attributes.Add("style", "display:block");
                GetAllEmployeebyid();
            }

        }
    }
    #endregion

    #region method
    protected void SetDefaultMessage()
    {
        string id = "?id=";
        string childurl = "child.aspx";
        if (Request.RawUrl.ToString().ToLower().Contains(childurl.ToLower()))
        {
            if (Request.RawUrl.ToString().ToLower().Contains(id.ToLower()))
            {
                lblDataDisplayTitle.Text = "Edit Children";

            }
            else
            {
                lblDataDisplayTitle.Text = "Add Children";
            }
        }
    }
    protected void GetAllCountry()
    {
        CountryBM obj = new CountryBM();
        obj.couIsActive = true;
        obj.couIsDeleted = false;
        //obj.GetAllCountry
        obj.GetAllCountrymaster();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlcountry.Items.Clear();
                ddlcountry.DataSource = ds.Tables[0];
                ddlcountry.DataTextField = "couName";
                ddlcountry.DataValueField = "couId";
                ddlcountry.DataBind();
                if (String.IsNullOrEmpty(Request.QueryString["emp"]))
                {
                    ddlcountry.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));

                }
                else
                {
                    GetState(Convert.ToInt32(ddlcountry.SelectedValue));
                }
            }
            else
            {
                ddlcountry.Items.Clear();
                ddlcountry.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
            }
        }
        else
        {
            ddlcountry.Items.Clear();
            ddlcountry.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
        }


    }
    protected void GetState(int contryid)
    {
        state.Attributes.Add("style", "display:block");

        StateBM obj = new StateBM();
        obj.staCountryId = contryid;
        obj.staIsActive = true;
        obj.staIsDeleted = false;
        //obj.GetStatebyCountryid();
        obj.GetStatebyCountryidMaster();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlstate.Items.Clear();
                ddlstate.DataSource = ds.Tables[0];
                ddlstate.DataTextField = "staName";
                ddlstate.DataValueField = "staId";
                ddlstate.DataBind();

                //  ddlstate.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
            }
            else
            {
                ddlstate.Items.Clear();
                ddlstate.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
            }
        }
        else
        {
            ddlstate.Items.Clear();
            ddlstate.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
        }
        //Getcity(Convert.ToInt32(ddlstate.SelectedValue));
    }
    //protected void Getcity(int stateid)
    //{
    //    city.Attributes.Add("style", "display:block");

    //    CityBM obj = new CityBM();
    //    obj.citStateId = stateid;
    //    obj.citIsActive = true;
    //    obj.citIsDeleted = false;
    //    obj.Getcitybystateid();
    //    DataSet ds = obj.ds;

    //    if (ds != null)
    //    {
    //        if (ds.Tables[0].Rows.Count > 0)
    //        {
    //            ddlcity.Items.Clear();
    //            ddlcity.DataSource = ds.Tables[0];
    //            ddlcity.DataTextField = "citName";
    //            ddlcity.DataValueField = "citId";
    //            ddlcity.DataBind();

    //            //  ddlstate.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
    //        }
    //        else
    //        {
    //            ddlcity.Items.Clear();
    //            ddlcity.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
    //        }
    //    }
    //    else
    //    {
    //        ddlcity.Items.Clear();
    //        ddlcity.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
    //    }

    //}
    protected void redirectpage(string msg, string paage)
    {
        //string message = "You will now be redirected to ASPSnippets Home Page.";
        string message = msg;
        //string url = "Login.aspx?msg=ins";
        string url = paage;
        string script = "window.onload = function(){ alert('";
        script += message;
        script += "');";
        script += "window.location = '";
        script += url;
        script += "'; }";
        ClientScript.RegisterStartupScript(this.GetType(), "Redirect", script, true);

    }
    protected void Insertuser()
    {
        string filePath = "";
        Common.WriteLog("Insertuser");
        Int32 Id = 0;
        String udivId = "";
        String udepId = "";
        UserBM obj2 = new UserBM();
        obj2.useremailCheckDuplication(txtEmail.Text, -1);
        String returnMsg = obj2.ReturnString;
        if (returnMsg == "")
        {
            UserBM obj = new UserBM();
            obj.comname = txtcomname.Text;
            obj.comAddress = txtcaddress.Text;
            obj.comphone = txtcphone.Text;
            obj.userFirstName = txtfName.Text;
            obj.userLastName = txtlname.Text;
            obj.userZip = txtzip.Text;
            obj.userAddress = txtaddress.Text;
            obj.userCountryId = Convert.ToInt32(ddlcountry.SelectedValue);
            obj.userStateId = Convert.ToInt32(ddlstate.SelectedValue);
            //obj.userCityId = Convert.ToInt32(ddlcity.SelectedValue);
            obj.userCityId = txtcity.Text;
            obj.usercontact = txtMobile.Text;
            obj.userEmail = txtEmail.Text;
            obj.userGender = ddlgender.SelectedValue;
            if (flupload1.HasFile)
            {
                filePath = System.DateTime.Now.ToString("MMddyyhhmmss") + "_" + flupload1.PostedFile.FileName;
                flupload1.SaveAs(Server.MapPath("../") + "Log/upload/Userimage/" + System.IO.Path.GetFileName(filePath.Replace(' ', '0')));
                obj.userImage = System.IO.Path.GetFileName(filePath.Replace(' ', '0'));

            }
            else
            {
                if (ddlgender.SelectedValue == "Male")
                {
                    obj.userImage = "male.png";
                }
                else
                {//ViewState["userImage"]
                    obj.userImage = "female.png";
                }
            }


            if (!String.IsNullOrEmpty(Request.QueryString["org"]))
            {
                obj.userType = 1;
            }
            else if (!String.IsNullOrEmpty(Request.QueryString["emp"]))
            {
                obj.userType = 2;
            }
            else
            {
                obj.userType = 1;
            }
            //obj.userCityId = Convert.ToInt32(ddlcity.SelectedValue);
            obj.userCreatedDate = DateTime.Now;
            obj.userIsActive = true;
            obj.userIsDeleted = false;
            if (!String.IsNullOrEmpty(Request.QueryString["org"]))
            {
                obj.userCompanyId = 0;
            }
            else if (!String.IsNullOrEmpty(Request.QueryString["emp"]))
            {
                obj.userCompanyId = 0;
            }
            else
            {
                obj.userCompanyId = 0;
            }
            obj.userPassword = CommonModule.encrypt(txtPassword.Text.Trim());
            if (txtfromdate.Text != "")
            {
                obj.userDOB = CommonUtility.ConvertStringToDateTime(txtfromdate.Text);// Convert.ToDateTime(CommonModule.FormatdtEnter(txtfromdate.Text));
               // obj.userDOB = Convert.ToDateTime(CommonModule.FormatdtEnter(txtfromdate.Text));
            }

            string userdepId = string.Empty;
            for (int i = 0; i < chk_list_Departments.Items.Count; i++)
            {
                if (chk_list_Departments.Items[i].Selected == true)
                {
                    if (string.IsNullOrEmpty(userdepId))
                    {
                        userdepId = "," + chk_list_Departments.Items[i].Value + ",";
                    }
                    else
                    {
                        userdepId = userdepId + chk_list_Departments.Items[i].Value + ",";
                    }
                }
            }
            obj.userdepId = userdepId;

            string userdivId = string.Empty;
            for (int i = 0; i < chk_list_Division.Items.Count; i++)
            {
                if (chk_list_Division.Items[i].Selected == true)
                {
                    if (string.IsNullOrEmpty(userdivId))
                    {
                        userdivId = "," + chk_list_Division.Items[i].Value + ",";
                    }
                    else
                    {
                        userdivId = userdivId + chk_list_Division.Items[i].Value + ",";
                    }
                }
            }
            obj.userdivId = userdivId;


            obj.InsertUser();

            #region thumbnail
            if (File.Exists(MapPath("../") + "Log/Upload/Userimage/" + System.IO.Path.GetFileName(filePath.Replace(' ', '0'))))
            {
                // flupload1.SaveAs(Server.MapPath("../") + "Log/upload/Userimage/" + System.IO.Path.GetFileName(filePath.Replace(' ', '0')));
                System.Drawing.Image img = System.Drawing.Image.FromFile(Server.MapPath("../") + "Log/Upload/Userimage/" + System.IO.Path.GetFileName(filePath.Replace(' ', '0')));
                System.Drawing.Image singleProduct = img.GetThumbnailImage(100, 100, null, IntPtr.Zero);
                // System.Drawing.Image imageSlider = img.GetThumbnailImage(200, 200, null, IntPtr.Zero);
                //System.Drawing.Image product = img.GetThumbnailImage(273, 204, null, IntPtr.Zero);
                img.Dispose();
                singleProduct.Save(Server.MapPath("../") + "Log/Upload/thumbnail/" + System.IO.Path.GetFileName(filePath.Replace(' ', '0')));
                //imageSlider.Save(Server.MapPath("../") + "Upload/new/" + System.IO.Path.GetFileName(filePath.Replace(' ', '0')));
                //product.Save(Server.MapPath("../") + "Upload/product/" + System.IO.Path.GetFileName(filePath.Replace(' ', '0')));

            }
            #endregion

            DataSet ds = obj.ds;
            if (CommonModule.IsDatasetvalid(ds))
            {
                //Id = Convert.ToInt32(ds.Tables[0].Rows[0]["userId"]);
                //UserBM obj1 = new UserBM();
                //obj1.userId = Convert.ToInt32(ds.Tables[0].Rows[0]["userId"]);
                //obj1.GetAllEmployeebyid();
                //DataSet ds1 = obj1.ds;
                //if (CommonModule.IsDatasetvalid(ds1))
                //{
                //    if (!string.IsNullOrEmpty(Convert.ToString(ds1.Tables[0].Rows[0]["userId"])))
                //        Id = Convert.ToInt32(ds1.Tables[0].Rows[0]["userId"]);

                //    if (!string.IsNullOrEmpty(Convert.ToString(ds1.Tables[0].Rows[0]["userdepId"])))
                //        udepId = Convert.ToString(ds1.Tables[0].Rows[0]["userdepId"]);

                //    if (!string.IsNullOrEmpty(Convert.ToString(ds1.Tables[0].Rows[0]["userdivId"])))
                //        udivId = Convert.ToString(ds1.Tables[0].Rows[0]["userdivId"]);
                //}
                //InsertBulkdata(Id, udepId, udivId);


            }

            if (obj.ReturnBoolean == true)
            {
                try
                {
                    Common.WriteLog("mail");
                    Common.WriteLog("mail" + txtEmail.Text + txtfName.Text + txtPassword.Text);
                    sendmail(txtEmail.Text, txtfName.Text, txtPassword.Text);
                    Common.WriteLog("end");
                    Response.Redirect("UserList.aspx?msg=ins");
                    //if (!String.IsNullOrEmpty(Request.QueryString["org"]))
                    //{
                    //    string msg = CommonModule.msgSuccessfullyRegistered;
                    //    redirectpage(msg, "Login.aspx");
                    //}
                }
                catch (Exception ex)
                {
                    Common.WriteLog("error ReturnBoolean" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                }
            }

        }
        else
        {
            if (returnMsg == "userEmail")
                lblMsg.Text = CommonModule.msgEmailAlreadyExists;
            else
                lblMsg.Text = CommonModule.msgProblemInsertRecord;
        }
    }
    protected void UpdateUser()
    {
        string filePath = "";
        UserBM obj2 = new UserBM();
        obj2.useremailCheckDuplication(txtEmail.Text, Convert.ToInt32(Request.QueryString["emp"]));
        String returnMsg = obj2.ReturnString;
        if (returnMsg == "")
        {
            UserBM obj = new UserBM();
            obj.userId = Convert.ToInt32(Request.QueryString["emp"]);
            obj.comname = txtcomname.Text;
            obj.comAddress = txtcaddress.Text;
            obj.comphone = txtcphone.Text;
            obj.userFirstName = txtfName.Text;
            obj.userLastName = txtlname.Text;
            obj.userZip = txtzip.Text;
            obj.userAddress = txtaddress.Text;
            obj.userCountryId = Convert.ToInt32(ddlcountry.SelectedValue);
            obj.userStateId = Convert.ToInt32(ddlstate.SelectedValue);
            //obj.userCityId = Convert.ToInt32(ddlcity.SelectedValue);
            obj.usercontact = txtMobile.Text;
            obj.userEmail = txtEmail.Text;
            obj.userCityId = txtcity.Text;
            obj.userUpdatedDate = DateTime.Now;
            obj.userPassword = CommonModule.encrypt(txtPassword.Text.Trim());
            if (txtfromdate.Text != "")
            {
                //obj.userDOB = Convert.ToDateTime(CommonModule.FormatdtEnter(txtfromdate.Text));

                try
                {
                    obj.userDOB = DateTime.ParseExact(txtfromdate.Text, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                }
                catch (Exception ex)
                {
                    obj.userDOB = Convert.ToDateTime(txtfromdate.Text);
                }

            }
            //obj.userDOB = Convert.ToDateTime(CommonModule.FormatdtEnter(txtfromdate.Text));
            obj.userGender = ddlgender.SelectedValue;
            if (flupload1.HasFile)
            {

                filePath = System.DateTime.Now.ToString("MMddyyhhmmss") + "_" + flupload1.PostedFile.FileName;
                flupload1.SaveAs(Server.MapPath("../") + "Log/upload/Userimage/" + System.IO.Path.GetFileName(filePath.Replace(' ', '0')));
                obj.userImage = System.IO.Path.GetFileName(filePath.Replace(' ', '0'));

            }
            else
            {
                obj.userImage = Convert.ToString(ViewState["userImage"]);

            }

            string userdepId = string.Empty;
            for (int i = 0; i < chk_list_Departments.Items.Count; i++)
            {
                if (chk_list_Departments.Items[i].Selected == true)
                {
                    if (string.IsNullOrEmpty(userdepId))
                    {
                        userdepId = chk_list_Departments.Items[i].Value;
                    }
                    else
                    {
                        userdepId = userdepId + "," + chk_list_Departments.Items[i].Value;
                    }
                }
            }
            obj.userdepId = userdepId;

            string userdivId = string.Empty;
            for (int i = 0; i < chk_list_Division.Items.Count; i++)
            {
                if (chk_list_Division.Items[i].Selected == true)
                {
                    if (string.IsNullOrEmpty(userdivId))
                    {
                        userdivId = chk_list_Division.Items[i].Value;
                    }
                    else
                    {
                        userdivId = userdivId + "," + chk_list_Division.Items[i].Value;
                    }
                }
            }
            obj.userdivId = userdivId;

            obj.UpdateUser();

            #region thumbnail
            if (File.Exists(MapPath("../") + "Log/Upload/Userimage/" + System.IO.Path.GetFileName(filePath.Replace(' ', '0'))))
            {
                // flupload1.SaveAs(Server.MapPath("../") + "Log/upload/Userimage/" + System.IO.Path.GetFileName(filePath.Replace(' ', '0')));
                System.Drawing.Image img = System.Drawing.Image.FromFile(Server.MapPath("../") + "Log/Upload/Userimage/" + System.IO.Path.GetFileName(filePath.Replace(' ', '0')));
                System.Drawing.Image singleProduct = img.GetThumbnailImage(100, 100, null, IntPtr.Zero);
                // System.Drawing.Image imageSlider = img.GetThumbnailImage(2000, 2000, null, IntPtr.Zero);

                //System.Drawing.Image product = img.GetThumbnailImage(273, 204, null, IntPtr.Zero);
                img.Dispose();
                singleProduct.Save(Server.MapPath("../") + "Log/Upload/thumbnail/" + System.IO.Path.GetFileName(filePath.Replace(' ', '0')));
                // imageSlider.Save(Server.MapPath("../") + "Log/Upload/new/" + System.IO.Path.GetFileName(filePath.Replace(' ', '0')));
                //product.Save(Server.MapPath("../") + "Upload/product/" + System.IO.Path.GetFileName(filePath.Replace(' ', '0')));

            }
            #endregion

            if (obj.ReturnBoolean == true)
            {
                Response.Redirect("UserList.aspx?msg=upd");
                //if (!String.IsNullOrEmpty(Request.QueryString["org"]))
                //{
                //    string msg = CommonModule.msgSuccessfullyRegistered;
                //    redirectpage(msg, "Login.aspx");
                //}
                //else if (!String.IsNullOrEmpty(Request.QueryString["emp"]))
                //{
                //    Response.Redirect("UserList.aspx?msg=upd");
                //}
            }
        }
        else
        {
            if (returnMsg == "userEmail")
                lblMsg.Text = CommonModule.msgEmailAlreadyExists;
            else
                lblMsg.Text = CommonModule.msgProblemInsertRecord;
        }
    }
    protected void InsertBulkdata(int id, String udepId, String udivId)
    {

        UserBM obj = new UserBM();
        obj.InsertBulkdata(id, udepId, udivId);

    }
    protected void GetAllEmployeebyid()
    {
        UserBM obj = new UserBM();
        obj.userId = Convert.ToInt32(Request.QueryString["emp"]);
        obj.GetAllEmployeebyid();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["comAddress"])))
                txtcaddress.Text = Convert.ToString(ds.Tables[0].Rows[0]["comAddress"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["comphone"])))
                txtcphone.Text = Convert.ToString(ds.Tables[0].Rows[0]["comphone"]);


            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["comname"])))
                txtcomname.Text = Convert.ToString(ds.Tables[0].Rows[0]["comname"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userFirstName"])))
                txtfName.Text = Convert.ToString(ds.Tables[0].Rows[0]["userFirstName"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userLastName"])))
                txtlname.Text = Convert.ToString(ds.Tables[0].Rows[0]["userLastName"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userZip"])))
                txtzip.Text = Convert.ToString(ds.Tables[0].Rows[0]["userZip"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userAddress"])))
                txtaddress.Text = Convert.ToString(ds.Tables[0].Rows[0]["userAddress"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userCountryId"])))
                ddlcountry.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["userCountryId"]);
            GetState(Convert.ToInt32(ddlcountry.SelectedValue));
            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userStateId"])))
                ddlstate.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["userStateId"]);
            // Getcity(Convert.ToInt32(ddlstate.SelectedValue));
            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userCityId"])))
                // ddlcity.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["userCityId"]);
                txtcity.Text = Convert.ToString(ds.Tables[0].Rows[0]["userCityId"]);


            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["usercontact"])))
                txtMobile.Text = Convert.ToString(ds.Tables[0].Rows[0]["usercontact"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userEmail"])))
                txtEmail.Text = Convert.ToString(ds.Tables[0].Rows[0]["userEmail"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userDOB"])))
                txtfromdate.Text = Convert.ToString(ds.Tables[0].Rows[0]["userDOB"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userPassword"])))
            {
                string Password = CommonModule.decrypt(Convert.ToString(ds.Tables[0].Rows[0]["userPassword"]));
                txtPassword.Attributes.Add("value", Password);
                txtConfirmPassword.Attributes.Add("value", Password);
            }

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userdepId"])))
            {
                string[] userdepId = Convert.ToString(ds.Tables[0].Rows[0]["userdepId"]).Split(',');
                if (userdepId.Count() > 0)
                {
                    for (int i = 0; i < chk_list_Departments.Items.Count; i++)
                    {
                        if (userdepId.Contains(chk_list_Departments.Items[i].Value))
                        {
                            chk_list_Departments.Items[i].Selected = true;
                        }
                    }
                }
            }

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userGender"])))
                ddlgender.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["userGender"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userImage"])))
                ViewState["userImage"] = Convert.ToString(ds.Tables[0].Rows[0]["userImage"]);
            //img.ImageUrl = "~/Log/upload/Userimage/" + Convert.ToString(ds.Tables[0].Rows[0]["userImage"]);
            img.ImageUrl = "~/Log/upload/thumbnail/" + Convert.ToString(ds.Tables[0].Rows[0]["userImage"]);
            //

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userdivId"])))
            {
                string[] userdivId = Convert.ToString(ds.Tables[0].Rows[0]["userdivId"]).Split(',');
                if (userdivId.Count() > 0)
                {
                    for (int i = 0; i < chk_list_Division.Items.Count; i++)
                    {
                        if (userdivId.Contains(chk_list_Division.Items[i].Value))
                        {
                            chk_list_Division.Items[i].Selected = true;
                        }
                    }
                }
            }

        }

    }
    protected void GetAllDepartmentsByPerentId()
    {
        UserBM obj = new UserBM();
        obj.depIsActive = true;
        obj.depIsDeleted = false;
        obj.depPerentId = 0;
        obj.GetAllDepartmentsByPerentId();
        DataSet ds = obj.ds;
        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                chk_list_Departments.DataSource = ds;
                chk_list_Departments.DataTextField = "depName";
                chk_list_Departments.DataValueField = "depId";
                chk_list_Departments.DataBind();
            }
        }
    }
    protected void GetAllDivisionsByPerentId()
    {
        UserBM obj = new UserBM();
        obj.divIsActive = true;
        obj.divIsDeleted = false;
        obj.divPerentId = 0;
        obj.GetAllDivisionsByPerentId();
        DataSet ds = obj.ds;
        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                chk_list_Division.DataSource = ds;
                chk_list_Division.DataTextField = "divName";
                chk_list_Division.DataValueField = "divId";
                chk_list_Division.DataBind();
            }
        }
    }

    protected void sendmail(string email, string name, string pass)
    {
        try
        {
            Common.WriteLog("start");
            string subject = "Registration";
            string fromid = Convert.ToString(ConfigurationManager.AppSettings["fromid"]);
            CmsBM objMail = new CmsBM();
            objMail.cmsName = "Registration";
            objMail.SelectMailTemplateByName();
            DataSet dsMail = objMail.ds;
            if (dsMail.Tables[0].Rows.Count > 0)
            {
                Common.WriteLog("start_in");
                string confirmMail = dsMail.Tables[0].Rows[0]["cmsDescription"].ToString();
                string tempString = confirmMail;
                tempString = tempString.Replace("###name###", name);
                tempString = tempString.Replace("###email###", email);
                tempString = tempString.Replace("###password###", pass);
                //SendMail(tempString, txtEmail.Text, txtFname.Text);
                CommonModule.SendMailToUser(email, subject, tempString, fromid,"0");
            }
        }
        catch (Exception ex)
        {
            Common.WriteLog("error in sendmail" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
        }

    }

    #endregion

    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Request.QueryString["emp"]))
        {
            UpdateUser();
        }
        else
        {
            Insertuser();
        }

    }
    protected void btnCancel_click(object sender, EventArgs e)
    {
        Response.Redirect("UserList.aspx");
    }
    #endregion

    #region SelectedIndexChanged
    protected void ddlcountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetState(Convert.ToInt32(ddlcountry.SelectedValue));
    }
    protected void ddlstate_SelectedIndexChanged(object sender, EventArgs e)
    {
        // Getcity(Convert.ToInt32(ddlstate.SelectedValue));
    }
    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        string language = Convert.ToString(Session["Cultureadmin"]);
        //string language = "Denish";
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Denish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Languageadmin"] != null)
        {
            if (!Session["Languageadmin"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Languageadmin"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Languageadmin"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion
}