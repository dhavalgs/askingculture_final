﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="webadmin_Login" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>Login</title>
    <!--=== CSS ===-->
    <!-- Bootstrap -->
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme -->
    <link href="../assets/css/main.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/plugins.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/StyleSheet.css" rel="stylesheet" type="text/css" />
    <!-- Login -->
    <link href="../assets/css/login.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/fontawesome/font-awesome.min.css">
    <!--[if IE 7]>
		<link rel="stylesheet" href="assets/css/fontawesome/font-awesome-ie7.min.css">
	<![endif]-->
    <!--[if IE 8]>
		<link href="assets/css/ie8.css" rel="stylesheet" type="text/css" />
	<![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet'
        type='text/css'>
    <!--=== JavaScript ===-->
    <script type="text/javascript" src="../assets/js/libs/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../assets/js/libs/lodash.compat.min.js"></script>
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
		<script src="assets/js/libs/html5shiv.js"></script>
	<![endif]-->
    <!-- Beautiful Checkboxes -->
    <script type="text/javascript" src="../plugins/uniform/jquery.uniform.min.js"></script>
    <!-- Form Validation -->
    <script type="text/javascript" src="../plugins/validation/jquery.validate.min.js"></script>
    <!-- Slim Progress Bars -->
    <script type="text/javascript" src="../plugins/nprogress/nprogress.js"></script>
    <!-- App -->
    <script type="text/javascript" src="../assets/js/login.js"></script>
    <script>
        $(document).ready(function () {
            "use strict";

            Login.init(); // Init login JavaScript
        });
    </script>
</head>
<body class="login" style="padding-top: 18px;">
    <form id="Form1" runat="server">
    <!-- Logo -->
    <div class="logo">
        <asp:Image runat="server" ID="imgLoginLogo" AlternateText="logo" style="max-width: 15%;"></asp:Image>
        <%--<img src="../assets/img/strateku_centreret_fv.png?v=1" alt="logo" style="max-width: 15%;"/>--%>
        <!--	<strong>ME</strong>LON-->
    </div>
    <!-- /Logo -->
    <!-- Login Box -->
    <div class="box">
        <div class="content">
            <!-- Login Formular -->
            <!-- Title -->
            <h3 class="form-title">
                Sign In to your Account</h3>
            <!-- Error Message -->
            <div class="alert fade in alert-danger" style="display: none;">
                <i class="icon-remove close" data-dismiss="alert"></i>Enter any username and password.
            </div>
            <!-- Input Fields -->
            <div class="form-group">
                <!--<label for="username">Username:</label>-->
                <div class="input-icon">
                    <i class="icon-user"></i>
                    <asp:TextBox ID="txtUsername" type="text" class="form-control" placeholder="Username"
                        runat="server" MaxLength="100" meta:resourcekey="txtUsernameResource1" />
                    <asp:RequiredFieldValidator ID="rfvUsername" runat="server" CssClass="commonerrormsg"
                        ControlToValidate="txtUsername" Display="Dynamic" ValidationGroup="chk" 
                        ErrorMessage="Please Enter Your User Name." 
                        meta:resourcekey="rfvUsernameResource1"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form-group">
                <!--<label for="password">Password:</label>-->
                <div class="input-icon">
                    <i class="icon-lock"></i>
                    <asp:TextBox ID="txtPassword" type="text" class="form-control" runat="server" MaxLength="20"
                        TextMode="Password" placeholder="Password" 
                        meta:resourcekey="txtPasswordResource1" />
                    <asp:RequiredFieldValidator ID="rfvPassword" runat="server" CssClass="commonerrormsg"
                        ControlToValidate="txtPassword" ErrorMessage="Please Enter Your Password." Display="Dynamic"
                        ValidationGroup="chk" meta:resourcekey="rfvPasswordResource1"></asp:RequiredFieldValidator>
                    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" 
                        meta:resourcekey="lblMsgResource1"></asp:Label>
                </div>
            </div>
            <!-- /Input Fields -->
            <!-- Form Actions -->
            <div class="form-actions">
                <label class="checkbox pull-left">
                    <asp:CheckBox runat="server" ID="cbRememberme" CssClass="uniform" 
                    meta:resourcekey="cbRemembermeResource1" />
                    Remember me</label>
                <%--<button type="submit" class="submit btn btn-warning pull-right">
                    Sign In <i class="icon-angle-right"></i>
                </button>--%>
                <asp:Button runat="server" ID="btnLogin" Text=" Sign In >" CssClass="submit btn btn-warning pull-right"
                    OnClick="btnLogin_Click" ValidationGroup="chk" 
                    meta:resourcekey="btnLoginResource1" />
            </div>
            <!-- /Login Formular -->
        </div>
        <!-- /.content -->
        <div class="inner-box">
            <div class="content">
                <!-- Close Button -->
                <i class="icon-remove close hide-default"></i>
                <!-- Link as Toggle Button -->
                <a href="forget_password.aspx" class="forgot-password-link1">Forgot Password?</a>
            </div>
            <!-- /.content -->
        </div>
    </div>
    
    <div class="footer">
       
    </div>
    <!-- /Footer -->
    </form>
</body>
</html>
