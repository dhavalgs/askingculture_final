﻿using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using starteku_BusinessLogic.Model;
using startetku.Business.Logic;
using System.Threading;
using System.Web.Services;


public partial class webadmin_WebActivityLists : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            hdnLoginUserID.Value = Convert.ToString(Session["OrgUserId"]);
            hdnUserType.Value = Convert.ToString(Session["OrguserType"]);
            


            CompetenceSelectAll();
            try
            {
                txtActName.Attributes["placeholder"] = GetLocalResourceObject("txtActName.Text").ToString();
                txtActDescription.Attributes["placeholder"] = GetLocalResourceObject("txtActDescription.Text").ToString();
                txtActRequirements.Attributes["placeholder"] = GetLocalResourceObject("txtActRequirements.Text").ToString();
                txtActTags.Attributes["placeholder"] = GetLocalResourceObject("txtActTags.Text").ToString();
                txtActStartDate.Attributes["placeholder"] = GetLocalResourceObject("txtActStartDate.Text").ToString();
                txtActEndDate.Attributes["placeholder"] = GetLocalResourceObject("txtActEndDate.Text").ToString();
                txtActCost.Attributes["placeholder"] = GetLocalResourceObject("txtActCost.Text").ToString();
                txtcom.Attributes["placeholder"] = GetLocalResourceObject("TYPEYOURCOMMENTHERE.Text").ToString();
            }
            catch (Exception ex)
            {

                ExceptionLogger.LogException(ex);
            }

            // GetAllJobType();
            //Competence.InnerHtml = CommonMessages.Welcome + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
           // Competence.InnerHtml = "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            SetDefaultMessage();
            GetAllJobTypeByCompanyId();
            GetAllDivisionByCompanyId();
            GetAllActivityCategory();

            GetActivityList(0);
            getalldt();
            

        }
    }


    protected void GetAllActivityCategory()
    {
        var db = new startetkuEntities1();
        var getAllActCat = db.ActivityCategoryMasters.ToList();

        if (getAllActCat.Any())
        {

            ddActCate.DataSource = getAllActCat;
            ddActCate.DataTextField = "ActCatName";
            ddActCate.DataValueField = "ActCatId";
            ddActCate.DataBind();


            ListItem li = new ListItem();
            li.Text = GetLocalResourceObject("SelectActivitytype.Text").ToString();
            li.Value = @"0";

            ddActCate.Items.Insert(0, li);


        }
    }

    protected void GetAllJobTypeByCompanyId()
    {
        JobTypeBM obj = new JobTypeBM();
        obj.jobIsActive = true;
        obj.jobIsDeleted = false;
        obj.jobCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);


        obj.GetAllJobTypeByCompanyId();


        DataSet ds = obj.ds;
        if (Convert.ToString(Session["Culture"]) == "Danish")
        {
            ds.Tables[0].Columns["jobName"].ColumnName = "abcd1";
            ds.Tables[0].Columns["jobNameDN"].ColumnName = "jobName";

        }
        ViewState["ds"] = ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                chkListJobtype.DataSource = ds.Tables[0];
                chkListJobtype.DataTextField = "jobName";
                chkListJobtype.DataValueField = "jobId";
                chkListJobtype.DataBind();
                chkListJobtype.Items.Insert(0, new ListItem(GetLocalResourceObject("chkListJobtype.Text").ToString(), "0"));
                // chkListDivision.Items.Insert(0, "All");


            }

        }
    }

    protected void GetAllDivisionByCompanyId()
    {
        DivisionBM obj = new DivisionBM();
        obj.depIsActive = true;
        obj.depIsDeleted = false;
        obj.depCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);


        obj.GetAllDivisionsbyCompanyId();


        DataSet ds = obj.ds;
        if (Convert.ToString(Session["Culture"]) == "Danish")
        {
            ds.Tables[0].Columns["divName"].ColumnName = "abcd";
            ds.Tables[0].Columns["divNameDN"].ColumnName = "divName";

        }
        ViewState["ds"] = ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                chkListDivision.DataSource = ds.Tables[0];
                chkListDivision.DataTextField = "divName";
                chkListDivision.DataValueField = "divId";
                chkListDivision.DataBind();
                chkListDivision.Items.Insert(0, new ListItem(GetLocalResourceObject("SelectDivision.Text").ToString(), "0"));



            }

        }
    }

    private void GetActivityList(int company)
    {
        var db = new startetkuEntities1();
        try
        {
           // var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
           // var userId = Convert.ToInt32(Session["OrgUserId"]);

            var actObj = new GetActivityListweb_Result();
            actObj.ActCompanyId = company;
            actObj.ActIsDeleted = false;
            //actObj.ActReqUserId = userId;
            var activityList = ActListsweb.GetActivityListweb(actObj);

            //Get user type

          //  var logginUserType = Convert.ToInt32(Session["OrguserType"]);

            

                var emplsManagerId = Convert.ToInt32(Session["OrgCreatedBy"]);//Mang Id

                //activityList = activityList.Where(o => o.ActIsActive ==true).ToList();


      
            

            if (activityList.Any())
            {
                gvGrid.DataSource = activityList;
                gvGrid.DataBind();

                if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["OrguserType"])))
                {
                    var type = Convert.ToInt32(Session["OrguserType"]);

                    if (type == 1)
                    {
                        //gvGrid.Columns[10].Visible = false;
                    }
                }
            }
            else
            {
                gvGrid.DataSource = null;
                gvGrid.DataBind();
            }

        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex);
        }
        finally
        {
            CommonAttributes.DisposeDBObject(ref db);
        }
        btnCreate.Visible = true;
        GetActivityListArchive(company);
    }


    protected void getc(object sender, EventArgs e) 
    {

        int comp = Convert.ToInt32(DropDownList1.SelectedItem.Value);
        GetActivityList(comp);
    
    }


    protected void getalldt()
    {
        var db = new startetkuEntities1();
       // ddActCate.Items.Insert(0, new ListItem(GetLocalResourceObject("Current.Text").ToString(), "0"));
        //   int userCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
        // var getdata = db.UserMasters.Where(o => o.PdpCompanyID == userCompanyId && o.PpdHisIsActive == true).ToList();

        UserBM obj = new UserBM();
        obj.userType = 1;
        obj.userIsDeleted = false;
        obj.userIsActive = true;
        obj.userCompanyId = 0;
        obj.userCreateBy = "0" ;

        //obj.userCompanyId = Convert.ToInt32(Session["UserId"]);
       
        obj.GetAllEmployee();
        DataSet ds = obj.ds;

        if (ds != null)
            {
                DropDownList1.DataSource = ds.Tables[0];
                DropDownList1.DataTextField = "comname";
                DropDownList1.DataValueField = "userId";
                DropDownList1.DataBind();
                //ddActCate.Items.Insert(0, new ListItem(GetLocalResourceObject("Current.Text").ToString(), "0"));
           }
    }

    private void GetActivityListArchive(int company)
    {
        var db = new startetkuEntities1();
        try
        {
            // var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
            // var userId = Convert.ToInt32(Session["OrgUserId"]);

            var actObj = new GetActivityListweb_Result();
            actObj.ActCompanyId = company;
            actObj.ActIsDeleted = true;
            //actObj.ActReqUserId = userId;
            var activityList = ActListsweb.GetActivityListweb(actObj);

            //Get user type

            //  var logginUserType = Convert.ToInt32(Session["OrguserType"]);



            var emplsManagerId = Convert.ToInt32(Session["OrgCreatedBy"]);//Mang Id

           // activityList = activityList.Where(o => o.ActIsActive == false).ToList();





            if (activityList.Any())
            {
                GridView1.DataSource = activityList;
                GridView1.DataBind();

                if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["OrguserType"])))
                {
                    var type = Convert.ToInt32(Session["OrguserType"]);

                    if (type == 1)
                    {
                        GridView1.Columns[10].Visible = false;
                    }
                }
            }
            else
            {
                GridView1.DataSource = null;
                GridView1.DataBind();
            }

        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex);
        }
        finally
        {
            CommonAttributes.DisposeDBObject(ref db);
        }
        btnCreate.Visible = true;

    }

    protected void CompetenceSelectAll()
    {
        CompetenceMasterBM obj = new CompetenceMasterBM();
        obj.comIsActive = true;
        obj.comIsDeleted = false;
        obj.comCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllCompetenceMasterAdd();
        DataSet ds = obj.ds;
        if (Convert.ToString(Session["Culture"]) == "Danish")
        {
            ds.Tables[0].Columns["catName"].ColumnName = "abcd";
            ds.Tables[0].Columns["catNameDN"].ColumnName = "catName";

        }
        ViewState["ds"] = ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddCompetence.DataSource = ds.Tables[0];
                ddCompetence.DataTextField = "comCompetence";
                ddCompetence.DataValueField = "comId";
                ddCompetence.DataBind();
                ddCompetence.Items.Insert(0, new ListItem(GetLocalResourceObject("SelectCompetence.Text").ToString(), "0"));
                ddCompetence.SelectedValue = "0";


            }
            else
            {
                ddCompetence.DataSource = null;
                ddCompetence.DataBind();
            }
        }
        else
        {
            ddCompetence.DataSource = null;
            ddCompetence.DataBind();
        }
    }

    protected void grd_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }

    protected void gvGrid_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            foreach (TableCell cell in e.Row.Cells)
            {
                cell.BorderStyle = BorderStyle.None;
            }
        }
    }

    public int total;
    protected void gvGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            //string sponsorBonus = ((Label)e.Row.FindControl("Label2")).Text;

            string count = ((HiddenField)e.Row.FindControl("hdnCost")).Value;
            

            Label lbl = (Label)e.Row.FindControl("lblActReqStatus");


            


            if (!string.IsNullOrWhiteSpace(count))
            {
               
                    total = total + Int32.Parse(count);
               

            }

        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            var type = Convert.ToInt32(Session["OrguserType"]);
            Label lblTotalCost = (Label)e.Row.FindControl("lblTotalCost");
            lblTotalCost.Text = total.ToString();
            if (type != 1)
            {
                gvGrid.ShowFooter = false;
            }
        }


    }

    protected void gvGrid_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        justificationDiv.Visible = false;
        Expected_Behaviour_Change_Div.Visible = false;
        btnRequest.Visible = false;
        btnUpdate.Visible = false;

        if (e.CommandName == "EditRow")
        {
            btnUpdate.Visible = true;
            btnCreate.Visible = false;


            GetActivityData(e, "EditRow");



        }
        if (e.CommandName == "Delete")
        {
            errorDiv.Visible = true;
           // lblMsg.Text = @"Delete option is not activated";
          //  lblMsg.ForeColor = System.Drawing.Color.White;
          //  return;
            string ActId = e.CommandArgument.ToString();

            var db = new startetkuEntities1();
            var ac = Convert.ToInt32(ActId);
            var isDup = db.ActivityMasters.FirstOrDefault(o => o.ActId == ac);
            if (isDup != null)
            {
                

                var getActReq = db.ActReqMasters.Any(o => o.ActReqActId == ac);// ANY will run fast so

                if (getActReq == false) {
                    db.ActivityMasters.Remove(isDup);
                    db.SaveChanges();
                    
                    int comp = Convert.ToInt32(DropDownList1.SelectedItem.Value);
                    GetActivityList(comp);
                    
                    lblMsg.Text = GetLocalResourceObject("CatActDe.Text").ToString();
                    lblMsg.Visible = true;
                   
                }
               
                
                Response.Redirect("WebActivityLists.aspx?msg=del");


            }



        }

        if (e.CommandName == "restore")
        {
            errorDiv.Visible = true;
            // lblMsg.Text = @"Delete option is not activated";
            //  lblMsg.ForeColor = System.Drawing.Color.White;
            //  return;
            string ActId = e.CommandArgument.ToString();

            var db = new startetkuEntities1();
            var ac = Convert.ToInt32(ActId);
            var isDup = db.ActivityMasters.FirstOrDefault(o => o.ActId == ac);
            if (isDup != null)
            {
                isDup.ActIsDeleted = false;
                db.SaveChanges();

                int comp = Convert.ToInt32(DropDownList1.SelectedItem.Value);
                GetActivityList(comp);


                lblMsg.Text = GetLocalResourceObject("CatActRe.Text").ToString();
                lblMsg.Visible = true;


            }



        }


        if (e.CommandName == "archive")
        {
            errorDiv.Visible = true;
            // lblMsg.Text = @"Delete option is not activated";
            //  lblMsg.ForeColor = System.Drawing.Color.White;
            //  return;
            string ActId = e.CommandArgument.ToString();

            var db = new startetkuEntities1();
            var ac = Convert.ToInt32(ActId);
            var isDup = db.ActivityMasters.FirstOrDefault(o => o.ActId == ac);
            if (isDup != null)
            {
                isDup.ActIsDeleted = true;
                db.SaveChanges();


                int comp = Convert.ToInt32(DropDownList1.SelectedItem.Value);
                GetActivityList(comp);
                lblMsg.Text = GetLocalResourceObject("CatActAr.Text").ToString();

                //lblMsg.ForeColor = System.Drawing.Color.White;
                lblMsg.Visible = true;


            }



        }


        else if (e.CommandName == "ViewRow")
        {
            justificationDiv.Visible = true;
            Expected_Behaviour_Change_Div.Visible = true;
            btnCreate.Visible = false;
            btnRequest.Visible = true;
            GetActivityData(e, "ViewRow");

        }

    }

    public void GetActivityData(GridViewCommandEventArgs e, string eventName)
    {
        var id = Convert.ToInt32(e.CommandArgument);
        hdnActId.Value = Convert.ToString(e.CommandArgument);
        ClientScript.RegisterStartupScript(this.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lbl').click();", true);

        //Get Data
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(Session["OrgUserId"]);
        var actObj = new GetActivityListweb_Result();
        actObj.ActId = id;
        actObj.ActCompanyId = companyId;
        //actObj.ActReqUserId = userId;
        var getActData = ActListsweb.GetActivityListweb(actObj).FirstOrDefault();// db.ActivityMasters.FirstOrDefault(o => o.ActId == id);
        //
        if (getActData == null)
        {
            lblMsg.Text = @"Sorry, Record not exists any more";
            lblMsg.ForeColor = System.Drawing.Color.White;
            return;
        }
        hdnActReqIdInt.Value = Convert.ToString(getActData.ActCreatedBy);
        hdnAcrCreaterId.Value = Convert.ToString(getActData.ActCreatedBy);

        //CHeck Row Event

        if (eventName == "ViewRow")
        {
            lblModelHeader.Text = hdnViewActivity.Value;
            btnCreate.Visible = false;
            btnUpdate.Visible = false;
            //if login user & creater of activity is same then requet button will visible =false
            if (getActData.ActIsPublic == false)
            {
                btnRequest.Visible = false;
                commentDivServerSide.Visible = false;//that means activity is private and user cant comment on provate activity
            }
        }



        if (eventName == "EditRow")
        {
            lblModelHeader.Text = hdnUpdateActivity.Value;
            btnCreate.Visible = false;
            btnRequest.Visible = false;

            //Check User is the same user who created this activity, if not then do not allow him to edit

            if (userId != getActData.ActCreatedBy)
            {
                btnUpdate.Visible = false;
            }
        }

        if (userId == getActData.ActCreatedBy)
        {
            //if user is creater of activity then comment box should be invisible
            commentDivServerSide.Visible = false;
        }

        //$("#txtCmtDiv").fadeOut();

        if (getActData != null)
        {

            //if (getActData.ActReqStatus == "3" || getActData.ActReqStatus == "2" || Convert.ToInt32(Session["OrguserType"]) == 1)
            //{
            //    btnRequest.Visible = false;
            //}

            txtActName.Text = getActData.ActName;
            txtActCost.Text = Convert.ToString(getActData.ActCost);

            txtActEndDate.Text = getActData.ActEndDate.ToString("dd/MM/yyyy");
            txtActStartDate.Text = getActData.ActStartDate.ToString("dd/MM/yyyy");

            if (Convert.ToBoolean(getActData.ActIsPublic) == true)
            {
                rdoPublic.Items[0].Selected = true;
                rdoPublic.Items[1].Selected = false;
                // rdoGender.SelectedIndex = 0;
            }

            if (Convert.ToBoolean(getActData.ActIsActive) == false)
            {
                rdoEnabled.Items[0].Selected = false;
                rdoEnabled.Items[1].Selected = true;
                // rdoGender.SelectedIndex = 0;
            }

            if (Convert.ToBoolean(getActData.ActIsComp) == true)
            {
                chkIsCompDevDoc.Items[0].Selected = true;
                chkIsCompDevDoc.Items[1].Selected = false;
                // rdoGender.SelectedIndex = 0;
            }

            chkListDivision.SelectedValue = Convert.ToString(getActData.ActDivId);
            chkListJobtype.SelectedValue = Convert.ToString(getActData.ActJobId);
            ddCompetence.SelectedValue = Convert.ToString(getActData.ActCompId);
            ddActCate.SelectedValue = Convert.ToString(getActData.ActCatRefId);

            txtActTags.Text = getActData.ActTags;
            txtActRequirements.Text = getActData.ActRequirements;

            txtActDescription.Text = getActData.ActDescription;


        }
    }

    public void ClearField()
    {
        txtActName.Text = string.Empty;
    }

    protected void SetDefaultMessage()
    {
        // lblMsg.Text = GetLocalResourceObject("lblHelloWorld.Text").ToString();

        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["msg"])))
        {
            if (Convert.ToString(Request.QueryString["msg"]) == "del")
            {
                //lblMsg.Text = CommonModule.msgRecordHasBeenDeletedSuccessfully;
                //lblMsg.Text = CommonMessages.msgRecordHasBeenDeletedSuccessfully;
                lblMsg.Text = GetLocalResourceObject("msgRecordHasBeenDeletedSuccessfully.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "ins")
            {
                //lblMsg.Text = CommonMessages.msgRecordInsertedSuccessfully;
                lblMsg.Text = GetLocalResourceObject("msgRecordInsertedSuccessfully.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "upd")
            {
                //lblMsg.Text = CommonMessages.msgRecordUpdatedSuccss;
                lblMsg.Text = GetLocalResourceObject("msgRecordUpdatedSuccss.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "arc")
            {
                // lblMsg.Text = CommonMessages.msgRecordArchiveSucces;
                lblMsg.Text = GetLocalResourceObject("CommonMessages.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
        }

    }

    protected void btnBack_click(object sender, EventArgs e)
    {
        switch (Convert.ToInt32(Session["OrguserType"]))
        {
            case 2:
                Response.Redirect("Manager-dashboard.aspx");
                break;
            case 1:
                Response.Redirect("Organisation-dashboard.aspx");
                break;
            default:
                Response.Redirect("Employee_dashboard.aspx");
                break;
        }
    }

    protected void CreateActivity(object sender, EventArgs e)
    {
        var db = new startetkuEntities1();
        try
        {
            // check Duplicate

            var isDup = db.ActivityMasters.FirstOrDefault(o => o.ActName == txtActName.Text);
            if (isDup != null)
            {
                //Opps there is duplicate records
                errorDiv.Visible = true;
                txtError.Text = @"Opps! Records already found";

                ClientScript.RegisterStartupScript(this.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lbl').click();", true);

            }
            else
            {
                errorDiv.Visible = false;
                //Save record
                var actObj = new ActivityMaster();

                actObj.ActName = txtActName.Text;
                actObj.ActBehaviouralChange = "";

                actObj.ActCost = Convert.ToInt32(txtActCost.Text);
                actObj.ActCreatedBy = Convert.ToInt32(Session["OrgUserId"]);


                actObj.ActEndDate = CommonUtilities.ConvertStringToDateTime(txtActEndDate.Text);
                actObj.ActStartDate = CommonUtilities.ConvertStringToDateTime(txtActStartDate.Text);

                actObj.ActDescription = txtActDescription.Text;
                actObj.ActTags = txtActTags.Text;


                actObj.ActIsActive = rdoEnabled.SelectedItem.Value == @"true";

                var actDivId = 0;
                if (!string.IsNullOrWhiteSpace(chkListDivision.SelectedItem.Value))
                {
                    try
                    {
                        actDivId = Convert.ToInt32(chkListDivision.SelectedItem.Value);
                    }
                    catch (Exception)
                    {


                    }

                }

                var actJobId = 0;
                if (!string.IsNullOrWhiteSpace(chkListDivision.SelectedItem.Value))
                {
                    try
                    {
                        actJobId = Convert.ToInt32(chkListJobtype.SelectedItem.Value);

                    }
                    catch (Exception)
                    {


                    }

                }

                var actCompId = 0;
                if (!string.IsNullOrWhiteSpace(ddCompetence.SelectedItem.Value))
                {
                    try
                    {
                        actCompId = Convert.ToInt32(ddCompetence.SelectedItem.Value);
                    }
                    catch (Exception)
                    {


                    }

                }

                var actCatRefId = 0;
                if (!string.IsNullOrWhiteSpace(chkListDivision.SelectedItem.Value))
                {
                    try
                    {
                        actCatRefId = Convert.ToInt32(ddActCate.SelectedItem.Value);
                    }
                    catch (Exception)
                    {


                    }

                }


                actObj.ActDivId = actDivId;
                actObj.ActJobId = actJobId;
                actObj.ActCompId = actCompId;
                actObj.ActCatRefId = actCatRefId;

                actObj.ActIsPublic = rdoPublic.SelectedItem.Value == @"true";
                actObj.ActStatus = "";

                actObj.ActJustification = string.Empty;
                actObj.ActRequirements = txtActRequirements.Text;

                actObj.ActCompLevel = rdoLevel.SelectedItem.Text;

                actObj.ActCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);

                actObj.ActCreatedDate = CommonUtilities.GetCurrentDateTime();
                actObj.ActUpdateDate = CommonUtilities.GetCurrentDateTime();

                db.ActivityMasters.Add(actObj);
                db.SaveChanges();

                GetActivityList(0);
                ClearField();
            }

        }
        catch (Exception ex)
        {

            ExceptionLogger.LogException(ex);
        }
        finally
        {
            CommonAttributes.DisposeDBObject(ref db);
        }
    }

    protected void UpdateActivity(object sender, EventArgs e)
    {
        var db = new startetkuEntities1();
        try
        {
            // check Duplicate
            var id = Convert.ToInt32(hdnActId.Value);

            var actObj = db.ActivityMasters.FirstOrDefault(o => o.ActId == id);
            if (actObj == null)
            {
                //Opps there is duplicate records
                errorDiv.Visible = true;
                txtError.Text = @"Opps! Records not found";

                ClientScript.RegisterStartupScript(this.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lbl').click();", true);

            }
            else
            {
                var chkDup = db.ActivityMasters.FirstOrDefault(o => o.ActId != id && o.ActName == txtActName.Text);
                if (chkDup != null)
                {
                    //Opps there is duplicate records
                    errorDiv.Visible = true;
                    txtError.Text = @"Record Already Found";

                    ClientScript.RegisterStartupScript(this.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lbl').click();", true);
                    return;

                }
                errorDiv.Visible = false;
                //Save record


                actObj.ActName = txtActName.Text;
                actObj.ActBehaviouralChange = "";

                actObj.ActCost = Convert.ToInt32(txtActCost.Text);
                actObj.ActCreatedBy = Convert.ToInt32(Session["OrgUserId"]);


                actObj.ActEndDate = CommonUtilities.ConvertStringToDateTime(txtActEndDate.Text);
                actObj.ActStartDate = CommonUtilities.ConvertStringToDateTime(txtActStartDate.Text);

                actObj.ActDescription = txtActDescription.Text;
                actObj.ActTags = txtActTags.Text;


                actObj.ActIsActive = rdoEnabled.SelectedItem.Value == @"true";

                var actDivId = 0;
                if (!string.IsNullOrWhiteSpace(chkListDivision.SelectedItem.Value))
                {
                    try
                    {
                        actDivId = Convert.ToInt32(chkListDivision.SelectedItem.Value);
                    }
                    catch (Exception)
                    {


                    }

                }

                var actJobId = 0;
                if (!string.IsNullOrWhiteSpace(chkListDivision.SelectedItem.Value))
                {
                    try
                    {
                        actJobId = Convert.ToInt32(chkListJobtype.SelectedItem.Value);

                    }
                    catch (Exception)
                    {


                    }

                }

                var actCompId = 0;
                if (!string.IsNullOrWhiteSpace(ddCompetence.SelectedItem.Value))
                {
                    try
                    {
                        actCompId = Convert.ToInt32(ddCompetence.SelectedItem.Value);
                    }
                    catch (Exception)
                    {


                    }

                }

                var actCatRefId = 0;
                if (!string.IsNullOrWhiteSpace(chkListDivision.SelectedItem.Value))
                {
                    try
                    {
                        actCatRefId = Convert.ToInt32(ddActCate.SelectedItem.Value);
                    }
                    catch (Exception)
                    {


                    }

                }


                actObj.ActDivId = actDivId;
                actObj.ActJobId = actJobId;
                actObj.ActCompId = actCompId;
                actObj.ActCatRefId = actCatRefId;

                actObj.ActIsPublic = rdoPublic.SelectedItem.Value == @"true";
                actObj.ActStatus = "";

                actObj.ActJustification = string.Empty;
                actObj.ActRequirements = txtActRequirements.Text;

                actObj.ActCompLevel = rdoLevel.SelectedItem.Text;

                actObj.ActCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
                actObj.ActUpdateDate = CommonUtilities.GetCurrentDateTime();

                db.SaveChanges();

                GetActivityList(0);
                ClearField();

            }

        }
        catch (Exception)
        {

            throw;
        }
        finally
        {

        }
    }

    protected void RequestActivity(object sender, EventArgs e)
    {
        var db = new startetkuEntities1();
        var isPointAllow = false;
        try
        {
            var req = new ActReqMaster();
            req.ActReqCreateDate = CommonUtilities.GetCurrentDateTime();
            req.ActReqUpdateDate = CommonUtilities.GetCurrentDateTime();
            req.ActReqIsActive = true;
            req.ActReqJustification = txtJustification.Text;
            req.ActReqExpJustBehav = txtExpctBehavChange.Text;
            req.ActReqOrgId = Convert.ToInt32(Session["OrgUserId"]);

            var toUserId = 0;
            if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["OrgCreatedBy"])))
            {
                toUserId = Convert.ToInt32(Session["OrgCreatedBy"]);
            }
            req.ActReqToUserId = toUserId;
            req.ActReqUserId = Convert.ToInt32(Session["OrgUserId"]);
            req.ActReqStatus = "2";
            req.ActReqActId = Convert.ToInt32(hdnActId.Value);

            req.ActReqOrgId = Convert.ToInt32(Session["OrgCompanyId"]);

            db.ActReqMasters.Add(req);
            db.SaveChanges();



            InsertNotification(req.ActReqId);
            LogMasterLogic obj = new LogMasterLogic();

            obj.LogDescription = string.Format("User {0} got point  for final of fill the competences score  user ID : {1} ", Convert.ToString(Session["OrgUserName"]), Convert.ToString(Session["OrgUserId"]));   // User log description..

            var userID = Convert.ToInt32(Session["OrgUserId"]);



            var finalfillcomp = Common.FinalFillCompetences();
            var user =
                (from o in db.LogMasters
                 where o.logUserId == userID && o.logPointInfo == finalfillcomp
                 select o).FirstOrDefault();

            if (user == null)
            {
                var point = LogMasterLogic.InsertUpdateLogParam(userID, Common.PLP(), Common.FinalFillCompetences(),
                    obj.LogDescription, isPointAllow, 0, 0);


            }
            GetActivityList(0);
            ClearField();
        }
        catch (Exception)
        {


        }
    }
    protected void InsertNotification(int reqId)
    {
        NotificationBM obj = new NotificationBM();
        obj.notsubject = "Has Set Reqest.";
        obj.notUserId = Convert.ToInt32(Session["OrgUserId"]);
        obj.notIsActive = true;
        obj.notIsDeleted = false;
        obj.notCreatedDate = DateTime.Now;
        obj.notCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.notpage = "ActivityRequestView.aspx?actreqid=" + reqId;
        obj.nottype = "ActReq";
        obj.notToUserId = Convert.ToInt32(Session["OrgCreatedBy"]);
        obj.InsertNotification();


        sendmail(Convert.ToInt32(Session["OrgCreatedBy"]));
    }

    protected void sendmail(Int32 touserid)
    {
        #region Send mail
        // String subject = "Set Competence for " + Convert.ToString(Session["OrgUserName"]);
        Template template = CommonModule.getTemplatebyname1("ActivityRequest", Convert.ToInt32(Session["OrgCreatedBy"]));
        //String confirmMail = CommonModule.getTemplatebyname("Set Competence", touserid);
        String confirmMail = template.TemplateName;
        if (!String.IsNullOrEmpty(confirmMail))
        {
            var localResourceObject = GetLocalResourceObject("Activity_Request_Email_Subject.Text");
            if (localResourceObject != null)
            {
                string subject = localResourceObject.ToString();


                UserBM Cust = new UserBM();
                Cust.userId = Convert.ToInt32(Session["OrgCreatedBy"]);
                Cust.SelectmanagerByUserId();
                //  Cust.SelectPasswordByUserId();
                DataSet ds = Cust.ds;
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        String name = Convert.ToString(ds.Tables[0].Rows[0]["userFirstName"]);
                        string fromid = Convert.ToString(ConfigurationManager.AppSettings["fromid"]);
                        String empname = Convert.ToString(Session["OrgUserName"]);
                        string tempString = confirmMail;
                        tempString = tempString.Replace("###name###", name);
                        tempString = tempString.Replace("###empname###", empname);
                        CommonModule.SendMailToUser(Convert.ToString(ds.Tables[0].Rows[0]["userEmail"]), subject, tempString, fromid, Convert.ToString(HttpContext.Current.Session["OrgCompanyId"]));

                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'> generate('information', '<b>'" + RequestSentSuccessful.Value + "'</b><br> ', 'bottomCenter');</script>", false);
                    }
                }
            }
        }

        #endregion
    }

    protected void OpenCreateActivity(object sender, EventArgs e)
    {
        Clear();
        lblModelHeader.Text = hdnAddNewActivity.Text;
        justificationDiv.Visible = false;
        Expected_Behaviour_Change_Div.Visible = false;
        btnCreate.Visible = true;
        btnRequest.Visible = false;
        btnUpdate.Visible = false;
        ClientScript.RegisterStartupScript(this.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lbl').click();", true);

    }

   

    #region Culture
    protected override void InitializeCulture()
    {
        // string language = "French";
        string language = Convert.ToString(Session["Cultureadmin"]);

        string languageId = "";

        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }

        if (Session["Languageadmin"] != null)
        {
            if (!Session["Languageadmin"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Languageadmin"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Languageadmin"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion


    protected void Clear()
    {

        // ddActCate.Items.Clear();

        txtActName.Text = string.Empty;
        txtActRequirements.Text = string.Empty;
        txtActDescription.Text = string.Empty;
        txtActRequirements.Text = string.Empty;
        txtActTags.Text = string.Empty;

        txtActStartDate.Text = string.Empty;
        txtActEndDate.Text = string.Empty;
        txtActCost.Text = string.Empty;

        ddCompetence.SelectedValue = "0";

        chkListDivision.SelectedValue = "0";
        chkListJobtype.SelectedValue = "0";
        ddActCate.SelectedValue = "0";
    }



    #region
    [WebMethod(EnableSession = true)]
    public static GetActivityCommentById_Result[] GetActivityCommentById(string AcrCreaterId, int ActID, int LoginUserID, int UserType)
    {

        int reqID = 0;
        if (String.IsNullOrEmpty(AcrCreaterId))
        {
            reqID = 0;
        }
        else
        {
            reqID = Convert.ToInt32(AcrCreaterId);
        }
        var actReqList = ActReqComment.GetActivityCommentById(reqID, ActID, LoginUserID, UserType);

        return actReqList.ToArray();
    }
    //public static GetActivityCommentById_Result[] GetActivityCommentById(int AcrCreaterId, int ActID = 0)
    //{

    //    var actReqList = ActReqComment.GetActivityCommentById(AcrCreaterId, ActID);


    //    return actReqList.ToArray();
    //}

    [WebMethod(EnableSession = true)]
    public static string insertmessage(string message, string ActReqId, string ActReqUserId, string ActID)
    {



        string msg = string.Empty;
        CompetenceBM obj = new CompetenceBM();


        var db = new startetkuEntities1();


        var actCat = new ActvityReqComment();
        actCat.AcrCompanyID = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
        actCat.AcrCompComm = message;

        actCat.AcrCreaterId = Convert.ToInt32(ActReqUserId);
        actCat.AcrComToUserID = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        actCat.ActComActReqId = Convert.ToInt32(ActReqId);
        actCat.ActID = Convert.ToInt32(ActID);  //swati on 24/01/2018

        actCat.AcrIsActive = true;
        actCat.AcrIsdeleted = false;
        actCat.AcrComCreateDate = CommonUtilities.GetCurrentDateTime();
        actCat.AcrComUpdateDate = CommonUtilities.GetCurrentDateTime();

        actCat.ActUserTypeId = Convert.ToInt32(HttpContext.Current.Session["OrguserType"]);
        actCat.ActUserCreatedById = Convert.ToInt32(HttpContext.Current.Session["OrgCreatedBy"]);

        db.ActvityReqComments.Add(actCat);
        db.SaveChanges();
        msg = "true";


        return msg;
    }
    public class comment
    {
        public string compId { get; set; }

        public string name { get; set; }
        public string comuserid { get; set; }
        public string Image { get; set; }
        public string compcomment { get; set; }
        public string comcreatedate { get; set; }

    }
    #endregion
}