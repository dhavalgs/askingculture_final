﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webadmin/AdminMasterPage.master"
    AutoEventWireup="true" CodeFile="dashboard.aspx.cs" Inherits="webadmin_dashboard"
    Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg"></asp:Label>
    <div class="row row-bg">
        <!-- .row-bg -->
        <div class="col-sm-6 col-md-3">
            <div class="statbox widget box box-shadow" onclick="javascript:SubmitFrm('DepartmentsList.aspx')">
                <div class="widget-content">
                    <%--<div class="visual cyan">--%>
                    <div class="visual green">
                        <a href="DepartmentsList.aspx">
                            <img src="../assets/img/checklist.png"></a>
                    </div>
                    <div class="title">
                        <%-- 326--%></div>
                    <div class="value">
                        <a href="DepartmentsList.aspx">
                        <%--    <%= CommonMessages.Department%>--%>
                            <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Department" enableviewstate="false"/></a></div>
                    <%--<a class="more" href="parents-list.aspx">View More <i class="pull-right icon-angle-right">
                    </i></a>--%>
                </div>
            </div>
            <!-- /.smallstat -->
        </div>
        <!-- /.col-md-3 -->
        <div class="col-sm-6 col-md-3">
            <div class="statbox widget box box-shadow" onclick="javascript:SubmitFrm('IndustryList.aspx')">
                <div class="widget-content">
                    <div class="visual green">
                        <a href="IndustryList.aspx">
                            <img src="../assets/img/checklist.png"></a>
                    </div>
                    <div class="title">
                        <%--1004--%></div>
                    <div class="value">
                        <a href="IndustryList.aspx">
                           <%-- <%= CommonMessages.Industry%>--%>
                             <asp:Literal ID="Literal2" runat="server" meta:resourcekey="Industry" enableviewstate="false"/></a></div>
                    <%--<a class="more" href="child-list.aspx">View More <i class="pull-right icon-angle-right">
                    </i></a>--%>
                </div>
            </div>
            <!-- /.smallstat -->
        </div>
        <div class="col-sm-6 col-md-3">
            <div class="statbox widget box box-shadow" onclick="javascript:SubmitFrm('CmsList.aspx')">
                <div class="widget-content">
                    <div class="visual green">
                        <a href="CmsList.aspx">
                            <img src="../assets/img/checklist.png"></a>
                    </div>
                    <div class="title">
                        <%--1004--%></div>
                    <div class="value">
                        <a href="CmsList.aspx">CMS</a></div>
                    <%--<a class="more" href="child-list.aspx">View More <i class="pull-right icon-angle-right">
                    </i></a>--%>
                </div>
            </div>
            <!-- /.smallstat -->
        </div>
        <!-- /.col-md-3 -->
        <div class="col-sm-6 col-md-3">
            <div class="statbox widget box box-shadow" onclick="javascript:SubmitFrm('CountryList.aspx')">
                <div class="widget-content">
                    <div class="visual green">
                        <a href="CountryList.aspx">
                            <img src="../assets/img/checklist.png"></a>
                    </div>
                    <div class="title">
                        <%--1004--%></div>
                    <div class="value">
                        <a href="CountryList.aspx">
                          <%--  <%= CommonMessages.Country%>--%>
                          <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Country" enableviewstate="false"/>   </a></div>
                    <%--<a class="more" href="child-list.aspx">View More <i class="pull-right icon-angle-right">
                    </i></a>--%>
                </div>
            </div>
            <!-- /.smallstat -->
        </div>
        <!-- /.col-md-3 -->
        <div class="col-sm-6 col-md-3">
            <div class="statbox widget box box-shadow" onclick="javascript:SubmitFrm('StateList.aspx')">
                <div class="widget-content">
                    <div class="visual green">
                        <a href="StateList.aspx">
                            <img src="../assets/img/checklist.png"></a>
                    </div>
                    <div class="title">
                        <%--1004--%></div>
                    <div class="value">
                        <a href="StateList.aspx">
                          <%--  <%= CommonMessages.State%>--%>
                            <asp:Literal ID="Literal4" runat="server" meta:resourcekey="State" enableviewstate="false"/></a></div>
                    <%--<a class="more" href="child-list.aspx">View More <i class="pull-right icon-angle-right">
                    </i></a>--%>
                </div>
            </div>
            <!-- /.smallstat -->
        </div>
        <!-- /.col-md-3 -->
        <div class="col-sm-6 col-md-3">
            <div class="statbox widget box box-shadow" onclick="javascript:SubmitFrm('WEBActivityCategory.aspx')">
                <div class="widget-content">
                    <div class="visual green">
                        <a href="StateList.aspx">
                            <img src="../assets/img/checklist.png"></a>
                    </div>
                    <div class="title">
                        <%--1004--%></div>
                    <div class="value">
                        <a href="WEBActivityCategory.aspx">
                          <%--  <%= CommonMessages.State%>--%>
                            <asp:Literal ID="Literal14" runat="server" meta:resourcekey="WEBActivityCategory" enableviewstate="false"/></a></div>
                    <%--<a class="more" href="child-list.aspx">View More <i class="pull-right icon-angle-right">
                    </i></a>--%>
                </div>
            </div>
            <!-- /.smallstat -->
        </div>
        <!-- /.col-md-3 -->
         <div class="col-sm-6 col-md-3">
            <div class="statbox widget box box-shadow" onclick="javascript:SubmitFrm('WebActivityLists.aspx')">
                <div class="widget-content">
                    <div class="visual green">
                        <a href="StateList.aspx">
                            <img src="../assets/img/checklist.png"></a>
                    </div>
                    <div class="title">
                        <%--1004--%></div>
                    <div class="value">
                        <a href="WebActivityLists.aspx">
                          <%--  <%= CommonMessages.State%>--%>
                            <asp:Literal ID="Literal15" runat="server" meta:resourcekey="WebActivityLists" enableviewstate="false"/></a></div>
                    <%--<a class="more" href="child-list.aspx">View More <i class="pull-right icon-angle-right">
                    </i></a>--%>
                </div>
            </div>
            <!-- /.smallstat -->
        </div>
        <!-- /.col-md-3 -->
        <%-- <div class="col-sm-6 col-md-3">
            <div class="statbox widget box box-shadow"   onclick="javascript:SubmitFrm('CityList.aspx')">
                <div class="widget-content">
                    <div class="visual green">
                        <a href="CityList.aspx">
                            <img src="../assets/img/checklist.png"></a>
                    </div>
                    <div class="title">--%>
        <%--1004--%><%--</div>--%>
        <%--  <div class="value">
                        <a href="CityList.aspx">City</a></div>--%>
        <%--<a class="more" href="child-list.aspx">View More <i class="pull-right icon-angle-right">
                    </i></a>--%>
        <%--  </div>
            </div>--%>
        <!-- /.smallstat -->
        <%-- </div>--%>
        <!-- /.col-md-3 -->
        <div class="col-sm-6 col-md-3">
            <div class="statbox widget box box-shadow" onclick="javascript:SubmitFrm('JobTypeList.aspx')">
                <div class="widget-content">
                    <div class="visual green">
                        <a href="JobTypeList.aspx">
                            <img src="../assets/img/checklist.png"></a>
                    </div>
                    <div class="title">
                        <%--1004--%></div>
                    <div class="value">
                        <a href="JobTypeList.aspx">Job Type</a></div>
                    <%--<a class="more" href="child-list.aspx">View More <i class="pull-right icon-angle-right">
                    </i></a>--%>
                </div>
            </div>
            <!-- /.smallstat -->
        </div>
        <!-- /.col-md-3 -->
        <div class="col-sm-6 col-md-3">
            <div class="statbox widget box box-shadow" onclick="javascript:SubmitFrm('Competence.aspx')">
                <div class="widget-content">
                    <div class="visual green">
                        <a href="Competence.aspx">
                            <img src="../assets/img/checklist.png"></a>
                    </div>
                    <div class="title">
                        <%--1004--%></div>
                    <div class="value">
                        <a href="Competence.aspx">
                            <%--<%= CommonMessages.Competence%>--%>
                            <asp:Literal ID="Literal5" runat="server" meta:resourcekey="Competence" enableviewstate="false"/></a></div>
                    <%--<a class="more" href="child-list.aspx">View More <i class="pull-right icon-angle-right">
                    </i></a>--%>
                </div>
            </div>
            <!-- /.smallstat -->
        </div>
        <!-- /.col-md-3 -->
        <div class="col-sm-6 col-md-3">
            <div class="statbox widget box box-shadow" onclick="javascript:SubmitFrm('UserList.aspx')">
                <div class="widget-content">
                    <div class="visual green">
                        <a href="UserList.aspx">
                            <img src="../assets/img/checklist.png"></a>
                    </div>
                    <div class="title">
                        <%--1004--%></div>
                    <div class="value">
                        <a href="UserList.aspx">
                          <%--  <%= CommonMessages.CompanyList%>--%>
                             <asp:Literal ID="Literal6" runat="server" meta:resourcekey="CompanyList" enableviewstate="false"/></a></div>
                    <%--<a class="more" href="child-list.aspx">View More <i class="pull-right icon-angle-right">
                    </i></a>--%>
                </div>
            </div>
            <!-- /.smallstat -->
        </div>
        <!-- /.col-md-3 -->
        <div class="col-sm-6 col-md-3">
            <div class="statbox widget box box-shadow" onclick="javascript:SubmitFrm('DivisionList.aspx')">
                <div class="widget-content">
                    <div class="visual green">
                        <a href="DivisionList.aspx">
                            <img src="../assets/img/checklist.png"></a>
                    </div>
                    <div class="title">
                        <%--1004--%></div>
                    <div class="value">
                        <a href="DivisionList.aspx">
                          <%--  <%= CommonMessages.Divisions%>--%>
                             <asp:Literal ID="Literal7" runat="server" meta:resourcekey="Divisions" enableviewstate="false"/></a></div>
                    <%--<a class="more" href="child-list.aspx">View More <i class="pull-right icon-angle-right">
                    </i></a>--%>
                </div>
            </div>
            <!-- /.smallstat -->
        </div>
         <div class="col-sm-6 col-md-3">
            <div class="statbox widget box box-shadow" onclick="javascript:SubmitFrm('DivisionList.aspx')">
                <div class="widget-content">
                    <div class="visual green">
                        <a href="DocumentCategoryList.aspx">
                            <img src="../assets/img/checklist.png"></a>
                    </div>
                    <div class="title">
                        <%--1004--%></div>
                    <div class="value">
                        <a href="DocumentCategoryList.aspx">
                          <%--  <%= CommonMessages.Divisions%>--%>
                             <asp:Literal ID="Literal11" runat="server" meta:resourcekey="DocumentCategoryList" enableviewstate="false"/></a></div>
                    <%--<a class="more" href="child-list.aspx">View More <i class="pull-right icon-angle-right">
                    </i></a>--%>
                </div>
            </div>
            <!-- /.smallstat -->
        </div>
        <div class="col-sm-6 col-md-3">
            <div class="statbox widget box box-shadow" onclick="javascript:SubmitFrm('CategoryList.aspx')">
                <div class="widget-content">
                    <div class="visual green">
                        <a href="DivisionList.aspx">
                            <img src="../assets/img/checklist.png"></a>
                    </div>
                    <div class="title">
                        <%--1004--%></div>
                    <div class="value">
                        <a href="CategoryList.aspx">
                          <%--  <%= CommonMessages.CATEGORY%>--%>
                            <asp:Literal ID="Literal8" runat="server" meta:resourcekey="CATEGORY" enableviewstate="false"/></a></div>
                    <%--<a class="more" href="child-list.aspx">View More <i class="pull-right icon-angle-right">
                    </i></a>--%>
                </div>
            </div>
            <!-- /.smallstat -->
        </div>
        <div class="col-sm-6 col-md-3">
            <div class="statbox widget box box-shadow" onclick="javascript:SubmitFrm('UpdateResource.aspx')">
                <div class="widget-content">
                    <div class="visual green">
                        <a href="DivisionList.aspx">
                            <img src="../assets/img/checklist.png"></a>
                    </div>
                    <div class="title">
                        <%--1004--%></div>
                    <div class="value">
                        <a href="UpdateResource.aspx"><%--Manage Resource--%>
                        <asp:Literal ID="Literal9" runat="server" meta:resourcekey="ManageResource" enableviewstate="false"/></a></div>
                    <%--<a class="more" href="child-list.aspx">View More <i class="pull-right icon-angle-right">
                    </i></a>--%>
                </div>
            </div>
            <!-- /.smallstat -->
        </div>
        <div class="col-sm-6 col-md-3">
            <div class="statbox widget box box-shadow" onclick="javascript:SubmitFrm('EmailSetting.aspx')">
                <div class="widget-content">
                    <div class="visual green">
                        <a href="EmailSetting.aspx">
                            <img src="../assets/img/checklist.png"></a>
                    </div>
                    <div class="title">
                        <%--1004--%></div>
                    <div class="value">
                        <a href="EmailSetting.aspx"><%--Manage Resource--%>
                        <asp:Literal ID="Literal10" runat="server" Text="Email Setting" meta:resourcekey="EmailSetting" enableviewstate="false"/></a></div>
                    <%--<a class="more" href="child-list.aspx">View More <i class="pull-right icon-angle-right">
                    </i></a>--%>
                </div>
            </div>
            <!-- /.smallstat -->
        </div>

        <div class="col-sm-6 col-md-3">
            <div class="statbox widget box box-shadow" onclick="javascript:SubmitFrm('Logo.aspx')">
                <div class="widget-content">
                    <div class="visual green">
                        <a href="Logo.aspx">
                            <img src="../assets/img/checklist.png"></a>
                    </div>
                    <div class="title">
                        <%--1004--%></div>
                    <div class="value">
                        <a href="Logo.aspx"><%--Manage Resource--%>
                        <asp:Literal ID="Literal12" runat="server" Text="Email Setting" meta:resourcekey="SystemSetting" enableviewstate="false"/></a></div>
                    <%--<a class="more" href="child-list.aspx">View More <i class="pull-right icon-angle-right">
                    </i></a>--%>
                </div>
            </div>
            <!-- /.smallstat -->
        </div>
        
        <div class="col-sm-6 col-md-3">
            <div class="statbox widget box box-shadow" onclick="javascript:SubmitFrm('addlanguage.aspx')">
                <div class="widget-content">
                    <div class="visual green">
                        <a href="Logo.aspx">
                            <img src="../assets/img/checklist.png"></a>
                    </div>
                    <div class="title">
                        <%--1004--%></div>
                    <div class="value">
                        <a href="addlanguage.aspx"><%--Manage Resource--%>
                        <asp:Literal ID="Literal13" runat="server" Text="Email Setting" meta:resourcekey="LanguageSetting" enableviewstate="false"/></a></div>
                    <%--<a class="more" href="child-list.aspx">View More <i class="pull-right icon-angle-right">
                    </i></a>--%>
                </div>
            </div>
            <!-- /.smallstat -->
        </div>

         
        <!-- /.col-md-3 -->
    </div>
    <script type="text/javascript">
        function SubmitFrm(cn) {
            window.location = cn;
        }
    </script>
</asp:Content>
