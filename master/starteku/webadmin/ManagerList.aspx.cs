﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;

public partial class webadmin_ManagerList : System.Web.UI.Page
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["UserId"])))
        {
            Response.Redirect("login.aspx");
        }

        if (!IsPostBack)
        {
            GetAllEmployeeList();
            EmployeeListArchiveAll();
            SetDefaultMessage();
        }
    }
    #endregion

    #region method
    protected void SetDefaultMessage()
    {
        // lblMsg.Text = GetLocalResourceObject("lblHelloWorld.Text").ToString();

        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["msg"])))
        {
            if (Convert.ToString(Request.QueryString["msg"]) == "del")
            {
                //lblMsg.Text = CommonModule.msgRecordHasBeenDeletedSuccessfully;
                //lblMsg.Text = CommonMessages.msgRecordHasBeenDeletedSuccessfully;
                lblMsg.Text = GetLocalResourceObject("msgRecordHasBeenDeletedSuccessfully.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.Green;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "ins")
            {
                //lblMsg.Text = CommonMessages.msgRecordInsertedSuccessfully;
                lblMsg.Text = GetLocalResourceObject("msgRecordInsertedSuccessfully.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.Green;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "upd")
            {
                //lblMsg.Text = CommonMessages.msgRecordUpdatedSuccss;
                lblMsg.Text = GetLocalResourceObject("msgRecordUpdatedSuccss.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.Green;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "arc")
            {
                // lblMsg.Text = CommonMessages.msgRecordArchiveSucces;
                lblMsg.Text = GetLocalResourceObject("CommonMessages.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.Green;
            }
        }

    }
    protected void GetAllEmployeeList()
    {
        UserBM obj = new UserBM();
        obj.userIsActive = true;
        obj.userIsDeleted = false;
        var ut = Request.QueryString["ut"];
        var ucId = Request.QueryString["ucId"];
        var ucbId = Request.QueryString["ucbId"];



        if (string.IsNullOrWhiteSpace(ut))
        {
            ut = "1";
        }
        if (string.IsNullOrWhiteSpace(ucId))
        {
            ucId = "0";
        }
        else
        {
            var firstOrDefault = UserBM.GetUserById(Convert.ToInt32(ucId)).FirstOrDefault();
            if (firstOrDefault != null)
                Literal9.Text = @"Company Name : " + firstOrDefault.UserFirstName;
        }
        if (string.IsNullOrWhiteSpace(ucbId))
        {
            //ucbId = "0";
        }
        else
        {
            obj.userCreateBy = (ucbId);
            var firstOrDefault = UserBM.GetUserById(Convert.ToInt32(ucbId)).FirstOrDefault();
            if (firstOrDefault != null)
                Literal9.Text = @"Manager Name : " + firstOrDefault.UserFirstName;
            

        }

        obj.userType = Convert.ToInt32(ut);
        //obj.userCompanyId = Convert.ToInt32(Session["UserId"]);
        obj.userCompanyId = Convert.ToInt32(ucId);

        obj.GetAllEmployee();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvGrid.DataSource = ds.Tables[0];
                gvGrid.DataBind();

                gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvGrid.DataSource = null;
                gvGrid.DataBind();
            }
        }
        else
        {
            gvGrid.DataSource = null;
            gvGrid.DataBind();
        }
    }
    protected void EmployeeListArchiveAll()
    {
        UserBM obj = new UserBM();
        obj.userIsActive = false;
        obj.userIsDeleted = true;
        var ut = Request.QueryString["ut"];
        var ucId = Request.QueryString["ucId"];
        var ucbId = Request.QueryString["ucbId"];

        if (string.IsNullOrWhiteSpace(ut))
        {
            ut = "1";
        }
        if (string.IsNullOrWhiteSpace(ucId))
        {
            ucId = "0";
        }
        if (string.IsNullOrWhiteSpace(ucbId))
        {
            //ucbId = "0";
        }
        else
        {
            obj.userCreateBy = (ucbId);
        }
        obj.userType = Convert.ToInt32(ut);
        //obj.userCompanyId = Convert.ToInt32(Session["UserId"]);
        obj.userCompanyId = Convert.ToInt32(ucId);

        obj.GetAllEmployee();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvArchive.DataSource = ds.Tables[0];
                gvArchive.DataBind();

                gvArchive.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvArchive.DataSource = null;
                gvArchive.DataBind();
            }
        }
        else
        {
            gvArchive.DataSource = null;
            gvArchive.DataBind();
        }
    }
    #endregion

    #region grid Event
    protected void gvGrid_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "archive")
        {
            UserBM obj = new UserBM();
            obj.userId = Convert.ToInt32(e.CommandArgument);
            obj.userIsActive = false;
            obj.userIsDeleted = true;
            obj.EmployeeStatusUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    GetAllEmployeeList();
                    EmployeeListArchiveAll();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.Green;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }
        if (e.CommandName == "restore")
        {
            UserBM obj = new UserBM();
            obj.userId = Convert.ToInt32(e.CommandArgument);
            obj.userIsActive = true;
            obj.userIsDeleted = true;
            obj.EmployeeStatusUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    GetAllEmployeeList();
                    EmployeeListArchiveAll();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.Green;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }
    }

    protected void gvArchive_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "restore")
        {
            UserBM obj = new UserBM();
            obj.userId = Convert.ToInt32(e.CommandArgument);
            obj.userIsActive = true;
            obj.userIsDeleted = false;
            obj.EmployeeStatusUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    GetAllEmployeeList();
                    EmployeeListArchiveAll();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.Green;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                    lblMsg.Text = returnMsg;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }
        if (e.CommandName == "archive")
        {
            UserBM obj = new UserBM();
            obj.userId = Convert.ToInt32(e.CommandArgument);
            obj.userIsActive = false;
            obj.userIsDeleted = true;
            obj.EmployeeStatusUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    GetAllEmployeeList();
                    EmployeeListArchiveAll();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.Green;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                    
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }
        else if (e.CommandName == "permanentlydelete")
        {
            UserBM obj = new UserBM();
            obj.userId = Convert.ToInt32(e.CommandArgument);
            obj.userIsActive = false;
            obj.userIsDeleted = false;
            obj.EmployeeStatusUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    GetAllEmployeeList();
                    EmployeeListArchiveAll();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.Green;
                    TabContainer1.ActiveTabIndex = 1;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                    TabContainer1.ActiveTabIndex = 1;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
                TabContainer1.ActiveTabIndex = 1;
            }
        }
    }

    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        string language = Convert.ToString(Session["Cultureadmin"]);
        //string language = "Denish";
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Denish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Languageadmin"] != null)
        {
            if (!Session["Languageadmin"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Languageadmin"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Languageadmin"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion
}