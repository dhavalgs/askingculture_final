﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using System.Configuration;
using System.Net.Mail;
using System.Web.UI.WebControls;
using starteku_BusinessLogic.Model;
using starteku_BusinessLogic;
using System.Data;


public partial class webadmin_forget_password : System.Web.UI.Page
{
    #region Page Event
    startetkuEntities1 db = new startetkuEntities1();
    protected void Page_Load(object sender, EventArgs e)
    {

        LogoMaster logo = db.LogoMasters.FirstOrDefault();
        if (logo != null)
        {

            imgLoginLogo.ImageUrl = "../Log/upload/Userimage/" + logo.LoginLogo;
            ViewState["imgLoginLogo"] = logo.LoginLogo;
        }
    }
     #endregion

      #region method
        private string encrypt(string str)
        {
            string _result = string.Empty;
            char[] temp = str.ToCharArray();
            foreach (var _singleChar in temp)
            {
                var i = (int)_singleChar;
                i = i - 2;
                _result += (char)i;
            }
            return _result;
        }

        private string decrypt(string str)
        {
            string _result = string.Empty;
            char[] temp = str.ToCharArray();
            foreach (var _singleChar in temp)
            {
                var i = (int)_singleChar;
                i = i + 2;
                _result += (char)i;
            }
            return _result;
        }

        #endregion


        protected void btnForgot_ServerClick(object sender, EventArgs e)
        {
            try
            {
                if (txtemail.Text != "")
                {
                    //string path = Server.MapPath("..") + "\\MailFiles\\forgot_password.html";
                    //StreamReader st = new StreamReader(path, false);
                    //string tempString = st.ReadToEnd();

                    UserBM Cust = new UserBM();
                    Cust.userEmail = txtemail.Text.Trim();
                    Cust.SelectPasswordByUserName();
                    DataSet ds = Cust.ds;
                    lblMsg.Text = "";
                    if (ds != null)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {

                            String name = Convert.ToString(ds.Tables[0].Rows[0]["userFirstName"]);
                            string fromid = Convert.ToString(ConfigurationManager.AppSettings["fromid"]);
                            String id = CommonModule.encrypt(Convert.ToString(ds.Tables[0].Rows[0]["userId"]));
                            if (name != null)
                            {
                                String Subject = "Password Reset Confirnation for " + name;
                                CmsBM objMail = new CmsBM();
                                objMail.cmsName = "Forgotpassword";
                                objMail.SelectMailTemplateByName();
                                DataSet dsMail = objMail.ds;
                                if (dsMail.Tables[0].Rows.Count > 0)
                                {
                                    string confirmMail = dsMail.Tables[0].Rows[0]["cmsDescription"].ToString();
                                    string tempString = confirmMail;
                                    tempString = tempString.Replace("###name###", name);
                                    tempString = tempString.Replace("###id###", id);
                                    //SendMail(tempString, txtemail.Text, name);
                                    CommonModule.SendMailToUser(txtemail.Text, Subject, tempString, fromid,"0");
                                }
                            }
                            else
                            {

                                lblMsg.Text = "Email address not exist in database.";
                            }
                        }
                        else
                        {

                            lblMsg.Text = "Email address not exist in database.";
                        }
                    }
                }
                else
                {
                    lblMsg.Text = "Please enter email address.";
                    lblMsg.Visible = true;

                }

            }
            catch(Exception ex)
            {
                lblMsg.Visible = true;
                lblMsg.Text += "Incorrect Email Address";
                ExceptionLogger.LogException(ex,0,lblMsg.Text);
            }

        }
       

}