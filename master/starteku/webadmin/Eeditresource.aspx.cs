﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Reflection;
using System.Resources;
using System.IO;
using System.Xml;
using System.Collections;
using System.Xml.Linq;
using System.Linq;
using System.Web;


public partial class webadmin_Eeditresource : System.Web.UI.Page
{
    public string filename;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoaderOptimization_global();
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        filename = Request.QueryString["file"];
        if (!String.IsNullOrEmpty(Request.QueryString["Lid"]))
        {

            filename = Request.PhysicalApplicationPath + "\\Organisation\\App_LocalResources\\" + filename;
        }
        else if (!String.IsNullOrEmpty(Request.QueryString["Bid"]))
        {
            filename = Request.PhysicalApplicationPath + "\\webadmin\\App_LocalResources\\" + filename;
        }
        else
        {
            filename = Request.PhysicalApplicationPath + "App_GlobalResources\\" + filename;
        }
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.Load(filename);
        XDocument xDoc = XDocument.Load(filename);
        var elementToBeEdited = from xle in xDoc.Element("root").Elements("data") where xle.Attribute("name").Value == Convert.ToString(Request.QueryString["key"]) select xle;
        if (elementToBeEdited != null && elementToBeEdited.Count() > 0)
        {
            elementToBeEdited.First().SetElementValue("value", HttpUtility.HtmlDecode(txtResourceValue.Text));
            xDoc.Save(filename);
        }
        //return;
      
        if (!String.IsNullOrEmpty(Request.QueryString["Bid"]))
        {
            Response.Redirect("UpdateResource.aspx?Bid="+Request.QueryString["Bid"]);
        }
        else if (!String.IsNullOrEmpty(Request.QueryString["Lid"]))
        {
            Response.Redirect("UpdateResource.aspx?Lid=" + Request.QueryString["Lid"]);
        }
      




    }

    protected void LoaderOptimization_global()
    {
        filename = Request.QueryString["file"];

        if (!String.IsNullOrEmpty(Request.QueryString["Lid"]))
        {
            filename = Request.PhysicalApplicationPath + "\\Organisation\\App_LocalResources\\" + filename;
        }
        else if (!String.IsNullOrEmpty(Request.QueryString["Bid"]))
        {
            filename = Request.PhysicalApplicationPath + "\\webadmin\\App_LocalResources\\" + filename;
        }
        else
        {
            filename = Request.PhysicalApplicationPath + "App_GlobalResources\\" + filename;
        }
        // 
        string key = Request.QueryString["key"];
        Label1.Text = key;
        ResXResourceSet rset = new ResXResourceSet(filename);
        txtResourceValue.Text = rset.GetString(key);
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Request.QueryString["Bid"]))
        {
            Response.Redirect("UpdateResource.aspx?Bid=" + Request.QueryString["Bid"]);
        }
        else if (!String.IsNullOrEmpty(Request.QueryString["Lid"]))
        {
            Response.Redirect("UpdateResource.aspx?Lid=" + Request.QueryString["Lid"]);
        }
    }

}