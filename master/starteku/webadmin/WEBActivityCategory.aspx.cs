﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using starteku_BusinessLogic.Model;

public partial class webadmin_WEBActivityCategory : System.Web.UI.Page
{
    // Developed By JAINAM SHAH -----  16-3-2017 ------

    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        //{
        //    Response.Redirect("login.aspx");
        //}
        if (!IsPostBack)
        {
            //Division.InnerHtml = "Welcome   " + Convert.ToString(Session["OrgUserName"]) + "!";
            Category.InnerHtml = GetLocalResourceObject("Welcome.Text").ToString() + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            //PopulateRootLevel();
            GetAllActivityCategory(0);
            // DivisionArchiveAll();
            SetDefaultMessage();

            gvGrid1();
            getalldt();



            //if (Convert.ToInt32(Session["OrguserType"]) == 2)
            //{
            //    Response.Redirect("Manager-dashboard.aspx");

            //}
            //else if (Convert.ToInt32(Session["OrguserType"]) == 3)
            //{
            //    Response.Redirect("Employee_dashboard.aspx");

            //}

        }


    }

    #endregion

    #region method
    protected void SetDefaultMessage()
    {
        // lblMsg.Text = GetLocalResourceObject("lblHelloWorld.Text").ToString();

        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["msg"])))
        {
            if (Convert.ToString(Request.QueryString["msg"]) == "del")
            {
                //lblMsg.Text = CommonModule.msgRecordHasBeenDeletedSuccessfully;
                //lblMsg.Text = CommonMessages.msgRecordHasBeenDeletedSuccessfully;
                lblMsg.Text = GetLocalResourceObject("msgRecordHasBeenDeletedSuccessfully.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "ins")
            {
                //lblMsg.Text = CommonMessages.msgRecordInsertedSuccessfully;
                lblMsg.Text = GetLocalResourceObject("msgRecordInsertedSuccessfully.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "upd")
            {
                //lblMsg.Text = CommonMessages.msgRecordUpdatedSuccss;
                lblMsg.Text = GetLocalResourceObject("msgRecordUpdatedSuccss.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "arc")
            {
                // lblMsg.Text = CommonMessages.msgRecordArchiveSucces;
                lblMsg.Text = GetLocalResourceObject("CommonMessages.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
        }

    }

    protected void GetAllActivityCategory(int comp)
    {

        var actCatList = ActivityCategoryLogic.GetActivityCategoryList(0, comp, -1);
        var db = new startetkuEntities1();

        //foreach (var i in actCatList) {

        //    if (i.ActCatReqEnabled == true) { 

        //    }

        //}

        if (actCatList.Any())
        {
            gvGrid.DataSource = actCatList;

            gvGrid.DataBind();
        }
        else
        {
            gvGrid.DataSource = null;

            gvGrid.DataBind();

        }

        GetAllActivityCategory_Archive();

    }

    protected void GetAllActivityCategory_Archive()
    {
        var actCatList = ActivityCategoryLogic.GetActivityCategoryList(0, 0,0);
        

        //foreach (var i in actCatList) {

        //    if (i.ActCatReqEnabled == true) { 

        //    }

        //}

        gvGridArchive.DataSource = actCatList;

        gvGridArchive.DataBind();


    }

    protected void gvGrid1()
    {
        var actCatList = ActivityCategoryLogic.GetActivityCategoryList(0, 0, 1);
        gvGrid.DataSource = actCatList;
        gvGrid.DataBind();
      
    }

    #endregion


    protected void Test_click(object sender, EventArgs e)
    {



        Session["ActID"] = 0;
        ResourceLanguageBM obj = new ResourceLanguageBM();
        DataSet ds = new DataSet();
        obj.LangCompanyID = Convert.ToInt32(Session["OrgUserId"]);
        obj.LangActCatId = Convert.ToInt32(Session["ActID"]);
        obj.LangType = "actcat";
        ds = obj.GetAllResourceLangTran();

        gridTranslate.DataSource = ds.Tables[0];
        gridTranslate.DataBind();
        ClientScript.RegisterStartupScript(this.GetType(), "ModelPopup", "$('.add_user').click(); ShowCreateLable(false);", true);

    }

    protected void getc(object sender, EventArgs e)
    {

        int comp = Convert.ToInt32(DropDownList1.SelectedItem.Value);
        GetAllActivityCategory(comp);

    }

    protected void getalldt()
    {
        var db = new startetkuEntities1();
        // ddActCate.Items.Insert(0, new ListItem(GetLocalResourceObject("Current.Text").ToString(), "0"));
        //   int userCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
        // var getdata = db.UserMasters.Where(o => o.PdpCompanyID == userCompanyId && o.PpdHisIsActive == true).ToList();

        UserBM obj = new UserBM();
        obj.userType = 1;
        obj.userIsDeleted = false;
        obj.userIsActive = true;
        obj.userCompanyId = 0;
        obj.userCreateBy = "0";

        //obj.userCompanyId = Convert.ToInt32(Session["UserId"]);

        obj.GetAllEmployee();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            DropDownList1.DataSource = ds.Tables[0];
            DropDownList1.DataTextField = "comname";
            DropDownList1.DataValueField = "userId";
            DropDownList1.DataBind();
            //ddActCate.Items.Insert(0, new ListItem(GetLocalResourceObject("Current.Text").ToString(), "0"));
        }
    }



    #region grid Event
    protected void gvGrid_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Archive_CAT")
        {
            string ActCatId = e.CommandArgument.ToString();

            var db = new startetkuEntities1();
            var ac = Convert.ToInt32(ActCatId);
            var isDup = db.ActivityCategoryMasters.FirstOrDefault(o => o.ActCatId == ac);
            if (isDup != null)
            {
                isDup.ActCatsActive = false; // for archive
                db.SaveChanges();
                int comp = Convert.ToInt32(DropDownList1.SelectedItem.Value);
                GetAllActivityCategory(comp);
                //lblMsg.Text = GetLocalResourceObject("msgRecordHasBeenDeletedSuccessfully.Text").ToString();
              
                gvGrid1();
                lblMsg.Text = GetLocalResourceObject("CatActAr.Text").ToString();

                //lblMsg.ForeColor = System.Drawing.Color.White;
                lblMsg.Visible = true;
            }
        }

        if (e.CommandName == "Restore_CAT")
        {
            string ActCatId = e.CommandArgument.ToString();

            var db = new startetkuEntities1();
            var ac = Convert.ToInt32(ActCatId);
            var isDup = db.ActivityCategoryMasters.FirstOrDefault(o => o.ActCatId == ac);
            if (isDup != null)
            {
                isDup.ActCatsActive = true; // for archive
                db.SaveChanges();

                int comp = Convert.ToInt32(DropDownList1.SelectedItem.Value);
                GetAllActivityCategory(comp);
               
                gvGrid1();
                lblMsg.Text = GetLocalResourceObject("CatActRe.Text").ToString();
                lblMsg.Visible = true;
                
            }
        }


        else if (e.CommandName == "Delete_CAT")
        {
            string ActCatId = e.CommandArgument.ToString();

            var db = new startetkuEntities1();
            var ac = Convert.ToInt32(ActCatId);
            var isDup = db.ActivityCategoryMasters.FirstOrDefault(o => o.ActCatId == ac);
            if (isDup != null)
            {
                db.ActivityCategoryMasters.Remove(isDup); // for Delete
                db.SaveChanges();
                int comp = Convert.ToInt32(DropDownList1.SelectedItem.Value);
                GetAllActivityCategory(comp);
                gvGrid1();
                lblMsg.Text = GetLocalResourceObject("CatActDe.Text").ToString();
                lblMsg.Visible = true;

            }
        }


        else if (e.CommandName == "Edit_CAT")
        {
            string ActCatId = e.CommandArgument.ToString();
            var db = new startetkuEntities1();
            var ac = Convert.ToInt32(ActCatId);
            var getActCatData = db.ActivityCategoryMasters.FirstOrDefault(o => o.ActCatId == ac);

            hdnActCatId.Value = getActCatData.ActCatId.ToString();
            txtActName.Text = getActCatData.ActCatName;
            string ACR = getActCatData.ActCatReqEnabled.ToString();
            if (ACR == "True")
            { rdoActCatReqEnabled.SelectedValue = "1"; }
            else
            { rdoActCatReqEnabled.SelectedValue = "2"; }

            string ACR1 = getActCatData.ActCompEnabled.ToString();
            if (ACR1 == "True")
            { rdoActCompEnabled.SelectedValue = "1"; }
            else
            { rdoActCompEnabled.SelectedValue = "2"; }



            //model open code

            //submit text : update
            btnsubmit.Visible = true;
            //lblCreate.Visible = false;
            //LblUpdate.Visible = true;
            Buttonupdate.Visible = true;


            ClientScript.RegisterStartupScript(this.GetType(), "ModelPopup", "$('.add_user').click();ShowEditLable(); ", true);
            Session["ActID"] = Convert.ToInt32(ActCatId);

            ResourceLanguageBM obj = new ResourceLanguageBM();
            DataSet ds = new DataSet();
            obj.LangCompanyID = Convert.ToInt32(Session["OrgUserId"]);
            obj.LangActCatId = Convert.ToInt32(Session["ActID"]);
            obj.LangType = "actcat";
            ds = obj.GetAllResourceLangTran();

            gridTranslate.DataSource = ds.Tables[0];
            gridTranslate.DataBind();
            //LangTranslationMasterBM lobj = new LangTranslationMasterBM();
            //string resLangID, LangText;
            //for (int i = 0; i < gridTranslate.Rows.Count; i++)
            //{
            //    resLangID = ((HiddenField)gridTranslate.Rows[i].Cells[0].FindControl("hdnResLangID")).Value;
            //    LangText = ((TextBox)gridTranslate.Rows[i].Cells[0].FindControl("txtTranValue")).Text;
            //    lobj.LangType = "actcat";
            //    lobj.LangActCatId = Convert.ToInt32(ActCatId);
            //    lobj.ResLangID = Convert.ToInt32(resLangID);
            //    lobj.LangText = LangText;
            //    lobj.LangCompanyID =Convert.ToInt32( getActCatData.ActCatCreatedBy);
            //    lobj.InsertLangResource();
            //}

        }
    }
    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        // string language = "French";
        string language = Convert.ToString(Session["Cultureadmin"]);

        string languageId = "";

        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }

        if (Session["Languageadmin"] != null)
        {
            if (!Session["Languageadmin"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Languageadmin"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Languageadmin"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion



    protected void Clear()
    {

        txtActName.Text = string.Empty;
        btnsubmit.Visible = true;
        Buttonupdate.Visible = false;

    }
}