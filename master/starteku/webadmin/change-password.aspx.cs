﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;

public partial class webadmin_change_password : System.Web.UI.Page
{
    #region Page Event

    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["UserName"])))
        {
            Response.Redirect("login.aspx");
        }

        SetDefaultMessage();
    }
    #endregion

    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        try
        {
            //if (Convert.ToString(Session["LoggedType"]) == "center")
            {
                Int32 userid = Convert.ToInt32(Session["UserId"]);

                UserBM obj = new UserBM();
                obj.userId = Convert.ToInt32(Session["UserId"]);
                obj.SelectPasswordByUserId();
                DataSet ds = obj.ds;
                if (CommonModule.decrypt(Convert.ToString(ds.Tables[0].Rows[0]["userPassword"])) == txtOldPassword.Text.Trim())
                {
                    UserBM obj1 = new UserBM();
                    obj1.userId = Convert.ToInt32(Session["UserId"]);
                    obj1.userPassword = CommonModule.encrypt(txtPassword.Text.Trim());
                    obj1.Updatepassword();
                    if (obj1.ReturnBoolean == true)
                    {
                        ClearData();
                        lblMsg.Text = CommonModule.msgPasswordHasBeenUpdateSuccess;
                        lblMsg.ForeColor = System.Drawing.Color.Green;
                    }
                    else
                    {
                        lblMsg.Text = CommonModule.msgSomeProblemOccure;
                    }
                }
                else
                {
                    lblMsg.Text = CommonModule.msgPasswordOldNotMatch;
                }
            }
            //else
            //{
            //    Int32 userID = Convert.ToInt32(Session["UserId"]);

            //    UserBM obj = new UserBM().UserMasterUserID(userID);

            //    if (CommonModule.decrypt(obj.password) == txtOldPassword.Text.Trim())
            //    {
            //        obj.password = CommonModule.encrypt(txtPassword.Text.Trim());
            //        if (new UserMasterBM().UserMasterUpdate(obj) == 1)
            //        {
            //            ClearData();
            //            lblMsg.Text = CommonModule.msgPasswordHasBeenUpdateSuccess;
            //            lblMsg.ForeColor = System.Drawing.Color.Green;
            //        }
            //        else
            //        {
            //            lblMsg.Text = CommonModule.msgSomeProblemOccure;
            //        }
            //    }
            //    else
            //    {
            //        lblMsg.Text = CommonModule.msgPasswordOldNotMatch;
            //    }
            //}
        }
        catch (Exception ex)
        {
            lblMsg.Text = CommonModule.msgSomeProblemOccure;
            CommonModule.WriteLog("error in btnsubmint  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
        }
    }

    protected void btnCancel_click(object sender, EventArgs e)
    {
        Response.Redirect("CountryList.aspx");        
    }
    #endregion

    #region method
    public void SetDefaultMessage()
    {
        lblDataDisplayTitle.Text = "Change Password";
    }

    public void ClearData()
    {
        txtPassword.Text = string.Empty;
        txtOldPassword.Text = string.Empty;
        txtConfirmPassword.Text = string.Empty;
    }
    #endregion
}