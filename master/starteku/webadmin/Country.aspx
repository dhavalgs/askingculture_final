﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webadmin/AdminMasterPage.master" AutoEventWireup="true" CodeFile="Country.aspx.cs" 
Inherits="webadmin_Country" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" 
        meta:resourcekey="lblMsgResource1"></asp:Label>
 <div class="row">
        <div class="col-md-12">
            <div class="col-md-12">
                <div class="indicatesRequireFiled">
                    * <%-- <%= CommonMessages.Indicatesrequiredfield%>--%>
                    <asp:Literal ID="Literal9" runat="server" meta:resourcekey="Indicatesrequiredfield" enableviewstate="false"/>
                </div>
                <div class="widget box">
                    <div class="widget-header">
                        <h4>
                            <i class="icon-reorder"></i>
                            <asp:Label runat="server" ID="lblDataDisplayTitle" Text="Country" 
                                meta:resourcekey="lblDataDisplayTitleResource1"></asp:Label>
                        </h4>
                    </div>
                    <div class="widget-content">
                        <%--   <form class="form-horizontal row-border" action="#">--%>
                        <div class="form-horizontal row-border">
                            <div class="form-group">
                                <label class="col-md-2 control-label">
                                    <%-- <%= CommonMessages.Country%>--%>
                                     <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Country" enableviewstate="false"/>:<span class="starValidation">*</span></label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="txtcountryName" MaxLength="50" 
                                        CssClass="form-control" meta:resourcekey="txtcountryNameResource1" />
                                   
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtcountryName"
                                        ErrorMessage="Please Enter Country Name." CssClass="commonerrormsg" Display="Dynamic"
                                        ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator5Resource1"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            
                            <div class="form-horizontal row-border">
                                <div class="btn-toolbar">                                    
                                    <asp:Button runat="server" ID="btnsubmit" Text="Save" CssClass="btn btn-success"
                                         ValidationGroup="chk" OnClick="btnsubmit_click" 
                                        meta:resourcekey="btnsubmitResource1"/>
                                    <asp:Button runat="server" ID="btnCancel" Text="Cancel" CssClass="btn" ValidationGroup="chk"
                                        CausesValidation="False" OnClick="btnCancel_click" 
                                        meta:resourcekey="btnCancelResource1" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--=== no-padding ===-->
    <div class="row">
    </div>
    <div class="row">
    </div>
    <!-- /no-padding and table-tabletools -->
    <!--=== no-padding and table-colvis ===-->
    <div class="row">
    </div>
    <!-- /no-padding and table-colvis -->
    <!--=== Horizontal Scrolling ===-->
    <div class="row">
    </div>
    <!-- /Normal -->
    <!-- /Page Content -->
</asp:Content>

