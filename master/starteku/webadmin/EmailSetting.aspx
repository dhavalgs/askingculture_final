﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webadmin/AdminMasterPage.master"
    AutoEventWireup="true" CodeFile="EmailSetting.aspx.cs" Inherits="webadmin_EmailSetting" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" 
        meta:resourcekey="lblMsgResource1"></asp:Label>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-12">
                <div class="indicatesRequireFiled">
                    * <%--Indicates required field--%>
                     <asp:Literal ID="Literal2" runat="server" Text="Indicates required field" 
                        EnableViewState="False" meta:resourcekey="Literal2Resource1" />
                </div>
                <div class="widget box">
                    <div class="widget-header">
                        <h4>
                            <i class="icon-reorder"></i>
                            <asp:Label runat="server" ID="lblDataDisplayTitle" Text="Email Setting" 
                                meta:resourcekey="lblDataDisplayTitleResource1"></asp:Label>
                        </h4>
                    </div>
                    <div class="widget-content">
                        <div class="form-horizontal row-border">
                            <div class="form-group">
                                <label class="col-md-2 control-label">
                                    <asp:Literal ID="Literal3" runat="server" Text="Email Sender" 
                                    EnableViewState="False" meta:resourcekey="Literal3Resource1" />:<span
                                        class="starValidation">*</span></label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="txtsEmail" MaxLength="50" 
                                        CssClass="form-control" meta:resourcekey="txtsEmailResource1" />
                                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" CssClass="commonerrormsg"
                                        ControlToValidate="txtsEmail" Display="Dynamic" ValidationGroup="chk" 
                                        ErrorMessage="Please Enter Email." meta:resourcekey="rfvEmailResource1"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="txtEmail" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                        ControlToValidate="txtsEmail" CssClass="commonerrormsg" Display="Dynamic" ValidationGroup="chk"
                                        ErrorMessage="Please Enter Valid Email." 
                                        meta:resourcekey="txtEmailResource1"></asp:RegularExpressionValidator>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">
                                    <asp:Literal ID="Literal4" runat="server" Text="password" 
                                    EnableViewState="False" meta:resourcekey="Literal4Resource1"/>:<span
                                        class="starValidation">*</span></label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="txtpassword" MaxLength="50" TextMode="Password"
                                        CssClass="form-control" meta:resourcekey="txtpasswordResource1" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtpassword"
                                        ErrorMessage="Please Enter Password." CssClass="commonerrormsg" Display="Dynamic"
                                        ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator1Resource1"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">
                                    <asp:Literal ID="Literal7" runat="server" Text="Confirm Password" 
                                    EnableViewState="False" meta:resourcekey="Literal7Resource1" />:<span
                                        class="starValidation">*</span></label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="txtConfirmPassword" MaxLength="50" TextMode="Password"
                                        CssClass="form-control" meta:resourcekey="txtConfirmPasswordResource1" />
                                    <asp:CompareValidator ID="cvPasswordNotMatch" ControlToValidate="txtConfirmPassword"
                                        ControlToCompare="txtpassword" runat="server" CssClass="commonerrormsg" Display="Dynamic"
                                        ValidationGroup="chk" ErrorMessage="Password and confirm password does not match."
                                        meta:resourcekey="cvPasswordNotMatchResource1"></asp:CompareValidator>
                                    <asp:RequiredFieldValidator ID="rfvConfirmPassword" runat="server" CssClass="commonerrormsg"
                                        ControlToValidate="txtConfirmPassword" Display="Dynamic" ErrorMessage="Confirm password is required."
                                        ValidationGroup="chk" meta:resourcekey="rfvConfirmPasswordResource1"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">
                                    Host:<span class="starValidation">*</span></label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="txthost" MaxLength="50" CssClass="form-control" 
                                        meta:resourcekey="txthostResource1" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txthost"
                                        ErrorMessage="Please Enter Host." CssClass="commonerrormsg" Display="Dynamic"
                                        ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator3Resource1"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">
                                    <asp:Literal ID="Literal5" runat="server" Text="Port" EnableViewState="False" meta:resourcekey="Literal5Resource1" />:<span
                                        class="starValidation"></span></label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="txtport" MaxLength="10" CssClass="form-control" 
                                        meta:resourcekey="txtportResource1" />
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"
                                        TargetControlID="txtport" Enabled="True" ValidChars=" ">
                                    </cc1:FilteredTextBoxExtender>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">
                                    <asp:Literal ID="Literal1" runat="server" EnableViewState="False" 
                                    Text="Display Name" meta:resourcekey="Literal1Resource1" />:<span
                                        class="starValidation">*</span></label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="txtdisplayname" MaxLength="50" 
                                        CssClass="form-control" meta:resourcekey="txtdisplaynameResource1" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtdisplayname"
                                        ErrorMessage="Please Enter  Display Name." CssClass="commonerrormsg" Display="Dynamic"
                                        ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator2Resource1"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-horizontal row-border">
                                <div class="btn-toolbar">
                                    <asp:Button runat="server" ID="btnsubmit" Text="Save" CssClass="btn btn-success"
                                        ValidationGroup="chk" meta:resourcekey="btnsubmitResource1" OnClick="btnsubmit_click"/>
                                    <asp:Button runat="server" ID="btnCancel" Text="Cancel" CssClass="btn" ValidationGroup="chk"
                                        CausesValidation="False" meta:resourcekey="btnCancelResource1" OnClick="btnCancel_click"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--=== no-padding ===-->
    <div class="row">
    </div>
    <div class="row">
    </div>
    <!-- /no-padding and table-tabletools -->
    <!--=== no-padding and table-colvis ===-->
    <div class="row">
    </div>
    <!-- /no-padding and table-colvis -->
    <!--=== Horizontal Scrolling ===-->
    <div class="row">
    </div>
    <!-- /Normal -->
    <!-- /Page Content -->
</asp:Content>
