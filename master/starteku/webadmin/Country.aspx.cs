﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using System.Threading;
using System.Globalization;

public partial class webadmin_Country : System.Web.UI.Page
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["UserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                GetAllCountrybyid();
            }
        }
    }
    #endregion

    #region method
    protected void SetDefaultMessage()
    {

        string id = "?id=";
        string childurl = "child.aspx";
        if (Request.RawUrl.ToString().ToLower().Contains(childurl.ToLower()))
        {
            if (Request.RawUrl.ToString().ToLower().Contains(id.ToLower()))
            {
                lblDataDisplayTitle.Text = "Edit Children";

            }
            else
            {
                lblDataDisplayTitle.Text = "Add Children";
            }
        }
    }
    protected void InsertCountry()
    {
        CountryBM obj2 = new CountryBM();
        obj2.CountryCheckDuplicationmaster(txtcountryName.Text, -1);
        String returnMsg = obj2.ReturnString;
        if (returnMsg == "")
        {
            CountryBM obj = new CountryBM();
            obj.couName = txtcountryName.Text;
            obj.couCompanyId = 0;
            obj.couIsActive = true;
            obj.couIsDeleted = false;
            obj.couCreatedDate = DateTime.Now;
            obj.countryCode = 0;
            //obj.InsertCountry();
            obj.InsertCountryMaster();
            //  DataSet ds = obj.ds;
            if (obj.ReturnBoolean == true)
            {
                Response.Redirect("CountryList.aspx?msg=ins");
            }
        }
        else
        {
            if (returnMsg == "CountryName")
                lblMsg.Text = CommonModule.msgNameAlreadyExists;
            else
                lblMsg.Text = CommonModule.msgProblemInsertRecord;

        }

    }
    protected void updateCountry()
    {
        CountryBM obj2 = new CountryBM();
        obj2.CountryCheckDuplicationmaster(txtcountryName.Text, Convert.ToInt32(Request.QueryString["id"]));
        String returnMsg = obj2.ReturnString;
        if (returnMsg == "")
        {
            CountryBM obj = new CountryBM();
            obj.couId = Convert.ToInt32(Request.QueryString["id"]);
            obj.couName = txtcountryName.Text;
            obj.couUpdatedDate = DateTime.Now;
            obj.UpdateCountrymaster();
            //  DataSet ds = obj.ds;
            if (obj.ReturnBoolean == true)
            {
                Response.Redirect("CountryList.aspx?msg=upd");
            }
        }
        else
        {
            if (returnMsg == "CountryName")
                lblMsg.Text = CommonModule.msgNameAlreadyExists;
            else
                lblMsg.Text = CommonModule.msgProblemInsertRecord;
        }
    }
    protected void GetAllCountrybyid()
    {
        CountryBM obj = new CountryBM();
        obj.couId = Convert.ToInt32(Request.QueryString["id"]);
        obj.GetAllCountrybyidmaster();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["couName"])))
                txtcountryName.Text = Convert.ToString(ds.Tables[0].Rows[0]["couName"]);
        }

    }

    #endregion
    
    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Request.QueryString["id"]))
        {
            updateCountry();
        }
        else
        {
            InsertCountry();
        }
    }
    protected void btnCancel_click(object sender, EventArgs e)
    {
        Response.Redirect("CountryList.aspx");
    }
    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        string language = Convert.ToString(Session["Cultureadmin"]);
        //string language = "Denish";
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Denish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Languageadmin"] != null)
        {
            if (!Session["Languageadmin"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Languageadmin"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Languageadmin"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion
}