﻿
using starteku_BusinessLogic.Model;
using System;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;


namespace App_code
{
    public class CommonUtility
    {
        #region
        public static void RemoveJob_UserCache(int jmid = 0, int umid = 0)
        {
            
        }
        public static int RemoveAllJobCache()
        {

            try
            {
                var i = 0;
                foreach (System.Collections.DictionaryEntry entry in HttpContext.Current.Cache)
                {
                    i++;
                    HttpContext.Current.Cache.Remove((string)entry.Key);
                }
                return i;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static void RemoveCache(string cacheKey)
        {
            try
            {
                HttpRuntime.Cache.Remove(cacheKey);
            }
            catch (Exception)
            {


            }
            try
            {
                if (HttpContext.Current != null)
                    HttpContext.Current.Cache.Remove(cacheKey);

            }
            catch (Exception)
            {


            }

        }
        #endregion



        #region cookie

     

        public static string GetCookie(string cookieName)
        {
            try
            {
                var httpCookie = HttpContext.Current.Request.Cookies[cookieName];
                if (httpCookie != null)
                    return httpCookie.Value;
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception)
            {

                return string.Empty;
            }
           
        }

        public static void SetCookie(string cookieName, string cookieValue)
        {
            var cookieUserName = new HttpCookie(cookieName);
            cookieUserName.Expires = DateTime.Today.AddYears(1);
            cookieUserName.Value = cookieValue;
            HttpContext.Current.Response.Cookies.Add(cookieUserName);
        }
        public static void RemoveCookie(string cookieName)
        {
            HttpCookie myCookie = new HttpCookie(cookieName);
            myCookie.Expires = DateTime.Now.AddDays(-1d);
            HttpContext.Current.Response.Cookies.Add(myCookie);
        }

        #endregion
       
        public static DateTime ConvertStringToDateTimewithformay(string dt)
        {
            try
            {
                IFormatProvider culture = new System.Globalization.CultureInfo("en-GB", true);

                if (string.IsNullOrWhiteSpace(dt))
                {
                    dt = null;
                }
                else
                {
                    // //"6/30/2015 08:00 PM"
                    DateTime date = DateTime.Parse(dt, culture, System.Globalization.DateTimeStyles.AssumeLocal);
                    return date;
                    /* DateTime date = DateTime.Parse(dt);
                     return date;*/
                }
            }
            catch (Exception ex)
            {

                CommonUtility.WriteErrorLog(ex);
                return CommonUtility.GetCurrentDateTime().AddDays(5);
            }
            return CommonUtility.GetCurrentDateTime();
        }

        public static string convertdateyear(DateTime? dt)
        {
            if (dt != null)
            {
                DateTime dt1 = Convert.ToDateTime(dt);
                string month = dt1.Month.ToString();
                string year = dt1.Year.ToString();
                string final = month + "/" + year;
                return final;
            }
            else
            {
                return "";
            }

        }

        /* public static StatusMessage ResponseMessage(string status, string description, string otherDetails = "N/A", string code = "N/A", string message = "N/A")
         {
             //string.Format("'status':'{0}','description':'{1}','OtherDetails':'{2}','statusCode':'{3}'", status, description, otherDetails,code);
             var stat = new StatusMessage();
             stat.Message = message;
             stat.Description = description;
             stat.Status = status;
             return stat;

         }*/


        /*public void Dispose(ref TrackMeEntities db)
        {
            try
            {
                db.Dispose();
            }
            catch (Exception)
            {

                throw;
            }

        }*/



        #region Variable
        private int _roleId;
        private string _roleName;
        private bool _isActive;
        private bool _isDelete;
        private int _optionId;

        private string _connectionString;
        private DataSet _returnDataSet;
        private string _optionName;
        private string _optionValue;

        #endregion

        #region Property
        public bool IsDelete
        {
            get { return _isDelete; }
            set { _isDelete = value; }
        }
        public int OptionId
        {
            get { return _optionId; }
            set { _optionId = value; }
        }
        public string ConnectionString
        {
            get { return _connectionString; }
            set { _connectionString = value; }
        }
        public DataSet ReturnDataSet
        {
            get { return _returnDataSet; }
            set { _returnDataSet = value; }
        }

        public int RoleId
        {
            get { return _roleId; }
            set { _roleId = value; }
        }

        public string OptionName
        {
            get { return _optionName; }
            set { _optionName = value; }
        }

        public string OptionValue
        {
            get { return _optionValue; }
            set { _optionValue = value; }
        }


        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }
        #endregion

        #region Time related
        public static DateTime GetCurrentDateTime()
        {
            return DateTime.Now;
        }
        public static string TimeFormat(int seconds)
        {
            TimeSpan ts = new TimeSpan(0, 0, 0, seconds, 0);
            int Hours = (int)ts.TotalHours;
            return string.Format("{0:00}:{1:00}:{2:00}", Hours, ts.Minutes, ts.Seconds);
            //return string.Format("{0}:{1}:{2}", Hours, ts.Minutes, ts.Seconds);
        }
        #endregion


        #region Exceptions

        public static void WindowServiceLog(string msg)
        {
            /*StreamWriter sw = null;
            try
            {
                string writelogtest = PowerUtility.GetOptionValueByName("writelogtest");
                if (string.IsNullOrEmpty(writelogtest)) return;
                if (writelogtest != "true") return;
                //c:/inetpub/wwwroot/SNCRM/Log/
                var errorlogPath = PowerUtility.GetOptionValueByName("ErrorlogPath");
                sw = new StreamWriter(errorlogPath + "WindowServiceLog" + DateTime.Now.ToString("dd_MMM_yyyy") + "_.txt", true);
                sw.WriteLine("--------------------------------------Start-----------------------------------------");
                sw.WriteLine("-----------------------------" + DateTime.Now.ToString() +
                             "----------------------------");
                sw.WriteLine("CMS Robot :  " + msg);
                sw.WriteLine("");
                sw.Flush();
                sw.Close();
            }
            catch (Exception)
            {


            }*/



        }

        public static void WriteErrorLog(Exception ex, string msg)
        {
            /*StreamWriter sw = null;
            try
            {
                string writelogtest = PowerUtility.GetOptionValueByName("writelogtest");
                if (string.IsNullOrEmpty(writelogtest)) return;
                if (writelogtest != "true") return;
                //c:/inetpub/wwwroot/SNCRM/Log/
                var errorlogPath = PowerUtility.GetOptionValueByName("ErrorlogPath");
                sw = new StreamWriter(errorlogPath + DateTime.Now.ToString("dd_MMM_yyyy") + "_.txt", true);
                sw.WriteLine("--------------------------------------Start-----------------------------------------");
                sw.WriteLine("-----------------------------" + DateTime.Now.ToString() +
                             "----------------------------");
                sw.WriteLine("Stacktrace : " + ex.StackTrace);
                sw.WriteLine("Source     : " + ex.Source);
                sw.WriteLine("Message    : " + ex.Message);
                sw.WriteLine("Data       : " + ex.Data);
                sw.WriteLine("Stacktrace : " + msg);
                sw.WriteLine("");
                sw.Flush();
                sw.Close();
            }
            catch (Exception)
            {


            }*/
        }

        public static void WriteErrorLog(Exception ex)
        {
            StreamWriter sw = null;
            try
            {
                string writelogtest = GetOptionValueByName("writelogtest", "true");
                if (string.IsNullOrEmpty(writelogtest)) return;
                if (writelogtest.ToLower().Trim() != "true") return;
                //c:/inetpub/wwwroot/SNCRM/Log/
                var errorlogPath = GetOptionValueByName("ErrorlogPath", "d:/");
                sw = new StreamWriter(errorlogPath + DateTime.Now.ToString("dd_MMM_yyyy") + "_.txt", true);
                sw.WriteLine("--------------------------------------Start-----------------------------------------");
                sw.WriteLine("-----------------------------" + DateTime.Now.ToString() +
                             "----------------------------");
                sw.WriteLine("Stacktrace : " + ex.StackTrace);
                sw.WriteLine("Source     : " + ex.Source);
                sw.WriteLine("Message    : " + ex.Message);
                sw.WriteLine("Data       : " + ex.Data);
                sw.WriteLine("Stacktrace : " + ex.StackTrace);
                sw.WriteLine("");
                sw.Flush();
                sw.Close();
            }
            catch (Exception)
            {


            }



        }
        public static void WriteLog(string functionName)
        {
            StreamWriter sw = null;
            string writelogtest = GetOptionValueByName("writelogtest", "true");
            if (string.IsNullOrEmpty(writelogtest)) return;
            if (writelogtest.ToLower().Trim() != "true") return;
            //c:/inetpub/wwwroot/SNCRM/Log/
            var errorlogPath = GetOptionValueByName("ErrorlogPath", "d:/");
            sw = new StreamWriter(errorlogPath + DateTime.Now.ToString("dd_MMM_yyyy") + "_.txt", true);
            sw.WriteLine("Msg : " + functionName);


            sw.Flush();
            sw.Close();
        }

        public static DateTime? ConvertStringToDateTimeReturnNullIfError(string dt)
        {
            try
            {
                IFormatProvider culture = new System.Globalization.CultureInfo("en-GB", true);

                if (string.IsNullOrWhiteSpace(dt))
                {
                    dt = null;
                }
                else
                {
                    DateTime date = DateTime.Parse(dt, culture, System.Globalization.DateTimeStyles.AssumeLocal);
                    return date;
                }
            }
            catch (Exception ex)
            {

                CommonUtility.WriteErrorLog(ex);
                return null;
            }
            return CommonUtility.GetCurrentDateTime();
        }
        public static DateTime ConvertStringToDateTime(string dt)
        {
            try
            {
                IFormatProvider culture = new System.Globalization.CultureInfo("en-GB", true);

                if (string.IsNullOrWhiteSpace(dt))
                {
                    dt = null;
                }
                else
                {
                    DateTime date = DateTime.Parse(dt, culture, System.Globalization.DateTimeStyles.AssumeLocal);
                    return date;
                }
            }
            catch (Exception ex)
            {
                try
                {
                    return DateTime.Parse(dt);
                }
                catch (Exception)
                {

                    CommonUtility.WriteErrorLog(ex); return CommonUtility.GetCurrentDateTime();
                }

                CommonUtility.WriteErrorLog(ex);
                return CommonUtility.GetCurrentDateTime();
            }
            return CommonUtility.GetCurrentDateTime();
        }

        #endregion
        #region encrtypt decrept
        public static string encrypt(string str)
        {
            if (string.IsNullOrWhiteSpace(str))
            {
                return string.Empty;
            }
            string _result = string.Empty;
            char[] temp = str.ToCharArray();
            foreach (var _singleChar in temp)
            {
                var i = (int)_singleChar;
                i = i - 2;
                _result += (char)i;
            }
            return _result;
        }

        public static string decrypt(string str)
        {
            if (string.IsNullOrWhiteSpace(str))
            {
                return string.Empty;
            }
            string _result = string.Empty;
            char[] temp = str.ToCharArray();
            foreach (var _singleChar in temp)
            {
                var i = (int)_singleChar;
                i = i + 2;
                _result += (char)i;
            }
            return _result;
        }
        #endregion
        #region OptionUtil -psd |  GetOptionValueByName
        #region pswd
        public static string Generate_Random_password()
        {
            string allowedChars = "";
            allowedChars = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,";
            allowedChars += "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,";
            allowedChars += "1,2,3,4,5,6,7,8,9,0,!,@,#,$,%,&,?";

            char[] sep = { ',' };

            string[] arr = allowedChars.Split(sep);

            string passwordString = "";

            string temp = "";

            Random rand = new Random();

            for (int i = 0; i <= 8; i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                passwordString += temp;
            }
            return passwordString;
        }

        #endregion

        public static string GetOptionValueByName(string optionName, string defaultValue)
        {
            //var db = new ezyfreightEntities();
            //try
            //{

            //    var result = (from o in db.OptionSwitchers where o.keyName.ToLower() == optionName.ToLower() select o).FirstOrDefault();
            //    if (result != null)
            //    {
            //        return result.value;
            //    }
            //}
            //catch (Exception ex)
            //{


            //    return defaultValue;//|Saurin 2015 03 19|
            //}
            return defaultValue;

        }




        #endregion
        #region   ConnString

        #endregion

        // Saurin : 20131224 : make tinyURL :  
        //Term of use: https://tinyurl.com/#termsTinyURL >> was created as a free service to make posting long URLs easier,This service is provided without warranty of any kind.

        #region
        //Brijesh | 20150616 | GoogleMap

        public static void GetLatlongFromAddress(out double lat, out double longi, string myaddress)
        {
            var latitude = 0.0;
            var longitude = 0.0;
            try
            {
                var address = myaddress;
                /*var locationService = new GoogleLocationService();
                var point = locationService.GetLatLongFromAddress(address);
                latitude = point.Latitude;
                longitude = point.Longitude;*/
            }
            catch (Exception)
            {

                lat = -32.1617;
                longi = 147.0178;
            }


            lat = latitude;
            longi = longitude;
        }

        #endregion

        public static string ToTinyUrl(string txt)
        {
            Regex regx = new Regex("https://([\\w+?\\.\\w+])+([a-zA-Z0-9\\~\\!\\@\\#\\$\\%\\^\\&amp;\\*\\(\\)_\\-\\=\\+\\\\\\/\\?\\.\\:\\;\\'\\,]*)?", RegexOptions.IgnoreCase);
            try
            {
                MatchCollection mactches = regx.Matches(txt);
                foreach (Match match in mactches)
                {
                    string tURL = MakeTinyUrl(match.Value);
                    txt = txt.Replace(match.Value, tURL);
                }
            }
            catch (Exception ex)
            {

                WriteErrorLog(ex);
            }
            return txt;
        }
        public static string MakeTinyUrl(string url)
        {

            try
            {
                if (url.Length <= 12)
                {
                    return url;
                }
                if (!url.ToLower().StartsWith("http") && !url.ToLower().StartsWith("ftp"))
                {
                    url = "https://" + url;
                }
                var request = WebRequest.Create("https://tinyurl.com/api-create.php?url=" + url);
                var res = request.GetResponse();
                string text;
                using (var reader = new StreamReader(res.GetResponseStream()))
                {
                    text = reader.ReadToEnd();
                }
                return text;
            }
            catch (Exception)
            {
                return url;
            }
        }

        //Saurin: 201301028 
        public static string GetMobileIPAddress()
        {
            try
            {
                string ip = string.Empty;
                try
                {
                    if (HttpContext.Current.Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        ip = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                }
                catch { }
                if (string.IsNullOrEmpty(ip))
                {
                    ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                }
                try
                {
                    if (string.IsNullOrEmpty(ip))
                    {
                        ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_CLIENT_IP"];
                    }
                }
                catch (Exception)
                {


                }

                if (string.IsNullOrEmpty(ip))
                {
                    ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                }
                //Get your local DNS Host
                if (string.IsNullOrEmpty(ip))
                {
                    ip = HttpContext.Current.Request.UserHostAddress; ;
                }



                ip = GetLastIPAddress(ip);
                return ip;

            }
            catch (Exception ex)
            {
                return "0.0.0.0";
            }


        }
        public static string GetLastIPAddress(string ipAddress)
        {
            try
            {
                var myLastIp = "";
                var mulitipleIp = ipAddress.Split(',');
                foreach (var s in mulitipleIp)
                {
                    if (!string.IsNullOrEmpty(s))
                        myLastIp = s.Trim();
                }
                if (string.IsNullOrEmpty(myLastIp))
                    myLastIp = ipAddress;
                return myLastIp;
            }
            catch (Exception)
            {
                return ipAddress;
            }
        }
        public static string GetIPAddress()
        {
            try
            {
                string ip = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (string.IsNullOrEmpty(ip))
                {
                    ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                }
                if (ip == "::1") ip = GetMobileIPAddress();
                ip = GetLastIPAddress(ip);
                return ip;


            }
            catch (Exception)
            {
                return "0.0.0.0";
            }


        }


        #region Dataset Manipulations
        public static DataTable RemoveColumnFromDataset(ref DataTable dataTable, string importcontacrtsid)
        {
            try
            {
                dataTable.Columns.Remove(importcontacrtsid);
                return dataTable;
            }
            catch (Exception ex)
            {

                // PowerUtility.WriteErrorLog(ex);
                return dataTable;
            }
            return dataTable;

        }
        public static DataTable RenameColumnFromDataset(ref DataTable dataTable, string oldColumnName, string newColumnName)
        {
            try
            {
                dataTable.Columns[oldColumnName].ColumnName = newColumnName;
                return dataTable;
            }
            catch (Exception ex)
            {

                // PowerUtility.WriteErrorLog(ex);
                return dataTable;
            }


        }

        #endregion

        public static bool IsContainSpecialCharacter(string checkString)
        {
            //var isTrue = Regex.IsMatch(checkString.Trim(), "^[a-zA-Z]");

            var expression = "^[a-zA-Z_. ]*$";

            var regexItem = new Regex(expression, RegexOptions.IgnoreCase);

            if (regexItem.IsMatch(checkString))
            {
                return false;
            }
            return true;
        }
        //saurin |20150402|
        public static string GetAssemblyInfo()
        {
            try
            {

                //Version version = Assembly.GetExecutingAssembly().GetName().Version;
                Version version = Assembly.GetCallingAssembly().GetName().Version;

                //return string.Format("V {0}", version.ToString());
                return string.Format("Ver :  1.0.0.1");
            }
            catch (Exception e)
            {
                return string.Empty;
            }

        }

        public static bool IsContains(string source, string toCheck, StringComparison comp)
        {
            try
            {
                return source.IndexOf(toCheck, comp) >= 0;
            }
            catch (Exception ex)
            {

                //ExceptionLogger.LogException(ex);
                return false;

            }

        }
        //saurin : 20140307 :  check Is value  convertable in other datatype 
        public static Boolean CanConvert<T>(string value)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    return false;
                }

                //Type type = Type.GetType(testType);
                TypeConverter converter = TypeDescriptor.GetConverter(typeof(T));
                return converter.IsValid(value);
            }

            catch (Exception)
            {

                return false;
            }
        }
        public static int ConvertToNumber(string p, int defaultValue = -1)
        {
            if (string.IsNullOrWhiteSpace(p)) return defaultValue;
            try
            {
                return int.Parse(p);
            }
            catch (Exception)
            {

                return defaultValue;
            }
        }
        public static bool ConvertToBoolean(string p, bool defaultValue)
        {
            if (string.IsNullOrWhiteSpace(p)) return defaultValue;
            try
            {
                if (p == "1") return true;
                if (p == "0") return false;
                return bool.Parse(p);
            }
            catch (Exception)
            {

                return defaultValue;
            }
        }

        public static bool IsMacOS(string userAgent)
        {
            var osInfo = userAgent.Split(new Char[] { '(', ')' })[1];
            return osInfo.Contains("Mac_PowerPC") || osInfo.Contains("Macintosh") || osInfo.Contains("Mac OS");
        }

        public static string GetOSInfo(HttpRequest httpRequest)
        {
            try
            {


                string OSName = string.Empty;
                OSName = httpRequest.Browser.Platform;
                if (httpRequest.UserAgent != null)
                {
                    string userAgent = httpRequest.UserAgent;
                    if (httpRequest.Browser.Platform.Contains("WinNT"))
                    {
                        if (userAgent.Contains("Windows NT 6.0"))
                            OSName = "Vista";
                        else if (userAgent.Contains("Windows NT 6.1"))
                            OSName = "Windows 7";
                        else if (userAgent.Contains("Windows NT 6.1"))
                            OSName = "Windows 7";
                        else if (userAgent.Contains("Windows NT 6.2"))
                            OSName = "Windows 8";
                        else if (userAgent.Contains("Windows NT 6.3"))
                            OSName = "Windows 8.1";
                        else if (userAgent.Contains("Windows NT 6.4"))
                            OSName = "Windows 8.2";

                        else if (userAgent.Contains("Windows NT 5.2"))
                            OSName = "Windows Server 2003";
                        else if (userAgent.Contains("Windows NT 5.1"))
                            OSName = "Windows XP";
                        else if (userAgent.Contains("Windows NT 5.0"))
                            OSName = "Windows Server 2000";

                        else
                        {
                            OSName = httpRequest.Browser.Platform;
                        }
                    }
                    else
                    {
                        if (IsMacOS(httpRequest.UserAgent))
                        {
                            OSName = "Macintosh";
                        }
                        if (userAgent.ToLower().Contains("iphone;"))
                        {
                            OSName = "iPhone";
                        }
                        if (userAgent.ToLower().Contains("iPad;"))
                        {
                            OSName = "iPad";
                        }
                    }
                }
                return OSName;
            }
            catch (Exception)
            {

                return string.Empty;
            }
        }
        //saurin : 20132910 : 
        public static string GetServerOSInfo()
        {
            try
            {


                //Get Operating system information.
                OperatingSystem os = Environment.OSVersion;
                //Get version information about the os.
                Version vs = os.Version;

                //Variable to hold our return value
                if (os.ToString().ToLower().Contains("win"))
                {
                    string operatingSystem = "";

                    if (os.Platform == PlatformID.Win32Windows)
                    {
                        //This is a pre-NT version of Windows
                        switch (vs.Minor)
                        {
                            case 0:
                                operatingSystem = "95";
                                break;
                            case 10:
                                if (vs.Revision.ToString() == "2222A")
                                    operatingSystem = "98SE";
                                else
                                    operatingSystem = "98";
                                break;
                            case 90:
                                operatingSystem = "Me";
                                break;
                            default:
                                break;
                        }
                    }
                    else if (os.Platform == PlatformID.Win32NT)
                    {
                        switch (vs.Major)
                        {
                            case 3:
                                operatingSystem = "NT 3.51";
                                break;
                            case 4:
                                operatingSystem = "NT 4.0";
                                break;
                            case 5:
                                if (vs.Minor == 0)
                                    operatingSystem = "2000";
                                else if (vs.Minor == 1)
                                    operatingSystem = "XP";
                                else if (vs.Minor == 2)
                                    operatingSystem = "XP 64-Bit edition";
                                break;
                            case 6:
                                if (vs.Minor == 0)
                                    operatingSystem = "Vista";
                                else if (vs.Minor == 1)
                                    operatingSystem = "7";
                                else if (vs.Minor == 2)
                                    operatingSystem = "8";
                                else if (vs.Minor == 3)
                                    operatingSystem = "8.1";
                                break;
                            default:
                                break;
                        }
                    }
                    //Make sure we actually got something in our OS check
                    //We don't want to just return " Service Pack 2" or " 32-bit"
                    //That information is useless without the OS version.
                    if (operatingSystem != "")
                    {
                        //Got something.  Let's prepend "Windows" and get more info.
                        operatingSystem = "Windows " + operatingSystem;
                        //See if there's a service pack installed.
                        if (os.ServicePack != "")
                        {
                            //Append it to the OS name.  i.e. "Windows XP Service Pack 3"
                            operatingSystem += " " + os.ServicePack;
                        }
                        //Append the OS architecture.  i.e. "Windows XP Service Pack 3 32-bit"
                        operatingSystem += " " + (Environment.Is64BitOperatingSystem ? 64 : 32).ToString() + "-bit";
                    }
                    //Return the information we've gathered.
                    return operatingSystem;
                }
                return os.ToString();
            }
            catch (Exception)
            {


            }
            return string.Empty;
        }
        /*saurin : 20140101 : for reset password , to check user is on mobile/tab or computer */
        public static bool IsMobileBrowser()
        {
            try
            {



                //GETS THE CURRENT USER CONTEXT
                HttpContext context = HttpContext.Current;

                //FIRST TRY BUILT IN ASP.NT CHECK
                if (context.Request.Browser.IsMobileDevice)
                {
                    return true;
                }
                //THEN TRY CHECKING FOR THE HTTP_X_WAP_PROFILE HEADER
                if (context.Request.ServerVariables["HTTP_X_WAP_PROFILE"] != null)
                {
                    return true;
                }
                //THEN TRY CHECKING THAT HTTP_ACCEPT EXISTS AND CONTAINS WAP
                if (context.Request.ServerVariables["HTTP_ACCEPT"] != null &&
                    context.Request.ServerVariables["HTTP_ACCEPT"].ToLower().Contains("wap"))
                {
                    return true;
                }

                //saurin : 

                //AND FINALLY CHECK THE HTTP_USER_AGENT 
                //HEADER VARIABLE FOR ANY ONE OF THE FOLLOWING
                if (context.Request.ServerVariables["HTTP_USER_AGENT"] != null)
                {
                    //Create a list of all mobile types
                    var mobiles =
                        new[]
                            {
                                "midp", "j2me", "avant", "docomo", 
                                "novarra", "palmos", "palmsource", 
                                "240x320", "opwv", "chtml",
                                "pda", "windows ce", "mmp/", 
                                "blackberry", "mib/", "symbian", 
                                "wireless", "nokia", "hand", "mobi",
                                "phone", "cdm", "up.b", "audio", 
                                "SIE-", "SEC-", "samsung", "HTC", 
                                "mot-", "mitsu", "sagem", "sony"
                                , "alcatel", "lg", "eric", "vx", 
                                "NEC", "philips", "mmm", "xx", 
                                "panasonic", "sharp", "wap", "sch",
                                "rover", "pocket", "benq", "java", 
                                "pt", "pg", "vox", "amoi", 
                                "bird", "compal", "kg", "voda","vodafone",
                                "sany", "kdd", "dbt", "sendo", 
                                "sgh", "gradi", "jb", "dddi", 
                                "moto", "iphone","micromax","fly","lava","android","intex","idea",
                                "china","t-mobile","hcl","benq","spice","airtel","xolo","swipe","asha","ipod","ipad",
                                "Karbonn","Max","sensui","salora","blackberry","dell","iris","iphone","huwai"
                            };

                    //Loop through each item in the list created above 
                    //and check if the header contains that text
                    foreach (var mobile in mobiles)
                    {
                        if (context.Request.ServerVariables["HTTP_USER_AGENT"].
                            ToLower().Contains(mobile.ToLower()))
                        {
                            return true;
                        }
                    }
                }

                return false;
            }
            catch (Exception ex)
            {
                // ExceptionLogger.LogException(ex);
                return false;
            }
        }



        public static bool IsValidEmail(string emailAddress)
        {

            emailAddress = Regex.Replace(emailAddress, @"\s+", "");
            emailAddress = emailAddress.Trim();

            bool isEmail = Regex.IsMatch(emailAddress,
                                         @"\A(?:[a-z0-9!#$%&'*+/=?^_`{</br>}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{</br>}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z",
                                         RegexOptions.IgnoreCase);

            return isEmail;
        }

        public static string ConvertToDatetime(DateTime locCreatedDate, string formate = "yyyy-MM-dd hh:mm:ss")
        {
            try
            {
                return locCreatedDate.ToString(formate);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static decimal ConvertToDecimal(string decimalValue)
        {
            try
            {
                return Convert.ToDecimal(decimalValue);
            }
            catch (Exception)
            {

                return 00;
            }
        }

        public static double Distance(double lat1, double lon1, double lat2, double lon2, char unit)
        {

            //double theta = lon1 - lon2;

            //double dist = Math.Sin(deg2rad(lat1)) * Math.Sin(deg2rad(lat2)) + Math.Cos(deg2rad(lat1)) * Math.Cos(deg2rad(lat2)) * Math.Cos(deg2rad(theta));
            //dist = Math.Acos(dist);
            //dist = rad2deg(dist);
            //dist = dist * 60 * 1.1515;
            //if (unit == 'K')
            //{
            //    dist = dist * 1.609344;
            //}
            //else if (unit == 'N')
            //{
            //    dist = dist * 0.8684;
            //}
            //return (dist);

            var radlat1 = Math.PI * lat1 / 180;
            var radlat2 = Math.PI * lat2 / 180;
            var radlon1 = Math.PI * lon1 / 180;
            var radlon2 = Math.PI * lon2 / 180;
            var theta = lon1 - lon2;
            var radtheta = Math.PI * theta / 180;
            var dist = Math.Sin(radlat1) * Math.Sin(radlat2) + Math.Cos(radlat1) * Math.Cos(radlat2) * Math.Cos(radtheta);
            Int32 id = Convert.ToInt32(dist);
            dist = Math.Acos(id);
            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;
            if (unit == 'K') { dist = dist * 1.609344; }
            if (unit == 'N') { dist = dist * 0.8684; }
            return dist;

        }


        private static double deg2rad(double deg)
        {
            return (deg * Math.PI / 180.0);
        }

        private static double rad2deg(double rad)
        {
            return (rad / Math.PI * 180.0);
        }



    }
}

