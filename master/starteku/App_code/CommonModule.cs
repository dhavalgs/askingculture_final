﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

using System.IO;
using System.Data;
using System.Net.Mail;
using System.Configuration;
using System.Web;
using Newtonsoft.Json;
using starteku_BusinessLogic;
using startetku.Business.Logic;
using Formatting = System.Xml.Formatting;


/// <summary>
/// Summary description for CommonModule
/// </summary>
public class CommonModule
{
    public CommonModule()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    #region User Define Function


    public static string FormatdtEnter(string date)
    {
        string dt = string.Empty;

        string[] str = date.Split('/');
        dt = str[1] + '/' + str[0] + '/' + str[2];
        return dt;
    }
    public static string Generate_Random_password()
    {
        string allowedChars = "";
        allowedChars = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,";
        allowedChars += "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,";
        allowedChars += "1,2,3,4,5,6,7,8,9,0,!,@,#,$,%,&,?";

        char[] sep = { ',' };

        string[] arr = allowedChars.Split(sep);

        string passwordString = "";

        string temp = "";

        Random rand = new Random();

        for (int i = 0; i <= 8; i++)
        {
            temp = arr[rand.Next(0, arr.Length)];
            passwordString += temp;
        }
        return passwordString;
    }
    //public static Boolean SendMailToUser(string toid, string subject, string body, string fromid)
    //{
    //    Boolean result = false;
    //    MailMessage mail = new MailMessage();
    //    mail.From = new MailAddress(fromid, Convert.ToString(ConfigurationManager.AppSettings["maildisplayname"]));
    //    mail.Sender = new MailAddress(fromid);
    //    mail.To.Add(toid);
    //    mail.IsBodyHtml = true;
    //    mail.Subject = subject;
    //    mail.Body = body;

    //    SmtpClient smtp = new SmtpClient(ConfigurationManager.AppSettings["host"].ToString(), Convert.ToInt16(ConfigurationManager.AppSettings["port"].ToString()));
    //    smtp = CheckMailCondition(smtp);

    //    //smtp.Port = 587;

    //    System.Net.NetworkCredential basicAuthenticationInfo = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["NetCreUsername"].ToString(), ConfigurationManager.AppSettings["NetCrePassword"].ToString());
    //    smtp.UseDefaultCredentials = false;
    //    smtp.Credentials = basicAuthenticationInfo;

    //    try
    //    {
    //        smtp.Send(mail);
    //        result = true;
    //    }
    //    catch (Exception ex)
    //    {
    //        WriteLog("error in SendMailToUser" + "error message " + ex.Message + "error stack trace " + ex.StackTrace + " " + ex.Source + " inner exception" + Convert.ToString(ex.InnerException));
    //    }
    //    return result;
    //}

    public static void TestMailSend()
    {
        var fromAddress = new MailAddress("skent.qc@gmail.com", "From Name");
        var toAddress = new MailAddress("test11@gmail.com", "To Name");
        const string fromPassword = "fipl3178";
        const string subject = "Subject";
        const string body = "Body";

        var smtp = new SmtpClient
        {
            Host = "smtp.gmail.com",
            Port = 587,
            EnableSsl = true,
            DeliveryMethod = SmtpDeliveryMethod.Network,
            UseDefaultCredentials = false,
            Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
        };
        using (var message = new MailMessage(fromAddress, toAddress)
        {
            Subject = subject,
            Body = body
        })
        {
            smtp.Send(message);
        }
    }

    public static Boolean SendMailToUser(string toid, string subject, string body, string fromid, String CompanyId)
    {
       
       // TestMailSend();
       // toid = "skent.qc@gmail.com";
        WriteLog("NEW STATIC SendMailToUser");
        Boolean result = false;
        EmailSettingBM getmail = new EmailSettingBM();
        getmail.EmailIsActive = true;
        getmail.EmailIsDeleted = false;
        getmail.EmailCompanyId = Convert.ToInt32(CompanyId);

        WriteLog("getmail.EmailCompanyId : " + CompanyId);

        getmail.GetAllEmailSetting();
        DataSet dsmail = getmail.ds;
        if (dsmail != null)
        {
            if (dsmail.Tables[0].Rows.Count > 0)
            {
               
                try
                {
                    MailMessage mail = new MailMessage();

                    String pass = CommonModule.decrypt(Convert.ToString(dsmail.Tables[0].Rows[0]["password"]));
                    //String pass = "fipl3178";
                    //WriteLog("pass:" + pass);
                    
                    mail.From = new MailAddress(Convert.ToString(dsmail.Tables[0].Rows[0]["EmailSender"]), Convert.ToString(dsmail.Tables[0].Rows[0]["EmailDisplayName"]));
                    //mail.From =new MailAddress("skent.qc@gmail.com");
                    //WriteLog("mail.From:" + mail.From);

                    //for testing
                    //mail.Bcc.Add("skent.qc@gmail.com");

                    mail.Sender = new MailAddress(Convert.ToString(dsmail.Tables[0].Rows[0]["EmailSender"]));
                    //mail.Sender = new MailAddress("skent.qc@gmail.com");
                  //  WriteLog("mail.Sender:" + mail.Sender);
                   
                    mail.To.Add(toid);
                  //  WriteLog("toid:" + toid);

                    mail.IsBodyHtml = true;
                    
                    mail.Subject = subject;
                    mail.Body = body;
                   
                    if (!String.IsNullOrEmpty(Convert.ToString(dsmail.Tables[0].Rows[0]["EmailPort"])))
                    {
                        SmtpClient smtp = new SmtpClient(Convert.ToString(dsmail.Tables[0].Rows[0]["EmailHost"]), Convert.ToInt16(Convert.ToString(dsmail.Tables[0].Rows[0]["EmailPort"])));
                        //SmtpClient smtp = new SmtpClient("smtp.gmail.com", 25);
                        
                       // WriteLog("smtp:" + smtp);
                        
                        smtp.EnableSsl = true;
                        //System.Net.NetworkCredential basicAuthenticationInfo = new System.Net.NetworkCredential("applarmpro@gmail.com", "applarm@123");
                        System.Net.NetworkCredential basicAuthenticationInfo = new System.Net.NetworkCredential(Convert.ToString(dsmail.Tables[0].Rows[0]["EmailSender"]), pass);
                        //System.Net.NetworkCredential basicAuthenticationInfo = new System.Net.NetworkCredential("skent.qc@gmail.com", "fipl3178");
                        smtp.UseDefaultCredentials = false;
                        smtp.Credentials = basicAuthenticationInfo;
                        
                        smtp.Send(mail);

                      //  WriteLog("smtp (send):" + smtp);
                        result = true;
                    }
                    else
                    {
                        SmtpClient smtp = new SmtpClient(Convert.ToString(dsmail.Tables[0].Rows[0]["EmailHost"]));
                        //SmtpClient smtp = new SmtpClient("smtp.gmail.com", 25);

                       // WriteLog("smtp:" + smtp);
                        smtp.EnableSsl = true;

                        // System.Net.NetworkCredential basicAuthenticationInfo = new System.Net.NetworkCredential("applarmpro@gmail.com", "applarm@123");
                        System.Net.NetworkCredential basicAuthenticationInfo = new System.Net.NetworkCredential(Convert.ToString(dsmail.Tables[0].Rows[0]["EmailSender"]), pass);
                        //System.Net.NetworkCredential basicAuthenticationInfo = new System.Net.NetworkCredential("skent.qc@gmail.com", "fipl3178");

                        smtp.UseDefaultCredentials = false;
                        smtp.Credentials = basicAuthenticationInfo;
                        smtp.Send(mail);
                      //  WriteLog("Mail Send successfully");
                        result = true;
                    }
                   // SmtpClient smtp = new SmtpClient(ConfigurationManager.AppSettings["host"].ToString(), Convert.ToInt16(ConfigurationManager.AppSettings["port"].ToString()));
                  
                }
                catch (Exception ex)
                {
                    WriteLog("Catch : " + "error   smtp.Send(mail); " + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                    WriteLog("error1" + ex.ToString());
                    // Response.Write(ex.Message);
                }
                return result;

            }
        }

        return result;
    }
    //public static string getTemplatebyname(string cmsName,Int32 userid)
    //{
    //    try
    //    {
    //        string dt = string.Empty;
    //        string confirmMail = "";
    //        CmsBM objMail = new CmsBM();
    //        objMail.cmsName = cmsName;
    //        objMail.SelectMailTemplateByName();
    //        DataSet dsMail = objMail.ds;
    //        if (dsMail.Tables[0].Rows.Count > 0)
    //        {
                
    //            confirmMail = dsMail.Tables[0].Rows[0]["cmsDescription"].ToString();
    //        }
    //        return confirmMail;
    //    }
    //    catch (Exception ex)
    //    {
    //        Common.WriteLog("error in getTemplatebyname" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
    //        return "";
    //    }

    //}

    public static string getTemplatebyname(string cmsName, Int32 userid)
    {
        try
        {
            string dt = string.Empty;
            string confirmMail = "";
            CmsBM objMail = new CmsBM();
            objMail.cmsName = cmsName;
            objMail.SelectMailTemplateByName();
            DataSet dsMail = objMail.ds;
            if (dsMail.Tables[0].Rows.Count > 0)
            {
                if (userid != 0 && userid != null)
                {
                    UserBM obj = new UserBM();
                    obj.GetLanguageById(userid);
                    DataSet ds = obj.ds;
                    if (ds != null)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            string languageName = Convert.ToString(ds.Tables[0].Rows[0]["languageName"]);

                            if (languageName == "Danish")
                            {
                                confirmMail = dsMail.Tables[0].Rows[0]["cmsDescriptionDN"].ToString();
                            }

                            else
                            {
                                confirmMail = dsMail.Tables[0].Rows[0]["cmsDescription"].ToString();
                            }
                        }

                        else
                        {
                            if (Convert.ToString(HttpContext.Current.Session["Culture"]) == "Danish")
                            {
                                dsMail.Tables[0].Columns["cmsDescription"].ColumnName = "abcd";
                                dsMail.Tables[0].Columns["cmsDescriptionDN"].ColumnName = "cmsDescription";

                            }
                            confirmMail = dsMail.Tables[0].Rows[0]["cmsDescription"].ToString();
                        }
                    }
                    else
                    {

                        if (Convert.ToString(HttpContext.Current.Session["Culture"]) == "Danish")
                        {
                            dsMail.Tables[0].Columns["cmsDescription"].ColumnName = "abcd";
                            dsMail.Tables[0].Columns["cmsDescriptionDN"].ColumnName = "cmsDescription";

                        }
                        confirmMail = dsMail.Tables[0].Rows[0]["cmsDescription"].ToString();
                    }
                    

                }



                else
                {
                    if (Convert.ToString(HttpContext.Current.Session["Culture"]) == "Danish")
                    {
                        dsMail.Tables[0].Columns["cmsDescription"].ColumnName = "abcd";
                        dsMail.Tables[0].Columns["cmsDescriptionDN"].ColumnName = "cmsDescription";

                    }
                    confirmMail = dsMail.Tables[0].Rows[0]["cmsDescription"].ToString();
                }
            }
            return confirmMail;
        }
        catch (Exception ex)
        {
            Common.WriteLog("error in getTemplatebyname" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            return "";
        }

    }
    public static Template getTemplatebyname1(string cmsName, Int32 userid)
    {
        try
        {

            Template template = new Template();
            template.Language = "English";
            string dt = string.Empty;
            string confirmMail = "";
            CmsBM objMail = new CmsBM();
            objMail.cmsName = cmsName;
            objMail.SelectMailTemplateByName();
            DataSet dsMail = objMail.ds;
            if (dsMail.Tables[0].Rows.Count > 0)
            {
                if (userid != 0 && userid != null)
                {
                    UserBM obj = new UserBM();
                    obj.GetLanguageById(userid);


                    DataSet ds = obj.ds;
                    if (ds != null)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            string languageName = Convert.ToString(ds.Tables[0].Rows[0]["languageName"]);

                            template.LanguageID = Convert.ToString(ds.Tables[0].Rows[0]["languageCulture"]);

                            if (languageName == "Danish")
                            {
                                confirmMail = dsMail.Tables[0].Rows[0]["cmsDescriptionDN"].ToString();
                                template.Language = "Danish";
                            }
                            else
                            {
                                confirmMail = dsMail.Tables[0].Rows[0]["cmsDescription"].ToString();
                            }
                        }
                        else
                        {
                            template.LanguageID = Convert.ToString("en-GB");
                            if (Convert.ToString(HttpContext.Current.Session["Culture"]) == "Danish")
                            {
                                dsMail.Tables[0].Columns["cmsDescription"].ColumnName = "abcd";
                                dsMail.Tables[0].Columns["cmsDescriptionDN"].ColumnName = "cmsDescription";
                                template.Language = "Danish";
                                template.LanguageID = Convert.ToString("da-DK");
                            }
                            confirmMail = dsMail.Tables[0].Rows[0]["cmsDescription"].ToString();
                        }
                    }
                    else
                    {
                        template.LanguageID = Convert.ToString("en-GB");
                        if (Convert.ToString(HttpContext.Current.Session["Culture"]) == "Danish")
                        {
                            template.LanguageID = Convert.ToString("da-DK");
                            dsMail.Tables[0].Columns["cmsDescription"].ColumnName = "abcd";
                            dsMail.Tables[0].Columns["cmsDescriptionDN"].ColumnName = "cmsDescription";
                            template.Language = "Danish";

                        }
                        confirmMail = dsMail.Tables[0].Rows[0]["cmsDescription"].ToString();
                    }
                }
                else
                {
                    template.LanguageID = Convert.ToString("en-GB");
                    if (Convert.ToString(HttpContext.Current.Session["Culture"]) == "Danish")
                    {
                        dsMail.Tables[0].Columns["cmsDescription"].ColumnName = "abcd";
                        dsMail.Tables[0].Columns["cmsDescriptionDN"].ColumnName = "cmsDescription";
                        template.Language = "Danish";
                        template.LanguageID = Convert.ToString("da-DK");
                    }
                    confirmMail = dsMail.Tables[0].Rows[0]["cmsDescription"].ToString();
                }
            }

            template.TemplateName = confirmMail;
            return template;
        }
        catch (Exception ex)
        {
            Common.WriteLog("error in getTemplatebyname" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            return null;
        }

    }

    public static void WriteLog(string functionName)
    {
        string writelogtest = Convert.ToString(ConfigurationSettings.AppSettings["writelogstatus"]).ToLower();
        //string writelogtest = "true";
        if (!string.IsNullOrEmpty(writelogtest))
        {
            if (writelogtest == "true")
            {
                string fileName = DateTime.Today.ToString("dd_MMM_yyyy");
                string path = ConfigurationSettings.AppSettings["logpath"].ToString() + fileName + ".txt";
                //string path = "c:/inetpub/wwwroot/starteku/Log/" + fileName + ".txt";
                //
                if (!System.IO.File.Exists(path))
                {
                    using (FileStream fs = File.Create(path))
                    {
                        Byte[] info = new UTF8Encoding(true).GetBytes("");
                        fs.Write(info, 0, 0);
                    }
                }
                TextWriter tw1 = new StreamWriter(path, true);
                tw1.WriteLine(functionName);
                tw1.Close();
                tw1.Dispose();
            }
        }
    }

    public static string encrypt(string str)
    {
        string _result = string.Empty;
        char[] temp = str.ToCharArray();
        foreach (var _singleChar in temp)
        {
            var i = (int)_singleChar;
            i = i - 2;
            _result += (char)i;
        }
        return _result;
    }

    public static string decrypt(string str)
    {
        string _result = string.Empty;
        char[] temp = str.ToCharArray();
        foreach (var _singleChar in temp)
        {
            var i = (int)_singleChar;
            i = i + 2;
            _result += (char)i;
        }
        return _result;
    }

    public static string welcomMsg(string userName)
    {
        return "Hi, " + userName;
    }

    public static SmtpClient CheckMailCondition(SmtpClient smtp)
    {
        WriteLog("CheckMailCondition");
        WriteLog("port"+ ConfigurationManager.AppSettings["port"]);
        if (Convert.ToString(ConfigurationManager.AppSettings["port"]) != "")
        {
           
            smtp = new SmtpClient(ConfigurationManager.AppSettings["host"].ToString(), Convert.ToInt32(ConfigurationManager.AppSettings["port"]));
            WriteLog("smtp" + smtp);
        }
        else
        {
            smtp = new SmtpClient(ConfigurationManager.AppSettings["host"].ToString());
        }
        if (Convert.ToString(ConfigurationManager.AppSettings["enablessl"]) == "true")
        {
            smtp.EnableSsl = true;
            WriteLog("smtp.EnableSsl" + smtp.EnableSsl);
        }
        //smtp.EnableSsl = true;
        return smtp;
    }
    public static bool IsDatasetvalid(DataSet ds)
    {
        if (ds != null)
        {
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    #endregion

    #region Enum
    public enum userType
    {
        startingZeroValue,
        admin
    }

    public enum DayName
    {
        Sunday,
        Monday,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday
    }

    public enum ManageType
    {
        Center,
        Room,
        Child
    }

    public enum CheckListStatus
    {
        Completed,
        Pending,
        Missed
    }

    #endregion

    #region common msg

    public static string msgSuccessfullyRegistered = "You are successfully registered.";
    public static string msgSessionExpire = "Your session expire.";
  
    public static string msgProblemInsertRecord = "Problem in insert record.";
    public static string msgRecordInsertedSuccessfully = "Record has been inserted successfully.";
   
   
    public static string msgRecordNotfound = "Record not found.";
    public static string msgFilePathNotfound = "File path not found.";
    public static string msgInvalidUserNameOrPassword = "Invalid username or password.";
    public static string msgPasswordSendToYourEmailAddress = "Password send to your email address.";
   
    public static string msgSomeProblemOccure = "Some problem occurred.";
    
    public static string msgAlreadyExists = "Already exists.";
    
    public static string selectdate = "Please select date";


  
    public static string selectDropdwon = "Please select List of Item";
    public static string msgRecordHasBeenDeletedSuccessfully = "Record has been deleted successfully.";
    public static string msgEmailAlreadyExists = "Email already exists.";
    public static string msgNameAlreadyExists = "Name already exists.";
    public static string msgstateNameAlreadyExists = "State Name already exists.";
   
    public static string msgUserNameAlreadyExists = "UserName already exists.";
    public static string msgMobileAlreadyExists = "Mobile number already exists.";
    public static string msgRecordUpdatedSuccss = "Record has been updated successfully.";
    public static string msgRecordDeletedSuccss = "Record has been deleted successfully.";
    public static string msgRecordArchiveSucces = "Record has been  archived successfully.";
    public static string msgRecordUNArchiveSucces = "Record has been restored successfully.";
    
  
    public static string msgYourFormSubmittedSuccessfully = "Your form has been submitted successfully.";
    
  
    public static string msgPasswordOldNotMatch = "Old password does not match.";
    public static string msgPasswordHasBeenUpdateSuccess = "Password has been updated successfully";
    public static string msgPleaseAddIpadNocenterroom = "Please enter IPad number,center name,room name";
    public static string msgRecordAlreadyExist = "Record already exist.";
   
   
   
    public static string CurrentCulture
    {
        get
        {
            return ConfigurationManager.AppSettings["currentculture"].ToString();
        }
    }


    #region Grid msg
    public static string msgGridRecordNotfound
    {
        get{
            return Convert.ToString( HttpContext.GetLocalResourceObject("~/Organisation/ActivityLists.aspx", "GridRecordNotfound.EmptyDataText"));//ConfigurationManager.AppSettings["currentculture"].ToString();
        }
    }//="Record not found.";
    #endregion

    #region dropDown msg
    public static string dropDownSelect = "Select";
    public static string dropDownSelectDepartments = "Select Department";
    public static string dropDownCategory = "Category";
    public static string dropDownSelectForumCategory = "Select Forum Category";
    public static string dropDownSelectJobtype = "Job Type";
    public static string dropDownSelectTeam = "Select Team";
    public static string dropDownSelectDivisions = "Select Division";

    public static string dropDownZeroValue = "0";
    public static string dropDownStateDefault = "Select";
    public static string dropDownOther = "Other";
    public static string dropDownNewOtherId = "50";
    public static string dropDownTitleForGender = "Title";
    public static string dropDownNewZealand = "New Zealand";
    public static string dropDownAustralia = "Australia";
    public static string dropDownNewZealandCountyiId = "156";
    public static string dropDownAustraliaCountyiId = "13";


    public static string dropDownMaxAgeGroupLimit = "7";
    public static string dropDownBlankLine = "---------------------";
    #endregion

    #region commong Id
    public static string countryId = "13";
    #endregion

    #endregion

    #region Email
    public static void sendmail(string email, string name, string pass)
    {
        try
        {
            string subject = "Registration";
            string fromid = Convert.ToString(ConfigurationManager.AppSettings["fromid"]);
            CmsBM objMail = new CmsBM();
            objMail.cmsName = "Registration";
            objMail.SelectMailTemplateByName();
            DataSet dsMail = objMail.ds;
            if (dsMail.Tables[0].Rows.Count > 0)
            {
                string confirmMail = dsMail.Tables[0].Rows[0]["cmsDescription"].ToString();
                string tempString = confirmMail;
                tempString = tempString.Replace("###name###", name);
                tempString = tempString.Replace("###email###", email);
                tempString = tempString.Replace("###password###", pass);
                CommonModule.SendMailToUser(email, subject, tempString, fromid, Convert.ToString(HttpContext.Current.Session["OrgCompanyId"]));
            }
        } 
        catch (Exception ex)
        {
            Common.WriteLog("error in sendmail" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
        }

    }
    //Converting dataset to json
    public static string ConverTableToJson(DataSet ds)
    { 
        return JsonConvert.SerializeObject(ds, (Newtonsoft.Json.Formatting) Formatting.Indented);
        return ";";
    }
    #endregion
}
public class clstempcomment
{
    public int Childid { get; set; }
    public string Comment { get; set; }
    public Boolean status { get; set; }


    public string value { get; set; }

}

public class Template
{
    public string Language { get; set; }
    public string LanguageID { get; set; }
    public string TemplateName { get; set; }
}


public partial class TeamUser
{
    public int TeamID;
    public List<UserIDs> UserIDs;
}

public partial class UserIDs
{
    public int userID { get; set; }
}