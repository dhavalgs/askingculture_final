﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using starteku_BusinessLogic;
using starteku_BusinessLogic.Model;

/// <summary>
/// Summary description for ActListsweb
/// </summary>
public class ActListsweb
{
	public ActListsweb()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static List<GetActivityListweb_Result> GetActivityListweb(GetActivityListweb_Result actObj, int resLandId = 1)
    {
        var db = new startetkuEntities1();
        try
        {
            //actObj.ActIsActive = true;
            actObj.comIsDeleted = false;

            if (actObj.ActStartDate < DateTime.Now.AddYears(-100))
            {
                actObj.ActStartDate = CommonUtilities.GetCurrentDateTime().AddYears(-100);
            }
            if (actObj.ActEndDate < DateTime.Now.AddYears(-100))
            {
                actObj.ActEndDate = CommonUtilities.GetCurrentDateTime().AddYears(100);
            }
            if (actObj.ActIsDeleted == null)
            {
                actObj.ActIsDeleted = false;
            }
            //1,0,0,null,null,0,0,0,0,11-May-17 9:32:12 AM,11-May-17 9:32:15 AM,0,0,null,null,true,false
            var getLists = db.GetActivityListweb(resLandId, actObj.ActId, actObj.ActCreatedBy, actObj.ActCompanyId, 0,0,0, actObj.ActCatRefId, null,
                              null,
                            actObj.ActDivId, actObj.ActJobId, actObj.ActCompId, actObj.ActName, actObj.ActIsActive, actObj.ActIsDeleted).ToList();
            return getLists;

        }
        catch (Exception ex)
        {

            ExceptionLogger.LogException(ex);
            return new List<GetActivityListweb_Result>();
        }
        finally
        {
            CommonAttributes.DisposeDBObject(ref db);
        }

    }
}