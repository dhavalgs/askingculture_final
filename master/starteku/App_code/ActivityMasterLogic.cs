﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using starteku_BusinessLogic;
using starteku_BusinessLogic.Model;

/// <summary>
/// Summary description for ActivityMasterLogic
/// </summary>
public static class ActivityMasterLogic
{
    static ActivityMasterLogic()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static List<GetActivityLists_Result> GetActivityList(GetActivityLists_Result actObj, int resLandId=1,int IsPublic=1)
    {
        var db = new startetkuEntities1();
        try
        {
            actObj.ActIsActive = true;
            actObj.comIsDeleted = false;

            if (actObj.ActStartDate < DateTime.Now.AddYears(-100))
            {
                actObj.ActStartDate = CommonUtilities.GetCurrentDateTime().AddYears(-100);
            }
            if (actObj.ActEndDate < DateTime.Now.AddYears(-100))
            {
                actObj.ActEndDate = CommonUtilities.GetCurrentDateTime().AddYears(100);
            }
            if (actObj.ActIsDeleted == null)
            {
                actObj.ActIsDeleted = false;
            }
            var getLists = db.GetActivityLists(resLandId,
                actObj.ActId, 
                actObj.ActCreatedBy, 
                actObj.ActCompanyId, 
                actObj.ActReqUserId, 
                actObj.ActReqId, 
                actObj.ActReqUserId, 
                actObj.ActCatRefId, 
                actObj.ActStartDate,
                actObj.ActEndDate,
                actObj.ActDivId, 
                actObj.ActJobId, 
                actObj.ActCompId, 
                actObj.ActName, 
                actObj.ActIsActive,
                actObj.ActIsDeleted, IsPublic, actObj.userTeamID).ToList();
            return getLists;

        }
        catch (Exception ex)
        {

            ExceptionLogger.LogException(ex);
            return new List<GetActivityLists_Result>();
        }
        finally
        {
            CommonAttributes.DisposeDBObject(ref db);
        }

    }
    public static List<Overview_ActivityStatus_Tab4_Result> GetActivityList_Tab4(GetActivityLists_Result actObj)
    {
        var db = new startetkuEntities1();
        try
        {
            actObj.ActIsActive = true;
            actObj.comIsDeleted = false;
            
            var getLists = db.Overview_ActivityStatus_Tab4(actObj.ActId, actObj.ActCreatedBy, actObj.ActCompanyId, 0, actObj.ActReqId, 0, actObj.ActCatRefId, null, null,
                            actObj.ActDivId, actObj.ActJobId, actObj.ActCompId, actObj.ActName, actObj.ActIsActive, actObj.comIsDeleted,actObj.userTeamID).ToList();
            return getLists;

        }
        catch (Exception ex)
        {

            ExceptionLogger.LogException(ex);
            return new List<Overview_ActivityStatus_Tab4_Result>();
        }
        finally
        {
            CommonAttributes.DisposeDBObject(ref db);
        }

    }
}