﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using starteku_BusinessLogic;
using starteku_BusinessLogic.Model;

/// <summary>
/// Summary description for ComchildBM
/// </summary>
public class ComchildBM
{
	public ComchildBM()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static List<GetComChild_Result> GetComChildRequestList(int comId, int comActual)
    {
        startetkuEntities1 db = DbOperation.GetDb(); 
        try
        {
            if (comActual == 0)
            {
                comActual = 1;
            }
            var getLists = db.GetComChild(comId, comActual).ToList();
            return getLists;

        }
        catch (Exception ex)
        {

            ExceptionLogger.LogException(ex);
            return new List<GetComChild_Result>();
        }
        finally
        {
            CommonAttributes.DisposeDBObject(ref db);
        }
    }

}