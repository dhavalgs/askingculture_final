﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using starteku.Data.Manager;
using System.Configuration;
using starteku_BusinessLogic.Model;

namespace starteku_BusinessLogic
{
    public class ActReqComment
    {
        public static List<GetActivityCommentById_Result> GetActivityCommentById(int AcrCreaterId,int ActID,int LoginUserID,int UserType)
        {
            var db = new startetkuEntities1();
            var actreqList = new List<GetActivityCommentById_Result>();
            try
            {
                
                actreqList = db.GetActivityCommentById(AcrCreaterId, ActID, LoginUserID, UserType).ToList();


            }
            catch (Exception ex)
            {

                ExceptionLogger.LogException(ex);
                return new List<GetActivityCommentById_Result>();
            }
            finally
            {

                CommonAttributes.DisposeDBObject(ref db);
            }

            return actreqList;
        }

    }
}