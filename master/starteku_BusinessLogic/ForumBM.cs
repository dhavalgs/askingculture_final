using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starteku.Data.Manager;
using System.Data;
using starteku_BusinessLogic;


namespace startetku.Business.Logic
{

    public class ForumBM
    {
        #region Private Declaration

        private DataSet _ds;
        private string _returnString;
        private bool _returnBoolean;
        private Int32 _forumId;
        private String _forumTitle;
        private String _forumDescripiton;
        private Int32 _forumUserId;
        private DateTime _forumCreatedDate;
        private DateTime _forumUpdatedDate;
        private Boolean _forumIsDeleted;
        private Boolean _fourmIsActive;
        private Int32 _fourmCompanyId;
        private Int32 _fourmCreateBy;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public DataSet ds
        {
            get { return _ds; }
            set { _ds = value; }
        }
        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public string ReturnString
        {
            get { return _returnString; }
            set { _returnString = value; }
        }

        /// <summary>
        /// Gets or sets the ReturnBoolean value.
        /// </summary>
        public bool ReturnBoolean
        {
            get { return _returnBoolean; }
            set { _returnBoolean = value; }
        }

        /// <summary>
        /// Gets or sets the forumId value.
        /// </summary>
        public Int32 forumId
        {
            get { return _forumId; }
            set { _forumId = value; }
        }

        /// <summary>
        /// Gets or sets the forumTitle value.
        /// </summary>
        public String forumTitle
        {
            get { return _forumTitle; }
            set { _forumTitle = value; }
        }

        /// <summary>
        /// Gets or sets the forumDescripiton value.
        /// </summary>
        public String forumDescripiton
        {
            get { return _forumDescripiton; }
            set { _forumDescripiton = value; }
        }

        /// <summary>
        /// Gets or sets the forumUserId value.
        /// </summary>
        public Int32 forumUserId
        {
            get { return _forumUserId; }
            set { _forumUserId = value; }
        }

        /// <summary>
        /// Gets or sets the forumCreatedDate value.
        /// </summary>
        public DateTime forumCreatedDate
        {
            get { return _forumCreatedDate; }
            set { _forumCreatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the forumUpdatedDate value.
        /// </summary>
        public DateTime forumUpdatedDate
        {
            get { return _forumUpdatedDate; }
            set { _forumUpdatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the forumIsDeleted value.
        /// </summary>
        public Boolean forumIsDeleted
        {
            get { return _forumIsDeleted; }
            set { _forumIsDeleted = value; }
        }

        /// <summary>
        /// Gets or sets the fourmIsActive value.
        /// </summary>
        public Boolean fourmIsActive
        {
            get { return _fourmIsActive; }
            set { _fourmIsActive = value; }
        }

        /// <summary>
        /// Gets or sets the fourmCompanyId value.
        /// </summary>
        public Int32 fourmCompanyId
        {
            get { return _fourmCompanyId; }
            set { _fourmCompanyId = value; }
        }

        /// <summary>
        /// Gets or sets the fourmCreateBy value.
        /// </summary>
        public Int32 fourmCreateBy
        {
            get { return _fourmCreateBy; }
            set { _fourmCreateBy = value; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Inserts Records in the Forum table.
        /// </summary>
        public void InsertForum()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();


                ParamStruct[] param = new ParamStruct[9];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue; 

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@forumTitle";
                param[0].DataType = DbType.String;
                if (String.IsNullOrEmpty(_forumTitle))
                    param[0].value = DBNull.Value;
                else
                    param[0].value = _forumTitle;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@forumDescripiton";
                param[1].DataType = DbType.String;
                if (String.IsNullOrEmpty(_forumDescripiton))
                    param[1].value = DBNull.Value;
                else
                    param[1].value = _forumDescripiton;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@forumUserId";
                param[2].DataType = DbType.Int32;
                if (_forumUserId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[2].value = _forumUserId;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@forumCreatedDate";
                param[3].DataType = DbType.DateTime;
                if (_forumCreatedDate.Ticks > dtmin.Ticks && _forumCreatedDate.Ticks < dtmax.Ticks)
                    param[3].value = _forumCreatedDate;
                else if (_forumCreatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[3].value = _forumCreatedDate;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@forumUpdatedDate";
                param[4].DataType = DbType.DateTime;
                if (_forumUpdatedDate.Ticks > dtmin.Ticks && _forumUpdatedDate.Ticks < dtmax.Ticks)
                    param[4].value = _forumUpdatedDate;
                else if (_forumUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[4].value = _forumUpdatedDate;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@forumIsDeleted";
                param[5].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_forumIsDeleted.ToString()))
                    param[5].value = DBNull.Value;
                else
                    param[5].value = _forumIsDeleted;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@fourmIsActive";
                param[6].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_fourmIsActive.ToString()))
                    param[6].value = DBNull.Value;
                else
                    param[6].value = _fourmIsActive;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@fourmCompanyId";
                param[7].DataType = DbType.Int32;
                if (_fourmCompanyId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[7].value = _fourmCompanyId;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@fourmCreateBy";
                param[8].DataType = DbType.Int32;
                param[8].value = _fourmCreateBy;

               
                _ds = DBAccess.ExecDataSet("InsertForum", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }
        public void GetAllfourm()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@fourmIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _fourmIsActive;


                _ds = DBAccess.ExecDataSet("GetAllfourm", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllfourm" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        #endregion

    }
}
