﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Web;
using System.Web.Caching;

namespace starteku_BusinessLogic
{
    public static class CacheHelper
    {
        /*--|Saurin Vala| ---------------| 2015-02-18 | ----|TESTED OK|---------------------Helper function allow to add cache, read cache, and remove cache-------*/
        /*Simple to use
         just call functon , no tnothing any special then it.
         *  USE KEY NAME  As
         * 
         * 
         */


        /// <summary>
        /// Insert value into the cache using
        /// appropriate name/value pairs
        /// </summary>
        /// <typeparam name="T">Type of cached item</typeparam>
        /// <param name="o">Item to be cached</param>
        /// <param name="key">Name of item</param>
        public static void Add<T>(T o, string key, double Timeout)
        {
            HttpContext.Current.Cache.Insert(
                key,
                o,
                null,
                DateTime.Now.AddMinutes(Timeout),
                System.Web.Caching.Cache.NoSlidingExpiration);
        }

        /// <summary>
        /// Remove item from cache
        /// </summary>
        /// <param name="key">Name of cached item</param>
        public static void Clear(string key)
        {
            HttpContext.Current.Cache.Remove(key);
        }

        /// <summary>
        /// Check for item in cache
        /// </summary>
        /// <param name="key">Name of cached item</param>
        /// <returns></returns>
        public static bool Exists(string key)
        {
            return HttpContext.Current.Cache[key] != null;
        }

        /// <summary>
        /// Retrieve cached item
        /// </summary>
        /// <typeparam name="T">Type of cached item</typeparam>
        /// <param name="key">Name of cached item</param>
        /// <param name="value">Cached value. Default(T) if item doesn't exist.</param>
        /// <returns>Cached item as type</returns>
        public static bool Get<T>(string key, out T value)
        {
            try
            {
                if (!Exists(key))
                {
                    value = default(T);
                    return false;
                }

                value = (T)HttpContext.Current.Cache[key];
            }
            catch
            {
                value = default(T);
                return false;
            }

            return true;
        }
        //One place to set cache expire time
        public static int CacheExpireTime()
        {
            return 1000*60;
        }
        //Emrgency function | this will remove all cache data.
        public static void RemoveAllCacheData(int userid)
        {/*From Report page*/
            Clear("GetAllCompetenceMasterAdd-" + userid);
            Clear("GetAllEmployeecreatebymanager_ViewPoint-" + userid);
            Clear("GetHelpRequestedByUserId-" + userid);
            // var cacheKeyName = "rptDevelopment-GetLevelByUserIdandcomId-Uid-" + Convert.ToString(userId.Value) + "-ComId-" + x.ToString();
            // var cacheKeyName = "rowRepeater-GetLevelByUserIdandcomId-Uid-" + Convert.ToString(userId.Value) + "-ComId-" + x.ToString();
            Clear("Developmentcomnames-" + userid); 
        }

        public static void RemoveAllCache()
        {
            foreach (var element in MemoryCache.Default)
            {
                MemoryCache.Default.Remove(element.Key);
            }
            foreach (var element in HttpContext.Current.Cache)
            {
                Clear(element.ToString());
            }
           // List<KeyValuePair<String, Object>> cacheItems = (from n in Cache as ParallelEnumerable() select n).ToList();

           /* foreach (KeyValuePair<String, Object> a in cacheItems)
                Cache.Remove(a.Key);*/
        }
    }

}
