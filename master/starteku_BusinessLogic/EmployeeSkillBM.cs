using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starteku.Data.Manager;
using System.Data;
using starteku_BusinessLogic;
using System.Data.SqlClient;
namespace startetku.Business.Logic
{

    public class EmployeeSkillBM
    {
        #region Private Declaration

        private DataSet _ds;
        private string _returnString;
        private bool _returnBoolean;
        private Int32 _skillId;
        private Int32 _skillUserId;
        private Int32 _skillSubId;
        private Int32 _skillComId;
        private String _skillDoc;
        private Int32 _skillStatus;
        private Int32 _skillPoint;
        private DateTime _skillCreatedDate;
        private DateTime _skillUpdatedDate;
        private Boolean _skillIsDeleted;
        private Boolean _skillIsActive;
        private Int32 _skillCreateBy;
        private Int32 _skillCompanyId;
        private Int32 _skillLevel;
        private Int32 _skillchildId;
        private Int32 _skillLocal;
        private Int32 _skillAchive;
        private Int32 _skilltarget;
        private Int32 _skillSet;
        private DateTime _skillAcceptDate;
        private Int32 _comcategoryId;
        private bool _skillIsSaved;
        private Int32 _temp;
        private string _mulsel;
        private string _skillComment;


        private Int32 _skillLongTermTarget;
        private Int32 _skillMaxLevelTarget;

        private Int32 _TargetType;

        private Int32 _skillTeamID;

        private Int32 _LoginUserID;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public DataSet ds
        {
            get { return _ds; }
            set { _ds = value; }
        }

        public bool skillIsSaved
        {
            get { return _skillIsSaved; }
            set { _skillIsSaved = value; }
        }

        public string skillComment
        {
            get { return _skillComment; }
            set { _skillComment = value; }
        }
        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public string ReturnString
        {
            get { return _returnString; }
            set { _returnString = value; }
        }

        /// <summary>
        /// Gets or sets the ReturnBoolean value.
        /// </summary>
        public bool ReturnBoolean
        {
            get { return _returnBoolean; }
            set { _returnBoolean = value; }
        }

        /// <summary>
        /// Gets or sets the skillId value.
        /// </summary>
        public Int32 skillId
        {
            get { return _skillId; }
            set { _skillId = value; }
        }

        public Int32 LoginUserID
        {
            get { return _LoginUserID; }
            set { _LoginUserID = value; }
        }

        /// <summary>
        /// Gets or sets the skillUserId value.
        /// </summary>
        public Int32 skillUserId
        {
            get { return _skillUserId; }
            set { _skillUserId = value; }
        }

        public Int32 skillCompanyId
        {
            get { return _skillCompanyId; }
            set { _skillCompanyId = value; }
        }

        /// <summary>
        /// Gets or sets the skillSubId value.
        /// </summary>
        public Int32 skillSubId
        {
            get { return _skillSubId; }
            set { _skillSubId = value; }
        }

        /// <summary>
        /// Gets or sets the skillComId value.
        /// </summary>
        public Int32 skillComId
        {
            get { return _skillComId; }
            set { _skillComId = value; }
        }

        /// <summary>
        /// Gets or sets the skillDoc value.
        /// </summary>
        public String skillDoc
        {
            get { return _skillDoc; }
            set { _skillDoc = value; }
        }

        /// <summary>
        /// Gets or sets the skillStatus value.
        /// </summary>
        public Int32 skillStatus
        {
            get { return _skillStatus; }
            set { _skillStatus = value; }
        }

        /// <summary>
        /// Gets or sets the skillPoint value.
        /// </summary>
        public Int32 skillPoint
        {
            get { return _skillPoint; }
            set { _skillPoint = value; }
        }

        /// <summary>
        /// Gets or sets the skillCreatedDate value.
        /// </summary>
        public DateTime skillCreatedDate
        {
            get { return _skillCreatedDate; }
            set { _skillCreatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the skillUpdatedDate value.
        /// </summary>
        public DateTime skillUpdatedDate
        {
            get { return _skillUpdatedDate; }
            set { _skillUpdatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the skillIsDeleted value.
        /// </summary>
        public Boolean skillIsDeleted
        {
            get { return _skillIsDeleted; }
            set { _skillIsDeleted = value; }
        }

        /// <summary>
        /// Gets or sets the skillIsActive value.
        /// </summary>
        public Boolean skillIsActive
        {
            get { return _skillIsActive; }
            set { _skillIsActive = value; }
        }
        public Int32 temp
        {
            get { return _temp; }
            set { _temp = value; }
        }
        public string mulsel
        {
            get { return _mulsel; }
            set { _mulsel = value; }
        }
        /// <summary>
        /// Gets or sets the skillCreateBy value.
        /// </summary>
        public Int32 skillCreateBy
        {
            get { return _skillCreateBy; }
            set { _skillCreateBy = value; }
        }
        public Int32 skillLevel
        {
            get { return _skillLevel; }
            set { _skillLevel = value; }
        }
        public Int32 skillchildId
        {
            get { return _skillchildId; }
            set { _skillchildId = value; }
        }
        public Int32 skillLocal
        {
            get { return _skillLocal; }
            set { _skillLocal = value; }
        }
        public Int32 skillAchive
        {
            get { return _skillAchive; }
            set { _skillAchive = value; }
        }
        public Int32 skilltarget
        {
            get { return _skilltarget; }
            set { _skilltarget = value; }
        }
        public Int32 skillSet
        {
            get { return _skillSet; }
            set { _skillSet = value; }
        }
        public DateTime skillAcceptDate
        {
            get { return _skillAcceptDate; }
            set { _skillAcceptDate = value; }
        }
        public Int32 comcategoryId
        {
            get { return _comcategoryId; }
            set { _comcategoryId = value; }
        }


        public Int32 skillLongTermTarget
        {
            get { return _skillLongTermTarget; }
            set { _skillLongTermTarget = value; }
        }
        public Int32 skillMaxLevelTarget
        {
            get { return _skillMaxLevelTarget; }
            set { _skillMaxLevelTarget = value; }
        }

        public Int32 TargetType
        {
            get { return _TargetType; }
            set { _TargetType = value; }
        }
        public Int32 skillTeamID
        {
            get { return _skillTeamID; }
            set { _skillTeamID = value; }
        }
        #endregion

        #region Methods

        public void InsertEmployeeSkill()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[12];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue;

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@skillUserId";
                param[0].DataType = DbType.Int32;
                if (_skillUserId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[0].value = _skillUserId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@skillSubId";
                param[1].DataType = DbType.Int32;
                param[1].value = _skillSubId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@skillComId";
                param[2].DataType = DbType.Int32;
                if (_skillComId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[2].value = _skillComId;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@skillDoc";
                param[3].DataType = DbType.String;
                if (String.IsNullOrEmpty(_skillDoc))
                    param[3].value = DBNull.Value;
                else
                    param[3].value = _skillDoc;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@skillStatus";
                param[4].DataType = DbType.Int32;
                param[4].value = _skillStatus;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@skillPoint";
                param[5].DataType = DbType.Int32;
                if (_skillPoint == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[5].value = _skillPoint;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@skillCreatedDate";
                param[6].DataType = DbType.DateTime;
                if (_skillCreatedDate.Ticks > dtmin.Ticks && _skillCreatedDate.Ticks < dtmax.Ticks)
                    param[6].value = _skillCreatedDate;
                else if (_skillCreatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[6].value = _skillCreatedDate;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@skillUpdatedDate";
                param[7].DataType = DbType.DateTime;
                if (_skillUpdatedDate.Ticks > dtmin.Ticks && _skillUpdatedDate.Ticks < dtmax.Ticks)
                    param[7].value = _skillUpdatedDate;
                else if (_skillUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[7].value = _skillUpdatedDate;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@skillIsDeleted";
                param[8].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_skillIsDeleted.ToString()))
                    param[8].value = DBNull.Value;
                else
                    param[8].value = _skillIsDeleted;

                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@skillIsActive";
                param[9].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_skillIsActive.ToString()))
                    param[9].value = DBNull.Value;
                else
                    param[9].value = _skillIsActive;

                param[10].direction = ParameterDirection.Input;
                param[10].ParamName = "@skillCreateBy";
                param[10].DataType = DbType.Int32;
                if (_skillCreateBy == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[10].value = _skillCreateBy;

                param[11].direction = ParameterDirection.Input;
                param[11].ParamName = "@skillCompanyId";
                param[11].DataType = DbType.Int32;
                if (_skillCompanyId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[11].value = _skillCompanyId;

                //obj.ExecScalar("InsertEmployeeSkill", CommandType.StoredProcedure, param);				_returnBoolean = true;
                _ds = DBAccess.ExecDataSet("InsertEmployeeSkill", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        public void GetAllPendingRequest()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[4];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@skillIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _skillIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@skillIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _skillIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@skillStatus";
                param[2].DataType = DbType.Int32;
                param[2].value = _skillStatus;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@skillCompanyId";
                param[3].DataType = DbType.Int32;
                param[3].value = _skillCompanyId;


                _ds = DBAccess.ExecDataSet("GetAllPendingRequest", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllEmployeecreatebymanager" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void GetAllPendingRequestbyid()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[1];


                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@skillId";
                param[0].DataType = DbType.Int32;
                param[0].value = _skillId;


                _ds = DBAccess.ExecDataSet("GetAllPendingRequestbyid", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllPendingRequestbyid" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void UpdateendingRequest()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[3];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue;

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@skillId";
                param[0].DataType = DbType.Int32;
                param[0].value = _skillId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@skillStatus";
                param[1].DataType = DbType.Int32;
                param[1].value = _skillStatus;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@skillPoint";
                param[2].DataType = DbType.Int32;
                if (_skillPoint == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[2].value = _skillPoint;

                _ds = DBAccess.ExecDataSet("UpdateendingRequest", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        public void RequestStatusUpdate()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] pm = new ParamStruct[3];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@skillId";
                pm[0].DataType = DbType.Int32;
                pm[0].value = _skillId;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@skillIsActive";
                pm[1].DataType = DbType.Boolean;
                pm[1].value = _skillIsActive;

                pm[2].direction = ParameterDirection.Input;
                pm[2].ParamName = "@skillIsDeleted";
                pm[2].DataType = DbType.Boolean;
                pm[2].value = _skillIsDeleted;

                _ds = DBAccess.ExecDataSet("RequestStatusUpdate", CommandType.StoredProcedure, pm);
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in RequestStatusUpdate  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            }
        }

        public void InsertSkillMaster()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[12];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = Convert.ToDateTime("1/1/9999 11:59:59 PM");

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@skillUserId";
                param[0].DataType = DbType.Int32;
                if (_skillUserId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[0].value = _skillUserId;


                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@skillComId";
                param[1].DataType = DbType.Int32;
                if (_skillComId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[1].value = _skillComId;


                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@skillStatus";
                param[2].DataType = DbType.Int32;
                param[2].value = _skillStatus;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@skillCreatedDate";
                param[3].DataType = DbType.DateTime;
                if (_skillCreatedDate.Ticks > dtmin.Ticks && _skillCreatedDate.Ticks < dtmax.Ticks)
                    param[3].value = _skillCreatedDate;
                else if (_skillCreatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[3].value = _skillCreatedDate;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@skillUpdatedDate";
                param[4].DataType = DbType.DateTime;
                if (_skillUpdatedDate.Ticks > dtmin.Ticks && _skillUpdatedDate.Ticks < dtmax.Ticks)
                    param[4].value = _skillUpdatedDate;
                else if (_skillUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[4].value = _skillUpdatedDate;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@skillIsDeleted";
                param[5].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_skillIsDeleted.ToString()))
                    param[5].value = DBNull.Value;
                else
                    param[5].value = _skillIsDeleted;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@skillIsActive";
                param[6].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_skillIsActive.ToString()))
                    param[6].value = DBNull.Value;
                else
                    param[6].value = _skillIsActive;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@skillCompanyId";
                param[7].DataType = DbType.Int32;
                if (_skillCompanyId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[7].value = _skillCompanyId;


                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@skillLocal";
                param[8].DataType = DbType.Int32;
                param[8].value = _skillLocal;


                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@skillAchive";
                param[9].DataType = DbType.Int32;
                param[9].value = _skillAchive;

                param[10].direction = ParameterDirection.Input;
                param[10].ParamName = "@skilltarget";
                param[10].DataType = DbType.Int32;
                param[10].value = _skilltarget;

                param[11].direction = ParameterDirection.Input;
                param[11].ParamName = "@skillComment";
                param[11].DataType = DbType.String;
                param[11].value = _skillComment;



                //obj.ExecScalar("InsertEmployeeSkill", CommandType.StoredProcedure, param);				_returnBoolean = true;
                _ds = DBAccess.ExecDataSet("InsertSkillMaster", CommandType.StoredProcedure, param);
                // return Convert.ToInt32(_ds.Tables[0].Rows[0][0]);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        public void InsertsetSkillMaster()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[15];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = Convert.ToDateTime("1/1/9999 11:59:59 PM");

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@skillUserId";
                param[0].DataType = DbType.Int32;
                if (_skillUserId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[0].value = _skillUserId;


                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@skillComId";
                param[1].DataType = DbType.Int32;
                if (_skillComId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[1].value = _skillComId;


                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@skillStatus";
                param[2].DataType = DbType.Int32;
                param[2].value = _skillStatus;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@skillCreatedDate";
                param[3].DataType = DbType.DateTime;
                if (_skillCreatedDate.Ticks > dtmin.Ticks && _skillCreatedDate.Ticks < dtmax.Ticks)
                    param[3].value = _skillCreatedDate;
                else if (_skillCreatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[3].value = _skillCreatedDate;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@skillUpdatedDate";
                param[4].DataType = DbType.DateTime;
                if (_skillUpdatedDate.Ticks > dtmin.Ticks && _skillUpdatedDate.Ticks < dtmax.Ticks)
                    param[4].value = _skillUpdatedDate;
                else if (_skillUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[4].value = _skillUpdatedDate;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@skillIsDeleted";
                param[5].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_skillIsDeleted.ToString()))
                    param[5].value = DBNull.Value;
                else
                    param[5].value = _skillIsDeleted;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@skillIsActive";
                param[6].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_skillIsActive.ToString()))
                    param[6].value = DBNull.Value;
                else
                    param[6].value = _skillIsActive;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@skillCompanyId";
                param[7].DataType = DbType.Int32;
                if (_skillCompanyId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[7].value = _skillCompanyId;


                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@skillLocal";
                param[8].DataType = DbType.Int32;
                param[8].value = _skillLocal;


                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@skillAchive";
                param[9].DataType = DbType.Int32;
                param[9].value = _skillAchive;

                param[10].direction = ParameterDirection.Input;
                param[10].ParamName = "@skilltarget";
                param[10].DataType = DbType.Int32;
                param[10].value = _skilltarget;


                param[11].direction = ParameterDirection.Input;
                param[11].ParamName = "@skillSet";
                param[11].DataType = DbType.Int32;
                param[11].value = _skillSet;

                param[12].direction = ParameterDirection.Input;
                param[12].ParamName = "@skillComment";
                param[12].DataType = DbType.String;
                param[12].value = _skillComment;

                param[13].direction = ParameterDirection.Input;
                param[13].ParamName = "@skillIsDeleted";
                param[13].DataType = DbType.Boolean;
                param[13].value = _skillIsDeleted;

                param[14].direction = ParameterDirection.Input;
                param[14].ParamName = "@skillIsSaved";
                param[14].DataType = DbType.Boolean;
                param[14].value = _skillIsSaved;

                //obj.ExecScalar("InsertEmployeeSkill", CommandType.StoredProcedure, param);				_returnBoolean = true;
                _ds = DBAccess.ExecDataSet("InsertsetSkillMaster", CommandType.StoredProcedure, param);
                // return Convert.ToInt32(_ds.Tables[0].Rows[0][0]);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        public void GetAllSkillPendingRequest_withComment()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[4];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@skillIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _skillIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@skillIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _skillIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@skillStatus";
                param[2].DataType = DbType.Int32;
                param[2].value = _skillStatus;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@skillCompanyId";
                param[3].DataType = DbType.Int32;
                param[3].value = _skillCompanyId;


                _ds = DBAccess.ExecDataSet("GetAllSkillPendingRequest_withComment", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllSkillPendingRequest" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void InsertSkillChild()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[8];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = Convert.ToDateTime("1/1/9999 11:59:59 PM");

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@skillId";
                param[0].DataType = DbType.Int32;
                param[0].value = _skillId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@skillDoc";
                param[1].DataType = DbType.String;
                if (String.IsNullOrEmpty(_skillDoc))
                    param[1].value = DBNull.Value;
                else
                    param[1].value = _skillDoc;



                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@skillPoint";
                param[2].DataType = DbType.Int32;
                if (_skillPoint == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[2].value = _skillPoint;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@skillCreatedDate";
                param[3].DataType = DbType.DateTime;
                if (_skillCreatedDate.Ticks > dtmin.Ticks && _skillCreatedDate.Ticks < dtmax.Ticks)
                    param[3].value = _skillCreatedDate;
                else if (_skillCreatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[3].value = _skillCreatedDate;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@skillUpdatedDate";
                param[4].DataType = DbType.DateTime;
                if (_skillUpdatedDate.Ticks > dtmin.Ticks && _skillUpdatedDate.Ticks < dtmax.Ticks)
                    param[4].value = _skillUpdatedDate;
                else if (_skillUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[4].value = _skillUpdatedDate;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@skillIsDeleted";
                param[5].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_skillIsDeleted.ToString()))
                    param[5].value = DBNull.Value;
                else
                    param[5].value = _skillIsDeleted;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@skillIsActive";
                param[6].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_skillIsActive.ToString()))
                    param[6].value = DBNull.Value;
                else
                    param[6].value = _skillIsActive;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@skillLevel";
                param[7].DataType = DbType.Int32;
                if (_skillLevel == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[7].value = _skillLevel;


                //obj.ExecScalar("InsertEmployeeSkill", CommandType.StoredProcedure, param);				_returnBoolean = true;
                _ds = DBAccess.ExecDataSet("InsertSkillChild", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        public void GetAllSkillPendingRequest()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[5];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@skillIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _skillIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@skillIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _skillIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@skillStatus";
                param[2].DataType = DbType.Int32;
                param[2].value = _skillStatus;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@skillCompanyId";
                param[3].DataType = DbType.Int32;
                param[3].value = _skillCompanyId;

                param[4].direction = ParameterDirection.Input; //COde update on  2015-10-07  | Get User by creater
                param[4].ParamName = "@userCreateBy";
                param[4].DataType = DbType.Int32;
                param[4].value = _skillCreateBy;


                _ds = DBAccess.ExecDataSet("GetAllSkillPendingRequest", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllSkillPendingRequest" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void GetViewSkillRequest()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[4];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@skillIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _skillIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@skillIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _skillIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@skillStatus";
                param[2].DataType = DbType.Int32;
                param[2].value = _skillStatus;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@skillUserId";
                param[3].DataType = DbType.Int32;
                param[3].value = _skillUserId;

                _ds = DBAccess.ExecDataSet("GetViewSkillRequest", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetViewSkillRequest" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void SkillRequestStatusUpdate()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] pm = new ParamStruct[3];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@skillId";
                pm[0].DataType = DbType.Int32;
                pm[0].value = _skillId;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@skillIsActive";
                pm[1].DataType = DbType.Boolean;
                pm[1].value = _skillIsActive;

                pm[2].direction = ParameterDirection.Input;
                pm[2].ParamName = "@skillIsDeleted";
                pm[2].DataType = DbType.Boolean;
                pm[2].value = _skillIsDeleted;

                _ds = DBAccess.ExecDataSet("SkillRequestStatusUpdate", CommandType.StoredProcedure, pm);
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in RequestStatusUpdate  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            }
        }

        public void GetAllSkillPendingRequestbyid()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[1];


                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@skillId";
                param[0].DataType = DbType.Int32;
                param[0].value = _skillId;


                _ds = DBAccess.ExecDataSet("GetAllSkillPendingRequestbyid", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllSkillPendingRequestbyid" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void UpdateSkillpendingRequest()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[2];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = Convert.ToDateTime("1/1/9999 11:59:59 PM");

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@skillId";
                param[0].DataType = DbType.Int32;
                param[0].value = _skillId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@skillStatus";
                param[1].DataType = DbType.Int32;
                param[1].value = _skillStatus;

                _ds = DBAccess.ExecDataSet("UpdateSkillpendingRequest", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }
        public void UpdateSkillchildpendingRequest()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[4];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = Convert.ToDateTime("1/1/9999 11:59:59 PM");

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@skillId";
                param[0].DataType = DbType.Int32;
                param[0].value = _skillId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@skillPoint";
                param[1].DataType = DbType.Int32;
                param[1].value = _skillPoint;


                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@skillchildId";
                param[2].DataType = DbType.Int32;
                param[2].value = _skillchildId;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@skillLevel";
                param[3].DataType = DbType.Int32;
                param[3].value = _skillLevel;

                _ds = DBAccess.ExecDataSet("UpdateSkillchildpendingRequest", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }


        public void UpdatesetRequest()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[3];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = Convert.ToDateTime("1/1/9999 11:59:59 PM");

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@skillUserId";
                param[0].DataType = DbType.Int32;
                param[0].value = _skillUserId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@skillStatus";
                param[1].DataType = DbType.Int32;
                param[1].value = _skillStatus;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@skillIsDeleted";
                param[2].DataType = DbType.Boolean;
                param[2].value = _skillIsDeleted;

                _ds = DBAccess.ExecDataSet("UpdatesetRequest", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        public void UpdateSkillgRequestbymanager()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[10];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = Convert.ToDateTime("1/1/9999 11:59:59 PM");

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@skillId";
                param[0].DataType = DbType.Int32;
                param[0].value = _skillId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@skillStatus";
                param[1].DataType = DbType.Int32;
                param[1].value = _skillStatus;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@skillUserId";
                param[2].DataType = DbType.Int32;
                param[2].value = _skillUserId;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@skillLocal";
                param[3].DataType = DbType.Int32;
                param[3].value = _skillLocal;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@skillAchive";
                param[4].DataType = DbType.Int32;
                param[4].value = _skillAchive;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@skilltarget";
                param[5].DataType = DbType.Int32;
                param[5].value = _skilltarget;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@skillAcceptDate";
                param[6].DataType = DbType.DateTime;
                if (_skillAcceptDate.Ticks > dtmin.Ticks && _skillAcceptDate.Ticks < dtmax.Ticks)
                    param[6].value = _skillAcceptDate;
                else if (_skillAcceptDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[6].value = _skillAcceptDate;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@skillComment";
                param[7].DataType = DbType.String;
                param[7].value = _skillComment;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@skillLongTermTarget";
                param[8].DataType = DbType.Int32;
                param[8].value = _skillLongTermTarget;

                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@skillMaxLevelTarget";
                param[9].DataType = DbType.Int32;
                param[9].value = _skillMaxLevelTarget;


                _ds = DBAccess.ExecDataSet("UpdateSkillgRequestbymanager", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        public void UpdateSkillgRequestbymanager_Request()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[8];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = Convert.ToDateTime("1/1/9999 11:59:59 PM");

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@skillId";
                param[0].DataType = DbType.Int32;
                param[0].value = _skillId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@skillStatus";
                param[1].DataType = DbType.Int32;
                param[1].value = _skillStatus;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@skillUserId";
                param[2].DataType = DbType.Int32;
                param[2].value = _skillUserId;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@skillLocal";
                param[3].DataType = DbType.Int32;
                param[3].value = _skillLocal;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@skillAchive";
                param[4].DataType = DbType.Int32;
                param[4].value = _skillAchive;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@skilltarget";
                param[5].DataType = DbType.Int32;
                param[5].value = _skilltarget;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@skillAcceptDate";
                param[6].DataType = DbType.DateTime;
                if (_skillAcceptDate.Ticks > dtmin.Ticks && _skillAcceptDate.Ticks < dtmax.Ticks)
                    param[6].value = _skillAcceptDate;
                else if (_skillAcceptDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[6].value = _skillAcceptDate;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@skillComment";
                param[7].DataType = DbType.String;
                param[7].value = _skillComment;

                //param[8].direction = ParameterDirection.Input;
                //param[8].ParamName = "@skillLongTermTarget";
                //param[8].DataType = DbType.Int32;
                //param[8].value = _skillLongTermTarget;

                //param[9].direction = ParameterDirection.Input;
                //param[9].ParamName = "@skillMaxLevelTarget";
                //param[9].DataType = DbType.Int32;
                //param[9].value = _skillMaxLevelTarget;


                _ds = DBAccess.ExecDataSet("UpdateSkillgRequestbymanager_Request", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }
        public void GetSkillByUserId()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[4];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@skillIsDeleted";
                param[0].DataType = DbType.Boolean;
                param[0].value = _skillIsDeleted;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@skillIsActive";
                param[1].DataType = DbType.Boolean;
                param[1].value = _skillIsActive;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@skillCompanyId";
                param[2].DataType = DbType.Int32;
                param[2].value = _skillCompanyId;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@skillUserId";
                param[3].DataType = DbType.Int32;
                param[3].value = _skillUserId;

                _ds = DBAccess.ExecDataSet("GetSkillByUserId", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllCompetencehelp" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                // throw ex;

            }
        }
        //saurin : 20150120 : For Chart-graph manager side

        //public void SpSkillByUserId()
        //{
        //    try
        //    {
        //        DataAccess DBAccess = new DataAccess();
        //        DBAccess.Provider = EnumProviders.SQLClient;
        //        DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
        //        ParamStruct[] param = new ParamStruct[5];

        //        param[0].direction = ParameterDirection.Input;
        //        param[0].ParamName = "@skillIsDeleted";
        //        param[0].DataType = DbType.Boolean;
        //        param[0].value = _skillIsDeleted;

        //        param[1].direction = ParameterDirection.Input;
        //        param[1].ParamName = "@skillIsActive";
        //        param[1].DataType = DbType.Boolean;
        //        param[1].value = _skillIsActive;

        //        param[2].direction = ParameterDirection.Input;
        //        param[2].ParamName = "@skillCompanyId";
        //        param[2].DataType = DbType.Int32;
        //        param[2].value = _skillCompanyId;

        //        param[3].direction = ParameterDirection.Input;
        //        param[3].ParamName = "@skillUserId";
        //        param[3].DataType = DbType.Int32;
        //        param[3].value = _skillUserId;

        //        param[4].direction = ParameterDirection.Input;
        //        param[4].ParamName = "@temp";
        //        param[4].DataType = DbType.Int32;
        //        param[4].value = _temp;

        //        _ds = DBAccess.ExecDataSet("SpSkillByUserId", CommandType.StoredProcedure, param);

        //    }
        //    catch (DataException ex)
        //    {
        //        Common.WriteLog("error in GetAllCompetencehelp" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
        //        // throw ex;

        //    }
        //}


        public DataSet mulselrecord()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[9];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@skillIsDeleted";
                param[0].DataType = DbType.Boolean;
                param[0].value = _skillIsDeleted;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@skillIsActive";
                param[1].DataType = DbType.Boolean;
                param[1].value = _skillIsActive;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@skillCompanyId";
                param[2].DataType = DbType.Int32;
                param[2].value = _skillCompanyId;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@skillUserId";
                param[3].DataType = DbType.Int32;
                param[3].value = _skillUserId;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@oderby";
                param[4].DataType = DbType.Int32;
                param[4].value = _temp;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@mulrec";
                param[5].DataType = DbType.String;
                param[5].value = _mulsel;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@TargetType";
                param[6].DataType = DbType.Int32;
                param[6].value = _TargetType;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@TeamID";
                param[7].DataType = DbType.Int32;
                param[7].value = _skillTeamID;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@LoginUserID";
                param[8].DataType = DbType.Int32;
                param[8].value = _LoginUserID;

                _ds = DBAccess.ExecDataSet("Spmulselrecord", CommandType.StoredProcedure, param);
                return _ds;
            }
            catch (DataException ex)
            {
                Common.WriteLog("error in Spmulselrecord" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        public void GetEmpAvgSkillByManager()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[5];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@skillIsDeleted";
                param[0].DataType = DbType.Boolean;
                param[0].value = _skillIsDeleted;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@skillIsActive";
                param[1].DataType = DbType.Boolean;
                param[1].value = _skillIsActive;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@userType";
                param[2].DataType = DbType.Int32;
                param[2].value = 2;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@userCreateBy";
                param[3].DataType = DbType.String;
                param[3].value = Convert.ToString(_skillUserId);

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@skillCompanyId";
                param[4].DataType = DbType.Int32;
                param[4].value = _skillCompanyId;


                _ds = DBAccess.ExecDataSet("GetEmpAvgSkillByManager", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetEmpAvgSkillByManager" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                // throw ex;

            }
        }
        public void SpEmpAvgSkillByManager()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[10];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@skillIsDeleted";
                param[0].DataType = DbType.Boolean;
                param[0].value = _skillIsDeleted;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@skillIsActive";
                param[1].DataType = DbType.Boolean;
                param[1].value = _skillIsActive;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@userType";
                param[2].DataType = DbType.Int32;
                param[2].value = 2;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@userCreateBy";
                param[3].DataType = DbType.String;
                param[3].value = Convert.ToString(_skillUserId);

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@skillCompanyId";
                param[4].DataType = DbType.Int32;
                param[4].value = _skillCompanyId;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@oderby";
                param[5].DataType = DbType.Int32;
                param[5].value = _temp;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@mulrec";
                param[6].DataType = DbType.String;
                param[6].value = _mulsel;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@TargetType";
                param[7].DataType = DbType.Int32;
                param[7].value = _TargetType;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@TeamID";
                param[8].DataType = DbType.Int32;
                param[8].value = _skillTeamID;

                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@LoginUserID";
                param[9].DataType = DbType.Int32;
                param[9].value = _LoginUserID;

                _ds = DBAccess.ExecDataSet("SpEmpAvgSkillByManager", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in SpEmpAvgSkillByManager" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                // throw ex;

            }
        }
        public void GetEmpSkill()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@skillIsDeleted";
                param[0].DataType = DbType.Boolean;
                param[0].value = _skillIsDeleted;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@skillIsActive";
                param[1].DataType = DbType.Boolean;
                param[1].value = _skillIsActive;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@skillCompanyId";
                param[2].DataType = DbType.Int32;
                param[2].value = _skillCompanyId;

                _ds = DBAccess.ExecDataSet("GetEmpSkill", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllCompetencehelp" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void GetHelpCompotenceLessthen(Int32 id)
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[6];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@skillIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _skillIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@skillIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _skillIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@skillStatus";
                param[2].DataType = DbType.Int32;
                param[2].value = _skillStatus;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@skillCompanyId";
                param[3].DataType = DbType.Int32;
                param[3].value = _skillCompanyId;


                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@skillComId";
                param[4].DataType = DbType.Int32;
                param[4].value = _skillComId;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@skillUserId";
                param[5].DataType = DbType.Int32;
                param[5].value = _skillUserId;

                if (id == 1)
                {
                    _ds = DBAccess.ExecDataSet("GetHelpCompotenceLessthen", CommandType.StoredProcedure, param);
                }
                else
                {
                    _ds = DBAccess.ExecDataSet("GetHelpCompotencegreterthen", CommandType.StoredProcedure, param);
                }

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetHelpCompotenceLessthen" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        public void GetViewSkillRequestbyAccordion()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[5];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@skillIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _skillIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@skillIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _skillIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@skillStatus";
                param[2].DataType = DbType.Int32;
                param[2].value = _skillStatus;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@skillUserId";
                param[3].DataType = DbType.Int32;
                param[3].value = _skillUserId;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@comcategoryId";
                param[4].DataType = DbType.Int32;
                param[4].value = _comcategoryId;

                _ds = DBAccess.ExecDataSet("GetViewSkillRequestbyAccordion", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetViewSkillRequestbyAccordion" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        public void GetMessgeSkillLessthen(Int32 id)
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[6];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@skillIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _skillIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@skillIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _skillIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@skillUserId";
                param[2].DataType = DbType.Int32;
                param[2].value = _skillUserId;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@skillCompanyId";
                param[3].DataType = DbType.Int32;
                param[3].value = _skillCompanyId;


                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@skillComId";
                param[4].DataType = DbType.Int32;
                param[4].value = _skillComId;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@skillAchive";
                param[5].DataType = DbType.Int32;
                param[5].value = _skillAchive;

                if (id == 1)
                {
                    _ds = DBAccess.ExecDataSet("GetMessgeSkillLessthen", CommandType.StoredProcedure, param);
                }
                else
                {
                    _ds = DBAccess.ExecDataSet("GetMessgeSkillGreterhen", CommandType.StoredProcedure, param);
                }

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetMessgeSkillLessthen" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void GetLevelByUserIdandcomId(Int32 CompId)
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[5];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@skillIsDeleted";
                param[0].DataType = DbType.Boolean;
                param[0].value = _skillIsDeleted;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@skillIsActive";
                param[1].DataType = DbType.Boolean;
                param[1].value = _skillIsActive;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@skillComId";
                param[2].DataType = DbType.Int32;
                param[2].value = _skillComId;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@skillUserId";
                param[3].DataType = DbType.Int32;
                param[3].value = _skillUserId;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@CompId";
                param[4].DataType = DbType.Int32;
                param[4].value = CompId;

                _ds = DBAccess.ExecDataSet("GetLevelByUserIdandcomId", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetLevelByUserIdandcomId" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                // throw ex;

            }
        }

        public void GetTeamTargetSkillRequest()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[5];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@skillIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _skillIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@skillIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _skillIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@skillStatus";
                param[2].DataType = DbType.Int32;
                param[2].value = _skillStatus;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@skillUserId";
                param[3].DataType = DbType.Int32;
                param[3].value = _skillUserId;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@skillTeamID";
                param[4].DataType = DbType.Int32;
                param[4].value = _skillTeamID;

                _ds = DBAccess.ExecDataSet("GetTeamTargetSkillRequest", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetTeamTargetSkillRequest" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        #endregion


    }
}
