using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starteku.Data.Manager;
using System.Data;
using System.Web;
using starteku_BusinessLogic;
using WatzOnTV.Logic;
using CommonUtilities = starteku_BusinessLogic.CommonUtilities;


namespace startetku.Business.Logic
{

    public class CompetenceMasterBM
    {
        #region Private Declaration

        private DataSet _ds;
        private string _returnString;
        private bool _returnBoolean;
        private Int32 _comId;
        private Int32 _comUserId;
        private String _comLevel;
        private String _comCompetence;
        private String _comCompetenceDN;
        private DateTime _comCreatedDate;
        private DateTime _comUpdatedDate;
        private Boolean _comIsDeleted;
        private Boolean _comIsActive;
        private Int32 _comCompanyId;
        private Int32 _comCreateBy;
        private Int32 _comDivId;
        private Int32 _comJobtype;

        private Int32 _catId;
        private String _catName;
        private DateTime _catCreatedDate;
        private DateTime _catUpdatedDate;
        private Boolean _catIsDeleted;
        private Boolean _catIsActive;
        private Int32 _catCompanyId;
        private Int32 _comcategoryId;


        private Int32 _comchildId;
        private Int32 _comchildcomID;
        private String _comchildlevel;
        private string _comchildlevelDN;

        private Int32 _TeamID;

        private String _ComIDs;

        //private DateTime _comCreatedDate;
        //private DateTime _comUpdatedDate;
        //private Boolean _comIsDeleted;
        //private Boolean _comIsActive;
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public DataSet ds
        {
            get { return _ds; }
            set { _ds = value; }
        }
        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public string ReturnString
        {
            get { return _returnString; }
            set { _returnString = value; }
        }

        /// <summary>
        /// Gets or sets the ReturnBoolean value.
        /// </summary>
        public bool ReturnBoolean
        {
            get { return _returnBoolean; }
            set { _returnBoolean = value; }
        }

        /// <summary>
        /// Gets or sets the comId value.
        /// </summary>
        public Int32 comId
        {
            get { return _comId; }
            set { _comId = value; }
        }
        public Int32 comJobtype
        {
            get { return _comJobtype; }
            set { _comJobtype = value; }
        }
        /// <summary>
        /// Gets or sets the comUserId value.
        /// </summary>
        public Int32 comUserId
        {
            get { return _comUserId; }
            set { _comUserId = value; }
        }

        /// <summary>
        /// Gets or sets the comLevel value.
        /// </summary>
        public String comLevel
        {
            get { return _comLevel; }
            set { _comLevel = value; }
        }

        /// <summary>
        /// Gets or sets the comCompetence value.
        /// </summary>
        public String comCompetence
        {
            get { return _comCompetence; }
            set { _comCompetence = value; }
        }

        public String comCompetenceDN
        {
            get { return _comCompetenceDN; }
            set { _comCompetenceDN = value; }
        }

        /// <summary>
        /// Gets or sets the comCreatedDate value.
        /// </summary>
        public DateTime comCreatedDate
        {
            get { return _comCreatedDate; }
            set { _comCreatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the comUpdatedDate value.
        /// </summary>
        public DateTime comUpdatedDate
        {
            get { return _comUpdatedDate; }
            set { _comUpdatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the comIsDeleted value.
        /// </summary>
        public Boolean comIsDeleted
        {
            get { return _comIsDeleted; }
            set { _comIsDeleted = value; }
        }

        /// <summary>
        /// Gets or sets the comIsActive value.
        /// </summary>
        public Boolean comIsActive
        {
            get { return _comIsActive; }
            set { _comIsActive = value; }
        }

        /// <summary>
        /// Gets or sets the comCompanyId value.
        /// </summary>
        public Int32 comCompanyId
        {
            get { return _comCompanyId; }
            set { _comCompanyId = value; }
        }

        /// <summary>
        /// Gets or sets the comCreateBy value.
        /// </summary>
        public Int32 comCreateBy
        {
            get { return _comCreateBy; }
            set { _comCreateBy = value; }
        }
        
             public Int32 TeamID
        {
            get { return _TeamID; }
            set { _TeamID = value; }
        }
        /// <summary>
        /// Gets or sets the comDivId value.
        /// </summary>
        public Int32 comDivId
        {
            get { return _comDivId; }
            set { _comDivId = value; }
        }





        public Int32 catId
        {
            get { return _catId; }
            set { _catId = value; }
        }
        public String catName
        {
            get { return _catName; }
            set { _catName = value; }
        }
        public DateTime catCreatedDate
        {
            get { return _catCreatedDate; }
            set { _catCreatedDate = value; }
        }
        public DateTime catUpdatedDate
        {
            get { return _catUpdatedDate; }
            set { _catUpdatedDate = value; }
        }
        public Boolean catIsDeleted
        {
            get { return _catIsDeleted; }
            set { _catIsDeleted = value; }
        }
        public Boolean catIsActive
        {
            get { return _catIsActive; }
            set { _catIsActive = value; }
        }
        public Int32 catCompanyId
        {
            get { return _catCompanyId; }
            set { _catCompanyId = value; }
        }
        public Int32 comcategoryId
        {
            get { return _comcategoryId; }
            set { _comcategoryId = value; }
        }

        public string comchildlevel
        {
            get { return _comchildlevel; }
            set { _comchildlevel = value; }
        }


        public string comchildlevelDN
        {
            get { return _comchildlevelDN; }
            set { _comchildlevelDN = value; }
        }

        public string ComIDs
        {
            get { return _ComIDs; }
            set { _ComIDs = value; }
        }

        public Int32 comchildcomID
        {
            get { return _comchildcomID; }
            set { _comchildcomID = value; }
        }

        #endregion

        #region Methods

        public void InsertCompetenceMaster()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[11];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue;

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@comUserId";
                param[0].DataType = DbType.Int32;
                if (_comUserId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[0].value = _comUserId;
                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@comLevel";
                param[1].DataType = DbType.String;
                if (String.IsNullOrEmpty(_comLevel))
                    param[1].value = DBNull.Value;
                else
                    param[1].value = _comLevel;
                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@comCompetence";
                param[2].DataType = DbType.String;
                if (String.IsNullOrEmpty(_comCompetence))
                    param[2].value = DBNull.Value;
                else
                    param[2].value = _comCompetence;
                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@comCreatedDate";
                param[3].DataType = DbType.DateTime;
                if (_comCreatedDate.Ticks > dtmin.Ticks && _comCreatedDate.Ticks < dtmax.Ticks)
                    param[3].value = _comCreatedDate;
                else if (_comCreatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[3].value = _comCreatedDate;
                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@comUpdatedDate";
                param[4].DataType = DbType.DateTime;
                if (_comUpdatedDate.Ticks > dtmin.Ticks && _comUpdatedDate.Ticks < dtmax.Ticks)
                    param[4].value = _comUpdatedDate;
                else if (_comUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[4].value = _comUpdatedDate;
                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@comIsDeleted";
                param[5].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_comIsDeleted.ToString()))
                    param[5].value = DBNull.Value;
                else
                    param[5].value = _comIsDeleted;
                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@comIsActive";
                param[6].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_comIsActive.ToString()))
                    param[6].value = DBNull.Value;
                else
                    param[6].value = _comIsActive;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@comCompanyId";
                param[7].DataType = DbType.Int32;
                param[7].value = _comCompanyId;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@comCreateBy";
                param[8].DataType = DbType.Int32;
                if (_comCreateBy == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[8].value = _comCreateBy;

                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@comDivId";
                param[9].DataType = DbType.Int32;
                param[9].value = _comDivId;

                param[10].direction = ParameterDirection.Input;
                param[10].ParamName = "@comJobtype";
                param[10].DataType = DbType.Int32;
                param[10].value = _comJobtype;

                //obj.ExecScalar("InsertCompetenceMaster", CommandType.StoredProcedure, param);				_returnBoolean = true;
                _ds = DBAccess.ExecDataSet("InsertCompetenceMaster", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }
        public void UpdateCompetenceMaster()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[4];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue;

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@comId";
                param[0].DataType = DbType.Int32;
                param[0].value = _comId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@comCompetence";
                param[1].DataType = DbType.String;
                if (String.IsNullOrEmpty(_comCompetence))
                    param[1].value = DBNull.Value;
                else
                    param[1].value = _comCompetence;


                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@comUpdatedDate";
                param[2].DataType = DbType.DateTime;
                if (_comUpdatedDate.Ticks > dtmin.Ticks && _comUpdatedDate.Ticks < dtmax.Ticks)
                    param[2].value = _comUpdatedDate;
                else if (_comUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[2].value = _comUpdatedDate;


                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@comJobtype";
                param[3].DataType = DbType.Int32;
                param[3].value = _comJobtype;

                //obj.ExecScalar("UpdateCompetenceMaster", CommandType.StoredProcedure, param);				_returnBoolean = true;
                _ds = DBAccess.ExecDataSet("UpdateCompetenceMaster", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }
        public void GetAllCompetenceMaster()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@comIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _comIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@comIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _comIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@comCompanyId";
                param[2].DataType = DbType.Int32;
                param[2].value = _comCompanyId;

                _ds = DBAccess.ExecDataSet("GetAllCompetenceMaster", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllCompetenceMaster" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        public void GetAllCompetenceMasterEmpskill()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[4];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@comIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _comIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@comIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _comIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@comCompanyId";
                param[2].DataType = DbType.Int32;
                param[2].value = _comCompanyId;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@comJobtype";
                param[3].DataType = DbType.Int32;
                param[3].value = _comJobtype;

                _ds = DBAccess.ExecDataSet("GetAllCompetenceMasterEmpskill", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllCompetenceMasterEmpskill" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        public void GetAllCompetenceMasterbyid()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@comId";
                param[0].DataType = DbType.Int32;
                param[0].value = _comId;

                _ds = DBAccess.ExecDataSet("GetAllCompetenceMasterbyid", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllCompetenceMasterbyid" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        public void CompetenceMasterUpdate()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] pm = new ParamStruct[3];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@comId";
                pm[0].DataType = DbType.Int32;
                pm[0].value = _comId;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@comIsActive";
                pm[1].DataType = DbType.Boolean;
                pm[1].value = _comIsActive;

                pm[2].direction = ParameterDirection.Input;
                pm[2].ParamName = "@comIsDeleted";
                pm[2].DataType = DbType.Boolean;
                pm[2].value = _comIsDeleted;

                _ds = DBAccess.ExecDataSet("CompetenceMasterUpdate", CommandType.StoredProcedure, pm);
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in CompetenceMasterUpdate  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            }
        }

        public void GetAllcompetenceCategory()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@catIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _catIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@catIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _catIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@catCompanyId";
                param[2].DataType = DbType.Int32;
                param[2].value = _catCompanyId;

                _ds = DBAccess.ExecDataSet("GetAllcompetenceCategory", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllcompetenceCategory" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public int InsertCompetenceMasteAdd()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[10];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue;

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@comUserId";
                param[0].DataType = DbType.Int32;
                if (_comUserId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[0].value = _comUserId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@comLevel";
                param[1].DataType = DbType.String;
                if (String.IsNullOrEmpty(_comLevel))
                    param[1].value = DBNull.Value;
                else
                    param[1].value = _comLevel;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@comCompetence";
                param[2].DataType = DbType.String;
                if (String.IsNullOrEmpty(_comCompetence))
                    param[2].value = DBNull.Value;
                else
                    param[2].value = _comCompetence;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@comCompetenceDN";
                param[3].DataType = DbType.String;
                if (String.IsNullOrEmpty(_comCompetenceDN))
                    param[3].value = DBNull.Value;
                else
                    param[3].value = _comCompetenceDN;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@comCreatedDate";
                param[4].DataType = DbType.DateTime;
                if (_comCreatedDate.Ticks > dtmin.Ticks && _comCreatedDate.Ticks < dtmax.Ticks)
                    param[4].value = _comCreatedDate;
                else if (_comCreatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[4].value = _comCreatedDate;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@comUpdatedDate";
                param[5].DataType = DbType.DateTime;
                if (_comUpdatedDate.Ticks > dtmin.Ticks && _comUpdatedDate.Ticks < dtmax.Ticks)
                    param[5].value = _comUpdatedDate;
                else if (_comUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[5].value = _comUpdatedDate;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@comIsDeleted";
                param[6].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_comIsDeleted.ToString()))
                    param[6].value = DBNull.Value;
                else
                    param[6].value = _comIsDeleted;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@comIsActive";
                param[7].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_comIsActive.ToString()))
                    param[7].value = DBNull.Value;
                else
                    param[7].value = _comIsActive;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@comCompanyId";
                param[8].DataType = DbType.Int32;
                param[8].value = _comCompanyId;

                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@comcategoryId";
                param[9].DataType = DbType.Int32;
                param[9].value = _comcategoryId;



                //obj.ExecScalar("InsertCompetenceMasteAdd", CommandType.StoredProcedure, param);				_returnBoolean = true;
                _ds = DBAccess.ExecDataSet("InsertCompetenceMasteAdd", CommandType.StoredProcedure, param);
                return Convert.ToInt32(_ds.Tables[0].Rows[0][0]);
                // _returnBoolean = true;
            }
            catch (DataException ex)
            {
                return 0;
                throw ex;
            }
        }
        public void GetAllCompetenceMasterAddforaverage(int usertyp, int userCreateBy,string ComIDs)
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[9];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@comIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _comIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@comIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _comIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@comCompanyId";
                param[2].DataType = DbType.Int32;
                param[2].value = _comCompanyId;


                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@usertype";
                param[3].DataType = DbType.Int32;
                param[3].value = usertyp;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@userCreateBy";
                param[4].DataType = DbType.Int32;
                param[4].value = userCreateBy;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@Jobid";
                param[5].DataType = DbType.Int32;
                param[5].value = _comJobtype;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@divid";
                param[6].DataType = DbType.Int32;
                param[6].value = _comDivId;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@TeamID";
                param[7].DataType = DbType.Int32;
                param[7].value = _TeamID;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@ComIDs";
                param[8].DataType = DbType.String;
                param[8].value = ComIDs;

                _ds = DBAccess.ExecDataSet("GetAllCompetenceMasterAddforaverage", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllCompetenceMasterAdd" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        public void InsertCompetencechild()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[7];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = Convert.ToDateTime("1/1/9999 11:59:59 PM");

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@comchildcomID";
                param[0].DataType = DbType.Int32;
                if (_comchildcomID == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[0].value = _comchildcomID;


                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@comchildlevel";
                param[1].DataType = DbType.String;
                if (String.IsNullOrEmpty(_comchildlevel))
                    param[1].value = DBNull.Value;
                else
                    param[1].value = _comchildlevel;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@comchildlevelDN";
                param[2].DataType = DbType.String;
                if (String.IsNullOrEmpty(_comchildlevelDN))
                    param[2].value = DBNull.Value;
                else
                    param[2].value = _comchildlevelDN;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@comCreatedDate";
                param[3].DataType = DbType.DateTime;
                if (_comCreatedDate.Ticks > dtmin.Ticks && _comCreatedDate.Ticks < dtmax.Ticks)
                    param[3].value = _comCreatedDate;
                else if (_comCreatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[3].value = _comCreatedDate;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@comUpdatedDate";
                param[4].DataType = DbType.DateTime;
                if (_comUpdatedDate.Ticks > dtmin.Ticks && _comUpdatedDate.Ticks < dtmax.Ticks)
                    param[4].value = _comUpdatedDate;
                else if (_comUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[4].value = _comUpdatedDate;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@comIsDeleted";
                param[5].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_comIsDeleted.ToString()))
                    param[5].value = DBNull.Value;
                else
                    param[5].value = _comIsDeleted;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@comIsActive";
                param[6].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_comIsActive.ToString()))
                    param[6].value = DBNull.Value;
                else
                    param[6].value = _comIsActive;


                //obj.ExecScalar("InsertCompetenceMasteAdd", CommandType.StoredProcedure, param);				_returnBoolean = true;
                _ds = DBAccess.ExecDataSet("InsertCompetencechild", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }
        public void GetAllCompetenceMasterAdd()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@comIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _comIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@comIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _comIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@comCompanyId";
                param[2].DataType = DbType.Int32;
                param[2].value = _comCompanyId;

                _ds = DBAccess.ExecDataSet("GetAllCompetenceMasterAdd", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllCompetenceMasterAdd" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        public void GetAllCompetenceAddbyid()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@comId";
                param[0].DataType = DbType.Int32;
                param[0].value = _comId;


                _ds = DBAccess.ExecDataSet("GetAllCompetenceAddbyid", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllCompetenceAddbyid" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void UpdateCompetenceAdd()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[5];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = Convert.ToDateTime("1/1/9999 11:59:59 PM");

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@comId";
                param[0].DataType = DbType.Int32;
                param[0].value = _comId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@comcategoryId";
                param[1].DataType = DbType.Int32;
                param[1].value = _comcategoryId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@comUpdatedDate";
                param[2].DataType = DbType.DateTime;
                if (_comUpdatedDate.Ticks > dtmin.Ticks && _comUpdatedDate.Ticks < dtmax.Ticks)
                    param[2].value = _comUpdatedDate;
                else if (_comUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[2].value = _comUpdatedDate;


                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@comCompetence";
                param[3].DataType = DbType.String;
                if (String.IsNullOrEmpty(_comCompetence))
                    param[3].value = DBNull.Value;
                else
                    param[3].value = _comCompetence;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@comCompetenceDN";
                param[4].DataType = DbType.String;
                if (String.IsNullOrEmpty(_comCompetenceDN))
                    param[4].value = DBNull.Value;
                else
                    param[4].value = _comCompetenceDN;



                _ds = DBAccess.ExecDataSet("UpdateCompetenceAdd", CommandType.StoredProcedure, param);
                _returnBoolean = true;
                // obj.ExecScalar("UpdateCompetence", CommandType.StoredProcedure, param); _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }
        public void DeleteCompetencechildbyId()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@comId";
                param[0].DataType = DbType.Int32;
                param[0].value = _comId;

                DBAccess.ExecDataSet("DeleteCompetencechildbyId", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch
            {
                _returnBoolean = false;
            }
        }
        public void CompetenceMasterAddUpdate()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] pm = new ParamStruct[3];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@comId";
                pm[0].DataType = DbType.Int32;
                pm[0].value = _comId;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@comIsActive";
                pm[1].DataType = DbType.Boolean;
                pm[1].value = _comIsActive;

                pm[2].direction = ParameterDirection.Input;
                pm[2].ParamName = "@comIsDeleted";
                pm[2].DataType = DbType.Boolean;
                pm[2].value = _comIsDeleted;

                _ds = DBAccess.ExecDataSet("CompetenceMasterAddUpdate", CommandType.StoredProcedure, pm);
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in CompetenceMasterAddUpdate  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            }
        }
        public void GetAllchildCompetenceskill()
        {

            //var cacheKey = string.Format("GetAllchildCompetenceskill-{0}-{1}-{2}-{3}-{4}", _comIsActive, _comIsDeleted, _comchildcomID, _comUserId, _comId);
            //var cacheItem = HttpRuntime.Cache.Get(cacheKey) as DataSet;
            try
            {
                //if (cacheItem == null)
                //{
                    DataAccess DBAccess = new DataAccess();
                    DBAccess.Provider = EnumProviders.SQLClient;
                    DBAccess.ConnectionString =
                        System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                    ParamStruct[] param = new ParamStruct[5];

                    param[0].direction = ParameterDirection.Input;
                    param[0].ParamName = "@comIsActive";
                    param[0].DataType = DbType.Boolean;
                    param[0].value = _comIsActive;

                    param[1].direction = ParameterDirection.Input;
                    param[1].ParamName = "@comIsDeleted";
                    param[1].DataType = DbType.Boolean;
                    param[1].value = _comIsDeleted;

                    param[2].direction = ParameterDirection.Input;
                    param[2].ParamName = "@comchildcomID";
                    param[2].DataType = DbType.Int32;
                    param[2].value = _comchildcomID;

                    param[3].direction = ParameterDirection.Input;
                    param[3].ParamName = "@comUserId";
                    param[3].DataType = DbType.Int32;
                    param[3].value = _comUserId;

                    param[4].direction = ParameterDirection.Input;
                    param[4].ParamName = "@comId";
                    param[4].DataType = DbType.Int32;
                    param[4].value = _comId;

                    _ds = DBAccess.ExecDataSet("GetAllchildCompetenceskill", CommandType.StoredProcedure, param);
                    //cacheItem = DBAccess.ExecDataSet("GetAllchildCompetenceskill", CommandType.StoredProcedure, param);
                    //CommonUtilities.GetConfigValue("CacheDuration");
                   // HttpRuntime.Cache.Insert(cacheKey, cacheItem, null, DateTime.Now.AddDays(Convert.ToInt32(CommonUtilities.GetConfigValue("CacheDuration","2"))), TimeSpan.Zero);
               // }

                //_ds = cacheItem;



            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllchildCompetenceskill" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void GetAllCategoryByJobTypeId_DivId()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[2];

                //param[0].direction = ParameterDirection.Input;
                //param[0].ParamName = "@comIsActive";
                //param[0].DataType = DbType.Boolean;
                //param[0].value = _comIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@jobtypeId";
                param[1].DataType = DbType.Int32;
                param[1].value = _comJobtype;

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@divId";
                param[0].DataType = DbType.Int32;
                param[0].value = _comDivId;

                //_ds = DBAccess.ExecDataSet("GetAllCategoryByJobTypeId_DivId1", CommandType.StoredProcedure, param);
                _ds = DBAccess.ExecDataSet("GetAllCategoryByJobTypeId_DivId2", CommandType.StoredProcedure, param);
            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllCompetenceMaster" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void GetAllCompetenceAddbyComCatId(int comCatId)
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[2];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@comCatId";
                param[0].DataType = DbType.Int32;
                param[0].value = comCatId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@comcompanyId";
                param[1].DataType = DbType.Int32;
                param[1].value = comCompanyId;

                _ds = DBAccess.ExecDataSet("GetAllCompetenceAddbyComCatId", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllCompetenceMaster" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        public void GetAllCategoryBypenddingrequest(int comUserId)
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@comUserId";
                param[0].DataType = DbType.Int32;
                param[0].value = comUserId;

                _ds = DBAccess.ExecDataSet("GetAllCategoryBypenddingrequest", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllCategoryBypenddingrequest" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        #endregion


        #region NewMethods
        public void GetAllCompetenceByCategoryID()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[2];

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@comcategoryId";
                param[1].DataType = DbType.Int32;
                param[1].value = _comcategoryId;

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@CompanyID";
                param[0].DataType = DbType.Int32;
                param[0].value = _comCompanyId;

                _ds = DBAccess.ExecDataSet("GetAllCompetenceByCategoryID", CommandType.StoredProcedure, param);
            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllCompetenceByCategoryID" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        #endregion
    }
}

