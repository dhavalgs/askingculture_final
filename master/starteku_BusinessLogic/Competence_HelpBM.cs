using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starteku.Data.Manager;
using System.Data;
using starteku_BusinessLogic;


namespace startetku.Business.Logic
{

    public class Competence_HelpBM
    {
        #region Private Declaration

        private DataSet _ds;
        private string _returnString;
        private bool _returnBoolean;
        private Int32 _helpId;
        private Int32 _helpUserIdHelper;
        private Int32 _helpUserIdHelped;
        private DateTime _helpstartDate;
        private DateTime _helpEndDate;
        private String _helpMessage;
        private DateTime _comCreatedDate;
        private DateTime _comUpdatedDate;
        private Boolean _comIsDeleted;
        private Boolean _comIsActive;
        private Int32 _comCompanyId;
        private Int32 _helpkillStatus;
        private Int32 _helpsubComId;
        private Boolean _helpstatus;
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public DataSet ds
        {
            get { return _ds; }
            set { _ds = value; }
        }
        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public string ReturnString
        {
            get { return _returnString; }
            set { _returnString = value; }
        }

        /// <summary>
        /// Gets or sets the ReturnBoolean value.
        /// </summary>
        public bool ReturnBoolean
        {
            get { return _returnBoolean; }
            set { _returnBoolean = value; }
        }

        /// <summary>
        /// Gets or sets the helpId value.
        /// </summary>
        public Int32 helpId
        {
            get { return _helpId; }
            set { _helpId = value; }
        }

        /// <summary>
        /// Gets or sets the helpUserIdHelper value.
        /// </summary>
        public Int32 helpUserIdHelper
        {
            get { return _helpUserIdHelper; }
            set { _helpUserIdHelper = value; }
        }

        /// <summary>
        /// Gets or sets the helpUserIdHelped value.
        /// </summary>
        public Int32 helpUserIdHelped
        {
            get { return _helpUserIdHelped; }
            set { _helpUserIdHelped = value; }
        }

        /// <summary>
        /// Gets or sets the helpstartDate value.
        /// </summary>
        public DateTime helpstartDate
        {
            get { return _helpstartDate; }
            set { _helpstartDate = value; }
        }

        /// <summary>
        /// Gets or sets the helpEndDate value.
        /// </summary>
        public DateTime helpEndDate
        {
            get { return _helpEndDate; }
            set { _helpEndDate = value; }
        }

        /// <summary>
        /// Gets or sets the helpMessage value.
        /// </summary>
        public String helpMessage
        {
            get { return _helpMessage; }
            set { _helpMessage = value; }
        }

        /// <summary>
        /// Gets or sets the comCreatedDate value.
        /// </summary>
        public DateTime comCreatedDate
        {
            get { return _comCreatedDate; }
            set { _comCreatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the comUpdatedDate value.
        /// </summary>
        public DateTime comUpdatedDate
        {
            get { return _comUpdatedDate; }
            set { _comUpdatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the comIsDeleted value.
        /// </summary>
        public Boolean comIsDeleted
        {
            get { return _comIsDeleted; }
            set { _comIsDeleted = value; }
        }

        /// <summary>
        /// Gets or sets the comIsActive value.
        /// </summary>
        public Boolean comIsActive
        {
            get { return _comIsActive; }
            set { _comIsActive = value; }
        }

        /// <summary>
        /// Gets or sets the comCompanyId value.
        /// </summary>
        public Int32 comCompanyId
        {
            get { return _comCompanyId; }
            set { _comCompanyId = value; }
        }
        public Int32 helpkillStatus
        {
            get { return _helpkillStatus; }
            set { _helpkillStatus = value; }
        }
        public Int32 helpsubComId
        {
            get { return _helpsubComId; }
            set { _helpsubComId = value; }
        }
        public Boolean helpstatus
        {
            get { return _helpstatus; }
            set { _helpstatus = value; }
        }
        #endregion

        #region Methods

        public void InsertCompetence_Help()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[13];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue; 

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@helpUserIdHelper";
                param[0].DataType = DbType.Int32;
                if (_helpUserIdHelper == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[0].value = _helpUserIdHelper;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@helpUserIdHelped";
                param[1].DataType = DbType.Int32;
                if (_helpUserIdHelped == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[1].value = _helpUserIdHelped;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@helpstartDate";
                param[2].DataType = DbType.DateTime;
                if (_helpstartDate.Ticks > dtmin.Ticks && _helpstartDate.Ticks < dtmax.Ticks)
                    param[2].value = _helpstartDate;
                else if (_helpstartDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[2].value = _helpstartDate;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@helpEndDate";
                param[3].DataType = DbType.DateTime;
                if (_helpEndDate.Ticks > dtmin.Ticks && _helpEndDate.Ticks < dtmax.Ticks)
                    param[3].value = _helpEndDate;
                else if (_helpEndDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[3].value = _helpEndDate;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@helpMessage";
                param[4].DataType = DbType.String;
                if (String.IsNullOrEmpty(_helpMessage))
                    param[4].value = DBNull.Value;
                else
                    param[4].value = _helpMessage;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@comCreatedDate";
                param[5].DataType = DbType.DateTime;
                if (_comCreatedDate.Ticks > dtmin.Ticks && _comCreatedDate.Ticks < dtmax.Ticks)
                    param[5].value = _comCreatedDate;
                else if (_comCreatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[5].value = _comCreatedDate;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@comUpdatedDate";
                param[6].DataType = DbType.DateTime;
                if (_comUpdatedDate.Ticks > dtmin.Ticks && _comUpdatedDate.Ticks < dtmax.Ticks)
                    param[6].value = _comUpdatedDate;
                else if (_comUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[6].value = _comUpdatedDate;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@comIsDeleted";
                param[7].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_comIsDeleted.ToString()))
                    param[7].value = DBNull.Value;
                else
                    param[7].value = _comIsDeleted;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@comIsActive";
                param[8].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_comIsActive.ToString()))
                    param[8].value = DBNull.Value;
                else
                    param[8].value = _comIsActive;

                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@comCompanyId";
                param[9].DataType = DbType.Int32;
                if (_comCompanyId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[9].value = _comCompanyId;

                param[10].direction = ParameterDirection.Input;
                param[10].ParamName = "@helpkillStatus";
                param[10].DataType = DbType.Int32;
                if (_comCompanyId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[10].value = _helpkillStatus;

                param[11].direction = ParameterDirection.Input;
                param[11].ParamName = "@helpsubComId";
                param[11].DataType = DbType.Int32;
                if (_comCompanyId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[11].value = _helpsubComId;

                param[12].direction = ParameterDirection.Input;
                param[12].ParamName = "@helpstatus";
                param[12].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_helpstatus.ToString()))
                    param[12].value = DBNull.Value;
                else
                    param[12].value = _helpstatus;

                _ds = DBAccess.ExecDataSet("InsertCompetence_Help", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        public void GetAllCompetencehelp()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@comIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _comIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@comIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _comIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@comCompanyId";
                param[2].DataType = DbType.Int32;
                param[2].value = _comCompanyId;

                _ds = DBAccess.ExecDataSet("GetAllCompetencehelp", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllCompetencehelp" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
    
        public void UpdateCompetence_Help()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[11];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue; 

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@helpId";
                param[0].DataType = DbType.Int32;
                param[0].value = _helpId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@helpUserIdHelper";
                param[1].DataType = DbType.Int32;
                if (_helpUserIdHelper == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[1].value = _helpUserIdHelper;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@helpUserIdHelped";
                param[2].DataType = DbType.Int32;
                if (_helpUserIdHelped == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[2].value = _helpUserIdHelped;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@helpstartDate";
                param[3].DataType = DbType.DateTime;
                if (_helpstartDate.Ticks > dtmin.Ticks && _helpstartDate.Ticks < dtmax.Ticks)
                    param[3].value = _helpstartDate;
                else if (_helpstartDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[3].value = _helpstartDate;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@helpEndDate";
                param[4].DataType = DbType.DateTime;
                if (_helpEndDate.Ticks > dtmin.Ticks && _helpEndDate.Ticks < dtmax.Ticks)
                    param[4].value = _helpEndDate;
                else if (_helpEndDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[4].value = _helpEndDate;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@helpMessage";
                param[5].DataType = DbType.String;
                if (String.IsNullOrEmpty(_helpMessage))
                    param[5].value = DBNull.Value;
                else
                    param[5].value = _helpMessage;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@comCreatedDate";
                param[6].DataType = DbType.DateTime;
                if (_comCreatedDate.Ticks > dtmin.Ticks && _comCreatedDate.Ticks < dtmax.Ticks)
                    param[6].value = _comCreatedDate;
                else if (_comCreatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[6].value = _comCreatedDate;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@comUpdatedDate";
                param[7].DataType = DbType.DateTime;
                if (_comUpdatedDate.Ticks > dtmin.Ticks && _comUpdatedDate.Ticks < dtmax.Ticks)
                    param[7].value = _comUpdatedDate;
                else if (_comUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[7].value = _comUpdatedDate;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@comIsDeleted";
                param[8].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_comIsDeleted.ToString()))
                    param[8].value = DBNull.Value;
                else
                    param[8].value = _comIsDeleted;

                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@comIsActive";
                param[9].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_comIsActive.ToString()))
                    param[9].value = DBNull.Value;
                else
                    param[9].value = _comIsActive;

                param[10].direction = ParameterDirection.Input;
                param[10].ParamName = "@comCompanyId";
                param[10].DataType = DbType.Int32;
                if (_comCompanyId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[10].value = _comCompanyId;

               // obj.ExecScalar("UpdateCompetence_Help", CommandType.StoredProcedure, param); _returnBoolean = true;
                _ds = DBAccess.ExecDataSet("UpdateCompetence_Help", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }


        public void GetAllPendingHelpRequest()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[4];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@comIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _comIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@comIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _comIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@helpkillStatus";
                param[2].DataType = DbType.Int32;
                param[2].value = _helpkillStatus;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@comCompanyId";
                param[3].DataType = DbType.Int32;
                param[3].value = _comCompanyId;


                _ds = DBAccess.ExecDataSet("GetAllPendingHelpRequest", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllPendingHelpRequest" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void RequestStatusHelpUpdate()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] pm = new ParamStruct[3];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@helpId";
                pm[0].DataType = DbType.Int32;
                pm[0].value = _helpId;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@comIsActive";
                pm[1].DataType = DbType.Boolean;
                pm[1].value = _comIsActive;

                pm[2].direction = ParameterDirection.Input;
                pm[2].ParamName = "@comIsDeleted";
                pm[2].DataType = DbType.Boolean;
                pm[2].value = _comIsDeleted;

                _ds = DBAccess.ExecDataSet("RequestStatusUpdate", CommandType.StoredProcedure, pm);
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in RequestStatusUpdate  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            }
        }

        public void GetAllPendingHelpRequestbyid()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[1];


                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@helpId";
                param[0].DataType = DbType.Int32;
                param[0].value = _helpId;


                _ds = DBAccess.ExecDataSet("GetAllPendingHelpRequestbyid", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllPendingHelpRequestbyid" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void UpdateendinghelpRequest()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[2];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue; 

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@helpId";
                param[0].DataType = DbType.Int32;
                param[0].value = _helpId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@helpkillStatus";
                param[1].DataType = DbType.Int32;
                param[1].value = _helpkillStatus;

                _ds = DBAccess.ExecDataSet("UpdateendinghelpRequest", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }
        #endregion

    }
}
