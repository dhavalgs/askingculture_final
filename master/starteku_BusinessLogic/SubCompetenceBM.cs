using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starteku.Data.Manager;
using System.Data;
using System.Web;
using starteku_BusinessLogic;

namespace startetku.Business.Logic
{

    public class SubCompetenceBM
    {

        #region Private Declaration

        private DataSet _ds;
        private string _returnString;
        private bool _returnBoolean;
        private Int32 _subId;
        private Int32 _subUserId;
        private Int32 _subComId;
        private String _subCompetence;
        private DateTime _subCreatedDate;
        private DateTime _subUpdatedDate;
        private Boolean _subIsDeleted;
        private Boolean _subIsActive;
        private Int32 _subCreateBy;
        private String _subDescription;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public DataSet ds
        {
            get { return _ds; }
            set { _ds = value; }
        }
        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public string ReturnString
        {
            get { return _returnString; }
            set { _returnString = value; }
        }

        /// <summary>
        /// Gets or sets the ReturnBoolean value.
        /// </summary>
        public bool ReturnBoolean
        {
            get { return _returnBoolean; }
            set { _returnBoolean = value; }
        }

        /// <summary>
        /// Gets or sets the subId value.
        /// </summary>
        public Int32 subId
        {
            get { return _subId; }
            set { _subId = value; }
        }

        /// <summary>
        /// Gets or sets the subUserId value.
        /// </summary>
        public Int32 subUserId
        {
            get { return _subUserId; }
            set { _subUserId = value; }
        }

        /// <summary>
        /// Gets or sets the subComId value.
        /// </summary>
        public Int32 subComId
        {
            get { return _subComId; }
            set { _subComId = value; }
        }

        /// <summary>
        /// Gets or sets the subCompetence value.
        /// </summary>
        public String subCompetence
        {
            get { return _subCompetence; }
            set { _subCompetence = value; }
        }

        /// <summary>
        /// Gets or sets the subCreatedDate value.
        /// </summary>
        public DateTime subCreatedDate
        {
            get { return _subCreatedDate; }
            set { _subCreatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the subUpdatedDate value.
        /// </summary>
        public DateTime subUpdatedDate
        {
            get { return _subUpdatedDate; }
            set { _subUpdatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the subIsDeleted value.
        /// </summary>
        public Boolean subIsDeleted
        {
            get { return _subIsDeleted; }
            set { _subIsDeleted = value; }
        }

        /// <summary>
        /// Gets or sets the subIsActive value.
        /// </summary>
        public Boolean subIsActive
        {
            get { return _subIsActive; }
            set { _subIsActive = value; }
        }

        /// <summary>
        /// Gets or sets the subCreateBy value.
        /// </summary>
        public Int32 subCreateBy
        {
            get { return _subCreateBy; }
            set { _subCreateBy = value; }
        }

        /// <summary>
        /// Gets or sets the subDescription value.
        /// </summary>
        public String subDescription
        {
            get { return _subDescription; }
            set { _subDescription = value; }
        }

        #endregion

        #region Methods



        public void InsertSubCompetence()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[9];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue;

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@subUserId";
                param[0].DataType = DbType.Int32;
                if (_subUserId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[0].value = _subUserId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@subComId";
                param[1].DataType = DbType.Int32;
                if (_subComId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[1].value = _subComId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@subCompetence";
                param[2].DataType = DbType.String;
                if (String.IsNullOrEmpty(_subCompetence))
                    param[2].value = DBNull.Value;
                else
                    param[2].value = _subCompetence;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@subCreatedDate";
                param[3].DataType = DbType.DateTime;
                if (_subCreatedDate.Ticks > dtmin.Ticks && _subCreatedDate.Ticks < dtmax.Ticks)
                    param[3].value = _subCreatedDate;
                else if (_subCreatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[3].value = _subCreatedDate;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@subUpdatedDate";
                param[4].DataType = DbType.DateTime;
                if (_subUpdatedDate.Ticks > dtmin.Ticks && _subUpdatedDate.Ticks < dtmax.Ticks)
                    param[4].value = _subUpdatedDate;
                else if (_subUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[4].value = _subUpdatedDate;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@subIsDeleted";
                param[5].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_subIsDeleted.ToString()))
                    param[5].value = DBNull.Value;
                else
                    param[5].value = _subIsDeleted;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@subIsActive";
                param[6].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_subIsActive.ToString()))
                    param[6].value = DBNull.Value;
                else
                    param[6].value = _subIsActive;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@subCreateBy";
                param[7].DataType = DbType.Int32;
                if (_subCreateBy == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[7].value = _subCreateBy;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@subDescription";
                param[8].DataType = DbType.String;
                if (String.IsNullOrEmpty(_subDescription))
                    param[8].value = DBNull.Value;
                else
                    param[8].value = _subDescription;

                _ds = DBAccess.ExecDataSet("InsertSubCompetence", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        /// <summary>
        /// Inserts Records in the SubCompetence table.
        /// </summary>
        public void UpdateSubCompetence()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[4];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue;

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@subId";
                param[0].DataType = DbType.Int32;
                param[0].value = _subId;


                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@subCompetence";
                param[1].DataType = DbType.String;
                if (String.IsNullOrEmpty(_subCompetence))
                    param[1].value = DBNull.Value;
                else
                    param[1].value = _subCompetence;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@subUpdatedDate";
                param[2].DataType = DbType.DateTime;
                if (_subUpdatedDate.Ticks > dtmin.Ticks && _subUpdatedDate.Ticks < dtmax.Ticks)
                    param[2].value = _subUpdatedDate;
                else if (_subUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[2].value = _subUpdatedDate;


                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@subDescription";
                param[3].DataType = DbType.String;
                if (String.IsNullOrEmpty(_subDescription))
                    param[3].value = DBNull.Value;
                else
                    param[3].value = _subDescription;

                _ds = DBAccess.ExecDataSet("UpdateSubCompetence", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        public void GetAllSubCompetenceMaster()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@subIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _subIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@subIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _subIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@subComId";
                param[2].DataType = DbType.Int32;
                param[2].value = _subComId;

                _ds = DBAccess.ExecDataSet("GetAllSubCompetenceMaster", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllSubCompetenceMaster" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void subCompetenceMasterUpdate()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] pm = new ParamStruct[3];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@subId";
                pm[0].DataType = DbType.Int32;
                pm[0].value = _subId;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@subIsActive";
                pm[1].DataType = DbType.Boolean;
                pm[1].value = _subIsActive;

                pm[2].direction = ParameterDirection.Input;
                pm[2].ParamName = "@subIsDeleted";
                pm[2].DataType = DbType.Boolean;
                pm[2].value = _subIsDeleted;

                _ds = DBAccess.ExecDataSet("subCompetenceMasterUpdate", CommandType.StoredProcedure, pm);
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in subCompetenceMasterUpdate  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            }
        }
        public void GetAllsubCompetenceMasterbyid()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@subId";
                param[0].DataType = DbType.Int32;
                param[0].value = _subId;

                _ds = DBAccess.ExecDataSet("GetAllsubCompetenceMasterbyid", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllsubCompetenceMasterbyid" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        //saurin : 20141215
        public void GetAllCompetenceSkills(int userId)
        {
            //var cacheKey = string.Format("GetAllCompetenceSkills-{0}-{1}-{2}-{3}-{4}", userId, 1, 1, 2, 3);
            //var cacheItem = HttpRuntime.Cache.Get(cacheKey) as DataSet;
            try
            {

                DataAccess DBAccess = GetDbAccess();

                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@userId";
                param[0].DataType = DbType.Int32;
                param[0].value = userId;

                _ds = DBAccess.ExecDataSet("GetCompotenceAddRecordsByDivId_Jobtype", CommandType.StoredProcedure, param);
                //cacheItem = DBAccess.ExecDataSet("GetCompotenceAddRecordsByDivId_Jobtype1", CommandType.StoredProcedure, param);
                //HttpRuntime.Cache.Insert(cacheKey, cacheItem, null, DateTime.Now.AddDays(Convert.ToInt32(CommonUtilities.GetConfigValue("CacheDuration", "2"))), TimeSpan.Zero);



            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllCompetenceCategory" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
            //_ds = cacheItem;

        }
        //
        public void GetPDPpoint(int userId)
        {
            try
            {
                DataAccess DBAccess = GetDbAccess();

                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@userId";
                param[0].DataType = DbType.Int32;
                param[0].value = userId;

                _ds = DBAccess.ExecDataSet("GetPDPpoint", CommandType.StoredProcedure, param);
            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetPDPpoint" + " error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }

        }
        public void GetPDPpointByManagerId(int userId,int TeamID)
        {
            try
            {
                DataAccess DBAccess = GetDbAccess();

                ParamStruct[] param = new ParamStruct[2];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@userId";
                param[0].DataType = DbType.String;
                param[0].value = Convert.ToString(userId);

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@TeamID";
                param[1].DataType = DbType.Int32;
                param[1].value = Convert.ToInt32(TeamID);

                _ds = DBAccess.ExecDataSet("GetPDPpointByManagerId", CommandType.StoredProcedure, param);
            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetPDPpointByManagerId" + " error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void GetCompotenceAddRecordsByDivId_JobtypebyAccordion(int userId, Int32 comcategoryId)
        {
            try
            {
                DataAccess DBAccess = GetDbAccess();

                ParamStruct[] param = new ParamStruct[2];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@userId";
                param[0].DataType = DbType.Int32;
                param[0].value = userId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@comcategoryId";
                param[1].DataType = DbType.Int32;
                param[1].value = comcategoryId;

                //_ds = DBAccess.ExecDataSet("GetCompotenceAddRecordsByDivId_Jobtype", CommandType.StoredProcedure, param);
                _ds = DBAccess.ExecDataSet("GetCompotenceAddRecordsByDivId_JobtypebyAccordion", CommandType.StoredProcedure, param);
            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetCompotenceAddRecordsByDivId_JobtypebyAccordion" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }

        }
        public void GetCompotenceAddForDeshbord(int userId)
        {
            try
            {
                DataAccess DBAccess = GetDbAccess();

                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@userId";
                param[0].DataType = DbType.Int32;
                param[0].value = userId;

                //_ds = DBAccess.ExecDataSet("GetCompotenceAddRecordsByDivId_Jobtype", CommandType.StoredProcedure, param);
                _ds = DBAccess.ExecDataSet("GetCompotenceAddForDeshbord", CommandType.StoredProcedure, param);
            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetCompotenceAddForDeshbord" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }

        }
        public void GetCompotenceAddForDeshbordbyAccordion(int userId, int comcategoryId)
        {
            try
            {
                DataAccess DBAccess = GetDbAccess();

                ParamStruct[] param = new ParamStruct[2];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@userId";
                param[0].DataType = DbType.Int32;
                param[0].value = userId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@comcategoryId";
                param[1].DataType = DbType.Int32;
                param[1].value = comcategoryId;

                //_ds = DBAccess.ExecDataSet("GetCompotenceAddRecordsByDivId_Jobtype", CommandType.StoredProcedure, param);
                _ds = DBAccess.ExecDataSet("GetCompotenceAddForDeshbordbyAccordion", CommandType.StoredProcedure, param);
            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetCompotenceAddForDeshbordbyAccordion" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }

        }

        public void GetPDPpointByUserIdByMonth(int userId)
        {
            try
            {
                DataAccess DBAccess = GetDbAccess();

                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@userId";
                param[0].DataType = DbType.String;
                param[0].value = Convert.ToString(userId);

                _ds = DBAccess.ExecDataSet("GetPDPpointByUserIdByMonth", CommandType.StoredProcedure, param);
            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetPDPpointByManagerId" + " error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        #endregion


        #region CommonFunction
        //saurin : 20141215
        public DataAccess GetDbAccess()
        {
            DataAccess DBAccess = new DataAccess();
            DBAccess.Provider = EnumProviders.SQLClient;
            DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
            return DBAccess;
        }


        #endregion

    }
}
