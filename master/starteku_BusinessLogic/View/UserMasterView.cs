﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace starteku_BusinessLogic.View
{
    public class UserMasterView
    {
        public int UserId { get; set; }

        public string UserFirstName { get; set; }

        public string Address { get; set; }

        public string Contact { get; set; }
        

        public string UserLastName { get; set; }

        public string UserGender { get; set; }

        public string UserZip { get; set; }

        public string UserEmail { get; set; }

        public int UserType { get; set; }

        public bool UserIsDeleted { get; set; }

        public int UserJobType { get; set; }

        public int UserCompanyId { get; set; }

        public string UserCategory { get; set; }

        public DateTime? UserLastLoginDateTime { get; set; }

        public int UserLevel
        { get; set; }
    }
}
