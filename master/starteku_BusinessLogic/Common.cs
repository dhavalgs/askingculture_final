﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.IO;
using System.Configuration;
using System.Data;
using System.Web;
using starteku_BusinessLogic.Model;


namespace starteku_BusinessLogic
{
    public class Common
    {
        public static string DbCookie(string cookieName, string cookieVal, DateTime expireDateTime)
        {
            var db = new startetkuEntities1();
            try
            {
                var getCookie = db.CookieMasters.FirstOrDefault(o => o.CookieName == cookieName && o.CookieCreatedDateTime.Date == DateTime.Now.Date);
                if (getCookie != null)
                {
                    return getCookie.CookieValue;
                }
                else
                {
                    getCookie = new CookieMaster();
                    getCookie.CookieCreatedDateTime = DateTime.Now;
                    getCookie.CookieValue = cookieVal;
                    getCookie.CookieUpdateDateTime = DateTime.Now;
                    getCookie.CookieStatus = "";
                    getCookie.CookieName = cookieName;
                    db.CookieMasters.Add(getCookie);
                    db.SaveChanges();
                }

            }
            catch (Exception ex)
            {


                ExceptionLogger.LogException(ex);

            }
            finally
            {
                db.Dispose();

            }
            return String.Empty;
        }
        public static void WriteLog(string functionName)
        {
            //string writelogtest = Convert.ToString(ConfigurationSettings.AppSettings["writelogstatus"]).ToLower();
            string writelogtest = "true";
            if (!string.IsNullOrEmpty(writelogtest))
            {
                if (writelogtest == "true")
                {
                    string fileName = DateTime.Today.ToString("dd_MMM_yyyy");
                    string path = ConfigurationSettings.AppSettings["logpath"].ToString() + fileName + ".txt";
                    // string path = "c:/inetpub/wwwroot/starteku/Log/" + fileName + ".txt";
                    if (!System.IO.File.Exists(path))
                    {
                        using (FileStream fs = File.Create(path))
                        {
                            Byte[] info = new UTF8Encoding(true).GetBytes("");
                            fs.Write(info, 0, 0);
                        }
                    }
                    TextWriter tw1 = new StreamWriter(path, true);
                    tw1.WriteLine(functionName);
                    tw1.Close();
                    tw1.Dispose();
                }
            }
        }


        public static string CreateTextFile(string functionName)
        {
            //string writelogtest = Convert.ToString(ConfigurationSettings.AppSettings["writelogstatus"]).ToLower();

            string fileName = "";

            fileName = DateTime.Now.ToString("ddMMyyyyhhmmmss") + ".txt";
            string path = ConfigurationSettings.AppSettings["pdpHistorypath"].ToString() + fileName;
            // string path = "c:/inetpub/wwwroot/starteku/Log/" + fileName + ".txt";
            if (!System.IO.File.Exists(path))
            {
                using (FileStream fs = File.Create(path))
                {
                    Byte[] info = new UTF8Encoding(true).GetBytes("");
                    fs.Write(info, 0, 0);
                }
            }
            TextWriter tw1 = new StreamWriter(path, true);
            tw1.WriteLine(functionName);
            tw1.Close();
            tw1.Dispose();

            return fileName;
        }

        public static string ReadFile(string fileName)
        {
            var returnString = "";

            try
            {
                var path = ConfigurationSettings.AppSettings["pdpHistorypath"].ToString();
                fileName = path + "\\" + fileName;
                using (StreamReader stringRead = new StreamReader(fileName))
                {
                    returnString = stringRead.ReadToEnd();
                }

            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex);
                returnString = "";
            }
            return returnString;

        }

        public static string CleanFileName(string fileName)
        {


            var s =
                fileName.Replace(' ', '_')
                        .Replace('^', '_')
                        .Replace('%', '_')
                        .Replace('*', '_')
                        .Replace('(', '_')
                        .Replace(')', '_')
                        .Replace('{', '_')
                        .Replace('}', '_')
                        .Replace('@', '_')
                        .Replace('$', '_')
                        .Replace('+', '_')
                        .Replace('-', '_')
                        .Replace('`', '_')
                        .Replace('?', '_')
                        .Replace('&', '_')
                        .Replace('#', '_');

            return s;
        }
        public static bool IsDatasetvalid(DataSet ds)
        {
            if (ds != null)
            {
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public static object GetCurrentDatetime()
        {
            return DateTime.Now;
        }

        public static string GetVisitorIPAddress(bool GetLan = false)
        {

            string visitorIPAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (String.IsNullOrEmpty(visitorIPAddress))
                visitorIPAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

            if (string.IsNullOrEmpty(visitorIPAddress))
                visitorIPAddress = HttpContext.Current.Request.UserHostAddress;

            if (string.IsNullOrEmpty(visitorIPAddress) || visitorIPAddress.Trim() == "::1")
            {
                GetLan = true;
                visitorIPAddress = string.Empty;
            }

            //if (GetLan)
            //{
            //    if (string.IsNullOrEmpty(visitorIPAddress))
            //    {
            //        //This is for Local(LAN) Connected ID Address
            //        string stringHostName = Dns.GetHostName();
            //        //Get Ip Host Entry
            //        IPHostEntry ipHostEntries = Dns.GetHostEntry(stringHostName);
            //        //Get Ip Address From The Ip Host Entry Address List
            //        IPAddress[] arrIpAddress = ipHostEntries.AddressList;

            //        try
            //        {
            //            visitorIPAddress = arrIpAddress[arrIpAddress.Length - 2].ToString();
            //        }
            //        catch
            //        {
            //            try
            //            {
            //                visitorIPAddress = arrIpAddress[0].ToString();
            //            }
            //            catch
            //            {
            //                try
            //                {
            //                    arrIpAddress = Dns.GetHostAddresses(stringHostName);
            //                    visitorIPAddress = arrIpAddress[0].ToString();
            //                }
            //                catch
            //                {
            //                    visitorIPAddress = "127.0.0.1";
            //                }
            //            }
            //        }
            //    }
            //}


            return visitorIPAddress;
        }

        public static string GetWebBrowserName()
        {

            string WebBrowserName = string.Empty;
            try
            {
                WebBrowserName = HttpContext.Current.Request.Browser.Browser + " " + HttpContext.Current.Request.Browser.Version;
            }
            catch (Exception ex)
            {

            }
            return WebBrowserName;
        }


        #region CommonGlobalstring

        public static string ALPPointForReadDoc()
        {
            return "ReadDoc";
        }


        public static string PLPPointForReadDocByOther()
        {
            return "ReadDocByOther";
        }

        public static string PublicDocUpload()
        {
            return "UploadPublicDoc";
        }

        public static string PointForSignIn()
        {
            return "SignIn";
        }
        public static string PLP()
        {
            return "PLP";
        }
        public static string ALP()
        {
            return "ALP";
        }
        #endregion

        public static string RequestSession()
        {
            return "RequestSession";
        }
        public static string SuggestDoc()
        {
            return "ReadDocByOther";
        }

        public static string SuggestSession()
        {
            return "SuggestSession";
        }

        public static string AcceptProvidedHelp()
        {
            return "AcceptProvidedHelp";
        }

        public static string OpenCompetencesScreen()
        {
            return "OpenCompetencesScreen";
        }

        public static string FinalFillCompetences()
        {
            return "FinalFillCompetences";
        }

        public static string ReadDocByOther()
        {
            return "ReadDocByOther";
        }
    }
}
