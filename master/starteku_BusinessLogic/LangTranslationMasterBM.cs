﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starteku.Data.Manager;
using System.Data;

namespace starteku_BusinessLogic
{
    public class LangTranslationMasterBM
    {
        #region Private Declaration

        private DataSet _ds;
        private int _LangID;
        private string _LangType;
        private int _LangActCatId;
        private int _ResLangID;
        private string _LangText;
        private int _LangCompanyID;
       
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public DataSet ds
        {
            get { return _ds; }
            set { _ds = value; }
        }
        public int LangID
        {
            get { return _LangID; }
            set { _LangID = value; }
        }
        public string LangType
        {
            get { return _LangType; }
            set { _LangType = value; }
        }
        public int LangActCatId
        {
            get { return _LangActCatId; }
            set { _LangActCatId = value; }
        }
        public int ResLangID
        {
            get { return _ResLangID; }
            set { _ResLangID = value; }
        }
        public string LangText
        {
            get { return _LangText; }
            set { _LangText = value; }
        }
        public int LangCompanyID
        {
            get { return _LangCompanyID; }
            set { _LangCompanyID = value; }
        }
        #endregion

        #region Method
        public DataSet GetAllLangResource()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[2];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@LangCompanyID";
                param[0].DataType = DbType.Int32;
                param[0].value = _LangCompanyID;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@ActCatID";
                param[1].DataType = DbType.Int32;
                param[1].value = _LangActCatId;

                _ds = DBAccess.ExecDataSet("GetAllLangResource", CommandType.StoredProcedure, param);
                return _ds;
            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllLangResource" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void InsertLangResource()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[5];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@LangType";
                param[0].DataType = DbType.String;
                param[0].value = _LangType;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@LangActCatId";
                param[1].DataType = DbType.Int32;
                param[1].value = _LangActCatId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@ResLangID";
                param[2].DataType = DbType.Int32;
                param[2].value = _ResLangID;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@LangText";
                param[3].DataType = DbType.String;
                param[3].value = _LangText;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@LangCompanyID";
                param[4].DataType = DbType.Int32;
                param[4].value = _LangCompanyID;



                _ds = DBAccess.ExecDataSet("InsertLangResource", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                ExceptionLogger.LogException(ex);
            }
        }
        #endregion

    }
}
