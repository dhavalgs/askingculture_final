﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using starteku.Data.Manager;

namespace starteku_BusinessLogic
{
    public class LogMasterLogic
    {
        #region Public Property

        private int _logId;
        private DateTime _logCreateDate;
        private int _logUserId;
        private DateTime _logUpdateDate;
        private int _logALP;
        private int _logPLP;
        private string _logIp;
        private DateTime? _logOutTime;
        private DataSet _ds;

        private string _returnString;
        private bool _returnBoolean;
        private bool _logIsActive;
        private bool _logIsDeleted;
        private string _logBrowser;
        private int _logCompanyId;
        private int _logUserType;
        private string _logDescription;
        private string _logPointInfo;
        private int _logToUserId;
        private int _logdocId;

        private int _TeamID;
     

        #endregion

        #region Properties

        public int LogdocId
        {
            get { return _logdocId; }
            set { _logdocId = value; }
        }

        public int LogToUserId
        {
            get { return _logToUserId; }
            set { _logToUserId = value; }
        }

        public string LogPointInfo
        {
            get { return _logPointInfo; }
            set { _logPointInfo = value; }
        }

        public string LogDescription
        {
            get { return _logDescription; }
            set { _logDescription = value; }
        }

        public int LogUserType
        {
            get { return _logUserType; }
            set { _logUserType = value; }
        }

        public int LogCompanyId
        {
            get { return _logCompanyId; }
            set { _logCompanyId = value; }
        }

        public string LogBrowser
        {
            get { return _logBrowser; }
            set { _logBrowser = value; }
        }


        public bool LogIsActive
        {
            get { return _logIsActive; }
            set { _logIsActive = value; }
        }
        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public DataSet ds
        {
            get { return _ds; }
            set { _ds = value; }
        }
        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public string ReturnString
        {
            get { return _returnString; }
            set { _returnString = value; }
        }
        public string LogIp
        {
            get { return _logIp; }
            set { _logIp = value; }
        }
        /// <summary>
        /// Gets or sets the ReturnBoolean value.
        /// </summary>
        public bool ReturnBoolean
        {
            get { return _returnBoolean; }
            set { _returnBoolean = value; }
        }

        /// <summary>
        /// Gets or sets the docId value.
        /// </summary>
        public Int32 LogPLP
        {
            get { return _logPLP; }
            set { _logPLP = value; }
        }



        public Int32 LogALP
        {
            get { return _logALP; }
            set { _logALP = value; }
        }
        /// <summary>
        /// Gets or sets the docTitle value.
        /// </summary>
        public int LogId
        {
            get { return _logId; }
            set { _logId = value; }
        }



        public int LogUserId
        {
            get { return _logUserId; }
            set { _logUserId = value; }
        }

        public int TeamID
        {
            get { return _TeamID; }
            set { _TeamID = value; }
        }

        /// <summary>
        /// Gets or sets the docDescription value.
        /// </summary>
        public DateTime LogCreateDate
        {
            get { return _logCreateDate; }
            set { _logCreateDate = value; }
        }

        public DateTime LogUpdateDate
        {
            get { return _logUpdateDate; }
            set { _logUpdateDate = value; }
        }

        public DateTime? LogOutTime
        {
            get { return _logOutTime; }
            set
            {
                if (value.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                {
                    _logOutTime = null;
                }
                else
                {
                    _logOutTime = value;
                }

            }
        }
        public bool LogIsDeleted
        {
            get { return _logIsDeleted; }
            set { _logIsDeleted = value; }
        }
        #endregion

        #region Methods
        //Saurin |20150102| 
        public static int InsertUpdateLogParam(int userId, string pointType, string pointName, string logDescription, bool isPointAllow,int toUser,int docId)
        {
            if (isPointAllow == false) return 0;
      

            var user = UserBM.GetUserById(Convert.ToInt32(userId)).FirstOrDefault();

            if (user == null) return -1; // something must be wrong. lets back , do not log it.

            LogMasterLogic obj = new LogMasterLogic();
            obj.LogIsActive = true;
            obj.LogIsDeleted = false;
            obj.LogCreateDate = DateTime.Now;
            obj.LogIp = Common.GetVisitorIPAddress(true);
            obj.LogUserId = userId;
            obj.LogCompanyId = user.UserCompanyId;
            obj.LogUserType = user.UserType;
            obj.LogToUserId = toUser;
            obj.LogdocId = docId;

            obj.LogDescription = logDescription;   // User log description..

            obj.LogOutTime = CommonUtilities.GetCurrentDateTime();
            obj.LogCreateDate = CommonUtilities.GetCurrentDateTime();
            obj.LogBrowser = Common.GetWebBrowserName();

            //
            //if (pointName == "SignIn")
            //{
            //    pointName = "Account";
            //}
            //else if (pointName == "ReadDoc")
            //{
            //    pointName = "Read Doc";
            //}
            //else if (pointName == "ReadDocByOther")
            //{
            //    pointName = "Read Doc Other";
            //}
            //else if (pointName == "UploadPublicDoc")
            //{
            //    pointName = "Upload Public Doc";
            //}
            //else if (pointName == "RequestSession")
            //{
            //    pointName = "Request Session";
            //}
            //else if (pointName == "SuggestSession")
            //{
            //    pointName = "Suggest Session";
            //}
            obj.LogPointInfo = pointName;
            
            // 
            var point = 0;
            if (obj.LogUserType != 1 && isPointAllow)  // we are not giving point other than employee, and point allow parameter should be passed true.
            {
                point = LogMasterLogic.GetPointByPointName(pointName,user.UserCompanyId);
                if (pointType == "PLP")
                {
                    obj.LogPLP = point;
                }
                else
                {
                    obj.LogALP = point;
                }

            }

            obj.InsertUpdateLog();

            return point; // this value will use to insert point into point table. // help to save one database call.

        }

        public void InsertUpdateLog()
        {

            DataAccess DBAccess = new DataAccess();
            DBAccess.Provider = EnumProviders.SQLClient;
            DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

            DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
            DateTime dtmax = Convert.ToDateTime("1/1/9999 11:59:59 PM");

            ParamStruct[] param = new ParamStruct[15];

            param[0].direction = ParameterDirection.Input;
            param[0].ParamName = "@logUserId";
            param[0].DataType = DbType.Int32;
            param[0].value = _logUserId;

            param[1].direction = ParameterDirection.Input;
            param[1].ParamName = "@logIsActive";
            param[1].DataType = DbType.Boolean;
            param[1].value = _logIsActive;


            param[2].direction = ParameterDirection.Input;
            param[2].ParamName = "@logCreateDate";
            param[2].DataType = DbType.DateTime;
            param[2].value = _logCreateDate;

            param[3].direction = ParameterDirection.Input;
            param[3].ParamName = "@logIsDeleted";
            param[3].DataType = DbType.Boolean;
            param[3].value = _logIsDeleted;



            param[4].direction = ParameterDirection.Input;
            param[4].ParamName = "@logPLP";
            param[4].DataType = DbType.Int32;
            param[4].value = _logPLP;

            param[5].direction = ParameterDirection.Input;
            param[5].ParamName = "@logALP";
            param[5].DataType = DbType.Int32;
            param[5].value = _logALP;

            param[6].direction = ParameterDirection.Input;
            param[6].ParamName = "@logIP";
            param[6].DataType = DbType.String;
            param[6].value = _logIp;

            param[7].direction = ParameterDirection.Input;
            param[7].ParamName = "@logOut";
            param[7].DataType = DbType.DateTime;
            param[7].value = _logOutTime;


            param[8].direction = ParameterDirection.Input;
            param[8].ParamName = "@logBrowser";
            param[8].DataType = DbType.String;
            param[8].value = _logBrowser;

            param[9].direction = ParameterDirection.Input;
            param[9].ParamName = "@logCompanyId";
            param[9].DataType = DbType.String;
            param[9].value = _logCompanyId;


            param[10].direction = ParameterDirection.Input;
            param[10].ParamName = "@logUserType";
            param[10].DataType = DbType.String;
            param[10].value = _logUserType;



            param[11].direction = ParameterDirection.Input;
            param[11].ParamName = "@logDescription";
            param[11].DataType = DbType.String;
            param[11].value = _logDescription;

            param[12].direction = ParameterDirection.Input;
            param[12].ParamName = "@logPointInfo";
            param[12].DataType = DbType.String;
            param[12].value = _logPointInfo;

            param[13].direction = ParameterDirection.Input;
            param[13].ParamName = "@logToUserId";
            param[13].DataType = DbType.Int32;
            param[13].value = _logToUserId;

            param[14].direction = ParameterDirection.Input;
            param[14].ParamName = "@logDocId";
            param[14].DataType = DbType.Int32;
            param[14].value = _logdocId;

            _ds = DBAccess.ExecDataSet("InsertUpdateLog", CommandType.StoredProcedure, param);

        }
        #endregion

        #region LogDescription Global text

        public string UserLogin()
        {
            return "User sign in";
        }
        #endregion

        // To get point of particualr point name
        public static int GetPointByPointName(string p,int companyId)
        {
            DataAccess DBAccess = new DataAccess();
            DBAccess.Provider = EnumProviders.SQLClient;
            DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

            ParamStruct[] param = new ParamStruct[4];

            param[0].direction = ParameterDirection.Input;
            param[0].ParamName = "@pointName";
            param[0].DataType = DbType.String;
            param[0].value = p;

            param[1].direction = ParameterDirection.Input;
            param[1].ParamName = "@paIsActive";
            param[1].DataType = DbType.Boolean;
            param[1].value = true;

            param[2].direction = ParameterDirection.Input;
            param[2].ParamName = "@paIsDeleted";
            param[2].DataType = DbType.Boolean;
            param[2].value = false;

            param[3].direction = ParameterDirection.Input;
            param[3].ParamName = "@paCompanyId";
            param[3].DataType = DbType.Int32;
            param[3].value = companyId;

            var ds = DBAccess.ExecDataSet("GetPointByPointName", CommandType.StoredProcedure, param);


            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    return Convert.ToInt32(ds.Tables[0].Rows[0]["paPoint"]);
                }

            }

            return 0;


        }

        //Saurin : Get data for chart by emp id ,total ALP , PLP

        public void GetTotalAlpPlpGroupByDateByUserId()
        {
            DataAccess DBAccess = new DataAccess();
            DBAccess.Provider = EnumProviders.SQLClient;
            DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

            ParamStruct[] param = new ParamStruct[4];

            param[0].direction = ParameterDirection.Input;
            param[0].ParamName = "@logIsActive";
            param[0].DataType = DbType.Boolean;
            param[0].value = _logIsActive;

            param[1].direction = ParameterDirection.Input;
            param[1].ParamName = "@logIsDeleted";
            param[1].DataType = DbType.Boolean;
            param[1].value = _logIsDeleted;

            param[2].direction = ParameterDirection.Input;
            param[2].ParamName = "@userId";
            param[2].DataType = DbType.Int32;
            param[2].value = _logUserId;

            param[3].direction = ParameterDirection.Input;
            param[3].ParamName = "@TeamID";
            param[3].DataType = DbType.Int32;
            param[3].value = _TeamID;

            _ds = DBAccess.ExecDataSet("GetTotalAlpPlpGroupByDateByUserId", CommandType.StoredProcedure, param);

        }

        public void GetAVGofAlpPlpGroupByDateByManagerId()
        {
            DataAccess DBAccess = new DataAccess();
            DBAccess.Provider = EnumProviders.SQLClient;
            DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

            ParamStruct[] param = new ParamStruct[4];

            param[0].direction = ParameterDirection.Input;
            param[0].ParamName = "@logIsActive";
            param[0].DataType = DbType.Boolean;
            param[0].value = _logIsActive;

            param[1].direction = ParameterDirection.Input;
            param[1].ParamName = "@logIsDeleted";
            param[1].DataType = DbType.Boolean;
            param[1].value = _logIsDeleted;

            param[2].direction = ParameterDirection.Input;
            param[2].ParamName = "@userId";
            param[2].DataType = DbType.Int32;
            param[2].value = _logUserId;

            param[3].direction = ParameterDirection.Input;
            param[3].ParamName = "@TeamID";
            param[3].DataType = DbType.Int32;
            param[3].value = _TeamID;

            _ds = DBAccess.ExecDataSet("GetAVGofAlpPlpGroupByDateByManagerId", CommandType.StoredProcedure, param);

        }


        public void GetTotalAlpPlpGroupByMonthByUserId()
        {
            DataAccess DBAccess = new DataAccess();
            DBAccess.Provider = EnumProviders.SQLClient;
            DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

            ParamStruct[] param = new ParamStruct[3];

            param[0].direction = ParameterDirection.Input;
            param[0].ParamName = "@logIsActive";
            param[0].DataType = DbType.Boolean;
            param[0].value = _logIsActive;

            param[1].direction = ParameterDirection.Input;
            param[1].ParamName = "@logIsDeleted";
            param[1].DataType = DbType.Boolean;
            param[1].value = _logIsDeleted;

            param[2].direction = ParameterDirection.Input;
            param[2].ParamName = "@userId";
            param[2].DataType = DbType.Int32;
            param[2].value = _logUserId;

            _ds = DBAccess.ExecDataSet("GetTotalAlpPlpGroupByMonthByUserId", CommandType.StoredProcedure, param);

        }

        public void GetPDPpointByUserIdByMonth(int userId)
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@userId";
                param[0].DataType = DbType.String;
                param[0].value = Convert.ToString(userId);

                _ds = DBAccess.ExecDataSet("GetPDPpointByUserIdByMonth", CommandType.StoredProcedure, param);
            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetPDPpointByManagerId" + " error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

       
    }
}
