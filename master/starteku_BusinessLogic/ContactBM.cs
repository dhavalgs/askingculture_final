﻿using starteku.Data.Manager;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace starteku_BusinessLogic
{
    public class ContactBM
    {

        #region Declare Private Variables

        private DataSet _ds;
        private string _returnString;
        private Boolean _returnBoolean;
        private int _ContactId;
        private int _UserId;
        private int _ContactUserId;
        private int _ContactCompanyId;
        private int _ContactJobId;
        private int _ContactLevel;
        private DateTime _ContactCreatedDate;
        private DateTime _ContactUpdatedDate;
        private Boolean _ContactIsActive;
        private Boolean _ContactIsDeleted;

        #endregion

        #region Properies

        public DataSet ds
        {
            get { return _ds; }
            set { _ds = value; }
        }
        public string returnString
        {
            get { return _returnString; }
            set { _returnString = value; }
        }
        public bool returnBoolean
        {
            get { return _returnBoolean; }
            set { _returnBoolean = value; }
        }

        public int ContactId
        {
            get { return _ContactId; }
            set { _ContactId = value; }
        }

        public int UserId
        {
            get { return _UserId; }
            set { _UserId = value; }
        }

        public int ContactUserId
        {
            get { return _ContactUserId; }
            set { _ContactUserId = value; }
        }

        public int ContactCompanyId
        {
            get { return _ContactCompanyId; }
            set { _ContactCompanyId = value; }
        }

        public int ContactJobId
        {
            get { return _ContactJobId; }
            set { _ContactJobId = value; }
        }

        public int ContactLevel
        {
            get { return _ContactLevel; }
            set { _ContactLevel = value; }
        }

        public DateTime ContactCreatedDate
        {
            get { return _ContactCreatedDate; }
            set { _ContactCreatedDate = value; }
        }

        public DateTime ContactUpdatedDate
        {
            get { return _ContactUpdatedDate; }
            set { _ContactUpdatedDate = value; }
        }

        public Boolean ContactIsActive
        {
            get { return _ContactIsActive; }
            set { _ContactIsActive = value; }
        }

        public Boolean ContactIsDeleted
        {
            get { return _ContactIsDeleted; }
            set { _ContactIsDeleted = value; }
        }

        #endregion

        #region Methods
        public Boolean InsertContact()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[9];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue; 

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@UserId";
                param[0].DataType = DbType.Int32;
                param[0].value = _UserId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@ContactUserId";
                param[1].DataType = DbType.Int32;
                param[1].value = _ContactUserId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@ContactCompanyId";
                param[2].DataType = DbType.Int32;
                param[2].value = _ContactCompanyId;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@ContactJobId";
                param[3].DataType = DbType.Int32;
                param[3].value = _ContactJobId;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@ContactLevel";
                param[4].DataType = DbType.Int32;
                param[4].value = _ContactLevel;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@ContactCreatedDate";
                param[5].DataType = DbType.DateTime;
                param[5].value = _ContactCreatedDate;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@ContactUpdatedDate";
                param[6].DataType = DbType.DateTime;
                if (_ContactUpdatedDate.Ticks > dtmin.Ticks && _ContactUpdatedDate.Ticks < dtmax.Ticks)
                    param[6].value = _ContactUpdatedDate;
                else if (_ContactUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[6].value = _ContactUpdatedDate;



                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@ContactIsActive";
                param[7].DataType = DbType.Boolean;
                param[7].value = _ContactIsActive;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@ContactIsDeleted";
                param[8].DataType = DbType.Boolean;
                param[8].value = _ContactIsDeleted;

                _ds = DBAccess.ExecDataSet("InsertContact", CommandType.StoredProcedure, param);
                if (Convert.ToBoolean(_ds.Tables[0].Rows[0]["status"]))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public void GetAllContact()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@UserId";
                param[0].DataType = DbType.Int32;
                param[0].value = _UserId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@ContactIsActive";
                param[1].DataType = DbType.Boolean;
                param[1].value = _ContactIsActive;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@ContactIsDeleted";
                param[2].DataType = DbType.Boolean;
                param[2].value = _ContactIsDeleted;


                _ds = DBAccess.ExecDataSet("GetAllContact", CommandType.StoredProcedure, param);

            }
            catch (Exception ex)
            {

            }
        }
        #endregion




    }
}
