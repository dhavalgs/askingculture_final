﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starteku.Data.Manager;
using System.Data;


namespace starteku_BusinessLogic
{
    public class ResourceLanguageBM
    {

        #region Private Declaration

        private DataSet _ds;
        private string _returnString;
        private string _LangType;
        private bool _returnBoolean;
        private int _resLangID;
        private string _resLanguage;
        private string _resExtension;
        private int _LangCompanyID;
        private int _LangActCatId;
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public DataSet ds
        {
            get { return _ds; }
            set { _ds = value; }
        }
        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public string ReturnString
        {
            get { return _returnString; }
            set { _returnString = value; }
        }
        public string LangType
        {
            get { return _LangType; }
            set { _LangType = value; }
        }
        /// <summary>
        /// Gets or sets the ReturnBoolean value.
        /// </summary>
        public bool ReturnBoolean
        {
            get { return _returnBoolean; }
            set { _returnBoolean = value; }
        }

        public int resLangID
        {
            get { return _resLangID; }
            set { _resLangID = value; }
        }

        public string resLanguage
        {
            get { return _resLanguage; }
            set { _resLanguage = value; }
        }

        public string resExtension
        {
            get { return _resExtension; }
            set { _resExtension = value; }
        }

        public int LangCompanyID
        {
            get { return _LangCompanyID; }
            set { _LangCompanyID = value; }
        }

        public int LangActCatId
        {
            get { return _LangActCatId; }
            set { _LangActCatId = value; }
        }
        #endregion


        #region Methods


        public DataSet GetAllResourceLanguage()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                _ds = DBAccess.ExecDataSet("GetAllResourceLanguage", CommandType.StoredProcedure);
                return _ds;
            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllResourceLanguage" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public DataSet GetAllResourceLangTran()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[3];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = Convert.ToDateTime("1/1/9999 11:59:59 PM");

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@LangCompanyID";
                param[0].DataType = DbType.Int32;
                param[0].value = _LangCompanyID;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@ActCatID";
                param[1].DataType = DbType.Int32;
                param[1].value = _LangActCatId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@LangType";
                param[2].DataType = DbType.String;
                param[2].value = _LangType;

                _ds = DBAccess.ExecDataSet("GetAllResourceLangTran", CommandType.StoredProcedure, param);
                return _ds;
            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllResourceLanguage" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public DataSet GetResourceLanguage(string language)
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[1];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = Convert.ToDateTime("1/1/9999 11:59:59 PM");

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@language";
                param[0].DataType = DbType.String;
                param[0].value = language;

                _ds = DBAccess.ExecDataSet("GetResourceLanguage", CommandType.StoredProcedure,param);
               
                return _ds;
            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetResourceLanguage" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        #endregion
    }
}
