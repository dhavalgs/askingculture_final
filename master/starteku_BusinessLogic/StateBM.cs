﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starteku.Data.Manager;
using System.Data;

namespace starteku_BusinessLogic
{
        public class StateBM
    {
        #region Private Declaration

        private DataSet _ds;
        private string _returnString;
        private bool _returnBoolean;
        private Int32 _staId;
        private String _staName;
        private Int32 _staCountryId;
        private DateTime _staCreatedDate;
        private DateTime _staUpdatedDate;
        private Boolean _staIsDeleted;
        private Boolean _staIsActive;
        private Int32 _staCompanyId;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public DataSet ds
        {
            get { return _ds; }
            set { _ds = value; }
        }
        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public string ReturnString
        {
            get { return _returnString; }
            set { _returnString = value; }
        }

        /// <summary>
        /// Gets or sets the ReturnBoolean value.
        /// </summary>
        public bool ReturnBoolean
        {
            get { return _returnBoolean; }
            set { _returnBoolean = value; }
        }

        /// <summary>
        /// Gets or sets the staId value.
        /// </summary>
        public Int32 staId
        {
            get { return _staId; }
            set { _staId = value; }
        }

        /// <summary>
        /// Gets or sets the staName value.
        /// </summary>
        public String staName
        {
            get { return _staName; }
            set { _staName = value; }
        }

        /// <summary>
        /// Gets or sets the staCountryId value.
        /// </summary>
        public Int32 staCountryId
        {
            get { return _staCountryId; }
            set { _staCountryId = value; }
        }

        /// <summary>
        /// Gets or sets the staCreatedDate value.
        /// </summary>
        public DateTime staCreatedDate
        {
            get { return _staCreatedDate; }
            set { _staCreatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the staUpdatedDate value.
        /// </summary>
        public DateTime staUpdatedDate
        {
            get { return _staUpdatedDate; }
            set { _staUpdatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the staIsDeleted value.
        /// </summary>
        public Boolean staIsDeleted
        {
            get { return _staIsDeleted; }
            set { _staIsDeleted = value; }
        }

        /// <summary>
        /// Gets or sets the staIsActive value.
        /// </summary>
        public Boolean staIsActive
        {
            get { return _staIsActive; }
            set { _staIsActive = value; }
        }

        /// <summary>
        /// Gets or sets the staCompanyId value.
        /// </summary>
        public Int32 staCompanyId
        {
            get { return _staCompanyId; }
            set { _staCompanyId = value; }
        }

        #endregion

        #region Method
        public void InsertState()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[7];

                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue; 

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@staName";
                param[0].DataType = DbType.String;
                if (String.IsNullOrEmpty(_staName))
                    param[0].value = DBNull.Value;
                else
                    param[0].value = _staName;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@staCountryId";
                param[1].DataType = DbType.Int32;                
                param[1].value = _staCountryId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@staCreatedDate";
                param[2].DataType = DbType.DateTime;
                if (_staCreatedDate.Ticks > dtmin.Ticks && _staCreatedDate.Ticks < dtmax.Ticks)
                    param[2].value = _staCreatedDate;
                else if (_staCreatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[2].value = _staCreatedDate;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@staUpdatedDate";
                param[3].DataType = DbType.DateTime;
                if (_staUpdatedDate.Ticks > dtmin.Ticks && _staUpdatedDate.Ticks < dtmax.Ticks)
                    param[3].value = _staUpdatedDate;
                else if (_staUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[3].value = _staUpdatedDate;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@staIsDeleted";
                param[4].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_staIsDeleted.ToString()))
                    param[4].value = DBNull.Value;
                else
                    param[4].value = _staIsDeleted;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@staIsActive";
                param[5].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_staIsActive.ToString()))
                    param[5].value = DBNull.Value;
                else
                    param[5].value = _staIsActive;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@staCompanyId";
                param[6].DataType = DbType.Int32;
                param[6].value = _staCompanyId;

             
                _ds = DBAccess.ExecDataSet("InsertState", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                Common.WriteLog("error in InsertState" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;
            }
        }
        public void InsertStateMaster()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[7];

                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue; 

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@staName";
                param[0].DataType = DbType.String;
                if (String.IsNullOrEmpty(_staName))
                    param[0].value = DBNull.Value;
                else
                    param[0].value = _staName;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@staCountryId";
                param[1].DataType = DbType.Int32;
                param[1].value = _staCountryId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@staCreatedDate";
                param[2].DataType = DbType.DateTime;
                if (_staCreatedDate.Ticks > dtmin.Ticks && _staCreatedDate.Ticks < dtmax.Ticks)
                    param[2].value = _staCreatedDate;
                else if (_staCreatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[2].value = _staCreatedDate;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@staUpdatedDate";
                param[3].DataType = DbType.DateTime;
                if (_staUpdatedDate.Ticks > dtmin.Ticks && _staUpdatedDate.Ticks < dtmax.Ticks)
                    param[3].value = _staUpdatedDate;
                else if (_staUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[3].value = _staUpdatedDate;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@staIsDeleted";
                param[4].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_staIsDeleted.ToString()))
                    param[4].value = DBNull.Value;
                else
                    param[4].value = _staIsDeleted;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@staIsActive";
                param[5].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_staIsActive.ToString()))
                    param[5].value = DBNull.Value;
                else
                    param[5].value = _staIsActive;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@staCompanyId";
                param[6].DataType = DbType.Int32;
                param[6].value = _staCompanyId;


                _ds = DBAccess.ExecDataSet("InsertStateMaster", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                Common.WriteLog("error in InsertStateMaster" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;
            }
        }
        public void UpdateState()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[4];

                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue; 

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@staId";
                param[0].DataType = DbType.Int32;
                param[0].value = _staId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@staName";
                param[1].DataType = DbType.String;
                if (String.IsNullOrEmpty(_staName))
                    param[1].value = DBNull.Value;
                else
                    param[1].value = _staName;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@staCountryId";
                param[2].DataType = DbType.Int32;
                if (_staCountryId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[2].value = _staCountryId;


                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@staUpdatedDate";
                param[3].DataType = DbType.DateTime;
                if (_staUpdatedDate.Ticks > dtmin.Ticks && _staUpdatedDate.Ticks < dtmax.Ticks)
                    param[3].value = _staUpdatedDate;
                else if (_staUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[3].value = _staUpdatedDate;


                _ds = DBAccess.ExecDataSet("UpdateState", CommandType.StoredProcedure, param);
                _returnBoolean = true;
              
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                Common.WriteLog("error in UpdateState" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;
            }
        }
        public void UpdateStateMaster()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[4];

                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue; 

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@staId";
                param[0].DataType = DbType.Int32;
                param[0].value = _staId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@staName";
                param[1].DataType = DbType.String;
                if (String.IsNullOrEmpty(_staName))
                    param[1].value = DBNull.Value;
                else
                    param[1].value = _staName;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@staCountryId";
                param[2].DataType = DbType.Int32;
                if (_staCountryId == 0)
                { }		//param[8].value = DBNull.Value;
                else
                    param[2].value = _staCountryId;


                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@staUpdatedDate";
                param[3].DataType = DbType.DateTime;
                if (_staUpdatedDate.Ticks > dtmin.Ticks && _staUpdatedDate.Ticks < dtmax.Ticks)
                    param[3].value = _staUpdatedDate;
                else if (_staUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[3].value = _staUpdatedDate;


                _ds = DBAccess.ExecDataSet("UpdateStateMaster", CommandType.StoredProcedure, param);
                _returnBoolean = true;

            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                Common.WriteLog("error in UpdateStateMaster" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;
            }
        }

        public void GetAllState()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@staIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _staIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@staIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _staIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@staCompanyId";
                param[2].DataType = DbType.Boolean;
                param[2].value = _staCompanyId;

                _ds = DBAccess.ExecDataSet("GetAllState", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllState" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        public void GetAllStateMaster()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@staIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _staIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@staIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _staIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@staCompanyId";
                param[2].DataType = DbType.Boolean;
                param[2].value = _staCompanyId;

                _ds = DBAccess.ExecDataSet("GetAllStateMaster", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllStateMaster" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        public void GetStatebyCountryid()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@staIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _staIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@staIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _staIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@staCountryId";
                param[2].DataType = DbType.Int32;
                param[2].value = _staCountryId;

                _ds = DBAccess.ExecDataSet("GetStatebyCountryid", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllState" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        public void GetStatebyCountryidMaster()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@staIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _staIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@staIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _staIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@staCountryId";
                param[2].DataType = DbType.Int32;
                param[2].value = _staCountryId;

                _ds = DBAccess.ExecDataSet("GetStatebyCountryidMaster", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetStatebyCountryidMaster" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void stateStatusUpdate()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] pm = new ParamStruct[3];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@staId";
                pm[0].DataType = DbType.Int32;
                pm[0].value = _staId;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@staIsActive";
                pm[1].DataType = DbType.Boolean;
                pm[1].value = _staIsActive;

                pm[2].direction = ParameterDirection.Input;
                pm[2].ParamName = "@staIsDeleted";
                pm[2].DataType = DbType.Boolean;
                pm[2].value = _staIsDeleted;

                _ds = DBAccess.ExecDataSet("stateStatusUpdate", CommandType.StoredProcedure, pm);
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in stateStatusUpdate  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            }
        }
        public void stateStatusUpdateMaster()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] pm = new ParamStruct[3];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@staId";
                pm[0].DataType = DbType.Int32;
                pm[0].value = _staId;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@staIsActive";
                pm[1].DataType = DbType.Boolean;
                pm[1].value = _staIsActive;

                pm[2].direction = ParameterDirection.Input;
                pm[2].ParamName = "@staIsDeleted";
                pm[2].DataType = DbType.Boolean;
                pm[2].value = _staIsDeleted;

                _ds = DBAccess.ExecDataSet("stateStatusUpdateMaster", CommandType.StoredProcedure, pm);
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in stateStatusUpdate  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            }
        }

        public void GetAllstatebyid()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@staId";
                param[0].DataType = DbType.Int32;
                param[0].value = _staId;


                _ds = DBAccess.ExecDataSet("GetAllstatebyid", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllstatebyid" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        public void GetAllstatebyidMaster()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@staId";
                param[0].DataType = DbType.Int32;
                param[0].value = _staId;


                _ds = DBAccess.ExecDataSet("GetAllstatebyidMaster", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllstatebyidMaster" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void stateCheckDuplication(string name, Int32 id)
        {
            try
            {
                DataAccess obj = new DataAccess();
                obj.Provider = EnumProviders.SQLClient;
                obj.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["connstring"].ToString();

                ParamStruct[] param = new ParamStruct[2];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue; 

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@staName";
                param[0].DataType = DbType.String;
                param[0].value = name;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@staId";
                param[1].DataType = DbType.Int32;
                param[1].value = id;

                _ds = obj.ExecDataSet("StateCheckDuplication", CommandType.StoredProcedure, param);
                if (_ds.Tables[0].Rows.Count > 0)
                {
                    _returnString = (_ds.Tables[0].Rows[0]["returnStatement"].ToString());

                }

            }
            catch (DataException ex)
            {
                throw ex;
            }


        }
        public void StateCheckDuplicationMaster(string name, Int32 id)
        {
            try
            {
                DataAccess obj = new DataAccess();
                obj.Provider = EnumProviders.SQLClient;
                obj.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["connstring"].ToString();

                ParamStruct[] param = new ParamStruct[2];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue; 

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@staName";
                param[0].DataType = DbType.String;
                param[0].value = name;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@staId";
                param[1].DataType = DbType.Int32;
                param[1].value = id;

                _ds = obj.ExecDataSet("StateCheckDuplicationMaster", CommandType.StoredProcedure, param);
                if (_ds.Tables[0].Rows.Count > 0)
                {
                    _returnString = (_ds.Tables[0].Rows[0]["returnStatement"].ToString());

                }

            }
            catch (DataException ex)
            {
                throw ex;
            }


        }
        #endregion


    }
}
