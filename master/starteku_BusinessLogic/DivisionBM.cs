﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starteku.Data.Manager;
using System.Data;


namespace starteku_BusinessLogic
{
    public class DivisionBM
    {
        #region Private Declaration

        private DataSet _ds;
        private string _returnString;
        private bool _returnBoolean;
        private Int32 _divId;
        private Int32 _divPerentId;
        private String _divName;
        private String _divNameDN;
        private DateTime _divCreatedDate;
        private DateTime _divUpdatedDate;
        private Boolean _divIsDeleted;
        private Boolean _divIsActive;
        private Int32 _divCompanyId;
        private String _divCreateBy;
        private Int32 _divDepId;
        private String _divDescription;
        private String _divcatId;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public DataSet ds
        {
            get { return _ds; }
            set { _ds = value; }
        }
        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public string ReturnString
        {
            get { return _returnString; }
            set { _returnString = value; }
        }

        /// <summary>
        /// Gets or sets the ReturnBoolean value.
        /// </summary>
        public bool ReturnBoolean
        {
            get { return _returnBoolean; }
            set { _returnBoolean = value; }
        }

        /// <summary>
        /// Gets or sets the depId value.
        /// </summary>
        public Int32 depId
        {
            get { return _divId; }
            set { _divId = value; }
        }

        /// <summary>
        /// Gets or sets the depPerentId value.
        /// </summary>
        public Int32 depPerentId
        {
            get { return _divPerentId; }
            set { _divPerentId = value; }
        }

        /// <summary>
        /// Gets or sets the depName value.
        /// </summary>
        public String depName
        {
            get { return _divName; }
            set { _divName = value; }
        }

        public String depNameDN
        {
            get { return _divNameDN; }
            set { _divNameDN = value; }
        }

        /// <summary>
        /// Gets or sets the depCreatedDate value.
        /// </summary>
        public DateTime depCreatedDate
        {
            get { return _divCreatedDate; }
            set { _divCreatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the depUpdatedDate value.
        /// </summary>
        public DateTime depUpdatedDate
        {
            get { return _divUpdatedDate; }
            set { _divUpdatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the depIsDeleted value.
        /// </summary>
        public Boolean depIsDeleted
        {
            get { return _divIsDeleted; }
            set { _divIsDeleted = value; }
        }

        /// <summary>
        /// Gets or sets the depIsActive value.
        /// </summary>
        public Boolean depIsActive
        {
            get { return _divIsActive; }
            set { _divIsActive = value; }
        }

        /// <summary>
        /// Gets or sets the depCompanyId value.
        /// </summary>
        public Int32 depCompanyId
        {
            get { return _divCompanyId; }
            set { _divCompanyId = value; }
        }

        /// <summary>
        /// Gets or sets the depCreateBy value.
        /// </summary>
        public String depCreateBy
        {
            get { return _divCreateBy; }
            set { _divCreateBy = value; }
        }

        /// <summary>
        /// Gets or sets the depDepId value.
        /// </summary>
        public Int32 depDepId
        {
            get { return _divDepId; }
            set { _divDepId = value; }
        }
        public String divDescription
        {
            get { return _divDescription; }
            set { _divDescription = value; }
        }
        public string divcatId
        {
            get { return _divcatId; }
            set { _divcatId = value; }
        }

        #endregion


        #region Methods

        public void InsertDocCat()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[12];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = Convert.ToDateTime("1/1/9999 11:59:59 PM");

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@divPerentId";
                param[0].DataType = DbType.Int32;
                param[0].value = _divPerentId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@divName";
                param[1].DataType = DbType.String;
                if (String.IsNullOrEmpty(_divName))
                    param[1].value = DBNull.Value;
                else
                    param[1].value = _divName;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@divCreatedDate";
                param[2].DataType = DbType.DateTime;
                if (_divCreatedDate.Ticks > dtmin.Ticks && _divCreatedDate.Ticks < dtmax.Ticks)
                    param[2].value = _divCreatedDate;
                else if (_divCreatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[2].value = _divCreatedDate;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@divUpdatedDate";
                param[3].DataType = DbType.DateTime;
                if (_divUpdatedDate.Ticks > dtmin.Ticks && _divUpdatedDate.Ticks < dtmax.Ticks)
                    param[3].value = _divUpdatedDate;
                else if (_divUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[3].value = _divUpdatedDate;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@divIsDeleted";
                param[4].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_divIsDeleted.ToString()))
                    param[4].value = DBNull.Value;
                else
                    param[4].value = _divIsDeleted;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@divIsActive";
                param[5].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_divIsActive.ToString()))
                    param[5].value = DBNull.Value;
                else
                    param[5].value = _divIsActive;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@divCompanyId";
                param[6].DataType = DbType.Int32;
                param[6].value = _divCompanyId;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@divCreateBy";
                param[7].DataType = DbType.String;
                if (String.IsNullOrEmpty(_divCreateBy))
                    param[7].value = DBNull.Value;
                else
                    param[7].value = _divCreateBy;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@divDepId";
                param[8].DataType = DbType.Int32;
                param[8].value = _divDepId;

                
                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@divDescription";
                param[9].DataType = DbType.String;
                if (String.IsNullOrEmpty(_divDescription))
                    param[9].value = DBNull.Value;
                else
                    param[9].value = _divDescription;

                param[10].direction = ParameterDirection.Input;
                param[10].ParamName = "@divcatId";
                param[10].DataType = DbType.String;
                if (String.IsNullOrEmpty(_divcatId))
                    param[10].value = DBNull.Value;
                else
                    param[10].value = _divcatId;


                param[11].direction = ParameterDirection.Input;
                param[11].ParamName = "@divNameDN";
                param[11].DataType = DbType.String;
                if (String.IsNullOrEmpty(_divNameDN))
                    param[11].value = DBNull.Value;
                else
                    param[11].value = _divNameDN;


                _ds = DBAccess.ExecDataSet("InsertDocCat", CommandType.StoredProcedure, param);
                _returnBoolean = true;
               
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        public void InsertDivision()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[12];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = Convert.ToDateTime("1/1/9999 11:59:59 PM");

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@divPerentId";
                param[0].DataType = DbType.Int32;
                param[0].value = _divPerentId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@divName";
                param[1].DataType = DbType.String;
                if (String.IsNullOrEmpty(_divName))
                    param[1].value = DBNull.Value;
                else
                    param[1].value = _divName;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@divCreatedDate";
                param[2].DataType = DbType.DateTime;
                if (_divCreatedDate.Ticks > dtmin.Ticks && _divCreatedDate.Ticks < dtmax.Ticks)
                    param[2].value = _divCreatedDate;
                else if (_divCreatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[2].value = _divCreatedDate;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@divUpdatedDate";
                param[3].DataType = DbType.DateTime;
                if (_divUpdatedDate.Ticks > dtmin.Ticks && _divUpdatedDate.Ticks < dtmax.Ticks)
                    param[3].value = _divUpdatedDate;
                else if (_divUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[3].value = _divUpdatedDate;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@divIsDeleted";
                param[4].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_divIsDeleted.ToString()))
                    param[4].value = DBNull.Value;
                else
                    param[4].value = _divIsDeleted;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@divIsActive";
                param[5].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(_divIsActive.ToString()))
                    param[5].value = DBNull.Value;
                else
                    param[5].value = _divIsActive;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@divCompanyId";
                param[6].DataType = DbType.Int32;
                param[6].value = _divCompanyId;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@divCreateBy";
                param[7].DataType = DbType.String;
                if (String.IsNullOrEmpty(_divCreateBy))
                    param[7].value = DBNull.Value;
                else
                    param[7].value = _divCreateBy;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@divDepId";
                param[8].DataType = DbType.Int32;
                param[8].value = _divDepId;


                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@divDescription";
                param[9].DataType = DbType.String;
                if (String.IsNullOrEmpty(_divDescription))
                    param[9].value = DBNull.Value;
                else
                    param[9].value = _divDescription;

                param[10].direction = ParameterDirection.Input;
                param[10].ParamName = "@divcatId";
                param[10].DataType = DbType.String;
                if (String.IsNullOrEmpty(_divcatId))
                    param[10].value = DBNull.Value;
                else
                    param[10].value = _divcatId;

                param[11].direction = ParameterDirection.Input;
                param[11].ParamName = "@divNameDN";
                param[11].DataType = DbType.String;
                if (String.IsNullOrEmpty(_divNameDN))
                    param[11].value = DBNull.Value;
                else
                    param[11].value = _divNameDN;

                _ds = DBAccess.ExecDataSet("InsertDivisions", CommandType.StoredProcedure, param);
                _returnBoolean = true;

            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }
        public void UpdateDivision()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[7];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = Convert.ToDateTime("1/1/9999 11:59:59 PM");

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@divId";
                param[0].DataType = DbType.Int32;
                param[0].value = _divId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@divPerentId";
                param[1].DataType = DbType.Int32;
                param[1].value = _divPerentId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@divName";
                param[2].DataType = DbType.String;
                if (String.IsNullOrEmpty(_divName))
                    param[2].value = DBNull.Value;
                else
                    param[2].value = _divName;


                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@divUpdatedDate";
                param[3].DataType = DbType.DateTime;
                if (_divUpdatedDate.Ticks > dtmin.Ticks && _divUpdatedDate.Ticks < dtmax.Ticks)
                    param[3].value = _divUpdatedDate;
                else if (_divUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[3].value = _divUpdatedDate;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@divDescription";
                param[4].DataType = DbType.String;
                if (String.IsNullOrEmpty(_divDescription))
                    param[4].value = DBNull.Value;
                else
                    param[4].value = _divDescription;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@divcatId";
                param[5].DataType = DbType.String;
                if (String.IsNullOrEmpty(_divcatId))
                    param[5].value = DBNull.Value;
                else
                    param[5].value = _divcatId;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@divNameDN";
                param[6].DataType = DbType.String;
                if (String.IsNullOrEmpty(_divNameDN))
                    param[6].value = DBNull.Value;
                else
                    param[6].value = _divNameDN;

                _ds = DBAccess.ExecDataSet("UpdateDivisions", CommandType.StoredProcedure, param);
                _returnBoolean = true;
               
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        public void UpdateDocCate()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[7];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = Convert.ToDateTime("1/1/9999 11:59:59 PM");

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@divId";
                param[0].DataType = DbType.Int32;
                param[0].value = _divId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@divPerentId";
                param[1].DataType = DbType.Int32;
                param[1].value = _divPerentId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@divName";
                param[2].DataType = DbType.String;
                if (String.IsNullOrEmpty(_divName))
                    param[2].value = DBNull.Value;
                else
                    param[2].value = _divName;


                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@divUpdatedDate";
                param[3].DataType = DbType.DateTime;
                if (_divUpdatedDate.Ticks > dtmin.Ticks && _divUpdatedDate.Ticks < dtmax.Ticks)
                    param[3].value = _divUpdatedDate;
                else if (_divUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[3].value = _divUpdatedDate;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@divDescription";
                param[4].DataType = DbType.String;
                if (String.IsNullOrEmpty(_divDescription))
                    param[4].value = DBNull.Value;
                else
                    param[4].value = _divDescription;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@divcatId";
                param[5].DataType = DbType.String;
                if (String.IsNullOrEmpty(_divcatId))
                    param[5].value = DBNull.Value;
                else
                    param[5].value = _divcatId;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@divNameDN";
                param[6].DataType = DbType.String;
                if (String.IsNullOrEmpty(_divNameDN))
                    param[6].value = DBNull.Value;
                else
                    param[6].value = _divNameDN;

                _ds = DBAccess.ExecDataSet("UpdateDocCate", CommandType.StoredProcedure, param);
                _returnBoolean = true;

            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        public void GetAllDivision()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@divIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _divIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@divIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _divIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@divCompanyId";
                param[2].DataType = DbType.Int32;
                param[2].value = _divCompanyId;

                _ds = DBAccess.ExecDataSet("GetAllDivisions", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllDivisions" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void GetAllDocCategory()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@divIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _divIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@divIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _divIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@divCompanyId";
                param[2].DataType = DbType.Int32;
                param[2].value = _divCompanyId;

                _ds = DBAccess.ExecDataSet("GetAllDocCategory", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllDivisions" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void DocCateStatusUpdate()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] pm = new ParamStruct[3];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@divId";
                pm[0].DataType = DbType.Int32;
                pm[0].value = _divId;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@divIsActive";
                pm[1].DataType = DbType.Boolean;
                pm[1].value = _divIsActive;

                pm[2].direction = ParameterDirection.Input;
                pm[2].ParamName = "@divIsDeleted";
                pm[2].DataType = DbType.Boolean;
                pm[2].value = _divIsDeleted;

                _ds = DBAccess.ExecDataSet("DocCateStatusUpdate", CommandType.StoredProcedure, pm);
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in DivisionsStatusUpdate  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            }
        }

        public void DivisionStatusUpdate()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] pm = new ParamStruct[3];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@divId";
                pm[0].DataType = DbType.Int32;
                pm[0].value = _divId;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@divIsActive";
                pm[1].DataType = DbType.Boolean;
                pm[1].value = _divIsActive;

                pm[2].direction = ParameterDirection.Input;
                pm[2].ParamName = "@divIsDeleted";
                pm[2].DataType = DbType.Boolean;
                pm[2].value = _divIsDeleted;

                _ds = DBAccess.ExecDataSet("DivisionsStatusUpdate", CommandType.StoredProcedure, pm);
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in DivisionsStatusUpdate  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            }
        }

        public void GetAllDivisionbyid()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@divId";
                param[0].DataType = DbType.Int32;
                param[0].value = _divId;


                _ds = DBAccess.ExecDataSet("GetAllDivisionsbyid", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllDivisionsbyid" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void GetAllDocCatebyid()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@divId";
                param[0].DataType = DbType.Int32;
                param[0].value = _divId;


                _ds = DBAccess.ExecDataSet("GetAllDocCatebyid", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllDivisionsbyid" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }


        public void GetAllDivisionsbyCompanyId()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@divCompanyId";
                param[0].DataType = DbType.Int32;
                param[0].value = _divCompanyId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@divIsActive";
                param[1].DataType = DbType.Boolean;
                param[1].value = _divIsActive;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@divIsDeleted";
                param[2].DataType = DbType.Boolean;
                param[2].value = _divIsDeleted;


                _ds = DBAccess.ExecDataSet("GetAllDivisionsbyCompanyId", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllDivisionsbyid" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void DivisionCheckDuplication(string name, Int32 id, Int32 divCompanyId)
        {
            try
            {
                DataAccess obj = new DataAccess();
                obj.Provider = EnumProviders.SQLClient;
                obj.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["connstring"].ToString();

                ParamStruct[] param = new ParamStruct[3];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = Convert.ToDateTime("1/1/9999 11:59:59 PM");

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@divName";
                param[0].DataType = DbType.String;
                param[0].value = name;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@divId";
                param[1].DataType = DbType.Int32;
                param[1].value = id;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@divCompanyId";
                param[2].DataType = DbType.Int32;
                param[2].value = divCompanyId;

                _ds = obj.ExecDataSet("DivisionsCheckDuplication", CommandType.StoredProcedure, param);
                if (_ds.Tables[0].Rows.Count > 0)
                {
                    _returnString = (_ds.Tables[0].Rows[0]["returnStatement"].ToString());

                }

            }
            catch (DataException ex)
            {
                throw ex;
            }


        }

        public void DocCateCheckDuplication(string name, Int32 id, Int32 divCompanyId)
        {
            try
            {
                DataAccess obj = new DataAccess();
                obj.Provider = EnumProviders.SQLClient;
                obj.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["connstring"].ToString();

                ParamStruct[] param = new ParamStruct[3];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = Convert.ToDateTime("1/1/9999 11:59:59 PM");

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@divName";
                param[0].DataType = DbType.String;
                param[0].value = name;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@divId";
                param[1].DataType = DbType.Int32;
                param[1].value = id;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@divCompanyId";
                param[2].DataType = DbType.Int32;
                param[2].value = divCompanyId;

                _ds = obj.ExecDataSet("DocCateCheckDuplication", CommandType.StoredProcedure, param);
                if (_ds.Tables[0].Rows.Count > 0)
                {
                    _returnString = (_ds.Tables[0].Rows[0]["returnStatement"].ToString());

                }

            }
            catch (DataException ex)
            {
                throw ex;
            }


        }
        public void GetDocCateFromParentId()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] pm = new ParamStruct[2];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@depPerentId";
                pm[0].DataType = DbType.Int32;
                pm[0].value = _divPerentId;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@depCompanyId";
                pm[1].DataType = DbType.Int32;
                pm[1].value = _divCompanyId;

                _ds = DBAccess.ExecDataSet("GetDocCateFromParentId", CommandType.StoredProcedure, pm);
            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetDivisionsFromParentId" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;
            }
        }
        public void GetDivisionsFromParentId()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] pm = new ParamStruct[2];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@depPerentId";
                pm[0].DataType = DbType.Int32;
                pm[0].value = _divPerentId;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@depCompanyId";
                pm[1].DataType = DbType.Int32;
                pm[1].value = _divCompanyId;

                _ds = DBAccess.ExecDataSet("GetDivisionsFromParentId", CommandType.StoredProcedure, pm);
            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetDivisionsFromParentId" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;
            }
        }
   
        #endregion
    }
}
