﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using starteku.Data.Manager;
using System.Configuration;
using starteku_BusinessLogic.Model;

namespace starteku_BusinessLogic
{
    public class ActivityCategoryBM
    {
        public static List<ActivityCategoryMaster> GetActivityCategoryList()
        {
            var db = new startetkuEntities1();

            var actCatList = new List<ActivityCategoryMaster>();
            try
            {
                actCatList = db.ActivityCategoryMasters.ToList();

            }
            catch (Exception ex)
            {

                ExceptionLogger.LogException(ex);
                return new List<ActivityCategoryMaster>();
            }
            finally
            {

                CommonAttributes.DisposeDBObject(ref db);
            }

            return actCatList;
        }
    }
}
