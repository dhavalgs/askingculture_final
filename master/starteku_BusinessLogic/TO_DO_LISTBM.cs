using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starteku.Data.Manager;
using System.Data;
using starteku_BusinessLogic;

namespace startetku.Business.Logic {

	public class TO_DO_LISTBM {
		#region Private Declaration
		private DataSet _ds;
		private string _returnString;
		private bool _returnBoolean;
		private Int32 _listId;
		private Int32 _listUserId;
		private String _listsubject;
		private DateTime _listCreatedDate;
		private DateTime _listUpdatedDate;
		private Boolean _listIsDeleted;
		private Boolean _listIsActive;
		private Int32 _listCompanyId;
		private Int32 _listCreateBy;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the ReturnString value.
		/// </summary>
		public DataSet ds
		{
			get { return _ds; }
			set { _ds = value; }
		}
		/// <summary>
		/// Gets or sets the ReturnString value.
		/// </summary>
		public string ReturnString {
			get { return _returnString; }
			set {  _returnString = value; }
		}

		/// <summary>
		/// Gets or sets the ReturnBoolean value.
		/// </summary>
		public bool ReturnBoolean {
			get { return _returnBoolean; }
			set {  _returnBoolean = value; }
		}

		/// <summary>
		/// Gets or sets the listId value.
		/// </summary>
		public Int32 listId  {
			get { return _listId; }
			set { _listId = value; }
		}

		/// <summary>
		/// Gets or sets the listUserId value.
		/// </summary>
		public Int32 listUserId  {
			get { return _listUserId; }
			set { _listUserId = value; }
		}

		/// <summary>
		/// Gets or sets the listsubject value.
		/// </summary>
		public String listsubject  {
			get { return _listsubject; }
			set { _listsubject = value; }
		}

		/// <summary>
		/// Gets or sets the listCreatedDate value.
		/// </summary>
		public DateTime listCreatedDate  {
			get { return _listCreatedDate; }
			set { _listCreatedDate = value; }
		}

		/// <summary>
		/// Gets or sets the listUpdatedDate value.
		/// </summary>
		public DateTime listUpdatedDate  {
			get { return _listUpdatedDate; }
			set { _listUpdatedDate = value; }
		}

		/// <summary>
		/// Gets or sets the listIsDeleted value.
		/// </summary>
		public Boolean listIsDeleted  {
			get { return _listIsDeleted; }
			set { _listIsDeleted = value; }
		}

		/// <summary>
		/// Gets or sets the listIsActive value.
		/// </summary>
		public Boolean listIsActive  {
			get { return _listIsActive; }
			set { _listIsActive = value; }
		}

		/// <summary>
		/// Gets or sets the listCompanyId value.
		/// </summary>
		public Int32 listCompanyId  {
			get { return _listCompanyId; }
			set { _listCompanyId = value; }
		}

		/// <summary>
		/// Gets or sets the listCreateBy value.
		/// </summary>
		public Int32 listCreateBy  {
			get { return _listCreateBy; }
			set { _listCreateBy = value; }
		}

		#endregion

		#region Methods
		

		public void InsertTO_DO_LIST() {
			try 
			{
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

				ParamStruct[] param = new ParamStruct[8];
				DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue; 

				param[0].direction = ParameterDirection.Input;
				param[0].ParamName = "@listUserId";
				param[0].DataType = DbType.Int32;					
				param[0].value = _listUserId;

				param[1].direction = ParameterDirection.Input;
				param[1].ParamName = "@listsubject";
				param[1].DataType = DbType.String;
				if (String.IsNullOrEmpty(_listsubject))
					param[1].value = DBNull.Value;
				else
					param[1].value = _listsubject;

				param[2].direction = ParameterDirection.Input;
				param[2].ParamName = "@listCreatedDate";
				param[2].DataType = DbType.DateTime;
				if (_listCreatedDate.Ticks > dtmin.Ticks && _listCreatedDate.Ticks < dtmax.Ticks)
					param[2].value = _listCreatedDate;
				else if (_listCreatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
				{}
				else
					param[2].value = _listCreatedDate;

				param[3].direction = ParameterDirection.Input;
				param[3].ParamName = "@listUpdatedDate";
				param[3].DataType = DbType.DateTime;
				if (_listUpdatedDate.Ticks > dtmin.Ticks && _listUpdatedDate.Ticks < dtmax.Ticks)
					param[3].value = _listUpdatedDate;
				else if (_listUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
				{}
				else
					param[3].value = _listUpdatedDate;

				param[4].direction = ParameterDirection.Input;
				param[4].ParamName = "@listIsDeleted";
				param[4].DataType = DbType.Boolean;
				if (String.IsNullOrEmpty(_listIsDeleted.ToString()))
					param[4].value = DBNull.Value;
				else
					param[4].value = _listIsDeleted;

				param[5].direction = ParameterDirection.Input;
				param[5].ParamName = "@listIsActive";
				param[5].DataType = DbType.Boolean;
				if (String.IsNullOrEmpty(_listIsActive.ToString()))
					param[5].value = DBNull.Value;
				else
					param[5].value = _listIsActive;

				param[6].direction = ParameterDirection.Input;
				param[6].ParamName = "@listCompanyId";
				param[6].DataType = DbType.Int32;
					if (_listCompanyId == 0)
					{ }		//param[8].value = DBNull.Value;
					else
					param[6].value = _listCompanyId;

				param[7].direction = ParameterDirection.Input;
				param[7].ParamName = "@listCreateBy";
				param[7].DataType = DbType.Int32;
					if (_listCreateBy == 0)
					{ }		//param[8].value = DBNull.Value;
					else
					param[7].value = _listCreateBy;

				//obj.ExecScalar("InsertTO_DO_LIST", CommandType.StoredProcedure, param);				_returnBoolean = true;
                    _ds = DBAccess.ExecDataSet("InsertTO_DO_LIST", CommandType.StoredProcedure, param);
                    _returnBoolean = true;
			}
			catch (DataException ex) 
			{
				_returnBoolean = false;
				throw ex;
			}
		}

        public void GetTO_DO_LISTbyid()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@listIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _listIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@listIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _listIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@listUserId";
                param[2].DataType = DbType.Int32;
                param[2].value = _listUserId;

                _ds = DBAccess.ExecDataSet("GetTO_DO_LISTbyid", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllDepartments" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void GetTO_DO_LISTUpdatestatus()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] pm = new ParamStruct[3];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@listId";
                pm[0].DataType = DbType.Int32;
                pm[0].value = _listId;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@listIsActive";
                pm[1].DataType = DbType.Boolean;
                pm[1].value = _listIsActive;

                pm[2].direction = ParameterDirection.Input;
                pm[2].ParamName = "@listIsDeleted";
                pm[2].DataType = DbType.Boolean;
                pm[2].value = _listIsDeleted;

                _ds = DBAccess.ExecDataSet("GetTO_DO_LISTUpdatestatus", CommandType.StoredProcedure, pm);
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in GetTO_DO_LISTUpdatestatus  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            }
        }
		#endregion

	}
}
