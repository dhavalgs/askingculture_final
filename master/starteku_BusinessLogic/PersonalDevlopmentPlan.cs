﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using starteku_BusinessLogic.Model;
using starteku.Data.Manager;

namespace starteku_BusinessLogic
{
    public class PersonalDevlopmentPlan
    {
        #region Private Declaration

        private DataSet _ds;

        private Int32 _metID;
        private String _metDescription;
        private String _metDescriptionDN;
        private DateTime _metCreateDate;
        private DateTime _metUpdateDate;
        private Boolean _metIsActive;
        private Int32 _metCompanyID;
        private Boolean _metIsDelete;
        private DateTime _metDate;
        private String _metParticipantsId;
        private Int32 _metUserId;
        private String _metStatus;

        private Int32 _meetDocID;
        private String _DocumentName;
        private String _DocumentPath;
        private bool _returnBoolean;


        #endregion

        #region Properties

        public DataSet ds
        {
            get { return _ds; }
            set { _ds = value; }
        }
        public bool ReturnBoolean
        {
            get { return _returnBoolean; }
            set { _returnBoolean = value; }
        }

        public Int32 metID
        {
            get { return _metID; }
            set { _metID = value; }
        }
        public Int32 meetDocID
        {
            get { return _meetDocID; }
            set { _meetDocID = value; }
        }

        public Int32 metUserId
        {
            get { return _metUserId; }
            set { _metUserId = value; }
        }

        public Int32 metCompanyID
        {
            get { return _metCompanyID; }
            set { _metCompanyID = value; }
        }

        public string metDescription
        {
            get { return _metDescription; }
            set { _metDescription = value; }
        }

        public String DocumentName
        {
            get { return _DocumentName; }
            set { _DocumentName = value; }
        }
        public String DocumentPath
        {
            get { return _DocumentPath; }
            set { _DocumentPath = value; }
        }

        public string metDescriptionDN
        {
            get { return _metDescriptionDN; }
            set { _metDescriptionDN = value; }
        }

        public string metParticipantsId
        {
            get { return _metParticipantsId; }
            set { _metParticipantsId = value; }
        }

        public DateTime metDate
        {
            get { return _metDate; }
            set { _metDate = value; }
        }

        public DateTime metCreateDate
        {
            get { return _metCreateDate; }
            set { _metCreateDate = value; }
        }

        public DateTime metUpdateDate
        {
            get { return _metUpdateDate; }
            set { _metUpdateDate = value; }
        }

        public Boolean metIsActive
        {
            get { return _metIsActive; }
            set { _metIsActive = value; }
        }

        public Boolean metIsDelete
        {
            get { return _metIsDelete; }
            set { _metIsDelete = value; }
        }

        public string metStatus
        {
            get { return _metStatus; }
            set { _metStatus = value; }
        }

        #endregion


        public void InsertMeetingPDP()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[12];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@metDate";
                param[0].DataType = DbType.DateTime;
                param[0].value = _metDate;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@metDescription";
                param[1].DataType = DbType.String;
                param[1].value = _metDescription;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@metDescriptionDN";
                param[2].DataType = DbType.String;
                param[2].value = _metDescriptionDN;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@metParticipantsId";
                param[3].DataType = DbType.String;
                param[3].value = _metParticipantsId;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@metCreateDate";
                param[4].DataType = DbType.DateTime;
                param[4].value = _metCreateDate;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@metUpdateDate";
                param[5].DataType = DbType.DateTime;
                param[5].value = _metUpdateDate;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@metCompanyID";
                param[6].DataType = DbType.Int32;
                param[6].value = _metCompanyID;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@metUserId";
                param[7].DataType = DbType.Int32;
                param[7].value = _metUserId;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@metIsActive";
                param[8].DataType = DbType.Boolean;
                param[8].value = _metIsActive;

                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@metIsDelete";
                param[9].DataType = DbType.Boolean;
                param[9].value = _metIsDelete;

                param[10].direction = ParameterDirection.Input;
                param[10].ParamName = "@metStatus";
                param[10].DataType = DbType.String;
                param[10].value = _metStatus;

                param[11].direction = ParameterDirection.Input;
                param[11].ParamName = "@metID";
                param[11].DataType = DbType.Int32;
                param[11].value = _metID;

                _ds = DBAccess.ExecDataSet("SpInsertMeeting_PDP", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {

                Common.WriteLog("error in SpInsertMeeting_PDP" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;
            }
        }

        public void UpdateMeetingStatus()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[12];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@metDate";
                param[0].DataType = DbType.DateTime;
                param[0].value = _metDate;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@metDescription";
                param[1].DataType = DbType.String;
                param[1].value = _metDescription;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@metDescriptionDN";
                param[2].DataType = DbType.String;
                param[2].value = _metDescriptionDN;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@metParticipantsId";
                param[3].DataType = DbType.String;
                param[3].value = _metParticipantsId;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@metCreateDate";
                param[4].DataType = DbType.DateTime;
                param[4].value = _metCreateDate;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@metUpdateDate";
                param[5].DataType = DbType.DateTime;
                param[5].value = _metUpdateDate;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@metCompanyID";
                param[6].DataType = DbType.Int32;
                param[6].value = _metCompanyID;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@metUserId";
                param[7].DataType = DbType.Int32;
                param[7].value = _metUserId;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@metIsActive";
                param[8].DataType = DbType.Boolean;
                param[8].value = _metIsActive;

                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@metIsDelete";
                param[9].DataType = DbType.Boolean;
                param[9].value = _metIsDelete;

                param[10].direction = ParameterDirection.Input;
                param[10].ParamName = "@metStatus";
                param[10].DataType = DbType.String;
                param[10].value = _metStatus;

                param[11].direction = ParameterDirection.Input;
                param[11].ParamName = "@metID";
                param[11].DataType = DbType.Int32;
                param[11].value = _metID;

                _ds = DBAccess.ExecDataSet("SpUpdateMeeting", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {

                Common.WriteLog("error in SpUpdateMeeting" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;
            }
        }

        public void GetStatusOfMeeting()
        {
            try
            {

                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@metCreateDate";
                param[0].DataType = DbType.DateTime;
                param[0].value = _metCreateDate;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@metCompanyID";
                param[1].DataType = DbType.Int32;
                param[1].value = _metCompanyID;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@metUserId";
                param[2].DataType = DbType.Int32;
                param[2].value = _metUserId;

                _ds = DBAccess.ExecDataSet("SpMeetingStaus", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {

                Common.WriteLog("error in SpMeetingStaus" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;
            }

        }

        public void MeetingStatusDelete()
        {
            try
            {

                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@metID";
                param[0].DataType = DbType.Int32;
                param[0].value = _metID;

                _ds = DBAccess.ExecDataSet("SpMeetingStausDelete", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {

                Common.WriteLog("error in SpMeetingStausDelete" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;
            }
        }

        public void MeetingStatusEdit()
        {
            try
            {

                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@metID";
                param[0].DataType = DbType.Int32;
                param[0].value = _metID;

                _ds = DBAccess.ExecDataSet("SpMeetingStatusEdit", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {

                Common.WriteLog("error in SpMeetingStatusEdit" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;
            }
        }

        public void ParticipantListBindData()
        {
            try
            {

                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@metID";
                param[0].DataType = DbType.Int32;
                param[0].value = _metID;

                _ds = DBAccess.ExecDataSet("SpParticipantListBind", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {

                Common.WriteLog("error in SpParticipantListBind" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;
            }
        }


        #region NewMethod
        public void InsertMeetingDocument()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[6];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue;

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@meetDocID";
                param[0].DataType = DbType.Int32;
                param[0].value = _meetDocID;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@DocumentName";
                param[1].DataType = DbType.String;
                param[1].value = _DocumentName;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@DocumentPath";
                param[2].DataType = DbType.String;
                param[2].value = _DocumentPath;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@CreateDate";
                param[3].DataType = DbType.DateTime;
                param[3].value = _metCreateDate;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@UserID";
                param[4].DataType = DbType.Int32;
                param[4].value = _metUserId;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@CompanyID";
                param[5].DataType = DbType.Int32;
                param[5].value = _metCompanyID;

                _ds = DBAccess.ExecDataSet("InsertUpdateMeetingDocument", CommandType.StoredProcedure, param);
                _returnBoolean = true;

            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }
        public void GetAllMeetingDocument()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[2];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@UserID";
                param[0].DataType = DbType.Int32;
                param[0].value = _metUserId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@CompanyID";
                param[1].DataType = DbType.Int32;
                param[1].value = _metCompanyID;

                _ds = DBAccess.ExecDataSet("GetAllMeetingDocumentList", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllMeetingDocument" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void DeleteMeetingDocumentData()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@meetDocID";
                param[0].DataType = DbType.Int32;
                param[0].value = _metID;



                _ds = DBAccess.ExecDataSet("DeleteMeetingDocument", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in DeleteMeetingDocument" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
       
        #endregion
    }
}
