using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using starteku.Data.Manager;
using starteku_BusinessLogic;

namespace startetku.Business.Logic {

	public class Knowledge_PointBM {
		#region Private Declaration

		private DataSet _ds;
		private string _returnString;
		private bool _returnBoolean;
		private Int32 _KnowledgeId;
		private Int32 _Knowledge_ComId;
		private Int32 _Knowledge_CompanyId;
		private String _Knowledge_point;
		private Int32 _Knowledge_UserId;
		private DateTime _CreatedDate;
		private DateTime _UpdatedDate;
		private Boolean _IsDeleted;
		private Boolean _IsActive;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the ReturnString value.
		/// </summary>
		public DataSet ds
		{
			get { return _ds; }
			set { _ds = value; }
		}
		/// <summary>
		/// Gets or sets the ReturnString value.
		/// </summary>
		public string ReturnString {
			get { return _returnString; }
			set {  _returnString = value; }
		}

		/// <summary>
		/// Gets or sets the ReturnBoolean value.
		/// </summary>
		public bool ReturnBoolean {
			get { return _returnBoolean; }
			set {  _returnBoolean = value; }
		}

		/// <summary>
		/// Gets or sets the KnowledgeId value.
		/// </summary>
		public Int32 KnowledgeId  {
			get { return _KnowledgeId; }
			set { _KnowledgeId = value; }
		}

		/// <summary>
		/// Gets or sets the Knowledge_ComId value.
		/// </summary>
		public Int32 Knowledge_ComId  {
			get { return _Knowledge_ComId; }
			set { _Knowledge_ComId = value; }
		}

		/// <summary>
		/// Gets or sets the Knowledge_CompanyId value.
		/// </summary>
		public Int32 Knowledge_CompanyId  {
			get { return _Knowledge_CompanyId; }
			set { _Knowledge_CompanyId = value; }
		}

		/// <summary>
		/// Gets or sets the Knowledge_point value.
		/// </summary>
		public String Knowledge_point  {
			get { return _Knowledge_point; }
			set { _Knowledge_point = value; }
		}

		/// <summary>
		/// Gets or sets the Knowledge_UserId value.
		/// </summary>
		public Int32 Knowledge_UserId  {
			get { return _Knowledge_UserId; }
			set { _Knowledge_UserId = value; }
		}

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime CreatedDate  {
			get { return _CreatedDate; }
			set { _CreatedDate = value; }
		}

		/// <summary>
		/// Gets or sets the UpdatedDate value.
		/// </summary>
		public DateTime UpdatedDate  {
			get { return _UpdatedDate; }
			set { _UpdatedDate = value; }
		}

		/// <summary>
		/// Gets or sets the IsDeleted value.
		/// </summary>
		public Boolean IsDeleted  {
			get { return _IsDeleted; }
			set { _IsDeleted = value; }
		}

		/// <summary>
		/// Gets or sets the IsActive value.
		/// </summary>
		public Boolean IsActive  {
			get { return _IsActive; }
			set { _IsActive = value; }
		}

		#endregion

		#region Methods
		
		public void InsertKnowledge_Point() {
			try 
			{
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

				ParamStruct[] param = new ParamStruct[8];
				DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
				DateTime dtmax = Convert.ToDateTime("1/1/9999 11:59:59 PM");
				param[0].direction = ParameterDirection.Input;
				param[0].ParamName = "@Knowledge_ComId";
				param[0].DataType = DbType.Int32;
					if (_Knowledge_ComId == 0)
					{ }		//param[8].value = DBNull.Value;
					else
					param[0].value = _Knowledge_ComId;

				param[1].direction = ParameterDirection.Input;
				param[1].ParamName = "@Knowledge_CompanyId";
				param[1].DataType = DbType.Int32;
					if (_Knowledge_CompanyId == 0)
					{ }		//param[8].value = DBNull.Value;
					else
					param[1].value = _Knowledge_CompanyId;

				param[2].direction = ParameterDirection.Input;
				param[2].ParamName = "@Knowledge_point";
				param[2].DataType = DbType.String;
				if (String.IsNullOrEmpty(_Knowledge_point))
					param[2].value = DBNull.Value;
				else
					param[2].value = _Knowledge_point;

				param[3].direction = ParameterDirection.Input;
				param[3].ParamName = "@Knowledge_UserId";
				param[3].DataType = DbType.Int32;
					if (_Knowledge_UserId == 0)
					{ }		//param[8].value = DBNull.Value;
					else
					param[3].value = _Knowledge_UserId;

				param[4].direction = ParameterDirection.Input;
				param[4].ParamName = "@CreatedDate";
				param[4].DataType = DbType.DateTime;
				if (_CreatedDate.Ticks > dtmin.Ticks && _CreatedDate.Ticks < dtmax.Ticks)
					param[4].value = _CreatedDate;
				else if (_CreatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
				{}
				else
					param[4].value = _CreatedDate;

				param[5].direction = ParameterDirection.Input;
				param[5].ParamName = "@UpdatedDate";
				param[5].DataType = DbType.DateTime;
				if (_UpdatedDate.Ticks > dtmin.Ticks && _UpdatedDate.Ticks < dtmax.Ticks)
					param[5].value = _UpdatedDate;
				else if (_UpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
				{}
				else
					param[5].value = _UpdatedDate;

				param[6].direction = ParameterDirection.Input;
				param[6].ParamName = "@IsDeleted";
				param[6].DataType = DbType.Boolean;
				if (String.IsNullOrEmpty(_IsDeleted.ToString()))
					param[6].value = DBNull.Value;
				else
					param[6].value = _IsDeleted;

				param[7].direction = ParameterDirection.Input;
				param[7].ParamName = "@IsActive";
				param[7].DataType = DbType.Boolean;
				if (String.IsNullOrEmpty(_IsActive.ToString()))
					param[7].value = DBNull.Value;
				else
					param[7].value = _IsActive;


                //if (ds != null)
                //{
                //    if (ds.Tables[0].Rows.Count > 0)
                //    {
                //        return Convert.ToInt32(ds.Tables[0].Rows[0]["paPoint"]);
                //    }

                //}

                //return 0;


				//obj.ExecScalar("InsertKnowledge_Point", CommandType.StoredProcedure, param);	
                _ds = DBAccess.ExecDataSet("InsertKnowledge_Point", CommandType.StoredProcedure, param);
                _returnBoolean = true;
			}
			catch (DataException ex) 
			{
				_returnBoolean = false;
				throw ex;
			}
		}

        public void GetKnowledge_PointByUserIdandcomId(int CompId)
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[5];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@IsDeleted";
                param[0].DataType = DbType.Boolean;
                param[0].value = _IsDeleted;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@IsActive";
                param[1].DataType = DbType.Boolean;
                param[1].value = _IsActive;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@Knowledge_ComId";
                param[2].DataType = DbType.Int32;
                param[2].value = _Knowledge_ComId;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@Knowledge_UserId";
                param[3].DataType = DbType.Int32;
                param[3].value = _Knowledge_UserId;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@CompId";
                param[4].DataType = DbType.Int32;
                param[4].value = CompId;

                _ds = DBAccess.ExecDataSet("GetKnowledge_PointByUserIdandcomId", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetKnowledge_PointByUserIdandcomId" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                // throw ex;

            }
        }
		

		#endregion

	}
}
