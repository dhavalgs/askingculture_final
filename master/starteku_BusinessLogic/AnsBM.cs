﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starteku.Data.Manager;
using System.Data;

namespace starteku_BusinessLogic
{
    public class AnsBM
    {
        #region Private Declaration
        private DataSet _ds;
        private string _returnString;
        private bool _returnBoolean;
        private Int32 _aId;
        private Int32 _ansNo;
        private String _ans;
        private Int32 _comId;
        private Boolean _answerSelect;
        #endregion

        #region Properties
        public DataSet ds
        {
            get { return _ds; }
            set { _ds = value; }
        }
        public string returnString
        {
            get { return _returnString; }
            set { _returnString = value; }
        }
        public bool returnBoolean
        {
            get { return _returnBoolean; }
            set { _returnBoolean = value; }
        }
        public Int32 aId
        {
            get { return _aId; }
            set { _aId = value; }
        }
        public Int32 aNo
        {
            get { return _ansNo; }
            set { _ansNo = value; }
        }
        public String ans
        {
            get { return _ans; }
            set { _ans = value; }
        }

        public int comId
        {
            get { return _comId; }
            set { _comId = value; }
        }
        public Boolean answerSelect
        {
            get { return _answerSelect; }
            set { _answerSelect = value; }
        }
        #endregion


        #region Methods
        public Boolean insert_ans()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@aqId";
                param[0].DataType = DbType.Int32;
                param[0].value = _ansNo;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@answer";
                param[1].DataType = DbType.String;
                if (String.IsNullOrEmpty(_ans))
                    param[1].value = DBNull.Value;
                else
                    param[1].value = _ans;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@answerSelect";
                param[2].DataType = DbType.Boolean;
                param[2].value = _answerSelect;

                _ds = DBAccess.ExecDataSet("AnswersInsert", CommandType.StoredProcedure, param);
                return true;
            }
            catch (Exception ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }
        public void DeleteAnswearsbyId()
        {
            try
            {
                //@comId
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@comId";
                param[0].DataType = DbType.Int32;
                param[0].value = _comId;

                DBAccess.ExecDataSet("DeleteAnswearsbyId", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch
            {
                _returnBoolean = false;
            }
        }
        #endregion

    }
}
