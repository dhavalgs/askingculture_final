﻿using starteku.Data.Manager;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace starteku_BusinessLogic
{


    public class PointBM
    {
        #region Private Declaration
        private DataSet _ds;
        private string _returnString;
        private bool _returnBoolean;
        private Int32 _PointId;
        private Int32 _PointUserId;
        private Int32 _PointCompanyId;
        private Int32 _PointComId;
        private Int32 _PointAnsId;
        private Int32 _PointAnsCount;
        private Boolean _PointIsDeleted;
        private Boolean _PointIsStatus;
        private Int32 _PointuserIDhelper;
        private DateTime _PointDate;
        private string _Pointmessage;
        private Int32 _PointuserIDhelped;
        private Int32 _pintSubCompetance;


        private Int32 _paID;
        private String _paPointName;
        private Int32 _paPoint;
        private Boolean _paIsActive;
        private Boolean _paIsDeleted;
        private DateTime _paCreateDate;
        private DateTime _paDeleteDate;
        private Boolean _paIsPLP;
        private Boolean _paIsALP;
        private Int32 _paCompnayId;
      
        //Saurin | 20150102 | 
        private int _pointAlp;
        private int _pointPlp;
        private int _pointTotal;

        #endregion

        #region Properties

        public int PointALP
        {
            get { return _pointAlp; }
            set { _pointAlp = value; }
        }

        public int PointPLP
        {
            get { return _pointPlp; }
            set { _pointPlp = value; }
        }

        public int PointTotal
        {
            get { return _pointTotal; }
            set { _pointTotal = value; }
        }

        public DataSet ds
        {
            get { return _ds; }
            set { _ds = value; }
        }

        public string returnString
        {
            get { return _returnString; }
            set { _returnString = value; }
        }

        public bool returnBoolean
        {
            get { return _returnBoolean; }
            set { _returnBoolean = value; }
        }

        public Int32 PointId
        {
            get { return _PointId; }
            set { _PointId = value; }
        }

        public Int32 PointUserId
        {
            get { return _PointUserId; }
            set { _PointUserId = value; }
        }

        public int PointCompanyId
        {
            get { return _PointCompanyId; }
            set { _PointCompanyId = value; }
        }

        public Int32 PointComId
        {
            get { return _PointComId; }
            set { _PointComId = value; }
        }
        public Int32 PointAnsId
        {
            get { return _PointAnsId; }
            set { _PointAnsId = value; }
        }

        public Int32 PointAnsCount
        {
            get { return _PointAnsCount; }
            set { _PointAnsCount = value; }
        }

        public Boolean PointIsDeleted
        {
            get { return _PointIsDeleted; }
            set { _PointIsDeleted = value; }
        }

        public Boolean PointIsStatus
        {
            get { return _PointIsStatus; }
            set { _PointIsStatus = value; }
        }
        public Int32 PointuserIDhelper
        {
            get { return _PointuserIDhelper; }
            set { _PointuserIDhelper = value; }
        }
        public Int32 PointuserIDhelped
        {
            get { return _PointuserIDhelped; }
            set { _PointuserIDhelped = value; }
        }
        public DateTime PointDate
        {
            get { return _PointDate; }
            set { _PointDate = value; }
        }
        public string Pointmessage
        {
            get { return _Pointmessage; }
            set { _Pointmessage = value; }
        }
        public Int32 pintSubCompetance
        {
            get { return _pintSubCompetance; }
            set { _pintSubCompetance = value; }
        }


        public int paID
        {
            get { return _paID; }
            set { _paID = value; }
        }

        public String paPointName
        {
            get { return _paPointName; }
            set { _paPointName = value; }
        }

        public int paPoint
        {
            get { return _paPoint; }
            set { _paPoint = value; }
        }
        public Boolean paIsActive
        {
            get { return _paIsActive; }
            set { _paIsActive = value; }
        }
        public Boolean paIsDeleted
        {
            get { return _paIsDeleted; }
            set { _paIsDeleted = value; }
        }
        public Boolean paIsPLP
        {
            get { return _paIsPLP; }
            set { _paIsPLP = value; }
        }
        public Boolean paIsALP
        {
            get { return _paIsALP; }
            set { _paIsALP = value; }
        }
        public DateTime paCreateDate
        {
            get { return _paCreateDate; }
            set { _paCreateDate = value; }
        }
        public DateTime paDeleteDate
        {
            get { return _paDeleteDate; }
            set { _paDeleteDate = value; }
        }

        #endregion

        #region Methods
        //Saurin | 20150102 | Created when started point flow
        public Boolean InsertUpdatePoint()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[10];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@PointUserId";
                param[0].DataType = DbType.Int32;
                param[0].value = _PointUserId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@PointCompanyId";
                param[1].DataType = DbType.Int32;
                param[1].value = _PointCompanyId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@PointComId";
                param[2].DataType = DbType.Int32;
                param[2].value = _PointComId;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@PointAnsId";
                param[3].DataType = DbType.Int32;
                param[3].value = _PointAnsId;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@PointAnsCount";
                param[4].DataType = DbType.Int32;
                param[4].value = _PointAnsCount;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@PointIsDeleted";
                param[5].DataType = DbType.Boolean;
                param[5].value = _PointIsDeleted;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@PointIsStatus";
                param[6].DataType = DbType.Boolean;
                param[6].value = _PointIsStatus;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@PointPLP";
                param[7].DataType = DbType.Int32;
                param[7].value = _pointPlp;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@PointALP";
                param[8].DataType = DbType.Int32;
                param[8].value = _pointAlp;

                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@PointTotal";
                param[9].DataType = DbType.Int32;
                param[9].value = _pointTotal;

              /*  param[10].direction = ParameterDirection.Input;
                param[10].ParamName = "@PointTotal";
                param[10].DataType = DbType.Int32;
                param[10].value = _pointTotal;*/

                _ds = DBAccess.ExecDataSet("InsertUpdatePoint", CommandType.StoredProcedure, param);
                return true;
            }
            catch (Exception ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        public static void InsertUpdatePoint(int userId,int pointPLP,int pointALP)
        {

            var user = UserBM.GetUserById(Convert.ToInt32(userId)).FirstOrDefault();
            if(user != null && user.UserType==1) return; // Currently , we are not provide point to company

            PointBM obj = new PointBM();
            obj.PointUserId = userId;
            obj.PointPLP = pointPLP;
            obj.PointALP = pointALP;
            if (user != null) obj.PointCompanyId = user.UserCompanyId;
            obj.PointIsDeleted = false;
            obj.PointIsStatus = true;
            obj.InsertUpdatePoint();
        }

        public Boolean InsertPoint()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[7];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@PointUserId";
                param[0].DataType = DbType.Int32;
                param[0].value = _PointUserId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@PointCompanyId";
                param[1].DataType = DbType.Int32;
                param[1].value = _PointCompanyId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@PointComId";
                param[2].DataType = DbType.Int32;
                param[2].value = _PointComId;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@PointAnsId";
                param[3].DataType = DbType.Int32;
                param[3].value = _PointAnsId;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@PointAnsCount";
                param[4].DataType = DbType.Int32;
                param[4].value = _PointAnsCount;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@PointIsDeleted";
                param[5].DataType = DbType.Boolean;
                param[5].value = _PointIsDeleted;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@PointIsStatus";
                param[6].DataType = DbType.Boolean;
                param[6].value = _PointIsStatus;

                _ds = DBAccess.ExecDataSet("InsertPoint", CommandType.StoredProcedure, param);
                return true;
            }
            catch (Exception ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }
        public void GetAllPoint()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@PointIsStatus";
                param[0].DataType = DbType.Boolean;
                param[0].value = _PointIsStatus;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@PointIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _PointIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@PointCompanyId";
                param[2].DataType = DbType.Int32;
                param[2].value = _PointCompanyId;

                _ds = DBAccess.ExecDataSet("GetAllPoint", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllCompetencehelp" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public Boolean UpdatePoint(String typecheck)
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[6];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@PointuserIDhelper";
                param[0].DataType = DbType.Int32;
                param[0].value = _PointuserIDhelper;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@PointDate";
                param[1].DataType = DbType.DateTime;
                param[1].value = _PointDate;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@Pointmessage";
                param[2].DataType = DbType.String;
                param[2].value = _Pointmessage;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@PointuserIDhelped";
                param[3].DataType = DbType.Int32;
                param[3].value = _PointuserIDhelped;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@PointComId";
                param[4].DataType = DbType.Int32;
                param[4].value = _PointComId;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@PointCompanyId";
                param[5].DataType = DbType.Int32;
                param[5].value = _PointCompanyId;

                if (typecheck == "1")
                {
                    _ds = DBAccess.ExecDataSet("UpdatePointhigh", CommandType.StoredProcedure, param);
                    return true;
                }
                else
                {
                    _ds = DBAccess.ExecDataSet("UpdatePoint", CommandType.StoredProcedure, param);
                    return true;
                }
            }
            catch (Exception ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }
        public Boolean UpdatePointmaster()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[4];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@PointUserId";
                param[0].DataType = DbType.Int32;
                param[0].value = _PointUserId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@PointCompanyId";
                param[1].DataType = DbType.Int32;
                param[1].value = _PointCompanyId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@pintSubCompetance";
                param[2].DataType = DbType.Int32;
                param[2].value = _pintSubCompetance;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@PointAnsCount";
                param[3].DataType = DbType.Int32;
                param[3].value = _PointAnsCount;

                _ds = DBAccess.ExecDataSet("UpdatePointmaster", CommandType.StoredProcedure, param);
                return true;

            }
            catch (Exception ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        public Boolean InsertUpdatePointAllocation(int paCompnayId, String paPointName, int paPoint, Boolean paIsPLP, Boolean paIsALP)
        {
            DataAccess DBAccess = new DataAccess();
            DBAccess.Provider = EnumProviders.SQLClient;
            DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
            ParamStruct[] param = new ParamStruct[5];

            param[0].direction = ParameterDirection.Input;
            param[0].ParamName = "@paCompnayId";
            param[0].DataType = DbType.Int32;
            param[0].value = paCompnayId;

            param[1].direction = ParameterDirection.Input;
            param[1].ParamName = "@paPointName";
            param[1].DataType = DbType.String;
            param[1].value = paPointName;

            param[2].direction = ParameterDirection.Input;
            param[2].ParamName = "@paPoint";
            param[2].DataType = DbType.Int32;
            param[2].value = paPoint;

            param[3].direction = ParameterDirection.Input;
            param[3].ParamName = "@paIsPLP";
            param[3].DataType = DbType.Boolean;
            param[3].value = paIsPLP;

            param[4].direction = ParameterDirection.Input;
            param[4].ParamName = "@paIsALP";
            param[4].DataType = DbType.Boolean;
            param[4].value = paIsALP;

            _ds = DBAccess.ExecDataSet("InsertUpdatePointAllocation", CommandType.StoredProcedure, param);
            return true;
        }
        public void GetUserPointAllocation(int paCompnayId)
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@paCompnayId";
                param[0].DataType = DbType.Int32;
                param[0].value = paCompnayId;

                _ds = DBAccess.ExecDataSet("GetUserPointAllocation", CommandType.StoredProcedure, param);
            }
            catch (Exception ex)
            {

            }
        }
        public void GetUserPoint(int PointUserId, Boolean PointIsStatus)
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[2];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@PointUserId";
                param[0].DataType = DbType.Int32;
                param[0].value = PointUserId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@PointIsStatus";
                param[1].DataType = DbType.Int32;
                param[1].value = PointIsStatus;

                _ds = DBAccess.ExecDataSet("GetUserPoint", CommandType.StoredProcedure, param);
            }
            catch (Exception ex)
            {

            }
        }
        public void GetUserPointByName(int userId, Boolean isPLP, Boolean isALP, Boolean logIsActive, Boolean  all)
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[5];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@userId";
                param[0].DataType = DbType.Int32;
                param[0].value = userId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@isPLP";
                param[1].DataType = DbType.Boolean;
                param[1].value = isPLP;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@isALP";
                param[2].DataType = DbType.Boolean;
                param[2].value = isALP;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@logIsActive";
                param[3].DataType = DbType.Boolean;
                param[3].value = logIsActive;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@all";
                param[4].DataType = DbType.Boolean;
                param[4].value = all;


                _ds = DBAccess.ExecDataSet("GetUserPointByName", CommandType.StoredProcedure, param);
            }
            catch (Exception ex)
            {

            }
        }
        public void GetUserPointByNameDetail(int userId, Boolean isPLP, Boolean isALP, Boolean logIsActive, Boolean all,String name)
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[6];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@userId";
                param[0].DataType = DbType.Int32;
                param[0].value = userId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@isPLP";
                param[1].DataType = DbType.Boolean;
                param[1].value = isPLP;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@isALP";
                param[2].DataType = DbType.Boolean;
                param[2].value = isALP;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@logIsActive";
                param[3].DataType = DbType.Boolean;
                param[3].value = logIsActive;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@all";
                param[4].DataType = DbType.Boolean;
                param[4].value = all;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@name";
                param[5].DataType = DbType.String;
                param[5].value = name;

                _ds = DBAccess.ExecDataSet("GetUserPointByNameDetail", CommandType.StoredProcedure, param);
            }
            catch (Exception ex)
            {

            }
        }
/*PDP*/
        public Boolean PDPPointMasterInsert(int pdpUserId, int pdpComId, int pdpCompanyId, int pdpCreatedById, string pdpDescription, string pdpPointAllocationFor, int pdpPoint)
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[8];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@pdpUserId";
                param[0].DataType = DbType.Int32;
                param[0].value = pdpUserId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@pdpComId ";
                param[1].DataType = DbType.Int32;
                param[1].value = pdpComId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@pdpCompanyId";
                param[2].DataType = DbType.Int32;
                param[2].value = pdpCompanyId;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@pdpCreatedById";
                param[3].DataType = DbType.Int32;
                param[3].value = pdpCreatedById;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@pdpCreatedDate";
                param[4].DataType = DbType.DateTime;
                param[4].value = DateTime.Now;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@pdpDescription";
                param[5].DataType = DbType.String;
                param[5].value = pdpDescription;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@pdpPointAllocationFor";
                param[6].DataType = DbType.String;
                param[6].value = pdpPointAllocationFor;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@pdpPoint";
                param[7].DataType = DbType.Int32;
                param[7].value = pdpPoint;

                _ds = DBAccess.ExecDataSet("PDPPointMasterInsert", CommandType.StoredProcedure, param);
                return true;
            }
            catch (Exception ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        public Boolean PDPPointMasterUpdate(int pdpUserId, int pdpComId, int pdpCompanyId, int pdpCreatedById, string pdpDescription, string pdpPointAllocationFor, int pdpPoint, int pdpId)
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[9];

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@pdpId";
                param[8].DataType = DbType.Int32;
                param[8].value = pdpId;

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@pdpUserId";
                param[0].DataType = DbType.Int32;
                param[0].value = pdpUserId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@pdpComId ";
                param[1].DataType = DbType.Int32;
                param[1].value = pdpComId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@pdpCompanyId";
                param[2].DataType = DbType.Int32;
                param[2].value = pdpCompanyId;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@pdpCreatedById";
                param[3].DataType = DbType.Int32;
                param[3].value = pdpCreatedById;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@pdpCreatedDate";
                param[4].DataType = DbType.DateTime;
                param[4].value = DateTime.Now;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@pdpDescription";
                param[5].DataType = DbType.String;
                param[5].value = pdpDescription;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@pdpPointAllocationFor";
                param[6].DataType = DbType.String;
                param[6].value = pdpPointAllocationFor;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@pdpPoint";
                param[7].DataType = DbType.Int32;
                param[7].value = pdpPoint;

                _ds = DBAccess.ExecDataSet("PDPPointMasterUpdate", CommandType.StoredProcedure, param);
                return true;
            }
            catch (Exception ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }
        #endregion
    }
}
