//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace starteku_BusinessLogic.Model
{
    using System;
    
    public partial class GetAllCompetenceMaster_Result
    {
        public int comId { get; set; }
        public Nullable<int> comUserId { get; set; }
        public string comLevel { get; set; }
        public string comCompetence { get; set; }
        public Nullable<int> comCompanyId { get; set; }
        public Nullable<int> comCreateBy { get; set; }
        public int comDivId { get; set; }
        public Nullable<int> comJobtype { get; set; }
    }
}
