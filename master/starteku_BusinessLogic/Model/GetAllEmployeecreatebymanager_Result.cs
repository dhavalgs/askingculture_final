//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace starteku_BusinessLogic.Model
{
    using System;
    
    public partial class GetAllEmployeecreatebymanager_Result
    {
        public int userId { get; set; }
        public string name { get; set; }
        public string userFirstName { get; set; }
        public string userLastName { get; set; }
        public string userAddress { get; set; }
        public Nullable<int> userCountryId { get; set; }
        public Nullable<int> userStateId { get; set; }
        public string userCityId { get; set; }
        public string usercontact { get; set; }
        public string userEmail { get; set; }
        public Nullable<int> userType { get; set; }
        public string userPassword { get; set; }
        public string userCreateBy { get; set; }
        public string userDOB { get; set; }
        public string comname { get; set; }
        public string userImage { get; set; }
        public string jobName { get; set; }
        public string jobNameDN { get; set; }
        public string depName { get; set; }
        public string divName { get; set; }
        public string divNameDN { get; set; }
    }
}
