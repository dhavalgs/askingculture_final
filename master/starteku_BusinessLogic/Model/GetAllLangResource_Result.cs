//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace starteku_BusinessLogic.Model
{
    using System;
    
    public partial class GetAllLangResource_Result
    {
        public int Lang_ID { get; set; }
        public string LangType { get; set; }
        public Nullable<int> LangActCatId { get; set; }
        public Nullable<int> ResLangID { get; set; }
        public string LangText { get; set; }
        public Nullable<System.DateTime> LangCreateDate { get; set; }
        public string LangUpdateDate { get; set; }
        public string resLanguage { get; set; }
    }
}
