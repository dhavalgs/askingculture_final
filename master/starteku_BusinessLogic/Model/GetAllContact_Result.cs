//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace starteku_BusinessLogic.Model
{
    using System;
    
    public partial class GetAllContact_Result
    {
        public Nullable<int> UserId { get; set; }
        public Nullable<int> ContactCompanyId { get; set; }
        public int ContactId { get; set; }
        public Nullable<int> ContactJobId { get; set; }
        public Nullable<int> ContactLevel { get; set; }
        public Nullable<int> ContactUserId { get; set; }
        public string userFullname { get; set; }
        public string userEmail { get; set; }
        public string usercontact { get; set; }
        public string userImage { get; set; }
        public string userCityId { get; set; }
        public string staName { get; set; }
        public string couName { get; set; }
        public string userFirstName { get; set; }
        public Nullable<int> userId1 { get; set; }
    }
}
