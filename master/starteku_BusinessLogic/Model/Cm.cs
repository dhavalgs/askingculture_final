//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace starteku_BusinessLogic.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Cm
    {
        public int cmsId { get; set; }
        public string cmsName { get; set; }
        public string cmsDescription { get; set; }
        public Nullable<System.DateTime> cmsCreatedDate { get; set; }
        public Nullable<System.DateTime> cmsUpdatedDate { get; set; }
        public Nullable<bool> cmsIsDeleted { get; set; }
        public Nullable<bool> cmsIsActive { get; set; }
        public Nullable<int> cmsCompanyId { get; set; }
        public string cmsCreateBy { get; set; }
        public string cmsDescriptionDN { get; set; }
    }
}
