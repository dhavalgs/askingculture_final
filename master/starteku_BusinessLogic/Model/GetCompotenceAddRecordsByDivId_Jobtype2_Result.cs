//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace starteku_BusinessLogic.Model
{
    using System;
    
    public partial class GetCompotenceAddRecordsByDivId_Jobtype2_Result
    {
        public Nullable<long> no { get; set; }
        public int comId { get; set; }
        public string comCompetence { get; set; }
        public Nullable<int> comcategoryId { get; set; }
        public string comCompetence1 { get; set; }
        public Nullable<System.DateTime> comCreatedDate { get; set; }
        public Nullable<System.DateTime> comUpdatedDate { get; set; }
        public Nullable<bool> comIsActive { get; set; }
        public Nullable<bool> comIsDeleted { get; set; }
        public string comLevel { get; set; }
        public string comchildlevel { get; set; }
        public Nullable<int> skillLocal { get; set; }
        public Nullable<int> skillAchive { get; set; }
        public Nullable<int> skilltarget { get; set; }
        public string depId { get; set; }
    }
}
