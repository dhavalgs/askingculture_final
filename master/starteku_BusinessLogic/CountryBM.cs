﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starteku.Data.Manager;
using System.Data;

namespace starteku_BusinessLogic
{
  public  class CountryBM
    {
        #region Private Declaration

        private DataSet _ds;
        private string _returnString;
        private bool _returnBoolean;

        private Int32 _couId;
        private Int32 _countryCode;
        private String _couName;
        private DateTime _couCreatedDate;
        private DateTime _couUpdatedDate;
        private Boolean _couIsDeleted;
        private Boolean _couIsActive;
        private Int32 _couCompanyId;
       
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public DataSet ds
        {
            get { return _ds; }
            set { _ds = value; }
        }
        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public string ReturnString
        {
            get { return _returnString; }
            set { _returnString = value; }
        }

        /// <summary>
        /// Gets or sets the ReturnBoolean value.
        /// </summary>
        public bool ReturnBoolean
        {
            get { return _returnBoolean; }
            set { _returnBoolean = value; }
        }

        /// <summary>
        /// Gets or sets the custId value.
        /// </summary>
        public Int32 couId
        {
            get { return _couId; }
            set { _couId = value; }
        }
        public Int32 countryCode
        {
            get { return _countryCode; }
            set { _countryCode = value; }
        }
        public String couName
        {
            get { return _couName; }
            set { _couName = value; }
        }

        /// <summary>
        /// Gets or sets the catCreatedDate value.
        /// </summary>
        public DateTime couCreatedDate
        {
            get { return _couCreatedDate; }
            set { _couCreatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the catModifiedDate value.
        /// </summary>
        public DateTime couUpdatedDate
        {
            get { return _couUpdatedDate; }
            set { _couUpdatedDate = value; }
        }
        public Boolean couIsDeleted
        {
            get { return _couIsDeleted; }
            set { _couIsDeleted = value; }
        }
        public Boolean couIsActive
        {
            get { return _couIsActive; }
            set { _couIsActive = value; }
        }

        public Int32 couCompanyId
        {
            get { return _couCompanyId; }
            set { _couCompanyId = value; }
        }
      
        #endregion

        #region Method
        public void InsertCountry()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[6];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue; 

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@couName";
                param[0].DataType = DbType.String;
                param[0].value = _couName;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@couCompanyId";
                param[1].DataType = DbType.Int32;
                param[1].value = _couCompanyId;
                
                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@couIsActive";
                param[2].DataType = DbType.Boolean;
                param[2].value = _couIsActive;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@couIsDeleted";
                param[3].DataType = DbType.Boolean;
                param[3].value = _couIsDeleted;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@couCreatedDate";
                param[4].DataType = DbType.DateTime;
                param[4].value = _couCreatedDate;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@couUpdatedDate";
                param[5].DataType = DbType.DateTime;
                if (_couUpdatedDate.Ticks > dtmin.Ticks && _couUpdatedDate.Ticks < dtmax.Ticks)
                    param[5].value = _couUpdatedDate;
                else if (_couUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[5].value = _couUpdatedDate;

                _ds = DBAccess.ExecDataSet("InsertCountry", CommandType.StoredProcedure, param);
                _returnBoolean = true;

            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                Common.WriteLog("error in InsertCountry" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        public void InsertCountryMaster()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[7];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue; 

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@couName";
                param[0].DataType = DbType.String;
                param[0].value = _couName;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@couCompanyId";
                param[1].DataType = DbType.Int32;
                param[1].value = _couCompanyId;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@couIsActive";
                param[2].DataType = DbType.Boolean;
                param[2].value = _couIsActive;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@couIsDeleted";
                param[3].DataType = DbType.Boolean;
                param[3].value = _couIsDeleted;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@couCreatedDate";
                param[4].DataType = DbType.DateTime;
                param[4].value = _couCreatedDate;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@couUpdatedDate";
                param[5].DataType = DbType.DateTime;
                if (_couUpdatedDate.Ticks > dtmin.Ticks && _couUpdatedDate.Ticks < dtmax.Ticks)
                    param[5].value = _couUpdatedDate;
                else if (_couUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[5].value = _couUpdatedDate;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@countryCode";
                param[6].DataType = DbType.Int32;
                param[6].value = _countryCode;

                _ds = DBAccess.ExecDataSet("InsertCountryMaster", CommandType.StoredProcedure, param);
                _returnBoolean = true;

            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                Common.WriteLog("error in InsertCountryMaster" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        public void GetAllCountry()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@couIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _couIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@couIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _couIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@couCompanyId";
                param[2].DataType = DbType.Int32;
                param[2].value = _couCompanyId;

                _ds = DBAccess.ExecDataSet("GetAllCountry", CommandType.StoredProcedure, param);               
                
            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllCountry" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        public void GetAllCountrymaster()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@couIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _couIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@couIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _couIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@couCompanyId";
                param[2].DataType = DbType.Int32;
                param[2].value = _couCompanyId;

                _ds = DBAccess.ExecDataSet("GetAllCountrymaster", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllCountrymaster" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        public void CountryStatusUpdate()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] pm = new ParamStruct[3];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@couId";
                pm[0].DataType = DbType.Int32;
                pm[0].value = _couId;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@couIsActive";
                pm[1].DataType = DbType.Boolean;
                pm[1].value = _couIsActive;

                pm[2].direction = ParameterDirection.Input;
                pm[2].ParamName = "@couIsDeleted";
                pm[2].DataType = DbType.Boolean;
                pm[2].value = _couIsDeleted;

                _ds = DBAccess.ExecDataSet("CountryStatusUpdate", CommandType.StoredProcedure, pm);
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in CountryStatusUpdate  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            }
        }
        public void CountryStatusUpdatemaster()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] pm = new ParamStruct[3];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@couId";
                pm[0].DataType = DbType.Int32;
                pm[0].value = _couId;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@couIsActive";
                pm[1].DataType = DbType.Boolean;
                pm[1].value = _couIsActive;

                pm[2].direction = ParameterDirection.Input;
                pm[2].ParamName = "@couIsDeleted";
                pm[2].DataType = DbType.Boolean;
                pm[2].value = _couIsDeleted;

                _ds = DBAccess.ExecDataSet("CountryStatusUpdatemaster", CommandType.StoredProcedure, pm);
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in CountryStatusUpdatemaster  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            }
        }
        public void UpdateCountry()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[3];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue; 

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@couId";
                param[0].DataType = DbType.Int32;
                param[0].value = _couId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@couName";
                param[1].DataType = DbType.String;
                if (String.IsNullOrEmpty(_couName))
                    param[1].value = DBNull.Value;
                else
                    param[1].value = _couName;

               
                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@couUpdatedDate";
                param[2].DataType = DbType.DateTime;
                if (_couUpdatedDate.Ticks > dtmin.Ticks && _couUpdatedDate.Ticks < dtmax.Ticks)
                    param[2].value = _couUpdatedDate;
                else if (_couUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[2].value = _couUpdatedDate;


                _ds = DBAccess.ExecDataSet("UpdateCountry", CommandType.StoredProcedure, param);
                _returnBoolean = true;
             
               // obj.ExecScalar("UpdateCountry", CommandType.StoredProcedure, param); _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                Common.WriteLog("error in InsertCountry" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;
            }
        }
        public void UpdateCountrymaster()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[3];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue; 

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@couId";
                param[0].DataType = DbType.Int32;
                param[0].value = _couId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@couName";
                param[1].DataType = DbType.String;
                if (String.IsNullOrEmpty(_couName))
                    param[1].value = DBNull.Value;
                else
                    param[1].value = _couName;


                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@couUpdatedDate";
                param[2].DataType = DbType.DateTime;
                if (_couUpdatedDate.Ticks > dtmin.Ticks && _couUpdatedDate.Ticks < dtmax.Ticks)
                    param[2].value = _couUpdatedDate;
                else if (_couUpdatedDate.Equals(Convert.ToDateTime("1/1/0001 12:00:00 AM")))
                { }
                else
                    param[2].value = _couUpdatedDate;


                _ds = DBAccess.ExecDataSet("UpdateCountrymaster", CommandType.StoredProcedure, param);
                _returnBoolean = true;

                // obj.ExecScalar("UpdateCountry", CommandType.StoredProcedure, param); _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                Common.WriteLog("error in UpdateCountrymaster" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;
            }
        }

        public void GetAllCountrybyid()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@couId";
                param[0].DataType = DbType.Int32;
                param[0].value = _couId;


                _ds = DBAccess.ExecDataSet("GetAllCountrybyid", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllCountrybyid" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        public void GetAllCountrybyidmaster()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@couId";
                param[0].DataType = DbType.Int32;
                param[0].value = _couId;


                _ds = DBAccess.ExecDataSet("GetAllCountrybyidmaster", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllCountrybyidmaster" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void CountryCheckDuplication(string name, Int32 id)
        {
            try
            {
                DataAccess obj = new DataAccess();
                obj.Provider = EnumProviders.SQLClient;
                obj.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["connstring"].ToString();

                ParamStruct[] param = new ParamStruct[2];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue; 

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@couName";
                param[0].DataType = DbType.String;
                param[0].value = name;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@couId";
                param[1].DataType = DbType.Int32;
                param[1].value = id;

                _ds = obj.ExecDataSet("CountryCheckDuplication", CommandType.StoredProcedure, param);
                if (_ds.Tables[0].Rows.Count > 0)
                {
                    _returnString = (_ds.Tables[0].Rows[0]["returnStatement"].ToString());

                }
              
            }
            catch (DataException ex)
            {
                throw ex;
            }

            
        }
        public void CountryCheckDuplicationmaster(string name, Int32 id)
        {
            try
            {
                DataAccess obj = new DataAccess();
                obj.Provider = EnumProviders.SQLClient;
                obj.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["connstring"].ToString();

                ParamStruct[] param = new ParamStruct[2];
                DateTime dtmin = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime dtmax = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue; 

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@couName";
                param[0].DataType = DbType.String;
                param[0].value = name;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@couId";
                param[1].DataType = DbType.Int32;
                param[1].value = id;

                _ds = obj.ExecDataSet("CountryCheckDuplicationmaster", CommandType.StoredProcedure, param);
                if (_ds.Tables[0].Rows.Count > 0)
                {
                    _returnString = (_ds.Tables[0].Rows[0]["returnStatement"].ToString());

                }

            }
            catch (DataException ex)
            {
                throw ex;
            }


        }
     
        #endregion
    }
}
