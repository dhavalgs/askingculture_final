﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using starteku.Data.Manager;

namespace starteku_BusinessLogic
{
    public class ForumMasterBM
    {
        #region Private Declaration

        private DataSet _ds;
        private string _returnString;
        private bool _returnBoolean;
        private Int32 _forumCatId;
        private Int32 _forumCatJobId;
        private Int32 _forumCatLevel;
        private String _forumCatCompetence;
        private DateTime _forumCatCreatedDate;
        private DateTime _forumCatUpdatedDate;
        private Boolean _forumCatIsDeleted;
        private Boolean _forumCatIsActive;
        private Int32 _forumCatCompanyId;
        private String _forumCatCreateBy;
        private Int32 _forumCatDepId;
        private String _forumCatQuestion;
        private Int32 _forumCatQuestionNo;
        private String _forumCatDepartmentId;
        private String _forumCatIndId;

        private int _forumMasterId;
        private string _fmTagNames;
        private DateTime _fmCreatedDate;
        private DateTime _fmUpdatedDate;
        private bool _fmIsActive;
        private string _fmQuestionName;
        private string _fmCreateBy;
        private int _fmCategoryId;
        private bool _fmIsDeleted;
        private string _fmDescription;
        private int _fmCreateByUserID;
        private int _fmCompanyId;
        private string _fmCategoryName;
        
        private string _fmIsCompDevDoc;
        private string _fmJobTypeId;
        private string _fmDivId;
        private string _fmLevel;

        private int _fmCompId;

        private string _fmCompetence;



        #endregion

        #region Properties
        public string fmCategoryName
        {
            get { return _fmCategoryName; }
            set { _fmCategoryName = value; }
        }
        public int fmCreateByUserID
        {
            get { return _fmCreateByUserID; }
            set { _fmCreateByUserID = value; }
        }

        public int forumMasterId
        {
            get { return _forumMasterId; }
            set { _forumMasterId = value; }
        }
         public string fmTagNames
        {
            get { return _fmTagNames; }
            set { _fmTagNames = value; }
        }
         public int fmCategoryId
        {
            get { return _fmCategoryId; }
            set { _fmCategoryId = value; }
        }
         public string fmCreateBy
        {
            get { return _fmCreateBy; }
            set { _fmCreateBy = value; }
        }
         public bool fmIsActive
        {
            get { return _fmIsActive; }
            set { _fmIsActive = value; }
        }
          public bool fmIsDeleted
        {
            get { return _fmIsDeleted; }
            set { _fmIsDeleted = value; }
        }
         public string fmQuestionName
        {
            get { return _fmQuestionName; }
            set { _fmQuestionName = value; }
        }
       
         public DateTime fmUpdatedDate
        {
            get { return _fmUpdatedDate; }
            set
            {
                
                _fmUpdatedDate = value;
            }
        }

        public DateTime fmCreatedDate
        {
            get { return _fmCreatedDate; }
            set { _fmCreatedDate = value; }
        }


        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public DataSet ds
        {
            get { return _ds; }
            set { _ds = value; }
        }
        /// <summary>
        /// Gets or sets the ReturnString value.
        /// </summary>
        public string ReturnString
        {
            get { return _returnString; }
            set { _returnString = value; }
        }

        /// <summary>
        /// Gets or sets the ReturnBoolean value.
        /// </summary>
        public bool ReturnBoolean
        {
            get { return _returnBoolean; }
            set { _returnBoolean = value; }
        }

        /// <summary>
        /// Gets or sets the comId value.
        /// </summary>
        public Int32 forumCatId
        {
            get { return _forumCatId; }
            set { _forumCatId = value; }
        }

        /// <summary>
        /// Gets or sets the forumCatJobId value.
        /// </summary>
        public Int32 forumCatJobId
        {
            get { return _forumCatJobId; }
            set { _forumCatJobId = value; }
        }

        /// <summary>
        /// Gets or sets the forumCatLevel value.
        /// </summary>
        public Int32 forumCatLevel
        {
            get { return _forumCatLevel; }
            set { _forumCatLevel = value; }
        }

        /// <summary>
        /// Gets or sets the forumCatforumCatpetence value.
        /// </summary>
        public String forumCatCompetence
        {
            get { return _forumCatCompetence; }
            set { _forumCatCompetence = value; }
        }

        /// <summary>
        /// Gets or sets the comCreatedDate value.
        /// </summary>
        public DateTime forumCatCreatedDate
        {
            get { return _forumCatCreatedDate; }
            set { _forumCatCreatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the forumCatUpdatedDate value.
        /// </summary>
        public DateTime forumCatUpdatedDate
        {
            get { return _forumCatUpdatedDate; }
            set { _forumCatUpdatedDate = value; }
        }

        /// <summary>
        /// Gets or sets the forumCatIsDeleted value.
        /// </summary>
        public Boolean forumCatIsDeleted
        {
            get { return _forumCatIsDeleted; }
            set { _forumCatIsDeleted = value; }
        }

        /// <summary>
        /// Gets or sets the forumCatIsActive value.
        /// </summary>
        public Boolean forumCatIsActive
        {
            get { return _forumCatIsActive; }
            set { _forumCatIsActive = value; }
        }

        /// <summary>
        /// Gets or sets the forumCatforumCatpanyId value.
        /// </summary>
        public Int32 forumCatCompanyId
        {
            get { return _forumCatCompanyId; }
            set { _forumCatCompanyId = value; }
        }

        /// <summary>
        /// Gets or sets the comCreateBy value.
        /// </summary>
        public String forumCatCreateBy
        {
            get { return _forumCatCreateBy; }
            set { _forumCatCreateBy = value; }
        }

        public int fmCompanyId
        {
            get { return _fmCompanyId; }
            set { _fmCompanyId = value; }
        }
        
        /// <summary>
        /// Gets or sets the forumCatDepId value.
        /// </summary>
        public Int32 forumCatDepId
        {
            get { return _forumCatDepId; }
            set { _forumCatDepId = value; }
        }

        public String forumCatQuestion
        {
            get { return _forumCatQuestion; }
            set { _forumCatQuestion = value; }
        }

        /// <summary>
        /// Gets or sets the forumCatDepId value.
        /// </summary>
        public Int32 forumCatQuestionNo
        {
            get { return _forumCatQuestionNo; }
            set { _forumCatQuestionNo = value; }
        }

        public String forumCatDepartmentId
        {
            get { return _forumCatDepartmentId; }
            set { _forumCatDepartmentId = value; }
        }
        public String forumCatIndId
        {
            get { return _forumCatIndId; }
            set { _forumCatIndId = value; }
        }
        public string fmDescription
        {
            get { return _fmDescription; }
            set { _fmDescription = value; }
        }

        public string fmJobTypeId
        {
            get { return _fmJobTypeId; }
            set { _fmJobTypeId = value; }
        }

        public string fmIsCompDevDoc
        {
            get { return _fmIsCompDevDoc; }
            set { _fmIsCompDevDoc = value; }
        }

        public string fmDivId
        {
            get { return _fmDivId; }
            set { _fmDivId = value; }
        }

        public string fmLevel
        {
            get { return _fmLevel; }
            set { _fmLevel = value; }
        }


        public Int32 fmCompId
        {
            get { return _fmCompId; }
            set { _fmCompId = value; }
        }

        public string fmCompetence
        {
            get { return _fmCompetence; }
            set { _fmCompetence = value; }
        }


        #endregion

        #region Methods Organization & Employer

        public void GetForumCategoryByforumCompanyId()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@forumCatIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _forumCatIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@forumCatIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _forumCatIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@forumCatCompanyId";
                param[2].DataType = DbType.Int32;
                param[2].value = _forumCatCompanyId;

                _ds = DBAccess.ExecDataSet("GetForumCategoryByCompanyId", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllState" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void GetForumTagsByforumCompanyId()
        {
            try
            {
                return;
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@tagIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _forumCatIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@tagIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _forumCatIsDeleted;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@tagCompanyId";
                param[2].DataType = DbType.Int32;
                param[2].value = _forumCatCompanyId;

                _ds = DBAccess.ExecDataSet("GetForumTagsByCompanyId", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllState" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }




        public int InsertForum()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[16];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@fmQuestionName";
                param[0].DataType = DbType.String;
                param[0].value = _fmQuestionName;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@fmCreatedDate";
                param[1].DataType = DbType.DateTime;
                param[1].value = _fmCreatedDate;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@fmTagNames";
                param[2].DataType = DbType.String;
                param[2].value = _fmTagNames;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@fmIsActive";
                param[3].DataType = DbType.Boolean;
                param[3].value = _fmIsActive;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@fmCompanyId";
                param[4].DataType = DbType.Int32;
                param[4].value = _fmCompanyId;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@fmCreateByUserID";
                param[5].DataType = DbType.Int32;
                param[5].value = _fmCreateByUserID;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@fmDescription";
                param[6].DataType = DbType.String;
                param[6].value = _fmDescription;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@fmCategoryId";
                param[7].DataType = DbType.Int32;
                param[7].value = _fmCategoryId;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@fmCategoryName";
                param[8].DataType = DbType.String;
                param[8].value = _fmCategoryName;

                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@fmIsDeleted";
                param[9].DataType = DbType.String;
                param[9].value = _fmIsDeleted;

                //2015-11-18
                param[10].direction = ParameterDirection.Input;
                param[10].ParamName = "@fmIsCompDevDoc";
                param[10].DataType = DbType.String;
                param[10].value = _fmIsCompDevDoc;

                param[11].direction = ParameterDirection.Input;
                param[11].ParamName = "@fmJobTypeId";
                param[11].DataType = DbType.String;
                param[11].value = _fmJobTypeId;


                param[12].direction = ParameterDirection.Input;
                param[12].ParamName = "@fmDivId";
                param[12].DataType = DbType.String;
                param[12].value = _fmDivId;

                param[13].direction = ParameterDirection.Input;
                param[13].ParamName = "@fmLevel";
                param[13].DataType = DbType.String;
                param[13].value = _fmLevel;

                param[13].direction = ParameterDirection.Input;
                param[13].ParamName = "@fmLevel";
                param[13].DataType = DbType.String;
                param[13].value = _fmLevel;

                param[14].direction = ParameterDirection.Input;
                param[14].ParamName = "@fmCompId";
                param[14].DataType = DbType.Int32;
                param[14].value = _fmCompId;

                param[15].direction = ParameterDirection.Input;
                param[15].ParamName = "@fmCompetence";
                param[15].DataType = DbType.String;
                param[15].value = _fmCompetence;

                

                _ds = DBAccess.ExecDataSet("InsertForum", CommandType.StoredProcedure, param);
            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllState" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            }
            return 0;
        }
        #endregion



        public void GetAllForum()
        {
            DataAccess DBAccess = new DataAccess();
            DBAccess.Provider = EnumProviders.SQLClient;
            DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
            ParamStruct[] param = new ParamStruct[3];

            param[0].direction = ParameterDirection.Input;
            param[0].ParamName = "@fmIsDeleted";
            param[0].DataType = DbType.Boolean;
            param[0].value = _fmIsDeleted;

            param[1].direction = ParameterDirection.Input;
            param[1].ParamName = "@fmIsActive";
            param[1].DataType = DbType.Boolean;
            param[1].value = _fmIsActive;

            param[2].direction = ParameterDirection.Input;
            param[2].ParamName = "@fmCompanyId";
            param[2].DataType = DbType.Int32;
            param[2].value = _fmCompanyId;

            _ds = DBAccess.ExecDataSet("GetAllForum", CommandType.StoredProcedure, param);


        }

        public void GetForumMasterById()
        {
            DataAccess DBAccess = new DataAccess();
            DBAccess.Provider = EnumProviders.SQLClient;
            DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
            ParamStruct[] param = new ParamStruct[4];

            param[0].direction = ParameterDirection.Input;
            param[0].ParamName = "@fmIsDeleted";
            param[0].DataType = DbType.Boolean;
            param[0].value = _fmIsDeleted;

            param[1].direction = ParameterDirection.Input;
            param[1].ParamName = "@fmIsActive";
            param[1].DataType = DbType.Boolean;
            param[1].value = _fmIsActive;

            param[2].direction = ParameterDirection.Input;
            param[2].ParamName = "@fmCompanyId";
            param[2].DataType = DbType.Int32;
            param[2].value = _fmCompanyId;

            param[3].direction = ParameterDirection.Input;
            param[3].ParamName = "@forumMasterId";
            param[3].DataType = DbType.Int32;
            param[3].value = _forumMasterId;

            _ds = DBAccess.ExecDataSet("GetForumMasterById", CommandType.StoredProcedure, param);

        }

        public void InsertForumReply()
        {
             try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[10];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@forumMasterId";
                param[0].DataType = DbType.Int32;
                param[0].value = _forumMasterId;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@frCreatedDate";
                param[1].DataType = DbType.DateTime;
                param[1].value = _fmCreatedDate;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@frTagNames";
                param[2].DataType = DbType.String;
                param[2].value = _fmTagNames;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@frIsActive";
                param[3].DataType = DbType.Boolean;
                param[3].value = _fmIsActive;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@frCompanyId";
                param[4].DataType = DbType.Int32;
                param[4].value = _fmCompanyId;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@frCreateByUserID";
                param[5].DataType = DbType.Int32;
                param[5].value = _fmCreateByUserID;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@frDescription";
                param[6].DataType = DbType.String;
                param[6].value = _fmDescription;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@frCategoryId";
                param[7].DataType = DbType.Int32;
                param[7].value = _fmCategoryId;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@frCategoryName";
                param[8].DataType = DbType.String;
                param[8].value = _fmCategoryName;

                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@frIsDeleted";
                param[9].DataType = DbType.String;
                param[9].value = _fmIsDeleted;



                _ds = DBAccess.ExecDataSet("InsertForumReply", CommandType.StoredProcedure, param);
            }
            catch( Exception e ){}
        }

        public void GetForumReplyByForumMasterId()
        {
            DataAccess DBAccess = new DataAccess();
            DBAccess.Provider = EnumProviders.SQLClient;
            DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
            ParamStruct[] param = new ParamStruct[4];

            param[0].direction = ParameterDirection.Input;
            param[0].ParamName = "@frIsDeleted";
            param[0].DataType = DbType.Boolean;
            param[0].value = _fmIsDeleted;

            param[1].direction = ParameterDirection.Input;
            param[1].ParamName = "@frIsActive";
            param[1].DataType = DbType.Boolean;
            param[1].value = _fmIsActive;

            param[2].direction = ParameterDirection.Input;
            param[2].ParamName = "@frCompanyId";
            param[2].DataType = DbType.Int32;
            param[2].value = _fmCompanyId;

            param[3].direction = ParameterDirection.Input;
            param[3].ParamName = "@forumMasterId";
            param[3].DataType = DbType.Int32;
            param[3].value = _forumMasterId;

            _ds = DBAccess.ExecDataSet("GetForumReplyByForumMasterId", CommandType.StoredProcedure, param);
        }
    }
}
