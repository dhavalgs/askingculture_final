﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using starteku_BusinessLogic.Model;
using starteku.Data.Manager;
namespace starteku_BusinessLogic
{
  public class CompanyDirectionElement
    {
        #region Private Declaration

        private DataSet _ds;
        private bool _returnBoolean;
        private Int32 _dirID;
        private String _dirName;
        private String _dirNameDN;
        private String _dirDescription;
        private Int32 _dirIndex;
        private Int32 _dirCreatedBy;
       
        private DateTime _dirCreateDate;
        private DateTime _dirUpdateDate;
        private Boolean _dirIsActive;
        private Boolean _dirIsMandatory;
        private Int32 _dirCompanyID;
        private Boolean _dirIsDelete;
        private String _dirCatID;
        private String _dirIDs;

        private Int32 _dassignID;
        private Int32 _UserID;
        private DateTime _StartDate;
        private DateTime _EndDate;
        private String _Status;
        private String _ElementIDs;
        private Int32 _TeamID;
        private String _UserIDs;

        private Int32 _LangID;

        #endregion

        #region Properties
        public Int32 LangID
        {
            get { return _LangID; }
            set { _LangID = value; }
        }
        public Int32 dirID
        {
            get { return _dirID; }
            set { _dirID = value; }
        }
        public string dirCatID
        {
            get { return _dirCatID; }
            set { _dirCatID = value; }
        }

        public string dirName
        {
            get { return _dirName; }
            set { _dirName = value; }
        }

        public string dirNameDN
        {
            get { return _dirNameDN; }
            set { _dirNameDN = value; }
        }

        /// Gets or sets the ReturnString value.

        public DataSet ds
        {
            get { return _ds; }
            set { _ds = value; }
        }



        public string dirDescription
        {
            get { return _dirDescription; }
            set { _dirDescription = value; }
        }

        public string dirIDs
        {
            get { return _dirIDs; }
            set { _dirIDs = value; }
        }

        public Int32 dirIndex
        {
            get { return _dirIndex; }
            set { _dirIndex = value; }
        }


        public Int32 dirCreatedBy
        {
            get { return _dirCreatedBy; }
            set { _dirCreatedBy = value; }
        }
        public DateTime dirCreateDate
        {
            get { return _dirCreateDate; }
            set { _dirCreateDate = value; }
        }


        public DateTime dirUpdateDate
        {
            get { return _dirUpdateDate; }
            set { _dirUpdateDate = value; }
        }



        public Int32 dirCompanyID
        {
            get { return _dirCompanyID; }
            set { _dirCompanyID = value; }
        }


        public Boolean dirIsActive
        {
            get { return _dirIsActive; }
            set { _dirIsActive = value; }
        }

        public Boolean dirIsDelete
        {
            get { return _dirIsDelete; }
            set { _dirIsDelete = value; }
        }
        public Boolean dirIsMandatory
        {
            get { return _dirIsMandatory; }
            set { _dirIsMandatory = value; }
        }

        public bool ReturnBoolean
        {
            get { return _returnBoolean; }
            set { _returnBoolean = value; }
        }


        public Int32 dassignID
        {
            get { return _dassignID;  }
            set { _dassignID = value; }
        }
        public Int32 UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }
        public Int32 TeamID
        {
            get { return _TeamID;  }
            set { _TeamID = value; }
        }
        public DateTime StartDate
        {
            get { return _StartDate; }
            set { _StartDate = value; }
        }
        public DateTime EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }
        public String Status
        {
            get { return _Status; }
            set { _Status = value; }
        }
        public String ElementIDs
        {
            get { return _ElementIDs; }
            set { _ElementIDs = value; }
        }
        public String UserIDs
        {
            get { return _UserIDs; }
            set { _UserIDs = value; }
        }
        #endregion

        #region Methods
        public void InsertCompanyDirectionElement()
        {
          //  Common.WriteLog("InsertCompanyDirectionElement 1");
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();

                ParamStruct[] param = new ParamStruct[13];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@dirName";
                param[0].DataType = DbType.String;
                param[0].value = _dirName;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@dirNameDN";
                param[1].DataType = DbType.String;
                param[1].value = _dirNameDN;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@dirDescription";
                param[2].DataType = DbType.String;
                param[2].value = _dirDescription;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@dirIndex";
                param[3].DataType = DbType.Int32;
                param[3].value = _dirIndex;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@dirCreatedBy";
                param[4].DataType = DbType.Int32;
                param[4].value = _dirCreatedBy;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@dirCreateDate";
                param[5].DataType = DbType.DateTime;
                param[5].value = _dirCreateDate;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@dirUpdateDate";
                param[6].DataType = DbType.DateTime;
                param[6].value = _dirUpdateDate;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@dirIsActive";
                param[7].DataType = DbType.Boolean;
                param[7].value = _dirIsActive;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@dirCompanyID";
                param[8].DataType = DbType.Int32;
                param[8].value = _dirCompanyID;

                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@dirIsDelete";
                param[9].DataType = DbType.Boolean;
                param[9].value = _dirIsDelete;

                param[10].direction = ParameterDirection.Input;
                param[10].ParamName = "@dirCatID";
                param[10].DataType = DbType.String;
                param[10].value = _dirCatID;

                param[11].direction = ParameterDirection.Input;
                param[11].ParamName = "@dirID";
                param[11].DataType = DbType.Int32;
                param[11].value = _dirID;

                param[12].direction = ParameterDirection.Input;
                param[12].ParamName = "@dirIsMandatory";
                param[12].DataType = DbType.Boolean;
                param[12].value = _dirIsMandatory;

               // Common.WriteLog("InsertCompanyDirectionElement SpInsertCategoryElement 2");
                _ds = DBAccess.ExecDataSet("SpInsertCategoryElement", CommandType.StoredProcedure, param);
                //Common.WriteLog("InsertCompanyDirectionElement SpInsertCategoryElement 3");

            }
            catch (DataException ex)
            {
                ExceptionLogger.LogException(ex, 0, "SpInsertCategoryElement");
                Common.WriteLog("error in SpInsertCategoryElement" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;
            }
        }

        public void GetAllElementList()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[5];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@dirIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _dirIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@dirIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _dirIsDelete;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@dirID";
                param[2].DataType = DbType.String;
                param[2].value = _dirIDs;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@dirCompanyID";
                param[3].DataType = DbType.String;
                param[3].value = _dirCompanyID;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@LangID";
                param[4].DataType = DbType.Int32;
                param[4].value = _LangID;

                _ds = DBAccess.ExecDataSet("GetAllElementList", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllElementList" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void ElementDelete()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] pm = new ParamStruct[3];

                pm[0].direction = ParameterDirection.Input;
                pm[0].ParamName = "@dirID";
                pm[0].DataType = DbType.Int32;
                pm[0].value = _dirID;

                pm[1].direction = ParameterDirection.Input;
                pm[1].ParamName = "@dirIsActive";
                pm[1].DataType = DbType.Boolean;
                pm[1].value = _dirIsActive;

                pm[2].direction = ParameterDirection.Input;
                pm[2].ParamName = "@dirIsDeleted";
                pm[2].DataType = DbType.Boolean;
                pm[2].value = _dirIsDelete;

                _ds = DBAccess.ExecDataSet("ElementDelete", CommandType.StoredProcedure, pm);
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in ElementDelete  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            }
        }

        public void GetElementByID()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[3];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@dirIsActive";
                param[0].DataType = DbType.Boolean;
                param[0].value = _dirIsActive;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@dirIsDeleted";
                param[1].DataType = DbType.Boolean;
                param[1].value = _dirIsDelete;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@dirCatID";
                param[2].DataType = DbType.String;
                param[2].value = _dirCatID;

                _ds = DBAccess.ExecDataSet("GetElementBySelID", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetElementBySelID" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }


        public void GetElementBy_Category()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[2];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@dirIDs";
                param[0].DataType = DbType.String;
                param[0].value = _dirIDs;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@dirCatID";
                param[1].DataType = DbType.String;
                param[1].value = _dirCatID;



                _ds = DBAccess.ExecDataSet("GetElementBy_Category", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetElementBy_Category" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        #endregion


        #region NewMethod
        public void InsertDirectionAssign()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[12];



                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@dassignID";
                param[0].DataType = DbType.Int32;
                param[0].value = _dassignID;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@UserID";
                param[1].DataType = DbType.Int32;
                param[1].value = _UserID;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@CompanyID";
                param[2].DataType = DbType.Int32;
                param[2].value = _dirCompanyID;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@StartDate";
                param[3].DataType = DbType.DateTime;
                param[3].value = _StartDate;


                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@EndDate";
                param[4].DataType = DbType.DateTime;
                param[4].value = _EndDate;

                param[5].direction = ParameterDirection.Input;
                param[5].ParamName = "@IsActive";
                param[5].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(Convert.ToString(_dirIsActive)))
                    param[5].value = DBNull.Value;
                else
                    param[5].value = _dirIsActive;

                param[6].direction = ParameterDirection.Input;
                param[6].ParamName = "@IsDeleted";
                param[6].DataType = DbType.Boolean;
                if (String.IsNullOrEmpty(Convert.ToString(_dirIsDelete)))
                    param[6].value = DBNull.Value;
                else
                    param[6].value = _dirIsDelete;

                param[7].direction = ParameterDirection.Input;
                param[7].ParamName = "@Status";
                param[7].DataType = DbType.String;
                param[7].value = _Status;

                param[8].direction = ParameterDirection.Input;
                param[8].ParamName = "@DirCatID";
                param[8].DataType = DbType.Int32;
                param[8].value = _dirCatID;

                param[9].direction = ParameterDirection.Input;
                param[9].ParamName = "@ElementIDs";
                param[9].DataType = DbType.String;
                param[9].value = _ElementIDs;

                param[10].direction = ParameterDirection.Input;
                param[10].ParamName = "@TeamID";
                param[10].DataType = DbType.Int32;
                param[10].value = _TeamID;

                param[11].direction = ParameterDirection.Input;
                param[11].ParamName = "@UserIDs";
                param[11].DataType = DbType.String;
                param[11].value = _UserIDs;

                _ds = DBAccess.ExecDataSet("InsertDirectionAssign", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }

        public void GetAllAssignDirectionList()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[5];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@UserID";
                param[0].DataType = DbType.Int32;
                param[0].value = _UserID;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@CompanyID";
                param[1].DataType = DbType.Int32;
                param[1].value = _dirCompanyID;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@IsActive";
                param[2].DataType = DbType.Boolean;
                param[2].value = _dirIsActive;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@IsDelete";
                param[3].DataType = DbType.Boolean;
                param[3].value = _dirIsDelete;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@LangId";
                param[4].DataType = DbType.Int32;
                param[4].value = _LangID;

                _ds = DBAccess.ExecDataSet("GetAllAssignDirectionList", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetAllQuestionAssignList" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        public void DeleteDirectionAssign()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[1];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@dassignID";
                param[0].DataType = DbType.Int32;
                param[0].value = _dassignID;


                _ds = DBAccess.ExecDataSet("DeleteDirectionAssign", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in DeleteDirectionAssign" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        public void UpdateDirectionAssignStatus()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[2];


                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@dassignID";
                param[0].DataType = DbType.Int32;
                param[0].value = _dassignID;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@Status";
                param[1].DataType = DbType.String;
                param[1].value = _Status;



                _ds = DBAccess.ExecDataSet("UpdateDirectionAssignStatus", CommandType.StoredProcedure, param);
                _returnBoolean = true;
            }
            catch (DataException ex)
            {
                _returnBoolean = false;
                throw ex;
            }
        }
        public void GetPublishDirectionUserList()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[2];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@CreatedByID";
                param[0].DataType = DbType.Int32;
                param[0].value = _UserID;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@CompanyID";
                param[1].DataType = DbType.Int32;
                param[1].value = _dirCompanyID;


                _ds = DBAccess.ExecDataSet("GetPublishDirectionUserList", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetPublishQuesUserList" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        public void GetPublishUserDirectionList()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[5];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@CreatedByID";
                param[0].DataType = DbType.Int32;
                param[0].value = _UserID;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@CompanyID";
                param[1].DataType = DbType.Int32;
                param[1].value = _dirCompanyID;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@assignuserID";
                param[2].DataType = DbType.Int32;
                param[2].value = _dassignID;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@Status";
                param[3].DataType = DbType.String;
                param[3].value = _Status;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@LangId";
                param[4].DataType = DbType.String;
                param[4].value = _LangID;

                _ds = DBAccess.ExecDataSet("GetPublishUserDirectionList", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetPublishUserDirectionList" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }

        public void GetAllUserDirectionList_PDP()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[5];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@UserID";
                param[0].DataType = DbType.Int32;
                param[0].value = _UserID;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@CompanyID";
                param[1].DataType = DbType.Int32;
                param[1].value = _dirCompanyID;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@Status";
                param[2].DataType = DbType.String;
                param[2].value = _Status;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@CurrentDate";
                param[3].DataType = DbType.DateTime;
                param[3].value = _dirCreateDate;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@LangID";
                param[4].DataType = DbType.Int32;
                param[4].value = _dirIndex;

                _ds = DBAccess.ExecDataSet("GetUserAssignDirectionList_PDP", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetUserAssignDirectionList_PDP" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
        public void GetUserAssignDirectionElementList_PDP()
        {
            try
            {
                DataAccess DBAccess = new DataAccess();
                DBAccess.Provider = EnumProviders.SQLClient;
                DBAccess.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnString"].ToString();
                ParamStruct[] param = new ParamStruct[5];

                param[0].direction = ParameterDirection.Input;
                param[0].ParamName = "@UserID";
                param[0].DataType = DbType.Int32;
                param[0].value = _UserID;

                param[1].direction = ParameterDirection.Input;
                param[1].ParamName = "@CompanyID";
                param[1].DataType = DbType.Int32;
                param[1].value = _dirCompanyID;

                param[2].direction = ParameterDirection.Input;
                param[2].ParamName = "@Status";
                param[2].DataType = DbType.String;
                param[2].value = _Status;

                param[3].direction = ParameterDirection.Input;
                param[3].ParamName = "@CurrentDate";
                param[3].DataType = DbType.DateTime;
                param[3].value = _dirCreateDate;

                param[4].direction = ParameterDirection.Input;
                param[4].ParamName = "@CatID";
                param[4].DataType = DbType.String;
                param[4].value = _dirCatID;

              

                _ds = DBAccess.ExecDataSet("GetUserAssignDirectionElementList_PDP", CommandType.StoredProcedure, param);

            }
            catch (DataException ex)
            {
                Common.WriteLog("error in GetUserAssignDirectionElementList_PDP" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
                throw ex;

            }
        }
#endregion
    }
}
